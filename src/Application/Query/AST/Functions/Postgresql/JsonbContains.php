<?php

namespace Application\Query\AST\Functions\Postgresql;


class JsonbContains extends PostgresqlBinaryFunctionNode
{
	const FUNCTION_NAME = 'JSONB_CONTAINS';
	const OPERATOR = '@>';
}
