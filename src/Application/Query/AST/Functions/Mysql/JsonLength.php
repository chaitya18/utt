<?php

namespace Application\Query\AST\Functions\Mysql;

/**
 * "JSON_LENGTH" "(" StringPrimary ")"
 */
class JsonLength extends JsonKeys
{
	const FUNCTION_NAME = 'JSON_LENGTH';
}
