<?php

namespace Application\Query\AST\Functions\Mysql;

/**
 * "JSON_REPLACE" "(" StringPrimary "," StringPrimary "," StringPrimary { "," StringPrimary "," StringPrimary }* ")"
 */
class JsonReplace extends JsonSet
{
	const FUNCTION_NAME = 'JSON_REPLACE';
}
