<?php

namespace UTT\EstateBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

use UTT\EstateBundle\Entity\Photo;
use UTT\EstateBundle\Entity\Estate;
use Doctrine\ORM\EntityManager;
use UTT\EstateBundle\Entity\PhotoRepository;

class EstatePhotosFileNameConvertCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('estate:photosFileNameConvert')
            ->setDescription('')
        ;
    }

    public function slugify($text){
        // replace non letter or digits by -
        $text = preg_replace('~[^\pL\d]+~u', '-', $text);

        // transliterate
        $text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);

        // remove unwanted characters
        $text = preg_replace('~[^-\w]+~', '', $text);

        // trim
        $text = trim($text, '-');

        // remove duplicate -
        $text = preg_replace('~-+~', '-', $text);

        // lowercase
        $text = strtolower($text);

        if (empty($text))
        {
            return 'n-a';
        }

        return $text;
    }


    protected function execute(InputInterface $input, OutputInterface $output)
    {
        /** @var EntityManager $_em */
        $_em = $this->getContainer()->get('doctrine')->getManager();

        /** @var PhotoRepository $photoRepository */
        $photoRepository = $_em->getRepository('UTTEstateBundle:Photo');

        $photos = $photoRepository->findBy(array('isFileNameConverted' => false), array());
        if($photos){
            /** @var Photo $photo */
            foreach($photos as $photo){
                $fullFileName = explode('/', $photo->getFileName());
                $dirName = $fullFileName[0];
                $fileName = $fullFileName[1];
                $fileName = explode('.', $fileName);

                if($photo->getEstate()){
                    $estateName = $this->slugify($photo->getEstate()->getName());
                    $newFileName = $estateName.'-'.$photo->getSortOrder();

                    $fn = 'web/'.$photo->getUploadDir().'/'.$dirName.'/'.$fileName[0].'.'.$fileName[1];
                    $newFn = 'web/'.$photo->getUploadDir().'/'.$dirName.'/'.$newFileName.'.'.$fileName[1];

                    if(file_exists($fn)){
                        rename($fn, $newFn);
                        if(file_exists($newFn)){
                            $photo->setFileName($dirName.'/'.$newFileName.'.'.$fileName[1]);
                            $photo->setIsFileNameConverted(true);
                            $_em->persist($photo);
                            $_em->flush();

                            echo $photo->getId().PHP_EOL;
                            echo $fn.PHP_EOL;
                            echo $newFn.PHP_EOL;
                            echo PHP_EOL;
                        }
                    }
                }
            }
        }
    }
}