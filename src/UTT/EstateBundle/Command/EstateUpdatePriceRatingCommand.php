<?php

namespace UTT\EstateBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

use UTT\ReservationBundle\Entity\PricingCategory;
use UTT\ReservationBundle\Service\PricingService;
use UTT\EstateBundle\Entity\Estate;
use UTT\EstateBundle\Entity\EstateRepository;
use UTT\UserBundle\Entity\ActivityLog;
use UTT\UserBundle\Entity\ActivityLogRepository;

use Doctrine\ORM\EntityManager;

class EstateUpdatePriceRatingCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('estate:updatePriceRating')
            ->setDescription('')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        /** @var EntityManager $_em */
        $_em = $this->getContainer()->get('doctrine')->getManager();
        /** @var EstateRepository $estateRepository */
        $estateRepository = $_em->getRepository('UTTEstateBundle:Estate');

        $estates = $estateRepository->getAllActive();
        if($estates){
            /** @var Estate $estate */
            foreach($estates as $estate){
                echo '--------------'.PHP_EOL;

                if(
                    $estate->getPricingCategory()->getId() == 1 ||
                    $estate->getPricingCategory()->getId() == 2 ||
                    $estate->getPricingCategory()->getId() == 3 ||
                    $estate->getPricingCategory()->getId() == 4
                ){
                    $estate->setStarRatingPrice(1);
                }elseif(
                    $estate->getPricingCategory()->getId() == 5 ||
                    $estate->getPricingCategory()->getId() == 6 ||
                    $estate->getPricingCategory()->getId() == 7 ||
                    $estate->getPricingCategory()->getId() == 8
                ){
                    $estate->setStarRatingPrice(2);
                }elseif(
                    $estate->getPricingCategory()->getId() == 9 ||
                    $estate->getPricingCategory()->getId() == 10 ||
                    $estate->getPricingCategory()->getId() == 11 ||
                    $estate->getPricingCategory()->getId() == 12
                ){
                    $estate->setStarRatingPrice(3);
                }elseif(
                    $estate->getPricingCategory()->getId() == 13 ||
                    $estate->getPricingCategory()->getId() == 14 ||
                    $estate->getPricingCategory()->getId() == 15 ||
                    $estate->getPricingCategory()->getId() == 16 ||
                    $estate->getPricingCategory()->getId() == 17 ||
                    $estate->getPricingCategory()->getId() == 18
                ){
                    $estate->setStarRatingPrice(4);
                }elseif($estate->getPricingCategory()->getId() >= 19){
                    $estate->setStarRatingPrice(5);
                }

                $_em->flush();

                echo $estate->getName().' '.$estate->getPricingCategory()->getName().' '.$estate->getStarRatingPrice().PHP_EOL;
                echo 'SAVED'.PHP_EOL.PHP_EOL;
            }
        }else{
            echo 'No estates found'.PHP_EOL;
        }
    }
}