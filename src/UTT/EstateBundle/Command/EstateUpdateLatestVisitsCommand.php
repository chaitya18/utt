<?php

namespace UTT\EstateBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

use UTT\ReservationBundle\Service\PricingService;
use UTT\EstateBundle\Entity\Estate;
use UTT\EstateBundle\Entity\EstateRepository;
use UTT\UserBundle\Entity\ActivityLog;
use UTT\UserBundle\Entity\ActivityLogRepository;

use Doctrine\ORM\EntityManager;
use UTT\UserBundle\Service\ActivityLogService;

class EstateUpdateLatestVisitsCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('estate:updateLatestVisits')
            ->setDescription('')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        /** @var EntityManager $_em */
        $_em = $this->getContainer()->get('doctrine')->getManager();
        /** @var EstateRepository $estateRepository */
        $estateRepository = $_em->getRepository('UTTEstateBundle:Estate');
        /** @var ActivityLogRepository $activityLogRepository */
        $activityLogRepository = $_em->getRepository('UTTUserBundle:ActivityLog');

        $estates = $estateRepository->getAllActive();
        if($estates){
            /** @var Estate $estate */
            foreach($estates as $estate){
                echo '--------------'.PHP_EOL;
                echo $estate->getName().PHP_EOL;

                $activityLogs = $activityLogRepository->findByEstateGroupedByHash($estate);
                $visits = 0;
                if($activityLogs){
                    $visits = count($activityLogs);
                }

                if($visits == 0 || $visits == 1 || $visits == 2 || $visits == 3){
                    $visits = rand(11, 17);
                }else{
                    $visits = $visits + 10;
                }

                $estate->setLatestVisits($visits);

                $_em->flush();
                echo 'SAVED'.$estate->getName().' '.$visits.PHP_EOL.PHP_EOL;
            }
        }else{
            echo 'No estates found'.PHP_EOL;
        }
    }
}