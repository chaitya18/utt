<?php

namespace UTT\EstateBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

use UTT\ReservationBundle\Service\PricingService;
use UTT\EstateBundle\Entity\Estate;

class EstateUpdateLowestHighestPricesCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('estate:updateLowestHighestPrices')
            ->setDescription('Update lowest and highest prices for all estates')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $_em = $this->getContainer()->get('doctrine')->getManager();
        $estates = $_em->getRepository('UTTEstateBundle:Estate')->getAllActive();
        if($estates){
            /** @var PricingService $pricingService */
            $pricingService = $this->getContainer()->get('utt.pricingservice');

            /** @var Estate $estate */
            foreach($estates as $estate){
                echo '--------------'.PHP_EOL;
                echo $estate->getName().PHP_EOL;

                $lowestPrice = $pricingService->findLowestPriceByPricingCategory($estate);
                if($lowestPrice != -1){
                    $estate->setLowestPrice($lowestPrice);
                    echo ' - updated lowest price: '.$lowestPrice.PHP_EOL;
                }

                $highestPrice = $pricingService->findHighestPrice($estate);
                if($highestPrice != -1){
                    $estate->setHighestPrice($highestPrice);
                    echo ' - updated highest price: '.$highestPrice.PHP_EOL;
                }

                $_em->flush();
                echo 'SAVED'.PHP_EOL.PHP_EOL;
            }
        }else{
            echo 'No estates found'.PHP_EOL;
        }
    }
}
