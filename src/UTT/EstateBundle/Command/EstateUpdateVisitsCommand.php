<?php

namespace UTT\EstateBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

use UTT\ReservationBundle\Service\PricingService;
use UTT\EstateBundle\Entity\Estate;
use UTT\EstateBundle\Entity\EstateRepository;
use UTT\UserBundle\Entity\ActivityLog;
use UTT\UserBundle\Entity\ActivityLogRepository;

use Doctrine\ORM\EntityManager;

class EstateUpdateVisitsCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('estate:updateVisits')
            ->setDescription('')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        /** @var EntityManager $_em */
        $_em = $this->getContainer()->get('doctrine')->getManager();
        /** @var EstateRepository $estateRepository */
        $estateRepository = $_em->getRepository('UTTEstateBundle:Estate');
        /** @var ActivityLogRepository $activityLogRepository */
        $activityLogRepository = $_em->getRepository('UTTUserBundle:ActivityLog');

        $estates = $estateRepository->getAllActive();
        if($estates){
            /** @var Estate $estate */
            foreach($estates as $estate){
                echo '--------------'.PHP_EOL;
                echo $estate->getName().PHP_EOL;

                $visits = $activityLogRepository->findNumberOfVisitsForEstate($estate);
                $estate->setVisits($visits);

                $_em->flush();
                echo 'SAVED'.PHP_EOL.PHP_EOL;
            }
        }else{
            echo 'No estates found'.PHP_EOL;
        }
    }
}