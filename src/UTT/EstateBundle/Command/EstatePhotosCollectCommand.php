<?php

namespace UTT\EstateBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

use UTT\EstateBundle\Entity\Photo;
use UTT\EstateBundle\Entity\Estate;
use UTT\EstateBundle\Entity\EstateRepository;
use Doctrine\ORM\EntityManager;

class EstatePhotosCollectCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('estate:photosCollect')
            ->setDescription('')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        /** @var EntityManager $_em */
        $_em = $this->getContainer()->get('doctrine')->getManager();
        /** @var EstateRepository $estateRepository */
        $estateRepository = $_em->getRepository('UTTEstateBundle:Estate');

        $estates = $estateRepository->getAllActive();
        if($estates){
            /** @var Estate $estate */
            foreach($estates as $estate){
                echo PHP_EOL.' === '.$estate->getName().' === '.PHP_EOL.PHP_EOL;

                $dir = 'web/photos/'.$estate->getShortName();
                if(!file_exists($dir)){ mkdir($dir); }
;
                if(count($estate->getPhotos()) > 0){
                    /** @var Photo $photo */
                    foreach($estate->getPhotos() as $photo){
                        $fn = 'web/'.$photo->getUploadDir().'/'.$photo->getFileName();
                        if(file_exists($fn)){
                            $fileName = $photo->getFileName();
                            $fileName = explode('/', $fileName);
                            $fileName = $fileName[1];

                            copy($fn, $dir.'/'.$fileName);

                            echo '    '.$fn.PHP_EOL;
                        }
                    }
                    echo ' ======================'.PHP_EOL;
                }
            }
        }
    }
}