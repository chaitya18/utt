<?php

namespace UTT\EstateBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

use UTT\EstateBundle\Entity\Estate;
use UTT\EstateBundle\Entity\EstateRepository;
use Doctrine\ORM\EntityManager;

class EstateUpdateLinksCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('estate:updateLinks')
            ->setDescription('Update link to open in new window for all estates')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        /** @var EntityManager $_em */
        $_em = $this->getContainer()->get('doctrine')->getManager();
        /** @var EstateRepository $estateRepository */
        $estateRepository = $_em->getRepository('UTTEstateBundle:Estate');

        $estates = $estateRepository->getAllActive();
        if($estates){
            /** @var Estate $estate */
            foreach($estates as $estate){
                echo '--------------'.PHP_EOL;
                echo $estate->getName().PHP_EOL;

                $textAccommodation = str_replace('target="_blank"', '', $estate->getTextAccommodation());
                $textInDetail = str_replace('target="_blank"', '', $estate->getTextInDetail());
                $textSustainability = str_replace('target="_blank"', '', $estate->getTextSustainability());
                $textWelshDescription = str_replace('target="_blank"', '', $estate->getTextWelshDescription());
                $textLocalArea = str_replace('target="_blank"', '', $estate->getTextLocalArea());
                $textTravelInfo = str_replace('target="_blank"', '', $estate->getTextTravelInfo());
                $textCustomerComments = str_replace('target="_blank"', '', $estate->getTextCustomerComments());
                $textArrivalInstructionsDirections = str_replace('target="_blank"', '', $estate->getTextArrivalInstructionsDirections());
                $textArrivalInstructionsPropertySpecific = str_replace('target="_blank"', '', $estate->getTextArrivalInstructionsPropertySpecific());

                $estate->setTextAccommodation(str_replace('<a ', '<a target="_blank"', $textAccommodation));
                $estate->setTextInDetail(str_replace('<a ', '<a target="_blank"', $textInDetail));
                $estate->setTextSustainability(str_replace('<a ', '<a target="_blank"', $textSustainability));
                $estate->setTextWelshDescription(str_replace('<a ', '<a target="_blank"', $textWelshDescription));
                $estate->setTextLocalArea(str_replace('<a ', '<a target="_blank"', $textLocalArea));
                $estate->setTextTravelInfo(str_replace('<a ', '<a target="_blank"', $textTravelInfo));
                $estate->setTextCustomerComments(str_replace('<a ', '<a target="_blank"', $textCustomerComments));
                $estate->setTextArrivalInstructionsDirections(str_replace('<a ', '<a target="_blank"', $textArrivalInstructionsDirections));
                $estate->setTextArrivalInstructionsPropertySpecific(str_replace('<a ', '<a target="_blank"', $textArrivalInstructionsPropertySpecific));

                $_em->flush();
                echo 'SAVED'.PHP_EOL.PHP_EOL;
            }
        }else{
            echo 'No estates found'.PHP_EOL;
        }
    }
}