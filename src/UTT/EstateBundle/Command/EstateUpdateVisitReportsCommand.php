<?php

namespace UTT\EstateBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

use UTT\EstateBundle\Entity\EstateVisitReport;
use UTT\ReservationBundle\Service\PricingService;
use UTT\EstateBundle\Entity\Estate;
use UTT\EstateBundle\Entity\EstateRepository;
use UTT\UserBundle\Entity\ActivityLog;
use UTT\UserBundle\Entity\ActivityLogRepository;

use Doctrine\ORM\EntityManager;
use UTT\EstateBundle\Entity\EstateVisitReportRepository;

class EstateUpdateVisitReportsCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('estate:updateVisitReports')
            ->setDescription('')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        /** @var EntityManager $_em */
        $_em = $this->getContainer()->get('doctrine')->getManager();
        /** @var EstateRepository $estateRepository */
        $estateRepository = $_em->getRepository('UTTEstateBundle:Estate');
        /** @var ActivityLogRepository $activityLogRepository */
        $activityLogRepository = $_em->getRepository('UTTUserBundle:ActivityLog');
        /** @var EstateVisitReportRepository $estateVisitReportRepository */
        $estateVisitReportRepository = $_em->getRepository('UTTEstateBundle:EstateVisitReport');

        $estates = $estateRepository->getAllActive();
        if($estates){
            $start = new \DateTime('now');
            $start->modify('-14 days');
            $end = new \DateTime('now');
            $interval = new \DateInterval('P1D');
            $period = new \DatePeriod($start, $interval, $end);

            /** @var \Datetime $periodDate */
            foreach($period as $periodDate) {
                /** @var Estate $estate */
                foreach($estates as $estate){
                    /** @var EstateVisitReport $estateVisitReport */
                    $estateVisitReport = $estateVisitReportRepository->findOneBy(array(
                        'estate' => $estate->getId(),
                        'reportDate' => $periodDate
                    ));
                    if(!($estateVisitReport instanceof EstateVisitReport)){
                        $visits = $activityLogRepository->findNumberOfVisitsForEstateDate($estate, $periodDate);

                        $estateVisitReport = new EstateVisitReport();
                        $estateVisitReport->setReportDate($periodDate);
                        $estateVisitReport->setVisits($visits);
                        $estateVisitReport->setEstate($estate);
                        $_em->persist($estateVisitReport);

                        $_em->flush();
                        echo 'SAVED: '.$estate->getName().' '.$periodDate->format('Y-m-d').PHP_EOL.PHP_EOL;
                    }else{
                        echo 'MISSED: '.$estate->getName().' '.$periodDate->format('Y-m-d').PHP_EOL.PHP_EOL;
                    }
                }
            }
        }else{
            echo 'No estates found'.PHP_EOL;
        }
    }
}