var estateSearchMapApp = angular.module('estateSearchMapApp', ['ngRoute', 'ui', 'leaflet-directive']);

estateSearchMapApp.controller('estateSearchMapAppCtrl', ['$scope', '$location', '$http', function($scope, $location, $http){
    $http.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';
    $http.defaults.headers.post["Content-Type"] = "application/x-www-form-urlencoded";

    var goSearch = function(){
        $http({ method: 'POST', url: $searchUrl, headers: {
                'Content-Type':'application/x-www-form-urlencoded; charset=UTF-8'
            }
        })
            .success(function(data, status, headers, config) {
                var dataOb = angular.fromJson(data);
                if(dataOb.success != undefined && dataOb.success == 1 && dataOb.result != undefined && dataOb.page != undefined){
                    if(dataOb.result.estates.length > 0){
                        $scope.mapMarkers = [];
                        angular.forEach(dataOb.result.estates, function(estate){
                            if(estate.lat != '-' && estate.lon != '-'){
                                $scope.mapMarkers.push({
                                    lat: parseFloat(estate.lat),
                                    lng: parseFloat(estate.lon),
                                    message: $scope.onMarkerClicked(estate),
                                    draggable: false,
                                    layer: 'realworld',
                                });
                            }
                        });
                    }
                }
            })
    };

    $scope.onMarkerClicked = function (estate) {
        return '<p style="min-width: 150px;"><div class="estate-info-window" style="display: flex;flex-direction: column;justify-content: center;text-align: center;margin-right: 12px"><img style="" src="'+estate.photoMain.fileName+'"/><a href="'+ Routing.generate('utt_estate_show_estate', { 'estateShortName': estate.shortName }) + '">' + estate.name + '</a></div></p>';
    };
    var estatemock = {
        photoMain: {
            fileName: 'sdadasd.jpg'
        },
        shortName: 'Testshort',
        name: 'test name'
    };

    $scope.mapMarkers = [];
    $scope.map = { center: { lat: 52.023491, lng: -5.060064 , zoom: 4}, defaults: {
            attributionControl: false,
            maxZoom: 12
        },
        layers: {
            baselayers: {
                osm: {
                    name: 'OpenStreetMap',
                    type: 'xyz',
                    url: 'http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png'
                }
            },
            overlays: {
                realworld: {
                    name: "Real world data",
                    type: "markercluster",
                    visible: true
                }
            }
        }
    };

    goSearch();
}]);

angular.bootstrap(document.getElementById("estateSearchMapAppHandler"),["estateSearchMapApp"]);