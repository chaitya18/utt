var estateSearchApp = angular.module('estateSearchApp', ['ngRoute', 'ui', 'google-maps'.ns()]);
estateSearchApp.config(['$locationProvider', function($locationProvider) {
    //$locationProvider.html5Mode(true);
    //$locationProvider.hashPrefix('!');
}]);

estateSearchApp.directive('slideit', function () {
    return function (scope, elm, attrs) {
        scope.$watch(attrs.slideit, function (estate) {
            if(estate.photosSorted != undefined && estate.photosSorted.length > 0){
                var html = '';
                for (var i = 0; i < estate.photosSorted.length; i++) {
                    if(i == 0){
                        html += '<li><img style="cursor: pointer;" onclick="window.location.href=\'' + Routing.generate('utt_estate_show_estate', { 'estateShortName': estate.shortName }, true) + '\'" title="' + estate.name + '" alt="' + estate.name + '" src="' + estate.photosSorted[i].fileName + '" alt="" /></li>';
                    }else{
                        var src = '/bundles/uttestate/images/icons/search-loader.gif';

                        html += '<li><img style="cursor: pointer;" onclick="window.location.href=\'' + Routing.generate('utt_estate_show_estate', { 'estateShortName': estate.shortName }, true) + '\'" class="lazy" title="' + estate.name + '" alt="' + estate.name + '" data-src="' + estate.photosSorted[i].fileName + '" src="' + src + '" alt="" /></li>';
                    }
                }
                $("#" + $(elm[0]).attr('id')).html(html).bxSlider({
                    onSlideBefore: function($slideElement, oldIndex, newIndex){
                        var $lazy = $slideElement.find(".lazy");
                        var $load = $lazy.attr("data-src");
                        $lazy.attr("src",$load).removeClass("lazy");
                    },
                    mode: "fade",
                    auto: false,
                    pause: 7000,
                    speed: 100,
                    slideBorderWidth: 4,
                    slideBorderHeight: 0,
                    //slideWidth: $(this).parent().width(),
                    minSlides: 1,
                    maxSlides: 1,
                    startSlide: 0,
                    slideMargin: 5,
                    tickerHover: true,
                    pager: false,
                    captions: true

                    //adaptiveHeight: true,
                });
            }else{
                $("#" + $(elm[0]).attr('id')).html('<li><img style="cursor: pointer;" onclick="window.location.href=\'' + Routing.generate('utt_estate_show_estate', { 'estateShortName': estate.shortName }, true) + '\'" title="' + estate.name + '" alt="' + estate.name + '" src="' + scope.searchResultNoImageThumb + '" alt="" /></li>');
            }
        });
    };
});

estateSearchApp.factory('formFilterService', function() {
    /**** Load parameters from Twig template ****/
    var allLocations = angular.fromJson($locationsJson);
    var allCategories = angular.fromJson($categoriesJson);
    var allSleepsChoices = angular.fromJson($allSleepsChoicesJson);
    var allEstateFeatures = angular.fromJson($allEstateFeaturesJson);
    var defaultPriceFrom = 0;
    var defaultPriceTo = 5000;

    /*
    var currentDate = new Date();
    var currentDateDate = currentDate.getDate();
    var currentDateMonth = currentDate.getMonth() + 1; //Months are zero based
    var currentDateYear = currentDate.getFullYear();
    */
    var defaultDateFrom = null;//currentDateYear+'-'+currentDateMonth+'-'+currentDateDate;
    var defaultDateTo = null;//currentDateYear+'-'+currentDateMonth+'-'+currentDateDate;

    var paramEstateName = '';
    var paramLocation = {};
    var paramCategory = {};
    var paramSleeps = {};
    var paramPets = false;
    var paramChildren = false;
    var paramInfants = false;
    var paramPriceFrom = defaultPriceFrom;
    var paramPriceTo = defaultPriceTo;
    var paramDateFrom = defaultDateFrom;
    var paramDateTo = defaultDateTo;

    /***** END - Load parameters from Twig template *****/

    var activateEstateFeature = function(estateFeatureId){
        angular.forEach(allEstateFeatures, function(estateFeature){
            if(estateFeature.id == estateFeatureId){
                var index = allEstateFeatures.indexOf(estateFeature);
                if(allEstateFeatures[index].isActive == false){
                    allEstateFeatures[index].isActive = true;
                }else{
                    allEstateFeatures[index].isActive = false;
                }
            }
        });
    };

    var clearActiveEstateFeatures = function(){
        angular.forEach(allEstateFeatures, function(estateFeature){
            estateFeature.isActive = false;
        });
    };

    var removeByFilterName = function(filterName){
        if(filterName == 'estateName'){
            paramEstateName = '';
        }else if(filterName == 'location'){
            paramLocation = {};
        }else if(filterName == 'category'){
            paramCategory = {};
        }else if(filterName == 'sleeps'){
            paramSleeps = {};
        }else if(filterName == 'pets'){
            paramPets = false;
        }else if(filterName == 'children'){
            paramChildren = false;
        }else if(filterName == 'infants'){
            paramInfants = false;
        }else if(filterName == 'priceFrom'){
            paramPriceFrom = defaultPriceFrom;
        }else if(filterName == 'priceTo'){
            paramPriceTo = defaultPriceTo;
        }else if(filterName == 'dateFrom'){
            paramDateFrom = defaultDateFrom;
        }else if(filterName == 'dateTo'){
            paramDateTo = defaultDateTo;
        }
    };
    var remove = function(filter){
        removeByFilterName(filter.name);
    };
    return {
        setParamEstateName: function(newParamEstateName){ paramEstateName = newParamEstateName; },
        setParamLocation: function(newParamLocation){ paramLocation = newParamLocation; },
        setParamCategory: function(newParamCategory){ paramCategory = newParamCategory; },
        setParamSleeps: function(newParamSleeps){ paramSleeps = newParamSleeps; },
        setParamPets: function(newParamPets){ paramPets = newParamPets; },
        setParamChildren: function(newParamChildren){ paramChildren = newParamChildren; },
        setParamInfants: function(newParamInfants){ paramInfants = newParamInfants; },
        setParamPriceFrom: function(newParamPriceFrom){ paramPriceFrom = newParamPriceFrom; },
        setParamPriceTo: function(newParamPriceTo){ paramPriceTo = newParamPriceTo; },
        setParamDateFrom: function(newParamDateFrom){ paramDateFrom = newParamDateFrom; },
        setParamDateTo: function(newParamDateTo){ paramDateTo = newParamDateTo; },
        getActiveEstateFeatures: function(){
            var newEstateFeatures = [];
            angular.forEach(allEstateFeatures, function(estateFeature){
                if(estateFeature.isActive == true){
                    newEstateFeatures.push(estateFeature);
                }
            });
            if(newEstateFeatures.length > 0){
                return newEstateFeatures;
            }else{
                return false;
            }
        },
        activateEstateFeature: activateEstateFeature,
        clearActiveEstateFeatures: clearActiveEstateFeatures,
        applyToScope: function($scope){
            $scope.allLocations = allLocations;
            $scope.allCategories = allCategories;
            $scope.allSleepsChoices = allSleepsChoices;
            $scope.allEstateFeatures = allEstateFeatures;
            $scope.defaultPriceFrom = defaultPriceFrom;
            $scope.defaultPriceTo = defaultPriceTo;
            $scope.defaultDateFrom = defaultDateFrom;
            $scope.defaultDateTo = defaultDateTo;

            $scope.paramEstateName = paramEstateName;
            $scope.paramLocation = paramLocation;
            $scope.paramCategory = paramCategory;
            $scope.paramSleeps = paramSleeps;
            $scope.paramPets = paramPets;
            $scope.paramChildren = paramChildren;
            $scope.paramInfants = paramInfants;
            $scope.paramPriceFrom = paramPriceFrom;
            $scope.paramPriceTo = paramPriceTo;
            $scope.paramDateFrom = paramDateFrom;
            $scope.paramDateTo = paramDateTo;

            return $scope;
        },
        setFilterParam: function(filterName, param){
            if(filterName == 'estateName'){
                paramEstateName = param;
            }else if(filterName == 'location'){
                paramLocation = param;
            }else if(filterName == 'category'){
                paramCategory = param;
            }else if(filterName == 'sleeps'){
                paramSleeps = param
            }else if(filterName == 'pets'){
                paramPets = param;
            }else if(filterName == 'children'){
                paramChildren = param
            }else if(filterName == 'infants'){
                paramInfants = param
            }else if(filterName == 'features[]'){
                activateEstateFeature(param.id);
            }else if(filterName == 'priceFrom'){
                paramPriceFrom = param;
            }else if(filterName == 'priceTo'){
                paramPriceTo = param;
            }else if(filterName == 'dateFrom'){
                paramDateFrom = param;
            }else if(filterName == 'dateTo'){
                paramDateTo = param;
            }
        },
        clearAll: function(){
            paramEstateName = '';
            paramLocation = {};
            paramCategory = {};
            paramSleeps = {};
            paramPets = false;
            paramChildren = false;
            paramInfants = false;
            paramPriceFrom = defaultPriceFrom;
            paramPriceTo = defaultPriceTo;
            paramDateFrom = defaultDateFrom;
            paramDateTo = defaultDateTo;
            clearActiveEstateFeatures();
        },
        remove: remove,
        removeByFilterName: removeByFilterName,
        getFilterParam: function(filterName, filterId){
            var param = {};
            if(filterName == 'location'){
                angular.forEach(allLocations, function(location){
                    if(location.id == filterId){
                        param = location;
                    }
                });
            }else if(filterName == 'category'){
                angular.forEach(allCategories, function(category){
                    if(category.id == filterId){
                        param = category;
                    }
                });
            }else if(filterName == 'sleeps'){
                angular.forEach(allSleepsChoices, function(sleepsChoice){
                    if(sleepsChoice.id == filterId){
                        param = sleepsChoice;
                    }
                });
            }else if(filterName == 'features[]'){
                angular.forEach(allEstateFeatures, function(estateFeature){
                    if(estateFeature.id == filterId){
                        param = estateFeature;
                    }
                });
            }

            return param;
        }
    };
});

estateSearchApp.factory('activeFilterService', ['$location', function($location) {
    var activeFilters = [];

    var remove = function(filter){
        var index = activeFilters.indexOf(filter);
        if (index != -1) {
            activeFilters.splice(index, 1);
        }
    };

    return {
        applyToScope: function($scope){
            $scope.activeFilters = activeFilters;

            return $scope;
        },
        clearAll: function(){
            activeFilters = [];
        },
        remove: remove,
        removeByFilterName: function(filterName){
            angular.forEach(activeFilters, function(activeFilter){
                if(activeFilter.name == filterName){
                    remove(activeFilter);
                }
            });
        },
        add: function(filterName, filterValue){
            var newFilter = new Object();
            newFilter.name = filterName;
            newFilter.value = filterValue;

            var filterExists = false;
            angular.forEach(activeFilters, function(activeFilter){
                if(activeFilter.name == newFilter.name){
                    filterExists = true;

                    var index = activeFilters.indexOf(activeFilter);
                    activeFilters[index] = newFilter;
                }
            });

            if(!filterExists){
                activeFilters.push(newFilter);
            }
        }
    };
}]);

estateSearchApp.controller('estateSearchAppCtrl', ['$scope', '$location', 'formFilterService', 'activeFilterService', '$http', '$sce', '$timeout', function($scope, $location, formFilterService, activeFilterService, $http, $sce, $timeout){
    $http.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';
    $http.defaults.headers.post["Content-Type"] = "application/x-www-form-urlencoded";

    $scope.paramPrice = null;

    $scope = formFilterService.applyToScope($scope);
    $scope = activeFilterService.applyToScope($scope);

    $scope.goToEstate = function(estate){
        window.location.href = Routing.generate('utt_estate_show_estate', {
            'estateShortName': estate.shortName
        }, true);
    };

    $scope.generateEstateImageSrc = function(estate){
        if(estate.photosSorted != undefined && estate.photosSorted.length > 0){
            return estate.photosSorted[0].fileName;
        }else{
            return $scope.searchResultNoImageThumb;
        }
    };

    $scope.searchResultEstates = [];
    $scope.currentPage = 1;
    $scope.loadMoreIsVisible = false;
    $scope.searchResultEstatesLength = function(){
        return $scope.searchResultEstates.length;
    };
    $scope.searchResultNoImageThumb = '';
    $scope.searchResultLoaderShow = false;

    var goSearch = function(args){
        $scope.isSearchInProgress = true;

        $scope.searchResultLoaderShow = true;

        var httpData = {};
        if($scope.paramEstateName != undefined && $scope.paramEstateName){ httpData.estateName = $scope.paramEstateName; }
        if($scope.paramLocation != undefined && $scope.paramLocation.id){ httpData.location = $scope.paramLocation.id; }
        if($scope.paramCategory != undefined && $scope.paramCategory.id){ httpData.category = $scope.paramCategory.id; }
        if($scope.paramSleeps != undefined && $scope.paramSleeps.id){ httpData.sleeps = $scope.paramSleeps.id; }
        if($scope.paramPets != undefined && $scope.paramPets == true){ httpData.pets = 1; }
        if($scope.paramChildren != undefined && $scope.paramChildren == true){ httpData.children = 1; }
        if($scope.paramInfants != undefined && $scope.paramInfants == true){ httpData.infants = 1; }

        var activeEstateFeatures = formFilterService.getActiveEstateFeatures();
        if(activeEstateFeatures){
            var activeEstateFeatureIds = [];
            angular.forEach(activeEstateFeatures, function(activeEstateFeature){
                activeEstateFeatureIds.push(activeEstateFeature.id);
            });
            httpData.features = activeEstateFeatureIds;
        }

        if($scope.paramPriceFrom != undefined && $scope.paramPriceFrom && $scope.paramPriceFrom != $scope.defaultPriceFrom){ httpData.priceFrom = $scope.paramPriceFrom; }
        if($scope.paramPriceTo != undefined && $scope.paramPriceTo && $scope.paramPriceTo != $scope.defaultPriceTo){ httpData.priceTo = $scope.paramPriceTo; }

        if($scope.paramDateFrom != undefined && $scope.paramDateFrom){ httpData.dateFrom = $scope.paramDateFrom; }
        if($scope.paramDateTo != undefined && $scope.paramDateTo){ httpData.dateTo = $scope.paramDateTo; }
        if(args != undefined && args.page != undefined){
            httpData.page = args.page;
        }else{
            httpData.page = 1;
        }

        $http({ method: 'POST', url: Routing.generate('utt_estate_search'), data: $.param(httpData), headers: {
                    'Content-Type':'application/x-www-form-urlencoded; charset=UTF-8'
                }
            })
            .success(function(data, status, headers, config) {
                var dataOb = angular.fromJson(data);
                if(dataOb.success != undefined && dataOb.success == 1 && dataOb.result != undefined && dataOb.page != undefined){
                    if(dataOb.result.estates.length > 0){
                        if(dataOb.page == 1){
                            $scope.searchResultEstates = dataOb.result.estates;
                        }else{
                            $scope.searchResultEstates = $scope.searchResultEstates.concat(dataOb.result.estates);
                        }
                        $scope.searchResultNoImageThumb = dataOb.result.noImageThumb;
                        $scope.currentPage = dataOb.page;
                        $scope.loadMoreIsVisible = true;

                        $scope.mapMarkers = [];
                        angular.forEach($scope.searchResultEstates, function(estate){
                            if(estate.lat != '-' && estate.lon != '-'){
                                $scope.mapMarkers.push({
                                    id: estate.id,
                                    shortName: estate.shortName,
                                    latitude: parseFloat(estate.lat),
                                    longitude: parseFloat(estate.lon),
                                    options: {
                                        'title': estate.name
                                    }
                                });
                            }
                        });

                        _.each($scope.mapMarkers, function (marker) {
                            marker.onClicked = function () {
                                $scope.onMarkerClicked(marker);
                            };
                            marker.closeClick = function () {
                                marker.showWindow = false;
                            };
                        });
                    }else{
                        if(dataOb.page == 1){
                            $scope.searchResultEstates = [];
                        }else{

                        }
                        $scope.loadMoreIsVisible = false;
                    }
                }else{
                    $scope.searchResultEstates = [];
                    $scope.searchResultNoImageThumb = '';
                    $scope.currentPage = 1;
                    $scope.loadMoreIsVisible = false;
                }

                $scope.searchResultLoaderShow = false;

                $scope.isSearchInProgress = false;
            })
            .error(function(data, status, headers, config) {
                $scope.searchResultEstates = [];
                $scope.searchResultNoImageThumb = '';
                $scope.currentPage = 1;
                $scope.loadMoreIsVisible = false;

                $scope.searchResultLoaderShow = false;

                $scope.isSearchInProgress = false;
            });
    };

    $scope.map = { center: { latitude: 51.505, longitude: -0.09 }, zoom: 5 };
    $scope.mapMarkers = [];
    $scope.onMarkerClicked = function (marker) {
        if(marker.showWindow == true){
            marker.showWindow = false;
            if(marker.shortName != undefined){
                window.location.href = Routing.generate('utt_estate_show_estate', { 'estateShortName': marker.shortName });
            }
        }else{
            marker.showWindow = true;
        }
    };

    $scope.loadMore = function(){
        $scope.currentPage ++;
        goSearch({
            page: $scope.currentPage
        });
    };

    $scope.activateEstateFeature = function(estateFeature){
        formFilterService.activateEstateFeature(estateFeature.id);
        var activeEstateFeatures = formFilterService.getActiveEstateFeatures();
        if(activeEstateFeatures){
            var activeEstateFeatureIds = [];
            angular.forEach(activeEstateFeatures, function(activeEstateFeature){
                activeEstateFeatureIds.push(activeEstateFeature.id);
            });
            $location.search('features[]', activeEstateFeatureIds);
        }else{
            $location.search('features[]', null);
        }

        $scope = formFilterService.applyToScope($scope);

        goSearch();
    };

    $scope.clearAllFilters = function(){
        // nothing more is necessary because $scope.$watch is running and makes it work
        $location.search({});
    };

    $scope.clearActiveFilter = function(filter){
        // remove from url
        $location.search(filter.name, null);

        // remove from form
        formFilterService.remove(filter);
        // remove from active filters
        activeFilterService.remove(filter);

        // sync objects to controller $scope
        $scope = formFilterService.applyToScope($scope);
        $scope = activeFilterService.applyToScope($scope);

        $scope.$emit('updateSliderValues');

        goSearch();
    };

    $scope.filterPrice = function(){
        if($scope.paramPriceFrom != undefined && $scope.paramPriceTo != undefined){
            formFilterService.setParamPriceFrom($scope.paramPriceFrom);
            formFilterService.setParamPriceTo($scope.paramPriceTo);
            $location.search('priceFrom', $scope.paramPriceFrom);
            $location.search('priceTo', $scope.paramPriceTo);
            activeFilterService.add('priceFrom', 'price from £' + $scope.paramPriceFrom);
            activeFilterService.add('priceTo', 'price to £' + $scope.paramPriceTo);
        }else{
            formFilterService.removeByFilterName('priceFrom');
            formFilterService.removeByFilterName('priceTo');
            $location.search('priceFrom', null);
            $location.search('priceTo', null);
            activeFilterService.removeByFilterName('priceFrom');
            activeFilterService.removeByFilterName('priceTo');
        }
        $scope = formFilterService.applyToScope($scope);
        $scope = activeFilterService.applyToScope($scope);

        goSearch();
    };

    $scope.filterDates = function(){
        if($scope.paramDateFrom != undefined){
            formFilterService.setParamDateFrom($scope.paramDateFrom);
            $location.search('dateFrom', $scope.paramDateFrom);
            activeFilterService.add('dateFrom', 'Date from ' + $scope.paramDateFrom);
        }else{
            formFilterService.removeByFilterName('dateFrom');
            $location.search('dateFrom', null);
            activeFilterService.removeByFilterName('dateFrom');
        }

        if($scope.paramDateTo != undefined){
            formFilterService.setParamDateTo($scope.paramDateTo);
            $location.search('dateTo', $scope.paramDateTo);
            activeFilterService.add('dateTo', 'Date to ' + $scope.paramDateTo);
        }else{
            formFilterService.removeByFilterName('dateTo');
            $location.search('dateTo', null);
            activeFilterService.removeByFilterName('dateTo');
        }

        $scope = formFilterService.applyToScope($scope);
        $scope = activeFilterService.applyToScope($scope);

        goSearch();
    };


    $scope.filterEstateName = function(){
        if($scope.paramEstateName != undefined && $scope.paramEstateName != ''){
            formFilterService.setParamEstateName($scope.paramEstateName);
            $location.search('estateName', $scope.paramEstateName);
            activeFilterService.add('estateName', $scope.paramEstateName);
        }else{
            formFilterService.removeByFilterName('estateName');
            $location.search('estateName', null);
            activeFilterService.removeByFilterName('estateName');
        }
        $scope = activeFilterService.applyToScope($scope);

        goSearch();
    };

    $scope.filterLocation = function(){
        if($scope.paramLocation != undefined){
            formFilterService.setParamLocation($scope.paramLocation);
            $location.search('location', $scope.paramLocation.id);
            activeFilterService.add('location', $scope.paramLocation.name);
        }else{
            formFilterService.removeByFilterName('location');
            $location.search('location', null);
            activeFilterService.removeByFilterName('location');
        }
        $scope = activeFilterService.applyToScope($scope);

        goSearch();
    };

    $scope.filterCategory = function(){
        if($scope.paramCategory != undefined){
            formFilterService.setParamCategory($scope.paramCategory);
            $location.search('category', $scope.paramCategory.id);
            activeFilterService.add('category', $scope.paramCategory.name);
        }else{
            formFilterService.removeByFilterName('category');
            $location.search('category', null);
            activeFilterService.removeByFilterName('category');
        }
        $scope = activeFilterService.applyToScope($scope);

        goSearch();
    };

    $scope.filterSleeps = function(){
        if($scope.paramSleeps != undefined){
            formFilterService.setParamSleeps($scope.paramSleeps);
            $location.search('sleeps', $scope.paramSleeps.id);
            activeFilterService.add('sleeps', $scope.paramSleeps.name);
        }else{
            formFilterService.removeByFilterName('sleeps');
            $location.search('sleeps', null);
            activeFilterService.removeByFilterName('sleeps');
        }
        $scope = activeFilterService.applyToScope($scope);

        goSearch();
    };

    $scope.filterPets = function(){
        if($scope.paramPets != undefined && $scope.paramPets == true){
            formFilterService.setParamPets($scope.paramPets);
            $location.search('pets', 1);
            activeFilterService.add('pets', 'pets: yes');
        }else{
            formFilterService.removeByFilterName('pets');
            $location.search('pets', null);
            activeFilterService.removeByFilterName('pets');
        }
        $scope = activeFilterService.applyToScope($scope);

        goSearch();
    };

    $scope.filterChildren = function(){
        if($scope.paramChildren != undefined && $scope.paramChildren == true){
            formFilterService.setParamChildren($scope.paramChildren);
            $location.search('children', 1);
            activeFilterService.add('children', 'children: yes');
        }else{
            formFilterService.removeByFilterName('children');
            $location.search('children', null);
            activeFilterService.removeByFilterName('children');
        }
        $scope = activeFilterService.applyToScope($scope);

        goSearch();
    };

    $scope.filterInfants = function(){
        if($scope.paramInfants != undefined && $scope.paramInfants == true){
            formFilterService.setParamInfants($scope.paramInfants);
            $location.search('infants', 1);
            activeFilterService.add('infants', 'infants: yes');
        }else{
            formFilterService.removeByFilterName('infants');
            $location.search('infants', null);
            activeFilterService.removeByFilterName('infants');
        }
        $scope = activeFilterService.applyToScope($scope);

        goSearch();
    };

    var syncFiltersByUrl = function(){
        formFilterService.clearAll();
        activeFilterService.clearAll();

        var urlParams = $location.search();
        if(urlParams.sleeps == undefined){
            $location.search('sleeps', formFilterService.getFilterParam('sleeps', 2).id);
        }
        for(i in urlParams){
            if(i == 'features[]'){
                angular.forEach(urlParams[i], function(featureId){
                    var param = formFilterService.getFilterParam(i, featureId);
                    if(param.id != undefined){
                        formFilterService.setFilterParam(i, param);
                    }
                });
            }else if(i == 'priceFrom'){
                formFilterService.setFilterParam(i, urlParams[i]);
                activeFilterService.add(i, 'price from £' + urlParams[i]);
            }else if(i == 'priceTo'){
                formFilterService.setFilterParam(i, urlParams[i]);
                activeFilterService.add(i, 'price to £' + urlParams[i]);
            }else if(i == 'dateFrom'){
                formFilterService.setFilterParam(i, urlParams[i]);
                activeFilterService.add(i, 'date from ' + urlParams[i]);
            }else if(i == 'dateTo'){
                formFilterService.setFilterParam(i, urlParams[i]);
                activeFilterService.add(i, 'date to ' + urlParams[i]);
            }else if(i == 'estateName'){
                formFilterService.setFilterParam(i, urlParams[i].replace('+', ' '));
                activeFilterService.add(i, urlParams[i].replace('+', ' '));
            }else if(i == 'pets'){
                formFilterService.setFilterParam(i, true);
                activeFilterService.add(i, 'pets: yes');
            }else if(i == 'children'){
                formFilterService.setFilterParam(i, true);
                activeFilterService.add(i, 'children: yes');
            }else if(i == 'infants'){
                formFilterService.setFilterParam(i, true);
                activeFilterService.add(i, 'infants: yes');
            }else{
                var param = formFilterService.getFilterParam(i, urlParams[i]);
                if(param.id != undefined){
                    formFilterService.setFilterParam(i, param);
                    activeFilterService.add(i, param.name);
                }
            }
        }
        $scope = activeFilterService.applyToScope($scope);
        $scope = formFilterService.applyToScope($scope);

        $scope.$emit('updateSliderValues');

        goSearch();
    };

    $scope.isSearchInProgress = false;
    $scope.goSearchFromScroll = function(){
        if(!$scope.isSearchInProgress && $scope.loadMoreIsVisible){
            $scope.loadMore();
        }
    };

    $scope.$watch(function(){ return $location.search() }, function(){
        syncFiltersByUrl();
    });
}]);

estateSearchApp.directive('datepicker', function() {
    return {
        restrict: 'A',
        require : 'ngModel',
        link : function (scope, element, attrs, ngModelCtrl) {
            $(function(){
                element.datepicker({
                    dateFormat: 'dd-M-yy',
                    minDate: $.datepicker.formatDate('dd-M-yy', new Date()),
                    beforeShow: function(){
                        if(attrs.ngModel == 'paramDateFrom'){
                            if(scope.paramDateTo){
                                element.datepicker( "option", "maxDate", scope.paramDateTo );
                            }else{
                                element.datepicker( "option", "maxDate", null );
                            }
                        }

                        if(attrs.ngModel == 'paramDateTo'){
                            if(scope.paramDateFrom){
                                element.datepicker( "option", "minDate", scope.paramDateFrom );
                            }else{
                                element.datepicker( "option", "minDate", $.datepicker.formatDate('dd-M-yy', new Date()) );
                            }
                        }
                    },
                    onSelect: function (date) {
                        ngModelCtrl.$setViewValue(date);
                        scope.$apply();
                    }
                });
            });
        }
    }
});

estateSearchApp.directive('slider', function() {
    return {
        restrict: 'A',
        require : 'ngModel',
        link : function (scope, element, attrs, ngModelCtrl) {
            element.slider({
                range: true,
                min: scope.defaultPriceFrom,
                max: scope.defaultPriceTo,
                values: [scope.paramPriceFrom, scope.paramPriceTo],
                start: function(event, ui){
                    scope.paramPriceFrom = ui.values[0];
                    scope.paramPriceTo = ui.values[1];
                    ngModelCtrl.$setViewValue(ui.values);
                    scope.$apply();
                },
                slide: function(event, ui){
                    scope.paramPriceFrom = ui.values[0];
                    scope.paramPriceTo = ui.values[1];
                    ngModelCtrl.$setViewValue(ui.values);
                    scope.$apply();
                }
            });
            scope.$on('updateSliderValues', function() {
                element.slider("option", "values", [scope.paramPriceFrom, scope.paramPriceTo]);
            });
        }
    }
});

estateSearchApp.directive("scroll", function ($window) {
    return function(scope, element, attrs) {
        angular.element($window).bind("scroll", function() {
            if(this.pageYOffset + element[0].offsetTop >= element[0].offsetHeight){
                scope.goSearchFromScroll();
            }
        });
    };
});
angular.bootstrap(document.getElementById("estateSearchAppHandler"),["estateSearchApp"]);