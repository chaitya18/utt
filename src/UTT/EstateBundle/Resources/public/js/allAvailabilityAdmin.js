var apiService = {
    getEstateCalendar: function(estateShortName, nights, callback){
        $.ajax({
            type: 'POST',
            url: Routing.generate('utt_estate_get_calendar_days', {
                'estateShortName': estateShortName,
                'nights': nights
            })
        })
            .success(function(data) {
                if(data){
                    callback(data);
                }
            })
            .error(function() {
                callback(false);
            });
    },
    refreshEstateCalendar: function(estateShortName, callback){
        $.ajax({
            type: 'POST',
            url: Routing.generate('utt_estate_refresh_calendar', {
                'estateShortName': estateShortName
            })
        })
            .success(function(data) {
                var dataOb = angular.fromJson(data);
                if(!(typeof dataOb.success === typeof undefined) && dataOb.success == true){
                    callback(true);
                }else{
                    callback(false);
                }
            })
            .error(function() {
                callback(false);
            });
    },
    bookBlocked: function(params, callback){
        $.ajax({
            type: 'POST',
            url: Routing.generate('utt_reservation_book_blocked'),
            data: $.param(params)
        })
            .success(function(data) {
                var dataOb = angular.fromJson(data);
                if(!(typeof dataOb.success === typeof undefined) && dataOb.success == true){
                    callback(dataOb.reservation);
                }else{
                    callback(false);
                }
            })
            .error(function() {
                callback(false);
            });
    },
    telephoneBooking: function(params, callback){
        $.ajax({
            type: "POST",
            url: Routing.generate('utt_reservation_telephone_booking'),
            data: $.param(params)
        })
            .success(function(data) {
                var dataOb = angular.fromJson(data);
                if(!(typeof dataOb.success === typeof undefined) && dataOb.success == true){
                    callback(dataOb.reservation);
                }else{
                    callback(false);
                }
            })
            .error(function() {
                callback(false);
            });
    }
};

$(function () {
    $('.pricingCalendar').on('click', '.day', function(){
        var self = $(this);

        var dataPrice = self.attr('data-price').split('|');
        if(dataPrice[0] == true){
            var prev = self;
            if(dataPrice[1] >= 1){
                for (var i = 1; i <= dataPrice[1]; i++) {
                    prev = prev.prev();
                    if(prev.hasClass('tooltip')){
                        prev = prev.prev();
                    }
                }
            }
            var next = self;
            if(dataPrice[2] >= 1){
                for (var i = 1; i <= dataPrice[2]; i++) {
                    next = next.next();
                    if(next.hasClass('tooltip')){
                        next = next.next();
                    }
                }
            }

            if(dataPrice[3]){
                var price = dataPrice[3].replace('£', '');
                $('#makeBookingModal .formField_fromDate').val(prev.attr('data-date'));
                $('#makeBookingModal .formField_toDate').val(next.attr('data-date'));
                $('#makeBookingModal .formField_price').val(price);
                $('#makeBookingModal .formField_estateShortName').val(self.parent().attr('data-estate-short-name'));
                $('#makeBookingModal .formField_sleeps').val(self.parent().attr('data-estate-minimum-guests'));
                $('#makeBookingModal').modal();

                $('#modal_telephone_booking_form button.submit-form').attr("disabled", false);
                $('#modal_owner_booking_form button.submit-form').attr("disabled", false);
            }
        }
    });

    $('#modal_telephone_booking_form button.submit-form').click(function(){
        $('#modal_telephone_booking_form button.submit-form').attr("disabled", true);

        var params = {};
        $('#modal_telephone_booking_form').serializeArray().map(function(item) {
            params[item.name] = item.value;
        });

        if(params.guestFirstName && params.guestLastName && params.guestEmail){
            apiService.telephoneBooking(params, function(result){
                if(result){
                    $('#makeBookingModal').modal('hide');

                    var monthElement = $('#month_' + params.estateShortName);
                    monthElement.parent().find('.month-loader').show();
                    apiService.refreshEstateCalendar(params.estateShortName, function(calendarResult){
                        if(calendarResult){
                            var nights = monthElement.attr('data-nights');
                            if(!nights){ nights = 2; }
                            apiService.getEstateCalendar(params.estateShortName, nights, function(daysResult){
                                monthElement.html(daysResult);
                                monthElement.parent().find('.month-loader').hide();
                            });
                        }
                    });
                }else{
                    alert('Error occurred. Try again')
                }

                $('#modal_telephone_booking_form button.submit-form').attr("disabled", false);
            });
        }else{
            alert('All fields are required. Make sure that user data are correct.');
            $('#modal_telephone_booking_form button.submit-form').attr("disabled", false);
        }
    });
    $('#modal_owner_booking_form button.submit-form').click(function(){
        $('#modal_owner_booking_form button.submit-form').attr("disabled", true);

        var params = {};
        $('#modal_owner_booking_form').serializeArray().map(function(item) {
            params[item.name] = item.value;
        });

        apiService.bookBlocked(params, function(result){
            if(result){
                $('#makeBookingModal').modal('hide');

                var monthElement = $('#month_' + params.estateShortName);
                monthElement.parent().find('.month-loader').show();
                apiService.refreshEstateCalendar(params.estateShortName, function(calendarResult){
                    if(calendarResult){
                        var nights = monthElement.attr('data-nights');
                        if(!nights){ nights = 2; }
                        apiService.getEstateCalendar(params.estateShortName, nights, function(daysResult){
                            monthElement.html(daysResult);
                            monthElement.parent().find('.month-loader').hide();
                        });
                    }
                });
            }else{
                alert('Error occurred. Try again')
            }

            $('#modal_owner_booking_form button.submit-form').attr("disabled", false);
        });
    });
});