$(function () {
    $('.pricingCalendar').on('click', '.day', function(){
        var self = $(this);

        var dataPrice = self.attr('data-price').split('|');
        if(dataPrice[0] == true){
            var prev = self;
            if(dataPrice[1] >= 1){
                for (var i = 1; i <= dataPrice[1]; i++) {
                    prev = prev.prev();
                    if(prev.hasClass('tooltip')){
                        prev = prev.prev();
                    }
                }
            }
            var next = self;
            if(dataPrice[2] >= 1){
                for (var i = 1; i <= dataPrice[2]; i++) {
                    next = next.next();
                    if(next.hasClass('tooltip')){
                        next = next.next();
                    }
                }
            }

            window.location.href = Routing.generate('utt_estate_show_estate', {
                'estateShortName': self.parent().attr('data-estate-short-name')
            }, true)
        }
    });
});