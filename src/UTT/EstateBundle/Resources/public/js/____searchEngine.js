var searchEngine = {
    activeFilters: {
        init: function(){
            $('#active-filters-handler .conteiner').on('click', '.clear-all-filters', function(){
                // TODO
            });
            $('#active-filters-handler .conteiner').on('click', '.close-filter', function(){
                var self = $(this);

                alert(self.attr('data-filter-name'));
            });
        },
        addFilter: function(formParam){
            if(formParam.name == 'location'){
                var formParamValue = $('#search-holiday-form select[name="location"] option[value="' + formParam.value + '"]').html();
                if(formParamValue){
                    searchEngine.activeFilters.add(formParam.name, formParamValue);
                }
            }else if(formParam.name == 'sleeps'){
                var formParamValue = $('#search-holiday-form select[name="sleeps"] option[value="' + formParam.value + '"]').html();
                if(formParamValue){
                    searchEngine.activeFilters.add(formParam.name, formParamValue);
                }
            }else if(formParam.name == 'pets'){
                var formParamValue = $('#search-holiday-form select[name="pets"] option[value="' + formParam.value + '"]').html();
                if(formParamValue){
                    searchEngine.activeFilters.add(formParam.name, formParamValue);
                }
            }else if(formParam.name == 'children'){
                var formParamValue = $('#search-holiday-form select[name="children"] option[value="' + formParam.value + '"]').html();
                if(formParamValue){
                    searchEngine.activeFilters.add(formParam.name, formParamValue);
                }
            }else{
                searchEngine.activeFilters.add(formParam.name, formParam.value);
            }
        },
        add: function(filterName, filterValue){
            var filterNames = new Array();
            filterNames['location'] = 'Location';
            filterNames['checkIn'] = 'Check In';
            filterNames['checkOut'] = 'Check Out';
            filterNames['sleeps'] = 'Sleeps';
            filterNames['pets'] = 'Pets';
            filterNames['children'] = 'Children';
            filterNames['priceFrom'] = 'Price from';
            filterNames['priceTo'] = 'Price to';

            if(filterNames[filterName] != undefined){
                filterName = filterNames[filterName];
            }

            $('#active-filters-handler .conteiner').append('<div class="active-filter"><div class="text">' + filterName + ': ' + filterValue + '</div><div class="close-filter" data-filter-name="' + filterName + '">&nbsp</div></div>');
        }
    },
    router: {
        init: function(url){
            var params = $.deparam(get_querystring(url));
            $('#active-filters-handler .conteiner .active-filter').remove();
            if(params){
                for(i in params){
                    var newParamsItem = new Object();
                    newParamsItem.name = i;
                    newParamsItem.value = params[i];

                    if(i == 'pets'){
                        $('#search-holiday-form select[name="pets"]').val(params[i]);
                    }

                    searchEngine.activeFilters.addFilter(newParamsItem);
                }
            }
        }
    },
    init: function(){
        var sDateFormat         = "yy-mm-dd";
        var oSearchFromDatapick = $('#search-from-date');
        var oSearchToDatapick   = $('#search-to-date');

        oSearchFromDatapick.datepicker({
            dateFormat:sDateFormat,
            numberOfMonths: 1,
            showButtonPanel: true,
            minDate: new Date(),
            onSelect: function(dateText, inst)
            {
                $('#search-holiday-form').submit();
                var minDate = new Date($.datepicker.parseDate(sDateFormat,dateText));
                oSearchToDatapick.datepicker( "option", "minDate", minDate );
                setTimeout(function(){oSearchToDatapick.datepicker( "show" )}, 100);
            }
        });
        oSearchToDatapick.datepicker({
            dateFormat:sDateFormat,
            numberOfMonths: 1,
            showButtonPanel: true,
            minDate: new Date(),
            onSelect: function(dateText, inst){
                $('#search-holiday-form').submit();
            }
        });

        var dataPriceFrom = 0;
        var dataPriceTo = 3000;
        if($('#estate-search-price-slider').attr('data-price-from')){
            dataPriceFrom = $('#estate-search-price-slider').attr('data-price-from');
        }
        if($('#estate-search-price-slider').attr('data-price-to')){
            dataPriceTo = $('#estate-search-price-slider').attr('data-price-to');
        }

        $('#estate-search-price-slider').slider({
            range: true,
            min: 0,
            max: 3000,
            values: [dataPriceFrom, dataPriceTo],
            slide: function(event, ui){
                $("#estate-search-price-from").html("$" + ui.values[0]);
                $("#estate-search-price-to").html("$" + ui.values[1]);
            },
            change: function(){
                $('#search-holiday-form').submit();
            }
        });
        $("#estate-search-price-from").html("$" + $( "#estate-search-price-slider" ).slider("values", 0));
        $("#estate-search-price-to").html("$" + $( "#estate-search-price-slider" ).slider("values", 1));

        $('.search-holiday-form-submit').change(function(){
            $('#search-holiday-form').submit();
        });

        $('#search-holiday-form').submit(function(e){
            e.preventDefault();

            var formParams = $(this).serializeArray();

            var priceFrom = new Object();
            priceFrom.name = 'priceFrom';
            priceFrom.value = $("#estate-search-price-slider").slider("values", 0);
            formParams.push(priceFrom);

            var priceTo = new Object();
            priceTo.name = 'priceTo';
            priceTo.value = $("#estate-search-price-slider").slider("values", 1);
            formParams.push(priceTo);

            $('#active-filters-handler .conteiner .active-filter').remove();

            var newFormParams = [];
            for(var i in formParams){
                if(formParams[i].value){
                    newFormParams.push(formParams[i]);
                    searchEngine.activeFilters.addFilter(formParams[i]);
                }
            }



            $.ajax({
                type: "POST",
                dataType: "json",
                url: Routing.generate('utt_estate_search'),
                data: newFormParams,
                success: function(data){
                    if(data){
                        if(data.success == true && data.html){
                            $('#search-result-handler').html(data.html);
                            searchEngine.initResultSliders();
                            History.pushState(null, null, '?'+$.param(newFormParams));
                        }else{
                            $('#search-result-handler').html('');
                            History.pushState(null, null, '?'+$.param(newFormParams));
                        }
                    }else{
                        $('#search-result-handler').html('');
                        History.pushState(null, null, '?'+$.param(newFormParams));
                    }
                },
                error: function(xhr, err) {
                    $('#search-result-handler').html('');
                    History.pushState(null, null, '?'+$.param(newFormParams));
                }
            });
        });

        searchEngine.router.init(window.location.href);

        History.Adapter.bind(window,'statechange',function(){
            var State = History.getState();
            searchEngine.router.init(State.url);
        });
    },
    initResultSliders: function(){
        // musi być z > > żeby po doczytaniu nie inicjowało już wcześniej zainicjowanych sliderów
        // TODO zrobić to inaczej, bardziej PRO
        $('#search-result-handler .single-result > .set-controls-direction-large > .slider').bxSlider(
            {
                mode: "fade",
                auto: false,
                pause: 7000,
                speed: 100,
                slideBorderWidth: 4,
                slideBorderHeight: 0,
                slideWidth: $(this).parent().width(),
                minSlides: 1,
                maxSlides: 1,
                startSlide: 0,
                slideMargin: 5,
                tickerHover: true,
                pager: false,
                captions: true
            });
    }
};

function get_querystring( url ) {
    return url.replace( /(?:^[^?#]*\?([^#]*).*$)?.*/, '$1' );
};
/*
 jQuery deparam is an extraction of the deparam method from Ben Alman's jQuery BBQ
 http://benalman.com/projects/jquery-bbq-plugin/
 */
(function ($) {
    $.deparam = function (params, coerce) {
        var obj = {},
            coerce_types = { 'true': !0, 'false': !1, 'null': null };

        // Iterate over all name=value pairs.
        $.each(params.replace(/\+/g, ' ').split('&'), function (j,v) {
            var param = v.split('='),
                key = decodeURIComponent(param[0]),
                val,
                cur = obj,
                i = 0,

            // If key is more complex than 'foo', like 'a[]' or 'a[b][c]', split it
            // into its component parts.
                keys = key.split(']['),
                keys_last = keys.length - 1;

            // If the first keys part contains [ and the last ends with ], then []
            // are correctly balanced.
            if (/\[/.test(keys[0]) && /\]$/.test(keys[keys_last])) {
                // Remove the trailing ] from the last keys part.
                keys[keys_last] = keys[keys_last].replace(/\]$/, '');

                // Split first keys part into two parts on the [ and add them back onto
                // the beginning of the keys array.
                keys = keys.shift().split('[').concat(keys);

                keys_last = keys.length - 1;
            } else {
                // Basic 'foo' style key.
                keys_last = 0;
            }

            // Are we dealing with a name=value pair, or just a name?
            if (param.length === 2) {
                val = decodeURIComponent(param[1]);

                // Coerce values.
                if (coerce) {
                    val = val && !isNaN(val)              ? +val              // number
                        : val === 'undefined'             ? undefined         // undefined
                        : coerce_types[val] !== undefined ? coerce_types[val] // true, false, null
                        : val;                                                // string
                }

                if ( keys_last ) {
                    // Complex key, build deep object structure based on a few rules:
                    // * The 'cur' pointer starts at the object top-level.
                    // * [] = array push (n is set to array length), [n] = array if n is
                    //   numeric, otherwise object.
                    // * If at the last keys part, set the value.
                    // * For each keys part, if the current level is undefined create an
                    //   object or array based on the type of the next keys part.
                    // * Move the 'cur' pointer to the next level.
                    // * Rinse & repeat.
                    for (; i <= keys_last; i++) {
                        key = keys[i] === '' ? cur.length : keys[i];
                        cur = cur[key] = i < keys_last
                            ? cur[key] || (keys[i+1] && isNaN(keys[i+1]) ? {} : [])
                            : val;
                    }

                } else {
                    // Simple key, even simpler rules, since only scalars and shallow
                    // arrays are allowed.

                    if ($.isArray(obj[key])) {
                        // val is already an array, so push on the next value.
                        obj[key].push( val );

                    } else if (obj[key] !== undefined) {
                        // val isn't an array, but since a second value has been specified,
                        // convert val into an array.
                        obj[key] = [obj[key], val];

                    } else {
                        // val is a scalar.
                        obj[key] = val;
                    }
                }

            } else if (key) {
                // No value was defined, so set something meaningful.
                obj[key] = coerce
                    ? undefined
                    : '';
            }
        });

        return obj;
    };
})(jQuery);
