var estateMapApp = angular.module('estateMapApp', ['ngRoute', 'ui', 'leaflet-directive']);

estateMapApp.controller('estateMapAppCtrl', ['$scope', function ($scope) {

    var estateLat = parseFloat($estateLat);
    var estateLon = parseFloat($estateLon);

    $scope.mapMarkers = {
        main: {
            lat: estateLat,
            lng: estateLon,
        }
    };
    $scope.map = {center: {lat: estateLat, lng: estateLon, zoom: 8}, defaults: {
            attributionControl: false,
            maxZoom: 12
        }};
}]);

angular.bootstrap(document.getElementById("estateMapAppHandler"), ["estateMapApp"]);