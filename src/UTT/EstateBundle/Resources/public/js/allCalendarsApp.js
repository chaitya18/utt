var allCalendarsApp = angular.module('allCalendarsApp', ['ui.bootstrap']);

allCalendarsApp.controller('allCalendarsAppCtrl', ['$scope', '$window', '$http', function($scope, $window, $http){
    $scope.allowNextLoadRun = true;

        var loadRecurrence = function(max, index, nightsFlexible, nightsStandard, locationId){
            if(index <= max){
                $scope.allowNextLoadRun = false;
                if(index == max) $scope.allowNextLoadRun = true;

                $http({
                    method: 'GET',
                    url: Routing.generate('utt_estate_all_calendars', {
                        'nightsFlexible': nightsFlexible,
                        'nightsStandard': nightsStandard,
                        'pages': max,
                        'page': index,
                        'locationId': locationId
                    }),
                    headers: {'Content-Type':'application/x-www-form-urlencoded; charset=UTF-8'}
                }).success(function(data, status, headers, config) {
                    if(index == 0 && nightsFlexible > 0){
                        $('#pricingCalendar').html(data);
                    }else{
                        $('#pricingCalendar').append(data);
                    }
                    //if(data)
                    {
                        loadRecurrence(max, index+1, nightsFlexible, nightsStandard, locationId);
                    }
                }).error(function(data, status, headers, config) {
                    if(index == 0){
                        $('#pricingCalendar').html('Error, try again.');
                    }else{
                        $('#pricingCalendar').append('Error, try again.');
                    }
                    loadRecurrence(max, index+1, nightsFlexible, nightsStandard, locationId);
                });
            }
        };
        var loadCalendars = function(){
            // load flexible
            loadRecurrence(20, 0, $scope.nightsFlexible, 0, ($scope.location ? $scope.location.id : null ));

            // load standard
            if(
                $scope.nightsFlexible == 3 || $scope.nightsFlexible == 4 || $scope.nightsFlexible == 7 ||
                $scope.nightsFlexible == 10 || $scope.nightsFlexible == 11 || $scope.nightsFlexible == 14
            ){
                loadRecurrence(20, 0, 0, $scope.nightsFlexible, ($scope.location ? $scope.location.id : null ));
            }
        };

    $scope.sleepsChoices = [
        {'id': 2, 'name': 2},
        {'id': 3, 'name': 3},
        {'id': 4, 'name': 4},
        {'id': 5, 'name': 5},
        {'id': 6, 'name': 6},
        {'id': 7, 'name': 7},
        {'id': 8, 'name': 8},
        {'id': 9, 'name': 9},
        {'id': 10, 'name': 10},
        {'id': 11, 'name': 11},
        {'id': 12, 'name': 12},
        {'id': 13, 'name': 13},
        {'id': 14, 'name': 14},
        {'id': 15, 'name': 15},
        {'id': 16, 'name': 16},
        {'id': 17, 'name': 17},
        {'id': 18, 'name': 18},
    ];

    $scope.location = null;
    $scope.initLocationChoices = function(){
        $scope.locationChoices = angular.fromJson($locations);
        $scope.locationChoices.unshift({'id': null, 'name': '-- anywhere --'});
        $scope.location = $scope.locationChoices[0];
    };
    $scope.changeLocation = function(location){
        $scope.location = location;
        loadCalendars();
    };

    $scope.buttonsFlexible = [
        {'id': 2, 'name': 2, 'allowed': true},
        {'id': 3, 'name': 3, 'allowed': true},
        {'id': 4, 'name': 4, 'allowed': true},
        {'id': 5, 'name': 5, 'allowed': true},
        {'id': 6, 'name': 6, 'allowed': true},
        {'id': 7, 'name': 7, 'allowed': true},
        {'id': 8, 'name': 8, 'allowed': true},
        {'id': 9, 'name': 9, 'allowed': true},
        {'id': 10, 'name': 10, 'allowed': true},
        {'id': 11, 'name': 11, 'allowed': true},
        {'id': 12, 'name': 12, 'allowed': true},
        {'id': 13, 'name': 13, 'allowed': true},
        {'id': 14, 'name': 14, 'allowed': true}
    ];

    var buttonsFlexibleReset = function(){
        $scope.nightsFlexibleBackup = false;

        angular.forEach($scope.buttonsFlexible, function(button){
            button.allowed = true;
        });
    };

    $scope.nightsFlexible = 3;
    $scope.nightsFlexibleBackup = false;
    $scope.setNightsFlexible = function(nightsFlexible){
        if(!$scope.allowNextLoadRun) return false;
        $scope.nightsFlexible = nightsFlexible;
        buttonsFlexibleReset();
        loadCalendars();
    };

    loadCalendars();
}]);

angular.bootstrap(document.getElementById("allCalendarsAppHandler"),["allCalendarsApp"]);

$(function () {
    $('.pricingCalendar').on('click', '.monthScrollPrev', function(){
        $('.pricingCalendar').find('.month').each(function(){
            var self = $(this);
            var marginLeft = self.css('margin-left').replace('px', '');
            if(marginLeft < 0){
                marginLeft = parseInt(marginLeft) + (36*7);
                self.css('margin-left', marginLeft + 'px');
            }
        });
    });

    $('.pricingCalendar').on('click', '.monthScrollNext', function(){
        $('.pricingCalendar').find('.month').each(function(){
            var self = $(this);
            var marginLeft = self.css('margin-left').replace('px', '');
            marginLeft = parseInt(marginLeft) - (36*7);
            self.css('margin-left', marginLeft + 'px');
        });
    });

    $('.pricingCalendar').on('mouseover', '.day', function(){
        //$(this).tooltip();
        $('.pricingCalendar .day.selected').removeClass('selected').removeClass('selectedStart').removeClass('selectedEnd');

        var self = $(this);
        self.tooltip();
        var dataPrice = self.attr('data-price').split('|');
        if(dataPrice[0] == true){
            self.addClass('selected');

            var prev = self;
            if(dataPrice[1] >= 1){
                for (var i = 1; i <= dataPrice[1]; i++) {
                    prev = prev.prev();
                    if(prev.hasClass('tooltip')){
                        prev = prev.prev();
                    }
                    prev.addClass('selected');

                    if(!prev.attr('data-original-title')){
                        prev.tooltip();
                    }
                }
            }
            var next = self;
            if(dataPrice[2] >= 1){
                for (var i = 1; i <= dataPrice[2]; i++) {
                    next = next.next();
                    if(next.hasClass('tooltip')){
                        next = next.next();
                    }
                    next.addClass('selected');

                    if(!next.attr('data-original-title')){
                        next.tooltip();
                    }
                }
            }

            prev.addClass('selectedStart');
            next.addClass('selectedEnd');
        }
    });
})