<?php

namespace UTT\EstateBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

class ReviewType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('comment', null, array('required' => true, 'label' => 'Comment holiday', 'attr' => array('class' => 'form-control')));
    }

    public function getName()
    {
        return 'review';
    }
}