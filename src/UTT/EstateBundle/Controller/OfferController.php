<?php

namespace UTT\EstateBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use UTT\ReservationBundle\Entity\Offer;
use UTT\ReservationBundle\Entity\OfferRepository;
use UTT\IndexBundle\Service\ConfigurationService;
use UTT\IndexBundle\Entity\Configuration;
use UTT\ReservationBundle\Service\PricingService;

class OfferController extends Controller
{
    public function showAction(Request $request, $offerId){
        $this->get('utt.activityLogService')->createForCurrentRoute();

        /** @var OfferRepository $offerRepository */
        $offerRepository = $this->getDoctrine()->getManager()->getRepository('UTTReservationBundle:Offer');
        $offer = $offerRepository->find($offerId);
        if(!($offer && $offer instanceof Offer)){
            return $this->render('UTTEstateBundle:Offer:not-exists.html.twig');
            //return $this->redirect($this->generateUrl('utt_index_homepage'));
        }

        $twigArray = array(
            'offer' => $offer
        );

        /** @var PricingService $pricingService */
        $pricingService = $this->get('utt.pricingservice');
        $adminCharge = $pricingService->getAdminCharge();
        $twigArray['adminCharge'] = ($adminCharge == -1 ? 0 : $adminCharge);

        return $this->render('UTTEstateBundle:Offer:show.html.twig', $twigArray);
    }

    public function listAction(Request $request){
        $this->get('utt.activityLogService')->createForCurrentRoute();

        /** @var OfferRepository $offerRepository */
        $offerRepository = $this->getDoctrine()->getManager()->getRepository('UTTReservationBundle:Offer');
        /** @var ConfigurationService $configurationService */
        $configurationService = $this->get('utt.configurationservice');

        /** @var Configuration $configuration */
        $configuration = $configurationService->get();
        $automaticSpecialOfferMaxPrice = 0;
        if($configuration instanceof Configuration){
            $automaticSpecialOfferMaxPrice = $configuration->getAutomaticSpecialOfferMaxPrice();
        }

        $offers = $offerRepository->findAvailableAtDateOffers(new \DateTime('now'), null, false, true, $automaticSpecialOfferMaxPrice);
        $lateAvailabilityOffers = $offerRepository->findAvailableAtDateOffers(new \DateTime('now'), null, true, false, $automaticSpecialOfferMaxPrice);

        $twigArray = array(
            'offers' => $offers,
            'lateAvailabilityOffers' => $lateAvailabilityOffers
        );

        /** @var PricingService $pricingService */
        $pricingService = $this->get('utt.pricingservice');
        $adminCharge = $pricingService->getAdminCharge();
        $twigArray['adminCharge'] = ($adminCharge == -1 ? 0 : $adminCharge);

        return $this->render('UTTEstateBundle:Offer:list.html.twig', $twigArray);
    }


}
