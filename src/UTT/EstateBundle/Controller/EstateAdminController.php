<?php

namespace UTT\EstateBundle\Controller;

use Sonata\AdminBundle\Controller\CRUDController as Controller;
use UTT\EstateBundle\Entity\Estate;
use UTT\ReservationBundle\Entity\PricingSeason;
use UTT\ReservationBundle\Entity\PricingSeasonRepository;
use UTT\ReservationBundle\Entity\PricingCategory;
use UTT\ReservationBundle\Entity\PricingCategoryRepository;
use UTT\ReservationBundle\Service\PricingService;
use UTT\ReservationBundle\Entity\Reservation;
use UTT\ReservationBundle\Entity\Offer;
use UTT\ReservationBundle\Entity\OfferRepository;
use UTT\ReservationBundle\Entity\Pricing;
use UTT\ReservationBundle\Service\ReservationService;
use UTT\ReservationBundle\Service\OfferService;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\ORM\EntityManager;
use UTT\EstateBundle\Entity\Photo;

class EstateAdminController extends Controller
{
    public function showAction($id = null){
        /** @var Estate $estate */
        $estate = $this->admin->getSubject();
        if($estate){
            $this->container->get('utt.aclService')->authEstateOwnerUser($estate);
        }

        return parent::showAction($id);
    }

    public function editAction($id = null){
        /** @var Estate $estate */
        $estate = $this->admin->getSubject();
        if($estate){
            $this->container->get('utt.aclService')->authEstateOwnerUser($estate);
        }

        return parent::editAction($id);
    }

    public function resetPricingTypeAction(Request $request){
        /** @var Estate $object */
        $object = $this->admin->getSubject();
        if($object) {
            $this->container->get('utt.aclService')->authForEstateGeneratePricing($object);

            $object->setType(null);
            $object->setPricingCategory(null);

            $this->getDoctrine()->getManager()->flush();

            /** @var PricingService $pricingService */
            $pricingService = $this->get('utt.pricingservice');
            $pricingService->clearPricingForEstate($object);

            $this->get('session')->getFlashBag()->add('success', 'Pricing and pricing type removed. Property is ready to change pricing policy.');
        }

        $referer = $request->headers->get('referer');
        if(!$referer) $referer = $this->admin->generateObjectUrl('edit', $object);

        return $this->redirect($referer);
    }

    public function pricingManagementAction(Request $request){
        /** @var Estate $estate */
        $estate = $this->admin->getSubject();
        if($estate) {
            $this->container->get('utt.aclService')->authForEstateGeneratePricing($estate);

            $twigArray = array(
                'action' => 'show',
                'object' => $estate,
                'elements' => $this->admin->getShow(),
                // custom params
            );

            return $this->render('UTTEstateBundle:EstateAdmin:pricing-management.html.twig', $twigArray);
        }

        return $this->redirect($this->generateUrl('sonata_admin_dashboard'));
    }

    public function generatePricingAction(Request $request){
        /** @var Estate $object */
        $object = $this->admin->getSubject();
        if($object){
            $this->container->get('utt.aclService')->authForEstateGeneratePricing($object);

                $postData = $request->query->all();
                if(isset($postData['pricingCategory'])){
                    /** @var PricingCategoryRepository $pricingCategoryRepository */
                    $pricingCategoryRepository = $this->getDoctrine()->getManager()->getRepository('UTTReservationBundle:PricingCategory');
                    /** @var PricingCategory $pricingCategory */
                    $pricingCategory = $pricingCategoryRepository->find($postData['pricingCategory']);
                    /** @var PricingService $pricingService */
                    $pricingService = $this->get('utt.pricingservice');

                    // generate standard pricing
                    // generate flexible pricing
                    if($object->getType() == Estate::TYPE_STANDARD_M_F && $pricingCategory->getType() == PricingCategory::TYPE_STANDARD_M_F){
                        $pricingService->generateStandardPricingForEstate($object, $pricingCategory);
                    }elseif($object->getType() == Estate::TYPE_FLEXIBLE_BOOKING && $pricingCategory->getType() == PricingCategory::TYPE_FLEXIBLE_BOOKING){
                        $pricingService->generateFlexiblePricingForEstate($object, $pricingCategory);
                    }

                    $lowestPrice = $pricingService->findLowestPriceByPricingCategory($object);
                    if($lowestPrice != -1){ $object->setLowestPrice($lowestPrice); }

                    $highestPrice = $pricingService->findHighestPrice($object);
                    if($highestPrice != -1){ $object->setHighestPrice($highestPrice); }

                    $this->getDoctrine()->getManager()->flush();

                    $this->get('session')->getFlashBag()->add('success', 'Pricing generated!');
                }
        }

        $referer = $request->headers->get('referer');
        if(!$referer) $referer = $this->admin->generateObjectUrl('show', $object);

        return $this->redirect($referer);
    }

    private function setPhotoMain(Request $request, Estate $object){
        $photoMainContainer = $request->get('photoMainContainer');
        if(is_numeric($photoMainContainer)){
            $photo = $this->getDoctrine()->getManager()->getRepository('UTTEstateBundle:Photo')->find($photoMainContainer);
            if($photo instanceof Photo){
                $object->setPhotoMain($photo);
            }
        }else{
            $object->setPhotoMain(null);
        }
    }

    private function attachPhotosToObject(Request $request, Estate $object){
        if(!($request->request->has('estatePhotosUploaderRun') && $request->request->get('estatePhotosUploaderRun') == 1)) return false;
        /** @var EntityManager $_em */
        $_em = $this->getDoctrine()->getManager();

        $estatePhotosIds = $request->get('estatePhotosIds');
        $estateHighlightedPhotosIds = $request->get('estateHighlightedPhotosIds');
        $estatePhotosSortOrderIds = $request->get('estatePhotosSortOrderIds');
        $estatePhotosTitles = $request->get('estatePhotosTitles');

        $objectPhotosArray = array();
        foreach($object->getPhotos() as $objectPhoto){
            if(is_array($estatePhotosIds)){
                if(!in_array($objectPhoto->getId(), $estatePhotosIds)){
                    $objectPhoto->setEstate(null);
                    $_em->remove($objectPhoto);
                }else{
                    $objectPhotosArray[] = $objectPhoto->getId();
                }
            }else{
                $objectPhoto->setEstate(null);
                $_em->remove($objectPhoto);
            }
        }
        if(is_array($estatePhotosIds)){
            foreach($estatePhotosIds as $photoId){
                /** @var Photo $photo */
                $photo = $_em->getRepository('UTTEstateBundle:Photo')->find($photoId);
                if($photo){
                    if(!in_array($photoId, $objectPhotosArray)){
                        $photo->setEstate($object);
                    }
                    if(is_array($estatePhotosSortOrderIds) && count($estatePhotosSortOrderIds)){
                        foreach($estatePhotosSortOrderIds as $key => $value){
                            if($value == $photoId){
                                $photo->setSortOrder($key);

                                if(is_array($estatePhotosTitles) && isset($estatePhotosTitles[$key])){
                                    $photo->setTitle($estatePhotosTitles[$key]);
                                }
                                break;
                            }
                        }
                    }

                    if(is_array($estateHighlightedPhotosIds) && count($estateHighlightedPhotosIds) > 0 && in_array($photoId, $estateHighlightedPhotosIds)){
                        $photo->setIsHighlighted(true);
                    }else{
                        $photo->setIsHighlighted(false);
                    }
                }
            }
        }
    }

    public function photosAction(Request $request){
        /** @var Estate $estate */
        $estate = $this->admin->getSubject();
        if($estate) {
            $this->container->get('utt.aclService')->authForEstatePhotos($estate);

            $twigArray = array(
                'action' => 'show',
                'object' => $estate,
                'elements' => $this->admin->getShow(),
                // custom params
            );


            if($request->getMethod() == 'POST'){
                $this->attachPhotosToObject($request, $estate);
                $this->setPhotoMain($request, $estate);

                $this->getDoctrine()->getManager()->flush();
            }

            return $this->render('UTTEstateBundle:EstateAdmin:photos.html.twig', $twigArray);
        }

        return $this->redirect($this->generateUrl('sonata_admin_dashboard'));
    }

    public function calendarAction(Request $request){
        /** @var Estate $estate */
        $estate = $this->admin->getSubject();
        if($estate){
            $this->container->get('utt.aclService')->authForEstateCalendar($estate);

            $twigArray = array(
                'action'   => 'show',
                'object'   => $estate,
                'elements' => $this->admin->getShow(),
                'isManageAllowed'   => ($this->container->get('utt.aclService')->isGrantedCleaner() ? 'false' : 'true')
                // custom params
            );
#################################### TODO
            //$yearDate = new \DateTime('now');
            //$yearDate->setDate($yearDate->format('Y'), $yearDate->format('m'), ($yearDate->format('d') == 1 ? 1 : $yearDate->format('d') -1));
            //$yearDate->setDate($yearDate->format('Y'), $yearDate->format('m'), 1);

            $yearDate = new \DateTime('now');
            $yearDate->setDate($yearDate->format('Y'), $yearDate->format('m'), ($yearDate->format('d') == 1 ? 1 : $yearDate->format('d') -1));
            $yearsInFuture = 4;


            /** @var PricingService $pricingService */
            $pricingService = $this->get('utt.pricingservice');
            $pricingList = $pricingService->getPricingListForEstateForYear($estate, $yearDate, $yearsInFuture);
            if($pricingList){
                $yearDate->modify('-3 months');
                $start = new \DateTime( sprintf('%04d-%d-01', $yearDate->format('Y'), $yearDate->format('m')) );
                $end = new \DateTime( sprintf('%04d-12-31', (int)$yearDate->format('Y') + ($yearsInFuture-1)) );

                /** @var ReservationService $reservationService */
                $reservationService = $this->get('utt.reservationservice');
                $reservations = $reservationService->getReservations($estate, $start, $end);

                /** @var OfferRepository $offerRepository */
                $offerRepository = $this->getDoctrine()->getManager()->getRepository('UTTReservationBundle:Offer');
                $offers = $offerRepository->findAvailableForEstateBetweenDates($estate, $start, $end);

                $end->modify('+1 day');

                $interval = new \DateInterval('P1D');
                $period   = new \DatePeriod($start, $interval, $end);

                $calendarData = array(
                    'estateShortName' => $estate->getShortName(),
                    'type' => ($estate->getType() == Estate::TYPE_STANDARD_M_F ? 'standard' : (
                        $estate->getType() == Estate::TYPE_FLEXIBLE_BOOKING ? 'flexible' : ''
                        ))
                );

                /** @var \Datetime $date */
                foreach($period as $date){
                    $isDateAvailable = false;
                    $isDateAvailableOffer = false;
                    $isDateBooked = false;
                    $isDateBookedStart = false;
                    $isDateBookedEnd = false;
                    $isDateReserved = false;
                    $isDateReservedStart = false;
                    $isDateReservedEnd = false;
                    $isDateBlocked = false;
                    $isDateBlockedStart = false;
                    $isDateBlockedEnd = false;

                    if($reservations){
                        /** @var Reservation $reservation */
                        foreach($reservations as $reservation){
                            if($date >= $reservation->getFromDate() && $date <= $reservation->getToDate()){
                                $isDateBooked = true;
                                if($reservation->isBlocked()){
                                    $isDateBlocked = true;

                                    if($date == $reservation->getFromDate()){ $isDateBlockedStart = true; }
                                    if($date == $reservation->getToDate()){ $isDateBlockedEnd = true; }
                                }

                                if($date == $reservation->getFromDate()){ $isDateBookedStart = true; }
                                if($date == $reservation->getToDate()){ $isDateBookedEnd = true; }

                                if($reservation->getStatus() == Reservation::STATUS_RESERVED){
                                    $isDateReserved = true;

                                    if($date == $reservation->getFromDate()){ $isDateReservedStart = true; }
                                    if($date == $reservation->getToDate()){ $isDateReservedEnd = true; }
                                }
                                if($date == $reservation->getFromDate() || $date == $reservation->getToDate()){
                                    $isDateAvailable = true;
                                }
                            }
                        }
                    }

                    if($offers){
                        /** @var Offer $offer */
                        foreach($offers as $offer){
                            if($date >= $offer->getValidFrom() && $date <= $offer->getValidTo()){
                                $isDateAvailableOffer = true;
                            }
                        }
                    }

                    if(!$isDateBooked){
                        /** @var Pricing $pricing */
                        foreach($pricingList as $pricing){
                            if($date >= $pricing->getFromDate() && $date <= $pricing->getToDate()){
                                $isDateAvailable = true;
                                break;
                            }
                        }
                    }

                    if($isDateBookedStart && $isDateBookedEnd){
                        $isDateAvailable = false;
                    }

                    $nowDate = new \DateTime('now');
                    $nowDate->modify('-1 day');
                    $calendarDataItem = (object) array(
                        'date' => $date->format('Y-m-d'),
                        'isPastDate' => ($date < $nowDate ? true: false),
                        'year' => $date->format('Y'),
                        'month' => $date->format('n'),
                        'day' => $date->format('d'),
                        'dayOfWeek' => $date->format('N'),
                        'available' => $isDateAvailable,
                        'availableOffer' => $isDateAvailableOffer,
                        'booked' => $isDateBooked,
                        'bookedStart' => $isDateBookedStart,
                        'bookedEnd' => $isDateBookedEnd,
                        'reserved' => $isDateReserved,
                        'reservedStart' => $isDateReservedStart,
                        'reservedEnd' => $isDateReservedEnd,
                        'blocked' => $isDateBlocked,
                        'blockedStart' => $isDateBlockedStart,
                        'blockedEnd' => $isDateBlockedEnd,
                    );

                    $calendarData['days'][]= $calendarDataItem;
                }

                /*foreach($calendarData['days'] as $calendarDataDay){
                    if($calendarDataDay->available == true && $calendarDataDay->booked == true){
                        $key = array_search($calendarDataDay, $calendarData['days']);
                        if($calendarData['days'][$key + 1]->booked == true && $calendarData['days'][$key - 1]->booked == true){
                            $calendarDataDay->available = false;
                        }
                    }
                }*/

                $pricingData = array();

                /** @var Pricing $pricing */
                foreach($pricingList as $pricing){
                    $pricingData[] = array(
                        'fromDate' => $pricing->getFromDate()->format('Y-m-d'),
                        'toDate' => $pricing->getToDate()->format('Y-m-d'),
                        'price' => $pricing->getPrice(),
                        'initialPrice' => $pricing->getInitialPrice()
                    );
                }

                /** @var OfferService $offerService */
                $offerService = $this->get('utt.offerservice');

                $offersData = array();
                if($offers){
                    /** @var Offer $offer */
                    foreach($offers as $offer){
                        $offersData[] = $offerService->offerResponseArray($offer);
                    }
                }

                $twigArray['pricingData'] = json_encode($pricingData);
                $twigArray['calendarData'] = json_encode($calendarData);
                $twigArray['offersData'] = json_encode($offersData);

                $reservationsData = array();
                if($reservations){
                    /** @var Reservation $reservation */
                    foreach($reservations as $reservation){
                        $reservationsData[] = $reservationService->reservationResponseArray($reservation);
                    }
                }
                $twigArray['reservationsData'] = json_encode($reservationsData);

                $adminCharge = $pricingService->getAdminCharge();
                $twigArray['adminCharge'] = ($adminCharge == -1 ? 0 : $adminCharge);
            }

#################################### TODO - end

            return $this->render('UTTEstateBundle:EstateAdmin:calendar.html.twig', $twigArray);
        }
        return $this->redirect($this->generateUrl('sonata_admin_dashboard'));
    }

}
