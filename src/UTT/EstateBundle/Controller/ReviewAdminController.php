<?php

namespace UTT\EstateBundle\Controller;

use Sonata\AdminBundle\Controller\CRUDController as Controller;
use UTT\EstateBundle\Entity\Review;

class ReviewAdminController extends Controller
{
    public function editAction($id = null){
        /** @var Review $review */
        $review = $this->admin->getSubject();
        if($review){
            $this->container->get('utt.aclService')->authEstateOwnerUser($review->getEstate());
        }

        return parent::editAction($id);
    }

    public function deleteAction($id){
        /** @var Review $review */
        $review = $this->admin->getSubject();
        if($review){
            $this->container->get('utt.aclService')->authEstateOwnerUser($review->getEstate());
        }

        return parent::deleteAction($id);
    }
}