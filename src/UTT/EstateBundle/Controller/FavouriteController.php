<?php

namespace UTT\EstateBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Symfony\Component\HttpFoundation\JsonResponse;
use UTT\EstateBundle\Entity\Estate;
use UTT\EstateBundle\Entity\Photo;
use UTT\EstateBundle\Entity\EstateFeature;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use UTT\EstateBundle\Service\FavouriteManager;
use UTT\UserBundle\Entity\User;
use FOS\UserBundle\Model\UserInterface;
use UTT\EstateBundle\Entity\Favourite;

class FavouriteController extends Controller
{
    private function userAuth($throwException = true){
        if(!$this->container->get('security.context')->isGranted('IS_AUTHENTICATED_FULLY') ){
            if($throwException){
                throw new AccessDeniedException('This user does not have access to this section.');
            }else{
                return false;
            }
        }

        $user = $this->container->get('security.context')->getToken()->getUser();
        if (!is_object($user) || !$user instanceof UserInterface) {
            if($throwException){
                throw new AccessDeniedException('This user does not have access to this section.');
            }else{
                return false;
            }
        }

        return $user;
    }

    public function listAction(Request $request){
        /** @var User $user */
        $user = $this->userAuth();

        $favourites = $this->getDoctrine()->getManager()->getRepository('UTTEstateBundle:Favourite')->findBy(array(
            'user' => $user->getId()
        ));

        return $this->render('UTTEstateBundle:Favourite:list.html.twig', array(
            'favourites' => $favourites
        ));
    }

    public function removeAction(Request $request, $estateId){
        /** @var User $user */
        $user = $this->userAuth();

        /** @var FavouriteManager $favouriteManager */
        $favouriteManager = $this->get('utt.favouritemanager');

        /** @var \UTT\EstateBundle\Entity\Estate $estate */
        $estate = $this->getDoctrine()->getManager()->getRepository('UTTEstateBundle:Estate')->find((int) $estateId);
        if($estate instanceof Estate){
            try{
                $favouriteManager->remove($estate, $user);

                $this->get('session')->getFlashBag()->add('success', 'Estate removed from your favourites list.');
            }catch (\Exception $e){
                $this->get('session')->getFlashBag()->add('error', $e->getMessage());
            }

            return $this->redirect($this->generateUrl('utt_estate_show_estate', array(
                'estateShortName' => $estate->getShortName()
            )));
        }

        $referer = $request->headers->get('referer');
        if(!$referer) $referer = $this->generateUrl('utt_index_homepage');

        return $this->redirect($referer);
    }

    public function addAction(Request $request, $estateId){
        /** @var User $user */
        $user = $this->userAuth();

        /** @var FavouriteManager $favouriteManager */
        $favouriteManager = $this->get('utt.favouritemanager');

        /** @var \UTT\EstateBundle\Entity\Estate $estate */
        $estate = $this->getDoctrine()->getManager()->getRepository('UTTEstateBundle:Estate')->find((int)$estateId);
        if($estate instanceof Estate){
            try{
                $favouriteManager->add($estate, $user);

                $this->get('session')->getFlashBag()->add('success', 'Property added to your favourites.');
            }catch (\Exception $e){
                $this->get('session')->getFlashBag()->add('error', $e->getMessage());
            }

            return $this->redirect($this->generateUrl('utt_estate_show_estate', array(
                'estateShortName' => $estate->getShortName()
            )));
        }

        $referer = $request->headers->get('referer');
        if(!$referer) $referer = $this->generateUrl('utt_index_homepage');

        return $this->redirect($referer);

    }
}
