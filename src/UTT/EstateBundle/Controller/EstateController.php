<?php

namespace UTT\EstateBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Symfony\Component\HttpFoundation\JsonResponse;
use UTT\EstateBundle\Entity\Estate;
use UTT\EstateBundle\Entity\EstateRepository;
use UTT\EstateBundle\Entity\Location;
use UTT\EstateBundle\Entity\LocationRepository;
use UTT\EstateBundle\Entity\Photo;
use UTT\EstateBundle\Entity\PhotoRepository;
use UTT\EstateBundle\Entity\EstateFeature;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use UTT\EstateBundle\Service\FavouriteManager;
use UTT\IndexBundle\Entity\CottageOfTheWeek;
use UTT\ReservationBundle\Entity\Pricing;
use UTT\ReservationBundle\Model\ReservationDataUserData;
use UTT\ReservationBundle\Model\ReservationData;
use UTT\ReservationBundle\Service\PricingService;
use UTT\ReservationBundle\Service\ReservationService;
use UTT\ReservationBundle\Entity\Reservation;
use FOS\UserBundle\Model\UserInterface;
use UTT\UserBundle\Entity\User;
use UTT\ReservationBundle\Entity\Offer;
use UTT\ReservationBundle\Entity\OfferRepository;
use UTT\EstateBundle\Model\EstateRoomLayout;
use UTT\EstateBundle\Service\EstateService;
use UTT\EstateBundle\Entity\Review;
use UTT\EstateBundle\Entity\ReviewRepository;
use UTT\ReservationBundle\Service\OfferService;
use UTT\EstateBundle\Entity\Category;
use UTT\EstateBundle\Entity\EstateFeatureCategory;
use UTT\EstateBundle\Entity\CategoryRepository;
use UTT\ReservationBundle\Entity\PricingSeason;
use UTT\EstateBundle\Service\EstateSearchService;
use UTT\EstateBundle\Service\EstateCalendarService;
use UTT\EstateBundle\Entity\EstateCalendarDayRepository;
use Doctrine\ORM\EntityManager;
use Liip\ImagineBundle\Imagine\Cache\CacheManager;
use UTT\EstateBundle\Service\PhotoUploadManager;
use Doctrine\Common\Collections\ArrayCollection;
use UTT\ReservationBundle\Entity\ReservationRepository;
use UTT\UserBundle\Entity\ActivityLog;
use UTT\IndexBundle\Service\ConfigurationService;
use UTT\IndexBundle\Entity\Configuration;

class EstateController extends Controller
{
    private function userAuth($throwException = true){
        if(!$this->container->get('security.context')->isGranted('IS_AUTHENTICATED_FULLY') ){
            if($throwException){
                throw new AccessDeniedException('This user does not have access to this section.');
            }else{
                return false;
            }
        }

        $user = $this->getUserFromSession();
        if(!$user && $throwException) {
            throw new AccessDeniedException('This user does not have access to this section.');
        }

        return $user;
    }

    protected function getUserFromSession() {
        $user = $this->container->get('security.context')->getToken()->getUser();
        if (!is_object($user) || !$user instanceof UserInterface) {
            return false;
        }
        return $user;
    }

    public function renderWhyUTTSectionAction(){
        return $this->render('UTTEstateBundle:Estate:renderWhyUTTSection.html.twig');
    }

    public function renderQuickSearchFormAction(Request $request){
        $twigArray = array();

        $_em = $this->getDoctrine()->getManager();
        $twigArray['locations'] = array();
        $locations = $_em->getRepository('UTTEstateBundle:Location')->getAllArray();
        if(is_array($locations) && count($locations) > 0){
            $twigArray['locations'] = ($locations);
        }

        $twigArray['allSleeps'] = array(
            (object) array('id' => 2, 'name' => 'sleeps 2'),
            (object) array('id' => 3, 'name' => 'sleeps 3'),
            (object) array('id' => 4, 'name' => 'sleeps 4'),
            (object) array('id' => 5, 'name' => 'sleeps 5'),
            (object) array('id' => 6, 'name' => 'sleeps 6'),
            (object) array('id' => 7, 'name' => 'sleeps 7'),
            (object) array('id' => 8, 'name' => 'sleeps 8'),
            (object) array('id' => 9, 'name' => 'sleeps 9'),
            (object) array('id' => 10, 'name' => 'sleeps 10'),
            (object) array('id' => 11, 'name' => 'sleeps 11'),
            (object) array('id' => 12, 'name' => 'sleeps 12'),
            (object) array('id' => 13, 'name' => 'sleeps 13'),
            (object) array('id' => 14, 'name' => 'sleeps 14'),
            (object) array('id' => 15, 'name' => 'sleeps 15'),
            (object) array('id' => 16, 'name' => 'sleeps 16'),
            (object) array('id' => 17, 'name' => 'sleeps 17'),
            (object) array('id' => 18, 'name' => 'sleeps 18'),
            (object) array('id' => 19, 'name' => 'sleeps 19'),
            (object) array('id' => 20, 'name' => 'sleeps 20'),
            (object) array('id' => 21, 'name' => 'sleeps 21'),
            (object) array('id' => 22, 'name' => 'sleeps 22'),
            (object) array('id' => 23, 'name' => 'sleeps 23'),
            (object) array('id' => 24, 'name' => 'sleeps 24'),
            (object) array('id' => 25, 'name' => 'sleeps 25'),
            (object) array('id' => 26, 'name' => 'sleeps 26'),
            (object) array('id' => 27, 'name' => 'sleeps 27'),
            (object) array('id' => 28, 'name' => 'sleeps 28'),
        );

        $twigArray['request'] = $request;

        return $this->render('UTTEstateBundle:Estate:renderQuickSearchForm.html.twig', $twigArray);
    }

    public function renderCottageOfTheWeekAction(){
        $twigArray = array();

        /** @var ConfigurationService $configurationService */
        $configurationService = $this->get('utt.configurationservice');

        /** @var CottageOfTheWeek $cottageOfTheWeek */
        $cottageOfTheWeek = $configurationService->getCottageOfTheWeek();
        if($cottageOfTheWeek instanceof CottageOfTheWeek && $cottageOfTheWeek->getEstateOfTheWeek() instanceof Estate){
            $twigArray['cottageOfTheWeek'] = $cottageOfTheWeek->getEstateOfTheWeek();
        }

        $photoEntity = new Photo();
        $estatePhotoUploadDir = $photoEntity->getUploadDir();
        unset($photoEntity);
        $twigArray['estatePhotoUploadDir'] = $estatePhotoUploadDir;

        return $this->render('UTTEstateBundle:Estate:renderCottageOfTheWeek.html.twig', $twigArray);
    }

    public function estateAirbnbIcsAction(Request $request, $estateShortName, $airbnbExportPassword){
        /** @var \BOMO\IcalBundle\Provider\IcsProvider $icsProvider */
        $icsProvider = $this->get('bomo_ical.ics_provider');
        /** @var EntityManager $_em */
        $_em = $this->get('doctrine')->getManager();
        /** @var EstateRepository $estateRepository */
        $estateRepository = $_em->getRepository('UTTEstateBundle:Estate');
        /** @var ReservationRepository $reservationRepository */
        $reservationRepository = $_em->getRepository('UTTReservationBundle:Reservation');

        /** @var Estate $estate */
        $estate = $estateRepository->findOneBy(array(
            'shortName' => $estateShortName
        ));
        if($estate instanceof Estate && $estate->getAirbnbExport() && $estate->getAirbnbExportPassword() == $airbnbExportPassword){
            $reservations = $reservationRepository->getReservationsForAirbnbExportSync($estate);
            if($reservations){
                /** @var \BOMO\IcalBundle\Model\Calendar $calendar */
                $calendar = $icsProvider->createCalendar($icsProvider->createTimezone());

                /** @var Reservation $reservation */
                foreach($reservations as $reservation){
                    /** @var \BOMO\IcalBundle\Model\Event $event */
                    $event = $calendar->newEvent();
                    $event->setIsAllDayEvent(true);
                    $event
                        ->setEndDate($reservation->getToDate())
                        ->setStartDate($reservation->getFromDate())
                        ->setName($reservation->getUserDataDecoded()->getFirstName().' '.$reservation->getUserDataDecoded()->getLastName())
                    ;
                }

                $calendarString = $calendar->returnCalendar();

                return new Response(
                    $calendarString, 200, array(
                        'Content-Type' => 'text/calendar; charset=utf-8',
                        'Content-Disposition' => 'attachment; filename="calendar.ics"',
                    )
                );
            }else{
                return new Response(
                    '', 200, array(
                        'Content-Type' => 'text/calendar; charset=utf-8',
                        'Content-Disposition' => 'attachment; filename="calendar.ics"',
                    )
                );
            }
        }

        throw new \Exception('Not found');
    }

    public function getAllEstatesAction($onlyActive = false){
        $result = array('success' => false);

        /** @var EstateRepository $estateRepository */
        $estateRepository = $this->getDoctrine()->getManager()->getRepository('UTTEstateBundle:Estate');
        if($onlyActive == true) {
            $estates = $estateRepository->getAllActive();
        } else {
            $estates = $estateRepository->findAll();
        }
        if($estates){
            $estatesArray = array();
            /** @var Estate $estate */
            foreach($estates as $estate){
                $estatesArray[] = array(
                    'shortName' => $estate->getShortname(),
                    'name' => $estate->getName(),
                    'maxSleeps' => $estate->getSleeps(),
                    'maxPets' => $estate->getMaxPetsTotal(),
                    'maxInfants' => $estate->getMaxInfantsTotal()
                );
            }

            if(count($estatesArray) > 0){
                $result = array(
                    'success' => true,
                    'estates' => $estatesArray
                );
            }
        }

        return new JsonResponse($result);
    }
    
    public function getActiveEstatesAction()
    {
        return $this->getAllEstatesAction(true);
    }

    public function adminEditAction($id){
        return $this->redirect($this->generateUrl('admin_utt_estate_estate_edit', array(
            'id' => $id
        )));
    }

    public function adminCalendarAction($id){
        return $this->redirect($this->generateUrl('admin_utt_estate_estate_calendar', array(
            'id' => $id
        )));
    }

    public function adminPhotosAction($id){
        return $this->redirect($this->generateUrl('admin_utt_estate_estate_photos', array(
            'id' => $id
        )));
    }

    public function cottagesAction(Request $request){
        $twigArray = array();

        /** @var EntityManager $_em */
        $_em = $this->getDoctrine()->getManager();

        $twigArray['categories'] = array();
        /** @var CategoryRepository $categoryRepository */
        $categoryRepository = $_em->getRepository('UTTEstateBundle:Category');
        $categories = $categoryRepository->findAll();
        if(is_array($categories) && count($categories) > 0){
            $twigArray['categories'] = $categories;
        }

        return $this->render('UTTEstateBundle:Estate:cottages.html.twig', $twigArray);
    }

    public function searchAction(Request $request){
        $this->get('utt.activityLogService')->createForCurrentRoute();

        $paramEstateName = null;
        $paramLocation = null;
        $paramCategory = null;
        $paramDateFrom = null;
        $paramDateTo = null;
        $paramSleeps = null;
        $paramEstateFeatures = null;
        $paramPets = null;
        $paramChildren = null;
        $paramInfants = null;
        $paramPrice = null;
        $paramPriceFrom = null;
        $paramPriceTo = null;
        $paramPage = 1;
        $paramSpecialCategory = null;

        $requestQuery = $request->query;

        if($requestQuery->has('estateName') && $requestQuery->get('estateName')) $paramEstateName = $requestQuery->get('estateName');
        if($requestQuery->has('location') && $requestQuery->get('location')) $paramLocation = $requestQuery->get('location');
        if($requestQuery->has('category') && $requestQuery->get('category')) $paramCategory = $requestQuery->get('category');
        if($requestQuery->has('dateFrom') && $requestQuery->get('dateFrom')) $paramDateFrom = $requestQuery->get('dateFrom');
        if($requestQuery->has('dateTo') && $requestQuery->get('dateTo')) $paramDateTo = $requestQuery->get('dateTo');
        if($requestQuery->has('sleeps') && $requestQuery->get('sleeps')) $paramSleeps = $requestQuery->get('sleeps');
        if($requestQuery->has('features') && $requestQuery->get('features')) $paramEstateFeatures = $requestQuery->get('features');
        if($requestQuery->has('pets')) $paramPets = (bool)$requestQuery->get('pets');
        if($requestQuery->has('children')) $paramChildren = (bool)$requestQuery->get('children');
        if($requestQuery->has('infants')) $paramInfants = (bool)$requestQuery->get('infants');
        if($requestQuery->has('price') && $requestQuery->get('price')) $paramPrice = $requestQuery->get('price');
        if($requestQuery->has('priceFrom') && $requestQuery->get('priceFrom')) $paramPriceFrom = $requestQuery->get('priceFrom');
        if($requestQuery->has('priceTo') && $requestQuery->get('priceTo')) $paramPriceTo = $requestQuery->get('priceTo');
        if($requestQuery->has('page') && $requestQuery->get('page')) $paramPage = $requestQuery->get('page');
        if($requestQuery->has('specialCategory')) $paramSpecialCategory = $requestQuery->get('specialCategory');

        $searchAllowed = true;
        if($paramEstateName == null && $paramLocation == null && $paramCategory == null && $paramDateFrom == null && $paramDateTo == null && $paramSleeps == null &&
            $paramEstateFeatures == null && $paramPets == null && $paramChildren == null && $paramInfants == null && $paramPriceFrom == null && $paramPriceTo == null &&
            $paramPage == 1 && $paramSpecialCategory == null){
            //$paramSpecialCategory = Estate::SPECIAL_CATEGORY_FINEST;
            //$searchAllowed = false;
        }

        /** @var EstateSearchService $estateSearchService */
        $estateSearchService = $this->get('utt.estatesearchservice');
        if(!$estateSearchService->parametersVerify($paramLocation, $paramDateFrom, $paramDateTo, $paramSleeps, $paramEstateFeatures, $paramPets, $paramChildren, $paramInfants, $paramPrice, $paramPriceFrom, $paramPriceTo, $paramPage, $paramEstateName, $paramCategory, $paramSpecialCategory)){
            if($request->isMethod('POST')){
                return new Response(json_encode(array('success' => false)));
            }else{
                return $this->redirect($this->generateUrl('utt_estate_search'));
            }
        }

        if(!($paramPrice == null)){
            if($paramPrice == 1){
                $paramPriceFrom = 0;
                $paramPriceTo = 200;
            }elseif($paramPrice == 2){
                $paramPriceFrom = 200;
                $paramPriceTo = 300;
            }elseif($paramPrice == 3){
                $paramPriceFrom = 300;
                $paramPriceTo = 500;
            }elseif($paramPrice == 5){
                $paramPriceFrom = 500;
                $paramPriceTo = 700;
            }elseif($paramPrice == 7){
                $paramPriceFrom = 700;
                $paramPriceTo = 1000;
            }
        }

        /** @var \UTT\EstateBundle\Service\EstateService $estateService */
        $estateService = $this->get('utt.estateservice');

        $estates = array();
        if($searchAllowed){
            $estates = $estateSearchService->search($paramLocation, $paramDateFrom, $paramDateTo, $paramSleeps, $paramEstateFeatures, $paramPets, $paramChildren, $paramInfants, $paramPriceFrom, $paramPriceTo, $paramPage, $paramEstateName, $paramCategory, $paramSpecialCategory);
        }

        if(is_array($estates) && count($estates) > 0){ //&& !$request->isMethod('POST')
            $estatesSorted = array();

            /** @var PhotoRepository $photoRepo */
            $photoRepo = $this->getDoctrine()->getRepository('UTTEstateBundle:Photo');
            foreach($estates as &$estate){
                /*
                $estate['photos'] = $photoRepo->getPhotosByEstateIdArraySorted($estate['id']);
                $estate['photosSorted'] = $estateService->sortPhotosArrayForEvent($estate['photos'], $estate['photoMain']);

                if(is_array($estate['photos']) && count($estate['photos']) > 0){
                    foreach($estate['photos'] as &$photo){
                        $photo['fileName'] = $estateService->generatePhotoSrc($photo, 'property_small');
                    }
                }
                if(is_array($estate['photosSorted']) && count($estate['photosSorted']) > 0){
                    foreach($estate['photosSorted'] as &$photo){
                        $photo['fileName'] = $estateService->generatePhotoSrc($photo, 'property_small');
                    }
                }
                */
                if($estate['photoMain'] && isset($estate['photoMain']['fileName']) && !$request->isMethod('POST')){
                    $estate['photoMain']['fileName'] = $estateService->generatePhotoSrc($estate['photoMain'], 'property_small');
                } else {
                    $estate['photoMain']['fileName'] = $estateService->generatePhotoSrc($estate['photoMain'], 'search_googlemap_thumb');
                }

                if($estate['specialCategory'] == Estate::SPECIAL_CATEGORY_FINEST){
                    array_unshift($estatesSorted, $estate);
                }else{
                    $estatesSorted[] = $estate;
                }
            }

            $estates = $estatesSorted;
        }

        $twigArray = array();
        $twigArray['estates'] = $estates;
        $twigArray['searchAllowed'] = $searchAllowed;
        $twigArray['noImageThumb'] = $estateService->generatePhotoSrc(false, 'property_small');
        if($request->isMethod('POST')){
            return new Response(json_encode(array(
                'success' => true,
                'result' => $twigArray,
                'page' => $paramPage
            )));
        }

        $twigArray['allSleepsChoices'] = array(
            (object) array('id' => 2, 'name' => 'sleeps 2'),
            (object) array('id' => 3, 'name' => 'sleeps 3'),
            (object) array('id' => 4, 'name' => 'sleeps 4'),
            (object) array('id' => 5, 'name' => 'sleeps 5'),
            (object) array('id' => 6, 'name' => 'sleeps 6'),
            (object) array('id' => 7, 'name' => 'sleeps 7'),
            (object) array('id' => 8, 'name' => 'sleeps 8'),
            (object) array('id' => 9, 'name' => 'sleeps 9'),
            (object) array('id' => 10, 'name' => 'sleeps 10'),
            (object) array('id' => 11, 'name' => 'sleeps 11'),
            (object) array('id' => 12, 'name' => 'sleeps 12'),
            (object) array('id' => 13, 'name' => 'sleeps 13'),
            (object) array('id' => 14, 'name' => 'sleeps 14'),
            (object) array('id' => 15, 'name' => 'sleeps 15'),
            (object) array('id' => 16, 'name' => 'sleeps 16'),
            (object) array('id' => 17, 'name' => 'sleeps 17'),
            (object) array('id' => 18, 'name' => 'sleeps 18'),
            (object) array('id' => 19, 'name' => 'sleeps 19'),
            (object) array('id' => 20, 'name' => 'sleeps 20'),
            (object) array('id' => 21, 'name' => 'sleeps 21'),
            (object) array('id' => 22, 'name' => 'sleeps 22'),
            (object) array('id' => 23, 'name' => 'sleeps 23'),
            (object) array('id' => 24, 'name' => 'sleeps 24'),
            (object) array('id' => 25, 'name' => 'sleeps 25'),
            (object) array('id' => 26, 'name' => 'sleeps 26'),
            (object) array('id' => 27, 'name' => 'sleeps 27'),
            (object) array('id' => 28, 'name' => 'sleeps 28'),
        );

        $_em = $this->getDoctrine()->getManager();
        $twigArray['locations'] = array();
        $locations = $_em->getRepository('UTTEstateBundle:Location')->getAllArray();
        if(is_array($locations) && count($locations) > 0){
            $twigArray['locations'] = ($locations);
        }

        $twigArray['categories'] = array();
        /** @var CategoryRepository $categoryRepository */
        $categoryRepository = $_em->getRepository('UTTEstateBundle:Category');
        $categories = $categoryRepository->getForSearchFilterArray();
        if(is_array($categories) && count($categories) > 0){
            $twigArray['categories'] = $categories;
        }

        $estateFeatureEntity = new EstateFeature();
        $estateFeatureUploadDir = $estateFeatureEntity->getUploadDir();
        unset($estateFeatureEntity);

        $twigArray['allEstateFeatures'] = array();
        $estateFeatures = $_em->getRepository('UTTEstateBundle:EstateFeature')->getAllArray();
        if(is_array($estateFeatures) && count($estateFeatures) > 0){
            $twigArray['estateFeatures'] = $estateFeatures;
            foreach($estateFeatures as &$estateFeature){
                $estateFeature['icon'] = '/'.$estateFeatureUploadDir.'/'.$estateFeature['icon'];
                $estateFeature['isActive'] = false;
            }
            $twigArray['allEstateFeatures'] = $estateFeatures;
        }

        return $this->render('UTTEstateBundle:Estate:search.html.twig', $twigArray);
    }

    public function makeReservationAction($estateShortName, $fromDate, $toDate, $sleeps = null){
        $this->get('utt.activityLogService')->createForCurrentRoute();

        /** @var User $user */
        $user = $this->userAuth(false);

        /** @var Estate $estate */
        $estate = $this->getDoctrine()->getManager()->getRepository('UTTEstateBundle:Estate')->findOneBy(array('shortName' => $estateShortName));
        if(!$estate) return $this->redirect($this->generateUrl('utt_index_homepage'));
        if(!((new \Datetime($fromDate)) < (new \Datetime($toDate)))) return $this->redirect($this->generateUrl('utt_index_homepage'));

        // UTT-495 Remove possibility to book 3 nights in a week for statndard properties when it's 4 night
        if($estate->getType() == Estate:: TYPE_STANDARD_M_F){
            $fromDateOb = new \DateTime($fromDate);
            $toDateOb = new \DateTime($toDate);

            $daysDiff = $fromDateOb->diff($toDateOb);
            if($daysDiff->days == 4){
                if(!($fromDateOb->format('N') == 1 && $toDateOb->format('N') == 5)){
                    return $this->redirect($this->generateUrl('utt_index_homepage'));
                }
            }
            if($daysDiff->days == 3){
                if(!($fromDateOb->format('N') == 5 && $toDateOb->format('N') == 1)){
                    return $this->redirect($this->generateUrl('utt_index_homepage'));
                }
            }

            if(!($daysDiff->days == 3 || $daysDiff->days == 4 || $daysDiff->days == 7 || $daysDiff->days == 10 || $daysDiff->days == 11 || $daysDiff->days == 14)){
                //return $this->redirect($this->generateUrl('utt_index_homepage'));
            }
        }
        // UTT-495 - END

        /** @var ReservationService $reservationService */
        $reservationService = $this->get('utt.reservationservice');

        if(!$reservationService->validateNoticePeriod($estate->getNoticePeriod(), $fromDate) || $reservationService->isBlockedForArriveOrDepart(new \Datetime($fromDate)) || $reservationService->isBlockedForArriveOrDepart(new \Datetime($toDate))) {
            return $this->redirect($this->generateUrl('utt_estate_show_estate', array(
                'estateShortName' => $estateShortName
            )));
        }

        $sleeps = (int) $sleeps;
        if(is_null($sleeps) || (!($sleeps >= $estate->getMinimumGuests() && $sleeps <= $estate->getSleeps()))){ $sleeps = $estate->getMinimumGuests(); }

        $newReservationPrice = $reservationService->newReservationPrice($estate, new \Datetime($fromDate), new \Datetime($toDate), $sleeps, 0, 0, true);
        if($newReservationPrice){
            $twigArray = array();
            $twigArray['estate'] = $estate;
            $twigArray['fromDate'] = $fromDate;
            $twigArray['toDate'] = $toDate;
            $twigArray['newReservationPrice'] = json_encode($newReservationPrice);
            $twigArray['sleeps'] = $sleeps;

            $userData = new ReservationDataUserData();

            /** @var ReservationData $reservationData */
            $reservationData = $reservationService->getReservationData();
            if($reservationData){
                $userData = $reservationData->getUserData();
            }elseif($user){
                $userData->setFirstName($user->getFirstName());
                $userData->setLastName($user->getLastName());
                $userData->setEmail($user->getEmail());
                $userData->setPhone($user->getPhone());
                $userData->setAddress($user->getAddress());
                $userData->setCity($user->getCity());
                $userData->setPostcode($user->getPostcode());
                if($user->getCountry()){
                    $userData->setCountry($user->getCountry()->getCode());
                }
            }

            $twigArray['userData'] = json_encode($userData);

            return $this->render('UTTEstateBundle:Estate:makeReservation.html.twig', $twigArray);
        }

        return $this->redirect($this->generateUrl('utt_estate_show_estate', array(
            'estateShortName' => $estateShortName
        )));
    }


    public function allAvailabilityAction(){
        $this->get('utt.activityLogService')->createForCurrentRoute();

        $twigArray = array();

        # locations
        $_em = $this->getDoctrine()->getManager();
        $locations = $_em->getRepository('UTTEstateBundle:Location')->getAllArray();
        if(is_array($locations) && count($locations) > 0){
            $twigArray['locations'] = json_encode($locations);
        }
        ##

        return $this->render('UTTEstateBundle:Estate:allAvailability.html.twig', $twigArray);
    }

    public function refreshCalendarAction(Request $request, $estateShortName){
        $result = array('success' => false);

        /** @var EstateRepository $estateRepository */
        $estateRepository = $this->getDoctrine()->getManager()->getRepository('UTTEstateBundle:Estate');
        /** @var EstateCalendarService $estateCalendarService */
        $estateCalendarService = $this->get('utt.estatecalendarservice');

        /** @var Estate $estate */
        $estate = $estateRepository->findOneBy(array(
            'shortName' => $estateShortName
        ));
        if($estate instanceof Estate){
            $estateCalendarService->generateEstateCalendar($estate);
            $result = array('success' => true);
        }

        return new JsonResponse($result);
    }

    public function getCalendarDaysAction(Request $request, $estateShortName, $nights){
        $twigArray = array();

        /** @var EntityManager $_em */
        $_em = $this->getDoctrine()->getManager();
        /** @var EstateRepository $estateRepository */
        $estateRepository = $_em->getRepository('UTTEstateBundle:Estate');
        /** @var EstateCalendarDayRepository $estateCalendarDayRepository */
        $estateCalendarDayRepository = $_em->getRepository('UTTEstateBundle:EstateCalendarDay');

        /** @var Estate $estate */
        $estate = $estateRepository->findOneBy(array(
            'shortName' => $estateShortName
        ));
        if($estate instanceof Estate){
            $days = $estateCalendarDayRepository->getDaysByEstate($nights, $estate);
            if (is_array($days)) {
                $twigArray['days'] = $days;
            }
        }

        return $this->render('UTTEstateBundle:Estate:getCalendarDays.html.twig', $twigArray);
    }

    public function allCalendarsAction($nightsFlexible, $nightsStandard, $pages, $page, $locationId){
        $twigArray = array();

        /** @var EntityManager $_em */
        $_em = $this->getDoctrine()->getManager();

        /** @var CacheManager $liipImagineCacheManager */
        $liipImagineCacheManager = $this->container->get('liip_imagine.cache.manager');
        /** @var PhotoUploadManager $photoUploadManager */
        $photoUploadManager = $this->get('utt.photouploadmanager');

        /** @var EstateCalendarDayRepository $estateCalendarDayRepository */
        $estateCalendarDayRepository = $_em->getRepository('UTTEstateBundle:EstateCalendarDay');
        /** @var EstateRepository $estateRepository */
        $estateRepository = $_em->getRepository('UTTEstateBundle:Estate');

        $estateMaxId = $estateRepository->getAllActiveMaxId();

        $pageLimit = round($estateMaxId/$pages);
        $searchMinId = $pageLimit * $page;
        $searchMaxId = $searchMinId + $pageLimit;

        $days = false;
        if($nightsFlexible > 0 && $nightsStandard > 0){
            $days = $estateCalendarDayRepository->getDays($nightsFlexible, $nightsStandard);
        }else{
            /** @var LocationRepository $locationRepository */
            $locationRepository = $_em->getRepository('UTTEstateBundle:Location');
            $locationsIds = array();

            if(!is_null($locationId)){
                /** @var Location $location */
                $location = $locationRepository->find($locationId);
                if($location instanceof Location){
                    $locationsIds[] = $location->getId();
                    $locationChildren = $location->getChildren();
                    if($locationChildren){
                        /** @var Location $locationChild */
                        foreach($locationChildren as $locationChild){
                            $locationsIds[] = $locationChild->getId();
                        }
                    }
                }
            }

            if($nightsFlexible > 0){
                $days = $estateCalendarDayRepository->getDaysFlexible($nightsFlexible, $searchMinId, $searchMaxId, $locationsIds);
                $twigArray['nights'] = $nightsFlexible;
            }elseif($nightsStandard > 0){
                $days = $estateCalendarDayRepository->getDaysStandard($nightsStandard, $searchMinId, $searchMaxId, $locationsIds);
                $twigArray['nights'] = $nightsStandard;
            }
        }

        $estateCalendars = array();
        if (is_array($days)) {
            foreach ($days as $key => $day) {
                if(isset($estateCalendars[$day['estate_id']]->days)){
                    $estateCalendars[$day['estate_id']]->days[] = $day;
                }else{
                    $estateCalendars[$day['estate_id']] = (object)array(
                        'estateName' => $day['estate_name'],
                        'estateLocationText' => $day['estate_location_text'],
                        'estateShortName' => $day['estate_short_name'],
                        'estateMinimumGuests' => $day['estate_minimum_guests'],
                        'estatePhotoFileName' => $liipImagineCacheManager->getBrowserPath($photoUploadManager->getFileDirectory().$day['estate_photo_file_name'], 'property_small'),
                        'days' => array((object)$day),
                        'months' => array()
                    );
                }

                $isMonth = false;
                foreach($estateCalendars[$day['estate_id']]->months as $month){
                    if($month['month'] == $day['month'] && $month['year'] == $day['year']){
                        $isMonth = true;
                    }
                }

                if(!$isMonth){
                    $estateCalendars[$day['estate_id']]->months[] = array(
                        'year' => $day['year'],
                        'month' => $day['month'],
                        'days' => 0
                    );
                }

                foreach($estateCalendars[$day['estate_id']]->months as $kkey => $month){
                    if($month['month'] == $day['month'] && $month['year'] == $day['year']){
                        $estateCalendars[$day['estate_id']]->months[$kkey]['days']++;
                    }
                }
            }

            $twigArray['estateCalendars'] = $estateCalendars;
        }
        return $this->render('UTTEstateBundle:Estate:allCalendars.html.twig', $twigArray);
    }

    public function showEstateAction(Request $request, $estateShortName, $sleeps = null){
        /** @var CategoryRepository $categoryRepository */
        $categoryRepository = $this->getDoctrine()->getRepository('UTTEstateBundle:Category');
        /** @var Category $category */
        $category = $categoryRepository->findOneBy(array('slug' => $estateShortName));
        if($category instanceof Category){
            /** @var \UTT\EstateBundle\Service\EstateService $estateService */
            $estateService = $this->get('utt.estateservice');

            /** @var EstateRepository $estateRepository */
            $estateRepository = $this->getDoctrine()->getManager()->getRepository('UTTEstateBundle:Estate');
            $estates = $estateRepository->findActiveCategoryFeaturedArray($category);
            if(is_array($estates) && count($estates) > 0 && !$request->isMethod('POST')){
                $estatesSorted = array();

                /** @var PhotoRepository $photoRepo */
                $photoRepo = $this->getDoctrine()->getRepository('UTTEstateBundle:Photo');
                foreach($estates as &$estate){
                    if($estate['photoMain'] && isset($estate['photoMain']['fileName'])){
                        $estate['photoMain']['fileName'] = $estateService->generatePhotoSrc($estate['photoMain'], 'property_small');
                    }

                    if($estate['specialCategory'] == Estate::SPECIAL_CATEGORY_FINEST){
                        array_unshift($estatesSorted, $estate);
                    }else{
                        $estatesSorted[] = $estate;
                    }
                }

                $estates = $estatesSorted;
            }

            $twigArray = array(
                'category' => $category,
                'featuredEstates' => $estates,
                'noImageThumb' => $estateService->generatePhotoSrc(false, 'property_small')
            );

            # categories
            /** @var CategoryRepository $categoryRepository */
            $categoryRepository = $this->getDoctrine()->getManager()->getRepository('UTTEstateBundle:Category');
            $categories = $categoryRepository->getRandForHomepageArray();
            if(is_array($categories) && count($categories) > 0){
                $twigArray['categories'] = $categories;
            }

            # locations
            $locations = $this->getDoctrine()->getManager()->getRepository('UTTEstateBundle:Location')->getAllArray();
            if(is_array($locations) && count($locations) > 0){
                $twigArray['locations'] = $locations;
            }
            ##

            $categoryEntity = new Category();
            $estateCategoryUploadDir = $categoryEntity->getUploadDir();
            unset($categoryEntity);

            $twigArray['estateCategoryUploadDir'] = $estateCategoryUploadDir;

            /** @var ReviewRepository $reviewRepository */
            $reviewRepository = $this->getDoctrine()->getManager()->getRepository('UTTEstateBundle:Review');
            $reviews = $reviewRepository->findForCategory($category, 10);
            $twigArray['verifiedReviews'] = $reviews;

            return $this->render('UTTEstateBundle:Estate:category.html.twig', $twigArray);
        }

        $this->get('utt.activityLogService')->createForCurrentRoute();

        $twigArray = array();

        /** @var EstateRepository $estateRepository */
        $estateRepository = $this->getDoctrine()->getManager()->getRepository('UTTEstateBundle:Estate');
        /** @var PhotoRepository $photoRepository */
        $photoRepository = $this->getDoctrine()->getManager()->getRepository('UTTEstateBundle:Photo');
        /** @var Estate $estate */
        $estate = $estateRepository->findOneBy(array(
            'shortName' => $estateShortName
        ));
        if(!$estate){
            return $this->render('UTTEstateBundle:Estate:not-exists.html.twig');
        }
        if(!$estate->isActiveEstate()) {
            return $this->render('UTTEstateBundle:Estate:disabled.html.twig', array(
                'estate' => $estate
            ));
        }
        $twigArray['estate'] = $estate;

        /** @var ReviewRepository $reviewRepository */
        $reviewRepository = $this->getDoctrine()->getManager()->getRepository('UTTEstateBundle:Review');
        $verifiedReviews = $reviewRepository->findBy(array(
            'estate' => $estate->getId(),
            'isVerified' => true
        ), array(
            'createdAt' => 'desc'
        ));
        if($verifiedReviews){
            $twigArray['verifiedReviews'] = $verifiedReviews;
        }

        /** @var PricingService $pricingService */
        $pricingService = $this->get('utt.pricingservice');

        $sleeps = (int) $sleeps;
        if(is_null($sleeps) || (!($sleeps >= $estate->getMinimumGuests() && $sleeps <= $estate->getSleeps()))){ $sleeps = $estate->getMinimumGuests(); }
        $twigArray['sleeps'] = $sleeps;

        $additionalSleepsPercentIncrease = $pricingService->getAdditionalSleepsPercentIncrease();
        $twigArray['additionalSleepsPercentIncrease'] = ($additionalSleepsPercentIncrease == -1 ? 0 : $additionalSleepsPercentIncrease);

#################################### TODO
        /** @var ReservationService $reservationService */
        $reservationService = $this->get('utt.reservationservice');
        /** @var EstateCalendarService $estateCalendarService */
        $estateCalendarService = $this->get('utt.estatecalendarservice');
        /** @var OfferRepository $offerRepository */
        $offerRepository = $this->getDoctrine()->getManager()->getRepository('UTTReservationBundle:Offer');

        $yearDate = new \DateTime('now');
        $yearDate->setDate($yearDate->format('Y'), $yearDate->format('m'), ($yearDate->format('d') == 1 ? 1 : $yearDate->format('d') -1));
        $yearsInFuture = 3;

        $start = new \DateTime( sprintf('%04d-%d-01', $yearDate->format('Y'), $yearDate->format('m')) );
        $end = new \DateTime( sprintf('%04d-12-31', (int)$yearDate->format('Y') + ($yearsInFuture-1)) );

        $pricingList = $pricingService->getPricingListForEstateForYear($estate, $yearDate, $yearsInFuture);
        if($pricingList){
            $reservations = $reservationService->getReservations($estate, $start, $end);

            $offersFromDate = $start;
            if(isset($pricingList[0]) && $pricingList[0] instanceof Pricing){ $offersFromDate = $pricingList[0]->getFromDate(); }
            $offers = $offerRepository->findAvailableForEstateBetweenDates($estate, $offersFromDate, $end);

            $end->modify('+1 day');
            $period = new \DatePeriod($start, new \DateInterval('P1D'), $end);

            $calendarData = array(
                'estateShortName' => $estate->getShortName(),
                'type' => ($estate->getType() == Estate::TYPE_STANDARD_M_F ? 'standard' : (
                        $estate->getType() == Estate::TYPE_FLEXIBLE_BOOKING ? 'flexible' : ''
                    )),
                'days' => $estateCalendarService->getEstateCalendarDays($estate, $period, $pricingList, $reservations, $offers),
                'isSaturdayOnly' => $estate->getIsSaturdayOnly()
            );
            $pricingData = $estateCalendarService->getEstatePricingData($pricingList);
            $offersData = $estateCalendarService->getEstateOffersData($offers);

            $twigArray['pricingData'] = json_encode($pricingData);
            $twigArray['calendarData'] = json_encode($calendarData);
            $twigArray['offersData'] = json_encode($offersData);

            $adminCharge = $pricingService->getAdminCharge();
            $twigArray['adminCharge'] = ($adminCharge == -1 ? 0 : $adminCharge);
        }

#################################### TODO - end
        $estatesNearToEstate = $estateRepository->getEstatesNearToEstateArray($estate);
        if(is_array($estatesNearToEstate) && count($estatesNearToEstate) > 0){
            $twigArray['estatesNearToEstate'] = array();

            $nearbyEstates = $estate->getNearByEstates();
            if($nearbyEstates && count($nearbyEstates) > 0){
                /** @var Estate $nearToEstate */
                foreach($estatesNearToEstate as $nearToEstate){
                    $exists = false;

                    /** @var  $nearbyEstate */
                    foreach($nearbyEstates as $nearbyEstate){
                        if($nearbyEstate->getId() == $nearToEstate['id']){
                            $exists = true;
                            break;
                        }
                    }

                    if(!$exists) $twigArray['estatesNearToEstate'][] = $nearToEstate;
                }
            }else {
                $twigArray['estatesNearToEstate'] = $estatesNearToEstate;
            }
        }

        $photoEntity = new Photo();
        $estatePhotoUploadDir = $photoEntity->getUploadDir();
        unset($photoEntity);
        $twigArray['estatePhotoUploadDir'] = $estatePhotoUploadDir;

        $estateFeatureEntity = new EstateFeature();
        $estateFeatureUploadDir = $estateFeatureEntity->getUploadDir();
        unset($estateFeatureEntity);
        $twigArray['estateFeatureUploadDir'] = $estateFeatureUploadDir;

        /** @var EstateService $estateService */
        $estateService = $this->get('utt.estateservice');
        $estateRoomLayout = $estateService->generateRoomLayout($estate->getRoomLayout(), $estate->getSleeps());
        if($estateRoomLayout instanceof EstateRoomLayout){
            $twigArray['estateRoomLayout'] = $estateRoomLayout;
        }

        $estatePhotos = $photoRepository->getPhotosByEstateIdSorted($estate->getId());
        if($estate->getPhotoMain() && $estatePhotos){
            $photos = new ArrayCollection();
            $photos->add($estate->getPhotoMain());
            foreach($estatePhotos as $estatePhoto){
                if($estatePhoto['id'] != $estate->getPhotoMain()->getId()){
                    $photos->add($estatePhoto);
                }
            }

            $estatePhotos = $photos;
        }
        $twigArray['estatePhotos'] = $estatePhotos;
        $twigArray['latestVisits'] = $estate->getLatestVisits();

        /** @var ReservationRepository $reservationRepository */
        $reservationRepository = $this->getDoctrine()->getManager()->getRepository('UTTReservationBundle:Reservation');
        /** @var Reservation $latestReservation */
        $latestReservation = $reservationRepository->getOneLatestByEstate($estate);
        if($latestReservation instanceof Reservation){
            $nowDate = new \DateTime('now');
            $nowDate->modify('-72 hours');

            if($latestReservation->getCreatedAt() > $nowDate){
                $twigArray['latestReservationDate'] = $latestReservation->getCreatedAt();
            }
        }

        $estateFeatureCategories = $this->getDoctrine()->getManager()->getRepository('UTTEstateBundle:EstateFeatureCategory')->findAll();
        $estateFeaturesArray = array();
        /** @var EstateFeatureCategory $estateFeatureCategory */
        foreach($estateFeatureCategories as $estateFeatureCategory){
            if(!isset($estateFeaturesArray[$estateFeatureCategory->getId()])){
                $estateFeaturesArray[$estateFeatureCategory->getId()] = array(
                    'estateFeatureCategory' => $estateFeatureCategory,
                    'estateFeatures' => array()
                );
            }

            /** @var EstateFeature $estateFeature */
            foreach($estate->getEstateFeatures() as $estateFeature){
                if($estateFeature->getEstateFeatureCategory() instanceof EstateFeatureCategory){
                    if($estateFeatureCategory->getId() == $estateFeature->getEstateFeatureCategory()->getId()){
                        $estateFeaturesArray[$estateFeatureCategory->getId()]['estateFeatures'][] = $estateFeature;
                    }
                }
            }
        }


        $twigArray['estateFeaturesArray'] = $estateFeaturesArray;
        $twigArray['isFavouriteEstateUser'] = $this->isFavouriteEstateUser($estate);

        return $this->render('UTTEstateBundle:Estate:showEstate.html.twig', $twigArray);
    }

    /**
     * @param Estate $estate
     * @return bool
     */
    protected function isFavouriteEstateUser(Estate $estate)
    {
        $result = false;
        try {
            /** @var FavouriteManager $favouriteManager */
            $favouriteManager = $this->get('utt.favouritemanager');
            $user = $this->getUserFromSession();
            if($user) {
                $result = $favouriteManager->isUserEstate($estate,$user);
            }
        } catch (\Exception $e) {
            $result = false;
        }

        return $result;
    }

    public function importPhotosFromFacebookAlbumAction($facebookAlbumId, $facebookAccessToken){
        $result = array('success' => false);

        $images = array();
        $url = 'https://graph.facebook.com/'.$facebookAlbumId.'/photos?access_token='.$facebookAccessToken;
        do{
            $cursor = false;
            $obj = json_decode(file_get_contents($url));
            if(isset($obj->data) && is_array($obj->data) && count($obj->data) > 0){
                foreach($obj->data as $fbImageObj){
                    if(isset($fbImageObj->images) && is_array($fbImageObj->images) && count($fbImageObj->images) > 0){
                        $images[] = array(
                            'image' => $fbImageObj->images[0]->source,
                            'title' => (isset($fbImageObj->name) ? $fbImageObj->name : '')
                        );
                    }
                }
                if(isset($obj->paging) && isset($obj->paging->next)){
                    $cursor = true;
                    $url = $obj->paging->next;
                }
            }
        }while($cursor == true);

        if(is_array($images) && count($images) > 0){
            $imagesArray = array();

            $liipImagineCacheManager = $this->container->get('liip_imagine.cache.manager');
            $photoUploadManager = $this->get('utt.photouploadmanager');
            $fileDirectory = $photoUploadManager->getFileDirectory();
            $fileSubDirectory = $photoUploadManager->getFileSubdirectory($fileDirectory);
            foreach($images as $image){
                $extension = explode('.', $image['image']);
                $extension = $extension[count($extension) - 1];
                if(strpos($extension, '?') !== false){
                    $extension = explode('?', $extension);
                    $extension = $extension[0];
                }
                $newFileName = $photoUploadManager->getFileName($fileDirectory, $fileSubDirectory, $extension);

                file_put_contents($fileDirectory.$newFileName, file_get_contents($image['image']));
                if(file_exists($fileDirectory.$newFileName)){
                    $photo = new Photo();
                    $photo->setFileName($newFileName);
                    $photo->setTitle($image['title']);
                    $_em = $this->getDoctrine()->getEntityManager();
                    $_em->persist($photo);
                    $_em->flush();

                    $imagesArray[] = (object) array(
                        'id' => $photo->getId(),
                        'fileNameOrigin' => $fileDirectory.$newFileName,
                        'fileName' => $liipImagineCacheManager->getBrowserPath($fileDirectory.$newFileName, 'admin_estate_photo_preview_thumb')
                    );
                }
            }

            if(is_array($imagesArray) && count($imagesArray) > 0){
                $result = array(
                    'success' => true,
                    'objects' => $imagesArray
                );
            }
        }

        return new JsonResponse($result);
    }

    public function ajaxSavePhotoTitleAction(Request $request){
        $result = array('success' => false);
        if($request->isXmlHttpRequest() && $request->isMethod('POST')){
            if($request->request->has('photoId') && $request->request->has('title')){
                $photoId = $request->request->get('photoId');
                $title = $request->request->get('title');

                $_em = $this->getDoctrine()->getManager();
                /** @var PhotoRepository $photoRepository */
                $photoRepository = $_em->getRepository('UTTEstateBundle:Photo');
                /** @var Photo $photo */
                $photo = $photoRepository->find($photoId);
                if($photo instanceof Photo){
                    $photo->setTitle($title);
                    $_em->flush();

                    $result = array('success' => true);
                }
            }
        }

        return new JsonResponse($result);
    }

    public function ajaxGetPhotosByEstateIdAction(Request $request, $estateId){
        $result = array('success' => false);
        if($request->isXmlHttpRequest()){
            $idsArray = $this->getDoctrine()->getRepository('UTTEstateBundle:Photo')->getPhotosByEstateIdArraySorted($estateId);
            if(is_array($idsArray) && count($idsArray) > 0){
                $liipImagineCacheManager = $this->container->get('liip_imagine.cache.manager');
                $photoUploadManager = $this->get('utt.photouploadmanager');
                $fileDirectory = $photoUploadManager->getFileDirectory();
                foreach($idsArray as &$photo){
                    $photo['fileNameOrigin'] = $fileDirectory.$photo['fileName'];
                    $photo['fileName'] = $liipImagineCacheManager->getBrowserPath($fileDirectory.$photo['fileName'], 'admin_estate_photo_preview_thumb');
                }

                $result = array(
                    'success' => true,
                    'objects' => $idsArray
                );
            }
        }

        return new JsonResponse($result);
    }

    public function uploadPhotoAction(){
        $result = array('success' => false);
        $allowed = array('png', 'jpg', 'jpeg', 'gif');
        if(isset($_FILES['upl']) && $_FILES['upl']['error'] == 0){
            $extension = pathinfo($_FILES['upl']['name'], PATHINFO_EXTENSION);

            if(!in_array(strtolower($extension), $allowed)){
                $result = array('success' => false, 'error' => 'not allowed');
                echo json_encode($result);
                exit;
            }

            $photoUploadManager = $this->get('utt.photouploadmanager');
            $fileDirectory = $photoUploadManager->getFileDirectory();
            $fileSubDirectory = $photoUploadManager->getFileSubdirectory($fileDirectory);
            $newFileName = $photoUploadManager->getFileName($fileDirectory, $fileSubDirectory, strtolower($extension));
                    $liipImagineCacheManager = $this->container->get('liip_imagine.cache.manager');
                    
            if(move_uploaded_file($_FILES['upl']['tmp_name'], $fileDirectory.$newFileName)){
                if(file_exists($fileDirectory.$newFileName)){
                    $photo = new Photo();
                    $photo->setFileName($newFileName);
                    $_em = $this->getDoctrine()->getEntityManager();
                    $_em->persist($photo);
                    $_em->flush();

                    $liipImagineCacheManager = $this->container->get('liip_imagine.cache.manager');
                    $result = array(
                        'success' => true,
                        'object' => array(
                            'id' => $photo->getId(),
                            'fileNameOrigin' => $fileDirectory.$newFileName,
                            'fileName' => $liipImagineCacheManager->getBrowserPath($fileDirectory.$newFileName, 'admin_estate_photo_preview_thumb')
                        )
                    );
                }else{
                    $result = array('success' => false, 'error' => 'not file_exists');
                }
            }else{
                $result = array('success' => false, 'error' => 'not move_uploaded_file');
            }
        }else{
            $result = array('success' => false, 'error' => $_FILES['upl']['error']);
        }

        echo json_encode($result);
        exit;








        $result = array('success' => false);
        if(!empty($_FILES)){
            $fileParts = pathinfo($_FILES['Filedata']['name']);
            if (in_array(strtolower($fileParts['extension']), array('jpg','jpeg','gif','png'))) {
                $photoUploadManager = $this->get('utt.photouploadmanager');

                $fileDirectory = $photoUploadManager->getFileDirectory();
                $fileSubDirectory = $photoUploadManager->getFileSubdirectory($fileDirectory);
                $newFileName = $photoUploadManager->getFileName($fileDirectory, $fileSubDirectory, strtolower($fileParts['extension']));

                move_uploaded_file($_FILES['Filedata']['tmp_name'], $fileDirectory.$newFileName);

                if(file_exists($fileDirectory.$newFileName)){
                    $photo = new Photo();
                    $photo->setFileName($newFileName);
                    $_em = $this->getDoctrine()->getEntityManager();
                    $_em->persist($photo);
                    $_em->flush();

                    $liipImagineCacheManager = $this->container->get('liip_imagine.cache.manager');
                    $result = array(
                        'success' => true,
                        'object' => array(
                            'id' => $photo->getId(),
                            'fileNameOrigin' => $fileDirectory.$newFileName,
                            'fileName' => $liipImagineCacheManager->getBrowserPath($fileDirectory.$newFileName, 'admin_estate_photo_preview_thumb')
                        )
                    );
                }
            }
        }
        echo json_encode($result);
        exit;
    }

    /*public function indexAction(){
        return $this->render('UTTEstateBundle:Estate:index.html.twig');
    }*/

    public function searchEstateAction(Request $request)
    {
        /** @var EntityManager $_em */
        $_em = $this->get('doctrine')->getManager();
        /** @var EstateRepository $estateRepository */
        $estateRepository = $_em->getRepository('UTTEstateBundle:Estate');
        $data = $estateRepository->getAllActiveNames();
        return new JsonResponse($data);
    }
}
