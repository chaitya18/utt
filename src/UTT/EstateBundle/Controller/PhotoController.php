<?php

namespace UTT\EstateBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use UTT\EstateBundle\Entity\Photo;
use UTT\EstateBundle\Entity\PhotoRepository;
use UTT\EstateBundle\Service\PhotoUploadManager;
use Symfony\Component\HttpFoundation\JsonResponse;
use Liip\ImagineBundle\Imagine\Cache\CacheManager;

class PhotoController extends Controller
{
    public function rotateAction(Request $request, $photoId, $rotateType){
        $result = array('success' => false);

        /** @var PhotoUploadManager $photoUploadManager */
        $photoUploadManager = $this->get('utt.photouploadmanager');
        /** @var PhotoRepository $photoRepository */
        $photoRepository = $this->getDoctrine()->getManager()->getRepository('UTTEstateBundle:Photo');
        /** @var CacheManager $liipImagineCacheManager */
        $liipImagineCacheManager = $this->container->get('liip_imagine.cache.manager');

        $photo = $photoRepository->find($photoId);
        if($photo instanceof Photo){
            $degrees = 0;
            if($rotateType == 'left'){
                $degrees = 90;
            }elseif($rotateType == 'right'){
                $degrees = -90;
            }

            $newPhoto = $photoUploadManager->duplicateRotatePhoto($photo, $degrees);
            if($newPhoto instanceof Photo){
                $fileDirectory = $photoUploadManager->getFileDirectory();

                $result = array(
                    'success' => true,
                    'object' => array(
                        'id' => $newPhoto->getId(),
                        'fileNameOrigin' => $fileDirectory.$newPhoto->getFileName(),
                        'fileName' => $liipImagineCacheManager->getBrowserPath($fileDirectory.$newPhoto->getFileName(), 'admin_estate_photo_preview_thumb'),
                        'isHighlighted' => $newPhoto->getIsHighlighted(),
                        'title' => $newPhoto->getTitle()
                    )
                );
            }
        }

        return new JsonResponse($result);
    }
}
