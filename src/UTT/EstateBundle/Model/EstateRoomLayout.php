<?php

namespace UTT\EstateBundle\Model;

class EstateRoomLayout {
    /** @var string $description */
    public $description;

    /** @var integer $beds */
    public $beds;

    /** @var integer $rooms */
    public $rooms;

    /** @var integer $sleeps */
    public $sleeps;

    /**
     * @param int $sleeps
     */
    public function setSleeps($sleeps)
    {
        $this->sleeps = $sleeps;
    }

    /**
     * @return int
     */
    public function getSleeps()
    {
        return $this->sleeps;
    }

    /**
     * @param int $beds
     */
    public function setBeds($beds)
    {
        $this->beds = $beds;
    }

    /**
     * @return int
     */
    public function getBeds()
    {
        return $this->beds;
    }

    /**
     * @param string $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param int $rooms
     */
    public function setRooms($rooms)
    {
        $this->rooms = $rooms;
    }

    /**
     * @return int
     */
    public function getRooms()
    {
        return $this->rooms;
    }
}