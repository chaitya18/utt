<?php

namespace UTT\EstateBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * EstateVisitReport
 *
 * @ORM\Table(name="estate_visit_report")
 * @ORM\Entity(repositoryClass="UTT\EstateBundle\Entity\EstateVisitReportRepository")
 */
class EstateVisitReport
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="Estate")
     * @ORM\JoinColumn(name="estate_id", referencedColumnName="id", nullable=true, onDelete="SET NULL")
     */
    protected $estate;

    /**
     * @ORM\Column(name="report_date", type="date")
     */
    protected $reportDate;

    /**
     * @ORM\Column(name="visits", type="integer")
     */
    protected $visits;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set reportDate
     *
     * @param \DateTime $reportDate
     * @return EstateVisitReport
     */
    public function setReportDate($reportDate)
    {
        $this->reportDate = $reportDate;

        return $this;
    }

    /**
     * Get reportDate
     *
     * @return \DateTime 
     */
    public function getReportDate()
    {
        return $this->reportDate;
    }

    /**
     * Set visits
     *
     * @param integer $visits
     * @return EstateVisitReport
     */
    public function setVisits($visits)
    {
        $this->visits = $visits;

        return $this;
    }

    /**
     * Get visits
     *
     * @return integer 
     */
    public function getVisits()
    {
        return $this->visits;
    }

    /**
     * Set estate
     *
     * @param \UTT\EstateBundle\Entity\Estate $estate
     * @return EstateVisitReport
     */
    public function setEstate(\UTT\EstateBundle\Entity\Estate $estate = null)
    {
        $this->estate = $estate;

        return $this;
    }

    /**
     * Get estate
     *
     * @return \UTT\EstateBundle\Entity\Estate 
     */
    public function getEstate()
    {
        return $this->estate;
    }
}
