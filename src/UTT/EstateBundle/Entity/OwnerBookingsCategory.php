<?php

namespace UTT\EstateBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * OwnerBookingsCategory
 *
 * @ORM\Table(name="owner_booking_category")
 * @ORM\Entity
 */
class OwnerBookingsCategory
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(name="name", type="string", length=255, nullable=true)
     */
    protected $name;

    /**
     * @ORM\Column(name="is_unlimited", type="boolean")
     */
    protected $isUnlimited = false;

    /**
     * @ORM\Column(name="nights", type="integer", nullable=true)
     */
    protected $nights;

#--------------------------------------------------- ENTITY METHODS ---------------------------------------------------#

    public function __toString(){
        if($this->isUnlimited){
            if($this->getName())
                return $this->getName().' [unlimited nights]';
            return '';
        }else{
            if($this->getName())
                return $this->getName().' ['.(string)$this->nights.' nights]';
            return '';
        }
    }

#################################################### ENTITY METHODS ####################################################

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return OwnerBookingsCategory
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set isUnlimited
     *
     * @param boolean $isUnlimited
     * @return OwnerBookingsCategory
     */
    public function setIsUnlimited($isUnlimited)
    {
        $this->isUnlimited = $isUnlimited;

        return $this;
    }

    /**
     * Get isUnlimited
     *
     * @return boolean 
     */
    public function getIsUnlimited()
    {
        return $this->isUnlimited;
    }

    /**
     * Set nights
     *
     * @param integer $nights
     * @return OwnerBookingsCategory
     */
    public function setNights($nights)
    {
        $this->nights = $nights;

        return $this;
    }

    /**
     * Get nights
     *
     * @return integer 
     */
    public function getNights()
    {
        return $this->nights;
    }
}
