<?php

namespace UTT\EstateBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * EstateCalendarDay
 *
 * @ORM\Table(name="estate_calendar_day")
 * @ORM\Entity(repositoryClass="UTT\EstateBundle\Entity\EstateCalendarDayRepository")
 */
class EstateCalendarDay
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="Estate")
     * @ORM\JoinColumn(name="estate_id", referencedColumnName="id", onDelete="CASCADE")
     */
    protected $estate;

    /**
     * @ORM\Column(name="date", type="date")
     */
    protected $date;

    /**
     * @ORM\Column(name="isPastDate", type="boolean")
     */
    protected $isPastDate = true;

    /**
     * @ORM\Column(name="year", type="integer")
     */
    protected $year;

    /**
     * @ORM\Column(name="month", type="integer")
     */
    protected $month;

    /**
     * @ORM\Column(name="day", type="integer")
     */
    protected $day;

    /**
     * @ORM\Column(name="dayOfWeek", type="integer")
     */
    protected $dayOfWeek;

    /**
     * @ORM\Column(name="available", type="boolean")
     */
    protected $available = true;

    /**
     * @ORM\Column(name="availableOffer", type="boolean")
     */
    protected $availableOffer = true;

    /**
     * @ORM\Column(name="booked", type="boolean")
     */
    protected $booked = true;

    /**
     * @ORM\Column(name="bookedStart", type="boolean")
     */
    protected $bookedStart = true;

    /**
     * @ORM\Column(name="bookedEnd", type="boolean")
     */
    protected $bookedEnd = true;

    /**
     * @ORM\Column(name="reserved", type="boolean")
     */
    protected $reserved = true;

    /**
     * @ORM\Column(name="reservedStart", type="boolean")
     */
    protected $reservedStart = true;

    /**
     * @ORM\Column(name="reservedEnd", type="boolean")
     */
    protected $reservedEnd = true;

    /**
     * @ORM\Column(name="isMin7NightsStay", type="boolean")
     */
    protected $isMin7NightsStay = true;

    /**
     * @ORM\Column(name="isBlockedForArriveOrDepart", type="boolean")
     */
    protected $isBlockedForArriveOrDepart = true;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     * @return EstateCalendarDay
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime 
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set isPastDate
     *
     * @param boolean $isPastDate
     * @return EstateCalendarDay
     */
    public function setIsPastDate($isPastDate)
    {
        $this->isPastDate = $isPastDate;

        return $this;
    }

    /**
     * Get isPastDate
     *
     * @return boolean 
     */
    public function getIsPastDate()
    {
        return $this->isPastDate;
    }

    /**
     * Set year
     *
     * @param integer $year
     * @return EstateCalendarDay
     */
    public function setYear($year)
    {
        $this->year = $year;

        return $this;
    }

    /**
     * Get year
     *
     * @return integer 
     */
    public function getYear()
    {
        return $this->year;
    }

    /**
     * Set month
     *
     * @param integer $month
     * @return EstateCalendarDay
     */
    public function setMonth($month)
    {
        $this->month = $month;

        return $this;
    }

    /**
     * Get month
     *
     * @return integer 
     */
    public function getMonth()
    {
        return $this->month;
    }

    /**
     * Set day
     *
     * @param integer $day
     * @return EstateCalendarDay
     */
    public function setDay($day)
    {
        $this->day = $day;

        return $this;
    }

    /**
     * Get day
     *
     * @return integer 
     */
    public function getDay()
    {
        return $this->day;
    }

    /**
     * Set dayOfWeek
     *
     * @param integer $dayOfWeek
     * @return EstateCalendarDay
     */
    public function setDayOfWeek($dayOfWeek)
    {
        $this->dayOfWeek = $dayOfWeek;

        return $this;
    }

    /**
     * Get dayOfWeek
     *
     * @return integer 
     */
    public function getDayOfWeek()
    {
        return $this->dayOfWeek;
    }

    /**
     * Set available
     *
     * @param boolean $available
     * @return EstateCalendarDay
     */
    public function setAvailable($available)
    {
        $this->available = $available;

        return $this;
    }

    /**
     * Get available
     *
     * @return boolean 
     */
    public function getAvailable()
    {
        return $this->available;
    }

    /**
     * Set availableOffer
     *
     * @param boolean $availableOffer
     * @return EstateCalendarDay
     */
    public function setAvailableOffer($availableOffer)
    {
        $this->availableOffer = $availableOffer;

        return $this;
    }

    /**
     * Get availableOffer
     *
     * @return boolean 
     */
    public function getAvailableOffer()
    {
        return $this->availableOffer;
    }

    /**
     * Set booked
     *
     * @param boolean $booked
     * @return EstateCalendarDay
     */
    public function setBooked($booked)
    {
        $this->booked = $booked;

        return $this;
    }

    /**
     * Get booked
     *
     * @return boolean 
     */
    public function getBooked()
    {
        return $this->booked;
    }

    /**
     * Set bookedStart
     *
     * @param boolean $bookedStart
     * @return EstateCalendarDay
     */
    public function setBookedStart($bookedStart)
    {
        $this->bookedStart = $bookedStart;

        return $this;
    }

    /**
     * Get bookedStart
     *
     * @return boolean 
     */
    public function getBookedStart()
    {
        return $this->bookedStart;
    }

    /**
     * Set bookedEnd
     *
     * @param boolean $bookedEnd
     * @return EstateCalendarDay
     */
    public function setBookedEnd($bookedEnd)
    {
        $this->bookedEnd = $bookedEnd;

        return $this;
    }

    /**
     * Get bookedEnd
     *
     * @return boolean 
     */
    public function getBookedEnd()
    {
        return $this->bookedEnd;
    }

    /**
     * Set reserved
     *
     * @param boolean $reserved
     * @return EstateCalendarDay
     */
    public function setReserved($reserved)
    {
        $this->reserved = $reserved;

        return $this;
    }

    /**
     * Get reserved
     *
     * @return boolean 
     */
    public function getReserved()
    {
        return $this->reserved;
    }

    /**
     * Set reservedStart
     *
     * @param boolean $reservedStart
     * @return EstateCalendarDay
     */
    public function setReservedStart($reservedStart)
    {
        $this->reservedStart = $reservedStart;

        return $this;
    }

    /**
     * Get reservedStart
     *
     * @return boolean 
     */
    public function getReservedStart()
    {
        return $this->reservedStart;
    }

    /**
     * Set reservedEnd
     *
     * @param boolean $reservedEnd
     * @return EstateCalendarDay
     */
    public function setReservedEnd($reservedEnd)
    {
        $this->reservedEnd = $reservedEnd;

        return $this;
    }

    /**
     * Get reservedEnd
     *
     * @return boolean 
     */
    public function getReservedEnd()
    {
        return $this->reservedEnd;
    }

    /**
     * Set isMin7NightsStay
     *
     * @param boolean $isMin7NightsStay
     * @return EstateCalendarDay
     */
    public function setIsMin7NightsStay($isMin7NightsStay)
    {
        $this->isMin7NightsStay = $isMin7NightsStay;

        return $this;
    }

    /**
     * Get isMin7NightsStay
     *
     * @return boolean 
     */
    public function getIsMin7NightsStay()
    {
        return $this->isMin7NightsStay;
    }

    /**
     * Set isBlockedForArriveOrDepart
     *
     * @param boolean $isBlockedForArriveOrDepart
     * @return EstateCalendarDay
     */
    public function setIsBlockedForArriveOrDepart($isBlockedForArriveOrDepart)
    {
        $this->isBlockedForArriveOrDepart = $isBlockedForArriveOrDepart;

        return $this;
    }

    /**
     * Get isBlockedForArriveOrDepart
     *
     * @return boolean 
     */
    public function getIsBlockedForArriveOrDepart()
    {
        return $this->isBlockedForArriveOrDepart;
    }

    /**
     * Set estate
     *
     * @param \UTT\EstateBundle\Entity\Estate $estate
     * @return EstateCalendarDay
     */
    public function setEstate(\UTT\EstateBundle\Entity\Estate $estate = null)
    {
        $this->estate = $estate;

        return $this;
    }

    /**
     * Get estate
     *
     * @return \UTT\EstateBundle\Entity\Estate 
     */
    public function getEstate()
    {
        return $this->estate;
    }
}
