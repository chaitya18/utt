<?php

namespace UTT\EstateBundle\Entity;

use Doctrine\ORM\EntityRepository;

class PhotoRepository extends EntityRepository
{
    public function getPhotosByEstateIdSorted($estateId){
        if(!(is_numeric($estateId) && $estateId > 0)) return false;

        $query = $this->_em->createQuery("SELECT p.id, p.fileName, p.isHighlighted, p.title FROM UTTEstateBundle:Photo p WHERE p.estate = :estateId ORDER BY p.sortOrder");
        $query->setParameter('estateId', $estateId);
        return $query->getResult();
    }

    public function getPhotosByEstateIdArraySorted($estateId){
        if(!(is_numeric($estateId) && $estateId > 0)) return false;

        $query = $this->_em->createQuery("SELECT p.id, p.fileName, p.isHighlighted, p.title FROM UTTEstateBundle:Photo p WHERE p.estate = :estateId ORDER BY p.sortOrder DESC");
        $query->setParameter('estateId', $estateId);
        return $query->getArrayResult();
    }

    public function getLatestArray($limit = false, $randomize = false){
        $query = $this->_em->createQuery("SELECT p.id, p.fileName, p.isHighlighted, e.id as estateId, e.name as estateName, e.shortName as estateShortName, e.sleeps as estateSleeps FROM UTTEstateBundle:Photo p LEFT JOIN p.estate e WHERE p.isHighlighted = 1 AND e.id IS NOT NULL ORDER BY p.id DESC");
        if($limit){
            $query->setMaxResults($limit);
        }
        $result = $query->getArrayResult();

        if(is_array($result) && count($result) > 0){
            if($randomize) shuffle($result);
            return $result;
        }

        return false;
    }

}
