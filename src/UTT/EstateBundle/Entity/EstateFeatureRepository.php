<?php

namespace UTT\EstateBundle\Entity;

use Doctrine\ORM\EntityRepository;

class EstateFeatureRepository extends EntityRepository
{
    public function getAllArray(){
        $query = $this->_em->createQuery("SELECT ef FROM UTTEstateBundle:EstateFeature ef ORDER BY ef.id ASC");
        return $query->getArrayResult();
    }

}
