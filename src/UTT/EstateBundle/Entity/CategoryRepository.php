<?php

namespace UTT\EstateBundle\Entity;

use Doctrine\ORM\EntityRepository;

/**
 * Class CategoryRepository
 * @package UTT\EstateBundle\Entity
 * @todo refactor to EntityRepository
 */
class CategoryRepository extends EntityRepository
{
    public function getPreferredSorted(){
        $query = $this->_em->createQuery("SELECT c FROM UTTEstateBundle:Category c WHERE c.sortOrder IS NOT NULL ORDER BY c.sortOrder ASC");
        return $query->getResult();
    }

    public function getForSearchFilterArray(){
        $query = $this->_em->createQuery("SELECT c FROM UTTEstateBundle:Category c WHERE c.isSearchFilter = TRUE ORDER BY c.id ASC");
        return $query->getArrayResult();
    }

    public function getForHeaderMenuArray(){
        $query = $this->_em->createQuery("SELECT c FROM UTTEstateBundle:Category c WHERE c.isHeaderMenu = TRUE ORDER BY c.id ASC");
        return $query->getArrayResult();
    }

    public function getRandForHomepageArray(){
        $query = $this->_em->createQuery("SELECT c FROM UTTEstateBundle:Category c WHERE c.isHomepage = TRUE");
        $result = $query->getArrayResult();

        if(is_array($result) && count($result) > 0){
            shuffle($result);
            return $result;
        }

        return false;
    }

    /**
     * @return array
     */
    public function getHomepageSorted(){
        $query = $this->_em->createQuery("SELECT c FROM UTTEstateBundle:Category c WHERE c.sortOrder IS NOT NULL AND c.isHomepage = 1 ORDER BY c.sortOrder ASC");
        return $query->getArrayResult();
    }

}
