<?php

namespace UTT\EstateBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Doctrine\Common\Collections\Collection;
use UTT\UserBundle\Entity\User;

/**
 * Estate
 *
 * @ORM\Table(name="estate")
 * @ORM\Entity(repositoryClass="UTT\EstateBundle\Entity\EstateRepository")
 * @UniqueEntity(fields="shortName", message="This short name is already in use")
 */
class Estate
{
    const SPECIAL_CATEGORY_NONE = 1;
    const SPECIAL_CATEGORY_FINEST = 2;
    const SPECIAL_CATEGORY_BUDGET = 3;

    public function getAllowedSpecialCategories(){
        $array = array(
            self::SPECIAL_CATEGORY_NONE => 'None',
            self::SPECIAL_CATEGORY_FINEST => 'Finest',
            self::SPECIAL_CATEGORY_BUDGET => 'Budget'
        );
        return $array;
    }

    public function getSpecialCategoryName($hideFromDisplayId = null){
        $array = $this->getAllowedSpecialCategories();
        if(is_null($hideFromDisplayId)){
            if($this->getSpecialCategory()) {
                return array_key_exists($this->getSpecialCategory(), $array) ? $array[$this->getSpecialCategory()] : false;
            }else{
                return false;
            }
        }
        return array_key_exists($hideFromDisplayId, $array) ? $array[$hideFromDisplayId] : false;
    }

########################################################################################################################

    const HIDE_FROM_DISPLAY_VISIBLE = 1;
    const HIDE_FROM_DISPLAY_HIDDEN = 2;
    const HIDE_FROM_DISPLAY_REMOVED = 3;

    public function getAllowedHideFromDisplays(){
        $array = array(
            self::HIDE_FROM_DISPLAY_VISIBLE => 'Visible',
            self::HIDE_FROM_DISPLAY_HIDDEN => 'Hidden',
            self::HIDE_FROM_DISPLAY_REMOVED => 'Removed'
        );
        return $array;
    }

    public function getHideFromDisplayName($hideFromDisplayId = null){
        $array = $this->getAllowedHideFromDisplays();
        if(is_null($hideFromDisplayId)){
            if($this->getHideFromDisplay()) {
                return array_key_exists($this->getHideFromDisplay(), $array) ? $array[$this->getHideFromDisplay()] : false;
            }else{
                return false;
            }
        }
        return array_key_exists($hideFromDisplayId, $array) ? $array[$hideFromDisplayId] : false;
    }

########################################################################################################################
    //const TYPE_BED_AND_BREAKFAST = 1;
    const TYPE_FLEXIBLE_BOOKING = 2;
    const TYPE_STANDARD_M_F = 3;
    //const TYPE_STANDARD_M_F_SAT = 4;
    //const TYPE_SATURDAY_ONLY = 5;
    //const TYPE_NIGHTLY_RATE = 6;

    public function getAllowedTypes(){
        $array = array(
            //self::TYPE_BED_AND_BREAKFAST => 'Bed and Breakfast',
            self::TYPE_FLEXIBLE_BOOKING => 'Flexible booking',
            self::TYPE_STANDARD_M_F => 'Standard (M-F)',
            //self::TYPE_STANDARD_M_F_SAT => 'Standard (M-F) + Sat',
            //self::TYPE_SATURDAY_ONLY => 'Saturday Only',
            //self::TYPE_NIGHTLY_RATE => 'Nightly Rate',
        );
        return $array;
    }

    public function getTypeName($typeId = null){
        $array = $this->getAllowedTypes();
        if(is_null($typeId)){
            if($this->getType()) {
                return array_key_exists($this->getType(), $array) ? $array[$this->getType()] : false;
            }else{
                return false;
            }
        }
        return array_key_exists($typeId, $array) ? $array[$typeId] : false;
    }
########################################################################################################################
    const CHILDREN_NOT_ALLOWED = 1;
    const CHILDREN_WELCOME = 2;
    const CHILDREN_NOT_RECOMMENDED = 3;
    const CHILDREN_AGE_RESTRICTIONS_APPLY = 4;

    public function getAllowedChildren(){
        $array = array(
            self::CHILDREN_NOT_ALLOWED => 'Children not allowed',
            self::CHILDREN_WELCOME => 'Children welcome',
            self::CHILDREN_NOT_RECOMMENDED => 'Not recommended for children',
            self::CHILDREN_AGE_RESTRICTIONS_APPLY => 'Children - age restrictions apply',
        );
        return $array;
    }

    public function getChildrenName($childrenId = null){
        $array = $this->getAllowedChildren();
        if(is_null($childrenId)){
            if($this->getChildren()) {
                return array_key_exists($this->getChildren(), $array) ? $array[$this->getChildren()] : false;
            }else{
                return false;
            }
        }
        return array_key_exists($childrenId, $array) ? $array[$childrenId] : false;
    }

    public function areChildrenAllowed(){
        if($this->getChildren() == self::CHILDREN_NOT_ALLOWED){
            return false;
        }

        return true;
    }
########################################################################################################################
    const INFANTS_NO_COT_AVAILABLE = 1;
    const INFANTS_COT_AVAILABLE = 2;
    const INFANTS_COT_HIGH_CHAIR_AVAILABLE = 3;
    const INFANTS_HIGH_CHAIR_ONLY_AVAILABLE = 4;
    const INFANTS_NOT_ALLOWED = 5;

    public function getAllowedInfants(){
        $array = array(
            self::INFANTS_NO_COT_AVAILABLE => 'Infants - No Cot available',
            self::INFANTS_COT_AVAILABLE => 'Infants - Cot available',
            self::INFANTS_COT_HIGH_CHAIR_AVAILABLE => 'Infants - Cot & High Chair available',
            self::INFANTS_HIGH_CHAIR_ONLY_AVAILABLE => 'Infants - High Chair only available',
            self::INFANTS_NOT_ALLOWED => 'Infants - Not allowed',
        );
        return $array;
    }

    public function getInfantsName($infantsId = null){
        $array = $this->getAllowedInfants();
        if(is_null($infantsId)){
            if($this->getInfants()) {
                return array_key_exists($this->getInfants(), $array) ? $array[$this->getInfants()] : false;
            }else{
                return false;
            }
        }
        return array_key_exists($infantsId, $array) ? $array[$infantsId] : false;
    }

    public function getMaxInfantsTotal(){
        switch ($this->getInfants()) {
            case self::INFANTS_NO_COT_AVAILABLE:
                return 3; break;
            case self::INFANTS_COT_AVAILABLE:
                return 3; break;
            case self::INFANTS_COT_HIGH_CHAIR_AVAILABLE:
                return 3; break;
            case self::INFANTS_HIGH_CHAIR_ONLY_AVAILABLE:
                return 3; break;
            default:
                return 0;
        }
    }
########################################################################################################################
    const PETS_STRICTLY_NOT_ALLOWED = 1;
    const PETS_ALLOWED_BY_SPECIAL_REQUEST_ONLY = 2;
    const PETS_WELCOME_MAX_1_DOG = 3;
    const PETS_WELCOME_MAX_2_DOGS = 4;
    const PETS_WELCOME_MAX_3_DOGS = 5;
    const PETS_WELCOME_MORE_THAN_3_CONTRACT_US_UPON_BOOKING = 6;

    public function getAllowedPets(){
        $array = array(
            self::PETS_STRICTLY_NOT_ALLOWED => 'strictly no pets allowed',
            self::PETS_ALLOWED_BY_SPECIAL_REQUEST_ONLY => 'pets allowed by special request only',
            self::PETS_WELCOME_MAX_1_DOG => 'pets welcome (max. 1 dog)',
            self::PETS_WELCOME_MAX_2_DOGS => 'pets welcome (max. 2 dogs)',
            self::PETS_WELCOME_MAX_3_DOGS => 'pets welcome (max. 3 dogs)',
            self::PETS_WELCOME_MORE_THAN_3_CONTRACT_US_UPON_BOOKING => 'pets welcome (more than 3 contact us upon booking)',
        );
        return $array;
    }

    public function isPetsAllowed(){
        if(
            $this->getPets() == self::PETS_WELCOME_MAX_1_DOG ||
            $this->getPets() == self::PETS_WELCOME_MAX_2_DOGS ||
            $this->getPets() == self::PETS_WELCOME_MAX_3_DOGS ||
            $this->getPets() == self::PETS_WELCOME_MORE_THAN_3_CONTRACT_US_UPON_BOOKING
        ){
            return true;
        }

        return false;
    }

    public function getPetsName($petsId = null){
        $array = $this->getAllowedPets();
        if(is_null($petsId)){
            if($this->getPets()) {
                return array_key_exists($this->getPets(), $array) ? $array[$this->getPets()] : false;
            }else{
                return false;
            }
        }
        return array_key_exists($petsId, $array) ? $array[$petsId] : false;
    }

    public function getMaxPetsTotal(){
        switch ($this->getPets()) {
            case self::PETS_WELCOME_MAX_1_DOG:
                return 1; break;
            case self::PETS_WELCOME_MAX_2_DOGS:
                return 2; break;
            case self::PETS_WELCOME_MAX_3_DOGS:
                return 3; break;
            case self::PETS_WELCOME_MORE_THAN_3_CONTRACT_US_UPON_BOOKING:
                return 10; break;
            default:
                return 0;
        }
    }

########################################################################################################################
    const NOTICE_PERIOD_UP_TO_3PM_TODAY = 1;
    const NOTICE_PERIOD_UP_TO_10AM_TODAY = 2;
    const NOTICE_PERIOD_UP_TO_8PM_DAY_BEFORE = 3;
    const NOTICE_PERIOD_NEXT_CHANGE_OVER_OR_8PM_NIGHT_BEFORE = 4;
    const NOTICE_PERIOD_MIN_2_DAYS_NOTICE = 5;
    const NOTICE_PERIOD_MIN_3_DAYS_NOTICE = 6;
    const NOTICE_PERIOD_MIN_4_DAYS_NOTICE = 7;
    const NOTICE_PERIOD_MIN_5_DAYS_NOTICE = 8;

    public function getAllowedNoticePeriods(){
        $array = array(
            self::NOTICE_PERIOD_UP_TO_3PM_TODAY => 'Up to 3pm today',
            self::NOTICE_PERIOD_UP_TO_10AM_TODAY => 'Up to 10am today',
            self::NOTICE_PERIOD_UP_TO_8PM_DAY_BEFORE => 'Up to 8pm day before',
            self::NOTICE_PERIOD_NEXT_CHANGE_OVER_OR_8PM_NIGHT_BEFORE => 'Next change over or 8pm night before',
            self::NOTICE_PERIOD_MIN_2_DAYS_NOTICE => 'min 2 days notice',
            self::NOTICE_PERIOD_MIN_3_DAYS_NOTICE => 'min 3 days notice',
            self::NOTICE_PERIOD_MIN_4_DAYS_NOTICE => 'min 4 days notice',
            self::NOTICE_PERIOD_MIN_5_DAYS_NOTICE => 'min 5 days notice',
        );
        return $array;
    }

    public function getNoticePeriodsName($noticePeriodId = null){
        $array = $this->getAllowedNoticePeriods();
        if(is_null($noticePeriodId)){
            if($this->getNoticePeriod()) {
                return array_key_exists($this->getNoticePeriod(), $array) ? $array[$this->getNoticePeriod()] : false;
            }else{
                return false;
            }
        }
        return array_key_exists($noticePeriodId, $array) ? $array[$noticePeriodId] : false;
    }

    public function isActiveEstate(){
        if($this->getHideFromDisplay() != Estate::HIDE_FROM_DISPLAY_VISIBLE){
            return false;
        }

        return true;
    }

    public function __construct() {
        $this->estateFeatures = new ArrayCollection();
        $this->categories = new ArrayCollection();
        $this->photos = new ArrayCollection();
    }

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToMany(targetEntity="Category")
     * @ORM\JoinTable(name="estates_categories",
     *      joinColumns={@ORM\JoinColumn(name="estate_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="estate_category_id", referencedColumnName="id")}
     *      )
     */
    protected $categories;

    /**
     * @ORM\ManyToOne(targetEntity="Category")
     * @ORM\JoinColumn(name="default_category_id", referencedColumnName="id", nullable=true, onDelete="SET NULL")
     */
    protected $defaultCategory;

    /**
     * @ORM\ManyToOne(targetEntity="OwnerBookingsCategory")
     * @ORM\JoinColumn(name="owner_bookings_category_id", referencedColumnName="id", nullable=true, onDelete="SET NULL")
     */
    protected $ownerBookingsCategory;

    /**
     * @ORM\Column(name="is_category_featured", type="boolean")
     */
    protected $isCategoryFeatured = false;

    /**
     * @ORM\Column(name="name", type="string", length=255, nullable=true)
     */
    protected $name;

    /**
     * @ORM\Column(name="short_name", type="string", length=12, nullable=true)
     */
    protected $shortName;

    /**
     * @ORM\Column(name="seo_description", type="string", length=512, nullable=true)
     */
    protected $seoDescription;

    /**
     * @ORM\Column(name="hide_from_display", type="integer", nullable=true)
     */
    protected $hideFromDisplay;

    /**
     * @ORM\Column(name="special_category", type="integer", nullable=true)
     */
    protected $specialCategory;

    /**
     * @ORM\Column(name="type", type="integer", nullable=true)
     */
    protected $type;

    /**
     * @ORM\Column(name="sleeps", type="integer", nullable=true)
     */
    protected $sleeps;

    /**
     * @ORM\Column(name="minimum_guests", type="integer", nullable=true)
     */
    protected $minimumGuests;

    /**
     * @ORM\Column(name="room_layout", type="string", length=255, nullable=true)
     */
    protected $roomLayout;

    /**
     * @ORM\Column(name="children", type="integer", nullable=true)
     */
    protected $children;

    /**
     * @ORM\Column(name="infants", type="integer", nullable=true)
     */
    protected $infants;

    /**
     * @ORM\Column(name="pets", type="integer", nullable=true)
     */
    protected $pets;

    /**
     * @ORM\Column(name="check_in_time", type="time", nullable=true)
     */
    protected $checkInTime;

    /**
     * @ORM\Column(name="check_out_time", type="time", nullable=true)
     */
    protected $checkOutTime;

    /**
     * @ORM\Column(name="discount_period", type="integer", nullable=true)
     */
    protected $discountPeriod;

    /**
     * @ORM\Column(name="discount_value", type="decimal", scale=2, nullable=true)
     */
    protected $discountValue;

    /**
     * @ORM\Column(name="notice_period", type="integer", nullable=true)
     */
    protected $noticePeriod;

    /**
     * @ORM\Column(name="key_safe_code", type="string", length=255, nullable=true)
     */
    protected $keySafeCode;

    /**
     * @ORM\Column(name="address", type="string", length=255, nullable=true)
     */
    protected $address;

    /**
     * @ORM\Column(name="post_code", type="string", length=255, nullable=true)
     */
    protected $postcode;

    /**
     * @ORM\Column(name="latitude", type="string", length=255, nullable=true)
     */
    protected $lat;

    /**
     * @ORM\Column(name="longitude", type="string", length=255, nullable=true)
     */
    protected $lon;

    /**
     * @ORM\ManyToMany(targetEntity="EstateFeature")
     * @ORM\JoinTable(name="estates_features",
     *      joinColumns={@ORM\JoinColumn(name="estate_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="estate_feature_id", referencedColumnName="id")}
     *      )
     */
    protected $estateFeatures;

    /**
     * @ORM\Column(name="star_rating_luxury", type="integer", nullable=true)
     */
    protected $starRatingLuxury;

    /**
     * @ORM\Column(name="star_rating_heritage", type="integer", nullable=true)
     */
    protected $starRatingHeritage;

    /**
     * @ORM\Column(name="star_rating_unique", type="integer", nullable=true)
     */
    protected $starRatingUnique;

    /**
     * @ORM\Column(name="star_rating_green", type="integer", nullable=true)
     */
    protected $starRatingGreen;

    /**
     * @ORM\Column(name="star_rating_price", type="integer", nullable=true)
     */
    protected $starRatingPrice;

    /**
     * @ORM\Column(name="landlord_address", type="string", length=255, nullable=true)
     */
    protected $landlordAddress;

    /**
     * @ORM\ManyToMany(targetEntity="UTT\UserBundle\Entity\User")
     * @ORM\JoinTable(name="estates_owner_users",
     *      joinColumns={@ORM\JoinColumn(name="estate_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="estate_owner_user_id", referencedColumnName="id")}
     *      )
     */
    protected $ownerUsers;

    /**
     * @ORM\ManyToMany(targetEntity="UTT\UserBundle\Entity\User")
     * @ORM\JoinTable(name="estates_cleaner_users",
     *      joinColumns={@ORM\JoinColumn(name="estate_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="estate_cleaner_user_id", referencedColumnName="id")}
     *      )
     */
    protected $cleanerUsers;

    /**
     * @ORM\ManyToMany(targetEntity="UTT\UserBundle\Entity\User")
     * @ORM\JoinTable(name="estates_administrator_users",
     *      joinColumns={@ORM\JoinColumn(name="estate_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="estate_administrator_user_id", referencedColumnName="id")}
     *      )
     */
    protected $administratorUsers;

    /**
     * @ORM\Column(name="bank_account_number", type="string", length=26, nullable=true)
     */
    protected $bankAccountNumber;

    /**
     * @ORM\Column(name="bank_iban", type="string", length=255, nullable=true)
     */
    protected $bankIBAN;

    /**
     * @ORM\Column(name="bank_bic", type="string", length=255, nullable=true)
     */
    protected $bankBIC;

    /**
     * @ORM\Column(name="phone1", type="string", length=255, nullable=true)
     */
    protected $phone1;

    /**
     * @ORM\Column(name="phone1_name", type="string", length=255, nullable=true)
     */
    protected $phone1Name;

    /**
     * @ORM\Column(name="phone2", type="string", length=255, nullable=true)
     */
    protected $phone2;

    /**
     * @ORM\Column(name="phone2_name", type="string", length=255, nullable=true)
     */
    protected $phone2Name;

    /**
     * @ORM\Column(name="phone3", type="string", length=255, nullable=true)
     */
    protected $phone3;

    /**
     * @ORM\Column(name="phone3_name", type="string", length=255, nullable=true)
     */
    protected $phone3Name;

    /**
     * @ORM\Column(name="phone4", type="string", length=255, nullable=true)
     */
    protected $phone4;

    /**
     * @ORM\Column(name="phone4_name", type="string", length=255, nullable=true)
     */
    protected $phone4Name;

    /**
     * @ORM\Column(name="phone5", type="string", length=255, nullable=true)
     */
    protected $phone5;

    /**
     * @ORM\Column(name="phone5_name", type="string", length=255, nullable=true)
     */
    protected $phone5Name;

    /**
     * @ORM\Column(name="phone6", type="string", length=255, nullable=true)
     */
    protected $phone6;

    /**
     * @ORM\Column(name="phone6_name", type="string", length=255, nullable=true)
     */
    protected $phone6Name;

    /**
     * @ORM\Column(name="utt_commission_rate", type="integer", nullable=true)
     */
    protected $uttCommissionRate;

    /**
     * @ORM\Column(name="utt_booking_fee", type="integer", nullable=true)
     */
    protected $uttBookingFee;

    /**
     * @ORM\Column(name="cheques_to", type="string", length=255, nullable=true)
     */
    protected $chequesTo;

    /**
     * @ORM\ManyToOne(targetEntity="UTT\EstateBundle\Entity\Location")
     * @ORM\JoinColumn(name="location_id", referencedColumnName="id", nullable=true, onDelete="SET NULL")
     */
    protected $location;

    /**
     * @ORM\Column(name="location_text", type="string", length=255, nullable=true)
     */
    protected $locationText;

    /**
     * @ORM\Column(name="booking_notes", type="text", nullable=true)
     */
    protected $bookingNotes;

    /**
     * @ORM\Column(name="accommodation_subtitle", type="text", nullable=true)
     */
    protected $accommodationSubtitle;

    /**
     * @ORM\Column(name="utt_admin_notes", type="text", nullable=true)
     */
    protected $uttAdminNotes;

    /**
     * @ORM\OneToMany(targetEntity="Photo", mappedBy="estate")
     */
    protected $photos;

    /**
     * @ORM\Column(name="is_just_launched", type="boolean")
     */
    protected $isJustLaunched = false;

    /**
     * @ORM\Column(name="text_accommodation", type="text", nullable=true)
     */
    protected $textAccommodation;

    /**
     * @ORM\Column(name="text_in_detail", type="text", nullable=true)
     */
    protected $textInDetail;

    /**
     * @ORM\Column(name="text_sustainability", type="text", nullable=true)
     */
    protected $textSustainability;

    /**
     * @ORM\Column(name="text_welsh_description", type="text", nullable=true)
     */
    protected $textWelshDescription;

    /**
     * @ORM\Column(name="text_local_area", type="text", nullable=true)
     */
    protected $textLocalArea;

    /**
     * @ORM\Column(name="text_travel_info", type="text", nullable=true)
     */
    protected $textTravelInfo;

    /**
     * @ORM\Column(name="text_customer_comments", type="text", nullable=true)
     */
    protected $textCustomerComments;

    /**
     * @ORM\Column(name="text_arrival_instructions_directions", type="text", nullable=true)
     */
    protected $textArrivalInstructionsDirections;

    /**
     * @ORM\Column(name="text_arrival_instructions_property_specific", type="text", nullable=true)
     */
    protected $textArrivalInstructionsPropertySpecific;

    /**
     * @ORM\Column(name="lowest_price", type="decimal", scale=2, nullable=true)
     */
    protected $lowestPrice;

    /**
     * @ORM\Column(name="highest_price", type="decimal", scale=2, nullable=true)
     */
    protected $highestPrice;

    /**
     * @ORM\Column(name="external_id", type="integer", nullable=true)
     */
    protected $externalId;

    /**
     * @ORM\ManyToOne(targetEntity="UTT\ReservationBundle\Entity\PricingCategory")
     * @ORM\JoinColumn(name="pricing_category_id", referencedColumnName="id", nullable=true, onDelete="SET NULL")
     */
    protected $pricingCategory;

    /**
     * @ORM\ManyToOne(targetEntity="Photo")
     * @ORM\JoinColumn(name="photo_main", referencedColumnName="id", nullable=true, onDelete="SET NULL")
     */
    protected $photoMain;
    public $photoMainContainer;

    /**
     * @ORM\ManyToMany(targetEntity="UTT\ReservationBundle\Entity\PricingSeason")
     * @ORM\JoinTable(name="estates_min_7_nights_pricing_seasons",
     *      joinColumns={@ORM\JoinColumn(name="estate_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="pricing_season_id", referencedColumnName="id")}
     *      )
     */
    protected $min7NightsPricingSeasons;

    /**
     * @ORM\Column(name="is_saturday_only", type="boolean")
     */
    protected $isSaturdayOnly = false;

    /**
     * @ORM\ManyToMany(targetEntity="Estate")
     * @ORM\JoinTable(name="estates_near_by_estates",
     *      joinColumns={@ORM\JoinColumn(name="estate_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="near_by_estate_id", referencedColumnName="id")}
     *      )
     */
    protected $nearByEstates;

    /**
     * @ORM\Column(name="airbnb_calendar", type="string", length=255, nullable=true)
     */
    protected $airbnbCalendar;

    /**
     * @ORM\Column(name="airbnb_export", type="boolean")
     */
    protected $airbnbExport = false;

    /**
     * @ORM\Column(name="airbnb_export_password", type="string", length=255, nullable=true)
     */
    protected $airbnbExportPassword;

    /**
     * @ORM\Column(name="visits", type="integer", nullable=true)
     */
    protected $visits;

    /**
     * @ORM\Column(name="latest_visits", type="integer", nullable=true)
     */
    protected $latestVisits;

    /**
     * @ORM\Column(name="is_multiproperty", type="boolean")
     */
    protected $isMultiproperty = false;

    /**
     * @ORM\ManyToMany(targetEntity="Estate")
     * @ORM\JoinTable(name="estates_sub_estates",
     *      joinColumns={@ORM\JoinColumn(name="estate_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="sub_estate_id", referencedColumnName="id")}
     *      )
     */
    protected $subEstates;

    public function __toString(){
        if($this->getName())
            return $this->getName();
        return '';
    }

    public function isWifiAvailable(){
        /** @var EstateFeature $estateFeature */
        foreach($this->getEstateFeatures() as $estateFeature){
            if($estateFeature->getId() == 16) return true;
        }

        return false;
    }

    public function getManagingUser(){
        if($this->getOwnerUsers()){
            $ownerUsers = $this->getOwnerUsers();
            if($ownerUsers instanceof Collection && count($ownerUsers) > 0){
                if($ownerUsers[0] instanceof User){
                    return $ownerUsers[0];
                }
            }
        }

        if($this->getAdministratorUsers()){
            $administratorUsers = $this->getAdministratorUsers();
            if($administratorUsers instanceof Collection && count($administratorUsers) > 0){
                if($administratorUsers[0] instanceof User){
                    return $administratorUsers[0];
                }
            }
        }

        return false;
    }

    public function getRandomPhoto(){
        if($this->getPhotoMain() instanceof Photo){
            return $this->getPhotoMain();
        }

        if($this->getPhotos()){
            $photos = $this->getPhotos();
            if($photos instanceof Collection && count($photos) > 0){
                if($photos[0] instanceof Photo){
                    return $photos[0];
                }
            }
        }
        return false;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Estate
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set shortName
     *
     * @param string $shortName
     * @return Estate
     */
    public function setShortName($shortName)
    {
        $this->shortName = $shortName;

        return $this;
    }

    /**
     * Get shortName
     *
     * @return string 
     */
    public function getShortName()
    {
        return $this->shortName;
    }

    /**
     * Set hideFromDisplay
     *
     * @param integer $hideFromDisplay
     * @return Estate
     */
    public function setHideFromDisplay($hideFromDisplay)
    {
        $this->hideFromDisplay = $hideFromDisplay;

        return $this;
    }

    /**
     * Get hideFromDisplay
     *
     * @return integer 
     */
    public function getHideFromDisplay()
    {
        return $this->hideFromDisplay;
    }

    /**
     * Set type
     *
     * @param integer $type
     * @return Estate
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return integer 
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set sleeps
     *
     * @param integer $sleeps
     * @return Estate
     */
    public function setSleeps($sleeps)
    {
        $this->sleeps = $sleeps;

        return $this;
    }

    /**
     * Get sleeps
     *
     * @return integer 
     */
    public function getSleeps()
    {
        return $this->sleeps;
    }

    /**
     * Set minimumGuests
     *
     * @param integer $minimumGuests
     * @return Estate
     */
    public function setMinimumGuests($minimumGuests)
    {
        $this->minimumGuests = $minimumGuests;

        return $this;
    }

    /**
     * Get minimumGuests
     *
     * @return integer 
     */
    public function getMinimumGuests()
    {
        return $this->minimumGuests;
    }

    /**
     * Set children
     *
     * @param integer $children
     * @return Estate
     */
    public function setChildren($children)
    {
        $this->children = $children;

        return $this;
    }

    /**
     * Get children
     *
     * @return integer 
     */
    public function getChildren()
    {
        return $this->children;
    }

    /**
     * Set infants
     *
     * @param integer $infants
     * @return Estate
     */
    public function setInfants($infants)
    {
        $this->infants = $infants;

        return $this;
    }

    /**
     * Get infants
     *
     * @return integer 
     */
    public function getInfants()
    {
        return $this->infants;
    }

    /**
     * Set pets
     *
     * @param integer $pets
     * @return Estate
     */
    public function setPets($pets)
    {
        $this->pets = $pets;

        return $this;
    }

    /**
     * Get pets
     *
     * @return integer 
     */
    public function getPets()
    {
        return $this->pets;
    }

    /**
     * Set checkInTime
     *
     * @param \DateTime $checkInTime
     * @return Estate
     */
    public function setCheckInTime($checkInTime)
    {
        $this->checkInTime = $checkInTime;

        return $this;
    }

    /**
     * Get checkInTime
     *
     * @return \DateTime 
     */
    public function getCheckInTime()
    {
        return $this->checkInTime;
    }

    /**
     * Set checkOutTime
     *
     * @param \DateTime $checkOutTime
     * @return Estate
     */
    public function setCheckOutTime($checkOutTime)
    {
        $this->checkOutTime = $checkOutTime;

        return $this;
    }

    /**
     * Get checkOutTime
     *
     * @return \DateTime 
     */
    public function getCheckOutTime()
    {
        return $this->checkOutTime;
    }

    /**
     * Set discountPeriod
     *
     * @param integer $discountPeriod
     * @return Estate
     */
    public function setDiscountPeriod($discountPeriod)
    {
        $this->discountPeriod = $discountPeriod;

        return $this;
    }

    /**
     * Get discountPeriod
     *
     * @return integer 
     */
    public function getDiscountPeriod()
    {
        return $this->discountPeriod;
    }

    /**
     * Set discountValue
     *
     * @param string $discountValue
     * @return Estate
     */
    public function setDiscountValue($discountValue)
    {
        $this->discountValue = $discountValue;

        return $this;
    }

    /**
     * Get discountValue
     *
     * @return string 
     */
    public function getDiscountValue()
    {
        return $this->discountValue;
    }

    /**
     * Set noticePeriod
     *
     * @param integer $noticePeriod
     * @return Estate
     */
    public function setNoticePeriod($noticePeriod)
    {
        $this->noticePeriod = $noticePeriod;

        return $this;
    }

    /**
     * Get noticePeriod
     *
     * @return integer 
     */
    public function getNoticePeriod()
    {
        return $this->noticePeriod;
    }



    /**
     * Set address
     *
     * @param string $address
     * @return Estate
     */
    public function setAddress($address)
    {
        $this->address = $address;

        return $this;
    }

    /**
     * Get address
     *
     * @return string 
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * Set postcode
     *
     * @param string $postcode
     * @return Estate
     */
    public function setPostcode($postcode)
    {
        $this->postcode = $postcode;

        return $this;
    }

    /**
     * Get postcode
     *
     * @return string 
     */
    public function getPostcode()
    {
        return $this->postcode;
    }

    /**
     * Set landlordAddress
     *
     * @param string $landlordAddress
     * @return Estate
     */
    public function setLandlordAddress($landlordAddress)
    {
        $this->landlordAddress = $landlordAddress;

        return $this;
    }

    /**
     * Get landlordAddress
     *
     * @return string 
     */
    public function getLandlordAddress()
    {
        return $this->landlordAddress;
    }

    /**
     * Set bankAccountNumber
     *
     * @param string $bankAccountNumber
     * @return Estate
     */
    public function setBankAccountNumber($bankAccountNumber)
    {
        $this->bankAccountNumber = $bankAccountNumber;

        return $this;
    }

    /**
     * Get bankAccountNumber
     *
     * @return string 
     */
    public function getBankAccountNumber()
    {
        return $this->bankAccountNumber;
    }

    /**
     * Set phone1
     *
     * @param string $phone1
     * @return Estate
     */
    public function setPhone1($phone1)
    {
        $this->phone1 = $phone1;

        return $this;
    }

    /**
     * Get phone1
     *
     * @return string 
     */
    public function getPhone1()
    {
        return $this->phone1;
    }

    /**
     * Set phone2
     *
     * @param string $phone2
     * @return Estate
     */
    public function setPhone2($phone2)
    {
        $this->phone2 = $phone2;

        return $this;
    }

    /**
     * Get phone2
     *
     * @return string 
     */
    public function getPhone2()
    {
        return $this->phone2;
    }

    /**
     * Set phone3
     *
     * @param string $phone3
     * @return Estate
     */
    public function setPhone3($phone3)
    {
        $this->phone3 = $phone3;

        return $this;
    }

    /**
     * Get phone3
     *
     * @return string 
     */
    public function getPhone3()
    {
        return $this->phone3;
    }

    /**
     * Set phone4
     *
     * @param string $phone4
     * @return Estate
     */
    public function setPhone4($phone4)
    {
        $this->phone4 = $phone4;

        return $this;
    }

    /**
     * Get phone4
     *
     * @return string 
     */
    public function getPhone4()
    {
        return $this->phone4;
    }

    /**
     * Set phone5
     *
     * @param string $phone5
     * @return Estate
     */
    public function setPhone5($phone5)
    {
        $this->phone5 = $phone5;

        return $this;
    }

    /**
     * Get phone5
     *
     * @return string 
     */
    public function getPhone5()
    {
        return $this->phone5;
    }

    /**
     * Set phone6
     *
     * @param string $phone6
     * @return Estate
     */
    public function setPhone6($phone6)
    {
        $this->phone6 = $phone6;

        return $this;
    }

    /**
     * Get phone6
     *
     * @return string 
     */
    public function getPhone6()
    {
        return $this->phone6;
    }

    /**
     * Set uttCommissionRate
     *
     * @param integer $uttCommissionRate
     * @return Estate
     */
    public function setUttCommissionRate($uttCommissionRate)
    {
        $this->uttCommissionRate = $uttCommissionRate;

        return $this;
    }

    /**
     * Get uttCommissionRate
     *
     * @return integer 
     */
    public function getUttCommissionRate()
    {
        return $this->uttCommissionRate;
    }

    /**
     * Set chequesTo
     *
     * @param string $chequesTo
     * @return Estate
     */
    public function setChequesTo($chequesTo)
    {
        $this->chequesTo = $chequesTo;

        return $this;
    }

    /**
     * Get chequesTo
     *
     * @return string 
     */
    public function getChequesTo()
    {
        return $this->chequesTo;
    }

    /**
     * Set bookingNotes
     *
     * @param string $bookingNotes
     * @return Estate
     */
    public function setBookingNotes($bookingNotes)
    {
        $this->bookingNotes = $bookingNotes;

        return $this;
    }

    /**
     * Get bookingNotes
     *
     * @return string 
     */
    public function getBookingNotes()
    {
        return $this->bookingNotes;
    }

    /**
     * Set accommodationSubtitle
     *
     * @param string $accommodationSubtitle
     * @return Estate
     */
    public function setAccommodationSubtitle($accommodationSubtitle)
    {
        $this->accommodationSubtitle = $accommodationSubtitle;

        return $this;
    }

    /**
     * Get accommodationSubtitle
     *
     * @return string 
     */
    public function getAccommodationSubtitle()
    {
        return $this->accommodationSubtitle;
    }

    /**
     * Set uttAdminNotes
     *
     * @param string $uttAdminNotes
     * @return Estate
     */
    public function setUttAdminNotes($uttAdminNotes)
    {
        $this->uttAdminNotes = $uttAdminNotes;

        return $this;
    }

    /**
     * Get uttAdminNotes
     *
     * @return string 
     */
    public function getUttAdminNotes()
    {
        return $this->uttAdminNotes;
    }

    /**
     * Set lat
     *
     * @param string $lat
     * @return Estate
     */
    public function setLat($lat)
    {
        $this->lat = $lat;

        return $this;
    }

    /**
     * Get lat
     *
     * @return string 
     */
    public function getLat()
    {
        return $this->lat;
    }

    /**
     * Set lon
     *
     * @param string $lon
     * @return Estate
     */
    public function setLon($lon)
    {
        $this->lon = $lon;

        return $this;
    }

    /**
     * Get lon
     *
     * @return string 
     */
    public function getLon()
    {
        return $this->lon;
    }

    /**
     * Set roomLayout
     *
     * @param string $roomLayout
     * @return Estate
     */
    public function setRoomLayout($roomLayout)
    {
        $this->roomLayout = $roomLayout;

        return $this;
    }

    /**
     * Get roomLayout
     *
     * @return string 
     */
    public function getRoomLayout()
    {
        return $this->roomLayout;
    }

    /**
     * Set keySafeCode
     *
     * @param string $keySafeCode
     * @return Estate
     */
    public function setKeySafeCode($keySafeCode)
    {
        $this->keySafeCode = $keySafeCode;

        return $this;
    }

    /**
     * Get keySafeCode
     *
     * @return string 
     */
    public function getKeySafeCode()
    {
        return $this->keySafeCode;
    }

    /**
     * Add estateFeatures
     *
     * @param \UTT\EstateBundle\Entity\EstateFeature $estateFeatures
     * @return Estate
     */
    public function addEstateFeature(\UTT\EstateBundle\Entity\EstateFeature $estateFeatures)
    {
        $this->estateFeatures[] = $estateFeatures;

        return $this;
    }

    /**
     * Remove estateFeatures
     *
     * @param \UTT\EstateBundle\Entity\EstateFeature $estateFeatures
     */
    public function removeEstateFeature(\UTT\EstateBundle\Entity\EstateFeature $estateFeatures)
    {
        $this->estateFeatures->removeElement($estateFeatures);
    }

    /**
     * Get estateFeatures
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getEstateFeatures()
    {
        return $this->estateFeatures;
    }

    /**
     * Set starRatingLuxury
     *
     * @param integer $starRatingLuxury
     * @return Estate
     */
    public function setStarRatingLuxury($starRatingLuxury)
    {
        $this->starRatingLuxury = $starRatingLuxury;

        return $this;
    }

    /**
     * Get starRatingLuxury
     *
     * @return integer 
     */
    public function getStarRatingLuxury()
    {
        return $this->starRatingLuxury;
    }

    /**
     * Set starRatingHeritage
     *
     * @param integer $starRatingHeritage
     * @return Estate
     */
    public function setStarRatingHeritage($starRatingHeritage)
    {
        $this->starRatingHeritage = $starRatingHeritage;

        return $this;
    }

    /**
     * Get starRatingHeritage
     *
     * @return integer 
     */
    public function getStarRatingHeritage()
    {
        return $this->starRatingHeritage;
    }

    /**
     * Set starRatingUnique
     *
     * @param integer $starRatingUnique
     * @return Estate
     */
    public function setStarRatingUnique($starRatingUnique)
    {
        $this->starRatingUnique = $starRatingUnique;

        return $this;
    }

    /**
     * Get starRatingUnique
     *
     * @return integer 
     */
    public function getStarRatingUnique()
    {
        return $this->starRatingUnique;
    }

    /**
     * Set starRatingGreen
     *
     * @param integer $starRatingGreen
     * @return Estate
     */
    public function setStarRatingGreen($starRatingGreen)
    {
        $this->starRatingGreen = $starRatingGreen;

        return $this;
    }

    /**
     * Get starRatingGreen
     *
     * @return integer 
     */
    public function getStarRatingGreen()
    {
        return $this->starRatingGreen;
    }

    /**
     * Set starRatingPrice
     *
     * @param integer $starRatingPrice
     * @return Estate
     */
    public function setStarRatingPrice($starRatingPrice)
    {
        $this->starRatingPrice = $starRatingPrice;

        return $this;
    }

    /**
     * Get starRatingPrice
     *
     * @return integer 
     */
    public function getStarRatingPrice()
    {
        return $this->starRatingPrice;
    }

    /**
     * Set phone1Name
     *
     * @param string $phone1Name
     * @return Estate
     */
    public function setPhone1Name($phone1Name)
    {
        $this->phone1Name = $phone1Name;

        return $this;
    }

    /**
     * Get phone1Name
     *
     * @return string 
     */
    public function getPhone1Name()
    {
        return $this->phone1Name;
    }

    /**
     * Set phone2Name
     *
     * @param string $phone2Name
     * @return Estate
     */
    public function setPhone2Name($phone2Name)
    {
        $this->phone2Name = $phone2Name;

        return $this;
    }

    /**
     * Get phone2Name
     *
     * @return string 
     */
    public function getPhone2Name()
    {
        return $this->phone2Name;
    }

    /**
     * Set phone3Name
     *
     * @param string $phone3Name
     * @return Estate
     */
    public function setPhone3Name($phone3Name)
    {
        $this->phone3Name = $phone3Name;

        return $this;
    }

    /**
     * Get phone3Name
     *
     * @return string 
     */
    public function getPhone3Name()
    {
        return $this->phone3Name;
    }

    /**
     * Set phone4Name
     *
     * @param string $phone4Name
     * @return Estate
     */
    public function setPhone4Name($phone4Name)
    {
        $this->phone4Name = $phone4Name;

        return $this;
    }

    /**
     * Get phone4Name
     *
     * @return string 
     */
    public function getPhone4Name()
    {
        return $this->phone4Name;
    }

    /**
     * Set phone5Name
     *
     * @param string $phone5Name
     * @return Estate
     */
    public function setPhone5Name($phone5Name)
    {
        $this->phone5Name = $phone5Name;

        return $this;
    }

    /**
     * Get phone5Name
     *
     * @return string 
     */
    public function getPhone5Name()
    {
        return $this->phone5Name;
    }

    /**
     * Set phone6Name
     *
     * @param string $phone6Name
     * @return Estate
     */
    public function setPhone6Name($phone6Name)
    {
        $this->phone6Name = $phone6Name;

        return $this;
    }

    /**
     * Get phone6Name
     *
     * @return string 
     */
    public function getPhone6Name()
    {
        return $this->phone6Name;
    }

    /**
     * Add categories
     *
     * @param \UTT\EstateBundle\Entity\Category $categories
     * @return Estate
     */
    public function addCategory(\UTT\EstateBundle\Entity\Category $categories)
    {
        $this->categories[] = $categories;

        return $this;
    }

    /**
     * Remove categories
     *
     * @param \UTT\EstateBundle\Entity\Category $categories
     */
    public function removeCategory(\UTT\EstateBundle\Entity\Category $categories)
    {
        $this->categories->removeElement($categories);
    }

    /**
     * Get categories
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getCategories()
    {
        return $this->categories;
    }

    /**
     * Add photos
     *
     * @param \UTT\EstateBundle\Entity\Photo $photos
     * @return Estate
     */
    public function addPhoto(\UTT\EstateBundle\Entity\Photo $photos)
    {
        $this->photos[] = $photos;

        return $this;
    }

    /**
     * Remove photos
     *
     * @param \UTT\EstateBundle\Entity\Photo $photos
     */
    public function removePhoto(\UTT\EstateBundle\Entity\Photo $photos)
    {
        $this->photos->removeElement($photos);
    }

    /**
     * Get photos
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getPhotos()
    {
        return $this->photos;
    }

    /**
     * Set isJustLaunched
     *
     * @param boolean $isJustLaunched
     * @return Estate
     */
    public function setIsJustLaunched($isJustLaunched)
    {
        $this->isJustLaunched = $isJustLaunched;

        return $this;
    }

    /**
     * Get isJustLaunched
     *
     * @return boolean 
     */
    public function getIsJustLaunched()
    {
        return $this->isJustLaunched;
    }

    /**
     * Set textAccommodation
     *
     * @param string $textAccommodation
     * @return Estate
     */
    public function setTextAccommodation($textAccommodation)
    {
        $this->textAccommodation = $textAccommodation;

        return $this;
    }

    /**
     * Get textAccommodation
     *
     * @return string 
     */
    public function getTextAccommodation()
    {
        return $this->textAccommodation;
    }

    /**
     * Set textInDetail
     *
     * @param string $textInDetail
     * @return Estate
     */
    public function setTextInDetail($textInDetail)
    {
        $this->textInDetail = $textInDetail;

        return $this;
    }

    /**
     * Get textInDetail
     *
     * @return string 
     */
    public function getTextInDetail()
    {
        return $this->textInDetail;
    }

    /**
     * Set textSustainability
     *
     * @param string $textSustainability
     * @return Estate
     */
    public function setTextSustainability($textSustainability)
    {
        $this->textSustainability = $textSustainability;

        return $this;
    }

    /**
     * Get textSustainability
     *
     * @return string 
     */
    public function getTextSustainability()
    {
        return $this->textSustainability;
    }

    /**
     * Set textWelshDescription
     *
     * @param string $textWelshDescription
     * @return Estate
     */
    public function setTextWelshDescription($textWelshDescription)
    {
        $this->textWelshDescription = $textWelshDescription;

        return $this;
    }

    /**
     * Get textWelshDescription
     *
     * @return string 
     */
    public function getTextWelshDescription()
    {
        return $this->textWelshDescription;
    }

    /**
     * Set textLocalArea
     *
     * @param string $textLocalArea
     * @return Estate
     */
    public function setTextLocalArea($textLocalArea)
    {
        $this->textLocalArea = $textLocalArea;

        return $this;
    }

    /**
     * Get textLocalArea
     *
     * @return string 
     */
    public function getTextLocalArea()
    {
        return $this->textLocalArea;
    }

    /**
     * Set textTravelInfo
     *
     * @param string $textTravelInfo
     * @return Estate
     */
    public function setTextTravelInfo($textTravelInfo)
    {
        $this->textTravelInfo = $textTravelInfo;

        return $this;
    }

    /**
     * Get textTravelInfo
     *
     * @return string 
     */
    public function getTextTravelInfo()
    {
        return $this->textTravelInfo;
    }

    /**
     * Set textCustomerComments
     *
     * @param string $textCustomerComments
     * @return Estate
     */
    public function setTextCustomerComments($textCustomerComments)
    {
        $this->textCustomerComments = $textCustomerComments;

        return $this;
    }

    /**
     * Get textCustomerComments
     *
     * @return string 
     */
    public function getTextCustomerComments()
    {
        return $this->textCustomerComments;
    }

    /**
     * Set textArrivalInstructionsDirections
     *
     * @param string $textArrivalInstructionsDirections
     * @return Estate
     */
    public function setTextArrivalInstructionsDirections($textArrivalInstructionsDirections)
    {
        $this->textArrivalInstructionsDirections = $textArrivalInstructionsDirections;

        return $this;
    }

    /**
     * Get textArrivalInstructionsDirections
     *
     * @return string 
     */
    public function getTextArrivalInstructionsDirections()
    {
        return $this->textArrivalInstructionsDirections;
    }

    /**
     * Set textArrivalInstructionsPropertySpecific
     *
     * @param string $textArrivalInstructionsPropertySpecific
     * @return Estate
     */
    public function setTextArrivalInstructionsPropertySpecific($textArrivalInstructionsPropertySpecific)
    {
        $this->textArrivalInstructionsPropertySpecific = $textArrivalInstructionsPropertySpecific;

        return $this;
    }

    /**
     * Get textArrivalInstructionsPropertySpecific
     *
     * @return string 
     */
    public function getTextArrivalInstructionsPropertySpecific()
    {
        return $this->textArrivalInstructionsPropertySpecific;
    }

    /**
     * Set location
     *
     * @param \UTT\EstateBundle\Entity\Location $location
     * @return Estate
     */
    public function setLocation(\UTT\EstateBundle\Entity\Location $location = null)
    {
        $this->location = $location;

        return $this;
    }

    /**
     * Get location
     *
     * @return \UTT\EstateBundle\Entity\Location 
     */
    public function getLocation()
    {
        return $this->location;
    }

    /**
     * Set locationText
     *
     * @param string $locationText
     * @return Estate
     */
    public function setLocationText($locationText)
    {
        $this->locationText = $locationText;

        return $this;
    }

    /**
     * Get locationText
     *
     * @return string 
     */
    public function getLocationText()
    {
        return $this->locationText;
    }

    /**
     * Set lowestPrice
     *
     * @param string $lowestPrice
     * @return Estate
     */
    public function setLowestPrice($lowestPrice)
    {
        $this->lowestPrice = $lowestPrice;

        return $this;
    }

    /**
     * Get lowestPrice
     *
     * @return string 
     */
    public function getLowestPrice()
    {
        return $this->lowestPrice;
    }

    /**
     * Add ownerUsers
     *
     * @param \UTT\UserBundle\Entity\User $ownerUsers
     * @return Estate
     */
    public function addOwnerUser(\UTT\UserBundle\Entity\User $ownerUsers)
    {
        $this->ownerUsers[] = $ownerUsers;

        return $this;
    }

    /**
     * Remove ownerUsers
     *
     * @param \UTT\UserBundle\Entity\User $ownerUsers
     */
    public function removeOwnerUser(\UTT\UserBundle\Entity\User $ownerUsers)
    {
        $this->ownerUsers->removeElement($ownerUsers);
    }

    /**
     * Get ownerUsers
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getOwnerUsers()
    {
        return $this->ownerUsers;
    }

    /**
     * Add cleanerUsers
     *
     * @param \UTT\UserBundle\Entity\User $cleanerUsers
     * @return Estate
     */
    public function addCleanerUser(\UTT\UserBundle\Entity\User $cleanerUsers)
    {
        $this->cleanerUsers[] = $cleanerUsers;

        return $this;
    }

    /**
     * Remove cleanerUsers
     *
     * @param \UTT\UserBundle\Entity\User $cleanerUsers
     */
    public function removeCleanerUser(\UTT\UserBundle\Entity\User $cleanerUsers)
    {
        $this->cleanerUsers->removeElement($cleanerUsers);
    }

    /**
     * Get cleanerUsers
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getCleanerUsers()
    {
        return $this->cleanerUsers;
    }

    /**
     * Set externalId
     *
     * @param integer $externalId
     * @return Estate
     */
    public function setExternalId($externalId)
    {
        $this->externalId = $externalId;

        return $this;
    }

    /**
     * Get externalId
     *
     * @return integer 
     */
    public function getExternalId()
    {
        return $this->externalId;
    }

    /**
     * Set pricingCategory
     *
     * @param \UTT\ReservationBundle\Entity\PricingCategory $pricingCategory
     * @return Estate
     */
    public function setPricingCategory(\UTT\ReservationBundle\Entity\PricingCategory $pricingCategory = null)
    {
        $this->pricingCategory = $pricingCategory;

        return $this;
    }

    /**
     * Get pricingCategory
     *
     * @return \UTT\ReservationBundle\Entity\PricingCategory 
     */
    public function getPricingCategory()
    {
        return $this->pricingCategory;
    }

    /**
     * Set bankIBAN
     *
     * @param string $bankIBAN
     * @return Estate
     */
    public function setBankIBAN($bankIBAN)
    {
        $this->bankIBAN = $bankIBAN;

        return $this;
    }

    /**
     * Get bankIBAN
     *
     * @return string 
     */
    public function getBankIBAN()
    {
        return $this->bankIBAN;
    }

    /**
     * Set bankBIC
     *
     * @param string $bankBIC
     * @return Estate
     */
    public function setBankBIC($bankBIC)
    {
        $this->bankBIC = $bankBIC;

        return $this;
    }

    /**
     * Get bankBIC
     *
     * @return string 
     */
    public function getBankBIC()
    {
        return $this->bankBIC;
    }

    /**
     * Set photoMain
     *
     * @param \UTT\EstateBundle\Entity\Photo $photoMain
     * @return Estate
     */
    public function setPhotoMain(\UTT\EstateBundle\Entity\Photo $photoMain = null)
    {
        $this->photoMain = $photoMain;

        return $this;
    }

    /**
     * Get photoMain
     *
     * @return \UTT\EstateBundle\Entity\Photo 
     */
    public function getPhotoMain()
    {
        return $this->photoMain;
    }

    /**
     * Add min7NightsPricingSeasons
     *
     * @param \UTT\ReservationBundle\Entity\PricingSeason $min7NightsPricingSeasons
     * @return Estate
     */
    public function addMin7NightsPricingSeason(\UTT\ReservationBundle\Entity\PricingSeason $min7NightsPricingSeasons)
    {
        $this->min7NightsPricingSeasons[] = $min7NightsPricingSeasons;

        return $this;
    }

    /**
     * Remove min7NightsPricingSeasons
     *
     * @param \UTT\ReservationBundle\Entity\PricingSeason $min7NightsPricingSeasons
     */
    public function removeMin7NightsPricingSeason(\UTT\ReservationBundle\Entity\PricingSeason $min7NightsPricingSeasons)
    {
        $this->min7NightsPricingSeasons->removeElement($min7NightsPricingSeasons);
    }

    /**
     * Get min7NightsPricingSeasons
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getMin7NightsPricingSeasons()
    {
        return $this->min7NightsPricingSeasons;
    }

    /**
     * Set highestPrice
     *
     * @param string $highestPrice
     * @return Estate
     */
    public function setHighestPrice($highestPrice)
    {
        $this->highestPrice = $highestPrice;

        return $this;
    }

    /**
     * Get highestPrice
     *
     * @return string 
     */
    public function getHighestPrice()
    {
        return $this->highestPrice;
    }

    /**
     * Add administratorUsers
     *
     * @param \UTT\UserBundle\Entity\User $administratorUsers
     * @return Estate
     */
    public function addAdministratorUser(\UTT\UserBundle\Entity\User $administratorUsers)
    {
        $this->administratorUsers[] = $administratorUsers;

        return $this;
    }

    /**
     * Remove administratorUsers
     *
     * @param \UTT\UserBundle\Entity\User $administratorUsers
     */
    public function removeAdministratorUser(\UTT\UserBundle\Entity\User $administratorUsers)
    {
        $this->administratorUsers->removeElement($administratorUsers);
    }

    /**
     * Get administratorUsers
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getAdministratorUsers()
    {
        return $this->administratorUsers;
    }

    /**
     * Set isSaturdayOnly
     *
     * @param boolean $isSaturdayOnly
     * @return Estate
     */
    public function setIsSaturdayOnly($isSaturdayOnly)
    {
        $this->isSaturdayOnly = $isSaturdayOnly;

        return $this;
    }

    /**
     * Get isSaturdayOnly
     *
     * @return boolean 
     */
    public function getIsSaturdayOnly()
    {
        return $this->isSaturdayOnly;
    }

    /**
     * Add nearByEstates
     *
     * @param \UTT\EstateBundle\Entity\Estate $nearByEstates
     * @return Estate
     */
    public function addNearByEstate(\UTT\EstateBundle\Entity\Estate $nearByEstates)
    {
        $this->nearByEstates[] = $nearByEstates;

        return $this;
    }

    /**
     * Remove nearByEstates
     *
     * @param \UTT\EstateBundle\Entity\Estate $nearByEstates
     */
    public function removeNearByEstate(\UTT\EstateBundle\Entity\Estate $nearByEstates)
    {
        $this->nearByEstates->removeElement($nearByEstates);
    }

    /**
     * Get nearByEstates
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getNearByEstates()
    {
        return $this->nearByEstates;
    }

    /**
     * Set airbnbCalendar
     *
     * @param string $airbnbCalendar
     * @return Estate
     */
    public function setAirbnbCalendar($airbnbCalendar)
    {
        $this->airbnbCalendar = $airbnbCalendar;

        return $this;
    }

    /**
     * Get airbnbCalendar
     *
     * @return string 
     */
    public function getAirbnbCalendar()
    {
        return $this->airbnbCalendar;
    }

    /**
     * Set airbnbExportPassword
     *
     * @param string $airbnbExportPassword
     * @return Estate
     */
    public function setAirbnbExportPassword($airbnbExportPassword)
    {
        $this->airbnbExportPassword = $airbnbExportPassword;

        return $this;
    }

    /**
     * Get airbnbExportPassword
     *
     * @return string 
     */
    public function getAirbnbExportPassword()
    {
        return $this->airbnbExportPassword;
    }

    /**
     * Set airbnbExport
     *
     * @param boolean $airbnbExport
     * @return Estate
     */
    public function setAirbnbExport($airbnbExport)
    {
        $this->airbnbExport = $airbnbExport;

        return $this;
    }

    /**
     * Get airbnbExport
     *
     * @return boolean 
     */
    public function getAirbnbExport()
    {
        return $this->airbnbExport;
    }

    /**
     * Set specialCategory
     *
     * @param integer $specialCategory
     * @return Estate
     */
    public function setSpecialCategory($specialCategory)
    {
        $this->specialCategory = $specialCategory;

        return $this;
    }

    /**
     * Get specialCategory
     *
     * @return integer 
     */
    public function getSpecialCategory()
    {
        return $this->specialCategory;
    }

    /**
     * Set visits
     *
     * @param integer $visits
     * @return Estate
     */
    public function setVisits($visits)
    {
        $this->visits = $visits;

        return $this;
    }

    /**
     * Get visits
     *
     * @return integer 
     */
    public function getVisits()
    {
        return $this->visits;
    }

    /**
     * Set isCategoryFeatured
     *
     * @param boolean $isCategoryFeatured
     * @return Estate
     */
    public function setIsCategoryFeatured($isCategoryFeatured)
    {
        $this->isCategoryFeatured = $isCategoryFeatured;

        return $this;
    }

    /**
     * Get isCategoryFeatured
     *
     * @return boolean 
     */
    public function getIsCategoryFeatured()
    {
        return $this->isCategoryFeatured;
    }

    /**
     * Set defaultCategory
     *
     * @param \UTT\EstateBundle\Entity\Category $defaultCategory
     * @return Estate
     */
    public function setDefaultCategory(\UTT\EstateBundle\Entity\Category $defaultCategory = null)
    {
        $this->defaultCategory = $defaultCategory;

        return $this;
    }

    /**
     * Get defaultCategory
     *
     * @return \UTT\EstateBundle\Entity\Category 
     */
    public function getDefaultCategory()
    {
        return $this->defaultCategory;
    }

    /**
     * Set seoDescription
     *
     * @param string $seoDescription
     * @return Estate
     */
    public function setSeoDescription($seoDescription)
    {
        $this->seoDescription = $seoDescription;

        return $this;
    }

    /**
     * Get seoDescription
     *
     * @return string 
     */
    public function getSeoDescription()
    {
        return $this->seoDescription;
    }

    /**
     * Set latestVisits
     *
     * @param integer $latestVisits
     * @return Estate
     */
    public function setLatestVisits($latestVisits)
    {
        $this->latestVisits = $latestVisits;

        return $this;
    }

    /**
     * Get latestVisits
     *
     * @return integer 
     */
    public function getLatestVisits()
    {
        return $this->latestVisits;
    }

    /**
     * Set isMultiproperty
     *
     * @param boolean $isMultiproperty
     * @return Estate
     */
    public function setIsMultiproperty($isMultiproperty)
    {
        $this->isMultiproperty = $isMultiproperty;

        return $this;
    }

    /**
     * Get isMultiproperty
     *
     * @return boolean 
     */
    public function getIsMultiproperty()
    {
        return $this->isMultiproperty;
    }

    /**
     * Add subEstates
     *
     * @param \UTT\EstateBundle\Entity\Estate $subEstates
     * @return Estate
     */
    public function addSubEstate(\UTT\EstateBundle\Entity\Estate $subEstates)
    {
        $this->subEstates[] = $subEstates;

        return $this;
    }

    /**
     * Remove subEstates
     *
     * @param \UTT\EstateBundle\Entity\Estate $subEstates
     */
    public function removeSubEstate(\UTT\EstateBundle\Entity\Estate $subEstates)
    {
        $this->subEstates->removeElement($subEstates);
    }

    /**
     * Get subEstates
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getSubEstates()
    {
        return $this->subEstates;
    }

    /**
     * Set ownerBookingsCategory
     *
     * @param \UTT\EstateBundle\Entity\OwnerBookingsCategory $ownerBookingsCategory
     * @return Estate
     */
    public function setOwnerBookingsCategory(\UTT\EstateBundle\Entity\OwnerBookingsCategory $ownerBookingsCategory = null)
    {
        $this->ownerBookingsCategory = $ownerBookingsCategory;

        return $this;
    }

    /**
     * Get ownerBookingsCategory
     *
     * @return \UTT\EstateBundle\Entity\OwnerBookingsCategory 
     */
    public function getOwnerBookingsCategory()
    {
        return $this->ownerBookingsCategory;
    }

    /**
     * Set uttBookingFee
     *
     * @param integer $uttBookingFee
     * @return Estate
     */
    public function setUttBookingFee($uttBookingFee)
    {
        $this->uttBookingFee = $uttBookingFee;

        return $this;
    }

    /**
     * Get uttBookingFee
     *
     * @return integer 
     */
    public function getUttBookingFee()
    {
        return $this->uttBookingFee;
    }
}
