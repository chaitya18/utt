<?php

namespace UTT\EstateBundle\Entity;

use Doctrine\ORM\EntityRepository;

class EstateVisitReportRepository extends EntityRepository
{
    public function findVisitsForEstate(Estate $estate){
        $query = $this->_em->createQuery("SELECT sum(evr.visits) as sumVisit, SUBSTRING(evr.reportDate, 1, 7) as dateString FROM UTTEstateBundle:EstateVisitReport evr WHERE evr.estate = :estateId GROUP BY dateString ORDER BY dateString DESC");
        $query->setParameter('estateId', $estate->getId());

        $result = $query->getArrayResult();
        if(count($result) > 0) return $result;

        return false;
    }

    public function findNumberOfVisitsForLatestDays(Estate $estate, $days = 7){
        $reportDate = new \DateTime('now');
        $reportDate->modify('-'.$days.' days');

        $query = $this->_em->createQuery("SELECT sum(evr.visits) as visits FROM UTTEstateBundle:EstateVisitReport evr WHERE evr.estate = :estateId AND evr.reportDate > :reportDate");
        $query->setParameter('estateId', $estate->getId());
        $query->setParameter('reportDate', $reportDate->format('Y-m-d'));

        $result = $query->getArrayResult();
        if(isset($result[0]) && isset($result[0]['visits'])){
            return $result[0]['visits'];
        }

        return 0;
    }
}
