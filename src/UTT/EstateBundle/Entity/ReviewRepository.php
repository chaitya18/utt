<?php

namespace UTT\EstateBundle\Entity;

use Doctrine\ORM\EntityRepository;

class ReviewRepository extends EntityRepository
{
    public function findForCategory(Category $category, $limit = null){
        $query = $this->_em->createQuery("
          SELECT r, u, uc
          FROM UTTEstateBundle:Review r
          LEFT JOIN r.estate e
          LEFT JOIN r.user u
          LEFT JOIN u.country uc
          LEFT JOIN e.categories ec
          WHERE ec.id IN (:categoryId) AND r.isVerified = TRUE
          ORDER BY r.id DESC
          ");
        $query->setParameter('categoryId', $category->getId());
        if($limit) $query->setMaxResults($limit);

        $result = $query->getArrayResult();
        if(count($result) > 0) return $result;

        return false;
    }
}
