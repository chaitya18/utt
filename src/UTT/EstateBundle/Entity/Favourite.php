<?php

namespace UTT\EstateBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Favourite
 *
 * @ORM\Table(name="favourite")
 * @ORM\Entity(repositoryClass="UTT\EstateBundle\Entity\FavouriteRepository")
 */
class Favourite
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="Estate")
     * @ORM\JoinColumn(name="estate_id", referencedColumnName="id", nullable=true, onDelete="CASCADE")
     */
    protected $estate;

    /**
     * @ORM\ManyToOne(targetEntity="UTT\UserBundle\Entity\User")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id", nullable=true, onDelete="CASCADE")
     */
    protected $user;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set estate
     *
     * @param \UTT\EstateBundle\Entity\Estate $estate
     * @return Favourite
     */
    public function setEstate(\UTT\EstateBundle\Entity\Estate $estate = null)
    {
        $this->estate = $estate;

        return $this;
    }

    /**
     * Get estate
     *
     * @return \UTT\EstateBundle\Entity\Estate 
     */
    public function getEstate()
    {
        return $this->estate;
    }

    /**
     * Set user
     *
     * @param \UTT\UserBundle\Entity\User $user
     * @return Favourite
     */
    public function setUser(\UTT\UserBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \UTT\UserBundle\Entity\User 
     */
    public function getUser()
    {
        return $this->user;
    }
}
