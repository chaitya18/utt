<?php

namespace UTT\EstateBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * EstateCalendarDayDataPrice
 *
 * @ORM\Table(name="estate_calendar_day_data_price")
 * @ORM\Entity(repositoryClass="UTT\EstateBundle\Entity\EstateCalendarDayDataPriceRepository")
 */
class EstateCalendarDayDataPrice
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="EstateCalendarDay")
     * @ORM\JoinColumn(name="estate_calendar_day_id", referencedColumnName="id", onDelete="CASCADE")
     */
    protected $estateCalendarDay;

    /**
     * @ORM\Column(name="nights", type="integer")
     */
    protected $nights;

    /**
     * @ORM\Column(name="isValid", type="boolean")
     */
    protected $isValid = false;

    /**
     * @ORM\Column(name="price", type="string", length=255, nullable=true)
     */
    protected $price;

    /**
     * @ORM\Column(name="selectPrev", type="integer")
     */
    protected $selectPrev;

    /**
     * @ORM\Column(name="selectNext", type="integer")
     */
    protected $selectNext;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nights
     *
     * @param integer $nights
     * @return EstateCalendarDayDataPrice
     */
    public function setNights($nights)
    {
        $this->nights = $nights;

        return $this;
    }

    /**
     * Get nights
     *
     * @return integer 
     */
    public function getNights()
    {
        return $this->nights;
    }

    /**
     * Set isValid
     *
     * @param boolean $isValid
     * @return EstateCalendarDayDataPrice
     */
    public function setIsValid($isValid)
    {
        $this->isValid = $isValid;

        return $this;
    }

    /**
     * Get isValid
     *
     * @return boolean 
     */
    public function getIsValid()
    {
        return $this->isValid;
    }

    /**
     * Set price
     *
     * @param string $price
     * @return EstateCalendarDayDataPrice
     */
    public function setPrice($price)
    {
        $this->price = $price;

        return $this;
    }

    /**
     * Get price
     *
     * @return string 
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * Set selectPrev
     *
     * @param integer $selectPrev
     * @return EstateCalendarDayDataPrice
     */
    public function setSelectPrev($selectPrev)
    {
        $this->selectPrev = $selectPrev;

        return $this;
    }

    /**
     * Get selectPrev
     *
     * @return integer 
     */
    public function getSelectPrev()
    {
        return $this->selectPrev;
    }

    /**
     * Set selectNext
     *
     * @param integer $selectNext
     * @return EstateCalendarDayDataPrice
     */
    public function setSelectNext($selectNext)
    {
        $this->selectNext = $selectNext;

        return $this;
    }

    /**
     * Get selectNext
     *
     * @return integer 
     */
    public function getSelectNext()
    {
        return $this->selectNext;
    }

    /**
     * Set estateCalendarDay
     *
     * @param \UTT\EstateBundle\Entity\EstateCalendarDay $estateCalendarDay
     * @return EstateCalendarDayDataPrice
     */
    public function setEstateCalendarDay(\UTT\EstateBundle\Entity\EstateCalendarDay $estateCalendarDay = null)
    {
        $this->estateCalendarDay = $estateCalendarDay;

        return $this;
    }

    /**
     * Get estateCalendarDay
     *
     * @return \UTT\EstateBundle\Entity\EstateCalendarDay 
     */
    public function getEstateCalendarDay()
    {
        return $this->estateCalendarDay;
    }
}
