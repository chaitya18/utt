<?php

namespace UTT\EstateBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Category
 *
 * @ORM\Table(name="estate_category")
 * @ORM\Entity(repositoryClass="UTT\EstateBundle\Entity\CategoryRepository")
 */
class Category
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(name="name", type="string", length=255, nullable=true)
     */
    protected $name;

    /**
     * @ORM\Column(name="slug", type="string", length=255, nullable=true)
     */
    protected $slug;

    /**
     * @ORM\Column(name="image", type="string", length=255, nullable=true)
     */
    protected $image;
    public $file;

    /**
     * @ORM\Column(name="is_homepage", type="boolean")
     */
    protected $isHomepage = false;

    /**
     * @ORM\Column(name="is_search_filter", type="boolean")
     */
    protected $isSearchFilter = false;

    /**
     * @ORM\Column(name="is_header_menu", type="boolean")
     */
    protected $isHeaderMenu = false;

    /**
     * @ORM\Column(name="text_description", type="text", nullable=true)
     */
    protected $textDescription;

    /**
     * @ORM\Column(name="sort_order", type="integer", nullable=true)
     */
    protected $sortOrder;

#--------------------------------------------------- ENTITY METHODS ---------------------------------------------------#

    public function __toString(){
        if($this->getName())
            return $this->getName();
        return '';
    }

    public function getUploadDir(){
        return 'uploads/estateCategoryImages';
    }

    protected function getUploadRootDir($basepath = null){
        return $basepath.$this->getUploadDir();
    }

    public function uploadImage($basepath = null){
        if (null === $this->file) {
            return;
        }

        if (null === $basepath) {
            return;
        }
        $name =  uniqid().substr($this->file->getClientOriginalName(),-4);
        $this->file->move($this->getUploadRootDir($basepath), $name);
        $this->setImage($name);
        $this->file = null;
    }

#################################################### ENTITY METHODS ####################################################

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Category
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set image
     *
     * @param string $image
     * @return Category
     */
    public function setImage($image)
    {
        $this->image = $image;

        return $this;
    }

    /**
     * Get image
     *
     * @return string 
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * Set slug
     *
     * @param string $slug
     * @return Category
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;

        return $this;
    }

    /**
     * Get slug
     *
     * @return string 
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * Set isHomepage
     *
     * @param boolean $isHomepage
     * @return Category
     */
    public function setIsHomepage($isHomepage)
    {
        $this->isHomepage = $isHomepage;

        return $this;
    }

    /**
     * Get isHomepage
     *
     * @return boolean 
     */
    public function getIsHomepage()
    {
        return $this->isHomepage;
    }

    /**
     * Set isSearchFilter
     *
     * @param boolean $isSearchFilter
     * @return Category
     */
    public function setIsSearchFilter($isSearchFilter)
    {
        $this->isSearchFilter = $isSearchFilter;

        return $this;
    }

    /**
     * Get isSearchFilter
     *
     * @return boolean 
     */
    public function getIsSearchFilter()
    {
        return $this->isSearchFilter;
    }

    /**
     * Set textDescription
     *
     * @param string $textDescription
     * @return Category
     */
    public function setTextDescription($textDescription)
    {
        $this->textDescription = $textDescription;

        return $this;
    }

    /**
     * Get textDescription
     *
     * @return string 
     */
    public function getTextDescription()
    {
        return $this->textDescription;
    }

    /**
     * Set isHeaderMenu
     *
     * @param boolean $isHeaderMenu
     * @return Category
     */
    public function setIsHeaderMenu($isHeaderMenu)
    {
        $this->isHeaderMenu = $isHeaderMenu;

        return $this;
    }

    /**
     * Get isHeaderMenu
     *
     * @return boolean 
     */
    public function getIsHeaderMenu()
    {
        return $this->isHeaderMenu;
    }

    /**
     * Set sortOrder
     *
     * @param integer $sortOrder
     * @return Category
     */
    public function setSortOrder($sortOrder)
    {
        $this->sortOrder = $sortOrder;

        return $this;
    }

    /**
     * Get sortOrder
     *
     * @return integer 
     */
    public function getSortOrder()
    {
        return $this->sortOrder;
    }
}
