<?php

namespace UTT\EstateBundle\Entity;

use Doctrine\ORM\EntityRepository;
use UTT\EstateBundle\Entity\Estate;

class EstateRepository extends EntityRepository
{
    CONST PAGE_LIMIT = 150;

    public function getForEstateSearchArray($isPriceRequired = true, $location, $sleeps, $estateFeatures, $pets, $children, $infants, $priceFrom, $priceTo, $paramPage, $estateName, $category, $specialCategory){
        $queryEstateName = '';
        if(!is_null($estateName)){
            $queryEstateName = 'e.name LIKE :estateName';
        }

        $queryLocationJoin = '';
        $queryLocation = '';
        if(!is_null($location)){
            $queryLocationJoin = 'LEFT JOIN e.location loc LEFT JOIN loc.parents par';
            $queryLocation = '(e.location = :location OR par.id = :location)';
        }

        $queryCategory = '';
        if(!is_null($category)){
            $queryCategory = 'ec.id IN (:category)';
        }

        $querySleeps = '';
        if(!is_null($sleeps)){
            $querySleeps = 'e.sleeps >= :sleeps';
        }

        $queryEstateFeatures = '';
        if(is_array($estateFeatures) && count($estateFeatures) > 0){
            $queryEstateFeatures = 'f.id IN (:estateFeatures)';
        }

        $queryPets = '';
        if(!is_null($pets)){
            $queryPets = '(e.pets = :PETS_WELCOME_MAX_1_DOG OR e.pets = :PETS_WELCOME_MAX_2_DOGS OR e.pets = :PETS_WELCOME_MAX_3_DOGS OR e.pets = :PETS_WELCOME_MORE_THAN_3_CONTRACT_US_UPON_BOOKING)';
        }

        $queryChildren = '';
        if(!is_null($children)){
            $queryChildren = 'e.children = :CHILDREN_WELCOME';
        }

        $queryInfants = '';
        if(!is_null($infants)){
            $queryInfants = '(e.infants = :INFANTS_COT_AVAILABLE OR e.infants = :INFANTS_COT_HIGH_CHAIR_AVAILABLE OR e.infants = :INFANTS_HIGH_CHAIR_ONLY_AVAILABLE)';
        }

        $queryPriceFrom = '';
        if(!is_null($priceFrom) && $isPriceRequired){
            $queryPriceFrom = '(e.lowestPrice > :priceFrom OR e.highestPrice > :priceFrom)';
        }

        $queryPriceTo = '';
        if(!is_null($priceTo) && $isPriceRequired){
            $queryPriceTo = '(e.lowestPrice < :priceTo OR e.highestPrice < :priceTo)';
        }

        $querySpecialCategory = '';
        if(!is_null($specialCategory)){
            if($specialCategory == Estate::SPECIAL_CATEGORY_BUDGET){
                $querySpecialCategory = '(e.specialCategory = :SPECIAL_CATEGORY_BUDGET)';
            }elseif($specialCategory == Estate::SPECIAL_CATEGORY_FINEST){
                $querySpecialCategory = '(e.specialCategory = :SPECIAL_CATEGORY_FINEST)';
            }elseif($specialCategory == Estate::SPECIAL_CATEGORY_NONE){
                $querySpecialCategory = '(e.specialCategory = :SPECIAL_CATEGORY_NONE)';
            }
        }

        $queryWhereArray = array('e.hideFromDisplay = :hideFromDisplay');
        if($queryEstateName != '' || $queryLocation != '' || $queryCategory != '' || $querySleeps != '' || $queryEstateFeatures != '' || $queryPets != '' || $queryChildren != '' || $queryInfants != '' || $queryPriceFrom != '' || $queryPriceTo != '' || $querySpecialCategory != ''){
            if($queryEstateName != '') $queryWhereArray[] = $queryEstateName;
            if($queryLocation != '') $queryWhereArray[] = $queryLocation;
            if($queryCategory != '') $queryWhereArray[] = $queryCategory;
            if($querySleeps != '') $queryWhereArray[] = $querySleeps;
            if($queryEstateFeatures != '') $queryWhereArray[] = $queryEstateFeatures;
            if($queryPets != '') $queryWhereArray[] = $queryPets;
            if($queryChildren != '') $queryWhereArray[] = $queryChildren;
            if($queryInfants != '') $queryWhereArray[] = $queryInfants;
            if($queryPriceFrom != '') $queryWhereArray[] = $queryPriceFrom;
            if($queryPriceTo != '') $queryWhereArray[] = $queryPriceTo;
            if($querySpecialCategory != '') $queryWhereArray[] = $querySpecialCategory;
        }
        if(count($queryWhereArray) > 0) $queryWhere = "WHERE ".implode(' and ', $queryWhereArray); else $queryWhere = '';

        $query = $this->_em->createQuery("SELECT e, pm FROM UTTEstateBundle:Estate e LEFT JOIN e.photos p LEFT JOIN e.photoMain pm LEFT JOIN e.estateFeatures f LEFT JOIN e.categories ec ".$queryLocationJoin." ".$queryWhere.' GROUP BY e.id');
        $query->setParameter('hideFromDisplay', Estate::HIDE_FROM_DISPLAY_VISIBLE);
        if(!is_null($estateName)) $query->setParameter('estateName', '%'.$estateName.'%');
        if(!is_null($location)) $query->setParameter('location', $location);
        if(!is_null($category)) $query->setParameter('category', $category);
        if(!is_null($sleeps)) $query->setParameter('sleeps', $sleeps);
        if(!is_null($estateFeatures)) $query->setParameter('estateFeatures', $estateFeatures);
        if(!is_null($pets)){
            $query->setParameter('PETS_WELCOME_MAX_1_DOG', Estate::PETS_WELCOME_MAX_1_DOG);
            $query->setParameter('PETS_WELCOME_MAX_2_DOGS', Estate::PETS_WELCOME_MAX_2_DOGS);
            $query->setParameter('PETS_WELCOME_MAX_3_DOGS', Estate::PETS_WELCOME_MAX_3_DOGS);
            $query->setParameter('PETS_WELCOME_MORE_THAN_3_CONTRACT_US_UPON_BOOKING', Estate::PETS_WELCOME_MORE_THAN_3_CONTRACT_US_UPON_BOOKING);
        }
        if(!is_null($children)) {
            $query->setParameter('CHILDREN_WELCOME', Estate::CHILDREN_WELCOME);
        }
        if(!is_null($infants)) {
            $query->setParameter('INFANTS_COT_AVAILABLE', Estate::INFANTS_COT_AVAILABLE);
            $query->setParameter('INFANTS_COT_HIGH_CHAIR_AVAILABLE', Estate::INFANTS_COT_HIGH_CHAIR_AVAILABLE);
            $query->setParameter('INFANTS_HIGH_CHAIR_ONLY_AVAILABLE', Estate::INFANTS_HIGH_CHAIR_ONLY_AVAILABLE);
        }
        if(!is_null($priceFrom) && $isPriceRequired) $query->setParameter('priceFrom', $priceFrom);
        if(!is_null($priceTo) && $isPriceRequired) $query->setParameter('priceTo', $priceTo);
        if(!is_null($specialCategory)){
            if($specialCategory == Estate::SPECIAL_CATEGORY_BUDGET){
                $query->setParameter('SPECIAL_CATEGORY_BUDGET', Estate::SPECIAL_CATEGORY_BUDGET);
            }elseif($specialCategory == Estate::SPECIAL_CATEGORY_FINEST){
                $query->setParameter('SPECIAL_CATEGORY_FINEST', Estate::SPECIAL_CATEGORY_FINEST);
            }elseif($specialCategory == Estate::SPECIAL_CATEGORY_NONE){
                $query->setParameter('SPECIAL_CATEGORY_NONE', Estate::SPECIAL_CATEGORY_NONE);
            }
        }
        if(!is_null($paramPage)){
            $query->setFirstResult(($paramPage-1) * self::PAGE_LIMIT);
        }
        $query->setMaxResults(self::PAGE_LIMIT);

        $result = $query->getArrayResult();
        if(is_array($result) && count($result) > 0) return $result;

        return array();
    }

    public function getJustLaunchedArray($limit = 4){
        $query = $this->_em->createQuery("SELECT e, p, pm FROM UTTEstateBundle:Estate e LEFT JOIN e.photos p LEFT JOIN e.photoMain pm WHERE e.isJustLaunched = 1 AND e.hideFromDisplay = :hideFromDisplay GROUP BY e.id ORDER BY e.id DESC");
        $query->setParameter('hideFromDisplay', Estate::HIDE_FROM_DISPLAY_VISIBLE);
        if($limit) $query->setMaxResults($limit);

        $result = $query->getArrayResult();
        if(is_array($result) && count($result) > 0) return $result;

        return array();
    }

    public function getForShortList($estateIds, $objectReturn = false){
        if(!(is_array($estateIds) && count($estateIds) > 0)) return false;
        $query = $this->_em->createQuery("SELECT e, p, pm, f FROM UTTEstateBundle:Estate e LEFT JOIN e.photos p LEFT JOIN e.photoMain pm LEFT JOIN e.estateFeatures f WHERE e.id IN (:estateIds) AND e.hideFromDisplay = :hideFromDisplay");
        $query->setParameter('hideFromDisplay', Estate::HIDE_FROM_DISPLAY_VISIBLE);
        $query->setParameter('estateIds', $estateIds);

        if($objectReturn){
            $result = $query->getResult();
        }else{
            $result = $query->getArrayResult();
        }

        if(is_array($result) && count($result) > 0) return $result;

        return false;
    }

    public function getEstatesNearToEstateArray(Estate $estate, $limit = 5){
        if(!(is_numeric($limit) && $limit > 0)) return false;

        $qry = "SELECT id, (((acos(sin((".$estate->getLat()."*pi()/180)) *
            sin((`latitude`*pi()/180))+cos((".$estate->getLat()."*pi()/180)) *
            cos((`latitude`*pi()/180)) * cos(((".$estate->getLon()."- `longitude`)*
            pi()/180))))*180/pi())*60*1.1515*1.609344
        ) as distance
        FROM estate
        WHERE hide_from_display = '".Estate::HIDE_FROM_DISPLAY_VISIBLE."'
        HAVING distance > 0
        ORDER BY distance ASC
        LIMIT ".$limit;

        $sqlConnection = $this->getEntityManager()->getConnection()->prepare($qry);
        $sqlConnection->execute();
        $result = $sqlConnection->fetchAll();
        if(!(is_array($result) && count($result) > 0)){
            return false;
        }

        $ids = array();
        foreach($result as $row){
            $ids[] = $row['id'];
        }

        $query = $this->_em->createQuery("SELECT e, pm FROM UTTEstateBundle:Estate e LEFT JOIN e.photoMain pm WHERE e.id IN (:estateIds) and e.id != :currentEstateId AND e.hideFromDisplay = :hideFromDisplay");
        $query->setParameter('estateIds', $ids);
        $query->setParameter('currentEstateId', $estate->getId());
        $query->setParameter('hideFromDisplay', Estate::HIDE_FROM_DISPLAY_VISIBLE);
        if(is_numeric($limit) && $limit > 0) $query->setMaxResults($limit);

        return $query->getArrayResult();
    }

    public function getFirstActiveEstate(){
        $query = $this->_em->createQuery("SELECT e FROM UTTEstateBundle:Estate e WHERE e.hideFromDisplay = :hideFromDisplay ORDER BY e.id ASC");
        $query->setParameter('hideFromDisplay', Estate::HIDE_FROM_DISPLAY_VISIBLE);
        $query->setMaxResults(1);

        $result = $query->getResult();
        if(count($result) > 0) return $result[0];

        return false;
    }

    public function getFirstActiveEstateOwnedByUser($userId){
        $query = $this->_em->createQuery("SELECT e FROM UTTEstateBundle:Estate e LEFT JOIN e.ownerUsers ou WHERE ou.id = :userId AND e.hideFromDisplay = :hideFromDisplay ORDER BY e.id ASC");
        $query->setParameter('hideFromDisplay', Estate::HIDE_FROM_DISPLAY_VISIBLE);
        $query->setParameter('userId', $userId);
        $query->setMaxResults(1);

        $result = $query->getResult();
        if(count($result) > 0) return $result[0];

        return false;
    }

    public function getAllActive(){
        $query = $this->_em->createQuery("SELECT e, min7 FROM UTTEstateBundle:Estate e LEFT JOIN e.min7NightsPricingSeasons min7 WHERE e.hideFromDisplay = :hideFromDisplay");
        $query->setParameter('hideFromDisplay', Estate::HIDE_FROM_DISPLAY_VISIBLE);

        $result = $query->getResult();
        if(count($result) > 0) return $result;

        return false;
    }

    public function getAllActiveOrHidden(){
        $query = $this->_em->createQuery("
            SELECT e, min7 
            FROM UTTEstateBundle:Estate e 
            LEFT JOIN e.min7NightsPricingSeasons min7 
            WHERE e.hideFromDisplay = :hideFromDisplayVisible OR e.hideFromDisplay = :hideFromDisplayHidden
        ");
        $query->setParameter('hideFromDisplayVisible', Estate::HIDE_FROM_DISPLAY_VISIBLE);
        $query->setParameter('hideFromDisplayHidden', Estate::HIDE_FROM_DISPLAY_HIDDEN);

        $result = $query->getResult();
        if(count($result) > 0) return $result;

        return false;
    }

    public function getAllActiveMaxId(){
        $query = $this->_em->createQuery("SELECT MAX(e.id) FROM UTTEstateBundle:Estate e WHERE e.hideFromDisplay = :hideFromDisplay");
        $query->setParameter('hideFromDisplay', Estate::HIDE_FROM_DISPLAY_VISIBLE);

        return $query->getSingleScalarResult();
    }

    public function getOwnedByUser($userId){
        $query = $this->_em->createQuery("SELECT e FROM UTTEstateBundle:Estate e LEFT JOIN e.ownerUsers ou WHERE ou.id = :userId");
        $query->setParameter('userId', $userId);

        $result = $query->getResult();
        if(count($result) > 0) return $result;

        return false;
    }

    public function getActiveOwnedByUser($userId, $limit = null){
        $query = $this->_em->createQuery("SELECT e FROM UTTEstateBundle:Estate e LEFT JOIN e.ownerUsers ou WHERE ou.id = :userId AND e.hideFromDisplay = :hideFromDisplay");
        $query->setParameter('userId', $userId);
        $query->setParameter('hideFromDisplay', Estate::HIDE_FROM_DISPLAY_VISIBLE);
        if($limit) {
            $query->setMaxResults($limit);
        }

        $result = $query->getResult();
        if(count($result) > 0) return $result;

        return false;
    }

    public function getActiveWithAirbnbCalendar(){
        $query = $this->_em->createQuery("SELECT e FROM UTTEstateBundle:Estate e LEFT JOIN e.ownerUsers ou WHERE e.hideFromDisplay = :hideFromDisplay AND e.airbnbCalendar IS NOT NULL AND e.airbnbCalendar != '' ");
        $query->setParameter('hideFromDisplay', Estate::HIDE_FROM_DISPLAY_VISIBLE);

        $result = $query->getResult();
        if(count($result) > 0) return $result;

        return false;
    }

    public function getCleanedByUser($userId){
        $query = $this->_em->createQuery("SELECT e FROM UTTEstateBundle:Estate e LEFT JOIN e.cleanerUsers cu WHERE cu.id = :userId");
        $query->setParameter('userId', $userId);

        $result = $query->getResult();
        if(count($result) > 0) return $result;

        return false;
    }

    public function getMultipropertyEstates(){
        $query = $this->_em->createQuery("SELECT e FROM UTTEstateBundle:Estate e WHERE e.isMultiproperty = TRUE AND e.hideFromDisplay = :hideFromDisplay");
        $query->setParameter('hideFromDisplay', Estate::HIDE_FROM_DISPLAY_VISIBLE);

        $result = $query->getResult();
        if(count($result) > 0) return $result;

        return false;
    }

    public function getActiveCleanedByUser($userId){
        $query = $this->_em->createQuery("SELECT e FROM UTTEstateBundle:Estate e LEFT JOIN e.cleanerUsers cu WHERE cu.id = :userId AND e.hideFromDisplay = :hideFromDisplay");
        $query->setParameter('userId', $userId);
        $query->setParameter('hideFromDisplay', Estate::HIDE_FROM_DISPLAY_VISIBLE);

        $result = $query->getResult();
        if(count($result) > 0) return $result;

        return false;
    }

    public function getFirstActiveEstateCleanedByUser($userId){
        $query = $this->_em->createQuery("SELECT e FROM UTTEstateBundle:Estate e LEFT JOIN e.cleanerUsers cu WHERE cu.id = :userId AND e.hideFromDisplay = :hideFromDisplay ORDER BY e.id ASC");
        $query->setParameter('userId', $userId);
        $query->setParameter('hideFromDisplay', Estate::HIDE_FROM_DISPLAY_VISIBLE);
        $query->setMaxResults(1);

        $result = $query->getResult();
        if(count($result) > 0) return $result[0];

        return false;
    }

    public function findActivePropertiesOverTime(\Datetime $datetime){
        $query = $this->_em->createQuery("
          SELECT e.id, e.name
          FROM UTTReservationBundle:Reservation r
          LEFT JOIN r.estate e
          WHERE r.createdAt < :maxDate AND e.hideFromDisplay = :hideFromDisplay
          GROUP BY e.id
          ");
        $query->setParameter('maxDate', $datetime);
        $query->setParameter('hideFromDisplay', Estate::HIDE_FROM_DISPLAY_VISIBLE);

        return count($query->getArrayResult());
    }

    public function findActiveCategoryFeaturedArray(Category $category, $limit = null){
        $query = $this->_em->createQuery("
          SELECT e, pm
          FROM UTTEstateBundle:Estate e
          LEFT JOIN e.photos p
          LEFT JOIN e.photoMain pm
          LEFT JOIN e.estateFeatures f
          LEFT JOIN e.categories ec
          WHERE e.hideFromDisplay = :hideFromDisplay AND ec.id IN (:categoryId) AND e.isCategoryFeatured = TRUE
          GROUP BY e.id
          ");
        $query->setParameter('categoryId', $category->getId());
        $query->setParameter('hideFromDisplay', Estate::HIDE_FROM_DISPLAY_VISIBLE);
        if($limit) $query->setMaxResults($limit);

        $result = $query->getArrayResult();
        if(count($result) > 0) return $result;

        return false;
    }

    public function findByName($estateName, $limit = 10)
    {
        if(empty($estateName)){
            return array();
        }
        $query = $this->_em->createQuery("SELECT e.name FROM UTTEstateBundle:Estate e WHERE e.name like :name");
        $query->setParameter('name', '%'.addcslashes($estateName, '%_') . '%');
        $query->setMaxResults($limit);

        $result = $query->getArrayResult();
        if(is_array($result) && count($result) > 0) return $result;

        return array();
    }

    public function getAllActiveNames(){
        $query = $this->_em->createQuery("SELECT e.name FROM UTTEstateBundle:Estate e LEFT JOIN e.min7NightsPricingSeasons min7 WHERE e.hideFromDisplay = :hideFromDisplay");
        $query->setParameter('hideFromDisplay', Estate::HIDE_FROM_DISPLAY_VISIBLE);

        $result = $query->getResult();
        if(count($result) > 0) return $result;

        return array();
    }
}
