<?php

namespace UTT\EstateBundle\Entity;

use Doctrine\ORM\EntityRepository;
use UTT\EstateBundle\Entity\Estate;

class EstateCalendarDayRepository extends EntityRepository
{
    public function getForNotActiveEstates(){
        $query = $this->_em->createQuery("
            SELECT ecd, e
            FROM UTTEstateBundle:EstateCalendarDay ecd
            LEFT JOIN ecd.estate e
            WHERE e.hideFromDisplay != :hideFromDisplay
        ");
        $query->setParameter('hideFromDisplay', Estate::HIDE_FROM_DISPLAY_VISIBLE);

        $result = $query->getResult();
        if(count($result) > 0) return $result;

        return false;
    }

    public function getDaysByEstate($nights, Estate $estate){
        $connection = $this->_em->getConnection();
        $statement = $connection->prepare("
          SELECT
              estate_calendar_day.*,
              estate.name as estate_name,
              estate.location_text as estate_location_text,
              estate.short_name as estate_short_name,
              estate.minimum_guests as estate_minimum_guests,
              estate_photo.file_name as estate_photo_file_name,
              CONCAT(
                estate_calendar_day_data_price.isValid, '|',
                estate_calendar_day_data_price.selectPrev, '|',
                estate_calendar_day_data_price.selectNext, '|',
                estate_calendar_day_data_price.price
              ) as dataPrice,
              estate_calendar_day_data_price.price as day_price
          FROM estate_calendar_day
          LEFT JOIN estate ON estate_calendar_day.estate_id = estate.id
          LEFT JOIN estate_calendar_day_data_price ON estate_calendar_day_data_price.estate_calendar_day_id = estate_calendar_day.id
          LEFT JOIN estate_photo ON estate.photo_main = estate_photo.id
          WHERE
                estate.id = :estateId AND estate_calendar_day.date >= :nowDate AND estate_calendar_day_data_price.nights = :nights
          GROUP BY CONCAT(estate_calendar_day.date, estate_calendar_day.estate_id)
        ");
        $nowDate = new \DateTime('now');
        $statement->bindValue('nowDate', $nowDate->format('Y-m-d'));
        $statement->bindValue('nights', $nights);
        $statement->bindValue('estateId', $estate->getId());
        $statement->execute();
        $results = $statement->fetchAll();
        return $results;
    }

    public function getDaysFlexible($nights, $estateMinId, $estateMaxId, $locationsIds){
        $locationId0 = null;
        $locationId1 = null;
        $locationId2 = null;
        $locationId3 = null;
        $locationId4 = null;
        $locationId5 = null;

        if(isset($locationsIds[0])){ $locationId0 = $locationsIds[0]; }
        if(isset($locationsIds[1])){ $locationId1 = $locationsIds[1]; }
        if(isset($locationsIds[2])){ $locationId2 = $locationsIds[2]; }
        if(isset($locationsIds[3])){ $locationId3 = $locationsIds[3]; }
        if(isset($locationsIds[4])){ $locationId4 = $locationsIds[4]; }
        if(isset($locationsIds[5])){ $locationId5 = $locationsIds[5]; }

        $connection = $this->_em->getConnection();
        $statement = $connection->prepare("
          SELECT
              estate_calendar_day.*,
              estate.name as estate_name,
              estate.location_text as estate_location_text,
              estate.short_name as estate_short_name,
              estate.minimum_guests as estate_minimum_guests,
              estate_photo.file_name as estate_photo_file_name,
              CONCAT(
                estate_calendar_day_data_price.isValid, '|',
                estate_calendar_day_data_price.selectPrev, '|',
                estate_calendar_day_data_price.selectNext, '|',
                estate_calendar_day_data_price.price
              ) as dataPrice,
              estate_calendar_day_data_price.price as day_price
          FROM estate_calendar_day
          LEFT JOIN estate ON estate_calendar_day.estate_id = estate.id
          LEFT JOIN estate_calendar_day_data_price ON estate_calendar_day_data_price.estate_calendar_day_id = estate_calendar_day.id
          LEFT JOIN estate_photo ON estate.photo_main = estate_photo.id
          WHERE
                estate.id > :estateMinId AND estate.id <= :estateMaxId ".

            (!is_null($locationId0) ? 'AND (estate.location_id = :locationId0 '.
                (!is_null($locationId1) ? ' OR estate.location_id = :locationId1 ' : '').
                (!is_null($locationId2) ? ' OR estate.location_id = :locationId2 ' : '').
                (!is_null($locationId3) ? ' OR estate.location_id = :locationId3 ' : '').
                (!is_null($locationId4) ? ' OR estate.location_id = :locationId4 ' : '').
                (!is_null($locationId5) ? ' OR estate.location_id = :locationId5 ' : '')
                .' ) ' : '')

            ." AND
                estate_calendar_day.date >= :nowDate AND
                (
                  (
                    estate.type = :estateTypeFlexible AND
                    estate_calendar_day_data_price.nights = :nights
                  )
                )
          GROUP BY CONCAT(estate_calendar_day.date, estate_calendar_day.estate_id)
        ");
        $nowDate = new \DateTime('now');
        $statement->bindValue('nowDate', $nowDate->format('Y-m-d'));
        $statement->bindValue('estateTypeFlexible', Estate::TYPE_FLEXIBLE_BOOKING);
        $statement->bindValue('nights', $nights);
        $statement->bindValue('estateMinId', $estateMinId);
        $statement->bindValue('estateMaxId', $estateMaxId);

        if(!is_null($locationId0)) $statement->bindValue('locationId0', $locationId0);
        if(!is_null($locationId1)) $statement->bindValue('locationId1', $locationId1);
        if(!is_null($locationId2)) $statement->bindValue('locationId2', $locationId2);
        if(!is_null($locationId3)) $statement->bindValue('locationId3', $locationId3);
        if(!is_null($locationId4)) $statement->bindValue('locationId4', $locationId4);
        if(!is_null($locationId5)) $statement->bindValue('locationId5', $locationId5);

        $statement->execute();
        $results = $statement->fetchAll();
        return $results;
    }

    public function getDaysStandard($nights, $estateMinId, $estateMaxId, $locationsIds){
        $locationId0 = null;
        $locationId1 = null;
        $locationId2 = null;
        $locationId3 = null;
        $locationId4 = null;
        $locationId5 = null;

        if(isset($locationsIds[0])){ $locationId0 = $locationsIds[0]; }
        if(isset($locationsIds[1])){ $locationId1 = $locationsIds[1]; }
        if(isset($locationsIds[2])){ $locationId2 = $locationsIds[2]; }
        if(isset($locationsIds[3])){ $locationId3 = $locationsIds[3]; }
        if(isset($locationsIds[4])){ $locationId4 = $locationsIds[4]; }
        if(isset($locationsIds[5])){ $locationId5 = $locationsIds[5]; }

        $connection = $this->_em->getConnection();
        $statement = $connection->prepare("
          SELECT
              estate_calendar_day.*,
              estate.name as estate_name,
              estate.location_text as estate_location_text,
              estate.short_name as estate_short_name,
              estate.minimum_guests as estate_minimum_guests,
              estate_photo.file_name as estate_photo_file_name,
              CONCAT(
                estate_calendar_day_data_price.isValid, '|',
                estate_calendar_day_data_price.selectPrev, '|',
                estate_calendar_day_data_price.selectNext, '|',
                estate_calendar_day_data_price.price
              ) as dataPrice,
              estate_calendar_day_data_price.price as day_price
          FROM estate_calendar_day
          LEFT JOIN estate ON estate_calendar_day.estate_id = estate.id
          LEFT JOIN estate_calendar_day_data_price ON estate_calendar_day_data_price.estate_calendar_day_id = estate_calendar_day.id
          LEFT JOIN estate_photo ON estate.photo_main = estate_photo.id
          WHERE
                estate.id > :estateMinId AND estate.id <= :estateMaxId ".

            (!is_null($locationId0) ? 'AND (estate.location_id = :locationId0 '.
                (!is_null($locationId1) ? ' OR estate.location_id = :locationId1 ' : '').
                (!is_null($locationId2) ? ' OR estate.location_id = :locationId2 ' : '').
                (!is_null($locationId3) ? ' OR estate.location_id = :locationId3 ' : '').
                (!is_null($locationId4) ? ' OR estate.location_id = :locationId4 ' : '').
                (!is_null($locationId5) ? ' OR estate.location_id = :locationId5 ' : '')
                .' ) ' : '')

            ." AND
                estate_calendar_day.date >= :nowDate AND
                (
                  (
                    estate.type = :estateTypeStandard AND
                    estate_calendar_day_data_price.nights = :nights
                  )
                )
          GROUP BY CONCAT(estate_calendar_day.date, estate_calendar_day.estate_id)
        ");
        $nowDate = new \DateTime('now');
        $statement->bindValue('nowDate', $nowDate->format('Y-m-d'));
        $statement->bindValue('estateTypeStandard', Estate::TYPE_STANDARD_M_F);
        $statement->bindValue('nights', $nights);
        $statement->bindValue('estateMinId', $estateMinId);
        $statement->bindValue('estateMaxId', $estateMaxId);

        if(!is_null($locationId0)) $statement->bindValue('locationId0', $locationId0);
        if(!is_null($locationId1)) $statement->bindValue('locationId1', $locationId1);
        if(!is_null($locationId2)) $statement->bindValue('locationId2', $locationId2);
        if(!is_null($locationId3)) $statement->bindValue('locationId3', $locationId3);
        if(!is_null($locationId4)) $statement->bindValue('locationId4', $locationId4);
        if(!is_null($locationId5)) $statement->bindValue('locationId5', $locationId5);

        $statement->execute();
        $results = $statement->fetchAll();
        return $results;
    }

    public function getDays($nightsFlexible, $nightsStandard){
        $connection = $this->_em->getConnection();
        $statement = $connection->prepare("
          SELECT
              estate_calendar_day.*,
              estate.name as estate_name,
              estate.location_text as estate_location_text,
              estate.short_name as estate_short_name,
              estate.minimum_guests as estate_minimum_guests,
              estate_photo.file_name as estate_photo_file_name,
              CONCAT(
                estate_calendar_day_data_price.isValid, '|',
                estate_calendar_day_data_price.selectPrev, '|',
                estate_calendar_day_data_price.selectNext, '|',
                estate_calendar_day_data_price.price
              ) as dataPrice,
              estate_calendar_day_data_price.price as day_price
          FROM estate_calendar_day
          LEFT JOIN estate ON estate_calendar_day.estate_id = estate.id
          LEFT JOIN estate_calendar_day_data_price ON estate_calendar_day_data_price.estate_calendar_day_id = estate_calendar_day.id
          LEFT JOIN estate_photo ON estate.photo_main = estate_photo.id
          WHERE
                estate_calendar_day.date >= :nowDate AND
                (
                  (
                    estate.type = :estateTypeFlexible AND
                    estate_calendar_day_data_price.nights = :nightsFlexible
                  )
                  OR
                  (
                    estate.type = :estateTypeStandard AND
                    estate_calendar_day_data_price.nights = :nightsStandard
                  )
                )
          GROUP BY CONCAT(estate_calendar_day.date, estate_calendar_day.estate_id)
        ");
        $nowDate = new \DateTime('now');
        $statement->bindValue('nowDate', $nowDate->format('Y-m-d'));
        $statement->bindValue('estateTypeFlexible', Estate::TYPE_FLEXIBLE_BOOKING);
        $statement->bindValue('estateTypeStandard', Estate::TYPE_STANDARD_M_F);
        $statement->bindValue('nightsFlexible', $nightsFlexible);
        $statement->bindValue('nightsStandard', $nightsStandard);
        $statement->execute();
        $results = $statement->fetchAll();
        return $results;
    }
}
