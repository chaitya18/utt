<?php

namespace UTT\EstateBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * EstateFeature
 *
 * @ORM\Table(name="estate_feature")
 * @ORM\Entity(repositoryClass="UTT\EstateBundle\Entity\EstateFeatureRepository")
 */
class EstateFeature
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="UTT\EstateBundle\Entity\EstateFeatureCategory")
     * @ORM\JoinColumn(name="estate_feature_category_id", referencedColumnName="id", nullable=true, onDelete="SET NULL")
     */
    protected $estateFeatureCategory;

    /**
     * @ORM\Column(name="name", type="string", length=255, nullable=true)
     */
    protected $name;

    /**
     * @ORM\Column(name="icon", type="string", length=255, nullable=true)
     */
    protected $icon;
    public $file;

#--------------------------------------------------- ENTITY METHODS ---------------------------------------------------#

    public function __toString(){
        if($this->getName())
            return $this->getName();
        return '';
    }

    public function getUploadDir(){
        return 'uploads/estateFeatureIcons';
    }

    protected function getUploadRootDir($basepath = null){
        return $basepath.$this->getUploadDir();
    }

    public function uploadIcon($basepath = null){
        if (null === $this->file) {
            return;
        }

        if (null === $basepath) {
            return;
        }
        $name =  uniqid().substr($this->file->getClientOriginalName(),-4);
        $this->file->move($this->getUploadRootDir($basepath), $name);
        $this->setIcon($name);
        $this->file = null;
    }

#################################################### ENTITY METHODS ####################################################

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return EstateFeature
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set icon
     *
     * @param string $icon
     * @return EstateFeature
     */
    public function setIcon($icon)
    {
        $this->icon = $icon;

        return $this;
    }

    /**
     * Get icon
     *
     * @return string 
     */
    public function getIcon()
    {
        return $this->icon;
    }

    /**
     * Set estateFeatureCategory
     *
     * @param \UTT\EstateBundle\Entity\EstateFeatureCategory $estateFeatureCategory
     * @return EstateFeature
     */
    public function setEstateFeatureCategory(\UTT\EstateBundle\Entity\EstateFeatureCategory $estateFeatureCategory = null)
    {
        $this->estateFeatureCategory = $estateFeatureCategory;

        return $this;
    }

    /**
     * Get estateFeatureCategory
     *
     * @return \UTT\EstateBundle\Entity\EstateFeatureCategory 
     */
    public function getEstateFeatureCategory()
    {
        return $this->estateFeatureCategory;
    }
}
