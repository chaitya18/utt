<?php

namespace UTT\EstateBundle\Entity;

use Doctrine\ORM\EntityRepository;

class LocationRepository extends EntityRepository
{
    public function getAllArray(){
        $query = $this->_em->createQuery("SELECT l.id, l.name FROM UTTEstateBundle:Location l ORDER BY l.sortOrder ASC");
        return $query->getArrayResult();
    }
}
