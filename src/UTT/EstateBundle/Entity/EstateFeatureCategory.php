<?php

namespace UTT\EstateBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * EstateFeatureCategory
 *
 * @ORM\Table()
 * @ORM\Entity
 */
class EstateFeatureCategory
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(name="name", type="string", length=255, nullable=true)
     */
    protected $name;

    public function __toString(){
        if($this->getName())
            return $this->getName();
        return '';
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return EstateFeatureCategory
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }
}
