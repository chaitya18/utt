<?php

namespace UTT\EstateBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Show\ShowMapper;

use Sonata\AdminBundle\Route\RouteCollection;

use Knp\Menu\ItemInterface as MenuItemInterface;
use UTT\AdminBundle\Service\UTTAclService;

class ReviewAdmin extends Admin
{
    /** @var  UTTAclService */
    private $uttAclService;

    public function setUttAclService(UTTAclService $uttAclService){
        $this->uttAclService = $uttAclService;
    }

    /**
     * {@inheritdoc}
     */
    public function getFilterParameters()
    {
        $this->datagridValues = array_merge(array(
            '_sort_order' => 'DESC',
            '_sort_by' => 'createdAt',
        ), $this->datagridValues );
        return parent::getFilterParameters();
    }

    public function createQuery($context = 'list')
    {
        $user = $this->uttAclService->userAuth();
        if($user && $this->uttAclService->isGrantedOwner()){
            $query = parent::createQuery($context);
            $query->innerJoin($query->getRootAlias().'.estate', 'eeee');
            $query->innerJoin('eeee.ownerUsers', 'ououou', 'WITH', $query->expr()->in('ououou.id', ':userId'));
            $query->setParameter('userId', $user->getId());

            return $query;
        }

        return parent::createQuery($context);
    }

    /**
     * {@inheritdoc}
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->with('general information')
                //->add('id', null, array('required' => false, 'label' => 'ID', 'disabled' => true))
                ->add('createdAt', null, array('required' => true, 'label' => 'created at'))
                ->add('estate', null, array('required' => true, 'label' => 'estate', 'attr' => array('class' => 'form-control')))
                //->add('user', null, array('required' => true, 'label' => 'user', 'attr' => array('class' => 'form-control'), 'disabled' => true))
                ->add('comment', null, array('required' => true, 'label' => 'comment', 'attr' => array('class' => 'form-control')))
                ->add('isVerified', null, array('required' => false, 'label' => 'is verified?', 'attr' => array('class' => 'form-control')))
            ->end()
        ;
    }

    /**
     * {@inheritdoc}
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            //->addIdentifier('id', null, array('label' => 'ID'))
            ->add('createdAt', null, array('label' => 'created at'))
            ->add('estate', null, array('label' => 'estate'))
            ->add('user', null, array('label' => 'user', 'template' => 'UTTEstateBundle:EstateAdmin:list_type_reviewUser.html.twig'))
            ->addIdentifier('comment', null, array('label' => 'comment'))
            ->add('isVerified', null, array('label' => 'is verified?', 'editable' => true))
            ->add('_action', 'actions', array(
                'actions' => array(
                    'view' => array(),
                    'edit' => array(),
                    'delete' => array(),
                )
            ))
        ;
    }

    /**
     * {@inheritdoc}
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('estate', null, array('label' => 'estate'))
            ->add('isVerified', null, array('label' => 'is verified?'))
        ;
    }

    /**
     * @param RouteCollection $collection
     */
    public function configureRoutes(RouteCollection $collection)
    {
        $collection->remove('batch');
        $collection->remove('export');
        $collection->remove('acl');
        //$collection->remove('create');
        $collection->remove('show');
    }

}

?>
