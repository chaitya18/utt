<?php

namespace UTT\EstateBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Show\ShowMapper;

use Sonata\AdminBundle\Route\RouteCollection;

use Knp\Menu\ItemInterface as MenuItemInterface;

class EstateFeatureAdmin extends Admin
{
    /**
     * @param \Sonata\AdminBundle\Show\ShowMapper $showMapper
     *
     * @return void
     */
    protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper
            ->with('general information')
                ->add('name', null, array('label' => 'name'))
            ->end()
        ;
    }

    /**
     * @param \Sonata\AdminBundle\Form\FormMapper $formMapper
     *
     * @return void
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->with('general information')
                ->add('name', null, array('required' => true, 'label' => 'name', 'attr' => array('class' => 'form-control')))
                ->add('estateFeatureCategory', null, array('required' => true, 'label' => 'category', 'attr' => array('class' => 'form-control')))
                ->add('file', 'file', array('required' => false, 'label' => 'icon'))
                ->add('icon', null, array('label' => 'icon preview', 'attr' => array('class' => 'estateFeatureIconPreview', 'data-upload-dir' => $this->getSubject()->getUploadDir())))
            ->end()
        ;
    }

    /**
     * @param \Sonata\AdminBundle\Datagrid\ListMapper $listMapper
     *
     * @return void
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->add('name', null, array('label' => 'name'))
            ->add('estateFeatureCategory', null, array('label' => 'category'))
            ->add('_action', 'actions', array(
                'actions' => array(
                    'view' => array(),
                    'edit' => array(),
                    'delete' => array(),
                )
            ))
        ;
    }

    /**
     * @param \Sonata\AdminBundle\Datagrid\DatagridMapper $datagridMapper
     *
     * @return void
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('name', null, array('label' => 'name'))
        ;
    }

    /**
     * Remove possibility to delete object.
     *
     * @param RouteCollection $collection
     */
    public function configureRoutes(RouteCollection $collection)
    {
        $collection->remove('show');
        $collection->remove('delete');
        $collection->remove('acl');
    }

    public function prePersist($block){
        $this->saveIcon($block);
    }

    public function preUpdate($block){
        $this->saveIcon($block);
    }

    public function saveIcon($object){
        $basepath = $this->getRequest()->getBasePath();
        $object->uploadIcon($basepath);
    }
}

?>
