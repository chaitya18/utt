<?php

namespace UTT\EstateBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Show\ShowMapper;

use Sonata\AdminBundle\Route\RouteCollection;

use Knp\Menu\ItemInterface as MenuItemInterface;

class CategoryAdmin extends Admin
{
    /**
     * @param \Sonata\AdminBundle\Show\ShowMapper $showMapper
     *
     * @return void
     */
    protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper
            ->with('general information')
                ->add('name', null, array('label' => 'name'))
                ->add('isHomepage', null, array('label' => 'is visible on homepage'))
                ->add('isSearchFilter', null, array('label' => 'is search filter'))
                ->add('isHeaderMenu', null, array('label' => 'is in header menu'))
            ->end()
        ;
    }

    /**
     * @param \Sonata\AdminBundle\Form\FormMapper $formMapper
     *
     * @return void
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->with('general information')
                //->add('id', null, array('required' => false, 'label' => 'ID', 'disabled' => true))
                ->add('name', null, array('required' => true, 'label' => 'name', 'attr' => array('class' => 'form-control')))
                ->add('sortOrder', null, array('required' => false, 'label' => 'sort index', 'attr' => array('class' => 'form-control')))
                ->add('isHomepage', null, array('required' => false, 'label' => 'is visible on homepage', 'attr' => array('class' => 'form-control')))
                ->add('isSearchFilter', null, array('required' => false, 'label' => 'is search filter', 'attr' => array('class' => 'form-control')))
                ->add('isHeaderMenu', null, array('required' => false, 'label' => 'is in header menu', 'attr' => array('class' => 'form-control')))
                ->add('textDescription', null, array('required' => false, 'label' => 'description', 'attr' => array('class' => 'form-control')))
                ->add('slug', null, array('required' => true, 'label' => 'slug', 'attr' => array('class' => 'form-control')))
                ->add('file', 'file', array('required' => false, 'label' => 'image'))
                ->add('image', null, array('label' => 'image preview', 'attr' => array('class' => 'estateCategoryImagePreview', 'data-upload-dir' => $this->getSubject()->getUploadDir())))
            ->end()
        ;
    }

    /**
     * @param \Sonata\AdminBundle\Datagrid\ListMapper $listMapper
     *
     * @return void
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->add('name', null, array('label' => 'name'))
            ->add('isHomepage', null, array('label' => 'is visible on homepage'))
            ->add('isSearchFilter', null, array('label' => 'is search filter'))
            ->add('isHeaderMenu', null, array('label' => 'is in header menu'))
            ->add('_action', 'actions', array(
                'actions' => array(
                    'view' => array(),
                    'edit' => array(),
                    'delete' => array(),
                )
            ))
        ;
    }

    /**
     * @param \Sonata\AdminBundle\Datagrid\DatagridMapper $datagridMapper
     *
     * @return void
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            //->add('id', null, array('label' => 'ID'))
            ->add('name', null, array('label' => 'name'))
        ;
    }

    /**
     * Remove possibility to delete object.
     *
     * @param RouteCollection $collection
     */
    public function configureRoutes(RouteCollection $collection)
    {
        $collection->remove('acl');
    }

    public function prePersist($block){
        $this->saveImage($block);
    }

    public function preUpdate($block){
        $this->saveImage($block);
    }

    public function saveImage($object){
        $basepath = $this->getRequest()->getBasePath();
        $object->uploadImage($basepath);
    }
}

?>
