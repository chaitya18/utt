<?php

namespace UTT\EstateBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Show\ShowMapper;

use Sonata\AdminBundle\Route\RouteCollection;

use Knp\Menu\ItemInterface as MenuItemInterface;
use UTT\ReservationBundle\Service\PricingService;
use UTT\EstateBundle\Entity\Estate;
use UTT\ReservationBundle\Entity\PricingCategoryRepository;
use UTT\ReservationBundle\Entity\PricingCategory;
use FOS\UserBundle\Model\UserInterface;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use UTT\AdminBundle\Service\UTTAclService;
use UTT\QueueBundle\Service\LateAvailabilityOffersQueueService;
use UTT\EstateBundle\Entity\Photo;
use Doctrine\ORM\EntityManager;
use \Symfony\Component\Routing\Router;

class EstateAdmin extends Admin
{
    protected $maxPerPage = 150;
    protected $maxPageLinks = 150;

    /** @var  UTTAclService */
    private $uttAclService;
    /** @var  Router */
    private $router;

    public function setUttAclService(UTTAclService $uttAclService){
        $this->uttAclService = $uttAclService;
    }

    public function setRouter($router){
        $this->router = $router;
    }
    /**
     * {@inheritdoc}
     */
    public function getFilterParameters()
    {
        $this->datagridValues = array_merge(array(
            '_per_page' => 150
        ), $this->datagridValues );
        return parent::getFilterParameters();
    }

    /**
     * Create query
     * @param string $context
     *
     * @return \Sonata\AdminBundle\Datagrid\ProxyQueryInterface
     */
    public function createQuery($context = 'list')
    {
        $user = $this->uttAclService->userAuth();
        if($user && $this->uttAclService->isGrantedOwner()){
            $query = parent::createQuery($context);
            $query->innerJoin($query->getRootAlias().'.ownerUsers', 'ououou', 'WITH', $query->expr()->in('ououou.id', ':userId'));
            $query->setParameter('userId', $user->getId());

            return $query;
        }elseif($user && $this->uttAclService->isGrantedCleaner()){
            $query = parent::createQuery($context);
            $query->innerJoin($query->getRootAlias().'.cleanerUsers', 'cucucucu', 'WITH', $query->expr()->in('cucucucu.id', ':userId'));
            $query->setParameter('userId', $user->getId());

            return $query;
        }

        return parent::createQuery($context);
    }

    public function getTemplate($name){
        switch ($name) {
            case 'edit':
                return 'UTTEstateBundle:EstateAdmin:edit.html.twig';
                break;
            default:
                return parent::getTemplate($name);
                break;
        }
    }

    protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper
            ->with('general information')
                ->add('id', null, array('label' => 'ID'))
                ->add('isJustLaunched', null, array('label' => 'added to new babies?'))
                ->add('name', null, array('label' => 'name'))
                ->add('shortName', null, array('label' => 'short name'))
                ->add('ownerBookingsCategory', null, array('label' => 'owner bookings category'))
                ->add('categories', null, array('label' => 'categories'))
                ->add('estateFeatures', null, array('label' => 'property features'))
                ->add('nearByEstates', null, array('label' => 'nearby properties'));
        if($this->uttAclService->isGrantedSuperAdmin()){
            $showMapper
                ->add('hideFromDisplay', null, array('label' => 'hide from display'))
                ->add('isCategoryFeatured', null, array('label' => 'show as featured on category page'))
                ->add('specialCategory', null, array('label' => 'special category'))
                ->add('isSaturdayOnly', null, array('label' => 'is saturday only?'))
                ->add('type', null, array('label' => 'change over policy'))
                ->add('pricingCategory', null, array('label' => 'pricing category'));
        }

        $showMapper
            ->with('accommodation options')
                ->add('sleeps', null, array('label' => 'sleeps'))
                ->add('minimumGuests', null, array('label' => 'minimum guests'))
                ->add('roomLayout', null, array('label' => 'room layout'))
                ->add('children', null, array('label' => 'children (2-18)'))
                ->add('infants', null, array('label' => 'infants'))
                ->add('pets', null, array('label' => 'pets'))
                ->add('checkInTime', null, array('label' => 'check in time'))
                ->add('checkOutTime', null, array('label' => 'check out time'));
        if($this->uttAclService->isGrantedSuperAdmin()){
            $showMapper
                ->with('accommodation options')
                    ->add('min7NightsPricingSeasons', null, array('label' => 'min 7 nights stay pricing seasons'))
                ->with('availability discount')
                    ->add('discountPeriod', null, array('label' => 'discount period', 'help' => '(recommended 10 days)'))
                    ->add('discountValue', null, array('label' => 'discount value', 'help' => '(recommended 20%)'))
                    ->add('noticePeriod', 'choice', array('label' => 'notice period'));
        }
        $showMapper
            ->with('contact information')
                ->add('keySafeCode', null, array('label' => 'key safe code'))
                ->add('location', null, array('label' => 'location - country'))
                ->add('locationText', null, array('label' => 'location - text'))
                ->add('address', null, array('label' => 'property address’', 'help' => 'Please enter FULL address'))
                ->add('postcode', null, array('label' => 'post code'))
                ->add('lat', null, array('label' => 'Google loc. - Lat'))
                ->add('lon', null, array('label' => 'Google loc. - Lon'))
                ->add('bookingNotes', null, array('label' => 'booking notes (web)'))
                ->add('accommodationSubtitle', null, array('label' => 'accommodation subtitle (web)'))
            ->with('star UTT ratings')
                ->add('starRatingLuxury', 'choice', array('label' => 'star rating luxury'))
                ->add('starRatingHeritage', 'choice', array('label' => 'star rating heritage'))
                ->add('starRatingUnique', 'choice', array('label' => 'star rating unique'))
                ->add('starRatingGreen', 'choice', array('label' => 'star rating green'))
                ->add('starRatingPrice', 'choice', array('label' => 'star rating price'));
        if($this->uttAclService->isGrantedSuperAdmin()){
            $showMapper
                ->with('contact information')
                    ->add('administratorUsers', null, array('label' => 'administrators'))
                    ->add('ownerUsers', null, array('label' => 'owners email'))
                    ->add('cleanerUsers', null, array('label' => 'cleaners'))
                    ->add('landlordAddress', null, array('label' => 'landlord address'))
                    ->add('bankAccountNumber', null, array('label' => 'bank account number', 'help' => 'Please store as 0000 / 00-00-00 / 00000000 (Mandate / Sort / Account)'))
                    ->add('bankIBAN', null, array('label' => 'IBAN'))
                    ->add('bankBIC', null, array('label' => 'BIC/Swift'));
        }
        $showMapper
            ->with('contact information')
                ->add('phone1Name', null, array('label' => 'contact number #1 - name'))
                ->add('phone1', null, array('label' => 'contact number #1'))
                ->add('phone2Name', null, array('label' => 'contact number #2 - name'))
                ->add('phone2', null, array('label' => 'contact number #2'))
                ->add('phone3Name', null, array('label' => 'contact number #3 - name'))
                ->add('phone3', null, array('label' => 'contact number #3'))
                ->add('phone4Name', null, array('label' => 'contact number #4 - name'))
                ->add('phone4', null, array('label' => 'contact number #4'))
                ->add('phone5Name', null, array('label' => 'contact number #5 - name'))
                ->add('phone5', null, array('label' => 'contact number #5'))
                ->add('phone6Name', null, array('label' => 'contact number #6 - name'))
                ->add('phone6', null, array('label' => 'contact number #6'));
        if($this->uttAclService->isGrantedSuperAdmin()){
            $showMapper
                ->with('UTT information')
                    ->add('uttCommissionRate', null, array('label' => 'UTT commission rate', 'help' => '% (before VAT)'))
                    ->add('uttBookingFee', null, array('label' => 'UTT Booking Fee', 'help' => '% of the booking price'))
                    ->add('chequesTo', null, array('label' => 'cheques to'))
                    ->add('uttAdminNotes', null, array('label' => 'UTT admin notes only'));
        }
        /*
        $showMapper
            ->with('pricing management')
                ->add('pricingGenerate', null, array('label' => 'pricing generate', 'template' => 'UTTEstateBundle:EstateAdmin:show_type_pricing_generate.html.twig'))
                ->add('pricing', null, array('label' => 'pricing', 'template' => 'UTTEstateBundle:EstateAdmin:show_type_actions.html.twig'))
            ->end()
        ;
        */
    }

    /**
     * @param \Sonata\AdminBundle\Form\FormMapper $formMapper
     *
     * @return void
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $hideFromDisplayChoices = $this->getSubject()->getAllowedHideFromDisplays();
        $specialCategoryChoices = $this->getSubject()->getAllowedSpecialCategories();
        $typeChoices = $this->getSubject()->getAllowedTypes();
        $typeDisabled = false;
        if($this->getSubject()->getId() && $this->getSubject()->getType()){
            $typeDisabled = true;
        }
        $childrenChoices = $this->getSubject()->getAllowedChildren();
        $infantsChoices = $this->getSubject()->getAllowedInfants();
        $petsChoices = $this->getSubject()->getAllowedPets();
        $checkInTimeChoices = $checkOutTimeChoices = array(
            '00:00'=>'00:00', '00:30'=>'00:30',
            '01:00'=>'01:00', '01:30'=>'01:30',
            '02:00'=>'02:00', '02:30'=>'02:30',
            '03:00'=>'03:00', '03:30'=>'03:30',
            '04:00'=>'04:00', '04:30'=>'04:30',
            '05:00'=>'05:00', '05:30'=>'05:30',
            '06:00'=>'06:00', '06:30'=>'06:30',
            '07:00'=>'07:00', '07:30'=>'07:30',
            '08:00'=>'08:00', '08:30'=>'08:30',
            '09:00'=>'09:00', '09:30'=>'09:30',
            '10:00'=>'10:00', '10:30'=>'10:30',
            '11:00'=>'11:00', '11:30'=>'11:30',
            '12:00'=>'12:00', '12:30'=>'12:30',
            '13:00'=>'13:00', '13:30'=>'13:30',
            '14:00'=>'14:00', '14:30'=>'14:30',
            '15:00'=>'15:00', '15:30'=>'15:30',
            '16:00'=>'16:00', '16:30'=>'16:30',
            '17:00'=>'17:00', '17:30'=>'17:30',
            '18:00'=>'18:00', '18:30'=>'18:30',
            '19:00'=>'19:00', '19:30'=>'19:30',
            '20:00'=>'20:00', '20:30'=>'20:30',
            '21:00'=>'21:00', '21:30'=>'21:30',
            '22:00'=>'22:00', '22:30'=>'22:30',
            '23:00'=>'23:00', '23:30'=>'23:30'
        );

        $checkInTimeData = false;
        if($this->getSubject()) if($this->getSubject()->getCheckInTime())  $checkInTimeData = $this->getSubject()->getCheckInTime()->format('H:i');
        $checkOutTimeData = false;
        if($this->getSubject()) if($this->getSubject()->getCheckOutTime())  $checkOutTimeData = $this->getSubject()->getCheckOutTime()->format('H:i');

        $noticePeriodChoices = $this->getSubject()->getAllowedNoticePeriods();

        $formMapper
            ->with('general information')
                ->add('id', null, array('required' => false, 'label' => 'ID', 'disabled' => true, 'attr' => array('class' => 'sonataFormEstateId')));

        /** @var PricingCategoryRepository $pricingCategoryRepository */
        $pricingCategoryRepository = $this->getConfigurationPool()->getContainer()->get('doctrine')->getManager()->getRepository('UTTReservationBundle:PricingCategory');
        $estateType = $this->getSubject()->getType();

        $pricingCategories = false;
        if($estateType == Estate::TYPE_FLEXIBLE_BOOKING){
            $pricingCategories = $pricingCategoryRepository->findBy(array('type' => PricingCategory::TYPE_FLEXIBLE_BOOKING));
        }elseif($estateType == Estate::TYPE_STANDARD_M_F){
            $pricingCategories = $pricingCategoryRepository->findBy(array('type' => PricingCategory::TYPE_STANDARD_M_F));
        }

        $photoMainId = null;
        if($this->getSubject()->getPhotoMain()){
            $photoMainId = $this->getSubject()->getPhotoMain()->getId();
        }

        if($this->uttAclService->isGrantedSuperAdmin()){
//            $formMapper
//                ->add('externalId', null, array('required' => false, 'label' => 'external ID', 'attr' => array('class' => 'form-control')));
        }


        $categoryRepository = $this->getConfigurationPool()->getContainer()->get('doctrine')->getEntityManager()->getRepository("UTTEstateBundle:Category");
        $categoryChoicesPreferred = $categoryRepository->getPreferredSorted();
        $categoryChoicesAll = $categoryRepository->findAll();

        $formMapper
                ->add('isJustLaunched', null, array('required' => false, 'label' => 'added to new babies?'))
                ->add('isMultiproperty', null, array('required' => false, 'label' => 'is multiproperty?'))
                ->add('subEstates', null, array('required' => false, 'label' => 'subproperties'))
                ->add('name', null, array('required' => true, 'label' => 'name', 'attr' => array('class' => 'form-control')))
                ->add('shortName', null, array('required' => true, 'label' => 'short name', 'attr' => array('class' => 'form-control')))
                ->add('seoDescription', null, array('required' => false, 'label' => 'seo description', 'attr' => array('class' => 'form-control')))
                ->add('categories', null, array('required' => false, 'expanded' => true, 'label' => 'categories',
                    'choices' => $categoryChoicesAll, 'preferred_choices'=> $categoryChoicesPreferred, 'attr' => array('class' => 'categoriesSeparatorInit', 'data-separator-preferred' => count($categoryChoicesPreferred))
                ))
                ->add('defaultCategory', null, array('required' => true, 'label' => 'default category', 'attr' => array('class' => 'form-control')))
                ->add('estateFeatures', null, array('required' => false, 'label' => 'property features', 'attr' => array('class' => 'form-control')))
                ->add('nearByEstates', null, array('required' => false, 'label' => 'nearby properties', 'attr' => array('class' => 'form-control')));
        if($this->uttAclService->isGrantedSuperAdmin()){
            $formMapper
                ->add('ownerBookingsCategory', null, array('required' => true, 'label' => 'owner bookings category', 'attr' => array('class' => 'form-control')))
                ->add('isCategoryFeatured', null, array('label' => 'show as featured on category page', 'required' => false))
                ->add('hideFromDisplay', 'choice', array('required' => true, 'label' => 'hide from display', 'choices' => $hideFromDisplayChoices, 'attr' => array('class' => 'form-control')))
                ->add('specialCategory', 'choice', array('required' => true, 'label' => 'special category', 'choices' => $specialCategoryChoices, 'attr' => array('class' => 'form-control')))
                ->add('isSaturdayOnly', null, array('required' => false, 'label' => 'is saturday only?'))
                ->add('type', 'choice', array('required' => true, 'label' => 'change over policy', 'choices' => $typeChoices, 'disabled' => $typeDisabled, 'attr' => array('class' => 'form-control')));
            if($pricingCategories){
                $formMapper->add('pricingCategory', 'entity', array('required' => false, 'label' => 'pricing category', 'attr' => array('class' => 'form-control'), 'class' => 'UTTReservationBundle:PricingCategory', 'choices' => $pricingCategories, 'data' => $this->getSubject()->getPricingCategory()));
            }
        }

        $airbnbExportUrl = '';
        if($this->getSubject()->getAirbnbExport() && $this->getSubject()->getShortName() && $this->getSubject()->getAirbnbExportPassword()){
            $airbnbExportUrl =  $this->router->generate('utt_estate_airbnb_ics', array(
                'estateShortName' => $this->getSubject()->getShortName(),
                'airbnbExportPassword' => $this->getSubject()->getAirbnbExportPassword()
            ), true);
        }

        if($this->uttAclService->isGrantedSuperAdmin()){
            $formMapper
                ->with('airbnb sync')
                    ->add('airbnbCalendar', null, array('required' => false, 'label' => 'airbnb import - calendar url', 'attr' => array('class' => 'form-control')))
                    ->add('airbnbExport', null, array('required' => false, 'label' => 'airbnb export', 'attr' => array('class' => 'form-control')))
                    ->add('airbnbExportPassword', null, array('required' => false, 'label' => 'airbnb export - password', 'attr' => array('class' => 'form-control')))
                    ->add('_airbnbExportUrl', 'text', array('mapped' => false, 'read_only' => true, 'required' => false, 'label' => 'airbnb export - url', 'data' => $airbnbExportUrl, 'attr' => array('class' => 'form-control')))
            ;
        }

        $formMapper
            ->with('accommodation options')
                ->add('sleeps', 'choice', array('required' => true, 'label' => 'sleeps', 'choices' => array(
                    0=>0, 1=>1, 2=>2, 3=>3, 4=>4, 5=>5, 6=>6, 7=>7, 8=>8, 9=>9, 10=>10, 11=>11, 12=>12, 13=>13, 14=>14, 15=>15, 16=>16, 17=>17, 18=>18, 19=>19, 20=>20, 21=>21, 22=>22, 23=>23, 24=>24, 25=>25, 26=>26, 27=>27, 28=>28
                ), 'attr' => array('class' => 'form-control')))
                ->add('minimumGuests', 'choice', array('required' => true, 'label' => 'minimum guests', 'choices' => array(
                    1=>1, 2=>2, 3=>3, 4=>4, 5=>5, 6=>6, 7=>7, 8=>8, 9=>9, 10=>10, 11=>11, 12=>12, 13=>13, 14=>14, 15=>15, 16=>16, 17=>17, 18=>18, 19=>19, 20=>20, 21=>21, 22=>22, 23=>23, 24=>24, 25=>25, 26=>26, 27=>27, 28=>28
                ), 'attr' => array('class' => 'form-control')))
                ->add('roomLayout', null, array('required' => false, 'label' => 'room layout', 'attr' => array('class' => 'form-control')))
                ->add('children', 'choice', array('required' => true, 'label' => 'children (2-18)', 'choices' => $childrenChoices, 'attr' => array('class' => 'form-control')))
                ->add('infants', 'choice', array('required' => true, 'label' => 'infants', 'choices' => $infantsChoices, 'attr' => array('class' => 'form-control')))
                ->add('pets', 'choice', array('required' => true, 'label' => 'pets', 'choices' => $petsChoices, 'attr' => array('class' => 'form-control')))
                ->add('checkInTime', 'choice', array('required' => true, 'label' => 'check in time', 'choices' => $checkInTimeChoices, 'data' => $checkInTimeData, 'attr' => array('class' => 'form-control')))
                ->add('checkOutTime', 'choice', array('required' => true, 'label' => 'check out time', 'choices' => $checkOutTimeChoices, 'data' => $checkOutTimeData, 'attr' => array('class' => 'form-control')));
        if($this->uttAclService->isGrantedSuperAdmin()){
            $formMapper
                ->with('accommodation options')
                    ->add('min7NightsPricingSeasons', null, array('required' => false, 'multiple' => true, 'expanded' => true, 'label' => 'min 7 nights stay pricing seasons'))
                ->with('availability discount')
                    ->add('discountPeriod', null, array('required' => true, 'data' => ($this->getSubject()->getId() ? $this->getSubject()->getDiscountPeriod() : 10), 'label' => 'discount period', 'help' => '(recommended 10 days)', 'attr' => array('class' => 'form-control')))
                    ->add('discountValue', null, array('required' => true, 'data' => ($this->getSubject()->getId() ? $this->getSubject()->getDiscountValue() : 20), 'label' => 'discount value', 'help' => '(recommended 20%)', 'attr' => array('class' => 'form-control')))
                    ->add('noticePeriod', 'choice', array('required' => true, 'label' => 'notice period', 'choices' => $noticePeriodChoices, 'attr' => array('class' => 'form-control')));
        }

        $starRatingLuxury = $this->getSubject()->getStarRatingLuxury() ? $this->getSubject()->getStarRatingLuxury() : 0;
        $starRatingHeritage = $this->getSubject()->getStarRatingHeritage() ? $this->getSubject()->getStarRatingHeritage() : 0;
        $starRatingUnique = $this->getSubject()->getStarRatingUnique() ? $this->getSubject()->getStarRatingUnique() : 0;
        $starRatingGreen = $this->getSubject()->getStarRatingGreen() ? $this->getSubject()->getStarRatingGreen() : 0;
        $starRatingPrice = $this->getSubject()->getStarRatingPrice() ? $this->getSubject()->getStarRatingPrice() : 0;

        $formMapper
            ->with('contact information')
                ->add('keySafeCode', null, array('required' => false, 'label' => 'key safe code', 'attr' => array('class' => 'form-control')))
                ->add('location', null, array('required' => true, 'label' => 'location - country', 'attr' => array('class' => 'form-control')))
                ->add('locationText', null, array('required' => false, 'label' => 'location - text', 'attr' => array('class' => 'form-control')))
                ->add('address', null, array('required' => false, 'label' => 'property address', 'help' => 'Please enter FULL address', 'attr' => array('class' => 'form-control')))
                ->add('postcode', null, array('required' => false, 'label' => 'post code', 'attr' => array('class' => 'form-control')))
                ->add('lat', null, array('required' => true, 'data' => ($this->getSubject()->getId() ? $this->getSubject()->getLat() : 0), 'label' => 'Google loc. - Lat', 'attr' => array('class' => 'form-control')))
                ->add('lon', null, array('required' => true, 'data' => ($this->getSubject()->getId() ? $this->getSubject()->getLon() : 0), 'label' => 'Google loc. - Lon', 'attr' => array('class' => 'form-control')))
                ->add('bookingNotes', null, array('required' => false, 'label' => 'booking notes (web)', 'attr' => array('class' => 'form-control')))
                ->add('accommodationSubtitle', null, array('required' => false, 'label' => 'accommodation subtitle (web)', 'attr' => array('class' => 'form-control')))
            ->with('star UTT ratings')
                ->add('starRatingLuxury', 'choice', array('required' => true, 'label' => 'star rating luxury', 'expanded' => true, 'data' => $starRatingLuxury, 'choices' => array(
                    0=>0, 1=>1, 2=>2, 3=>3, 4=>4, 5=>5
                )))
                ->add('starRatingHeritage', 'choice', array('required' => true, 'label' => 'star rating heritage', 'expanded' => true, 'data' => $starRatingHeritage,  'choices' => array(
                    0=>0, 1=>1, 2=>2, 3=>3, 4=>4, 5=>5
                )))
                ->add('starRatingUnique', 'choice', array('required' => true, 'label' => 'star rating unique', 'expanded' => true, 'data' => $starRatingUnique,  'choices' => array(
                    0=>0, 1=>1, 2=>2, 3=>3, 4=>4, 5=>5
                )))
                ->add('starRatingGreen', 'choice', array('required' => true, 'label' => 'star rating green', 'expanded' => true, 'data' => $starRatingGreen,  'choices' => array(
                    0=>0, 1=>1, 2=>2, 3=>3, 4=>4, 5=>5
                )))
                ->add('starRatingPrice', 'choice', array('required' => true, 'label' => 'star rating price', 'expanded' => true, 'data' => $starRatingPrice,  'choices' => array(
                    0=>0, 1=>1, 2=>2, 3=>3, 4=>4, 5=>5
                )));
        if($this->uttAclService->isGrantedSuperAdmin()){
            $formMapper
                ->with('contact information')
                    ->add('administratorUsers', null, array('required' => false, 'label' => 'administrators', 'attr' => array('class' => 'form-control'),
                        'choices' => $this->getConfigurationPool()->getContainer()->get('doctrine')->getEntityManager()->getRepository("UTTUserBundle:User")->getUsersWithType()
                    ))
                    //->add('ownerUsers', null, array('required' => false, 'label' => 'accommodation owners', 'attr' => array('class' => 'form-control')))
                    ->add('ownerUsers', null, array('required' => false, 'label' => 'owners email', 'attr' => array('class' => 'form-control'),
                        'choices' => $this->getConfigurationPool()->getContainer()->get('doctrine')->getEntityManager()->getRepository("UTTUserBundle:User")->getUsersWithType()
                    ))
                    ->add('landlordAddress', null, array('required' => false, 'label' => 'landlord address', 'attr' => array('class' => 'form-control')))
                    ->add('bankAccountNumber', null, array('required' => false, 'label' => 'bank account number', 'help' => 'Please store as 0000 / 00-00-00 / 00000000 (Mandate / Sort / Account)', 'attr' => array('class' => 'form-control')))
                    ->add('bankIBAN', null, array('required' => false, 'label' => 'IBAN', 'attr' => array('class' => 'form-control')))
                    ->add('bankBIC', null, array('required' => false, 'label' => 'BIC/Swift', 'attr' => array('class' => 'form-control')));
        }
        $formMapper
            ->with('contact information')
                //->add('cleanerUsers', null, array('required' => false, 'label' => 'cleaners', 'attr' => array('class' => 'form-control')))
                ->add('cleanerUsers', null, array('required' => false, 'label' => 'cleaners', 'attr' => array('class' => 'form-control'),
                    'choices' => $this->getConfigurationPool()->getContainer()->get('doctrine')->getEntityManager()->getRepository("UTTUserBundle:User")->getUsersWithType()
                ))
                ->add('phone1Name', null, array('required' => false, 'label' => 'contact number #1 - name', 'attr' => array('class' => 'form-control')))
                ->add('phone1', null, array('required' => false, 'label' => 'contact number #1', 'attr' => array('class' => 'form-control')))
                ->add('phone2Name', null, array('required' => false, 'label' => 'contact number #2 - name', 'attr' => array('class' => 'form-control')))
                ->add('phone2', null, array('required' => false, 'label' => 'contact number #2', 'attr' => array('class' => 'form-control')))
                ->add('phone3Name', null, array('required' => false, 'label' => 'contact number #3 - name', 'attr' => array('class' => 'form-control')))
                ->add('phone3', null, array('required' => false, 'label' => 'contact number #3', 'attr' => array('class' => 'form-control')))
                ->add('phone4Name', null, array('required' => false, 'label' => 'contact number #4 - name', 'attr' => array('class' => 'form-control')))
                ->add('phone4', null, array('required' => false, 'label' => 'contact number #4', 'attr' => array('class' => 'form-control')))
                ->add('phone5Name', null, array('required' => false, 'label' => 'contact number #5 - name', 'attr' => array('class' => 'form-control')))
                ->add('phone5', null, array('required' => false, 'label' => 'contact number #5', 'attr' => array('class' => 'form-control')))
                ->add('phone6Name', null, array('required' => false, 'label' => 'contact number #6 - name', 'attr' => array('class' => 'form-control')))
                ->add('phone6', null, array('required' => false, 'label' => 'contact number #6', 'attr' => array('class' => 'form-control')));
        if($this->uttAclService->isGrantedSuperAdmin()){
            $formMapper
                ->with('UTT information')
                ->add('uttCommissionRate', null, array('required' => true, 'data' => ($this->getSubject()->getId() ? $this->getSubject()->getUttCommissionRate() : 0), 'label' => 'UTT commission rate', 'help' => '% (before VAT)', 'attr' => array('class' => 'form-control')))
                ->add('uttBookingFee', null, array('required' => false, 'label' => 'UTT Booking Fee', 'help' => '% of the booking price. when empty system applies default value from the configuration.', 'attr' => array('class' => 'form-control')))
                ->add('chequesTo', null, array('required' => false, 'label' => 'cheques to', 'attr' => array('class' => 'form-control')))
                ->add('uttAdminNotes', null, array('required' => false, 'label' => 'UTT admin notes only', 'attr' => array('class' => 'form-control')));
        }
        $formMapper
//            ->with('photos')
//                ->add('estatePhotosUploader', 'text', array('mapped' => false, 'required' => false, 'label' => 'photos', 'attr' => array('class' => 'estatePhotosUploader')))
//                ->add('estatePhotosFacebookAccessToken', 'text', array('mapped' => false, 'required' => false, 'label' => 'import from Facebook', 'attr' => array('placeholder' => 'Facebook access token', 'class' => 'estatePhotosFacebookAccessToken')))
//                ->add('estatePhotosFacebookImporter', 'text', array('mapped' => false, 'required' => false, 'label' => 'import from Facebook', 'attr' => array('placeholder' => 'Facebook photo album ID', 'class' => 'estatePhotosFacebookImporter')))
//                ->add('photoMainContainer', 'hidden', array('required' => false, 'data' => $photoMainId, 'attr' => array('class' => 'photoMainContainer')))
            ->with('text blocks')
                ->add('textAccommodation', 'ckeditor', array('required' => false, 'label' => 'accommodation', 'attr' => array('class' => 'form-control')))
                ->add('textInDetail', 'ckeditor', array('required' => false, 'label' => 'in detail', 'attr' => array('class' => 'form-control')))
                ->add('textSustainability', 'ckeditor', array('required' => false, 'label' => 'sustainability', 'attr' => array('class' => 'form-control')))
                ->add('textWelshDescription', 'ckeditor', array('required' => false, 'label' => 'welsh description', 'attr' => array('class' => 'form-control')))
                ->add('textLocalArea', 'ckeditor', array('required' => false, 'label' => 'local area', 'attr' => array('class' => 'form-control')))
                ->add('textTravelInfo', 'ckeditor', array('required' => false, 'label' => 'travel info', 'attr' => array('class' => 'form-control')))
                //->add('textCustomerComments', 'ckeditor', array('required' => false, 'label' => 'customer comments', 'attr' => array('class' => 'form-control')))
                ->add('textArrivalInstructionsDirections', 'ckeditor', array('required' => false, 'label' => 'arrival instructions - directions', 'attr' => array('class' => 'form-control')))
                ->add('textArrivalInstructionsPropertySpecific', 'ckeditor', array('required' => false, 'label' => 'arrival instructions - property specific', 'attr' => array('class' => 'form-control')))
            ->end()
        ;
    }

    /**
     * @param \Sonata\AdminBundle\Datagrid\ListMapper $listMapper
     *
     * @return void
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            //->add('id', null, array('label' => 'ID'))
            ->add('_action', 'actions', array(
                'actions' => array(
                    'calendar' => array('template' => 'UTTEstateBundle:EstateAdmin:list__action_calendar.html.twig'),
                    'photos' => array('template' => 'UTTEstateBundle:EstateAdmin:list__action_photos.html.twig'),
                    'view' => array('template' => 'UTTEstateBundle:EstateAdmin:list__action_show.html.twig'),
                    'edit' => array(),
                    'delete' => array(),
                )
            ))
            ->addIdentifier('name', null, array('label' => 'name'));
        if(!($this->uttAclService->isGrantedCleaner())){
            $listMapper
                ->addIdentifier('shortName', null, array('label' => 'short name'))
                ->add('pricingCategory', null, array('label' => 'pricing category'))
                ->add('categories', null, array('label' => 'categories'))
                ->add('checkInTime', 'date', array('required' => true, 'label' => 'check in time', 'format' => 'H:i'))
                ->add('checkOutTime', 'date', array('required' => true, 'label' => 'check out time', 'format' => 'H:i'))
                ->add('hideFromDisplay', null, array('label' => 'hide from display', 'template' => 'UTTEstateBundle:EstateAdmin:list_type_hideFromDisplay.html.twig'))
                ->add('isCategoryFeatured', null, array('label' => 'show as featured on category page'))
                ->add('isJustLaunched', null, array('label' => 'added to new babies?', 'editable' => true))
            ;
        }
    }

    /**
     * @param \Sonata\AdminBundle\Datagrid\DatagridMapper $datagridMapper
     *
     * @return void
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            //->add('id', null, array('label' => 'ID'))
            ->add('name', null, array('label' => 'name'))
            ->add('pricingCategory', null, array('label' => 'pricing category'));
        ;
    }

    /**
     * Remove possibility to delete object.
     *
     * @param RouteCollection $collection
     */
    public function configureRoutes(RouteCollection $collection)
    {
        $collection->remove('delete');
        $collection->remove('batch');
        $collection->remove('acl');
        $collection->add('generate-pricing', $this->getRouterIdParameter().'/generate-pricing');
        $collection->add('calendar', $this->getRouterIdParameter().'/calendar');
        $collection->add('reset-pricing-type', $this->getRouterIdParameter().'/reset-pricing-type');
        $collection->add('photos', $this->getRouterIdParameter().'/photos');
        $collection->add('pricing-management', $this->getRouterIdParameter().'/pricing-management');
    }

    public function preUpdate($object){
        if($object->getCheckInTime() instanceof \DateTime) $object->setCheckInTime($object->getCheckInTime());
            else $object->setCheckInTime(new \DateTime($object->getCheckInTime()));
        if($object->getCheckOutTime() instanceof \DateTime) $object->setCheckOutTime($object->getCheckOutTime());
            else $object->setCheckOutTime(new \DateTime($object->getCheckOutTime()));

        if($object->getShortName()) $object->setShortName(strtolower($object->getShortName()));
        if(!$object->getSeoDescription()) $object->setSeoDescription($object->getName());

        $this->addToLateAvailabilityOffersQueue($object);
    }

    public function prePersist($object){
        /** @var Estate $object */

        $object->setCheckInTime(new \DateTime($object->getCheckInTime()));
        $object->setCheckOutTime(new \DateTime($object->getCheckOutTime()));

        if($object->getShortName()) $object->setShortName(strtolower($object->getShortName()));
        if(!$object->getSeoDescription()) $object->setSeoDescription($object->getName());
    }

    private function addToLateAvailabilityOffersQueue($object){
        if($this->getSubject()->getHideFromDisplay() == Estate::HIDE_FROM_DISPLAY_VISIBLE){
            /** @var LateAvailabilityOffersQueueService $lateAvailabilityOffersQueueService */
            $lateAvailabilityOffersQueueService = $this->getConfigurationPool()->getContainer()->get('utt.lateavailabilityoffersqueueservice');
            $lateAvailabilityOffersQueueService->pushToQueue($object);
        }
    }
}

?>
