<?php

namespace UTT\EstateBundle\Service;

use Doctrine\ORM\EntityManager;
use UTT\EstateBundle\Entity\Estate;
use UTT\ReservationBundle\Entity\Reservation;
use UTT\ReservationBundle\Entity\Pricing;
use UTT\ReservationBundle\Entity\Offer;
use UTT\ReservationBundle\Entity\PricingSeason;
use UTT\ReservationBundle\Service\ReservationService;
use UTT\ReservationBundle\Service\PricingService;
use UTT\ReservationBundle\Entity\OfferRepository;
use UTT\EstateBundle\Entity\EstateCalendarDay;
use UTT\EstateBundle\Entity\EstateCalendarDayRepository;
use UTT\EstateBundle\Entity\EstateCalendarDayDataPrice;
use UTT\EstateBundle\Entity\EstateCalendarDayDataPriceRepository;

class EstateCalendarService {
    protected $_em;
    protected $reservationService;
    protected $pricingService;

    public function __construct(EntityManager $em, ReservationService $reservationService, PricingService $pricingService){
        $this->_em = $em;
        $this->reservationService = $reservationService;
        $this->pricingService = $pricingService;
    }

    public function generateEstateCalendar(Estate $estate){
        /** @var OfferRepository $offerRepository */
        $offerRepository = $this->_em->getRepository('UTTReservationBundle:Offer');
        /** @var EstateCalendarDayRepository $estateCalendarDayRepository */
        $estateCalendarDayRepository = $this->_em->getRepository('UTTEstateBundle:EstateCalendarDay');
        /** @var EstateCalendarDayDataPriceRepository $estateCalendarDayDataPriceRepository */
        $estateCalendarDayDataPriceRepository = $this->_em->getRepository('UTTEstateBundle:EstateCalendarDayDataPrice');

        $nightsFlexible = array(2,3,4,5,6,7,8,9,10,11,12,13,14);
        $nightsStandard = array(3,4,7,10,11,14);

        $adminCharge = $this->pricingService->getAdminCharge();
        $adminCharge = ($adminCharge == -1 ? 0 : $adminCharge);

        $yearDate = new \DateTime('now');
        $yearDate->setDate($yearDate->format('Y'), $yearDate->format('m'), ($yearDate->format('d') == 1 ? 1 : $yearDate->format('d') -1));
        $yearsInFuture = 2;

        $start = new \DateTime( sprintf('%04d-%d-%d', $yearDate->format('Y'), $yearDate->format('m'), $yearDate->format('d')) );
        $end = new \DateTime( sprintf('%04d-12-31', (int)$yearDate->format('Y') + ($yearsInFuture-1)) );

        $pricingList = $this->pricingService->getPricingListForEstateForYear($estate, $yearDate, $yearsInFuture);
        if($pricingList){
            $reservations = $this->reservationService->getReservations($estate, $start, $end);

            $offersFromDate = $start;
            if(isset($pricingList[0]) && $pricingList[0] instanceof Pricing){ $offersFromDate = $pricingList[0]->getFromDate(); }
            $offers = $offerRepository->findAvailableForEstateBetweenDates($estate, $offersFromDate, $end);

            $end->modify('+1 day');
            $period = new \DatePeriod($start, new \DateInterval('P1D'), $end);

            $days = $this->getEstateCalendarDays($estate, $period, $pricingList, $reservations, $offers);
            foreach($days as $day){
                $estateCalendarDay = $estateCalendarDayRepository->findOneBy(array(
                    'estate' => $estate->getId(),
                    'date' => new \DateTime($day->date)
                ));
                if(!($estateCalendarDay instanceof EstateCalendarDay)){
                    $estateCalendarDay = new EstateCalendarDay();
                }

                $estateCalendarDay->setEstate($estate);
                $estateCalendarDay->setDate(new \DateTime($day->date));
                $estateCalendarDay->setIsPastDate($day->isPastDate);
                $estateCalendarDay->setYear($day->year);
                $estateCalendarDay->setMonth($day->month);
                $estateCalendarDay->setDay($day->day);
                $estateCalendarDay->setDayOfWeek($day->dayOfWeek);
                $estateCalendarDay->setAvailable($day->available);
                $estateCalendarDay->setAvailableOffer($day->availableOffer);
                $estateCalendarDay->setBooked($day->booked);
                $estateCalendarDay->setBookedStart($day->bookedStart);
                $estateCalendarDay->setBookedEnd($day->bookedEnd);
                $estateCalendarDay->setReserved($day->reserved);
                $estateCalendarDay->setReservedStart($day->reservedStart);
                $estateCalendarDay->setReservedEnd($day->reservedEnd);
                $estateCalendarDay->setIsMin7NightsStay($day->isMin7NightsStay);
                $estateCalendarDay->setIsBlockedForArriveOrDepart($day->isBlockedForArriveOrDepart);

                $this->_em->persist($estateCalendarDay);

                $nightsArray = array();
                if($estate->getType() == Estate::TYPE_FLEXIBLE_BOOKING){
                    $nightsArray = $nightsFlexible;
                }elseif($estate->getType() == Estate::TYPE_STANDARD_M_F){
                    $nightsArray = $nightsStandard;
                }

                if($nightsArray){
                    foreach($nightsArray as $nights){
                        $dataPrice = $this->generateEstateCalendarDayDataPrice($nights, $estate->getType(), $day, $days, $this->getEstatePricingData($pricingList), $this->getEstateOffersData($offers), $adminCharge);
                        $estateCalendarDayDataPrice = $estateCalendarDayDataPriceRepository->findOneBy(array(
                            'estateCalendarDay' => $estateCalendarDay->getId(),
                            'nights' => $nights
                        ));
                        if(!($estateCalendarDayDataPrice instanceof EstateCalendarDayDataPrice)){
                            $estateCalendarDayDataPrice = new EstateCalendarDayDataPrice();
                        }

                        $estateCalendarDayDataPrice->setEstateCalendarDay($estateCalendarDay);
                        $estateCalendarDayDataPrice->setNights($nights);
                        $estateCalendarDayDataPrice->setIsValid($dataPrice[0]);
                        $estateCalendarDayDataPrice->setSelectPrev($dataPrice[1]);
                        $estateCalendarDayDataPrice->setSelectNext($dataPrice[2]);
                        $estateCalendarDayDataPrice->setPrice($dataPrice[3]);

                        $this->_em->persist($estateCalendarDayDataPrice);
                    }
                }
            }
            $this->_em->flush();
        }
    }

    private function calculateFlexible($adminCharge, $pricingData, $days){
        if(count($days) > 2){
            $price = 0;
            foreach($days as $day){
                if(array_search($day, $days) < count($days) -1){
                    foreach($pricingData as $pricing){
                        if($day->date >= $pricing['fromDate'] && $day->date <= $pricing['toDate']){
                            $index = array_search($day, $days);
                            if($index == 0){
                                $price = (float)$pricing['initialPrice'] + (float)$adminCharge;
                            }

                            $price = (float)$price + (float)$pricing['price'];
                        }
                    }
                }
            }
            if($price > 0){ return $price/*.toFixed(2)*/; }
        }
        return -1;
    }

    private function calculateStandard($adminCharge, $pricingData, $day, $daysCount){
        $found = false;

        foreach($pricingData as $pricing){
            $dayDate = new \DateTime($day->date);
            $pricingFromDate = new \DateTime($pricing['fromDate']);
            $pricingToDate = new \DateTime($pricing['toDate']);
            $diff = $pricingToDate->diff($pricingFromDate)->days;

            if($dayDate >= $pricingFromDate && $dayDate <= $pricingToDate && $diff == $daysCount){
                $found = $pricing;
                $found['price'] = (float)$found['price'] + (float)$adminCharge;
            }
        }

        return $found;
    }

    private function getOfferPriceForDates($offersData, $adminCharge, $fromDate, $toDate){
        $offerPrice = false;

        foreach($offersData as $offer){
            if($offer['validFrom'] == $fromDate && $offer['validTo'] == $toDate){
                $offerPrice = (float)$offer['value'] + (float)$adminCharge;
            }
        }

        return $offerPrice;
    }

    public function generateEstateCalendarDayDataPrice($nights, $estateType, $day, $days, $pricingData, $offersData, $adminCharge){
        $isValid = false;
        $selectedDataPrice = '';
        $selectPrev = 0;
        $selectNext = 0;

        $index = array_search($day, $days);
        if($days[$index]->available == true){
            if ($estateType == Estate::TYPE_FLEXIBLE_BOOKING) {
                $selectedIndexes = array($index);

                for($i=1; $i<=$nights; $i++){
                    if(isset($days[$index+$i]) && $days[$index+$i]->available == true){
                        $selectedIndexes[] = $index+$i;
                        $selectNext++;
                    }else{
                        break;
                    }
                }

                if(count($selectedIndexes) <= $nights){
                    for($i=1; $i <= $nights; $i++){
                        if(isset($days[$index-$i]) && $days[$index-$i]->available == true && count($selectedIndexes) <= $nights){
                            array_unshift($selectedIndexes, $index-$i);
                            $selectPrev++;
                        }else{
                            break;
                        }
                    }
                }

                if($days[$selectedIndexes[0]]->isBlockedForArriveOrDepart == true){
                    $selectedDataPrice = 'Holidays are not allowed to begin at selected day.<br />Please call UTT office on 01239 727 029 in order to book your dates.';
                    return array($isValid, $selectPrev, $selectNext, $selectedDataPrice);
                }
                if($days[$selectedIndexes[count($selectedIndexes)-1]]->isBlockedForArriveOrDepart == true){
                    $selectedDataPrice = 'Holidays are not allowed to begin at selected day.<br />Please call UTT office on 01239 727 029 in order to book your dates.';
                    return array($isValid, $selectPrev, $selectNext, $selectedDataPrice);
                }

                /*
                if(
                    isset($days[$selectedIndexes[count($selectedIndexes)-1]]) && $days[$selectedIndexes[count($selectedIndexes)-1]]->booked != true &&
                    isset($days[$selectedIndexes[count($selectedIndexes)-1] + 1]) && $days[$selectedIndexes[count($selectedIndexes)-1] + 1]->booked == true
                ){
                    $selectedDataPrice = 'The break you\'ve selected leaves a single, unsellable night. Please re-select for either a different start date, or a shorter or longer break so as not to trap any single nights. Thank you :)';
                    return array($isValid, $selectPrev, $selectNext, $selectedDataPrice);
                }
                if($days[$selectedIndexes[0]]->booked != true && isset($days[$selectedIndexes[0] - 1]) && $days[$selectedIndexes[0] - 1]->booked == true && $days[$selectedIndexes[0] - 1]->isPastDate != true){
                    $selectedDataPrice = 'The break you\'ve selected leaves a single, unsellable night. Please re-select for either a different start date, or a shorter or longer break so as not to trap any single nights. Thank you :)';
                    return array($isValid, $selectPrev, $selectNext, $selectedDataPrice);
                }
                */

                $daysSelected = [];
                foreach($selectedIndexes as $index){
                    $daysSelected[] = $days[$index];
                }

                $price = $this->calculateFlexible($adminCharge, $pricingData, $daysSelected);
                if($price != -1){
                    $isValid = true;
                    $fromDate = $days[$selectedIndexes[0]]->date;
                    $toDate = $days[$selectedIndexes[count($selectedIndexes) - 1]]->date;
                    $selectedDataPrice = '£'.$price;

                    $offerPrice = $this->getOfferPriceForDates($offersData, $adminCharge, $fromDate, $toDate);
                    if($offerPrice){
                        $selectedDataPrice = 'Now only £' . $offerPrice . ' was ' . $selectedDataPrice . ' - save £' . ((float)$price - (float)$offerPrice);//.toFixed(2);
                    }
                }
            }else if($estateType == Estate::TYPE_STANDARD_M_F){
                $calculated = $this->calculateStandard($adminCharge, $pricingData, $day, $nights);
                if($calculated){
                    $selectedIndexes = array($index);

                    for($i=1; $i<=$nights; $i++){
                        if(isset($days[$index+$i]) && new \DateTime($days[$index+$i]->date) >= new \DateTime($calculated['fromDate']) && new \DateTime($days[$index+$i]->date) <= new \DateTime($calculated['toDate']) && $days[$index+$i]->available == true){
                            $selectedIndexes[] = $index+$i;
                            $selectNext++;
                        }else{ break; }
                    }

                    if(count($selectedIndexes) <= $nights){
                        for($i=1; $i<=$nights; $i++){
                            if(new \DateTime($days[$index-$i]->date) >= new \DateTime($calculated['fromDate']) && new \DateTime($days[$index-$i]->date) <= new \DateTime($calculated['toDate']) && isset($days[$index-$i]) && $days[$index-$i]->available == true && count($selectedIndexes) <= $nights){
                                array_unshift($selectedIndexes, $index-$i);
                                $selectPrev++;
                            }else{ break; }
                        }
                    }

                    if($days[$selectedIndexes[0]]->isBlockedForArriveOrDepart == true){
                        $selectedDataPrice = 'Holidays are not allowed to begin at selected day.<br />Please call UTT office on 01239 727 029 in order to book your dates.';
                        return array($isValid, $selectPrev, $selectNext, $selectedDataPrice);
                    }
                    if($days[$selectedIndexes[count($selectedIndexes)-1]]->isBlockedForArriveOrDepart == true){
                        $selectedDataPrice = 'Holidays are not allowed to begin at selected day.<br />Please call UTT office on 01239 727 029 in order to book your dates.';
                        return array($isValid, $selectPrev, $selectNext, $selectedDataPrice);
                    }

                    if(count($selectedIndexes) == $nights + 1){
                        $isValid = true;
                        $fromDate = $calculated['fromDate'];
                        $toDate = $calculated['toDate'];
                        $selectedDataPrice = '£'.$calculated['price'];

                        $offerPrice = $this->getOfferPriceForDates($offersData, $adminCharge, $fromDate, $toDate);
                        if($offerPrice){
                            $selectedDataPrice = 'Now only £' . $offerPrice . ' was ' . $selectedDataPrice . ' - save £' . ((float)$calculated['price'] - (float)$offerPrice);//.toFixed(2);
                        }
                    }else{
                        // TODO
                    }
                }else{
                    $selectedIndexes = array($index);
                    if($nights == 3 && $days[$selectedIndexes[0]]->dayOfWeek >=2 && $days[$selectedIndexes[0]]->dayOfWeek <= 4) {
                        $selectedDataPrice = 'Please choose 4 nights if you want to book selected dates.';
                        return array($isValid, $selectPrev, $selectNext, $selectedDataPrice);
                    }elseif($nights == 4 && $days[$selectedIndexes[0]]->dayOfWeek >=6 && $days[$selectedIndexes[0]]->dayOfWeek <= 7) {
                        $selectedDataPrice = 'Please choose 3 nights if you want to book selected dates.';
                        return array($isValid, $selectPrev, $selectNext, $selectedDataPrice);
                    }
                }
            }
        }

        return array($isValid, $selectPrev, $selectNext, $selectedDataPrice);
    }


    public function getEstateOffersData($offers){
        $offersData = array();
        if($offers){
            /** @var Offer $offer */
            foreach($offers as $offer){
                $offersData[] = array(
                    'name' => $offer->getName(),
                    'validFrom' => $offer->getValidFrom()->format('Y-m-d'),
                    'validTo' => $offer->getValidTo()->format('Y-m-d'),
                    'value' => $offer->getValue()
                );
            }
        }

        return $offersData;
    }

    public function getEstatePricingData($pricingList){
        $pricingData = array();
        /** @var Pricing $pricing */
        foreach($pricingList as $pricing){
            if($pricing instanceof Pricing){
                $pricingData[] = array(
                    'fromDate' => $pricing->getFromDate()->format('Y-m-d'),
                    'toDate' => $pricing->getToDate()->format('Y-m-d'),
                    'price' => $pricing->getPrice(),
                    'initialPrice' => $pricing->getInitialPrice()
                );
            }elseif(is_array($pricing)){
                $pricingData[] = array(
                    'fromDate' => $pricing['from_date'],
                    'toDate' => $pricing['to_date'],
                    'price' => $pricing['price'],
                    'initialPrice' => $pricing['initial_price']
                );
            }
        }

        return $pricingData;
    }

    public function getEstateCalendarDays(Estate $estate, $period, $pricingList, $reservations, $offers){
        $calendarDays = array();

        /** @var \Datetime $date */
        foreach($period as $date){
            $isDateAvailable = false;
            $isDateAvailableOffer = false;
            $isDateBooked = false;
            $isDateBookedStart = false;
            $isDateBookedEnd = false;
            $isDateReserved = false;
            $isDateReservedStart = false;
            $isDateReservedEnd = false;
            $isMin7NightsStay = false;

            if($reservations){
                /** @var Reservation $reservation */
                foreach($reservations as $reservation){
                    if($date >= $reservation->getFromDate() && $date <= $reservation->getToDate()){
                        $isDateBooked = true;

                        if($date == $reservation->getFromDate()){ $isDateBookedStart = true; }
                        if($date == $reservation->getToDate()){ $isDateBookedEnd = true; }

                        if($reservation->getStatus() == Reservation::STATUS_RESERVED){
                            $isDateReserved = true;

                            if($date == $reservation->getFromDate()){ $isDateReservedStart = true; }
                            if($date == $reservation->getToDate()){ $isDateReservedEnd = true; }
                        }
                        if($date == $reservation->getFromDate() || $date == $reservation->getToDate()){
                            $isDateAvailable = true;
                        }
                    }
                }
            }

            if($offers){
                /** @var Offer $offer */
                foreach($offers as $offer){
                    if($date >= $offer->getValidFrom() && $date <= $offer->getValidTo()){
                        $isDateAvailableOffer = true;
                    }
                }
            }

            if(!$isDateBooked){
                foreach($pricingList as $pricing){
                    if($pricing instanceof Pricing){
                        if($date >= $pricing->getFromDate() && $date <= $pricing->getToDate()){
                            $isDateAvailable = true;
                            break;
                        }
                    }elseif(is_array($pricing)){
                        if($date >= new \DateTime($pricing['from_date']) && $date <= new \DateTime($pricing['to_date'])){
                            $isDateAvailable = true;
                            break;
                        }
                    }
                }
            }

            if($isDateBookedStart && $isDateBookedEnd){
                $isDateAvailable = false;
            }

            /** @var PricingSeason $pricingSeason */
            foreach($estate->getMin7NightsPricingSeasons() as $pricingSeason){
                if($date >= $pricingSeason->getFromDate() && $date <= $pricingSeason->getToDate()){
                    $isMin7NightsStay = true;
                }
            }

            $nowDate = new \DateTime('now');
            $nowDate->modify('-1 day');
            $calendarDay = (object) array(
                'date' => $date->format('Y-m-d'),
                'isPastDate' => ($date < $nowDate ? true: false),
                'year' => $date->format('Y'),
                'month' => $date->format('n'),
                'day' => $date->format('d'),
                'dayOfWeek' => $date->format('N'),
                'available' => ($date < $nowDate ? false: $isDateAvailable),
                'availableOffer' => $isDateAvailableOffer,
                'booked' => $isDateBooked,
                'bookedStart' => $isDateBookedStart,
                'bookedEnd' => $isDateBookedEnd,
                'reserved' => $isDateReserved,
                'reservedStart' => $isDateReservedStart,
                'reservedEnd' => $isDateReservedEnd,
                'isMin7NightsStay' => $isMin7NightsStay,
                'isBlockedForArriveOrDepart' => ($this->reservationService->isBlockedForArriveOrDepart($date) ? true : false)
            );

            $calendarDays[]= $calendarDay;
        }

        /*foreach($calendarDays as $calendarDataDay){
            if($calendarDataDay->available == true && $calendarDataDay->booked == true){
                $key = array_search($calendarDataDay, $calendarDays);
                if($key > 0){
                    if($calendarDays[$key + 1]->booked == true && $calendarDays[$key - 1]->booked == true){
                        $calendarDataDay->available = false;
                    }
                }
            }
        }*/

        return $calendarDays;
    }
}