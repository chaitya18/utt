<?php

namespace UTT\EstateBundle\Service;

use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\EntityManager;
use UTT\EstateBundle\Entity\Estate;
use UTT\EstateBundle\Entity\Photo;
use UTT\ReservationBundle\Service\ReservationService;
use UTT\ReservationBundle\Service\PricingService;
use UTT\EstateBundle\Entity\EstateRepository;
use UTT\ReservationBundle\Model\ReservationDataPrice;

class EstateSearchService {
    protected $_em;
    protected $reservationService;
    protected $pricingService;

    public function __construct(EntityManager $em, ReservationService $reservationService, PricingService $pricingService){
        $this->_em = $em;
        $this->reservationService = $reservationService;
        $this->pricingService = $pricingService;
    }

    public function parametersVerify($paramLocation, $paramDateFrom, $paramDateTo, $paramSleeps, $paramEstateFeatures, $paramPets, $paramChildren, $paramInfants, $paramPrice, $paramPriceFrom, $paramPriceTo, $paramPage, $paramEstateName, $paramCategory, $paramSpecialCategory){
        if(!($paramLocation == null || (is_numeric($paramLocation) && $paramLocation > 0))) return false;
        if(!($paramCategory == null || (is_numeric($paramCategory) && $paramCategory > 0))) return false;
        if($paramDateFrom != null && $paramDateTo != null){
            try{
                $nowDate = new \DateTime('now');
                $nowDate->setTime(0,0,0);
                $paramDateFrom = new \Datetime($paramDateFrom);
                $paramDateTo = new \Datetime($paramDateTo);
                if($paramDateFrom >= $nowDate && $paramDateTo >= $nowDate && $paramDateTo > $paramDateFrom){

                }else{
                    return false;
                }
            }catch (\Exception $e){
                return false;
            }
        }
        if(!($paramSleeps == null || (is_numeric($paramSleeps) && $paramSleeps > 0))) return false;
        if(!($paramEstateFeatures == null || (is_array($paramEstateFeatures) && count($paramEstateFeatures) > 0 ))) return false;
        if(!($paramPets == null || (is_bool($paramPets)))) return false;
        if(!($paramChildren == null || (is_bool($paramChildren)))) return false;
        if(!($paramInfants == null || (is_bool($paramInfants)))) return false;
        if(!($paramPrice == null || (is_numeric($paramPrice) && $paramPrice > 0))) return false;
        if(!($paramPriceFrom == null || (is_numeric($paramPriceFrom) && $paramPriceFrom > 0))) return false;
        if(!($paramPriceTo == null || (is_numeric($paramPriceTo) && $paramPriceTo > 0))) return false;
        if(!($paramPage == null || (is_numeric($paramPage) && $paramPage > 0))) return false;
        if(!($paramEstateName == null || !empty($paramPage))) return false;
        //if(!($paramSpecialCategory == null || (is_numeric($paramSpecialCategory) && $paramSpecialCategory >= 0))) return false;
        //if(!($paramSpecialCategory == null || $paramSpecialCategory == Estate::SPECIAL_CATEGORY_NONE || $paramSpecialCategory == Estate::SPECIAL_CATEGORY_BUDGET || $paramSpecialCategory == Estate::SPECIAL_CATEGORY_FINEST)) return false;

        return true;
    }

    public function search($paramLocation, $paramDateFrom, $paramDateTo, $paramSleeps, $paramEstateFeatures, $paramPets, $paramChildren, $paramInfants, $paramPriceFrom, $paramPriceTo, $paramPage, $paramEstateName, $paramCategory, $paramSpecialCategory){
        # TODO improve this!!
        /** @var EstateRepository $estateRepository */
        $estateRepository = $this->_em->getRepository('UTTEstateBundle:Estate');

        $newEstates = array();
        if($paramDateFrom != null && $paramDateTo != null){
            $estates = $estateRepository->getForEstateSearchArray(false, $paramLocation, $paramSleeps, $paramEstateFeatures, $paramPets, $paramChildren, $paramInfants, $paramPriceFrom, $paramPriceTo, $paramPage, $paramEstateName, $paramCategory, $paramSpecialCategory);

            foreach($estates as $estate){
                /** @var Estate $estateOb */
                $estateOb = $estateRepository->find($estate['id']);
                $estateFromDate = new \DateTime($paramDateFrom);
                $estateToDate = new \DateTime($paramDateTo);
                if($estateOb instanceof Estate){
                    if($this->reservationService->isAvailableForBooking($estateOb, $estateFromDate, $estateToDate)){
                        /** @var ReservationDataPrice $price */
                        $price = $this->reservationService->getPriceForParams($estateOb, new \Datetime($paramDateFrom), new \Datetime($paramDateTo), $paramSleeps, 0, 0, false, true, false);
                        if($price instanceof ReservationDataPrice){
                            if($paramPriceFrom && $paramPriceTo && !($price->getTotal() >= $paramPriceFrom && $price->getTotal() <= $paramPriceTo)) continue;
                            $estate['basePrice'] = round($price->getTotal(), 0);

                            if($estate['type'] == Estate::TYPE_STANDARD_M_F){
                                if($estateFromDate->format('N') == 5 || $estateFromDate->format('N') == 6 || $estateFromDate->format('N') == 7){
                                    if($estateToDate->format('N') == 6 || $estateToDate->format('N') == 7 || $estateToDate->format('N') == 1){
                                        $estate['basePriceNights'] = 3;
                                    }
                                }

                                if($estateFromDate->format('N') == 1 || $estateFromDate->format('N') == 2 || $estateFromDate->format('N') == 3 || $estateFromDate->format('N') == 4){
                                    if($estateToDate->format('N') == 2 || $estateToDate->format('N') == 3 || $estateToDate->format('N') == 4 || $estateToDate->format('N') == 5){
                                        $estate['basePriceNights'] = 4;
                                    }
                                }
                            }

                            if($price->isIsSpecialOffer()) $estate['basePriceSpecialPrice'] = true;
                        }

                        $newEstates[] = $estate;
                    }
                }
            }
        }else{
            $estates = $estateRepository->getForEstateSearchArray(true, $paramLocation, $paramSleeps, $paramEstateFeatures, $paramPets, $paramChildren, $paramInfants, $paramPriceFrom, $paramPriceTo, $paramPage, $paramEstateName, $paramCategory, $paramSpecialCategory);

            /** @var Array $estate */
            foreach($estates as &$estate){
                if(isset($estate['lowestPrice'])){
                    $lowestPriceAdditional = $this->pricingService->calculateAdditionalSleepsPriceByEstateArray($estate, $estate['lowestPrice'], $paramSleeps);

                    if($lowestPriceAdditional != -1){
                        $estate['lowestPrice'] = (float)$estate['lowestPrice'] + (float)$lowestPriceAdditional;
                    }
                }

                if(isset($estate['highestPrice'])){
                    $highestPriceAdditional = $this->pricingService->calculateAdditionalSleepsPriceByEstateArray($estate, $estate['highestPrice'], $paramSleeps);

                    if($highestPriceAdditional != -1){
                        $estate['highestPrice'] = (float)$estate['highestPrice'] + (float)$highestPriceAdditional;
                    }
                }
            }

            $newEstates = $estates;
        }

        shuffle($newEstates);
        return $newEstates;
    }
}