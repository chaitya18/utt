<?php

namespace UTT\EstateBundle\Service;

use Doctrine\ORM\EntityManager;
use Symfony\Component\HttpFoundation\Request;
use UTT\EstateBundle\Entity\Photo;

class PhotoUploadManager {
    protected $_em;
    protected $request;

    public function __construct(EntityManager $em, Request $request){
        $this->_em = $em;
        $this->request = $request;
    }

    public function getFileDirectory(){
        $photo = new Photo();

        $fileDirectory = rtrim($this->request->getBasePath().$photo->getUploadDir(),'/').'/';
        if(!file_exists($fileDirectory)) mkdir($fileDirectory, 0777);

        return $fileDirectory;
    }

    public function getFileSubdirectory($fileDirectory){
        $fileSubDirectory = rand(0,1000).'/';
        if(!file_exists($fileDirectory.$fileSubDirectory)) mkdir($fileDirectory.$fileSubDirectory, 0777);

        return $fileSubDirectory;
    }

    public function getFileName($fileDirectory, $fileSubDirectory, $extension){
        $fileName = uniqid().'_'.uniqid().'.'.$extension;
        $fileName = $fileSubDirectory.$fileName;

        while(file_exists($fileDirectory.$fileName)){
            $fileName = uniqid().'_'.uniqid().'.'.$extension;
            $fileName = $fileSubDirectory.$fileName;
        }

        return $fileName;
    }

    public function duplicateRotatePhoto(Photo $photo, $degrees){
        $fileDirectory = $this->getFileDirectory();
        $fileName = $photo->getFileName();
        $extension = strtolower(substr($fileName, strrpos($fileName, '.') + 1));

        $fileSubDirectory = $this->getFileSubdirectory($fileDirectory);
        $newFileName = $this->getFileName($fileDirectory, $fileSubDirectory, strtolower($extension));

        $source = false;
        $rotate = false;
        if($extension == 'png' || $extension == 'PNG'){
            header('Content-type: image/png');
            $source = imagecreatefrompng($fileDirectory.$fileName);
            $bgColor = imagecolorallocatealpha($source, 255, 255, 255, 127);
            // Rotate
            $rotate = imagerotate($source, $degrees, $bgColor);
            imagesavealpha($rotate, true);
            imagepng($rotate, $fileDirectory.$newFileName);
        }
        if($extension == 'jpg' || $extension == 'jpeg'){
            header('Content-type: image/jpeg');
            $source = imagecreatefromjpeg($fileDirectory.$fileName);
            // Rotate
            $rotate = imagerotate($source, $degrees, 0);
            imagejpeg($rotate, $fileDirectory.$newFileName);
        }

        imagedestroy($source);
        imagedestroy($rotate);

        $newPhoto = new Photo();
//        $newPhoto->setEstate($photo->getEstate());
        $newPhoto->setIsHighlighted($photo->getIsHighlighted());
        $newPhoto->setTitle($photo->getTitle());
        $newPhoto->setFileName($newFileName);

        try{
            $this->_em->persist($newPhoto);
            $this->_em->flush();

            return $newPhoto;
        }catch (\Exception $e){
            return false;
        }
    }
}