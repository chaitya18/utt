<?php

namespace UTT\EstateBundle\Service;

use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\EntityManager;
use Liip\ImagineBundle\Imagine\Cache\CacheManager;
use UTT\EstateBundle\Entity\Estate;
use UTT\EstateBundle\Entity\Photo;
use UTT\EstateBundle\Model\EstateRoomLayout;

class EstateService {
    protected $_em;
    protected $liipImagineCacheManager;

    public function __construct(EntityManager $em, CacheManager $liipImagineCacheManager){
        $this->_em = $em;
        $this->liipImagineCacheManager = $liipImagineCacheManager;
    }

    public function sortPhotosArrayForEvent($estatePhotos, $photoMain){
        if($photoMain){
            $photos = array($photoMain);
            foreach($estatePhotos as $estatePhoto){
                $photos[] = $estatePhoto;
            }

            return $photos;
        }else{
            return $estatePhotos;
        }
    }

    public function generateRandomPhotoSrc($estate, $thumb){
        $estatePhoto = false;
        if($estate instanceof Estate){
            $estatePhoto = $estate->getRandomPhoto();
        }elseif(is_array($estate)){
            if(isset($estate['photoMain']) && $estate['photoMain']){
                $estatePhoto = $estate['photoMain'];
            }else{
                if(isset($estate['photos']) && is_array($estate['photos']) && count($estate['photos']) > 0){
                    $estatePhoto = $estate['photos'][0];
                }
            }
        }

        return $this->generatePhotoSrc($estatePhoto, $thumb);
    }

    public function generatePhotoSrc($photo, $thumb){
        if($photo instanceof Photo || is_array($photo)){
            $photoEntity = new Photo();
            $estatePhotoUploadDir = $photoEntity->getUploadDir();
            unset($photoEntity);

            if($photo instanceof Photo){
                return $this->liipImagineCacheManager->getBrowserPath($estatePhotoUploadDir.'/'.$photo->getFileName(), $thumb);
            }elseif(is_array($photo)){
                return $this->liipImagineCacheManager->getBrowserPath($estatePhotoUploadDir.'/'.$photo['fileName'], $thumb);
            }
        }

        return $this->liipImagineCacheManager->getBrowserPath('/bundles/uttindex/images/placeholder.png', $thumb);
    }

    # TODO przerobić to na moją własną implementacje bo to jest kod skopiowany z obecnej wersji
    public function generateRoomLayout($bedformat, $guests){
        // D - Double // S - Single // T - Twin // B - Bunk

        $i = 0;
        $count = 0;
        $description = "";
        $beds = 0;
        $rooms = 0;
        $sleeps = 0;

        while ($count < $guests){
            $bed = substr($bedformat, $i, 1);
            switch (strtoupper($bed)){
                case "D" :
                    $room = ($count == 0) ? "Master Double" : "Double";
                    $sleeps = 2;
                    $beds++;
                    $rooms++;
                    break;
                case "S" : $room = "Single";
                    $sleeps = 1; $beds++; $rooms++; break;
                case "T" : $room = "Twin";
                    $sleeps = 2; $beds+=2; $rooms++; break;
                case "B" : $room = "Bunk";
                    $sleeps = 2; $beds++; $rooms++; break;
                case "O" : $room = "Sofabed (double)";
                    $sleeps = 2; $beds++; break;
                case "E" : $room = "Extra bed (single)";
                    $sleeps = 1; $beds++; break;
                case "R" :  $room = "Triple Room";
                    $sleeps = 3; $beds+=3; $rooms++; break;
                case "V" : $room = "Double + Single";
                    $sleeps = 3; $rooms++; $beds+=2; break;
                case "F" :  $room = "Double + 2 Singles";
                    $sleeps = 3; $beds+=3; $rooms++; break;
                default: $room = ""; $sleeps = 0; break;
            }
            if (strtolower($bed) == $bed) $room .= " (ground floor)";
            if ($description != "") $description .= ", ";
            $description .= $room;
            $count += $sleeps;
            $i++;
            if ($i >= strlen($bedformat)) break;
        }

        if ($i < strlen($bedformat)) {
            $pos = strrpos($description,',');
            if ($pos > 0) $description = substr($description,0,$pos) . " and the " .substr($description,$pos+2,100);
            $description .= " only";
        }
        //elseif ($sleeps >= $guests)
        //    $description = "All rooms";

        $sleeps = $count;

        $estateRoomLayout = new EstateRoomLayout();
        $estateRoomLayout->setBeds($beds);
        $estateRoomLayout->setRooms($rooms);
        $estateRoomLayout->setSleeps($sleeps);
        $estateRoomLayout->setDescription($description);

        return $estateRoomLayout;
    }

}