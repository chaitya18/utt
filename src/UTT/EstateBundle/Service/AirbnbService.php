<?php

namespace UTT\EstateBundle\Service;

use Doctrine\ORM\EntityManager;
use UTT\ReservationBundle\Service\ReservationService;
use UTT\EstateBundle\Entity\Estate;

class AirbnbService {
    protected $_em;
    protected $reservationService;
    protected $kernelRootDir;

    public function __construct(EntityManager $em, ReservationService $reservationService, $kernelRootDir){
        $this->_em = $em;
        $this->reservationService = $reservationService;
        $this->kernelRootDir = $kernelRootDir;
    }

    function downloadListing($url){
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        $data = curl_exec($ch);
        curl_close($ch);
        return $data;
    }

    public function generateListingFilName(Estate $estate){
        return 'listing-'.$estate->getId().'.ics';
    }

    private function getUploadDir(){
        return 'airbnb-listings';
    }

    public function getFileDirectory($forCommand = false){
        if($forCommand){
            $fileDirectory = rtrim($this->kernelRootDir.'/../web/'.$this->getUploadDir(),'/').'/';
        }else{
            $fileDirectory = rtrim($this->getUploadDir(),'/').'/';
        }

        if(!file_exists($fileDirectory)) mkdir($fileDirectory, 0777);

        return $fileDirectory;
    }

}