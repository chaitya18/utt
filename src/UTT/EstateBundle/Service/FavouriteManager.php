<?php

namespace UTT\EstateBundle\Service;

use Doctrine\ORM\EntityManager;
use UTT\EstateBundle\Entity\Estate;
use UTT\UserBundle\Entity\User;
use UTT\EstateBundle\Entity\Favourite;

class FavouriteManager {
    protected $_em;

    public function __construct(EntityManager $em){
        $this->_em = $em;
    }

    /*
    public function clear(User $user){
        $favourites = $this->_em->getRepository('UTTEstateBundle:Favourite')->findBy(array(
            'user' => $user->getId()
        ));
        if(is_array($favourites) && count($favourites) > 0){
            foreach($favourites as $favourite){
                $this->_em->remove($favourite);
            }
            $this->_em->flush();
        }

        return true;
    }
    */

    public function remove(Estate $estate, User $user){
        /** @var Favourite $favourite */
        $favourite = $this->_em->getRepository('UTTEstateBundle:Favourite')->findOneBy(array(
            'user' => $user->getId(),
            'estate' => $estate->getId()
        ));
        if(!($favourite instanceof Favourite)){
            throw new \Exception('You can not remove estate. Estate does not exists on your favourites list.');
        }

        $this->_em->remove($favourite);
        $this->_em->flush();

        return true;
    }

    /**
     * @param Estate $estate
     * @param User $user
     * @return bool
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function isUserEstate(Estate $estate, User $user){
        /** @var Favourite $favourite */
        $favourite = $this->_em->getRepository('UTTEstateBundle:Favourite')->findOneBy(array(
            'user' => $user->getId(),
            'estate' => $estate->getId()
        ));
        return $favourite instanceof Favourite;
    }

    public function add(Estate $estate, User $user){
        /** @var Favourite $favourite */
        $favourite = $this->_em->getRepository('UTTEstateBundle:Favourite')->findOneBy(array(
            'user' => $user->getId(),
            'estate' => $estate->getId()
        ));
        if($favourite instanceof Favourite){
            throw new \Exception('Estate already exists on your favourites list.');
        }

        $favourite = new Favourite();
        $favourite->setUser($user);
        $favourite->setEstate($estate);

        $this->_em->persist($favourite);
        $this->_em->flush();

        return true;
    }
}