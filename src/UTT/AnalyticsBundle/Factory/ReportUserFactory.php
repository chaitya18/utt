<?php

namespace UTT\AnalyticsBundle\Factory;

use UTT\AnalyticsBundle\Entity\ReportUser;
use UTT\AnalyticsBundle\Entity\ReportUserSession;
use UTT\UserBundle\Entity\User;

class ReportUserFactory {

    public function create($trackingCookieHash, User $user){
        $reportUser = new ReportUser();
        $reportUser->setTrackingCookieHash($trackingCookieHash);
        $reportUser->setUser($user);

        return $reportUser;
    }

    public function createReportUserSession(ReportUser $reportUser, $sessionId){
        $reportUserSession = new ReportUserSession();
        $reportUserSession->setReportUser($reportUser);
        $reportUserSession->setSessionId($sessionId);

        return $reportUserSession;
    }
}