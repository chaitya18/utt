<?php

namespace UTT\AnalyticsBundle\Factory;

use UTT\AnalyticsBundle\Entity\ReportReferer;
use UTT\ReservationBundle\Entity\Reservation;

class ReportRefererFactory {

    public function create(Reservation $reservation, $refererType, $referer){
        $reportReferer = new ReportReferer();
        $reportReferer->setReservation($reservation);
        $reportReferer->setRefererType($refererType);
        $reportReferer->setReferer($referer);

        return $reportReferer;
    }
}