<?php

namespace UTT\AnalyticsBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ReportReferer
 *
 * @ORM\Table(name="report_referer")
 * @ORM\Entity(repositoryClass="UTT\AnalyticsBundle\Entity\ReportRefererRepository")
 */
class ReportReferer
{

    const REFERER_TYPE_DIRECT = 1;
    const REFERER_TYPE_WEBSITE = 2;
    const REFERER_TYPE_NEWSLETTER = 3;
    const REFERER_TYPE_DISCOUNT_CODE = 4;

    public function getAllowedRefererTypes(){
        $array = array(
            self::REFERER_TYPE_NEWSLETTER => 'Newsletter',
            self::REFERER_TYPE_WEBSITE => 'Website',
            self::REFERER_TYPE_DIRECT => 'Direct',
            self::REFERER_TYPE_DISCOUNT_CODE => 'Discount code',
        );
        return $array;
    }

    public static function getAllowedRefererTypesStatic(){
        $array = array(
            self::REFERER_TYPE_NEWSLETTER => 'Newsletter',
            self::REFERER_TYPE_WEBSITE => 'Website',
            self::REFERER_TYPE_DIRECT => 'Direct',
            self::REFERER_TYPE_DISCOUNT_CODE => 'Discount code',
        );
        return $array;
    }

    public function getRefererTypeName($refererTypeId = null){
        $array = $this->getAllowedRefererTypes();
        if(is_null($refererTypeId)){
            if($this->getRefererType()) {
                return array_key_exists($this->getRefererType(), $array) ? $array[$this->getRefererType()] : false;
            }else{
                return false;
            }
        }
        return array_key_exists($refererTypeId, $array) ? $array[$refererTypeId] : false;
    }

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="UTT\ReservationBundle\Entity\Reservation")
     * @ORM\JoinColumn(name="reservation_id", referencedColumnName="id", nullable=true, onDelete="CASCADE")
     */
    protected $reservation;

    /**
     * @ORM\Column(name="referer_type", type="integer", nullable=true)
     */
    protected $refererType;

    /**
     * @ORM\Column(name="referer", type="string", length=255, nullable=true)
     */
    protected $referer;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set referer
     *
     * @param string $referer
     * @return ReportReferer
     */
    public function setReferer($referer)
    {
        $this->referer = $referer;

        return $this;
    }

    /**
     * Get referer
     *
     * @return string 
     */
    public function getReferer()
    {
        return $this->referer;
    }

    /**
     * Set refererType
     *
     * @param integer $refererType
     * @return ReportReferer
     */
    public function setRefererType($refererType)
    {
        $this->refererType = $refererType;

        return $this;
    }

    /**
     * Get refererType
     *
     * @return integer 
     */
    public function getRefererType()
    {
        return $this->refererType;
    }

    /**
     * Set reservation
     *
     * @param \UTT\ReservationBundle\Entity\Reservation $reservation
     * @return ReportReferer
     */
    public function setReservation(\UTT\ReservationBundle\Entity\Reservation $reservation = null)
    {
        $this->reservation = $reservation;

        return $this;
    }

    /**
     * Get reservation
     *
     * @return \UTT\ReservationBundle\Entity\Reservation 
     */
    public function getReservation()
    {
        return $this->reservation;
    }
}
