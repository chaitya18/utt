<?php

namespace UTT\AnalyticsBundle\Entity;

use Doctrine\ORM\EntityRepository;

class ReportUserRepository extends EntityRepository
{
    public function getReports(){
        $query = $this->_em->createQuery("
          SELECT ru, rus, u
          FROM UTTAnalyticsBundle:ReportUser ru
          LEFT JOIN ru.reportUserSessions rus
          LEFT JOIN ru.user u
          ORDER BY ru.userLastVisitAt DESC, rus.lastVisitAt DESC
        ");

        $result = $query->getResult();
        if(count($result) > 0) return $result;

        return false;
    }
}
