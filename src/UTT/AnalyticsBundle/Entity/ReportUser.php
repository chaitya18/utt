<?php

namespace UTT\AnalyticsBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ReportUser
 *
 * @ORM\Table(name="report_user")
 * @ORM\Entity(repositoryClass="UTT\AnalyticsBundle\Entity\ReportUserRepository")
 */
class ReportUser
{
    public function __construct(){
        $this->setUpdatedAt(new \DateTime('now'));
        $this->reportUserSessions = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(name="tracking_cookie_hash", type="string", length=255, nullable=true)
     */
    protected $trackingCookieHash;

    /**
     * @ORM\ManyToOne(targetEntity="UTT\UserBundle\Entity\User")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id", nullable=true, onDelete="SET NULL")
     */
    protected $user;

    /**
     * @ORM\Column(name="user_last_visit_at", type="datetime", nullable=true)
     */
    protected $userLastVisitAt;

    /**
     * @ORM\Column(name="updated_at", type="datetime")
     */
    protected $updatedAt;

    /**
     * @ORM\OneToMany(targetEntity="ReportUserSession", mappedBy="reportUser", cascade={"persist"}, orphanRemoval=true)
     */
    protected $reportUserSessions;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set trackingCookieHash
     *
     * @param string $trackingCookieHash
     * @return ReportUser
     */
    public function setTrackingCookieHash($trackingCookieHash)
    {
        $this->trackingCookieHash = $trackingCookieHash;

        return $this;
    }

    /**
     * Get trackingCookieHash
     *
     * @return string 
     */
    public function getTrackingCookieHash()
    {
        return $this->trackingCookieHash;
    }

    /**
     * Set userLastVisitAt
     *
     * @param \DateTime $userLastVisitAt
     * @return ReportUser
     */
    public function setUserLastVisitAt($userLastVisitAt)
    {
        $this->userLastVisitAt = $userLastVisitAt;

        return $this;
    }

    /**
     * Get userLastVisitAt
     *
     * @return \DateTime 
     */
    public function getUserLastVisitAt()
    {
        return $this->userLastVisitAt;
    }
    
    /**
     * Set user
     *
     * @param \UTT\UserBundle\Entity\User $user
     * @return ReportUser
     */
    public function setUser(\UTT\UserBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \UTT\UserBundle\Entity\User 
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Add reportUserSessions
     *
     * @param \UTT\AnalyticsBundle\Entity\ReportUserSession $reportUserSessions
     * @return ReportUser
     */
    public function addReportUserSession(\UTT\AnalyticsBundle\Entity\ReportUserSession $reportUserSessions)
    {
        $this->reportUserSessions[] = $reportUserSessions;

        return $this;
    }

    /**
     * Remove reportUserSessions
     *
     * @param \UTT\AnalyticsBundle\Entity\ReportUserSession $reportUserSessions
     */
    public function removeReportUserSession(\UTT\AnalyticsBundle\Entity\ReportUserSession $reportUserSessions)
    {
        $this->reportUserSessions->removeElement($reportUserSessions);
    }

    /**
     * Get reportUserSessions
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getReportUserSessions()
    {
        return $this->reportUserSessions;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     * @return ReportUser
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime 
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }
}
