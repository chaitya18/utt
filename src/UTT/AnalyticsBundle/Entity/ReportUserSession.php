<?php

namespace UTT\AnalyticsBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ReportUserSession
 *
 * @ORM\Table(name="report_user_session")
 * @ORM\Entity(repositoryClass="UTT\AnalyticsBundle\Entity\ReportUserSessionRepository")
 */
class ReportUserSession
{
    public function __construct(){
        $this->setUpdatedAt(new \DateTime('now'));
    }

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="ReportUser", inversedBy="reportUserSessions")
     * @ORM\JoinColumn(name="report_user_id", referencedColumnName="id", nullable=true, onDelete="CASCADE")
     */
    protected $reportUser;

    /**
     * @ORM\Column(name="session_id", type="string", length=255, nullable=true)
     */
    protected $sessionId;

    /**
     * @ORM\Column(name="visits_number", type="integer", nullable=true)
     */
    protected $visitsNumber;

    /**
     * @ORM\Column(name="last_visit_at", type="datetime", nullable=true)
     */
    protected $lastVisitAt;

    /**
     * @ORM\Column(name="points", type="integer", nullable=true)
     */
    protected $points;

    /**
     * @ORM\Column(name="properties_points", type="text", nullable=true)
     */
    protected $propertiesPoints;

    /**
     * @ORM\Column(name="updated_at", type="datetime")
     */
    protected $updatedAt;

    /**
     * @ORM\ManyToOne(targetEntity="UTT\ReservationBundle\Entity\Reservation")
     * @ORM\JoinColumn(name="reservation_id", referencedColumnName="id", nullable=true, onDelete="SET NULL")
     */
    protected $reservation;

    /**
     * @ORM\Column(name="is_discount_email_sent", type="boolean")
     */
    protected $isDiscountEmailSent = false;

    /**
     * @ORM\ManyToOne(targetEntity="UTT\ReservationBundle\Entity\DiscountCode")
     * @ORM\JoinColumn(name="discount_code_id", referencedColumnName="id", nullable=true, onDelete="SET NULL")
     */
    protected $discountCode;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set sessionId
     *
     * @param string $sessionId
     * @return ReportUserSession
     */
    public function setSessionId($sessionId)
    {
        $this->sessionId = $sessionId;

        return $this;
    }

    /**
     * Get sessionId
     *
     * @return string 
     */
    public function getSessionId()
    {
        return $this->sessionId;
    }

    /**
     * Set visitsNumber
     *
     * @param integer $visitsNumber
     * @return ReportUserSession
     */
    public function setVisitsNumber($visitsNumber)
    {
        $this->visitsNumber = $visitsNumber;

        return $this;
    }

    /**
     * Get visitsNumber
     *
     * @return integer 
     */
    public function getVisitsNumber()
    {
        return $this->visitsNumber;
    }

    /**
     * Set lastVisitAt
     *
     * @param \DateTime $lastVisitAt
     * @return ReportUserSession
     */
    public function setLastVisitAt($lastVisitAt)
    {
        $this->lastVisitAt = $lastVisitAt;

        return $this;
    }

    /**
     * Get lastVisitAt
     *
     * @return \DateTime 
     */
    public function getLastVisitAt()
    {
        return $this->lastVisitAt;
    }

    /**
     * Set reportUser
     *
     * @param \UTT\AnalyticsBundle\Entity\ReportUser $reportUser
     * @return ReportUserSession
     */
    public function setReportUser(\UTT\AnalyticsBundle\Entity\ReportUser $reportUser = null)
    {
        $this->reportUser = $reportUser;

        return $this;
    }

    /**
     * Get reportUser
     *
     * @return \UTT\AnalyticsBundle\Entity\ReportUser 
     */
    public function getReportUser()
    {
        return $this->reportUser;
    }

    /**
     * Set points
     *
     * @param integer $points
     * @return ReportUserSession
     */
    public function setPoints($points)
    {
        $this->points = $points;

        return $this;
    }

    /**
     * Get points
     *
     * @return integer 
     */
    public function getPoints()
    {
        return $this->points;
    }

    /**
     * Set propertiesPoints
     *
     * @param string $propertiesPoints
     * @return ReportUserSession
     */
    public function setPropertiesPoints($propertiesPoints)
    {
        $this->propertiesPoints = $propertiesPoints;

        return $this;
    }

    /**
     * Get propertiesPoints
     *
     * @return string 
     */
    public function getPropertiesPoints()
    {
        return $this->propertiesPoints;
    }

    /**
     * Get propertiesPoints
     *
     * @return string
     */
    public function getPropertiesPointsDecoded()
    {
        return json_decode($this->propertiesPoints);
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     * @return ReportUserSession
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime 
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set reservation
     *
     * @param \UTT\ReservationBundle\Entity\Reservation $reservation
     * @return ReportUserSession
     */
    public function setReservation(\UTT\ReservationBundle\Entity\Reservation $reservation = null)
    {
        $this->reservation = $reservation;

        return $this;
    }

    /**
     * Get reservation
     *
     * @return \UTT\ReservationBundle\Entity\Reservation 
     */
    public function getReservation()
    {
        return $this->reservation;
    }

    /**
     * Set isDiscountEmailSent
     *
     * @param boolean $isDiscountEmailSent
     * @return ReportUserSession
     */
    public function setIsDiscountEmailSent($isDiscountEmailSent)
    {
        $this->isDiscountEmailSent = $isDiscountEmailSent;

        return $this;
    }

    /**
     * Get isDiscountEmailSent
     *
     * @return boolean 
     */
    public function getIsDiscountEmailSent()
    {
        return $this->isDiscountEmailSent;
    }

    /**
     * Set discountCode
     *
     * @param \UTT\ReservationBundle\Entity\DiscountCode $discountCode
     * @return ReportUserSession
     */
    public function setDiscountCode(\UTT\ReservationBundle\Entity\DiscountCode $discountCode = null)
    {
        $this->discountCode = $discountCode;

        return $this;
    }

    /**
     * Get discountCode
     *
     * @return \UTT\ReservationBundle\Entity\DiscountCode 
     */
    public function getDiscountCode()
    {
        return $this->discountCode;
    }
}
