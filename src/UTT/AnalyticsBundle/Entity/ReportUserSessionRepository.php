<?php

namespace UTT\AnalyticsBundle\Entity;

use Doctrine\ORM\EntityRepository;

class ReportUserSessionRepository extends EntityRepository
{
    public function getSessionsForQueue($pointsLimit){
        $nowDate = new \DateTime('now');
        $nowDate->modify('-10 day');

        $query = $this->_em->createQuery("
            SELECT rus, r
            FROM UTTAnalyticsBundle:ReportUserSession rus
            LEFT JOIN rus.reservation r
            WHERE
                rus.lastVisitAt >= :minLastVisitAt and
                rus.points >= :pointsLimit
            ORDER BY rus.id DESC");
        $query->setParameter('minLastVisitAt', $nowDate);
        $query->setParameter('pointsLimit', $pointsLimit);

        $result = $query->getResult();
        if(count($result) > 0) return $result;

        return false;
    }
}
