<?php

namespace UTT\AnalyticsBundle\Service;

use Doctrine\ORM\EntityManager;
use UTT\AnalyticsBundle\Factory\ReportRefererFactory;
use UTT\AnalyticsBundle\Entity\ReportReferer;
use UTT\AnalyticsBundle\Entity\ReportRefererRepository;
use UTT\ReservationBundle\Entity\Reservation;
use UTT\UserBundle\Entity\ActivityLog;

class ReportRefererService {
    protected $_em;
    protected $reportRefererFactory;

    public function __construct(EntityManager $em, ReportRefererFactory $reportRefererFactory){
        $this->_em = $em;
        $this->reportRefererFactory = $reportRefererFactory;
    }

    public function create(Reservation $reservation, $refererType, $referer){
        /** @var ReportReferer $reportReferer */
        $reportReferer = $this->reportRefererFactory->create($reservation, $refererType, $referer);
        return $this->save($reportReferer);
    }

    public function save($object){
        $this->_em->persist($object);
        $this->_em->flush();

        return $object;
    }

    public function saveAll(){
        $this->_em->flush();

        return true;
    }
}