<?php

namespace UTT\AnalyticsBundle\Service;

use Doctrine\ORM\EntityManager;
use UTT\AnalyticsBundle\Factory\ReportUserFactory;
use UTT\AnalyticsBundle\Entity\ReportUser;
use UTT\AnalyticsBundle\Entity\ReportUserRepository;
use UTT\AnalyticsBundle\Entity\ReportUserSession;
use UTT\AnalyticsBundle\Entity\ReportUserSessionRepository;
use UTT\UserBundle\Entity\User;

class ReportUserService {
    protected $_em;
    protected $reportUserFactory;

    public function __construct(EntityManager $em, ReportUserFactory $reportUserFactory){
        $this->_em = $em;
        $this->reportUserFactory = $reportUserFactory;
    }

    public function create($trackingCookieHash, User $user){
        /** @var ReportUser $reportUser */
        $reportUser = $this->reportUserFactory->create($trackingCookieHash, $user);
        return $this->save($reportUser);
    }

    public function createReportUserSession(ReportUser $reportUser, $sessionId){
        /** @var ReportUserSession $reportUserSession */
        $reportUserSession = $this->reportUserFactory->createReportUserSession($reportUser, $sessionId);
        return $this->save($reportUserSession);
    }

    public function save($object){
        $this->_em->persist($object);
        $this->_em->flush();

        return $object;
    }

    public function saveAll(){
        $this->_em->flush();

        return true;
    }

    public function find($trackingCookieHash, User $user){
        /** @var ReportUserRepository $reportUserRepository */
        $reportUserRepository = $this->_em->getRepository('UTTAnalyticsBundle:ReportUser');
        $reportUser = $reportUserRepository->findOneBy(array(
            'trackingCookieHash' => $trackingCookieHash,
            'user' => $user->getId()
        ));
        if($reportUser instanceof ReportUser){
            return $reportUser;
        }

        return false;
    }

    public function findReportUserSession(ReportUser $reportUser, $sessionId){
        /** @var ReportUserSessionRepository $reportUserSessionRepository */
        $reportUserSessionRepository = $this->_em->getRepository('UTTAnalyticsBundle:ReportUserSession');
        $reportUserSession = $reportUserSessionRepository->findOneBy(array(
            'reportUser' => $reportUser->getId(),
            'sessionId' => $sessionId
        ));
        if($reportUserSession instanceof ReportUserSession){
            return $reportUserSession;
        }

        return false;
    }

    public function allowedUserForAnalyticsMail(User $user){
        if($user->getType() == User::TYPE_ACC){ return false; }
        if($user->getType() == User::TYPE_ADMIN){ return false; }
        if($user->getType() == User::TYPE_CLEANER){ return false; }
        if($user->getType() == User::TYPE_OWNER){ return false; }

        return true;
    }
}