<?php

namespace UTT\AnalyticsBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

use UTT\EstateBundle\Entity\Estate;
use UTT\ReservationBundle\Entity\Reservation;
use UTT\ReservationBundle\Entity\ReservationRepository;

use UTT\UserBundle\Entity\User;

use Doctrine\ORM\EntityManager;
use UTT\UserBundle\Entity\ActivityLog;
use UTT\UserBundle\Entity\ActivityLogRepository;

use UTT\AnalyticsBundle\Service\ReportRefererService;
use UTT\AnalyticsBundle\Entity\ReportReferer;
use UTT\AnalyticsBundle\Entity\ReportRefererRepository;

class ReportRefererUpdateCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('analytics:reportRefererUpdate')
            ->setDescription('')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        /** @var EntityManager $_em */
        $_em = $this->getContainer()->get('doctrine')->getManager();

        /** @var ReportRefererService $reportRefererService */
        $reportRefererService = $this->getContainer()->get('utt.reportRefererService');
        /** @var ActivityLogRepository $activityLogRepository */
        $activityLogRepository = $_em->getRepository('UTTUserBundle:ActivityLog');
        /** @var ReportRefererRepository $reportRefererRepository */
        $reportRefererRepository = $_em->getRepository('UTTAnalyticsBundle:ReportReferer');
        /** @var ReservationRepository $reservationRepository */
        $reservationRepository = $_em->getRepository('UTTReservationBundle:Reservation');

        $reservations = $reservationRepository->findForActivityLog();

        if($reservations){
            /** @var Reservation $reservation */
            foreach($reservations as $reservation){
                $trackingCookieHash = $activityLogRepository->findTrackingCookieHashBySessionId($reservation->getSessionId());
                $activityLogs = $activityLogRepository->findByReservationSessionIdTrackingCookieHash($reservation->getSessionId(), $trackingCookieHash);

                $referer = '';
                $refererType = null;

                /** @var ActivityLog $activityLog */
                foreach($activityLogs as $activityLog){
                    if($activityLog->getCreatedAt() < $reservation->getCreatedAt()){
                        if(isset($activityLog->getQueryParamsDecoded()->rf)){
                            $referer = $activityLog->getQueryParamsDecoded()->rf;
                            $refererType = ReportReferer::REFERER_TYPE_WEBSITE;
                            break;
                        }elseif(isset($activityLog->getQueryParamsDecoded()->mailchimp)){
                            $referer = $activityLog->getQueryParamsDecoded()->mailchimp;
                            $refererType = ReportReferer::REFERER_TYPE_NEWSLETTER;
                            break;
                        }elseif(isset($activityLog->getQueryParamsDecoded()->utm_campaign)){
                            $referer = $activityLog->getQueryParamsDecoded()->utm_campaign;
                            $refererType = ReportReferer::REFERER_TYPE_NEWSLETTER;
                            break;
                        }else if (strpos($activityLog->getRefererUrl(), 'http://www.underthethatch.co.uk') !== false) {

                        }else{
                            $referer = parse_url($activityLog->getRefererUrl(), PHP_URL_HOST);
                            $refererType = ReportReferer::REFERER_TYPE_WEBSITE;
                            break;
                        }
                    }
                }

                if($referer == ''){
                    $referer = '';
                    $refererType = ReportReferer::REFERER_TYPE_DIRECT;
                }

                /*
                if($reservation->getDiscountPriceDecoded()->getDiscountCode()){
                    if($referer){
                        $referer .= ' ( discount code '.$reservation->getDiscountPriceDecoded()->getDiscountCode().' )';
                    }else{
                        $referer = $reservation->getDiscountPriceDecoded()->getDiscountCode();
                    }
                }
                */

                /** @var ReportReferer $reportReferer */
                $reportReferer = $reportRefererRepository->findOneBy(array('reservation' => $reservation->getId(), 'refererType' => $refererType));
                if(!($reportReferer instanceof ReportReferer)){
                    /** @var ReportReferer $reportReferer */
                    $reportReferer = $reportRefererService->create($reservation, $refererType, $referer);
                    if($reportReferer instanceof ReportReferer){
                        $nowDate = new \DateTime('now');
                        echo $nowDate->format('Y-m-d H:i:s').' | ReportReferer CREATED: '.$reportReferer->getReservation()->getId().PHP_EOL;
                    }
                }

                if($reservation->getDiscountPriceDecoded()->getDiscountCode()){
                    $referer = $reservation->getDiscountPriceDecoded()->getDiscountCode().' - £'.$reservation->getDiscountPriceDecoded()->getDiscountCodePrice();
                    $refererType = ReportReferer::REFERER_TYPE_DISCOUNT_CODE;

                    /** @var ReportReferer $reportReferer */
                    $reportReferer = $reportRefererRepository->findOneBy(array('reservation' => $reservation->getId(), 'refererType' => $refererType));
                    if(!($reportReferer instanceof ReportReferer)){
                        /** @var ReportReferer $reportReferer */
                        $reportReferer = $reportRefererService->create($reservation, $refererType, $referer);
                        if($reportReferer instanceof ReportReferer){
                            $nowDate = new \DateTime('now');
                            echo $nowDate->format('Y-m-d H:i:s').' | ReportReferer CREATED: '.$reportReferer->getReservation()->getId().PHP_EOL;
                        }
                    }
                }
            }
        }
    }
}