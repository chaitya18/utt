<?php

namespace UTT\AnalyticsBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

use UTT\EstateBundle\Entity\Estate;
use UTT\ReservationBundle\Entity\Reservation;
use UTT\ReservationBundle\Entity\ReservationRepository;

use UTT\UserBundle\Entity\User;

use Doctrine\ORM\EntityManager;
use UTT\UserBundle\Entity\ActivityLog;
use UTT\UserBundle\Entity\ActivityLogRepository;

use UTT\AnalyticsBundle\Service\ReportUserService;
use UTT\AnalyticsBundle\Entity\ReportUser;
use UTT\AnalyticsBundle\Entity\ReportUserRepository;
use UTT\AnalyticsBundle\Entity\ReportUserSession;
use UTT\AnalyticsBundle\Entity\ReportUserSessionRepository;

use UTT\EstateBundle\Entity\EstateRepository;
use UTT\QueueBundle\Service\ReportUserSessionEmailQueueService;
use UTT\IndexBundle\Service\ConfigurationService;
use UTT\IndexBundle\Entity\Configuration;

class ReportUserUpdateCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('analytics:reportUserUpdate')
            ->setDescription('')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        /** @var EntityManager $_em */
        $_em = $this->getContainer()->get('doctrine')->getManager();

        /** @var ConfigurationService $configurationService */
        $configurationService = $this->getContainer()->get('utt.configurationservice');
        /** @var ReportUserService $reportUserService */
        $reportUserService = $this->getContainer()->get('utt.reportUserService');
        /** @var ReportUserSessionEmailQueueService $reportUserSessionEmailQueueService */
        $reportUserSessionEmailQueueService = $this->getContainer()->get('utt.reportUserSessionEmailQueueService');
        /** @var ActivityLogRepository $activityLogRepository */
        $activityLogRepository = $_em->getRepository('UTTUserBundle:ActivityLog');
        /** @var ReportUserRepository $reportUserRepository */
        $reportUserRepository = $_em->getRepository('UTTAnalyticsBundle:ReportUser');
        /** @var ReportUserSessionRepository $reportUserSessionRepository */
        $reportUserSessionRepository = $_em->getRepository('UTTAnalyticsBundle:ReportUserSession');
        /** @var EstateRepository $estateRepository */
        $estateRepository = $_em->getRepository('UTTEstateBundle:Estate');
        /** @var ReservationRepository $reservationRepository */
        $reservationRepository = $_em->getRepository('UTTReservationBundle:Reservation');

        $activityLogsGrouped = $activityLogRepository->findGroupedForReportUserUpdate();
        if($activityLogsGrouped){
            /** @var ActivityLog $activityLog */
            foreach($activityLogsGrouped as $activityLog){
                /** @var ReportUser $reportUser */
                $reportUser = $reportUserService->find($activityLog->getTrackingCookieHash(), $activityLog->getUser());
                if(!($reportUser instanceof ReportUser)){
                    $reportUser = $reportUserService->create($activityLog->getTrackingCookieHash(), $activityLog->getUser());
                    if($reportUser instanceof ReportUser){
                        $nowDate = new \DateTime('now');
                        echo $nowDate->format('Y-m-d H:i:s').' | ReportUser CREATED: '.$reportUser->getUser()->getEmail().' '.$reportUser->getTrackingCookieHash().PHP_EOL;
                    }
                }
            }
        }

        $reportUsers = $reportUserRepository->findAll();
        if($reportUsers){
            /** @var ReportUser $reportUser */
            foreach($reportUsers as $reportUser){
                $activityLogsGrouped = $activityLogRepository->findByTrackingCookieHashGroupedBySession($reportUser->getTrackingCookieHash());
                if($activityLogsGrouped){
                    /** @var ActivityLog $activityLog */
                    foreach($activityLogsGrouped as $activityLog){
                        /** @var ReportUserSession $reportUserSession */
                        $reportUserSession = $reportUserService->findReportUserSession($reportUser, $activityLog->getSessionId());
                        if(!($reportUserSession instanceof ReportUserSession)){
                            $reportUserSession = $reportUserService->createReportUserSession($reportUser, $activityLog->getSessionId());
                            if($reportUserSession instanceof ReportUserSession){
                                $nowDate = new \DateTime('now');
                                echo $nowDate->format('Y-m-d H:i:s').' | ReportUserSession CREATED: '.$reportUserSession->getSessionId().' '.$reportUserSession->getReportUser()->getUser()->getEmail().PHP_EOL;
                            }
                        }
                    }
                }
            }
        }

        $reportUserSessions = $reportUserSessionRepository->findAll();
        if($reportUserSessions){
            /** @var ReportUserSession $reportUserSession */
            foreach($reportUserSessions as $reportUserSession){
                $activityLogs = $activityLogRepository->findBySessionIdTrackingCookieHash($reportUserSession->getSessionId(), $reportUserSession->getReportUser()->getTrackingCookieHash());
                if($activityLogs){
                    $lastVisitAt = null;
                    $visitsNumber = count($activityLogs);
                    $points = 0;
                    $propertiesPoints = array();
                    $pointsReset = false;

                    /** @var ActivityLog $activityLog */
                    foreach($activityLogs as $activityLog){
                        if($lastVisitAt == null){
                            $lastVisitAt = $activityLog->getCreatedAt();
                        }elseif($activityLog->getCreatedAt() > $lastVisitAt){
                            $lastVisitAt = $activityLog->getCreatedAt();
                        }

                        if($activityLog->getRouteName() == 'utt_estate_show_estate'){
                            $points = $points + 1;

                            $routeParams = $activityLog->getRouteParamsDecoded();
                            if(isset($routeParams->estateShortName)){
                                $estate = $estateRepository->findOneByShortName($routeParams->estateShortName);
                                if($estate instanceof Estate){
                                    if(!array_key_exists($routeParams->estateShortName, $propertiesPoints)){
                                        $propertiesPoints[$routeParams->estateShortName] = 1;
                                    }else{
                                        $propertiesPoints[$routeParams->estateShortName]++;;
                                    }
                                }
                            }
                        }elseif($activityLog->getRouteName() == 'utt_estate_make_reservation'){
                            $points = $points + 3;

                            $routeParams = $activityLog->getRouteParamsDecoded();
                            if(isset($routeParams->estateShortName)){
                                $estate = $estateRepository->findOneByShortName($routeParams->estateShortName);
                                if($estate instanceof Estate){
                                    if(!array_key_exists($routeParams->estateShortName, $propertiesPoints)){
                                        $propertiesPoints[$routeParams->estateShortName] = 1;
                                    }else{
                                        $propertiesPoints[$routeParams->estateShortName]++;;
                                    }
                                }
                            }
                        }elseif($activityLog->getRouteName() == 'utt_reservation_confirm'){
                            $pointsReset = true;
                        }
                    }

                    if($pointsReset){
                        $points = 0;
                    }

                    $nowDate = new \DateTime('now');
                    $reportUserSession->setPropertiesPoints(json_encode($propertiesPoints));
                    $reportUserSession->setPoints($points);
                    $reportUserSession->setLastVisitAt($lastVisitAt);
                    $reportUserSession->setVisitsNumber($visitsNumber);
                    $reportUserSession->setUpdatedAt($nowDate);

                    $reportUserSession->getReportUser()->setUserLastVisitAt($lastVisitAt);
                    $reportUserSession->getReportUser()->setUpdatedAt($nowDate);

                    /** @var Reservation $reservation */
                    $reservation = $reservationRepository->findOneBySessionId($reportUserSession->getSessionId());
                    if($reservation instanceof Reservation){
                        $reportUserSession->setReservation($reservation);
                    }

                    $reportUserService->saveAll();

                    echo $nowDate->format('Y-m-d H:i:s').' | ReportUserSession UPDATED: '.$reportUserSession->getSessionId().' '.$reportUserSession->getReportUser()->getUser()->getEmail().PHP_EOL;


                    $metadata = $_em->getClassMetaData(get_class($reportUserSession));
                    $metadata->setIdGeneratorType(\Doctrine\ORM\Mapping\ClassMetadata::GENERATOR_TYPE_NONE);

                    $_em->getConnection()->getConfiguration()->setSQLLogger(null);
                }
            }
        }

        /** @var Configuration $configuration */
        $configuration = $configurationService->get();
        if($configuration instanceof Configuration && $configuration->getAnalyticsPointsLimit()){
            // reportUserSessionEmailQueueService
            $sessionsForQueue = $reportUserSessionRepository->getSessionsForQueue($configuration->getAnalyticsPointsLimit());
            if($sessionsForQueue){
                /** @var ReportUserSession $reportUserSession */
                foreach($sessionsForQueue as $reportUserSession){
                    if($reportUserSession->getIsDiscountEmailSent()) continue;
                    if(!$reportUserService->allowedUserForAnalyticsMail($reportUserSession->getReportUser()->getUser())) continue;

                    $reportUserSessionEmailQueueService->pushToQueue($reportUserSession);
                    echo 'ADDED TO QUEUE: '.$reportUserSession->getSessionId().PHP_EOL;
                }
            }
        }
    }
}