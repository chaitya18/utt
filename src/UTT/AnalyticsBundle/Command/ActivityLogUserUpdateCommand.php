<?php

namespace UTT\AnalyticsBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

use UTT\ReservationBundle\Entity\Reservation;
use UTT\ReservationBundle\Entity\ReservationRepository;

use UTT\UserBundle\Entity\User;

use Doctrine\ORM\EntityManager;
use UTT\UserBundle\Entity\ActivityLog;
use UTT\UserBundle\Entity\ActivityLogRepository;

class ActivityLogUserUpdateCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('activityLog:userUpdate')
            ->setDescription('Update user from booking for activity log')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        /** @var EntityManager $_em */
        $_em = $this->getContainer()->get('doctrine')->getManager();

        /** @var ReservationRepository $reservationRepository */
        $reservationRepository = $_em->getRepository('UTTReservationBundle:Reservation');
        /** @var ActivityLogRepository $activityLogRepository */
        $activityLogRepository = $_em->getRepository('UTTUserBundle:ActivityLog');

        $reservations = $reservationRepository->findForActivityLog();
        if($reservations) {
            /** @var Reservation $reservation */
            foreach ($reservations as $reservation) {
                $trackingCookieHash = $activityLogRepository->findTrackingCookieHashBySessionId($reservation->getSessionId());
                $activityLogs = $activityLogRepository->findByReservationSessionIdTrackingCookieHash($reservation->getSessionId(), $trackingCookieHash);
                if($activityLogs){
                    /** @var ActivityLog $activityLog */
                    foreach($activityLogs as $activityLog){
                        if(!($activityLog->getUser() instanceof User)){
                            $activityLog->setUser($reservation->getUser());
                        }
                    }
                }

                $_em->flush();

                echo $reservation->getId().PHP_EOL;
            }
        }

    }
}