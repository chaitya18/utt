<?php

namespace UTT\ApiBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use UTT\UserBundle\Entity\CountryRepository;
use UTT\ReservationBundle\Service\OfferService;

class ApiController extends Controller
{
    public function allCountriesAction(Request $request){
        $result = array('success' => false);

        /** @var CountryRepository $countryRepository */
        $countryRepository = $this->getDoctrine()->getManager()->getRepository('UTTUserBundle:Country');
        $countries = $countryRepository->findAllArray();
        if($countries){
            $result = array('success' => true, 'countries' => $countries);
        }

        return new JsonResponse($result);
    }
}
