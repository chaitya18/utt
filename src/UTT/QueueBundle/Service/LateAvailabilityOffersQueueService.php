<?php

namespace UTT\QueueBundle\Service;

use Doctrine\ORM\EntityManager;
use UTT\EstateBundle\Entity\Estate;
use UTT\QueueBundle\Entity\LateAvailabilityOffersQueue;
use UTT\QueueBundle\Entity\LateAvailabilityOffersQueueRepository;

class LateAvailabilityOffersQueueService {
    protected $_em;

    public function __construct(EntityManager $em){
        $this->_em = $em;
    }

    public function pushToQueue(Estate $estate){
        $lateAvailabilityOffersQueue = $this->findInQueue($estate);

        if(!($lateAvailabilityOffersQueue instanceof LateAvailabilityOffersQueue)){
            $lateAvailabilityOffersQueue = new LateAvailabilityOffersQueue();
        }

        $lateAvailabilityOffersQueue->setEstate($estate);

        try {
            $this->_em->persist($lateAvailabilityOffersQueue);
            $this->_em->flush();

            return $lateAvailabilityOffersQueue;
        }catch (\Exception $e){
            return false;
        }
    }

    public function removeFromQueue(Estate $estate){
        $lateAvailabilityOffersQueue = $this->findInQueue($estate);
        if(!($lateAvailabilityOffersQueue instanceof LateAvailabilityOffersQueue)){
            return false;
        }

        try {
            $this->_em->remove($lateAvailabilityOffersQueue);
            $this->_em->flush();

            return true;
        }catch (\Exception $e){
            return false;
        }
    }

    public function getLatest($limit){
        /** @var LateAvailabilityOffersQueueRepository $lateAvailabilityOffersQueueRepository */
        $lateAvailabilityOffersQueueRepository = $this->_em->getRepository('UTTQueueBundle:LateAvailabilityOffersQueue');
        return $lateAvailabilityOffersQueueRepository->getLatest($limit);
    }

    public function findInQueue(Estate $estate){
        $lateAvailabilityOffersQueue = $this->_em->getRepository('UTTQueueBundle:LateAvailabilityOffersQueue')->findOneBy(array(
            'estate' => $estate->getId()
        ));

        if($lateAvailabilityOffersQueue instanceof LateAvailabilityOffersQueue){
            return $lateAvailabilityOffersQueue;
        }

        return false;
    }
}