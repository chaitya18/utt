<?php

namespace UTT\QueueBundle\Service;

use Doctrine\ORM\EntityManager;
use UTT\AnalyticsBundle\Entity\ReportUserSession;
use UTT\QueueBundle\Entity\ReportUserSessionEmailQueue;
use UTT\QueueBundle\Entity\ReportUserSessionEmailQueueRepository;

class ReportUserSessionEmailQueueService {
    protected $_em;

    public function __construct(EntityManager $em){
        $this->_em = $em;
    }

    public function pushToQueue(ReportUserSession $reportUserSession){
        $reportUserSessionEmailQueue = $this->findInQueue($reportUserSession);

        if(!($reportUserSessionEmailQueue instanceof ReportUserSessionEmailQueue)){
            $reportUserSessionEmailQueue = new ReportUserSessionEmailQueue();
        }

        $reportUserSessionEmailQueue->setReportUserSession($reportUserSession);

        try {
            $this->_em->persist($reportUserSessionEmailQueue);
            $this->_em->flush();

            return $reportUserSessionEmailQueue;
        }catch (\Exception $e){
            return false;
        }
    }

    public function removeFromQueue(ReportUserSession $reportUserSession){
        $reportUserSessionEmailQueue = $this->findInQueue($reportUserSession);
        if(!($reportUserSessionEmailQueue instanceof ReportUserSessionEmailQueue)){
            return false;
        }

        try {
            $this->_em->remove($reportUserSessionEmailQueue);
            $this->_em->flush();

            return true;
        }catch (\Exception $e){
            return false;
        }
    }

    public function getLatest($limit){
        /** @var ReportUserSessionEmailQueueRepository $reportUserSessionEmailQueueRepository */
        $reportUserSessionEmailQueueRepository = $this->_em->getRepository('UTTQueueBundle:ReportUserSessionEmailQueue');
        return $reportUserSessionEmailQueueRepository->getLatest($limit);
    }

    public function findInQueue(ReportUserSession $reportUserSession){
        $reportUserSessionEmailQueue = $this->_em->getRepository('UTTQueueBundle:ReportUserSessionEmailQueue')->findOneBy(array(
            'reportUserSession' => $reportUserSession->getId()
        ));

        if($reportUserSessionEmailQueue instanceof ReportUserSessionEmailQueue){
            return $reportUserSessionEmailQueue;
        }

        return false;
    }
}