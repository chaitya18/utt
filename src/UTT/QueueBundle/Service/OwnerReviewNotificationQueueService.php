<?php

namespace UTT\QueueBundle\Service;

use Doctrine\ORM\EntityManager;
use UTT\EstateBundle\Entity\Review;
use UTT\QueueBundle\Entity\OwnerReviewNotificationQueue;
use UTT\QueueBundle\Entity\OwnerReviewNotificationQueueRepository;

class OwnerReviewNotificationQueueService {
    protected $_em;

    public function __construct(EntityManager $em){
        $this->_em = $em;
    }

    public function pushToQueue(Review $review){
        $ownerReviewNotificationQueue = $this->findInQueue($review);

        if(!($ownerReviewNotificationQueue instanceof OwnerReviewNotificationQueue)){
            $ownerReviewNotificationQueue = new OwnerReviewNotificationQueue();
        }

        $ownerReviewNotificationQueue->setReview($review);

        try {
            $this->_em->persist($ownerReviewNotificationQueue);
            $this->_em->flush();

            return $ownerReviewNotificationQueue;
        }catch (\Exception $e){
            return false;
        }
    }

    public function removeFromQueue(Review $review){
        $ownerReviewNotificationQueue = $this->findInQueue($review);
        if(!($ownerReviewNotificationQueue instanceof OwnerReviewNotificationQueue)){
            return false;
        }

        try {
            $this->_em->remove($ownerReviewNotificationQueue);
            $this->_em->flush();

            return true;
        }catch (\Exception $e){
            return false;
        }
    }

    public function getLatest($limit){
        /** @var OwnerReviewNotificationQueueRepository $ownerReviewNotificationQueueRepository */
        $ownerReviewNotificationQueueRepository = $this->_em->getRepository('UTTQueueBundle:OwnerReviewNotificationQueue');
        return $ownerReviewNotificationQueueRepository->getLatest($limit);
    }

    public function findInQueue(Review $review){
        $ownerReviewNotificationQueue = $this->_em->getRepository('UTTQueueBundle:OwnerReviewNotificationQueue')->findOneBy(array(
            'review' => $review->getId()
        ));

        if($ownerReviewNotificationQueue instanceof OwnerReviewNotificationQueue){
            return $ownerReviewNotificationQueue;
        }

        return false;
    }
}