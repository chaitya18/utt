<?php

namespace UTT\QueueBundle\Service;

use Doctrine\ORM\EntityManager;
use UTT\EstateBundle\Entity\Estate;
use UTT\QueueBundle\Entity\PropertyStatementQueue;
use UTT\QueueBundle\Entity\PropertyStatementQueueRepository;

class PropertyStatementQueueService {
    protected $_em;

    public function __construct(EntityManager $em){
        $this->_em = $em;
    }

    public function pushToQueue(Estate $estate, \Datetime $date){
        $propertyStatementQueue = $this->findInQueue($estate, $date);

        if(!($propertyStatementQueue instanceof PropertyStatementQueue)){
            $propertyStatementQueue = new PropertyStatementQueue();
        }

        $propertyStatementQueue->setEstate($estate);
        $propertyStatementQueue->setMonthDate($date);

        try {
            $this->_em->persist($propertyStatementQueue);
            $this->_em->flush();

            return $propertyStatementQueue;
        }catch (\Exception $e){
            return false;
        }
    }

    public function removeFromQueue(Estate $estate, \Datetime $date){
        $propertyStatementQueue = $this->findInQueue($estate, $date);
        if(!($propertyStatementQueue instanceof PropertyStatementQueue)){
            return false;
        }

        try {
            $this->_em->remove($propertyStatementQueue);
            $this->_em->flush();

            return true;
        }catch (\Exception $e){
            return false;
        }
    }

    public function getLatest($limit){
        /** @var PropertyStatementQueueRepository $propertyStatementQueueRepository */
        $propertyStatementQueueRepository = $this->_em->getRepository('UTTQueueBundle:PropertyStatementQueue');
        return $propertyStatementQueueRepository->getLatest($limit);
    }

    public function getQueueForDate(\Datetime $date){
        $propertyStatementQueues = $this->_em->getRepository('UTTQueueBundle:PropertyStatementQueue')->findBy(array(
            'monthDate' => $date
        ));

        if($propertyStatementQueues){
            return $propertyStatementQueues;
        }

        return false;
    }

    public function findInQueue(Estate $estate, \Datetime $date){
        $propertyStatementQueue = $this->_em->getRepository('UTTQueueBundle:PropertyStatementQueue')->findOneBy(array(
            'estate' => $estate->getId(),
            'monthDate' => $date
        ));

        if($propertyStatementQueue instanceof PropertyStatementQueue){
            return $propertyStatementQueue;
        }

        return false;
    }
}