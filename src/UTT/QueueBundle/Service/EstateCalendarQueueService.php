<?php

namespace UTT\QueueBundle\Service;

use Doctrine\ORM\EntityManager;
use UTT\EstateBundle\Entity\Estate;
use UTT\QueueBundle\Entity\EstateCalendarQueue;
use UTT\QueueBundle\Entity\EstateCalendarQueueRepository;

class EstateCalendarQueueService {
    protected $_em;

    public function __construct(EntityManager $em){
        $this->_em = $em;
    }

    public function pushToQueue(Estate $estate){
        $estateCalendarQueue = $this->findInQueue($estate);

        if(!($estateCalendarQueue instanceof EstateCalendarQueue)){
            $estateCalendarQueue = new EstateCalendarQueue();
        }

        $estateCalendarQueue->setEstate($estate);

        try {
            $this->_em->persist($estateCalendarQueue);
            $this->_em->flush();

            return $estateCalendarQueue;
        }catch (\Exception $e){
            return false;
        }
    }

    public function removeFromQueue(Estate $estate){
        $estateCalendarQueue = $this->findInQueue($estate);
        if(!($estateCalendarQueue instanceof EstateCalendarQueue)){
            return false;
        }

        try {
            $this->_em->remove($estateCalendarQueue);
            $this->_em->flush();

            return true;
        }catch (\Exception $e){
            return false;
        }
    }

    public function getLatest($limit){
        /** @var EstateCalendarQueueRepository $estateCalendarQueueRepository */
        $estateCalendarQueueRepository = $this->_em->getRepository('UTTQueueBundle:EstateCalendarQueue');
        return $estateCalendarQueueRepository->getLatest($limit);
    }

    public function findInQueue(Estate $estate){
        $estateCalendarQueue = $this->_em->getRepository('UTTQueueBundle:EstateCalendarQueue')->findOneBy(array(
            'estate' => $estate->getId()
        ));

        if($estateCalendarQueue instanceof EstateCalendarQueue){
            return $estateCalendarQueue;
        }

        return false;
    }
}