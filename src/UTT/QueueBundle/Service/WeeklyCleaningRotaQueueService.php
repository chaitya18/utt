<?php

namespace UTT\QueueBundle\Service;

use Doctrine\ORM\EntityManager;
use UTT\EstateBundle\Entity\Estate;
use UTT\QueueBundle\Entity\WeeklyCleaningRotaQueue;
use UTT\QueueBundle\Entity\WeeklyCleaningRotaQueueRepository;

class WeeklyCleaningRotaQueueService {
    protected $_em;

    public function __construct(EntityManager $em){
        $this->_em = $em;
    }

    public function pushToQueue(Estate $estate){
        $weeklyCleaningRotaQueue = $this->findInQueue($estate);

        if(!($weeklyCleaningRotaQueue instanceof WeeklyCleaningRotaQueue)){
            $weeklyCleaningRotaQueue = new WeeklyCleaningRotaQueue();
        }

        $weeklyCleaningRotaQueue->setEstate($estate);

        try {
            $this->_em->persist($weeklyCleaningRotaQueue);
            $this->_em->flush();

            return $weeklyCleaningRotaQueue;
        }catch (\Exception $e){
            return false;
        }
    }

    public function removeFromQueue(Estate $estate){
        $weeklyCleaningRotaQueue = $this->findInQueue($estate);
        if(!($weeklyCleaningRotaQueue instanceof WeeklyCleaningRotaQueue)){
            return false;
        }

        try {
            $this->_em->remove($weeklyCleaningRotaQueue);
            $this->_em->flush();

            return true;
        }catch (\Exception $e){
            return false;
        }
    }

    public function getLatest($limit){
        /** @var WeeklyCleaningRotaQueueRepository $weeklyCleaningRotaQueueRepository */
        $weeklyCleaningRotaQueueRepository = $this->_em->getRepository('UTTQueueBundle:WeeklyCleaningRotaQueue');
        return $weeklyCleaningRotaQueueRepository->getLatest($limit);
    }

    public function findInQueue(Estate $estate){
        $weeklyCleaningRotaQueue = $this->_em->getRepository('UTTQueueBundle:WeeklyCleaningRotaQueue')->findOneBy(array(
            'estate' => $estate->getId()
        ));

        if($weeklyCleaningRotaQueue instanceof WeeklyCleaningRotaQueue){
            return $weeklyCleaningRotaQueue;
        }

        return false;
    }
}