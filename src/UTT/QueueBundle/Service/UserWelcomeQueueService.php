<?php

namespace UTT\QueueBundle\Service;

use Doctrine\ORM\EntityManager;
use UTT\UserBundle\Entity\User;
use UTT\QueueBundle\Entity\UserWelcomeQueue;
use UTT\QueueBundle\Entity\UserWelcomeQueueRepository;

class UserWelcomeQueueService {
    protected $_em;

    public function __construct(EntityManager $em){
        $this->_em = $em;
    }

    public function pushToQueue(User $user){
        $userWelcomeQueue = $this->findInQueue($user);

        if(!($userWelcomeQueue instanceof UserWelcomeQueue)){
            $userWelcomeQueue = new UserWelcomeQueue();
        }

        $userWelcomeQueue->setUser($user);

        try {
            $this->_em->persist($userWelcomeQueue);
            $this->_em->flush();

            return $userWelcomeQueue;
        }catch (\Exception $e){
            return false;
        }
    }

    public function removeFromQueue(User $user){
        $userWelcomeQueue = $this->findInQueue($user);
        if(!($userWelcomeQueue instanceof UserWelcomeQueue)){
            return false;
        }

        try {
            $this->_em->remove($userWelcomeQueue);
            $this->_em->flush();

            return true;
        }catch (\Exception $e){
            return false;
        }
    }

    public function getLatest($limit){
        /** @var UserWelcomeQueueRepository $userWelcomeQueueRepository */
        $userWelcomeQueueRepository = $this->_em->getRepository('UTTQueueBundle:UserWelcomeQueue');
        return $userWelcomeQueueRepository->getLatest($limit);
    }

    public function findInQueue(User $user){
        $userWelcomeQueue = $this->_em->getRepository('UTTQueueBundle:UserWelcomeQueue')->findOneBy(array(
            'user' => $user->getId()
        ));

        if($userWelcomeQueue instanceof UserWelcomeQueue){
            return $userWelcomeQueue;
        }

        return false;
    }
}