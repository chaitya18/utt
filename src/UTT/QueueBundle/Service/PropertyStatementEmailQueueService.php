<?php

namespace UTT\QueueBundle\Service;

use Doctrine\ORM\EntityManager;
use UTT\ReservationBundle\Entity\OwnerStatement;
use UTT\QueueBundle\Entity\PropertyStatementEmailQueue;
use UTT\QueueBundle\Entity\PropertyStatementEmailQueueRepository;

class PropertyStatementEmailQueueService {
    protected $_em;

    public function __construct(EntityManager $em){
        $this->_em = $em;
    }

    public function pushToQueue(OwnerStatement $ownerStatement){
        $propertyStatementEmailQueue = $this->findInQueue($ownerStatement);

        if(!($propertyStatementEmailQueue instanceof PropertyStatementEmailQueue)){
            $propertyStatementEmailQueue = new PropertyStatementEmailQueue();
        }

        $propertyStatementEmailQueue->setOwnerStatement($ownerStatement);

        try {
            $this->_em->persist($propertyStatementEmailQueue);
            $this->_em->flush();

            return $propertyStatementEmailQueue;
        }catch (\Exception $e){
            return false;
        }
    }

    public function removeFromQueue(OwnerStatement $ownerStatement){
        $propertyStatementEmailQueue = $this->findInQueue($ownerStatement);
        if(!($propertyStatementEmailQueue instanceof PropertyStatementEmailQueue)){
            return false;
        }

        try {
            $this->_em->remove($propertyStatementEmailQueue);
            $this->_em->flush();

            return true;
        }catch (\Exception $e){
            return false;
        }
    }

    public function getLatest($limit){
        /** @var PropertyStatementEmailQueueRepository $propertyStatementEmailQueueRepository */
        $propertyStatementEmailQueueRepository = $this->_em->getRepository('UTTQueueBundle:PropertyStatementEmailQueue');
        return $propertyStatementEmailQueueRepository->getLatest($limit);
    }

    public function findInQueue(OwnerStatement $ownerStatement){
        $propertyStatementEmailQueue = $this->_em->getRepository('UTTQueueBundle:PropertyStatementEmailQueue')->findOneBy(array(
            'ownerStatement' => $ownerStatement->getId()
        ));

        if($propertyStatementEmailQueue instanceof PropertyStatementEmailQueue){
            return $propertyStatementEmailQueue;
        }

        return false;
    }
}