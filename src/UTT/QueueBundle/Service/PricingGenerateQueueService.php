<?php

namespace UTT\QueueBundle\Service;

use Doctrine\ORM\EntityManager;
use UTT\EstateBundle\Entity\Estate;
use UTT\QueueBundle\Entity\PricingGenerateQueue;
use UTT\QueueBundle\Entity\PricingGenerateQueueRepository;

class PricingGenerateQueueService {
    protected $_em;

    public function __construct(EntityManager $em){
        $this->_em = $em;
    }

    public function pushToQueue(Estate $estate){
        $pricingGenerateQueue = $this->findInQueue($estate);

        if(!($pricingGenerateQueue instanceof PricingGenerateQueue)){
            $pricingGenerateQueue = new PricingGenerateQueue();
        }

        $pricingGenerateQueue->setEstate($estate);

        try {
            $this->_em->persist($pricingGenerateQueue);
            $this->_em->flush();

            return $pricingGenerateQueue;
        }catch (\Exception $e){
            return false;
        }
    }

    public function removeFromQueue(Estate $estate){
        $pricingGenerateQueue = $this->findInQueue($estate);
        if(!($pricingGenerateQueue instanceof PricingGenerateQueue)){
            return false;
        }

        try {
            $this->_em->remove($pricingGenerateQueue);
            $this->_em->flush();

            return true;
        }catch (\Exception $e){
            return false;
        }
    }

    public function getLatest($limit){
        /** @var PricingGenerateQueueRepository $pricingGenerateQueueRepository */
        $pricingGenerateQueueRepository = $this->_em->getRepository('UTTQueueBundle:PricingGenerateQueue');
        return $pricingGenerateQueueRepository->getLatest($limit);
    }

    public function findInQueue(Estate $estate){
        $pricingGenerateQueue = $this->_em->getRepository('UTTQueueBundle:PricingGenerateQueue')->findOneBy(array(
            'estate' => $estate->getId()
        ));

        if($pricingGenerateQueue instanceof PricingGenerateQueue){
            return $pricingGenerateQueue;
        }

        return false;
    }
}