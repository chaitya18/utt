<?php
namespace UTT\QueueBundle\Command;

ini_set('date.timezone', 'Europe/London');

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

use UTT\QueueBundle\Service\WeeklyCleaningRotaQueueService;
use UTT\QueueBundle\Entity\WeeklyCleaningRotaQueue;
use UTT\EstateBundle\Entity\Estate;
use UTT\EstateBundle\Entity\EstateRepository;

class QueueWeeklyCleaningRotaCreateCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('queue:weeklyCleaningRotaCreate')
            ->setDescription('Create queue for weekly cleaning rota e-mails')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $_em = $this->getContainer()->get('doctrine')->getManager();
        /** @var EstateRepository $estateRepository */
        $estateRepository = $_em->getRepository('UTTEstateBundle:Estate');

        /** @var WeeklyCleaningRotaQueueService $weeklyCleaningRotaQueueService */
        $weeklyCleaningRotaQueueService = $this->getContainer()->get('utt.weeklycleaningrotaqueueservice');

        $estates = $estateRepository->getAllActive();
        if($estates){
            /** @var Estate $estate */
            foreach($estates as $estate){
                /** @var WeeklyCleaningRotaQueue $weeklyCleaningRotaQueue */
                $weeklyCleaningRotaQueue = $weeklyCleaningRotaQueueService->pushToQueue($estate);
                if($weeklyCleaningRotaQueue instanceof WeeklyCleaningRotaQueue){
                    echo 'SUCCESS: '.$estate->getName().' added'.PHP_EOL;
                }else{
                    echo 'FAIL: '.$estate->getName().' NOT added'.PHP_EOL;
                }
            }
        }
    }
}
