<?php

namespace UTT\QueueBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

use UTT\IndexBundle\Service\EmailService;

use UTT\QueueBundle\Service\WeeklyCleaningRotaQueueService;
use UTT\QueueBundle\Entity\WeeklyCleaningRotaQueue;
use UTT\ReservationBundle\Entity\ReservationRepository;

class QueueWeeklyCleaningRotaRunCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('queue:weeklyCleaningRotaRun')
            ->setDescription('Run queue for weekly cleaning rota e-mails')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $_em = $this->getContainer()->get('doctrine')->getManager();

        /** @var ReservationRepository $reservationRepository */
        $reservationRepository = $_em->getRepository('UTTReservationBundle:Reservation');

        /** @var WeeklyCleaningRotaQueueService $weeklyCleaningRotaQueueService */
        $weeklyCleaningRotaQueueService = $this->getContainer()->get('utt.weeklycleaningrotaqueueservice');

        /** @var EmailService $emailService */
        $emailService = $this->getContainer()->get('utt.emailservice');

        $weeklyCleaningRotaQueues = $weeklyCleaningRotaQueueService->getLatest(2);
        if($weeklyCleaningRotaQueues){
            /** @var WeeklyCleaningRotaQueue $weeklyCleaningRotaQueue */
            foreach($weeklyCleaningRotaQueues as $weeklyCleaningRotaQueue){
                $estate = $weeklyCleaningRotaQueue->getEstate();
                $reservations = $reservationRepository->getForCleaningRota($estate, 31);
                if($reservations){
                    $emailService->sendWeeklyCleaningRotaEmail($estate, $reservations);
                }

                $weeklyCleaningRotaQueueService->removeFromQueue($estate);
                echo $estate->getName().PHP_EOL;
            }
        }else{
            echo 'Queue is empty'.PHP_EOL;
        }
    }
}