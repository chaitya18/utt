<?php

namespace UTT\QueueBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

use UTT\QueueBundle\Service\PricingGenerateQueueService;
use UTT\QueueBundle\Entity\PricingGenerateQueue;

use UTT\ReservationBundle\Service\PricingService;

use UTT\EstateBundle\Entity\Estate;
use UTT\ReservationBundle\Entity\PricingCategory;

class QueuePricingGenerateRunCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('queue:pricingGenerateRun')
            ->setDescription('Run queue for pricing generate')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $_em = $this->getContainer()->get('doctrine')->getManager();

        /** @var PricingGenerateQueueService $pricingGenerateQueueService */
        $pricingGenerateQueueService = $this->getContainer()->get('utt.pricinggeneratequeueservice');

        /** @var PricingService $pricingService */
        $pricingService = $this->getContainer()->get('utt.pricingservice');

        $pricingGenerateQueues = $pricingGenerateQueueService->getLatest(6);
        if($pricingGenerateQueues){
            /** @var PricingGenerateQueue $pricingGenerateQueue */
            foreach($pricingGenerateQueues as $pricingGenerateQueue){
                $estate = $pricingGenerateQueue->getEstate();




#################################################################################

                $pricingCategory = $estate->getPricingCategory();

                if($estate->getType() == Estate::TYPE_STANDARD_M_F && $pricingCategory->getType() == PricingCategory::TYPE_STANDARD_M_F){
                    $pricingService->generateStandardPricingForEstate($estate, $pricingCategory);
                }elseif($estate->getType() == Estate::TYPE_FLEXIBLE_BOOKING && $pricingCategory->getType() == PricingCategory::TYPE_FLEXIBLE_BOOKING){
                    $pricingService->generateFlexiblePricingForEstate($estate, $pricingCategory);
                }

                $lowestPrice = $pricingService->findLowestPriceByPricingCategory($estate);
                if($lowestPrice != -1){ $estate->setLowestPrice($lowestPrice); }

                $highestPrice = $pricingService->findHighestPrice($estate);
                if($highestPrice != -1){ $estate->setHighestPrice($highestPrice); }

                $_em->flush();

#################################################################################



                $pricingGenerateQueueService->removeFromQueue($estate);
                echo $estate->getName().PHP_EOL;
            }
        }else{
            echo 'Queue is empty'.PHP_EOL;
        }
    }
}
