<?php

namespace UTT\QueueBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

use UTT\QueueBundle\Service\LateAvailabilityOffersQueueService;
use UTT\QueueBundle\Entity\LateAvailabilityOffersQueue;

use UTT\ReservationBundle\Service\OfferService;

class QueueLateAvailabilityOffersRunCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('queue:lateAvailabilityOffersRun')
            ->setDescription('Run queue for late availability offers')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        /** @var LateAvailabilityOffersQueueService $lateAvailabilityOffersQueueService */
        $lateAvailabilityOffersQueueService = $this->getContainer()->get('utt.lateavailabilityoffersqueueservice');

        /** @var OfferService $offerService */
        $offerService = $this->getContainer()->get('utt.offerservice');

        $lateAvailabilityOffersQueues = $lateAvailabilityOffersQueueService->getLatest(2);
        if($lateAvailabilityOffersQueues){
            /** @var LateAvailabilityOffersQueue $lateAvailabilityOffersQueue */
            foreach($lateAvailabilityOffersQueues as $lateAvailabilityOffersQueue){
                $estate = $lateAvailabilityOffersQueue->getEstate();

                $offerService->updateLateAvailabilityOffers($estate);

                $lateAvailabilityOffersQueueService->removeFromQueue($estate);
                echo $estate->getName().PHP_EOL;
            }
        }else{
            echo 'Queue is empty'.PHP_EOL;
        }
    }
}