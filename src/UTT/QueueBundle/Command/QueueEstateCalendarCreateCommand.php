<?php

namespace UTT\QueueBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

use Doctrine\ORM\EntityManager;

use UTT\QueueBundle\Service\EstateCalendarQueueService;
use UTT\QueueBundle\Entity\EstateCalendarQueue;
use UTT\EstateBundle\Entity\Estate;
use UTT\EstateBundle\Entity\EstateRepository;
use UTT\EstateBundle\Entity\EstateCalendarDay;
use UTT\EstateBundle\Entity\EstateCalendarDayRepository;

class QueueEstateCalendarCreateCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('queue:estateCalendarCreate')
            ->setDescription('Create queue for estate calendar')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        /** @var EntityManager $_em */
        $_em = $this->getContainer()->get('doctrine')->getManager();
        /** @var EstateRepository $estateRepository */
        $estateRepository = $_em->getRepository('UTTEstateBundle:Estate');
        /** @var EstateCalendarDayRepository $estateCalendarDayRepository */
        $estateCalendarDayRepository = $_em->getRepository('UTTEstateBundle:EstateCalendarDay');
        $calendarDays = $estateCalendarDayRepository->getForNotActiveEstates();
        if($calendarDays){
            /** @var EstateCalendarDay $calendarDay */
            foreach($calendarDays as $calendarDay){
                if($calendarDay instanceof EstateCalendarDay){
                    echo 'REMOVED DAY '.$calendarDay->getId().' - '.$calendarDay->getEstate()->getName().PHP_EOL;
                    $_em->remove($calendarDay);
                }
            }
            $_em->flush();
        }

        /** @var EstateCalendarQueueService $estateCalendarQueueService */
        $estateCalendarQueueService = $this->getContainer()->get('utt.estatecalendarqueueservice');

        $estates = $estateRepository->getAllActive();
        if($estates){
            /** @var Estate $estate */
            foreach($estates as $estate){
                /** @var EstateCalendarQueue $estateCalendarQueue */
                $estateCalendarQueue = $estateCalendarQueueService->pushToQueue($estate);
                if($estateCalendarQueue instanceof EstateCalendarQueue){
                    echo 'SUCCESS: '.$estate->getName().' added'.PHP_EOL;
                }else{
                    echo 'FAIL: '.$estate->getName().' NOT added'.PHP_EOL;
                }
            }
        }
    }
}