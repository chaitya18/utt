<?php

namespace UTT\QueueBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

use UTT\EstateBundle\Entity\Estate;
use UTT\IndexBundle\Service\EmailService;

use Doctrine\ORM\EntityManager;

use UTT\QueueBundle\Service\ReportUserSessionEmailQueueService;
use UTT\QueueBundle\Entity\ReportUserSessionEmailQueue;
use UTT\ReservationBundle\Entity\ReservationRepository;
use UTT\ReservationBundle\Service\DiscountCodeService;
use UTT\AnalyticsBundle\Entity\ReportUserSession;
use UTT\EstateBundle\Entity\EstateRepository;
use UTT\ReservationBundle\Entity\DiscountCode;

class QueueReportUserSessionEmailRunCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('queue:reportUserSessionEmailRun')
            ->setDescription('Run queue for report user session email e-mails')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        /** @var EntityManager $_em */
        $_em = $this->getContainer()->get('doctrine')->getManager();
        /** @var EstateRepository $estateRepository */
        $estateRepository = $_em->getRepository('UTTEstateBundle:Estate');
        /** @var ReportUserSessionEmailQueueService $reportUserSessionEmailQueueService */
        $reportUserSessionEmailQueueService = $this->getContainer()->get('utt.reportUserSessionEmailQueueService');
        /** @var EmailService $emailService */
        $emailService = $this->getContainer()->get('utt.emailservice');
        /** @var DiscountCodeService $discountCodeService */
        $discountCodeService = $this->getContainer()->get('utt.discountcodeservice');

        $reportUserSessionEmailQueues = $reportUserSessionEmailQueueService->getLatest(2);
        if($reportUserSessionEmailQueues){
            /** @var ReportUserSessionEmailQueue $reportUserSessionEmailQueue */
            foreach($reportUserSessionEmailQueues as $reportUserSessionEmailQueue){
                /** @var ReportUserSession $reportUserSession */
                $reportUserSession = $reportUserSessionEmailQueue->getReportUserSession();
                $propertiesPoints = (array)$reportUserSession->getPropertiesPointsDecoded();
                $estates = array();
                foreach($propertiesPoints as $propertyShortName => $points){
                    $estate = $estateRepository->findOneByShortName($propertyShortName);
                    if($estate instanceof Estate){
                        $estates[] = $estate;
                    }
                }

                /** @var DiscountCode $discountCode */
                $discountCode = $discountCodeService->createReportUserSessionDiscount($estates);
                if($discountCode instanceof DiscountCode){
                    $reportUserSession->setDiscountCode($discountCode);

                    echo 'Discount code created: '.$discountCode->getId().' | '.$discountCode->getCode().PHP_EOL;
                    $emailSent = $emailService->sendDiscountCodeEmail($reportUserSession->getReportUser()->getUser()->getEmail(), $discountCode);
                    if($emailSent){

                        $reportUserSession->setIsDiscountEmailSent(true);
                        echo 'Email sent.'.PHP_EOL.PHP_EOL;
                    }
                }

                $_em->flush();

                $reportUserSessionEmailQueueService->removeFromQueue($reportUserSession);
            }
        }else{
            echo 'Queue is empty'.PHP_EOL;
        }
    }
}