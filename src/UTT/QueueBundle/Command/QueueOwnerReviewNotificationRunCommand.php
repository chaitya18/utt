<?php

namespace UTT\QueueBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use UTT\EstateBundle\Entity\Review;
use UTT\QueueBundle\Entity\OwnerReviewNotificationQueue;
use UTT\QueueBundle\Service\OwnerReviewNotificationQueueService;
use UTT\IndexBundle\Service\EmailService;
use Psr\Log\LoggerInterface;
class QueueOwnerReviewNotificationRunCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('queue:ownerReviewNotificationRun')
            ->setDescription('')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        /** @var EmailService $emailService */
        $emailService = $this->getContainer()->get('utt.emailservice');
        /** @var OwnerReviewNotificationQueueService $ownerReviewNotificationQueueService */
        $ownerReviewNotificationQueueService = $this->getContainer()->get('utt.ownerReviewNotificationQueueService');
        $ownerReviewNotificationQueues = $ownerReviewNotificationQueueService->getLatest(10);
        if($ownerReviewNotificationQueues){
            /** @var OwnerReviewNotificationQueue $ownerReviewNotificationQueue */
            foreach($ownerReviewNotificationQueues as $ownerReviewNotificationQueue){
                /** @var Review $review */
                $review = $ownerReviewNotificationQueue->getReview();

                $ownerReviewNotificationSent = $emailService->sendOwnerReviewNotificationEmail($review);
                $logger = $this->getContainer()->get('logger');
                    $logger->info("\n## started ".$this->getName()." cron at ".date('M j H:i')."####");
                if($ownerReviewNotificationSent){
                    $output->writeln("######### Mail Send To ".$review->getId()." Please Check Mail ########");
                    error_log(date('c')." Maile Send To : ". $review->getId() ."\n", 3, realpath( __DIR__.'/../../../../ownerNotificationReview.log'));
                    $ownerReviewNotificationQueueService->removeFromQueue($review);
                    // echo $review->getId().PHP_EOL;
                }
            }
        }else{
            echo 'Queue is empty'.PHP_EOL;
        }

    }
}