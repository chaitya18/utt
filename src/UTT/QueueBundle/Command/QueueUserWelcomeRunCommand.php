<?php

namespace UTT\QueueBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

use UTT\UserBundle\Entity\User;
use UTT\UserBundle\Entity\UserRepository;
use UTT\QueueBundle\Service\UserWelcomeQueueService;
use UTT\QueueBundle\Entity\UserWelcomeQueue;

use FOS\UserBundle\Model\UserManagerInterface;
use UTT\IndexBundle\Service\EmailService;

class QueueUserWelcomeRunCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('queue:userWelcomeRun')
            ->setDescription('Run queue for user welcome e-mails')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        /** @var UserWelcomeQueueService $userWelcomeQueueService */
        $userWelcomeQueueService = $this->getContainer()->get('utt.userwelcomequeueservice');

        /** @var UserManagerInterface $userManager */
        $userManager = $this->getContainer()->get('fos_user.user_manager');

        /** @var EmailService $emailService */
        $emailService = $this->getContainer()->get('utt.emailservice');

        $userWelcomeQueues = $userWelcomeQueueService->getLatest(2);
        if($userWelcomeQueues){
            /** @var UserWelcomeQueue $userWelcomeQueue */
            foreach($userWelcomeQueues as $userWelcomeQueue){
                $user = $userWelcomeQueue->getUser();
                $plainPassword = substr(md5($user->getEmail().'raczekUTT'), 0, 12);

                $user->setPlainPassword($plainPassword);
                $user->setEnabled(true);
                if($user->getType() == User::TYPE_ADMIN) {
                    $user->addRole('ROLE_SUPER_ADMIN');
                }elseif($user->getType() == User::TYPE_ACC){
                    $user->addRole('ROLE_ADMIN_ACC');
                }elseif($user->getType() == User::TYPE_OWNER){
                    $user->addRole('ROLE_ADMIN_OWNER');
                }elseif($user->getType() == User::TYPE_CLEANER){
                    $user->addRole('ROLE_ADMIN_CLEANER');
                }
                $userManager->updateUser($user);

                $emailService->sendUserWelcomeEmail($user, $plainPassword);

                $userWelcomeQueueService->removeFromQueue($user);
                echo $user->getEmail().PHP_EOL;
            }
        }else{
            echo 'Queue is empty'.PHP_EOL;
        }
    }
}