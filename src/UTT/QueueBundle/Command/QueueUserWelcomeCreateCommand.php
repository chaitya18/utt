<?php

namespace UTT\QueueBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

use UTT\UserBundle\Entity\User;
use UTT\UserBundle\Entity\UserRepository;
use UTT\QueueBundle\Service\UserWelcomeQueueService;
use UTT\QueueBundle\Entity\UserWelcomeQueue;

class QueueUserWelcomeCreateCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('queue:userWelcomeCreate')
            ->setDescription('Create queue for user welcome e-mails')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $_em = $this->getContainer()->get('doctrine')->getManager();
        /** @var UserRepository $userRepository */
        $userRepository = $_em->getRepository('UTTUserBundle:User');

        /** @var UserWelcomeQueueService $userWelcomeQueueService */
        $userWelcomeQueueService = $this->getContainer()->get('utt.userwelcomequeueservice');

        # owner users
        $ownerUsers = $userRepository->findBy(array('type' => User::TYPE_OWNER));
        if($ownerUsers){
            /** @var User $ownerUser */
            foreach($ownerUsers as $ownerUser){
                /** @var UserWelcomeQueue $userWelcomeQueue */
                $userWelcomeQueue = $userWelcomeQueueService->pushToQueue($ownerUser);
                if($userWelcomeQueue instanceof UserWelcomeQueue){
                    echo 'SUCCESS: '.$ownerUser->getTypeName().' | '.$ownerUser->getId().' added'.PHP_EOL;
                }else{
                    echo 'FAIL: '.$ownerUser->getId().' NOT added'.PHP_EOL;
                }
            }
        }

        # cleaner users
        $cleanerUsers = $userRepository->findBy(array('type' => User::TYPE_CLEANER));
        if($cleanerUsers){
            /** @var User $cleanerUser */
            foreach($cleanerUsers as $cleanerUser){
                /** @var UserWelcomeQueue $userWelcomeQueue */
                $userWelcomeQueue = $userWelcomeQueueService->pushToQueue($cleanerUser);
                if($userWelcomeQueue instanceof UserWelcomeQueue){
                    echo 'SUCCESS: '.$cleanerUser->getTypeName().' | '.$cleanerUser->getId().' added'.PHP_EOL;
                }else{
                    echo 'FAIL: '.$cleanerUser->getId().' NOT added'.PHP_EOL;
                }
            }
        }

        # acc users
        $accUsers = $userRepository->findBy(array('type' => User::TYPE_ACC));
        if($accUsers){
            /** @var User $accUser */
            foreach($accUsers as $accUser){
                /** @var UserWelcomeQueue $userWelcomeQueue */
                $userWelcomeQueue = $userWelcomeQueueService->pushToQueue($accUser);
                if($userWelcomeQueue instanceof UserWelcomeQueue){
                    echo 'SUCCESS: '.$accUser->getTypeName().' | '.$accUser->getId().' added'.PHP_EOL;
                }else{
                    echo 'FAIL: '.$accUser->getId().' NOT added'.PHP_EOL;
                }
            }
        }

        # admin users
        $adminUsers = $userRepository->findBy(array('type' => User::TYPE_ADMIN));
        if($adminUsers){
            /** @var User $adminUser */
            foreach($adminUsers as $adminUser){
                /** @var UserWelcomeQueue $userWelcomeQueue */
                $userWelcomeQueue = $userWelcomeQueueService->pushToQueue($adminUser);
                if($userWelcomeQueue instanceof UserWelcomeQueue){
                    echo 'SUCCESS: '.$adminUser->getTypeName().' | '.$adminUser->getId().' added'.PHP_EOL;
                }else{
                    echo 'FAIL: '.$adminUser->getId().' NOT added'.PHP_EOL;
                }
            }
        }
    }
}