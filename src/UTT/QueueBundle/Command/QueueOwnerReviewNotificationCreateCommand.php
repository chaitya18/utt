<?php

namespace UTT\QueueBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

use Doctrine\ORM\EntityManager;

use UTT\EstateBundle\Entity\Review;
use UTT\EstateBundle\Entity\ReviewRepository;
use UTT\QueueBundle\Entity\OwnerReviewNotificationQueue;
use UTT\QueueBundle\Service\OwnerReviewNotificationQueueService;

class QueueOwnerReviewNotificationCreateCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('queue:ownerReviewNotificationCreate')
            ->setDescription('')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        /** @var EntityManager $_em */
        $_em = $this->getContainer()->get('doctrine')->getManager();
        /** @var ReviewRepository $reviewRepository */
        $reviewRepository = $_em->getRepository('UTTEstateBundle:Review');
        /** @var OwnerReviewNotificationQueueService $ownerReviewNotificationQueueService */
        $ownerReviewNotificationQueueService = $this->getContainer()->get('utt.ownerReviewNotificationQueueService');

        $reviews = $reviewRepository->findBy(array('isVerified' => false));
        if($reviews){
            /** @var Review $review */
            foreach($reviews as $review){
                /** @var OwnerReviewNotificationQueue $ownerReviewNotificationQueue */
                $ownerReviewNotificationQueue = $ownerReviewNotificationQueueService->pushToQueue($review);
                if($ownerReviewNotificationQueue instanceof OwnerReviewNotificationQueue){
                    echo 'SUCCESS: '.$review->getId().' added'.PHP_EOL;
                }else{
                    echo 'FAIL: '.$review->getId().' NOT added'.PHP_EOL;
                }
            }
        }
    }
}