<?php

namespace UTT\QueueBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

use UTT\QueueBundle\Service\EstateCalendarQueueService;
use UTT\QueueBundle\Entity\EstateCalendarQueue;
use UTT\EstateBundle\Service\EstateCalendarService;

class QueueEstateCalendarRunCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('queue:estateCalendarRun')
            ->setDescription('Run queue for estate calendar')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        /** @var EstateCalendarQueueService $estateCalendarQueueService */
        $estateCalendarQueueService = $this->getContainer()->get('utt.estatecalendarqueueservice');
        /** @var EstateCalendarService $estateCalendarService */
        $estateCalendarService = $this->getContainer()->get('utt.estatecalendarservice');

        $estateCalendarQueues = $estateCalendarQueueService->getLatest(10);
        if($estateCalendarQueues){
            /** @var EstateCalendarQueue $estateCalendarQueue */
            foreach($estateCalendarQueues as $estateCalendarQueue){
                $estate = $estateCalendarQueue->getEstate();

                $estateCalendarService->generateEstateCalendar($estate);

                $estateCalendarQueueService->removeFromQueue($estate);
                echo $estate->getName().PHP_EOL;
            }
        }else{
            echo 'Queue is empty'.PHP_EOL;
        }
    }
}