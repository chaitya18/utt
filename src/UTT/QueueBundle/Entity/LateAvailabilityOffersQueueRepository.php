<?php

namespace UTT\QueueBundle\Entity;

use Doctrine\ORM\EntityRepository;

class LateAvailabilityOffersQueueRepository extends EntityRepository
{
    public function getLatest($limit = 2){
        $query = $this->_em->createQuery("SELECT laoq FROM UTTQueueBundle:LateAvailabilityOffersQueue laoq");
        if($limit) $query->setMaxResults($limit);

        $result = $query->getResult();
        if(count($result) > 0) return $result;

        return false;
    }
}
