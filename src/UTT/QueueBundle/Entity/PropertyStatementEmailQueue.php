<?php

namespace UTT\QueueBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * PropertyStatementEmailQueue
 *
 * @ORM\Table(name="queue_property_statement_email")
 * @ORM\Entity(repositoryClass="UTT\QueueBundle\Entity\PropertyStatementEmailQueueRepository")
 */
class PropertyStatementEmailQueue
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="UTT\ReservationBundle\Entity\OwnerStatement")
     * @ORM\JoinColumn(name="owner_statement_id", referencedColumnName="id", nullable=true, onDelete="CASCADE")
     */
    protected $ownerStatement;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set ownerStatement
     *
     * @param \UTT\ReservationBundle\Entity\OwnerStatement $ownerStatement
     * @return PropertyStatementEmailQueue
     */
    public function setOwnerStatement(\UTT\ReservationBundle\Entity\OwnerStatement $ownerStatement = null)
    {
        $this->ownerStatement = $ownerStatement;

        return $this;
    }

    /**
     * Get ownerStatement
     *
     * @return \UTT\ReservationBundle\Entity\OwnerStatement 
     */
    public function getOwnerStatement()
    {
        return $this->ownerStatement;
    }
}
