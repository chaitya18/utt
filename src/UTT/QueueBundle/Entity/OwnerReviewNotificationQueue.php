<?php

namespace UTT\QueueBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * OwnerReviewNotificationQueue
 *
 * @ORM\Table(name="queue_owner_review_notification")
 * @ORM\Entity(repositoryClass="UTT\QueueBundle\Entity\OwnerReviewNotificationQueueRepository")
 */
class OwnerReviewNotificationQueue
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="UTT\EstateBundle\Entity\Review")
     * @ORM\JoinColumn(name="review_id", referencedColumnName="id", nullable=true, onDelete="CASCADE")
     */
    protected $review;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set review
     *
     * @param \UTT\EstateBundle\Entity\Review $review
     * @return OwnerReviewNotificationQueue
     */
    public function setReview(\UTT\EstateBundle\Entity\Review $review = null)
    {
        $this->review = $review;

        return $this;
    }

    /**
     * Get review
     *
     * @return \UTT\EstateBundle\Entity\Review 
     */
    public function getReview()
    {
        return $this->review;
    }
}
