<?php

namespace UTT\QueueBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * PropertyStatementQueue
 *
 * @ORM\Table(name="queue_property_statement")
 * @ORM\Entity(repositoryClass="UTT\QueueBundle\Entity\PropertyStatementQueueRepository")
 */
class PropertyStatementQueue
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="UTT\EstateBundle\Entity\Estate")
     * @ORM\JoinColumn(name="estate_id", referencedColumnName="id", nullable=true, onDelete="CASCADE")
     */
    protected $estate;

    /**
     * @ORM\Column(name="monthDate", type="date")
     */
    protected $monthDate;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set monthDate
     *
     * @param \DateTime $monthDate
     * @return PropertyStatementQueue
     */
    public function setMonthDate($monthDate)
    {
        $this->monthDate = $monthDate;

        return $this;
    }

    /**
     * Get monthDate
     *
     * @return \DateTime 
     */
    public function getMonthDate()
    {
        return $this->monthDate;
    }

    /**
     * Set estate
     *
     * @param \UTT\EstateBundle\Entity\Estate $estate
     * @return PropertyStatementQueue
     */
    public function setEstate(\UTT\EstateBundle\Entity\Estate $estate = null)
    {
        $this->estate = $estate;

        return $this;
    }

    /**
     * Get estate
     *
     * @return \UTT\EstateBundle\Entity\Estate 
     */
    public function getEstate()
    {
        return $this->estate;
    }
}
