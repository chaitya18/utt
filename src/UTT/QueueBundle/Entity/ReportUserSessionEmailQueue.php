<?php

namespace UTT\QueueBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ReportUserSessionEmailQueue
 *
 * @ORM\Table(name="queue_report_user_session_email")
 * @ORM\Entity(repositoryClass="UTT\QueueBundle\Entity\ReportUserSessionEmailQueueRepository")
 */
class ReportUserSessionEmailQueue
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="UTT\AnalyticsBundle\Entity\ReportUserSession")
     * @ORM\JoinColumn(name="report_user_session_id", referencedColumnName="id", nullable=true, onDelete="CASCADE")
     */
    protected $reportUserSession;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set reportUserSession
     *
     * @param \UTT\AnalyticsBundle\Entity\ReportUserSession $reportUserSession
     * @return ReportUserSessionEmailQueue
     */
    public function setReportUserSession(\UTT\AnalyticsBundle\Entity\ReportUserSession $reportUserSession = null)
    {
        $this->reportUserSession = $reportUserSession;

        return $this;
    }

    /**
     * Get reportUserSession
     *
     * @return \UTT\AnalyticsBundle\Entity\ReportUserSession 
     */
    public function getReportUserSession()
    {
        return $this->reportUserSession;
    }
}
