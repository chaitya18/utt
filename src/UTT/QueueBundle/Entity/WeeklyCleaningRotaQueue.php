<?php

namespace UTT\QueueBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * WeeklyCleaningRotaQueue
 *
 * @ORM\Table(name="queue_weekly_cleaning_rota")
 * @ORM\Entity(repositoryClass="UTT\QueueBundle\Entity\WeeklyCleaningRotaQueueRepository")
 */
class WeeklyCleaningRotaQueue
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="UTT\EstateBundle\Entity\Estate")
     * @ORM\JoinColumn(name="estate_id", referencedColumnName="id", nullable=true, onDelete="CASCADE")
     */
    protected $estate;
    
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set estate
     *
     * @param \UTT\EstateBundle\Entity\Estate $estate
     * @return WeeklyCleaningRotaQueue
     */
    public function setEstate(\UTT\EstateBundle\Entity\Estate $estate = null)
    {
        $this->estate = $estate;

        return $this;
    }

    /**
     * Get estate
     *
     * @return \UTT\EstateBundle\Entity\Estate 
     */
    public function getEstate()
    {
        return $this->estate;
    }
}
