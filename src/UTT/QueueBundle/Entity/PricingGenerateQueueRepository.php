<?php

namespace UTT\QueueBundle\Entity;

use Doctrine\ORM\EntityRepository;

class PricingGenerateQueueRepository extends EntityRepository
{
    public function getLatest($limit = 2){
        $query = $this->_em->createQuery("SELECT pgq FROM UTTQueueBundle:PricingGenerateQueue pgq");
        if($limit) $query->setMaxResults($limit);

        $result = $query->getResult();
        if(count($result) > 0) return $result;

        return false;
    }
}
