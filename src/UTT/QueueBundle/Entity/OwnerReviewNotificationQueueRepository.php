<?php

namespace UTT\QueueBundle\Entity;

use Doctrine\ORM\EntityRepository;

class OwnerReviewNotificationQueueRepository extends EntityRepository
{
    public function getLatest($limit = 2){
        $query = $this->_em->createQuery("SELECT ornq FROM UTTQueueBundle:OwnerReviewNotificationQueue ornq");
        if($limit) $query->setMaxResults($limit);

        $result = $query->getResult();
        if(count($result) > 0) return $result;

        return false;
    }
}
