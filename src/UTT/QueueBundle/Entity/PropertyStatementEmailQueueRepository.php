<?php

namespace UTT\QueueBundle\Entity;

use Doctrine\ORM\EntityRepository;

class PropertyStatementEmailQueueRepository extends EntityRepository
{
    public function getLatest($limit = 2){
        $query = $this->_em->createQuery("SELECT pseq FROM UTTQueueBundle:PropertyStatementEmailQueue pseq");
        if($limit) $query->setMaxResults($limit);

        $result = $query->getResult();
        if(count($result) > 0) return $result;

        return false;
    }
}
