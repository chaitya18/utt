<?php

namespace UTT\QueueBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class QueueController extends Controller
{
    public function indexAction()
    {
        return $this->render('UTTQueueBundle:Queue:index.html.twig');
    }
}
