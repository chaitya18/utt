<?php

namespace UTT\UserBundle\Controller;

use Sonata\AdminBundle\Controller\CRUDController as Controller;
use UTT\UserBundle\Entity\User;
use Symfony\Component\HttpFoundation\Request;
use UTT\QueueBundle\Service\UserWelcomeQueueService;
use UTT\QueueBundle\Entity\UserWelcomeQueue;

class UserAdminController extends Controller
{
    public function userWelcomeResendAction(Request $request){
        /** @var User $user */
        $user = $this->admin->getSubject();
        if($user instanceof User && ($user->getType() == User::TYPE_CLEANER || $user->getType() == User::TYPE_OWNER || $user->getType() == User::TYPE_ACC)){
            $this->container->get('utt.aclService')->authForUserWelcomeResend();

            /** @var UserWelcomeQueueService $userWelcomeQueueService */
            $userWelcomeQueueService = $this->get('utt.userwelcomequeueservice');
            /** @var UserWelcomeQueue $userWelcomeQueue */
            $userWelcomeQueue = $userWelcomeQueueService->pushToQueue($user);
            if($userWelcomeQueue instanceof UserWelcomeQueue){
                $this->get('session')->getFlashBag()->add('success', 'Welcome email for '.$user->getEmail().' resent. ');
            }else{
                $this->get('session')->getFlashBag()->add('success', 'Welcome email for '.$user->getEmail().' NOT resent. ');
            }

            $referer = $request->headers->get('referer');
            if(!$referer) $referer = $this->admin->generateObjectUrl('list', $user);

            return $this->redirect($referer);
        }
        return $this->redirect($this->generateUrl('sonata_admin_dashboard'));
    }

}