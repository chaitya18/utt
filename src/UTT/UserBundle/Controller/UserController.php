<?php

namespace UTT\UserBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use FOS\UserBundle\Model\UserInterface;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use UTT\ReservationBundle\Entity\ReservationRepository;
use UTT\UserBundle\Entity\User;
use UTT\ReservationBundle\Entity\Reservation;
use UTT\ReservationBundle\Factory\ReservationFactory;

use UTT\EstateBundle\Form\Type\ReviewType;
use UTT\EstateBundle\Entity\Review;
use UTT\EstateBundle\Entity\ReviewRepository;
use UTT\EstateBundle\Entity\EstateRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;
use UTT\ReservationBundle\Service\ReservationService;
use UTT\IndexBundle\Service\EmailService;
use UTT\ReservationBundle\Service\DiscountCodeService;
use UTT\ReservationBundle\Entity\DiscountCode;
use UTT\ReservationBundle\Service\HistoryEntryService;
use UTT\ReservationBundle\Entity\HistoryEntry;
use UTT\QueueBundle\Service\OwnerReviewNotificationQueueService;
use UTT\ReservationBundle\Entity\Voucher;
use UTT\ReservationBundle\Entity\VoucherRepository;
use UTT\ReservationBundle\Factory\VoucherFactory;

class UserController extends Controller
{
    private function userAuth($throwException = true){
        if(!$this->container->get('security.context')->isGranted('IS_AUTHENTICATED_FULLY') ){
            if($throwException){
                throw new AccessDeniedException('This user does not have access to this section.');
            }else{
                return false;
            }
        }

        $user = $this->container->get('security.context')->getToken()->getUser();
        if (!is_object($user) || !$user instanceof UserInterface) {
            if($throwException){
                throw new AccessDeniedException('This user does not have access to this section.');
            }else{
                return false;
            }
        }

        return $user;
    }

    /**
     * Authenticate a user with Symfony Security
     *
     * @param \FOS\UserBundle\Model\UserInterface        $user
     * @param \Symfony\Component\HttpFoundation\Response $response
     */
    protected function authenticateUser(UserInterface $user, Response $response)
    {
        try {
            $this->container->get('fos_user.security.login_manager')->loginUser(
                $this->container->getParameter('fos_user.firewall_name'),
                $user,
                $response);
        } catch (AccountStatusException $ex) {
            // We simply do not authenticate users which do not pass the user
            // checker (not enabled, expired, etc.).
        }
    }

    public function reservationGuestNoteAction(Request $request, $reservationId){
        $user = $this->userAuth();

        /** @var ReservationRepository $reservationRepository */
        $reservationRepository = $this->getDoctrine()->getManager()->getRepository('UTTReservationBundle:Reservation');
        $reservation = $reservationRepository->find($reservationId);
        if(!($reservation instanceof Reservation)){
            return $this->redirect($this->generateUrl('fos_user_profile_show'));
        }

        /** @var ReservationService $reservationService */
        $reservationService = $this->get('utt.reservationservice');

        $postData = $request->request->all();
        $newHistoryCommentAdded = false;

        if(isset($postData['submitToAgent']) && $postData['submitToAgent'] == true){
            $newHistoryCommentAdded = $reservationService->newHistoryCommentFromGuestToAgent($reservation, $user, $postData['historyCommentText']);
        }elseif(isset($postData['submitToOwner']) && $postData['submitToOwner'] == true){
            $newHistoryCommentAdded = $reservationService->newHistoryCommentFromGuestToOwner($reservation, $user, $postData['historyCommentText']);
        }elseif(isset($postData['submitHousekeeping']) && $postData['submitHousekeeping'] == true){
            $newHistoryCommentAdded = $reservationService->newHistoryCommentFromGuestHousekeeping($reservation, $user, $postData['historyCommentText']);
        }

        if($newHistoryCommentAdded){
            $this->get('session')->getFlashBag()->add('success', 'Note added!');
        }else{
            $this->get('session')->getFlashBag()->add('error', 'Note NOT added.');
        }

        return $this->redirect($this->generateUrl('utt_user_reservation_manage', array(
            'reservationId' => $reservationId
        )));
    }

    public function voucherManageWithPinAction(Request $request){
        $voucherId = $request->request->get('voucherId');
        $voucherPin = $request->request->get('voucherPin');

        return $this->redirect($this->generateUrl('utt_user_voucher_manage_auth', array(
            'voucherId' => $voucherId,
            'pin' => $voucherPin
        )));
    }

    public function reservationManageWithPinAction(Request $request){
        $reservationId = $request->request->get('reservationId');
        $reservationPin = $request->request->get('reservationPin');

        return $this->redirect($this->generateUrl('utt_user_reservation_manage_auth', array(
            'reservationId' => $reservationId,
            'pin' => $reservationPin
        )));
    }

    public function voucherManageAction(Request $request, $voucherId, $pin = null){
        /** @var Voucher $voucher */
        $voucher = $this->getDoctrine()->getManager()->getRepository('UTTReservationBundle:Voucher')->find($voucherId);
        if(!$voucher) {
            $this->get('session')->getFlashBag()->add('error', 'Voucher "'.$voucherId.'" not found.');
            return $this->redirect($this->generateUrl('fos_user_profile_show'));
        }

        /** @var User $user */
        $user = $this->userAuth(false);
        if(!$user){
            if($voucher->getUser() && !is_null($pin) && $pin == $voucher->getPin()){
                $response = new RedirectResponse($this->generateUrl('utt_user_voucher_manage', array(
                    'voucherId' => $voucherId
                )));

                $this->authenticateUser($voucher->getUser(), $response);

                return $response;
            }else{
                $this->get('session')->getFlashBag()->add('error', 'Voucher PIN is not valid.');
                throw new AccessDeniedException('This user does not have access to this section.');
            }
        }

        if($voucher->getUser()->getId() == $user->getId()){
            $twigArray['voucher'] = $voucher;
            $twigArray['isWorldPay'] = true;

            if($request->cookies->has('worldpay')) {
                $twigArray['isWorldPay'] = true;
            }

            if($voucher->isAllowedToPay()){ $twigArray['isAllowedToPay'] = true; }

//            if($voucher->isAllowedResendConfirmationMail()){
//                $twigArray['isAllowedResendConfirmationMail'] = true;
//            }

            return $this->render('UTTUserBundle:User:voucherManage.html.twig', $twigArray);
        }

        return $this->redirect($this->generateUrl('utt_user_vouchers'));
    }

    public function vouchersAction(){
        /** @var User $user */
        $user = $this->userAuth();

        $vouchers = $this->getDoctrine()->getManager()->getRepository('UTTReservationBundle:Voucher')->findBy(array('user' => $user->getId()));

        $twigArray = array('vouchers' => $vouchers);

        return $this->render('UTTUserBundle:User:vouchers.html.twig', $twigArray);
    }

    public function reservationReinstateAction(Request $request, $reservationId, $pin){
        /** @var ReservationService $reservationService */
        $reservationService = $this->get('utt.reservationservice');

        /** @var Reservation $reservation */
        $reservation = $this->getDoctrine()->getManager()->getRepository('UTTReservationBundle:Reservation')->find($reservationId);
        if(!$reservation) return $this->redirect($this->generateUrl('utt_index_homepage'));

        if($reservation->getUser() && !is_null($pin) && $pin == $reservation->getPin()){
            $reinstated = $reservationService->reinstate($reservation, $reservation->getUser());
            if($reinstated){
                $reservationService->referredFlagChange($reservation, $reservation->getUser(), Reservation::REFERRED_FLAG_LUKASZ_RAK);
                $this->get('session')->getFlashBag()->add('success', 'Reservation reinstated!');
            }else{
                $this->get('session')->getFlashBag()->add('error', 'Reservation NOT reinstated.');
            }

            $response = new RedirectResponse($this->generateUrl('utt_user_reservation_manage', array(
                'reservationId' => $reservationId
            )));

            $this->authenticateUser($reservation->getUser(), $response);

            return $response;
        }

        return $this->redirect($this->generateUrl('utt_index_homepage'));
    }

    public function reservationManageAction(Request $request, $reservationId, $pin = null){
        /** @var Reservation $reservation */
        $reservation = $this->getDoctrine()->getManager()->getRepository('UTTReservationBundle:Reservation')->find($reservationId);
        if(!$reservation) {
            $this->get('session')->getFlashBag()->add('error', 'Reservation "'.$reservationId.'" not found.');
            return $this->redirect($this->generateUrl('fos_user_profile_show'));
        }

        /** @var User $user */
        $user = $this->userAuth(false);
        if(!$user){
            if($reservation->getUser() && !is_null($pin) && $pin == $reservation->getPin()){
                $response = new RedirectResponse($this->generateUrl('utt_user_reservation_manage', array(
                    'reservationId' => $reservationId
                )));

                $this->authenticateUser($reservation->getUser(), $response);

                return $response;
            }elseif($reservation->getUser() && !is_null($pin) && $pin == $reservation->getExternalPin()){
                if($reservation->isCompletedBooking()){
                    $this->get('session')->getFlashBag()->add('error', 'Call us to manage your booking.');
                }else{
                    /** @var EmailService $emailService */
                    $emailService = $this->get('utt.emailService');

                    if($emailService->sendReservationManageConfirmation($reservation)){
                        $this->get('session')->getFlashBag()->add('success', 'Reservation PIN is valid. Confirmation e-mail sent. You will find reservation manage link in a message.');
                    }else{
                        $this->get('session')->getFlashBag()->add('error', 'Reservation PIN is valid but error occurred. Try again later.');
                    }
                }

                throw new AccessDeniedException('This user does not have access to this section.');
            }else{
                $this->get('session')->getFlashBag()->add('error', 'Reservation PIN is not valid.');
                throw new AccessDeniedException('This user does not have access to this section.');
            }
        }

        if($reservation->getUser()->getId() == $user->getId()){
            $twigArray['reservation'] = $reservation;

            /** @var ReservationFactory $reservationFactory */
            $reservationFactory = $this->get('utt.reservationfactory');
            $isFullPaymentRequired = $reservationFactory->isFullPaymentRequired($reservation);
            $twigArray['isFullPaymentRequired'] = $isFullPaymentRequired;

            if($reservation->isAllowedToEdit()){
                $twigArray['isAllowedToEdit'] = true;
            }

            if($reservation->isAllowedToPay()){
                $twigArray['isAllowedToPay'] = true;
            }

            if($reservation->isAllowedCancel()){
                $twigArray['isAllowedCancel'] = true;
            }

            if($reservation->isAllowedResendConfirmationMail()){
                $twigArray['isAllowedResendConfirmationMail'] = true;
            }

            if($reservation->isAllowedResendArrivalInstructionMail()){
                $twigArray['isAllowedResendArrivalInstructionMail'] = true;
            }

            $nowDate = new \DateTime('now');
            if($reservation->getFromDate() < $nowDate && $reservation->getToDate() < $nowDate && $reservation->getStatus() == Reservation::STATUS_FULLY_PAID){
                /** @var ReviewRepository $reviewRepository */
                $reviewRepository = $this->getDoctrine()->getManager()->getRepository('UTTEstateBundle:Review');
                /** @var Review $userReview */
                $userReview = $reviewRepository->findOneBy(array(
                    'estate' => $reservation->getEstate()->getId(),
                    'user' => $user->getId()
                ));
                if($userReview instanceof Review){
                    $twigArray['userReview'] = $userReview;
                }else{
                    $reviewForm = $this->createForm(new ReviewType(), new Review());

                    if($request->isMethod('POST')){
                        $reviewForm->handleRequest($request);
                        if ($reviewForm->isValid()) {
                            /** @var Review $newReview */
                            $newReview = $reviewForm->getData();
                            $newReview->setEstate($reservation->getEstate());
                            $newReview->setUser($user);

                            $_em = $this->getDoctrine()->getManager();
                            $_em->persist($newReview);
                            $_em->flush();

                            /** @var EstateRepository $estateRepository */
                            $estateRepository = $this->getDoctrine()->getManager()->getRepository('UTTEstateBundle:Estate');
                            /** @var DiscountCodeService $discountCodeService */
                            $discountCodeService = $this->get('utt.discountcodeservice');
                            /** @var EmailService $emailService */
                            $emailService = $this->get('utt.emailService');

                            /** @var DiscountCode $discountCode */
                            $discountCode = $discountCodeService->createBookingReviewDiscount();
                            if($discountCode instanceof DiscountCode){
                                $this->get('session')->getFlashBag()->add('success', 'Thank you for the review. We have created special voucher for you. Further instructions in email.');

                                $emailSent = $emailService->sendDiscountCodeEmail($user->getEmail(), $discountCode);
                               
                                if($emailSent){
                                    /** @var HistoryEntryService $historyEntryService */
                                    $historyEntryService = $this->get('utt.historyentryservice');
                                    $historyEntryComment = $historyEntryService->newCommentDiscountAfterReview($discountCode->getCode(), $discountCode->getExpiresAt());
                                    $historyEntryService->newEntry(HistoryEntry::TYPE_DISCOUNT_CODE_IN_RETURN_OF_REVIEW, $reservation, $user, $historyEntryComment);
                                }
                            }

                            $this->get('session')->getFlashBag()->add('success', 'Your comment has been saved! Please wait for verification.');

                            /** @var OwnerReviewNotificationQueueService $ownerReviewNotificationQueueService */
                            $ownerReviewNotificationQueueService = $this->get('utt.ownerReviewNotificationQueueService');
                            $ownerReviewNotificationQueueService->pushToQueue($newReview);

                            return $this->redirect($this->generateUrl('utt_user_reservation_manage', array(
                                'reservationId' => $reservationId
                            )));
                        }
                    }

                    $twigArray['reviewForm'] = $reviewForm->createView();
                }
            }

            $twigArray['isWorldPay'] = true;

            if($request->cookies->has('worldpay')) {
                $twigArray['isWorldPay'] = true;
            }


            return $this->render('UTTUserBundle:User:reservationManage.html.twig', $twigArray);
        }

        return $this->redirect($this->generateUrl('utt_user_reservations'));
    }

    public function renderUserReservationsAction(User $user){
        $twigArray = array();

        /** @var ReservationRepository $reservationRepository */
        $reservationRepository = $this->getDoctrine()->getManager()->getRepository('UTTReservationBundle:Reservation');
        $reservations = $reservationRepository->findBy(array(
            'user' => $user->getId()
        ));
        if($reservations){
            $twigArray['reservations'] = $reservations;
        }

        return $this->render('UTTUserBundle:User:renderUserReservations.html.twig', $twigArray);
    }

    public function renderLatestReservationsAction(){
        /** @var User $user */
        $user = $this->userAuth(false);
        $twigArray = array();

        if($user){
            /** @var ReservationRepository $reservationRepository */
            $reservationRepository = $this->getDoctrine()->getManager()->getRepository('UTTReservationBundle:Reservation');
            $reservations = $reservationRepository->getLatestByUserId($user->getId(), 2);
            if($reservations){
                $twigArray['reservations'] = $reservations;
            }
        }

        return $this->render('UTTUserBundle:User:renderLatestReservations.html.twig', $twigArray);
    }

    public function renderLatestVouchersAction(){
        /** @var User $user */
        $user = $this->userAuth(false);
        $twigArray = array();

        if($user){
            /** @var VoucherRepository $voucherRepository */
            $voucherRepository = $this->getDoctrine()->getManager()->getRepository('UTTReservationBundle:Voucher');
            $vouchers = $voucherRepository->getLatestByUserId($user->getId(), 2);
            if($vouchers){
                $twigArray['vouchers'] = $vouchers;
            }
        }

        return $this->render('UTTUserBundle:User:renderLatestVouchers.html.twig', $twigArray);
    }

    public function reservationsAction(){
        /** @var User $user */
        $user = $this->userAuth();

        $reservations = $this->getDoctrine()->getManager()->getRepository('UTTReservationBundle:Reservation')->findBy(array(
            'user' => $user->getId()
        ));

        $twigArray = array(
            'reservations' => $reservations
        );

        return $this->render('UTTUserBundle:User:reservations.html.twig', $twigArray);
    }

    public function renderLoginInformationAction(){
        /** @var User $user */
        $user = $this->userAuth(false);

        return $this->render('UTTUserBundle:User:renderLoginInformation.html.twig', array(
            'user' => $user
        ));
    }

    public function indexAction()
    {
        return $this->render('UTTUserBundle:User:index.html.twig');
    }
}
