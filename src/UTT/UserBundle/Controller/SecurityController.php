<?php

namespace UTT\UserBundle\Controller;

use FOS\UserBundle\Controller\SecurityController as FOSSecurityController;

use UTT\ReservationBundle\Service\ReservationService;
use UTT\ReservationBundle\Model\ReservationData;

use UTT\ReservationBundle\Service\VoucherService;
use UTT\ReservationBundle\Model\VoucherData;

class SecurityController extends FOSSecurityController
{
    /**
     * Renders the login template with the given parameters. Overwrite this function in
     * an extended controller to provide additional data for the login template.
     *
     * @param array $data
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    protected function renderLogin(array $data){
        /** @var ReservationService $reservationService */
        $reservationService = $this->container->get('utt.reservationservice');
        /** @var ReservationData $reservationData */
        $reservationData = $reservationService->getReservationData();
        if($reservationData){
            $data['makeGuestBooking'] = true;
        }

        /** @var VoucherService $voucherService */
        $voucherService = $this->container->get('utt.voucherservice');
        /** @var VoucherData $voucherData */
        $voucherData = $voucherService->getVoucherData();
        if($voucherData){
            $data['makeGuestVoucher'] = true;
        }

        $template = sprintf('FOSUserBundle:Security:login.html.%s', $this->container->getParameter('fos_user.template.engine'));

        return $this->container->get('templating')->renderResponse($template, $data);
    }

}
