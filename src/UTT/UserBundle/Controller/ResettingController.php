<?php

namespace UTT\UserBundle\Controller;

use FOS\UserBundle\Controller\ResettingController as BaseController;
use FOS\UserBundle\Model\UserInterface;
use UTT\ReservationBundle\Service\ReservationService;
use UTT\ReservationBundle\Model\ReservationData;

class ResettingController extends BaseController
{
    protected function getRedirectionUrl(UserInterface $user)
    {
        /** @var ReservationService $reservationService */
        $reservationService = $this->container->get('utt.reservationservice');
        /** @var ReservationData $reservationData */
        $reservationData = $reservationService->getReservationData();
        if($reservationData){
            return $this->container->get('router')->generate('utt_reservation_confirm');
        }else{
            return $this->container->get('router')->generate('fos_user_profile_edit');
        }
    }
}