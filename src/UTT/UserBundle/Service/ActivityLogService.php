<?php

namespace UTT\UserBundle\Service;

use Doctrine\ORM\EntityManager;
use \Symfony\Component\Routing\Router;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Cookie;
use Symfony\Component\HttpFoundation\Response;

use UTT\UserBundle\Factory\ActivityLogFactory;
use UTT\UserBundle\Entity\ActivityLog;
use UTT\UserBundle\Entity\ActivityLogRepository;
use UTT\UserBundle\Entity\User;

use Symfony\Component\Security\Core\SecurityContext;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use FOS\UserBundle\Model\UserInterface;
use UTT\EstateBundle\Entity\Estate;

class ActivityLogService {
    protected $_em;
    protected $activityLogFactory;
    protected $router;
    protected $request;
    protected $securityContext;

    public function __construct(EntityManager $em, ActivityLogFactory $activityLogFactory, Router $router, Request $request, SecurityContext $securityContext){
        $this->_em = $em;
        $this->activityLogFactory = $activityLogFactory;
        $this->router = $router;
        $this->request = $request;
        $this->securityContext = $securityContext;
    }

    public function userAuth($throwException = true){
        if(!$this->securityContext->isGranted('IS_AUTHENTICATED_FULLY') ){
            if($throwException){
                throw new AccessDeniedException();
            }else{
                return false;
            }
        }

        $user = $this->securityContext->getToken()->getUser();
        if (!is_object($user) || !$user instanceof UserInterface) {
            if($throwException){
                throw new AccessDeniedException();
            }else{
                return false;
            }
        }

        return $user;
    }

    public function create($trackingCookieHash, $sessionId, $routeName, $routeParams, $queryParams, $refererUrl, User $user = null){
        /** @var ActivityLog $activityLog */
        $activityLog = $this->activityLogFactory->create($trackingCookieHash, $sessionId, $routeName, $routeParams, $queryParams, $refererUrl, $user);
        $this->save($activityLog);

        return $activityLog;
    }

    public function save(ActivityLog $activityLog){
        $this->_em->persist($activityLog);
        $this->_em->flush();

        return $activityLog;
    }

    public function detectReferer($activityLogs){
        $referer = '';

        /** @var ActivityLog $activityLog */
        foreach($activityLogs as $activityLog){
            if (strpos($activityLog->getRefererUrl(), 'http://www.underthethatch.co.uk') !== false) {

            }else{
                $referer = parse_url($activityLog->getRefererUrl(), PHP_URL_HOST);
            }
        }

        if($referer == ''){
            $referer = 'Direct';
        }

        return $referer;
    }

#############

    public function createForCurrentRoute(){
        $trackingCookieHash = $this->getTrackingCookieHash();
        $sessionId = $this->request->getSession()->getId();
        if(!$sessionId){
            $this->request->getSession()->start();
            $sessionId = $this->request->getSession()->getId();
        }
        $routeName = $this->request->attributes->get('_route');
        $routeParams = $this->request->attributes->get('_route_params');
        $referer = $this->request->headers->get('referer');
        $queryParams = $this->request->query->all();

        $user = $this->userAuth(false);
        if(!($user instanceof User)){ $user = null; }

        return $this->create($trackingCookieHash, $sessionId, $routeName, $routeParams, $queryParams, $referer, $user);
    }

    private function getTrackingCookieHash(){
        $trackingCookieHash = $this->request->cookies->get('trackingCookieHash');
        if($trackingCookieHash){
            return $trackingCookieHash;
        }else{
            $nowDate = new \DateTime('now');
            $trackingCookieHash = md5(uniqid().'trackingCookieHash'.$nowDate->getTimestamp());

            $cookie = new Cookie('trackingCookieHash', $trackingCookieHash, time() + 3600 * 24 * 365);

            $response = new Response();
            $response->headers->setCookie($cookie);
            $response->sendHeaders();

            return $trackingCookieHash;
        }
    }
}