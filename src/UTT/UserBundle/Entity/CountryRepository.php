<?php

namespace UTT\UserBundle\Entity;

use Doctrine\ORM\EntityRepository;

class CountryRepository extends EntityRepository
{
    public function findAllArray(){
        $query = $this->_em->createQuery("SELECT c FROM UTTUserBundle:Country c ORDER BY c.isFeatured DESC, c.id ASC");

        $result = $query->getArrayResult();
        if(count($result) > 0) return $result;

        return false;
    }
}
