<?php

namespace UTT\UserBundle\Entity;

use Doctrine\ORM\EntityRepository;
use UTT\EstateBundle\Entity\Estate;

class ActivityLogRepository extends EntityRepository
{
    public function findBySessionIdTrackingCookieHash($sessionId, $trackingCookieHash){
        $query = $this->_em->createQuery("SELECT al FROM UTTUserBundle:ActivityLog al WHERE al.sessionId = :sessionId AND al.trackingCookieHash = :trackingCookieHash ORDER BY al.createdAt ASC");
        $query->setParameter('sessionId', $sessionId);
        $query->setParameter('trackingCookieHash', $trackingCookieHash);

        $result = $query->getResult();
        if(count($result) > 0) return $result;

        return false;
    }

    public function findByTrackingCookieHashGroupedBySession($trackingCookieHash){
        $query = $this->_em->createQuery("SELECT al FROM UTTUserBundle:ActivityLog al WHERE al.trackingCookieHash = :trackingCookieHash GROUP BY al.sessionId ORDER BY al.createdAt ASC");
        $query->setParameter('trackingCookieHash', $trackingCookieHash);

        $result = $query->getResult();
        if(count($result) > 0) return $result;

        return false;
    }

    public function findGroupedForReportUserUpdate(){
        $query = $this->_em->createQuery("SELECT al FROM UTTUserBundle:ActivityLog al WHERE al.user IS NOT NULL AND al.trackingCookieHash IS NOT NULL GROUP BY al.trackingCookieHash ORDER BY al.createdAt ASC");

        $result = $query->getResult();
        if(count($result) > 0) return $result;

        return false;
    }

    public function findByEstateGroupedByHash(Estate $estate){
        $nowDate = new \DateTime('now');
        $nowDate->modify('-24 hours');

        $query = $this->_em->createQuery("
          SELECT al FROM UTTUserBundle:ActivityLog al
          WHERE
              al.createdAt >= :minDate AND
              al.routeName = 'utt_estate_show_estate' AND al.routeParams LIKE :routeParams AND
              al.trackingCookieHash IS NOT NULL
          GROUP BY al.trackingCookieHash
          ORDER BY al.createdAt ASC
        ");
        $query->setParameter('minDate', $nowDate);
        $query->setParameter('routeParams', '%'.$estate->getShortName().'%');

        $result = $query->getResult();
        if(count($result) > 0) return $result;

        return false;
    }

    public function findByReservationSessionIdTrackingCookieHash($sessionId, $trackingCookieHash){
        $query = $this->_em->createQuery("SELECT al FROM UTTUserBundle:ActivityLog al WHERE al.sessionId = :sessionId OR al.trackingCookieHash = :trackingCookieHash ORDER BY al.createdAt ASC");
        $query->setParameter('sessionId', $sessionId);
        $query->setParameter('trackingCookieHash', $trackingCookieHash);

        $result = $query->getResult();
        if(count($result) > 0) return $result;

        return false;
    }

    public function findTrackingCookieHashBySessionId($sessionId){
        $query = $this->_em->createQuery("SELECT al.trackingCookieHash FROM UTTUserBundle:ActivityLog al WHERE al.sessionId = :sessionId ORDER BY al.createdAt ASC");
        $query->setParameter('sessionId', $sessionId);
        $query->setMaxResults(1);

        $result = $query->getArrayResult();
        if(count($result) > 0) {
            if(isset($result[0]['trackingCookieHash'])) return $result[0]['trackingCookieHash'];
        }

        return false;
    }

    public function findNumberOfVisitsForEstate(Estate $estate){
        $query = $this->_em->createQuery("SELECT count(al) as visits FROM UTTUserBundle:ActivityLog al WHERE al.routeName = :routeName AND al.routeParams LIKE :estateShortName");
        $query->setParameter('routeName', 'utt_estate_show_estate');
        $query->setParameter('estateShortName', '%'.$estate->getShortName().'%');

        $result = $query->getArrayResult();
        if(isset($result[0]) && isset($result[0]['visits'])){
            return $result[0]['visits'];
        }

        return 0;
    }

    public function findNumberOfVisitsForEstateDate(Estate $estate, \Datetime $date){
        $query = $this->_em->createQuery("SELECT count(al) as visits FROM UTTUserBundle:ActivityLog al WHERE al.routeName = :routeName AND al.routeParams LIKE :estateShortName AND SUBSTRING(al.createdAt, 1, 10) LIKE :reportDateString");
        $query->setParameter('routeName', 'utt_estate_show_estate');
        $query->setParameter('estateShortName', '%'.$estate->getShortName().'%');
        $query->setParameter('reportDateString', $date->format('Y-m-d'));

        $result = $query->getArrayResult();
        if(isset($result[0]) && isset($result[0]['visits'])){
            return $result[0]['visits'];
        }

        return 0;
    }
}
