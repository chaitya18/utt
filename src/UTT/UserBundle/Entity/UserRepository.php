<?php

namespace UTT\UserBundle\Entity;

use Doctrine\ORM\EntityRepository;
use UTT\UserBundle\Entity\User;

class UserRepository extends EntityRepository
{
    public function getUsersWithType(){
        $query = $this->_em->createQuery("SELECT u FROM UTTUserBundle:User u WHERE
            u.type = :typeAdmin OR u.type = :typeACC OR u.type = :typeOwner OR u.type = :typeCleaner OR u.type = :typeNone
        ");
        $query->setParameters(array(
            'typeAdmin' => User::TYPE_ADMIN,
            'typeACC' => User::TYPE_ACC,
            'typeOwner' => User::TYPE_OWNER,
            'typeCleaner' => User::TYPE_CLEANER,
            'typeNone' => User::TYPE_NONE
        ));

        $result = $query->getResult();
        if(count($result) > 0) return $result;

        return false;
    }

    public function getOwners(){
        $query = $this->_em->createQuery("
            SELECT u 
            FROM UTTUserBundle:User u 
            WHERE u.type = :typeOwner
        ");
        $query->setParameters(array(
            'typeOwner' => User::TYPE_OWNER
        ));

        $result = $query->getResult();
        if(count($result) > 0) return $result;

        return false;
    }
}
