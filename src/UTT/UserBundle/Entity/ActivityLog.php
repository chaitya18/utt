<?php

namespace UTT\UserBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ActivityLog
 *
 * @ORM\Table(name="utt_activity_log")
 * @ORM\Entity(repositoryClass="UTT\UserBundle\Entity\ActivityLogRepository")
 */
class ActivityLog
{
    public function __construct(){
        $this->setCreatedAt(new \DateTime('now'));
    }

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(name="tracking_cookie_hash", type="string", length=255, nullable=true)
     */
    protected $trackingCookieHash;

    /**
     * @ORM\Column(name="session_id", type="string", length=255, nullable=true)
     */
    protected $sessionId;

    /**
     * @ORM\Column(name="route_name", type="string", length=255, nullable=true)
     */
    protected $routeName;

    /**
     * @ORM\Column(name="route_params", type="text")
     */
    protected $routeParams;

    /**
     * @ORM\Column(name="query_params", type="text")
     */
    protected $queryParams;

    /**
     * @ORM\Column(name="referer_url", type="text", nullable=true)
     */
    protected $refererUrl;

    /**
     * @ORM\Column(name="created_at", type="datetime")
     */
    protected $createdAt;

    /**
     * @ORM\ManyToOne(targetEntity="UTT\UserBundle\Entity\User")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id", nullable=true, onDelete="SET NULL")
     */
    protected $user;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set sessionId
     *
     * @param string $sessionId
     * @return ActivityLog
     */
    public function setSessionId($sessionId)
    {
        $this->sessionId = $sessionId;

        return $this;
    }

    /**
     * Get sessionId
     *
     * @return string 
     */
    public function getSessionId()
    {
        return $this->sessionId;
    }

    /**
     * Set routeName
     *
     * @param string $routeName
     * @return ActivityLog
     */
    public function setRouteName($routeName)
    {
        $this->routeName = $routeName;

        return $this;
    }

    /**
     * Get routeName
     *
     * @return string 
     */
    public function getRouteName()
    {
        return $this->routeName;
    }

    /**
     * Set routeParams
     *
     * @param string $routeParams
     * @return ActivityLog
     */
    public function setRouteParams($routeParams)
    {
        $this->routeParams = $routeParams;

        return $this;
    }

    /**
     * Get routeParams
     *
     * @return string 
     */
    public function getRouteParams()
    {
        return $this->routeParams;
    }

    /**
     * Get routeParams
     *
     * @return string
     */
    public function getRouteParamsDecoded()
    {
        return json_decode($this->routeParams);
    }

    /**
     * Set refererUrl
     *
     * @param string $refererUrl
     * @return ActivityLog
     */
    public function setRefererUrl($refererUrl)
    {
        $this->refererUrl = $refererUrl;

        return $this;
    }

    /**
     * Get refererUrl
     *
     * @return string 
     */
    public function getRefererUrl()
    {
        return $this->refererUrl;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return ActivityLog
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime 
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set queryParams
     *
     * @param string $queryParams
     * @return ActivityLog
     */
    public function setQueryParams($queryParams)
    {
        $this->queryParams = $queryParams;

        return $this;
    }

    /**
     * Get queryParams
     *
     * @return string 
     */
    public function getQueryParams()
    {
        return $this->queryParams;
    }

    /**
     * Get queryParams
     *
     * @return string
     */
    public function getQueryParamsDecoded()
    {
        return json_decode($this->queryParams);
    }

    /**
     * Set trackingCookieHash
     *
     * @param string $trackingCookieHash
     * @return ActivityLog
     */
    public function setTrackingCookieHash($trackingCookieHash)
    {
        $this->trackingCookieHash = $trackingCookieHash;

        return $this;
    }

    /**
     * Get trackingCookieHash
     *
     * @return string 
     */
    public function getTrackingCookieHash()
    {
        return $this->trackingCookieHash;
    }

    /**
     * Set user
     *
     * @param \UTT\UserBundle\Entity\User $user
     * @return ActivityLog
     */
    public function setUser(\UTT\UserBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \UTT\UserBundle\Entity\User 
     */
    public function getUser()
    {
        return $this->user;
    }
}
