<?php

namespace UTT\UserBundle\Form\Handler;

use FOS\UserBundle\Form\Handler\RegistrationFormHandler as BaseFormHandler;

class RegistrationFormHandler extends BaseFormHandler
{
    public function process($confirmation = false)
    {
        $user = $this->createUser();
        $user->setUsername('any');
        $this->form->setData($user);

        if ('POST' === $this->request->getMethod()) {
            $this->form->bind($this->request);

            if ($this->form->isValid()) {
                $user->setUsername($user->getEmail());
                $user->addRole($user::ROLE_DEFAULT);
                $this->onSuccess($user, $confirmation);

                return true;
            }
        }

        return false;
    }
}
