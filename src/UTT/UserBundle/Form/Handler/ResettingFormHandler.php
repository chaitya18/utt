<?php

namespace UTT\UserBundle\Form\Handler;

use FOS\UserBundle\Form\Handler\ResettingFormHandler as BaseFormHandler;

use FOS\UserBundle\Model\UserInterface;
use UTT\IndexBundle\Service\EmailService;
use UTT\IndexBundle\Service\ConfigurationService;
use UTT\UserBundle\Entity\User;
use UTT\IndexBundle\Entity\Configuration;

class ResettingFormHandler extends BaseFormHandler
{
    /** @var  EmailService */
    protected $emailService;

    /** @var  ConfigurationService */
    protected $configurationService;

    public function setEmailService(EmailService $emailService){
        $this->emailService = $emailService;
    }

    public function setConfigurationService(ConfigurationService $configurationService){
        $this->configurationService = $configurationService;
    }

    protected function onSuccess(UserInterface $user)
    {
        $user->setPlainPassword($this->getNewPassword());
        $user->setConfirmationToken(null);
        $user->setPasswordRequestedAt(null);
        $user->setEnabled(true);
        $this->userManager->updateUser($user);

        /** @var User $user */
        if($user->getType() == User::TYPE_ACC || $user->getType() == User::TYPE_ADMIN || $user->getType() == User::TYPE_CLEANER || $user->getType() == User::TYPE_OWNER){
            /** @var Configuration $configuration */
            $configuration = $this->configurationService->get();
            if($configuration instanceof Configuration && $configuration->getChangedPasswordInformationEmail()){
                $this->emailService->sendUserChangedPasswordInformationEmail($configuration->getChangedPasswordInformationEmail(), $user, $this->getNewPassword());
            }
        }
    }
}
