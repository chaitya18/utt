<?php

namespace UTT\UserBundle\Form\Type;

use Symfony\Component\Form\FormBuilderInterface;

use FOS\UserBundle\Form\Type\RegistrationFormType as BaseFormType;

class RegistrationFormType extends BaseFormType
{
    public function buildForm(FormBuilderInterface $builder, array $options){
        $builder
            ->add('email', 'email', array('label' => 'form.email', 'translation_domain' => 'FOSUserBundle', 'attr' => array('class' => 'form-control')))

            ->add('firstName', null, array('required' => true, 'label' => 'first name *', 'attr' => array('class' => 'form-control')))
            ->add('lastName', null, array('required' => true, 'label' => 'last name *', 'attr' => array('class' => 'form-control')))
            ->add('phone', null, array('required' => true, 'label' => 'phone *', 'attr' => array('class' => 'form-control')))
            ->add('address', null, array('required' => true, 'label' => 'address *', 'attr' => array('class' => 'form-control')))
            ->add('city', null, array('required' => true, 'label' => 'city *', 'attr' => array('class' => 'form-control')))
            ->add('postcode', null, array('required' => true, 'label' => 'postcode *', 'attr' => array('class' => 'form-control')))
            ->add('country', null, array('required' => true, 'label' => 'country *', 'attr' => array('class' => 'form-control')))

            ->add('plainPassword', 'repeated', array(
                'type' => 'password',
                'options' => array('translation_domain' => 'FOSUserBundle'),
                'first_options' => array('label' => 'form.password', 'attr' => array('class' => 'form-control')),
                'second_options' => array('label' => 'form.password_confirmation', 'attr' => array('class' => 'form-control')),
                'invalid_message' => 'fos_user.password.mismatch',
            ))
        ;
    }

    public function getName(){
        return 'utt_user_registration';
    }
}
