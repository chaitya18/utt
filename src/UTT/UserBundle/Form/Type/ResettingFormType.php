<?php

namespace UTT\UserBundle\Form\Type;

use Symfony\Component\Form\FormBuilderInterface;

use FOS\UserBundle\Form\Type\ResettingFormType as BaseFormType;

class ResettingFormType extends BaseFormType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('new', 'repeated', array(
            'type' => 'password',
            'options' => array('translation_domain' => 'FOSUserBundle'),
            'first_options' => array('label' => 'password', 'attr' => array('class' => 'form-control')),
            'second_options' => array('label' => 'confirm password', 'attr' => array('class' => 'form-control')),
            'invalid_message' => 'fos_user.password.mismatch',
        ));
    }

    public function getName()
    {
        return 'utt_user_resetting';
    }
}
