<?php

namespace UTT\UserBundle\Form\Type;

use Symfony\Component\Form\FormBuilderInterface;

use FOS\UserBundle\Form\Type\ProfileFormType as BaseFormType;

class ProfileFormType extends BaseFormType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $this->buildUserForm($builder, $options);
    }

    public function getName()
    {
        return 'utt_user_profile';
    }

    /**
     * Builds the embedded form representing the user.
     *
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    protected function buildUserForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('username', null, array('disabled' => true, 'label' => 'form.username', 'translation_domain' => 'FOSUserBundle', 'attr' => array(
                'class' => 'form-control'
            )))
            ->add('email', 'email', array('label' => 'form.email', 'translation_domain' => 'FOSUserBundle', 'attr' => array(
                'class' => 'form-control'
            )))
            ->add('firstName', null, array('required' => true, 'label' => 'First Name', 'attr' => array(
                'class' => 'form-control'
            )))
            ->add('lastName', null, array('required' => true, 'label' => 'Last Name', 'attr' => array(
                'class' => 'form-control'
            )))
            ->add('phone', null, array('required' => true, 'label' => 'Phone', 'attr' => array(
                'class' => 'form-control'
            )))
            ->add('address', null, array('required' => true, 'label' => 'Address', 'attr' => array(
                'class' => 'form-control'
            )))
            ->add('city', null, array('required' => true, 'label' => 'City', 'attr' => array(
                'class' => 'form-control'
            )))
            ->add('postcode', null, array('required' => true, 'label' => 'Postcode', 'attr' => array(
                'class' => 'form-control'
            )))
            ->add('country', null, array('required' => true, 'label' => 'Country', 'attr' => array(
                'class' => 'form-control'
            )))

        ;
    }
}
