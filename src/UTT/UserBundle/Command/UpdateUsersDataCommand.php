<?php

namespace UTT\UserBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

use Doctrine\ORM\EntityManager;

use UTT\UserBundle\Entity\User;
use UTT\UserBundle\Entity\UserRepository;

use UTT\ReservationBundle\Entity\Reservation;
use UTT\ReservationBundle\Entity\ReservationRepository;

use UTT\UserBundle\Entity\Country;
use UTT\UserBundle\Entity\CountryRepository;

class UpdateUsersDataCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('user:updateData')
            ->setDescription('')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        /** @var EntityManager $_em */
        $_em = $this->getContainer()->get('doctrine')->getManager();

        /** @var UserRepository $userRepository */
        $userRepository = $_em->getRepository('UTTUserBundle:User');
        $users = $userRepository->findBy(array(
            'type' => null
        ), array(
            'id' => 'desc'
        ), 3500);

        /** @var ReservationRepository $reservationRepository */
        $reservationRepository = $_em->getRepository('UTTReservationBundle:Reservation');

        /** @var CountryRepository $countryRepository */
        $countryRepository = $_em->getRepository('UTTUserBundle:Country');

        /** @var User $user */
        foreach($users as $user){
            if(!$user->getCountry() || !$user->getCity()){
                $reservations = $reservationRepository->findBy(array(
                    'user' => $user->getId()
                ));

                /** @var Reservation $reservation */
                foreach($reservations as $reservation){
                    $isCountryUpdated = false;
                    $isCityUpdated = false;

                    if(!$user->getCountry() && $reservation->getUserDataDecoded()->getCountry() && !$isCountryUpdated){
                        /** @var Country $country */
                        $country = $countryRepository->findOneBy(array('code' => $reservation->getUserDataDecoded()->getCountry()));
                        if($country instanceof Country){
                            $user->setCountry($country);
                            $_em->flush();

                            echo $user->getId().' updated with country '.$country->getName().PHP_EOL;
                            $isCountryUpdated = true;
                        }
                    }

                    if(!$user->getCity() && $reservation->getUserDataDecoded()->getCity() && !$isCityUpdated){
                        $cityName = trim(ucfirst($reservation->getUserDataDecoded()->getCity()));

                        $user->setCity($cityName);
                        $_em->flush();

                        echo $user->getId().' updated with city '.$cityName.PHP_EOL;
                        $isCityUpdated = true;
                    }

                    if($isCountryUpdated && $isCityUpdated){
                        break;
                    }
                }
            }
        }
    }
}