<?php

namespace UTT\UserBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

use UTT\UserBundle\Entity\User;
use UTT\UserBundle\Entity\UserRepository;
use FOS\UserBundle\Model\UserManagerInterface;

class ChangeUsersPasswordsCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('user:changePasswords')
            ->setDescription('Change random password for all imported users')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        /** @var UserManagerInterface $userManager */
        $userManager = $this->getContainer()->get('fos_user.user_manager');

        /** @var UserRepository $userRepository */
        $userRepository = $this->getContainer()->get('doctrine')->getManager()->getRepository('UTTUserBundle:User');
        $users = $userRepository->findBy(array(
            'type' => null,
            'lastLogin' => null
        ));
        /** @var User $user */
        foreach($users as $user){
            $plainPassword = substr(md5($user->getEmail().'raczekUTT'), 0, 12);
            $user->setPlainPassword($plainPassword);
            $user->setType(User::TYPE_NONE);
            $userManager->updateUser($user);
        }
    }
}