<?php

namespace UTT\UserBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

use UTT\UserBundle\Entity\User;
use UTT\UserBundle\Entity\UserRepository;
use FOS\UserBundle\Model\UserManagerInterface;
use UTT\ReservationBundle\Entity\ReservationRepository;

class UsersWithTwoBookingsCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('user:usersWithTwoBookings')
            ->setDescription('')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        /** @var UserRepository $userRepository */
        $userRepository = $this->getContainer()->get('doctrine')->getManager()->getRepository('UTTUserBundle:User');
        /** @var ReservationRepository $reservationRepository */
        $reservationRepository = $this->getContainer()->get('doctrine')->getManager()->getRepository('UTTReservationBundle:Reservation');
        $users = $userRepository->findAll();

        $number = 0;
        /** User $user */
        foreach($users as $user){
            $reservations = $reservationRepository->findBy(array('user' => $user->getId()));
            if($reservations && count($reservations)  > 1){
                $number++;
                echo $number.PHP_EOL;
            }
        }
    }
}