<?php

namespace UTT\UserBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

use UTT\UserBundle\Entity\User;
use UTT\UserBundle\Entity\UserRepository;

class ExportEmailsCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('user:exportEmails')
            ->setDescription('Export emails for mailchimp')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        /** @var UserRepository $userRepository */
        $userRepository = $this->getContainer()->get('doctrine')->getManager()->getRepository('UTTUserBundle:User');
        $users = $userRepository->findBy(array(
            'type' => null
        ));

        $handle = fopen('emailsList.csv', 'w+');
        fputcsv($handle, array('Email Address', 'First Name', 'Last Name'),',');

        /** @var User $user */
        foreach($users as $user){
            fputcsv($handle, array($user->getEmail(), $user->getFirstName(), $user->getLastName()),',');
        }

        fclose($handle);
    }
}