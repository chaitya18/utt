<?php

namespace UTT\UserBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class UTTUserBundle extends Bundle
{
    public function getParent(){
        return 'FOSUserBundle';
    }
}
