<?php

namespace UTT\UserBundle\Factory;

use UTT\UserBundle\Entity\ActivityLog;
use UTT\UserBundle\Entity\User;

use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\Encoder\XmlEncoder;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\GetSetMethodNormalizer;

class ActivityLogFactory {

    public function create($trackingCookieHash, $sessionId, $routeName, $routeParams, $queryParams, $refererUrl, User $user = null){
        $encoders = array(new XmlEncoder(), new JsonEncoder());
        $normalizers = array(new GetSetMethodNormalizer());
        $serializer = new Serializer($normalizers, $encoders);

        $routeParams = $serializer->serialize($routeParams, 'json');
        $queryParams = $serializer->serialize($queryParams, 'json');

        $activityLog = new ActivityLog();
        $activityLog->setTrackingCookieHash($trackingCookieHash);
        $activityLog->setSessionId($sessionId);
        $activityLog->setRouteName($routeName);
        $activityLog->setRouteParams($routeParams);
        $activityLog->setQueryParams($queryParams);
        $activityLog->setRefererUrl($refererUrl);
        $activityLog->setUser($user);

        return $activityLog;
    }

}