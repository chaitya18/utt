<?php

namespace UTT\UserBundle\Factory;

use Doctrine\ORM\EntityManager;
use FOS\UserBundle\Model\UserManagerInterface;
use FOS\UserBundle\Mailer\MailerInterface;
use FOS\UserBundle\Util\TokenGeneratorInterface;

use UTT\UserBundle\Entity\User;

class UserFactory {
    protected $_em;
    protected $userManager;
    protected $mailer;
    protected $tokenGenerator;

    public function __construct(EntityManager $em, UserManagerInterface $userManager, MailerInterface $mailer, TokenGeneratorInterface $tokenGenerator){
        $this->_em = $em;
        $this->userManager = $userManager;
        $this->mailer = $mailer;
        $this->tokenGenerator = $tokenGenerator;
    }

    public function generateTemporaryPassword($email){
        return substr(md5($email.'utt temporary password'),0, 12);
    }

    public function createImported($email, $name, $phone, $mobile, $address1, $address2, $city, $postcode, $country){
        /** @var User $user */
        $user = $this->userManager->createUser();
        $user->addRole($user::ROLE_DEFAULT);
        $user->addRole($user::ROLE_IMPORTED);

        $user->setUsername($email);
        $user->setEmail($email);
        $user->setFirstName($name);
        $user->setLastName('');
        $user->setPlainPassword($email);

        $user->setEnabled(false);

        $user->setPhone($phone.', '.$mobile);
        $user->setAddress($address1.', '.$address2);
        $user->setCity($city);
        $user->setPostcode($postcode);

        # TODO ustawianie country
        #$user->setCountry();

        $this->userManager->updateUser($user);

        return $user;
    }

    public function create($email, $firstName, $lastName, $password){
        /** @var User $user */
        $user = $this->userManager->createUser();
        $user->addRole($user::ROLE_DEFAULT);

        $user->setUsername($email);
        $user->setEmail($email);

        $user->setFirstName($firstName);
        $user->setLastName($lastName);
        $user->setPlainPassword($password);

        $user->setEnabled(true);
        if (null === $user->getConfirmationToken()) {
            $user->setConfirmationToken($this->tokenGenerator->generateToken());
        }

        $this->mailer->sendConfirmationEmailMessage($user);

        $this->userManager->updateUser($user);

        return $user;
    }
}