var reservationManageApp = angular.module('reservationManageApp', []);

reservationManageApp.factory('priceFactory', function() {
    var basePrice = 0;
    var additionalSleepsPrice = 0;
    var adminCharge = 0;
    var creditCardCharge = 0;
    var bookingProtectCharge = 0;
    var charityCharge = 0;
    var petsPrice = 0;

    var setBasePrice = function(price){ basePrice = price; };
    var getBasePrice = function(){ return basePrice; };
    var setAdditionalSleepsPrice = function(price) { additionalSleepsPrice = price; };
    var getAdditionalSleepsPrice = function(){ return additionalSleepsPrice; };
    var setAdminCharge = function(price) { adminCharge = price; };
    var getAdminCharge = function(){ return adminCharge; };
    var setCreditCardCharge = function(price) { creditCardCharge = price; };
    var getCreditCardCharge = function(){ return creditCardCharge; };
    var setBookingProtectCharge = function(price) { bookingProtectCharge = price; };
    var getBookingProtectCharge = function(){ return bookingProtectCharge; };
    var setCharityCharge = function(price) { charityCharge = price; };
    var getCharityCharge = function(){ return charityCharge; };
    var setPetsPrice = function(price){ petsPrice = price; };
    var getPetsPrice = function(){ return petsPrice; };

    return {
        setBasePrice: setBasePrice,
        getBasePrice: getBasePrice,
        setAdditionalSleepsPrice: setAdditionalSleepsPrice,
        getAdditionalSleepsPrice: getAdditionalSleepsPrice,
        setAdminCharge: setAdminCharge,
        getAdminCharge: getAdminCharge,
        setCreditCardCharge: setCreditCardCharge,
        getCreditCardCharge: getCreditCardCharge,
        setBookingProtectCharge: setBookingProtectCharge,
        getBookingProtectCharge: getBookingProtectCharge,
        setCharityCharge: setCharityCharge,
        getCharityCharge: getCharityCharge,
        setPetsPrice: setPetsPrice,
        getPetsPrice: getPetsPrice,

        getPrice: function(){
            return {
                basePrice: getBasePrice(),
                additionalSleepsPrice: getAdditionalSleepsPrice(),
                adminCharge: getAdminCharge(),
                creditCardCharge: getCreditCardCharge(),
                bookingProtectCharge: getBookingProtectCharge(),
                charityCharge: getCharityCharge(),
                petsPrice: getPetsPrice(),
                total: function(){
                    return parseFloat(getBasePrice()) + parseFloat(getAdditionalSleepsPrice()) + parseFloat(getAdminCharge()) + parseFloat(getCreditCardCharge()) + parseFloat(getBookingProtectCharge()) + parseFloat(getCharityCharge()) + parseFloat(getPetsPrice());
                }
            }
        }
    }
});

reservationManageApp.factory('filterFactory', function() {
    var maxSleeps = false;
    var maxPets = false;
    var maxInfants = false;

    var sleepsAdultsChoices = [];
    var sleepsChildrenChoices = [];
    var petsChoices = [];
    var infantsChoices = [];
    var getSleepsAdultsChoices = function(){ return sleepsAdultsChoices; };
    var getSleepsChildrenChoices = function(){ return sleepsChildrenChoices; };
    var getPetsChoices = function(){ return petsChoices; };
    var getInfantsChoices = function(){ return infantsChoices; };

    var sleepsAdults = false;
    var sleepsChildren = false;
    var pets = false;
    var infants = false;
    var fromDate = null;
    var toDate = null;
    var setSleepsAdults = function(sleepsAdultsItem){ sleepsAdults = sleepsAdultsItem; };
    var getSleepsAdults = function(){ return sleepsAdults; };
    var setSleepsChildren = function(sleepsChildrenItem){ sleepsChildren = sleepsChildrenItem; };
    var getSleepsChildren = function(){ return sleepsChildren; };
    var setPets = function(petsItem){ pets = petsItem; };
    var getPets = function() { return pets; };
    var setInfants = function(infantsItem){ infants = infantsItem; };
    var getInfants = function() { return infants; };
    var setFromDate = function(date) { fromDate = date; };
    var getFromDate = function() { return fromDate; };
    var setToDate = function(date) { toDate = date; };
    var getToDate = function() { return toDate; };
    var getMaxSleeps = function(){ return maxSleeps; };

    var initMaxSleeps = function(maxSleepsItem){
        maxSleeps = maxSleepsItem;
    };

    var initSleepsAdults = function(maxSleepsAdults){
        sleepsAdultsChoices = [];
        for(var i=0; i<=maxSleepsAdults; i++){
            sleepsAdultsChoices.push({ 'id': i });
        }
    };

    var initSleepsChildren = function(maxSleepsChildren){
        sleepsChildrenChoices = [];
        for(var i=0; i<=maxSleepsChildren; i++){
            sleepsChildrenChoices.push({ 'id': i });
        }
    };

    var initPets = function(pets){
        maxPets = pets;

        petsChoices = [];
        for(var i=0; i<=maxPets; i++){
            petsChoices.push({ 'id': i });
        }
    };

    var initInfants = function(infants){
        maxInfants = infants;

        infantsChoices = [];
        for(var i=0; i<=maxInfants; i++){
            infantsChoices.push({ 'id': i });
        }
    };
    return {
        setSleepsAdults: setSleepsAdults,
        getSleepsAdults: getSleepsAdults,
        setSleepsChildren: setSleepsChildren,
        getSleepsChildren: getSleepsChildren,
        getPets: getPets,
        setPets: setPets,
        getInfants: getInfants,
        setInfants: setInfants,
        setFromDate: setFromDate,
        getFromDate: getFromDate,
        setToDate: setToDate,
        getToDate: getToDate,
        getMaxSleeps: getMaxSleeps,

        getSleepsTotal: function(){
            return parseInt(getSleepsAdults().id) + parseInt(getSleepsChildren().id);
        },
        getSleepsAdultsTotal: function(){
            return getSleepsAdults().id;
        },
        getSleepsChildrenTotal: function(){
            return getSleepsChildren().id;
        },
        getPetsTotal: function(){
            return getPets().id;
        },
        getInfantsTotal: function(){
            return getInfants().id;
        },

        getSleepsAdultsChoices: getSleepsAdultsChoices,
        getSleepsChildrenChoices: getSleepsChildrenChoices,
        getPetsChoices: getPetsChoices,
        getInfantsChoices: getInfantsChoices,
        initMaxSleeps: initMaxSleeps,
        initSleepsAdults: initSleepsAdults,
        initSleepsChildren: initSleepsChildren,
        initPets: initPets,
        initInfants: initInfants
    }
});

reservationManageApp.factory('apiService', function($http) {
    $http.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';
    $http.defaults.headers.post["Content-Type"] = "application/x-www-form-urlencoded";

    return {
        getPrice: function(params, callback){
            $http({
                method: 'POST',
                url: Routing.generate('utt_reservation_manage_price'),
                data: $.param(params),
                headers: {'Content-Type':'application/x-www-form-urlencoded; charset=UTF-8'}
            })
                .success(function(data, status, headers, config) {
                    var dataOb = angular.fromJson(data);
                    if(!(typeof dataOb.success === typeof undefined) && dataOb.success == true && !(typeof dataOb.newReservationPrice === typeof undefined)){
                        callback(dataOb.newReservationPrice);
                    }else{
                        callback(false);
                    }
                })
                .error(function(data, status, headers, config) {
                    callback(false);
                });
        },
        changeReservation: function(params, callback){
            $http({
                method: 'POST',
                url: Routing.generate('utt_reservation_change_create'),
                data: $.param(params),
                headers: {'Content-Type':'application/x-www-form-urlencoded; charset=UTF-8'}
            })
                .success(function(data, status, headers, config) {
                    var dataOb = angular.fromJson(data);
                    if(!(typeof dataOb.success === typeof undefined) && dataOb.success == true){
                        callback(true);
                    }else{
                        callback(false);
                    }
                })
                .error(function(data, status, headers, config) {
                    callback(false);
                });
        },
        getCountries: function(callback){
            $http({
                method: 'POST',
                url: Routing.generate('utt_api_all_countries'),
                headers: {'Content-Type':'application/x-www-form-urlencoded; charset=UTF-8'}
            })
                .success(function(data, status, headers, config) {
                    var dataOb = angular.fromJson(data);
                    if(!(typeof dataOb.success === typeof undefined) && dataOb.success == true && !(typeof dataOb.countries === typeof undefined)){
                        callback(dataOb.countries);
                    }else{
                        callback(false);
                    }
                })
                .error(function(data, status, headers, config) {
                    callback(false);
                });
        }
    }
});

reservationManageApp.factory('userDataFactory', function($http) {
    var countryChoices = [];
    var setCountryChoices = function(countries){ countryChoices = countries; };
    var getCountryChoices = function(){ return countryChoices; };

    var userData = {
        firstName: '',
        lastName: '',
        email: '',
        phone: '',
        address: '',
        city: '',
        postcode: '',
        country: ''
    };

    var setUserFirstName = function(data) { userData.firstName = data; };
    var getUserFirstName = function() { return userData.firstName; };

    var setUserLastName = function(data) { userData.lastName = data; };
    var getUserLastName = function() { return userData.lastName; };

    var setUserEmail = function(data) { userData.email = data; };
    var getUserEmail = function() { return userData.email; };

    var setUserPhone = function(data) { userData.phone = data; };
    var getUserPhone = function() { return userData.phone; };

    var setUserAddress = function(data) { userData.address = data; };
    var getUserAddress = function() { return userData.address; };

    var setUserCity = function(data) { userData.city = data; };
    var getUserCity = function() { return userData.city; };

    var setUserPostcode = function(data) { userData.postcode = data; };
    var getUserPostcode = function() { return userData.postcode; };

    var setUserCountry = function(data) { userData.country = data; };
    var getUserCountry = function() { return userData.country; };

    return {
        setCountryChoices: setCountryChoices,
        getCountryChoices: getCountryChoices,
        setUserFirstName: setUserFirstName,
        getUserFirstName: getUserFirstName,
        setUserLastName: setUserLastName,
        getUserLastName: getUserLastName,
        setUserEmail: setUserEmail,
        getUserEmail: getUserEmail,
        setUserPhone: setUserPhone,
        getUserPhone: getUserPhone,
        setUserAddress: setUserAddress,
        getUserAddress: getUserAddress,
        setUserCity: setUserCity,
        getUserCity: getUserCity,
        setUserPostcode: setUserPostcode,
        getUserPostcode: getUserPostcode,
        setUserCountry: setUserCountry,
        getUserCountry: getUserCountry,

        setData: function(data){
            if(typeof data.firstName === typeof undefined){}else{
                setUserFirstName(data.firstName);
            }
            if(typeof data.lastName === typeof undefined){}else{
                setUserLastName(data.lastName);
            }
            if(typeof data.email === typeof undefined){}else{
                setUserEmail(data.email);
            }
            if(typeof data.phone === typeof undefined){}else{
                setUserPhone(data.phone);
            }
            if(typeof data.address === typeof undefined){}else{
                setUserAddress(data.address);
            }
            if(typeof data.city === typeof undefined){}else{
                setUserCity(data.city);
            }
            if(typeof data.postcode === typeof undefined){}else{
                setUserPostcode(data.postcode);
            }
            if(typeof data.country === typeof undefined){}else{
                setUserCountry(data.country);
            }
        },
        getData: function(){
            return userData;
        }
    }
});

reservationManageApp.factory('reservationDataFactory', function() {
    var userData = null;
    var price = null;
    var sleepsAdults = null;
    var sleepsChildren = null;
    var pets = null;
    var infants = null;
    var fromDate = null;
    var toDate = null;

    var setUserData = function(data){ userData = data; };
    var getUserData = function(){ return userData; };
    var setPrice = function(data) { price = data; };
    var getPrice = function(){ return price; };
    var setSleepsAdults = function(data) { sleepsAdults = data; };
    var getSleepsAdults = function(){ return sleepsAdults; };
    var setSleepsChildren = function(data) { sleepsChildren = data; };
    var getSleepsChildren = function(){ return sleepsChildren; };
    var setPets = function(data) { pets = data; };
    var getPets = function() { return pets; };
    var setInfants = function(data) { infants = data; };
    var getInfants = function() { return infants; };
    var setFromDate = function(data) { fromDate = data; };
    var getFromDate = function() { return fromDate; };
    var setToDate = function(data) { toDate = data; };
    var getToDate = function() { return toDate; };

    return {
        setUserData: setUserData,
        getUserData: getUserData,
        setPrice: setPrice,
        getPrice: getPrice,
        setSleepsAdults: setSleepsAdults,
        getSleepsAdults: getSleepsAdults,
        setSleepsChildren: setSleepsChildren,
        getSleepsChildren: getSleepsChildren,
        setPets: setPets,
        getPets: getPets,
        setInfants: setInfants,
        getInfants: getInfants,
        setFromDate: setFromDate,
        getFromDate: getFromDate,
        setToDate: setToDate,
        getToDate: getToDate
    }
});

reservationManageApp.controller('reservationManageAppCtrl', ['$scope', '$window', 'priceFactory', 'filterFactory', 'apiService', 'userDataFactory', 'reservationDataFactory', function($scope, $window, priceFactory, filterFactory, apiService, userDataFactory, reservationDataFactory){
    $scope.reservationDataFactory = reservationDataFactory;

    $scope.initUserData = function(json){
        var userData = angular.fromJson(json);
        userDataFactory.setData(userData);

        reservationDataFactory.setUserData(angular.copy(userDataFactory.getData()));

        userDataApply();
    };

    $scope.initCountryChoices = function(){
        apiService.getCountries(function(countries){
            var countryChoices = countries;
            userDataFactory.setCountryChoices(countryChoices);

            $scope.countryChoices = countryChoices;
            if(userDataFactory.getUserCountry()){
                angular.forEach(countryChoices, function(country){
                    if(country.code == userDataFactory.getUserCountry()){
                        var index = countryChoices.indexOf(country);
                        $scope.userDataCountry = countryChoices[index];
                    }
                });
            }else{
                $scope.userDataCountry = countryChoices[0];
            }
        });
    };

    $scope.initNewReservationPrice = function(json){
        var newReservationPrice = angular.fromJson(json);
        if(typeof newReservationPrice.basePrice === typeof undefined){}else{
            priceFactory.setBasePrice(newReservationPrice.basePrice);
        }
        if(typeof newReservationPrice.additionalSleepsPrice === typeof undefined){}else{
            priceFactory.setAdditionalSleepsPrice(newReservationPrice.additionalSleepsPrice);
        }
        if(typeof newReservationPrice.adminCharge === typeof undefined){}else{
            priceFactory.setAdminCharge(newReservationPrice.adminCharge);
        }
        if(typeof newReservationPrice.creditCardCharge === typeof undefined){}else{
            priceFactory.setCreditCardCharge(newReservationPrice.creditCardCharge);
        }
        if(typeof newReservationPrice.bookingProtectCharge === typeof undefined){}else{
            priceFactory.setBookingProtectCharge(newReservationPrice.bookingProtectCharge);
        }
        if(typeof newReservationPrice.charityCharge === typeof undefined){}else{
            priceFactory.setCharityCharge(newReservationPrice.charityCharge);
        }
        if(typeof newReservationPrice.petsPrice === typeof undefined){}else{
            priceFactory.setPetsPrice(newReservationPrice.petsPrice);
        }

        reservationDataFactory.setPrice(angular.copy(priceFactory.getPrice()));
    };
    $scope.totalPrice = function(){
        return priceFactory.getPrice().total();
    };
    $scope.basePrice = function(){
        return priceFactory.getPrice().basePrice;
    };
    $scope.additionalSleepsPrice = function(){
        var price = priceFactory.getPrice().additionalSleepsPrice;
        if(price){ return price; }

        return false;
    };
    $scope.adminCharge = function(){
        var price = priceFactory.getPrice().adminCharge;
        if(price){ return price; }

        return false;
    };
    $scope.creditCardCharge = function(){
        var price = priceFactory.getPrice().creditCardCharge;
        if(price){ return price; }

        return false;
    };
    $scope.bookingProtectCharge = function(){
        var price = priceFactory.getPrice().bookingProtectCharge;
        if(price){ return price; }

        return false;
    };
    $scope.charityCharge = function(){
        var price = priceFactory.getPrice().charityCharge;
        if(price){ return price; }

        return false;
    };
    $scope.petsPrice = function(){
        var price = priceFactory.getPrice().petsPrice;
        if(price){ return price; }

        return false;
    };

    /*     ####     */

    $scope.initFilterSleeps = function(maxSleeps, reservationSleeps, reservationSleepsChildren){
        var reservationSleepsAdults = reservationSleeps-reservationSleepsChildren;

        filterFactory.initMaxSleeps(maxSleeps);
        $scope.initFilterSleepsAdults(reservationSleepsAdults, maxSleeps);
        $scope.initFilterSleepsChildren(reservationSleepsChildren, maxSleeps);
    };

    $scope.initFilterSleepsAdults = function(adults, maxAdults){
        if(adults > maxAdults){ adults = 0; }
        filterFactory.initSleepsAdults(maxAdults);

        var sleepsAdultsChoices = filterFactory.getSleepsAdultsChoices();
        $scope.sleepsAdultsChoices = sleepsAdultsChoices;
        angular.forEach(sleepsAdultsChoices, function(choice){
            if(choice.id == adults){
                filterFactory.setSleepsAdults(choice);
            }
        });

        $scope.sleepsAdults = filterFactory.getSleepsAdults();
        reservationDataFactory.setSleepsAdults(angular.copy(filterFactory.getSleepsAdults()));
    };

    $scope.initFilterSleepsChildren = function(children, maxChildren){
        if(children > maxChildren){ children = 0; }
        filterFactory.initSleepsChildren(maxChildren);

        var sleepsChildrenChoices = filterFactory.getSleepsChildrenChoices();
        $scope.sleepsChildrenChoices = sleepsChildrenChoices;
        angular.forEach(sleepsChildrenChoices, function(choice){
            if(choice.id == children){
                filterFactory.setSleepsChildren(choice);
            }
        });

        $scope.sleepsChildren = filterFactory.getSleepsChildren();
        reservationDataFactory.setSleepsChildren(angular.copy(filterFactory.getSleepsChildren()));
    };

    $scope.initFilterPets = function(maxPets, reservationPets){
        filterFactory.initPets(maxPets);

        var petsChoices = filterFactory.getPetsChoices();
        $scope.petsChoices = petsChoices;
        angular.forEach(petsChoices, function(choice){
            if(choice.id == reservationPets){
                filterFactory.setPets(choice);
            }
        });

        $scope.pets = filterFactory.getPets();
        reservationDataFactory.setPets(angular.copy(filterFactory.getPets()));
    };

    $scope.initFilterInfants = function(maxInfants, reservationInfants){
        filterFactory.initInfants(maxInfants);

        var infantsChoices = filterFactory.getInfantsChoices();
        $scope.infantsChoices = infantsChoices;
        angular.forEach(infantsChoices, function(choice){
            if(choice.id == reservationInfants){
                filterFactory.setInfants(choice);
            }
        });

        $scope.infants = filterFactory.getInfants();
        reservationDataFactory.setInfants(angular.copy(filterFactory.getInfants()));
    };

    $scope.initFilterFromDate = function(fromDate){
        filterFactory.setFromDate(fromDate);

        $scope.fromDate = filterFactory.getFromDate();
        reservationDataFactory.setFromDate(angular.copy(filterFactory.getFromDate()));
    };

    $scope.initFilterToDate = function(toDate){
        filterFactory.setToDate(toDate);

        $scope.toDate = filterFactory.getToDate();
        reservationDataFactory.setToDate(angular.copy(filterFactory.getToDate()));
    };

    $scope.sleepsAdultsChoices = [];
    $scope.sleepsChildrenChoices = [];
    $scope.petsChoices = [];
    $scope.infantsChoices = [];

    $scope.sleepsAdults = false;
    $scope.sleepsChildren = false;
    $scope.pets = false;
    $scope.infants = false;
    $scope.fromDate = null;
    $scope.toDate = null;

    $scope.setSleepsAdults = function(sleepsAdults){
        filterFactory.setSleepsAdults(sleepsAdults);

        var maxChildren = filterFactory.getMaxSleeps() - filterFactory.getSleepsAdultsTotal();
        var children = filterFactory.getSleepsChildrenTotal();

        $scope.initFilterSleepsChildren(children, maxChildren);
        formApply();
    };

    $scope.setSleepsChildren = function(sleepsChildren){
        filterFactory.setSleepsChildren(sleepsChildren);

        var maxAdults = filterFactory.getMaxSleeps() - filterFactory.getSleepsChildrenTotal();
        var adults = filterFactory.getSleepsAdultsTotal();

        $scope.initFilterSleepsAdults(adults, maxAdults);
        formApply();
    };

    $scope.setPets = function(pets){
        filterFactory.setPets(pets);
        formApply();
    };

    $scope.setInfants = function(infants){
        filterFactory.setInfants(infants);
        formApply();
    };

    $scope.setFromDate = function(fromDate){
        filterFactory.setFromDate(fromDate);
        formApply();
    };

    $scope.setToDate = function(toDate){
        filterFactory.setToDate(toDate);
        formApply();
    };


    /*
     User change methods
     */
    $scope.changeUserFirstName = function(firstName){
        userDataFactory.setUserFirstName(firstName);
        userDataApply();
    };
    $scope.changeUserLastName = function(lastName){
        userDataFactory.setUserLastName(lastName);
        userDataApply();
    };
    $scope.changeUserEmail = function(email){
        userDataFactory.setUserEmail(email);
        userDataApply();
    };
    $scope.changeUserPhone = function(phone){
        userDataFactory.setUserPhone(phone);
        userDataApply();
    };
    $scope.changeUserAddress = function(address){
        userDataFactory.setUserAddress(address);
        userDataApply();
    };
    $scope.changeUserCity = function(city){
        userDataFactory.setUserCity(city);
        userDataApply();
    };
    $scope.changeUserPostcode = function(postcode){
        userDataFactory.setUserPostcode(postcode);
        userDataApply();
    };
    $scope.countryChoices = [];
    $scope.userDataCountry = false;
    $scope.changeUserCountry = function(country){
        $scope.userDataCountry = country;
        userDataFactory.setUserCountry(country.code);

        userDataApply();
    };

    var userDataApply = function(){
        $scope.userData = userDataFactory.getData();
    };

    $scope.isPriceValid = null;
    var formApply = function(){
        $scope.isPriceValid = null;
        $scope.sleepsAdults = filterFactory.getSleepsAdults();
        $scope.sleepsChildren = filterFactory.getSleepsChildren();
        $scope.pets = filterFactory.getPets();
        $scope.infants = filterFactory.getInfants();
        $scope.fromDate = filterFactory.getFromDate();
        $scope.toDate = filterFactory.getToDate();

        var httpData = { reservationId: $scope.reservationId };

        if($scope.editModes.filterSleeps == true){ httpData.sleeps = filterFactory.getSleepsTotal(); }
        if($scope.editModes.filterPets == true){ httpData.pets = filterFactory.getPetsTotal(); }
        if($scope.editModes.filterInfants == true){ httpData.infants = filterFactory.getInfantsTotal(); }
        if($scope.editModes.filterFromDate == true){ httpData.fromDate = filterFactory.getFromDate(); }
        if($scope.editModes.filterToDate == true){ httpData.toDate = filterFactory.getToDate(); }

        apiService.getPrice(httpData, function(result){
            if(result){
                $scope.isPriceValid = true;

                if(typeof result.basePrice === typeof undefined){}else{
                    priceFactory.setBasePrice(result.basePrice);
                }
                if(typeof result.additionalSleepsPrice === typeof undefined){}else{
                    priceFactory.setAdditionalSleepsPrice(result.additionalSleepsPrice);
                }
                if(typeof result.adminCharge === typeof undefined){}else{
                    priceFactory.setAdminCharge(result.adminCharge);
                }
                if(typeof result.creditCardCharge === typeof undefined){}else{
                    priceFactory.setCreditCardCharge(result.creditCardCharge);
                }
                if(typeof result.bookingProtectCharge === typeof undefined){}else{
                    priceFactory.setBookingProtectCharge(result.bookingProtectCharge);
                }
                if(typeof result.charityCharge === typeof undefined){}else{
                    priceFactory.setCharityCharge(result.charityCharge);
                }
                if(typeof result.petsPrice === typeof undefined){}else{
                    priceFactory.setPetsPrice(result.petsPrice);
                }
            }else{
                $scope.isPriceValid = false;
                $scope.editModeCancel();
            }
        });
    };

    $scope.changeHoliday = function(){
        var httpData = {
            reservationId: $scope.reservationId,
            fromDate: filterFactory.getFromDate(),
            toDate: filterFactory.getToDate(),
            sleeps: filterFactory.getSleepsTotal(),
            sleepsChildren: filterFactory.getSleepsChildrenTotal(),
            pets: filterFactory.getPetsTotal(),
            infants: filterFactory.getInfantsTotal(),
            userData: userDataFactory.getData(),
            price: priceFactory.getPrice()
        };
        apiService.changeReservation(httpData, function(result){
            if(result){
                $window.location.href = Routing.generate('utt_reservation_change_confirm', {}, true);
            }
        });
    };


    $scope.reservationId = '';
    $scope.setReservationId = function(reservationId){
        $scope.reservationId = reservationId;
    };

    $scope.editModes = {
        userDataFirstName: false,
        userDataLastName: false,
        userDataEmail: false,
        userDataPhone: false,
        userDataAddress: false,
        userDataCity: false,
        userDataPostcode: false,
        userDataCountry: false,

        filterSleeps: false,
        filterPets: false,
        filterInfants: false,
        filterFromDate: false,
        filterToDate: false
    };

    $scope.toggleEditMode = function(item){
        if(typeof $scope.editModes[item] === typeof undefined){}else{
            if($scope.editModes[item] == true){
                $scope.editModes[item] = false;
            }else{
                $scope.editModes[item] = true;
            }
        }
    };

    $scope.editModeCancel = function(){
        /*userDataFactory.setUserFirstName(reservationDataFactory.getUserData().firstName);
        userDataFactory.setUserLastName(reservationDataFactory.getUserData().lastName);
        userDataFactory.setUserEmail(reservationDataFactory.getUserData().email);
        userDataFactory.setUserPhone(reservationDataFactory.getUserData().phone);
        userDataFactory.setUserAddress(reservationDataFactory.getUserData().address);
        userDataFactory.setUserCity(reservationDataFactory.getUserData().city);
        userDataFactory.setUserPostcode(reservationDataFactory.getUserData().postcode);
        userDataFactory.setUserCountry(reservationDataFactory.getUserData().country);
        filterFactory.setSleeps(reservationDataFactory.getSleeps());
        filterFactory.setPets(reservationDataFactory.getPets());
        filterFactory.setInfants(reservationDataFactory.getInfants());
        filterFactory.setFromDate(reservationDataFactory.getFromDate());
        filterFactory.setToDate(reservationDataFactory.getToDate());*/

        angular.forEach($scope.editModes, function(value, key){
            $scope.editModes[key] = false;
        });
    };

    $scope.isEditMode = function(){
        if($scope.editModes.userDataFirstName == true){
            if(!angular.equals(reservationDataFactory.getUserData().firstName, userDataFactory.getUserFirstName())) { return true; }
        }
        if($scope.editModes.userDataLastName == true){
            if(!angular.equals(reservationDataFactory.getUserData().lastName, userDataFactory.getUserLastName())) { return true; }
        }
        if($scope.editModes.userDataEmail == true){
            if(!angular.equals(reservationDataFactory.getUserData().email, userDataFactory.getUserEmail())) { return true; }
        }
        if($scope.editModes.userDataPhone == true){
            if(!angular.equals(reservationDataFactory.getUserData().phone, userDataFactory.getUserPhone())) { return true; }
        }
        if($scope.editModes.userDataAddress == true){
            if(!angular.equals(reservationDataFactory.getUserData().address, userDataFactory.getUserAddress())) { return true; }
        }
        if($scope.editModes.userDataCity == true){
            if(!angular.equals(reservationDataFactory.getUserData().city, userDataFactory.getUserCity())) { return true; }
        }
        if($scope.editModes.userDataPostcode == true){
            if(!angular.equals(reservationDataFactory.getUserData().postcode, userDataFactory.getUserPostcode())) { return true; }
        }
        if($scope.editModes.userDataCountry == true){
            if(!angular.equals(reservationDataFactory.getUserData().country, userDataFactory.getUserCountry())) { return true; }
        }

        if($scope.editModes.filterSleeps == true){
            if(!angular.equals(reservationDataFactory.getSleepsAdults(), filterFactory.getSleepsAdults())) { return true; }
            if(!angular.equals(reservationDataFactory.getSleepsChildren(), filterFactory.getSleepsChildren())) { return true; }
        }
        if($scope.editModes.filterPets == true){
            if(!angular.equals(reservationDataFactory.getPets(), filterFactory.getPets())) { return true; }
        }
        if($scope.editModes.filterInfants == true){
            if(!angular.equals(reservationDataFactory.getInfants(), filterFactory.getInfants())) { return true; }
        }
        if($scope.editModes.filterFromDate == true){
            if(!angular.equals(reservationDataFactory.getFromDate(), filterFactory.getFromDate())) { return true; }
        }
        if($scope.editModes.filterToDate == true){
            if(!angular.equals(reservationDataFactory.getToDate(), filterFactory.getToDate())) { return true; }
        }

        return false;
    };

    $scope.isFormValid = function(){
        if($scope.reservationManageForm.$valid == true){
            return true;
        }
        return false;
    };
}]);

reservationManageApp.directive('datepicker', function() {
    return {
        restrict: 'A',
        require : 'ngModel',
        link : function (scope, element, attrs, ngModelCtrl) {
            $(function(){
                element.datepicker({
                    dateFormat: 'yy-mm-dd',
                    minDate: new Date(),
                    onSelect: function (date) {
                        ngModelCtrl.$setViewValue(date);
                        scope.$apply();
                    }
                });
            });
        }
    }
});

angular.bootstrap(document.getElementById("reservationManageAppHandler"),["reservationManageApp"]);