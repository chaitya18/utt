var reservationPaymentApp = angular.module('reservationPaymentApp', []);

reservationPaymentApp.controller('reservationPaymentAppCtrl', ['$scope', function($scope){

    $scope.trackFbPurchase = function (price) {
        fbq('track', 'Purchase', {currency: "GBP", value: price});
    };
}]);

angular.bootstrap(document.getElementById("reservationPaymentAppHandler"),["reservationPaymentApp"]);