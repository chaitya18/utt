<?php

namespace UTT\UserBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Show\ShowMapper;

use Sonata\AdminBundle\Route\RouteCollection;
use Sonata\UserBundle\Admin\Model\UserAdmin as BaseUserAdmin;

use UTT\UserBundle\Entity\User;
use FOS\UserBundle\Model\UserManagerInterface;

class UserAdmin extends BaseUserAdmin
{
    /**
     * @param \Sonata\AdminBundle\Form\FormMapper $formMapper
     *
     * @return void
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $typeChoices = $this->getSubject()->getAllowedTypes();

        $formMapper
            ->with('general information')
                //->add('id', null, array('required' => false, 'label' => 'ID', 'disabled' => true))
                //->add('externalId', null, array('required' => false, 'label' => 'external ID', 'attr' => array('class' => 'form-control')))
                ->add('email', null, array('required' => true, 'label' => 'user e-mail', 'attr' => array('class' => 'form-control')))
                ->add('username', null, array('required' => true, 'label' => 'user e-mail again', 'attr' => array('class' => 'form-control')))
                ->add('type', 'choice', array('required' => true, 'label' => 'type', 'choices' => $typeChoices, 'attr' => array('class' => 'form-control')))
                ->add('plainPassword', 'text', array(
                    'required' => (!$this->getSubject() || is_null($this->getSubject()->getId())),
                    'label' => 'user password'
                ))
            ->with('contact information - necessary for reservation')
                ->add('firstName', null, array('required' => true, 'label' => 'first name', 'attr' => array('class' => 'form-control')))
                ->add('lastName', null, array('required' => true, 'label' => 'last name', 'attr' => array('class' => 'form-control')))
                ->add('phone', null, array('required' => false, 'label' => 'phone', 'attr' => array('class' => 'form-control')))
                ->add('address', null, array('required' => false, 'label' => 'address', 'attr' => array('class' => 'form-control')))
                ->add('city', null, array('required' => false, 'label' => 'city', 'attr' => array('class' => 'form-control')))
                ->add('postcode', null, array('required' => false, 'label' => 'postcode', 'attr' => array('class' => 'form-control')))
                ->add('country', null, array('required' => false, 'label' => 'country', 'attr' => array('class' => 'form-control')))
            ->end()
        ;
    }

    /**
     * @param \Sonata\AdminBundle\Datagrid\ListMapper $listMapper
     *
     * @return void
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->add('_action', 'actions', array('label' => 'actions',
                'actions' => array(
                    'user-welcome-resend' => array('template' => 'UTTUserBundle:UserAdmin:list__action_user-welcome-resend.html.twig'),
                    'view' => array(),
                    'edit' => array(),
                    'delete' => array(),
                )
            ))
            //->add('id', 'text', array('label' => 'ID'))
            ->addIdentifier('username', null, array('label' => 'username'))
            ->add('firstName', null, array('label' => 'first name'))
            ->add('lastName', null, array('label' => 'last name'))
            ->add('lastLogin', null, array('label' => 'last login'))
        ;
    }

    /**
     * @param \Sonata\AdminBundle\Datagrid\DatagridMapper $datagridMapper
     *
     * @return void
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('username', null, array('label' => 'email'))
        ;
    }

    /**
     * Remove possibility to delete object.
     *
     * @param RouteCollection $collection
     */
    public function configureRoutes(RouteCollection $collection)
    {
        $collection->remove('acl');
        $collection->remove('show');
        $collection->remove('delete');
        $collection->add('user-welcome-resend', $this->getRouterIdParameter().'/user-welcome-resend');
    }

    private function prepareUser($object){
        /** @var User $object */

        $object->clearRoles();
        $object->setEnabled(true);
        if($object->getType() == User::TYPE_ADMIN) {
            $object->addRole('ROLE_SUPER_ADMIN');
        }elseif($object->getType() == User::TYPE_ACC){
            $object->addRole('ROLE_ADMIN_ACC');
        }elseif($object->getType() == User::TYPE_OWNER){
            $object->addRole('ROLE_ADMIN_OWNER');
        }elseif($object->getType() == User::TYPE_CLEANER){
            $object->addRole('ROLE_ADMIN_CLEANER');
        }

        /** @var UserManagerInterface $userManager */
        $userManager = $this->getConfigurationPool()->getContainer()->get('fos_user.user_manager');
        $userManager->updateUser($object, false);
    }

    public function prePersist($object){
        $this->prepareUser($object);
    }

    public function preUpdate($object){
        $this->prepareUser($object);
    }
}

?>
