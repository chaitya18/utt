<?php

namespace UTT\IndexBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Show\ShowMapper;

use Sonata\AdminBundle\Route\RouteCollection;

use Knp\Menu\ItemInterface as MenuItemInterface;

class EmailAdmin extends Admin
{

    /**
     * @param \Sonata\AdminBundle\Form\FormMapper $formMapper
     *
     * @return void
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $typeChoices = $this->getSubject()->getAllowedTypes();

        $formMapper
            ->with('general information')
                ->add('type', 'choice', array('required' => true, 'label' => 'type', 'disabled' => true, 'choices' => $typeChoices))
                ->add('subject', null, array('required' => false, 'label' => 'subject', 'attr' => array('class' => 'form-control')))
                ->add('replyTo', null, array('required' => false, 'label' => 'reply to', 'attr' => array('class' => 'form-control')))
                ->add('content', 'ckeditor', array('required' => true, 'label' => 'content', 'attr' => array('class' => 'form-control')))
            ->end()
        ;
    }

    /**
     * @param \Sonata\AdminBundle\Datagrid\ListMapper $listMapper
     *
     * @return void
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('type', 'text', array('label' => 'type', 'template' => 'UTTIndexBundle:EmailAdmin:list_type_type.html.twig'))
            ->add('subject', null, array('label' => 'subject'))
            ->add('replyTo', null, array('label' => 'reply to'))
            ->add('_action', 'actions', array(
                'actions' => array(
                    'view' => array(),
                    'edit' => array(),
                    'delete' => array(),
                )
            ))
        ;
    }

    /**
     * Remove possibility to delete object.
     *
     * @param RouteCollection $collection
     */
    public function configureRoutes(RouteCollection $collection)
    {
        $collection->clearExcept(array('edit', 'list'));
    }

}

?>
