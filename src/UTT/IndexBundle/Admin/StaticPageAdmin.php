<?php

namespace UTT\IndexBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Show\ShowMapper;

use Sonata\AdminBundle\Route\RouteCollection;

use Knp\Menu\ItemInterface as MenuItemInterface;

class StaticPageAdmin extends Admin
{
    /**
     * {@inheritdoc}
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $typeChoices = $this->getSubject()->getAllowedTypes();

        $formMapper
            ->with('general information')
                //->add('id', null, array('required' => false, 'label' => 'ID', 'disabled' => true))
                ->add('type', 'choice', array('required' => true, 'label' => 'type', 'choices' => $typeChoices, 'attr' => array('class' => 'form-control'), 'disabled' => true))
                ->add('url', null, array('required' => true, 'label' => 'url', 'attr' => array('class' => 'form-control')))
                ->add('title', null, array('required' => true, 'label' => 'title', 'attr' => array('class' => 'form-control')))
                ->add('content', 'ckeditor', array('required' => true, 'label' => 'content'))
            ->end()
        ;
    }

    /**
     * {@inheritdoc}
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->add('title', null, array('label' => 'title'))
            ->add('_action', 'actions', array(
                'actions' => array(
                    'view' => array(),
                    'edit' => array(),
                    'delete' => array(),
                )
            ))
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function configureRoutes(RouteCollection $collection)
    {
        $collection->clearExcept(array(
            'edit', 'list'
        ));
    }
}

?>
