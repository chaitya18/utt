<?php

namespace UTT\IndexBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Show\ShowMapper;

use Sonata\AdminBundle\Route\RouteCollection;

use Knp\Menu\ItemInterface as MenuItemInterface;

use UTT\IndexBundle\Service\HomeBannerService;

class HomeBannerAdmin extends Admin
{
    /**
     * @param \Sonata\AdminBundle\Show\ShowMapper $showMapper
     *
     * @return void
     */
    protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper
            ->with('general information')
                //->add('id', null, array('label' => 'ID'))
                ->add('title', null, array('label' => 'title'))
            ->end()
        ;
    }

    /**
     * @param \Sonata\AdminBundle\Form\FormMapper $formMapper
     *
     * @return void
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->with('general information')
                //->add('id', null, array('required' => false, 'label' => 'ID', 'disabled' => true))
                ->add('title', null, array('required' => true, 'label' => 'title', 'attr' => array('class' => 'form-control')))
                ->add('fontColor', null, array('required' => true, 'label' => 'font color', 'attr' => array('class' => 'colorPicker')))
                ->add('url', null, array('required' => false, 'label' => 'url'))
                ->add('file', 'file', array('required' => false, 'label' => 'image'))
                ->add('imagePosition', null, array('required' => false, 'label' => 'image position', 'attr' => array('class' => 'homeBannerImagePosition', 'readonly' => true)))
                ->add('image', null, array('label' => 'image preview', 'attr' => array('class' => 'homeBannerImagePreview', 'data-upload-dir' => $this->getSubject()->getUploadDir())))
            ->end()
        ;
    }

    /**
     * @param \Sonata\AdminBundle\Datagrid\ListMapper $listMapper
     *
     * @return void
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            //->add('id', null, array('label' => 'ID'))
            ->add('title', null, array('label' => 'title'))
            ->add('_action', 'actions', array(
                'actions' => array(
                    'view' => array(),
                    'edit' => array(),
                    'delete' => array(),
                    'sort' => array('template' => 'UTTIndexBundle:HomeBannerAdmin:list__action_sort.html.twig'),
                )
            ))
        ;
    }

    /**
     * @param \Sonata\AdminBundle\Datagrid\DatagridMapper $datagridMapper
     *
     * @return void
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        //$datagridMapper
            //->add('id', null, array('label' => 'ID'))
        //;
    }

    /**
     * Remove possibility to delete object.
     *
     * @param RouteCollection $collection
     */
    public function configureRoutes(RouteCollection $collection)
    {
        $collection->remove('acl');
        $collection->remove('show');
        $collection->add('sort', $this->getRouterIdParameter().'/sort');
    }

    public function prePersist($block){
        $this->saveImage($block);
    }

    public function preUpdate($block){
        $this->saveImage($block);
    }

    public function saveImage($object){
        $basepath = $this->getRequest()->getBasePath();
        $object->uploadImage($basepath);

        /** @var HomeBannerService $homeBannerService */
        $homeBannerService = $this->getConfigurationPool()->getContainer()->get('utt.homebannerservice');
        $fullSrc = $homeBannerService->saveHomeBannerCacheImage($object);
        if($fullSrc){
            $object->setFullSrc($fullSrc);
        }
    }
}

?>
