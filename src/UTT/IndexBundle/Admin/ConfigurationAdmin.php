<?php

namespace UTT\IndexBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Show\ShowMapper;

use Sonata\AdminBundle\Route\RouteCollection;

use Knp\Menu\ItemInterface as MenuItemInterface;

use UTT\ReservationBundle\Entity\Reservation;
use UTT\IndexBundle\Entity\Configuration;

class ConfigurationAdmin extends Admin
{
    /**
     * @param \Sonata\AdminBundle\Form\FormMapper $formMapper
     *
     * @return void
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $minimumNightsForFlexibleChoices = array(1 => 1, 2 => 2, 3 => 3);
        $maximumNightsForFlexibleChoices = array(10 => 10, 11 => 11, 12 => 12, 13 => 13, 14 => 14, 30 => 30);
        $formMapper
            ->with('company information')
                ->add('companyName', null, array('required' => true, 'label' => 'name', 'attr' => array('class' => 'form-control')))
                ->add('companyShortCode', null, array('required' => true, 'label' => 'shortcode', 'attr' => array('class' => 'form-control')))
                ->add('baseCommission', null, array('required' => true, 'label' => 'base commission', 'attr' => array('class' => 'form-control')))
                ->add('companyEmail', null, array('required' => true, 'label' => 'email', 'attr' => array('class' => 'form-control')))
                ->add('companyEmailReply', null, array('required' => true, 'label' => 'email reply to', 'attr' => array('class' => 'form-control')))
                ->add('companyPhone', null, array('required' => true, 'label' => 'phone', 'attr' => array('class' => 'form-control')))
                ->add('companyAddress', null, array('required' => true, 'label' => 'address', 'attr' => array('class' => 'form-control')))
                ->add('companyChequeTo', null, array('required' => true, 'label' => 'cheque to', 'attr' => array('class' => 'form-control')))
                ->add('companyVatNo', null, array('required' => true, 'label' => 'vat no.', 'attr' => array('class' => 'form-control')))
                ->add('estateOfTheWeek', null, array('required' => true, 'label' => 'cottage of the week', 'attr' => array('class' => 'form-control')))
            ->with('booking options')
                ->add('additionalSleepsPercentIncrease', null, array('required' => true, 'label' => 'additional sleeps percent increase'))
                ->add('minimumNightsForFlexible', 'choice', array('required' => true, 'label' => 'minimum nights for flexible', 'choices' => $minimumNightsForFlexibleChoices))
                ->add('maximumNightsForFlexible', 'choice', array('required' => true, 'label' => 'maximum nights for flexible', 'choices' => $maximumNightsForFlexibleChoices))
                ->add('petPriceFirst', null, array('required' => true, 'label' => 'pet price - first'))
                ->add('petPriceAdditional', null, array('required' => true, 'label' => 'pet price - additional'))

                ->add('monthsInCalendar', null, array('required' => true, 'label' => 'months in calendar'))
                ->add('chequeRequired', null, array('required' => true, 'label' => 'cheque required'))
                ->add('fullPaymentLimit', null, array('required' => true, 'label' => 'full payment limit'))
                ->add('balanceDueLimit', null, array('required' => true, 'label' => 'balance due limit'))
                ->add('depositPercentage', null, array('required' => true, 'label' => 'deposit percentage'))
                ->add('creditCardSurcharge', null, array('required' => true, 'label' => 'credit card surcharge'))
                ->add('noRefundLimit', null, array('required' => true, 'label' => 'no refund limit'))
                ->add('adminFee', null, array('required' => true, 'label' => 'default service fee percentage'))
                ->add('oddNightMultiplier', null, array('required' => true, 'label' => 'odd night multiplier'))
                ->add('autoCancelOption', null, array('required' => true, 'label' => 'auto cancel option'))
                ->add('paymentDeadline', null, array('required' => true, 'label' => 'payment deadline'))
            ->with('payment options - SAGEPAY')
                ->add('sagepayId', null, array('required' => false, 'disabled' => true, 'label' => 'SAGEPAY Id', 'attr' => array('class' => 'form-control')))
                ->add('sagepayKey', null, array('required' => false, 'disabled' => true, 'label' => 'SAGEPAY Key', 'attr' => array('class' => 'form-control')))
                ->add('sagepaySimulatorMode', null, array('required' => false, 'disabled' => true, 'label' => 'SAGEPAY Simulator Mode', 'attr' => array('class' => 'form-control')))
            ->with('payment options - SmartPay')
                ->add('smartPaySimulatorMode', null, array('required' => false, 'label' => 'SmartPay Simulator Mode', 'attr' => array('class' => 'form-control')))
                ->add('smartPayMerchantAccount', null, array('required' => true, 'label' => 'SmartPay Merchant Account', 'attr' => array('class' => 'form-control')))
                ->add('smartPaySharedSecret', null, array('required' => true, 'label' => 'SmartPay Shared Secret', 'attr' => array('class' => 'form-control')))
                ->add('smartPaySkinCodeReservation', null, array('required' => true, 'label' => 'SmartPay Skin Code for Reservation', 'attr' => array('class' => 'form-control')))
                ->add('smartPaySkinCodeVoucher', null, array('required' => true, 'label' => 'SmartPay Skin Code for Voucher', 'attr' => array('class' => 'form-control')))
            ->with('website configuration')
                ->add('footerText', 'ckeditor', array('required' => false, 'label' => 'footer text'))
            ->with('referred flag emails configuration')
                ->add('referredFlagEmailAdmin', null, array('required' => false, 'label' => 'Admin'))
                ->add('referredFlagEmailLukaszRak', null, array('required' => false, 'label' => 'Łukasz Rak'))
                ->add('referredFlagEmailSpot', null, array('required' => false, 'label' => 'Spot'))
                ->add('referredFlagEmailGreg', null, array('required' => false, 'label' => 'Greg'))
                ->add('referredFlagEmailEleri', null, array('required' => false, 'label' => 'Eleri'))
                ->add('referredFlagEmailBethan', null, array('required' => false, 'label' => 'Bethan'))
                ->add('referredFlagEmailKatie', null, array('required' => false, 'label' => 'Katie'))
            ->end()
            ->with('automatic special offers configuration')
                ->add('automaticSpecialOfferMonths', null, array('required' => false, 'label' => 'automatic special offer months'))
                ->add('automaticSpecialOfferMaxPrice', null, array('required' => false, 'label' => 'automatic special offer max price'))
            ->end()
            ->with('availability discount')
                ->add('discountPeriod', null, array('label' => 'discount period', 'help' => '(recommended 10 days)'))
                ->add('discountValue', null, array('label' => 'discount value', 'help' => '(recommended 20%)'))
            ->with('vouchers offers prices')
                ->add('voucherValue50OfferPrice', null, array('required' => true, 'label' => 'voucher 50 - offer price'))
                ->add('voucherValue100OfferPrice', null, array('required' => true, 'label' => 'voucher 100 - offer price'))
                ->add('voucherValue150OfferPrice', null, array('required' => true, 'label' => 'voucher 150 - offer price'))
                ->add('voucherValue200OfferPrice', null, array('required' => true, 'label' => 'voucher 200 - offer price'))
                ->add('voucherValue229OfferPrice', null, array('required' => true, 'label' => 'voucher 229 - offer price'))
                ->add('voucherValue250OfferPrice', null, array('required' => true, 'label' => 'voucher 250 - offer price'))
                ->add('voucherValue300OfferPrice', null, array('required' => true, 'label' => 'voucher 300 - offer price'))
                ->add('voucherValue500OfferPrice', null, array('required' => true, 'label' => 'voucher 500 - offer price'))
                ->add('voucherValue1000OfferPrice', null, array('required' => true, 'label' => 'voucher 1000 - offer price'))
            ->end()
            ->with('analytics for users configuration')
                ->add('analyticsPointsLimit', null, array('required' => true, 'label' => 'points limit'))
                ->add('analyticsPointsForMakeReservationPage', null, array('required' => true, 'label' => 'points for make reservation page'))
                ->add('analyticsPointsForEstatePage', null, array('required' => true, 'label' => 'points for estate page'))
            ->end()
            ->with('changed password information email configuration')
                ->add('changedPasswordInformationEmail', null, array('required' => true, 'label' => 'email'))
            ->end()
        ;
    }

    /**
     * @param \Sonata\AdminBundle\Datagrid\ListMapper $listMapper
     *
     * @return void
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('name', null, array('label' => 'name'))
            ->add('_action', 'actions', array(
                'actions' => array(
                    'view' => array(),
                    'edit' => array(),
                    'delete' => array(),
                )
            ))
        ;
    }

    /**
     * Remove possibility to delete object.
     *
     * @param RouteCollection $collection
     */
    public function configureRoutes(RouteCollection $collection)
    {
        $collection->clearExcept(array('list', 'edit'));
    }
}

?>
