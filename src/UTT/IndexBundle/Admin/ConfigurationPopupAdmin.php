<?php

namespace UTT\IndexBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Show\ShowMapper;

use Sonata\AdminBundle\Route\RouteCollection;

use Knp\Menu\ItemInterface as MenuItemInterface;

use UTT\ReservationBundle\Entity\Reservation;
use UTT\IndexBundle\Entity\ConfigurationPopup;

class ConfigurationPopupAdmin extends Admin
{
    /**
     * @param \Sonata\AdminBundle\Form\FormMapper $formMapper
     *
     * @return void
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->with('sale popup')
                ->add('salePopupEnabled', null, array('required' => false, 'label' => 'enabled?', 'attr' => array('class' => 'form-control')))
                ->add('salePopupDescription', null, array('required' => false, 'label' => 'description', 'attr' => array('class' => 'form-control')))
                ->add('salePopupTitle', null, array('required' => false, 'label' => 'title', 'attr' => array('class' => 'form-control')))
                ->add('salePopupButtonCaption', null, array('required' => false, 'label' => 'button caption', 'attr' => array('class' => 'form-control')))
                ->add('salePopupLink', null, array('required' => false, 'label' => 'link', 'attr' => array('class' => 'form-control')))
                ->add('salePopupTimerLink', null, array('required' => false, 'label' => 'timer image src', 'attr' => array('class' => 'form-control')))
                ->add('salePopupExpiresAt', null, array('required' => false, 'label' => 'expires by', 'years' => range(date('Y'), date('Y')+3)))
                ->add('file', 'file', array('required' => false, 'label' => 'image'))
                ->add('salePopupImage', null, array('label' => 'image preview', 'attr' => array('class' => 'estateCategoryImagePreview', 'data-upload-dir' => $this->getSubject()->getUploadDir())))
        ;
    }

    public function getTemplate($name){
        switch ($name) {
            case 'edit':
                return 'UTTIndexBundle:ConfigurationPopupAdmin:edit.html.twig';
                break;
            default:
                return parent::getTemplate($name);
                break;
        }
    }

    /**
     * @param \Sonata\AdminBundle\Datagrid\ListMapper $listMapper
     *
     * @return void
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('salePopupTitle', null, array('label' => 'title'))
            ->addIdentifier('salePopupLink', null, array('label' => 'link'))
            ->addIdentifier('salePopupExpiresAt', null, array('label' => 'expires at'))
            ->addIdentifier('salePopupEnabled', null, array('label' => 'is enabled'))
            ->add('_action', 'actions', array(
                'actions' => array(
                    'view' => array(),
                    'edit' => array(),
                    'delete' => array(),
                )
            ))
        ;
    }

    /**
     * Remove possibility to delete object.
     *
     * @param RouteCollection $collection
     */
    public function configureRoutes(RouteCollection $collection)
    {
        $collection->clearExcept(array('list', 'edit', 'create'));
    }

    public function prePersist($block){
        $this->saveImage($block);
    }

    public function preUpdate($block){
        $this->saveImage($block);
    }

    public function saveImage($object){
        /** @var ConfigurationPopup $object */

        $basepath = $this->getRequest()->getBasePath();
        $object->uploadImage($basepath);
    }
}

?>
