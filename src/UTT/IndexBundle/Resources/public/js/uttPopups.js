if($("#newsletterPopupAlert").length){
    setTimeout(function(){
        $('#newsletterPopupAlertBackground').show();
        $('#newsletterPopupAlert').show("slow");
    }, 10000);

    $('#newsletterPopupAlert').on('closed.bs.alert', function () {
        Cookies.set('newsletterPopupSeen', 1, { expires: 365 });
        $('#newsletterPopupAlertBackground').hide();
    });

    $('#newsletterPopupAlert .newsletterPopupClose').on('click', function(){
        Cookies.set('newsletterPopupSeen', 1, { expires: 365 });
        $('#newsletterPopupAlertBackground').hide();
    });
}

var windowWidth = window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth;

if($("#latestVisitsAlert").length){
    if(windowWidth > 400){
        setTimeout(function(){
            $('#latestVisitsAlert').show("slow");
        }, 5000);
    }
}

if($("#latestReservationDateAlert").length){
    if(windowWidth > 400){
        setTimeout(function(){
            $('#latestReservationDateAlert').show("slow");
        }, 8000);
    }
}