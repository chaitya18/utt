/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
var frontPage =
{
    init: function()
    {
        frontPage.initSliders();
        frontPage.initDatapicker();
        frontPage.offersBoxesAutoHeight();
        frontPage.socialMedia.init();
        $( window ).resize(function()
        {
            frontPage.offersBoxesAutoHeight();
        });
    },
    socialMedia: {
        init: function(){
            return true;
            setTimeout(function() {
                var url = Routing.generate('utt_index_social_media_plugins');

                $.get(url, function(result){
                    $('#social-media-plugins').html(result);
                });
            }, 3000);
        }
    },

    /* sliders */
    initSliders: function()
    {
        frontPage.initRecommendedPlacesTabs();
        $('.features__slider-mobile').bxSlider();
    },

    oRecommendedPlacesTabs: $('#slider-recommended-places .slider-recommended-head .tabs div div'),
    initRecommendedPlacesTabs: function()
    {
        frontPage.oRecommendedPlacesTabs.each(function()
        {
            $(this).click(function()
            {
                var oTemp = $(this);
                if(! oTemp.hasClass('active'))
                {
                    frontPage.oRecommendedPlacesTabs.each(function()
                    {
                        $(this).removeClass('active');
                        $($(this).attr('contentId')).hide();
                    });
                    oTemp.addClass('active');
                    $(oTemp.attr('contentId')).show();
                }
            });
        });

        // if tab have class active on start, is default
        frontPage.oRecommendedPlacesTabs.each(function()
        {
            if($(this).hasClass('active'))
            {
                $(this).removeClass('active');
                $(this).trigger('click');
            }
        });
    },
    /* END - sliders */

    /* datapicker */
    oSearchFromDatapick:    $('.search-panel-from-date'),
    oSearchToDatapick:      $('.search-panel-to-date'),
    oSearchNoDates:         $('.search-panel-no-date'),

    initDatapicker: function()
    {
        var sDateFormat         = "dd-M-yy";
        var oSearchFromDatapick = frontPage.oSearchFromDatapick;
        var oSearchToDatapick   = frontPage.oSearchToDatapick;
        var oSearchNoDates      = frontPage.oSearchNoDates;

        oSearchFromDatapick.datepicker(
        {
            dateFormat:sDateFormat,
            firstDay: 1,
            numberOfMonths: 1,
            showButtonPanel: true,
            minDate: new Date(),
            onSelect: function(dateText, inst)
            {
                var minDate = new Date($.datepicker.parseDate(sDateFormat,dateText));
                oSearchToDatapick.datepicker( "option", "minDate", minDate );
                //settimeout(function(){oSearchToDatapick.datepicker( "show" )}, 500);
            }
        });
        oSearchToDatapick.datepicker(
        {
            dateFormat:sDateFormat,
            firstDay: 1,
            numberOfMonths: 1,
            showButtonPanel: true,
            minDate: new Date()
        });

        oSearchNoDates.click(function()
        {
            if($(this).is(':checked'))
            {
                oSearchNoDates.each(function()
                {
                    $(this).prop('checked', true);
                });
            }
            else
            {
                oSearchNoDates.each(function()
                {
                    $(this).prop('checked', false);
                });
            }
            frontPage.disableDatapickerInput();
        });
        frontPage.disableDatapickerInput();
    },
    disableDatapickerInput: function()
    {
        if(frontPage.oSearchNoDates.is(':checked'))
        {
            frontPage.oSearchFromDatapick.attr('disabled', 'disabled');
            frontPage.oSearchToDatapick.attr('disabled', 'disabled');
        }
        else
        {
            frontPage.oSearchFromDatapick.removeAttr('disabled', 'disabled');
            frontPage.oSearchToDatapick.removeAttr('disabled', 'disabled');
        }
    },
    /* END - datapicker */

    offersBoxesAutoHeight: function()
    {
        return true;
        var tmp = [];
        $('#recommendedPlaceTab-1 .single-offer .hand').each(function()
        {
            var oHan = $(this), count = 0;
            oHan.find('div').each(function()
            {
                $(this).height('auto');
            });
            oHan.find('div').each(function()
            {
                if(typeof tmp[count] == 'undefined' || $(this).height() > tmp[count])
                {
                    tmp[count] = $(this).height();
                }
                count++;
            });
        });

        $('#recommendedPlaceTab-1 .single-offer .hand').each(function()
        {
            var oHan = $(this), count = 0;
            oHan.find('div').each(function()
            {
                $(this).height(tmp[count]);
                count++;
            });
        });
    }
};
