var googleMaps = {
    oMap:           document.getElementById('mapCanvasHandler'),
    oConfig: {
        zoom:       8,
        mapTypeId:  google.maps.MapTypeId.ROADMAP,
        streetViewControl: false,
        maxZoom: 12
    },
    map: null,
    
    setMapByLatLng: function(lat, lng)
    {
        var oLatlng = new google.maps.LatLng(lat, lng);
        var oConfigHandler = googleMaps.oConfig;
        googleMaps.oConfig.center = oLatlng;

        googleMaps.map = googleMaps.getMap();
        
        googleMaps.oConfig = oConfigHandler;
    },
    setMarker: function(lat, lng)
    {
        var oLatlng = new google.maps.LatLng(lat, lng);
        new google.maps.Marker(
        {
            map: googleMaps.map,
            position: oLatlng
        });
    },
    getMap: function()
    {
        return new google.maps.Map(googleMaps.oMap, googleMaps.oConfig);
    }
}
