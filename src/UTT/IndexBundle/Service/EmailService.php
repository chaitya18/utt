<?php

namespace UTT\IndexBundle\Service;

use Doctrine\ORM\EntityManager;
use UTT\EstateBundle\Entity\Review;
use UTT\ReservationBundle\Entity\OwnerStatement;
use UTT\IndexBundle\Entity\Configuration;
use UTT\IndexBundle\Service\ConfigurationService;
use UTT\ReservationBundle\Entity\DiscountCode;
use UTT\ReservationBundle\Entity\Reservation;
use UTT\IndexBundle\Entity\Email;
use UTT\IndexBundle\Entity\EmailRepository;
use \Symfony\Bundle\TwigBundle\TwigEngine;
use UTT\IndexBundle\Service\SpecialTextReplaceService;
use UTT\ReservationBundle\Entity\HistoryEntry;
use UTT\ReservationBundle\Entity\Voucher;
use UTT\ReservationBundle\Entity\VoucherHistoryEntry;
use UTT\UserBundle\Entity\User;
use UTT\ReservationBundle\Service\VoucherHistoryEntryService;
use UTT\ReservationBundle\Service\HistoryEntryService;
use UTT\ReservationBundle\Entity\PaymentTransaction;
use UTT\EstateBundle\Entity\Estate;
use SendGrid;

class EmailService {
    protected $_em;
    protected $configurationService;
    protected $twigEngine;
    protected $mailer;
    protected $specialTextReplaceService;
    protected $historyEntryService;
    protected $voucherHistoryEntryService;

    /** @var EmailRepository  */
    protected $emailRepository;

    public function __construct(EntityManager $em, ConfigurationService $configurationService, TwigEngine $twigEngine, \Swift_Mailer $mailer, SpecialTextReplaceService $specialTextReplaceService, HistoryEntryService $historyEntryService, VoucherHistoryEntryService $voucherHistoryEntryService){
        $this->_em = $em;
        $this->configurationService = $configurationService;
        $this->twigEngine = $twigEngine;
        $this->mailer = $mailer;
        $this->specialTextReplaceService = $specialTextReplaceService;
        $this->historyEntryService = $historyEntryService;
        $this->voucherHistoryEntryService = $voucherHistoryEntryService;

        $this->emailRepository = $this->_em->getRepository('UTTIndexBundle:Email');
    }

    public function sendWeeklyCleaningRotaEmail(Estate $estate, $reservations){
        /** @var Configuration $configuration */
        $configuration = $this->configurationService->get();
        if($configuration){
            try{
                $body = $this->twigEngine->render('UTTAdminBundle:CleaningRota:partial/cleaningRotaItems.html.twig', array(
                    'reservations' => $reservations
                ));

                $nowDate = new \DateTime('now');
                $subject = 'Under The Thatch Weekly Bookings Digest for '.$estate->getName().' '.$nowDate->format('d/M/Y');

                /** @var User $administratorUser */
                foreach($estate->getAdministratorUsers() as $administratorUser){
                    $message = \Swift_Message::newInstance()
                        ->setSubject($subject)
                        ->setFrom($configuration->getCompanyEmail(), $configuration->getCompanyName())
                        ->setReplyTo($configuration->getCompanyEmailReply(), $configuration->getCompanyName())
                        ->setTo($administratorUser->getEmail())
                        //->setCc('lukasz@underthethatch.co.uk')
                        ->setBody($body)
                        ->setContentType("text/html")
                    ;
                    $this->mailer->send($message);
                }

                /** @var User $ownerUser */
                foreach($estate->getOwnerUsers() as $ownerUser){
                    $message = \Swift_Message::newInstance()
                        ->setSubject($subject)
                        ->setFrom($configuration->getCompanyEmail(), $configuration->getCompanyName())
                        ->setReplyTo($configuration->getCompanyEmailReply(), $configuration->getCompanyName())
                        ->setTo($ownerUser->getEmail())
                        //->setCc('lukasz@underthethatch.co.uk')
                        ->setBody($body)
                        ->setContentType("text/html")
                    ;
                    $this->mailer->send($message);
                }

                /** @var User $cleanerUser */
                foreach($estate->getCleanerUsers() as $cleanerUser){
                    $message = \Swift_Message::newInstance()
                        ->setSubject($subject)
                        ->setFrom($configuration->getCompanyEmail(), $configuration->getCompanyName())
                        ->setReplyTo($configuration->getCompanyEmailReply(), $configuration->getCompanyName())
                        ->setTo($cleanerUser->getEmail())
                        //->setCc('lukasz@underthethatch.co.uk')
                        ->setBody($body)
                        ->setContentType("text/html")
                    ;
                    $this->mailer->send($message);
                }

                return true;
            }catch (\Exception $e){
                return false;
            }
        }
        return false;
    }

    public function sendOwnerReviewNotificationEmail(Review $review){
        /** @var Configuration $configuration */
        $configuration = $this->configurationService->get();
        if($configuration){
            /** @var Email $email */
            $email = $this->emailRepository->findOneBy(array(
                'type' => Email::TYPE_OWNER_REVIEW_NOTIFICATION
            ));
            if($email instanceof Email){
                try{
                    $subject = $email->getSubject();
                    $subject = $this->specialTextReplaceService->replaceWithConfiguration($subject);
                    $subject = $this->specialTextReplaceService->replaceWithReview($subject, $review);

                    /** @var User $administratorUser */
                    foreach($review->getEstate()->getAdministratorUsers() as $administratorUser){
                        $body = $this->twigEngine->render('UTTIndexBundle:Email:ownerReviewNotificationEmail.html.twig', array(
                            'content' => $email->getContent(),
                            'review' => $review,
                            'user' => $administratorUser
                        ));

                        $message = \Swift_Message::newInstance()
                            ->setSubject($subject)
                            ->setFrom($configuration->getCompanyEmail(), $configuration->getCompanyName())
                            ->setReplyTo($configuration->getCompanyEmailReply(), $configuration->getCompanyName())
                            ->setTo('cychaitya18082000@gmail.com')
                            //->setCc('lukasz@underthethatch.co.uk')
                            ->setBody($body)
                            ->setContentType("text/html")
                        ;
                        $this->mailer->send($message);
                    }

                    /** @var User $cleanerUser */
                    foreach($review->getEstate()->getCleanerUsers() as $cleanerUser){

                        $body = $this->twigEngine->render('UTTIndexBundle:Email:ownerReviewNotificationEmail.html.twig', array(
                            'content' => $email->getContent(),
                            'review' => $review,
                            'user' => $cleanerUser
                        ));

                        $message = \Swift_Message::newInstance()
                            ->setSubject($subject)
                            ->setFrom($configuration->getCompanyEmail(), $configuration->getCompanyName())
                            ->setReplyTo($configuration->getCompanyEmailReply(), $configuration->getCompanyName())
                            ->setTo('cychaitya18082000@gmail.com')
                            //->setCc('lukasz@underthethatch.co.uk')
                            ->setBody($body)
                            ->setContentType("text/html")
                        ;
                        $this->mailer->send($message);
                    }

                    /** @var User $ownerUser */
                    foreach($review->getEstate()->getOwnerUsers() as $ownerUser){

                        $body = $this->twigEngine->render('UTTIndexBundle:Email:ownerReviewNotificationEmail.html.twig', array(
                            'content' => $email->getContent(),
                            'review' => $review,
                            'user' => $ownerUser
                        ));

                        $message = \Swift_Message::newInstance()
                            ->setSubject($subject)
                            ->setFrom($configuration->getCompanyEmail(), $configuration->getCompanyName())
                            ->setReplyTo($configuration->getCompanyEmailReply(), $configuration->getCompanyName())
                            ->setTo('cychaitya18082000@gmail.com')
                            //->setCc('lukasz@underthethatch.co.uk')
                            ->setBody($body)
                            ->setContentType("text/html")
                        ;
                        $this->mailer->send($message);
                    }

                    return true;
                }catch (\Exception $e){
                    return false;
                }
            }
        }
        return false;
    }

    public function sendOwnerStatementsEmail(OwnerStatement $ownerStatement){
        /** @var Configuration $configuration */
        $configuration = $this->configurationService->get();
        if($configuration){
            /** @var Email $email */
            $email = $this->emailRepository->findOneBy(array(
                'type' => Email::TYPE_AGENCY_ACCOUNTS
            ));
            if($email instanceof Email){
                try{
                    $body = $this->twigEngine->render('UTTIndexBundle:Email:ownerStatementsEmail.html.twig', array(
                        'content' => $email->getContent(),
                        'ownerStatement' => $ownerStatement
                    ));

                    $subject = $email->getSubject();
                    $subject = $this->specialTextReplaceService->replaceWithConfiguration($subject);
                    $subject = $this->specialTextReplaceService->replaceWithOwnerStatement($subject, $ownerStatement);

                    /** @var User $administratorUser */
                    foreach($ownerStatement->getEstate()->getAdministratorUsers() as $administratorUser){
                        $message = \Swift_Message::newInstance()
                            ->setSubject($subject)
                            ->setFrom($configuration->getCompanyEmail(), $configuration->getCompanyName())
                            ->setReplyTo($configuration->getCompanyEmailReply(), $configuration->getCompanyName())
                            ->setTo($administratorUser->getEmail())
                            ->setBcc('bethan@underthethatch.co.uk')
                            ->setBody($body)
                            ->setContentType("text/html")
                        ;
                        $this->mailer->send($message);
                    }

                    /** @var User $ownerUser */
                    foreach($ownerStatement->getEstate()->getOwnerUsers() as $ownerUser){
                        $message = \Swift_Message::newInstance()
                            ->setSubject($subject)
                            ->setFrom($configuration->getCompanyEmail(), $configuration->getCompanyName())
                            ->setReplyTo($configuration->getCompanyEmailReply(), $configuration->getCompanyName())
                            ->setTo($ownerUser->getEmail())
		            ->setBcc('bethan@underthethatch.co.uk')
                            ->setBody($body)
                            ->setContentType("text/html")
                        ;
                        $this->mailer->send($message);
                    }

                    return true;
                }catch (\Exception $e){
                    return false;
                }
            }
        }
        return false;
    }

    public function sendUserChangedPasswordInformationEmail($toEmail, User $user, $plainPassword){
        /** @var Configuration $configuration */
        $configuration = $this->configurationService->get();
        if($configuration){
            /** @var Email $email */
            $email = $this->emailRepository->findOneBy(array(
                'type' => Email::TYPE_CHANGED_PASSWORD_INFORMATION
            ));
            if($email instanceof Email){
                try{
                    $body = $this->twigEngine->render('UTTIndexBundle:Email:changedPasswordInformationEmail.html.twig', array(
                        'content' => $email->getContent(),
                        'user' => $user,
                        'plainPassword' => $plainPassword
                    ));

                    $subject = $email->getSubject();
                    $subject = $this->specialTextReplaceService->replaceWithConfiguration($subject);
                    $subject = $this->specialTextReplaceService->replaceWithUser($subject, $user);
                    $subject = $this->specialTextReplaceService->replaceWithPlainPassword($subject, $plainPassword);

                    $message = \Swift_Message::newInstance()
                        ->setSubject($subject)
                        ->setFrom($configuration->getCompanyEmail(), $configuration->getCompanyName())
                        ->setReplyTo($configuration->getCompanyEmailReply(), $configuration->getCompanyName())
                        ->setTo($toEmail)
                        //->setCc('lukasz@underthethatch.co.uk')
                        ->setBody($body)
                    ;
                    $message->setContentType("text/html");

                    $this->mailer->send($message);

                    return true;
                }catch (\Exception $e){
                    return false;
                }
            }
        }
        return false;
    }

    public function sendUserWelcomeEmail(User $user, $plainPassword){
        /** @var Configuration $configuration */
        $configuration = $this->configurationService->get();
        if($configuration){
            /** @var Email $email */
            $email = $this->emailRepository->findOneBy(array(
                'type' => Email::TYPE_USER_WELCOME
            ));
            if($email instanceof Email){
                try{
                    $body = $this->twigEngine->render('UTTIndexBundle:Email:userWelcomeEmail.html.twig', array(
                        'content' => $email->getContent(),
                        'user' => $user,
                        'plainPassword' => $plainPassword
                    ));

                    $subject = $email->getSubject();
                    $subject = $this->specialTextReplaceService->replaceWithConfiguration($subject);
                    $subject = $this->specialTextReplaceService->replaceWithUser($subject, $user);
                    $subject = $this->specialTextReplaceService->replaceWithPlainPassword($subject, $plainPassword);

                    $message = \Swift_Message::newInstance()
                        ->setSubject($subject)
                        ->setFrom($configuration->getCompanyEmail(), $configuration->getCompanyName())
                        ->setReplyTo($configuration->getCompanyEmailReply(), $configuration->getCompanyName())
                        ->setTo($user->getEmail())
                        //->setCc('lukasz@underthethatch.co.uk')
                        ->setBody($body)
                    ;
                    $message->setContentType("text/html");

                    $this->mailer->send($message);

                    return true;
                }catch (\Exception $e){
                    return false;
                }
            }
        }
        return false;
    }

    public function sendThankYouForPaymentEmail(Reservation $reservation, PaymentTransaction $paymentTransaction){
        /** @var Configuration $configuration */
        $configuration = $this->configurationService->get();
        if($configuration){
            /** @var Email $email */
            $email = $this->emailRepository->findOneBy(array(
                'type' => Email::TYPE_THANK_YOU_FOR_PAYMENT
            ));
            if($email instanceof Email){
                try{
                    $body = $this->twigEngine->render('UTTIndexBundle:Email:sendThankYouForPaymentEmail.html.twig', array(
                        'content' => $email->getContent(),
                        'reservation' => $reservation,
                        'paymentTransaction' => $paymentTransaction
                    ));

                    $subject = $email->getSubject();
                    $subject = $this->specialTextReplaceService->replaceWithConfiguration($subject);
                    $subject = $this->specialTextReplaceService->replaceWithReservation($subject, $reservation);
                    $subject = $this->specialTextReplaceService->replaceWithPaymentTransaction($subject, $paymentTransaction);

                    $message = \Swift_Message::newInstance()
                        ->setSubject($subject)
                        ->setFrom($configuration->getCompanyEmail(), $configuration->getCompanyName())
                        ->setReplyTo($configuration->getCompanyEmailReply(), $configuration->getCompanyName())
                        ->setTo($reservation->getUserDataDecoded()->getEmail())
                        //->setCc('lukasz@underthethatch.co.uk')
                        ->setBody($body)
                    ;
                    $message->setContentType("text/html");

                    $this->mailer->send($message);

                    return true;
                }catch (\Exception $e){
                    return false;
                }
            }
        }
        return false;
    }

    public function sendThankYouForCharityPaymentEmail(Reservation $reservation, PaymentTransaction $paymentTransaction){
        /** @var Configuration $configuration */
        $configuration = $this->configurationService->get();
        if($configuration){
            /** @var Email $email */
            $email = $this->emailRepository->findOneBy(array(
                'type' => Email::TYPE_CHARITY_PAYMENT_THANK_YOU
            ));
            if($email instanceof Email){
                try{
                    $body = $this->twigEngine->render('UTTIndexBundle:Email:sendThankYouForCharityPaymentEmail.html.twig', array(
                        'content' => $email->getContent(),
                        'reservation' => $reservation,
                        'paymentTransaction' => $paymentTransaction
                    ));

                    $subject = $email->getSubject();
                    $subject = $this->specialTextReplaceService->replaceWithConfiguration($subject);
                    $subject = $this->specialTextReplaceService->replaceWithReservation($subject, $reservation);
                    $subject = $this->specialTextReplaceService->replaceWithPaymentTransaction($subject, $paymentTransaction);

                    $message = \Swift_Message::newInstance()
                        ->setSubject($subject)
                        ->setFrom($configuration->getCompanyEmail(), $configuration->getCompanyName())
                        ->setReplyTo($configuration->getCompanyEmailReply(), $configuration->getCompanyName())
                        ->setTo($reservation->getUserDataDecoded()->getEmail())
                        //->setCc('lukasz@underthethatch.co.uk')
                        ->setBody($body)
                    ;
                    $message->setContentType("text/html");

                    $this->mailer->send($message);

                    return true;
                }catch (\Exception $e){
                    return false;
                }
            }
        }
        return false;
    }

    public function sendReferredFlagChangedEmail(Reservation $reservation, $emailReason = false){
        /** @var Configuration $configuration */
        $configuration = $this->configurationService->get();
        if($configuration){
            /** @var Email $email */
            $email = $this->emailRepository->findOneBy(array(
                'type' => Email::TYPE_REFERRED_FLAG_CHANGED
            ));

            $referredFlagEmail = false;
            if($reservation->getReferredFlag() == $reservation::REFERRED_FLAG_ADMIN){
                $referredFlagEmail = $configuration->getReferredFlagEmailAdmin();
            }elseif($reservation->getReferredFlag() == $reservation::REFERRED_FLAG_LUKASZ_RAK){
                $referredFlagEmail = $configuration->getReferredFlagEmailLukaszRak();
            }elseif($reservation->getReferredFlag() == $reservation::REFERRED_FLAG_SPOT){
                $referredFlagEmail = $configuration->getReferredFlagEmailSpot();
            }elseif($reservation->getReferredFlag() == $reservation::REFERRED_FLAG_GREG){
                $referredFlagEmail = $configuration->getReferredFlagEmailGreg();
            }elseif($reservation->getReferredFlag() == $reservation::REFERRED_FLAG_ELERI){
                $referredFlagEmail = $configuration->getReferredFlagEmailEleri();
            }elseif($reservation->getReferredFlag() == $reservation::REFERRED_FLAG_YVONNE){
                $referredFlagEmail = $configuration->getReferredFlagEmailYvonne();
            }elseif($reservation->getReferredFlag() == $reservation::REFERRED_FLAG_BETHAN){
                $referredFlagEmail = $configuration->getReferredFlagEmailBethan();
            }elseif($reservation->getReferredFlag() == $reservation::REFERRED_FLAG_KATIE){
                $referredFlagEmail = $configuration->getReferredFlagEmailKatie();
            }

            if($email instanceof Email && $referredFlagEmail){
                try{
                    $body = $this->twigEngine->render('UTTIndexBundle:Email:sendReferredFlagChangedEmail.html.twig', array(
                        'content' => $email->getContent(),
                        'reservation' => $reservation,
                        'emailReason' => ($emailReason ? $emailReason : '')
                    ));

                    $subject = $email->getSubject();
                    $subject = $this->specialTextReplaceService->replaceWithConfiguration($subject);
                    $subject = $this->specialTextReplaceService->replaceWithReservation($subject, $reservation);

                    //$this->sendWithSendgrid($referredFlagEmail, $configuration->getCompanyEmailReply(), $configuration->getCompanyName(), $subject, $body, $body);

                    $message = \Swift_Message::newInstance()
                        ->setSubject($subject)
                        ->setFrom($configuration->getCompanyEmail(), $configuration->getCompanyName())
                        ->setReplyTo($configuration->getCompanyEmailReply(), $configuration->getCompanyName())
                        ->setTo($referredFlagEmail)
                        ->setBody($body)
                    ;
                    $message->setContentType("text/html");

                    $this->mailer->send($message);

                    return true;
                }catch (\Exception $e){
                    return false;
                }
            }
        }
        return false;
    }

    public function sendReservationBookingProtectEmail(Reservation $reservation){
        /** @var Configuration $configuration */
        $configuration = $this->configurationService->get();
        if($configuration){
            /** @var Email $email */
            $email = $this->emailRepository->findOneBy(array(
                'type' => Email::TYPE_BOOKING_PROTECT_EMAIL
            ));
            if($email instanceof Email){
                try{
                    $body = $this->twigEngine->render('UTTIndexBundle:Email:reservationBookingProtectEmail.html.twig', array(
                        'content' => $email->getContent(),
                        'reservation' => $reservation
                    ));

                    $subject = $email->getSubject();
                    $subject = $this->specialTextReplaceService->replaceWithConfiguration($subject);
                    $subject = $this->specialTextReplaceService->replaceWithReservation($subject, $reservation);

                    $message = \Swift_Message::newInstance()
                        ->setSubject($subject)
                        ->setFrom($configuration->getCompanyEmail(), $configuration->getCompanyName())
                        ->setReplyTo($configuration->getCompanyEmailReply(), $configuration->getCompanyName())
                        ->setTo($reservation->getUserDataDecoded()->getEmail())
                        ->setBody($body)
                    ;
                    $message->setContentType("text/html");

                    $this->mailer->send($message);

                    return true;
                }catch (\Exception $e){
                    return false;
                }
            }
        }
        return false;
    }

    public function sendReservationArrivalInstructionEmail(Reservation $reservation){
        /** @var Configuration $configuration */
        $configuration = $this->configurationService->get();
        if($configuration){
            /** @var Email $email */
            $email = $this->emailRepository->findOneBy(array(
                'type' => Email::TYPE_ARRIVAL_DETAILS
            ));
            if($email instanceof Email){
                
                try{
                    $body = $this->twigEngine->render('UTTIndexBundle:Email:reservationArrivalInstructionEmail.html.twig', array(
                        'content' => $email->getContent(),
                        'reservation' => $reservation
                    ));

                    $subject = $email->getSubject();
                    $subject = $this->specialTextReplaceService->replaceWithConfiguration($subject);
                    $subject = $this->specialTextReplaceService->replaceWithReservation($subject, $reservation);

                    $message = \Swift_Message::newInstance()
                        ->setSubject($subject)
                        ->setFrom($configuration->getCompanyEmail(), $configuration->getCompanyName())
                        ->setReplyTo($configuration->getCompanyEmailReply(), $configuration->getCompanyName())
                        ->setTo($reservation->getUserDataDecoded()->getEmail())
                        ->setBody($body)
                    ;
                    $message->setContentType("text/html");

                    $this->mailer->send($message);

                    return true;
                }catch (\Exception $e){
                    echo $e->getMessage();
                    exit();
                    return false;
                }
            }
        }
        return false;
    }

    public function sendReservationThankYouEmail(Reservation $reservation){
        /** @var Configuration $configuration */

        $configuration = $this->configurationService->get();
        if($configuration){
            /** @var Email $email */
            $email = $this->emailRepository->findOneBy(array(
                'type' => Email::TYPE_THANK_YOU
            ));
            if($email instanceof Email){
                try{
                    $body = $this->twigEngine->render('UTTIndexBundle:Email:reservationThankYouEmail.html.twig', array(
                        'content' => $email->getContent(),
                        'reservation' => $reservation
                    ));
                    $subject = $email->getSubject();
                    $subject = $this->specialTextReplaceService->replaceWithConfiguration($subject);
                    $subject = $this->specialTextReplaceService->replaceWithReservation($subject, $reservation);

                    $message = \Swift_Message::newInstance()
                        ->setSubject($subject)
                        ->setFrom($configuration->getCompanyEmail(), $configuration->getCompanyName())
                        ->setReplyTo($configuration->getCompanyEmailReply(), $configuration->getCompanyName())
                        ->setTo($reservation->getUserDataDecoded()->getEmail())
                        ->setBody($body)
                    ;
                    $message->setContentType("text/html");

                    $this->mailer->send($message);

                    return true;
                }catch (\Exception $e){
                    return false;
                }
            }
        }
        return false;
    }

    public function sendReservationBrokenStatusChangedEmail(Reservation $reservation){
        /** @var Configuration $configuration */
        $configuration = $this->configurationService->get();
        if($configuration){
            /** @var Email $email */
            $email = $this->emailRepository->findOneBy(array(
                'type' => Email::TYPE_BROKEN_STATUS_CHANGED
            ));
            if($email instanceof Email){
                try{
                    $body = $this->twigEngine->render('UTTIndexBundle:Email:reservationBrokenStatusChangedEmail.html.twig', array(
                        'content' => $email->getContent(),
                        'reservation' => $reservation
                    ));

                    $subject = $email->getSubject();
                    $subject = $this->specialTextReplaceService->replaceWithConfiguration($subject);
                    $subject = $this->specialTextReplaceService->replaceWithReservation($subject, $reservation);

                    $message = \Swift_Message::newInstance()
                        ->setSubject($subject)
                        ->setFrom($configuration->getCompanyEmail(), $configuration->getCompanyName())
                        ->setReplyTo($configuration->getCompanyEmailReply(), $configuration->getCompanyName())
                        //->setTo('lukasz@underthethatch.co.uk')
                        ->setBody($body)
                    ;
                    $message->setContentType("text/html");

                    $this->mailer->send($message);

                    return true;
                }catch (\Exception $e){
                    return false;
                }
            }
        }
        return false;
    }

    public function sendReservationManageConfirmation(Reservation $reservation){
        /** @var Configuration $configuration */
        $configuration = $this->configurationService->get();
        if($configuration){
            /** @var Email $email */
            $email = $this->emailRepository->findOneBy(array(
                'type' => Email::TYPE_RESERVATION_MANAGE_CONFIRMATION
            ));
            if($email instanceof Email){
                try{
                    $body = $this->twigEngine->render('UTTIndexBundle:Email:reservationManageConfirmation.html.twig', array(
                        'content' => $email->getContent(),
                        'reservation' => $reservation
                    ));

                    $subject = $email->getSubject();
                    $subject = $this->specialTextReplaceService->replaceWithConfiguration($subject);
                    $subject = $this->specialTextReplaceService->replaceWithReservation($subject, $reservation);

                    $message = \Swift_Message::newInstance()
                        ->setSubject($subject)
                        ->setFrom($configuration->getCompanyEmail(), $configuration->getCompanyName())
                        ->setReplyTo($configuration->getCompanyEmailReply(), $configuration->getCompanyName())
                        ->setTo($reservation->getUserDataDecoded()->getEmail())
                        ->setBody($body)
                    ;
                    $message->setContentType("text/html");

                    $this->mailer->send($message);

                    return true;
                }catch (\Exception $e){
                    return false;
                }
            }
        }
        return false;
    }

    public function sendDiscountCodeEmail($toEmail, DiscountCode $discountCode){
        /** @var Configuration $configuration */
        $configuration = $this->configurationService->get();
        if($configuration){
            /** @var Email $email */
            $email = $this->emailRepository->findOneBy(array(
                'type' => Email::TYPE_DISCOUNT_CODE_SEND
            ));
            if($email instanceof Email){
                try{
                    $body = $this->twigEngine->render('UTTIndexBundle:Email:discountCodeEmail.html.twig', array(
                        'content' => $email->getContent(),
                        'discountCode' => $discountCode
                    ));

                    $subject = $email->getSubject();
                    $subject = $this->specialTextReplaceService->replaceWithConfiguration($subject);

                    $message = \Swift_Message::newInstance()
                        ->setSubject($subject)
                        ->setFrom($configuration->getCompanyEmail(), $configuration->getCompanyName())
                        ->setReplyTo($configuration->getCompanyEmailReply(), $configuration->getCompanyName())
                        ->setTo($toEmail)
                        //->setCc('lukasz@underthethatch.co.uk')
                        ->setBody($body)
                    ;
                    $message->setContentType("text/html");

                    $this->mailer->send($message);

                    return true;
                }catch (\Exception $e){
                    return false;
                }
            }
        }
        return false;
    }

    public function sendHistoryEmail(Reservation $reservation, HistoryEntry $historyEntry){
        /** @var Configuration $configuration */
        $configuration = $this->configurationService->get();
        if($configuration){
            /** @var Email $email */
            $email = $this->emailRepository->findOneBy(array(
                'type' => Email::TYPE_HISTORY
            ));
            if($email instanceof Email){
                try{
                    $body = $this->twigEngine->render('UTTIndexBundle:Email:historyEmail.html.twig', array(
                        'content' => $email->getContent(),
                        'reservation' => $reservation,
                        'historyEntry' => $historyEntry
                    ));

                    $subject = $email->getSubject();
                    $subject = $this->specialTextReplaceService->replaceWithConfiguration($subject);
                    $subject = $this->specialTextReplaceService->replaceWithReservation($subject, $reservation);

                    $message = \Swift_Message::newInstance()
                        ->setSubject($subject)
                        ->setFrom($configuration->getCompanyEmail(), $configuration->getCompanyName())
                        ->setReplyTo($configuration->getCompanyEmailReply(), $configuration->getCompanyName())
                        ->setTo($reservation->getUserDataDecoded()->getEmail())
                        ->setBody($body)
                    ;
                    $message->setContentType("text/html");

                    $this->mailer->send($message);

                    return true;
                }catch (\Exception $e){
                    return false;
                }
            }
        }
        return false;
    }

    public function sendBookingCancelledEleriNotificationEmail(Reservation $reservation){
        /** @var Configuration $configuration */
        $configuration = $this->configurationService->get();
        if($configuration){
            /** @var Email $email */
            $email = $this->emailRepository->findOneBy(array(
                'type' => Email::TYPE_BOOKING_CANCELLED_ELERI_NOTIFICATION
            ));
            if($email instanceof Email){
                try{
                    $body = $this->twigEngine->render('UTTIndexBundle:Email:bookingCancelledEleriNotificationEmail.html.twig', array(
                        'content' => $email->getContent(),
                        'reservation' => $reservation
                    ));

                    $subject = $email->getSubject();
                    $subject = $this->specialTextReplaceService->replaceWithConfiguration($subject);
                    $subject = $this->specialTextReplaceService->replaceWithReservation($subject, $reservation);

                    $message = \Swift_Message::newInstance()
                        ->setSubject($subject)
                        ->setFrom($configuration->getCompanyEmail(), $configuration->getCompanyName())
                        ->setReplyTo($configuration->getCompanyEmailReply(), $configuration->getCompanyName())
                        ->setTo('eleri@underthethatch.co.uk')
                        //->setCc('lukasz@underthethatch.co.uk')
                        ->setBody($body)
                    ;
                    $message->setContentType("text/html");

                    $this->mailer->send($message);

                    return true;
                }catch (\Exception $e){
                    return false;
                }
            }
        }
        return false;
    }

/*
 mail do usera
- cron który sprawdza czy rezerwacje zostały opłacone i jeśl nie to wysyla przypomnienie   # DONE
 */
    public function sendAutoCancelOverdueBalancePaymentReminder(Reservation $reservation){
        /** @var Configuration $configuration */
        $configuration = $this->configurationService->get();
        if($configuration){
            /** @var Email $email */
            $email = $this->emailRepository->findOneBy(array(
                'type' => Email::TYPE_PAYMENT_DUE
            ));
            if($email instanceof Email){
                try{
                    $body = $this->twigEngine->render('UTTIndexBundle:Email:autoCancelOverdueBalancePaymentReminder.html.twig', array(
                        'content' => $email->getContent(),
                        'reservation' => $reservation
                    ));

                    $subject = $email->getSubject();
                    $subject = $this->specialTextReplaceService->replaceWithConfiguration($subject);
                    $subject = $this->specialTextReplaceService->replaceWithReservation($subject, $reservation);

                    $message = \Swift_Message::newInstance()
                        ->setSubject($subject)
                        ->setFrom($configuration->getCompanyEmail(), $configuration->getCompanyName())
                        ->setReplyTo($configuration->getCompanyEmailReply(), $configuration->getCompanyName())
                        ->setTo($reservation->getUserDataDecoded()->getEmail())
                        ->setBody($body)
                    ;
                    $message->setContentType("text/html");

                    $this->mailer->send($message);

                    return true;
                }catch (\Exception $e){
                    var_dump($e->getMessage());
                    return false;
                }
            }
        }
        return false;
    }

    /*
CANCELBALNOTPAID  mail do usera
   - cron który sprawdza czy rezerwacje zostały opłacone i jeśl nie to je anuluje   # DONE
     */
    public function sendAutoCancelOverdueBalancePayment(Reservation $reservation){
        /** @var Configuration $configuration */
        $configuration = $this->configurationService->get();
        if($configuration){
            /** @var Email $email */
            $email = $this->emailRepository->findOneBy(array(
                'type' => Email::TYPE_CANCEL_BAL_NOT_PAID
            ));
            if($email instanceof Email){
                try{
                    $body = $this->twigEngine->render('UTTIndexBundle:Email:autoCancelOverdueBalancePayment.html.twig', array(
                        'content' => $email->getContent(),
                        'reservation' => $reservation
                    ));

                    $subject = $email->getSubject();
                    $subject = $this->specialTextReplaceService->replaceWithConfiguration($subject);
                    $subject = $this->specialTextReplaceService->replaceWithReservation($subject, $reservation);

                    $message = \Swift_Message::newInstance()
                        ->setSubject($subject)
                        ->setFrom($configuration->getCompanyEmail(), $configuration->getCompanyName())
                        ->setReplyTo($configuration->getCompanyEmailReply(), $configuration->getCompanyName())
                        ->setTo($reservation->getUserDataDecoded()->getEmail())
                        ->setBody($body)
                    ;
                    $message->setContentType("text/html");

                    $this->mailer->send($message);

                    return true;
                }catch (\Exception $e){
                    return false;
                }
            }
        }
        return false;
    }

    /*
AUTOCANCEL mail do usera
   - cron który sprawdza czy rezerwacje zostały opłacone i jeśl nie to je anuluje       # DONE
     */
    public function sendAutoCancelNoPayment(Reservation $reservation){
        /** @var Configuration $configuration */
        $configuration = $this->configurationService->get();
        if($configuration){
            /** @var Email $email */
            $email = $this->emailRepository->findOneBy(array(
                'type' => Email::TYPE_AUTO_CANCEL
            ));
            if($email instanceof Email){
                try{
                    $body = $this->twigEngine->render('UTTIndexBundle:Email:autoCancelNoPayment.html.twig', array(
                        'content' => $email->getContent(),
                        'reservation' => $reservation
                    ));

                    $subject = $email->getSubject();
                    $subject = $this->specialTextReplaceService->replaceWithConfiguration($subject);
                    $subject = $this->specialTextReplaceService->replaceWithReservation($subject, $reservation);

                    $message = \Swift_Message::newInstance()
                        ->setSubject($subject)
                        ->setFrom($configuration->getCompanyEmail(), $configuration->getCompanyName())
                        ->setReplyTo($configuration->getCompanyEmailReply(), $configuration->getCompanyName())
                        ->setTo($reservation->getUserDataDecoded()->getEmail())
                        ->setBody($body)
                    ;
                    $message->setContentType("text/html");

                    $this->mailer->send($message);

                    return true;
                }catch (\Exception $e){
                    return false;
                }
            }
        }
        return false;
    }

    public function sendNoPaymentCancellationReinstate(Reservation $reservation){
        // Greg asked to remove it
        return false;

        /** @var Configuration $configuration */
        $configuration = $this->configurationService->get();
        if($configuration){
            /** @var Email $email */
            $email = $this->emailRepository->findOneBy(array(
                'type' => Email::TYPE_NO_PAYMENT_CANCELLATION_REINSTATE_EMAIL
            ));
            if($email instanceof Email){
                try{
                    $body = $this->twigEngine->render('UTTIndexBundle:Email:noPaymentCancellationReinstate.html.twig', array(
                        'content' => $email->getContent(),
                        'reservation' => $reservation
                    ));

                    $subject = $email->getSubject();
                    $subject = $this->specialTextReplaceService->replaceWithConfiguration($subject);
                    $subject = $this->specialTextReplaceService->replaceWithReservation($subject, $reservation);

                    $message = \Swift_Message::newInstance()
                        ->setSubject($subject)
                        ->setFrom($configuration->getCompanyEmail(), $configuration->getCompanyName())
                        ->setReplyTo($configuration->getCompanyEmailReply(), $configuration->getCompanyName())
                        ->setTo($reservation->getUserDataDecoded()->getEmail())
                        //->setCc('lukasz@underthethatch.co.uk')
                        ->setBody($body)
                    ;
                    $message->setContentType("text/html");

                    $this->mailer->send($message);

                    $historyEntryComment = $this->historyEntryService->newCommentNoPaymentCancellationReinstate();
                    $this->historyEntryService->newEntry(HistoryEntry::TYPE_NO_PAYMENT_CANCELLATION_REINSTATE_EMAIL_SENT, $reservation, $reservation->getUser(), $historyEntryComment);

                    return true;
                }catch (\Exception $e){
                    return false;
                }
            }
        }
        return false;
    }
    /*
LANDLORD mail do właściciela ze zmianą w zamówieniu
  - REFERTOUTT
  - REFERTOSPOT
  - REFERTOLANDLORD
  - booking robiony z admina w pliku cal.php
  - cancel booking z poziomu admina
  - przy zmianie rezerwacji przez usera:
     - przy zmianie parametrów rezerwacji                       # DONE
     - przy comment do owner
     - przy calcelacji przez usera                              # DONE
   - przy opłaceniu rezerwacji przez usera                      # DONE
   - przy requote booking                                       # DONE
   - // Confirm the payment has been received / taken : admin_functions.php
   - updated by owner w adminie czyli chyba requote             # DONE
     */
    public function sendForLandlord(Reservation $reservation, $emailReason = false, $emailType = Email::TYPE_LANDLORD,  $estate = null){
        /** @var Configuration $configuration */
        $configuration = $this->configurationService->get();
        $estate = $estate ? $estate: $reservation->getEstate();
        if($configuration){
            /** @var Email $email */
            $email = $this->emailRepository->findOneBy(array(
                'type' => $emailType
            ));
            if($email instanceof Email){
                try{
                    $body = $this->twigEngine->render('UTTIndexBundle:Email:forLandlord.html.twig', array(
                        'content' => $email->getContent(),
                        'reservation' => $reservation,
                        'emailReason' => ($emailReason ? $emailReason : '')
                    ));

                    $subject = $email->getSubject();
                    $subject = $this->specialTextReplaceService->replaceWithConfiguration($subject);
                    $subject = $this->specialTextReplaceService->replaceWithReservation($subject, $reservation);

                    /** @var User $administratorUser */
                    foreach($estate->getAdministratorUsers() as $administratorUser){
                        $message = \Swift_Message::newInstance()
                            ->setSubject($subject)
                            ->setFrom($configuration->getCompanyEmail(), $configuration->getCompanyName())
                            ->setReplyTo($configuration->getCompanyEmailReply(), $configuration->getCompanyName())
                            ->setTo($administratorUser->getEmail())
                            ->setBody($body)
                            ->setContentType("text/html")
                        ;
                        $this->mailer->send($message);

                        $historyEntryComment = $this->historyEntryService->newCommentLandlordNotificationEmail($administratorUser->getEmail());
                        $this->historyEntryService->newEntry(HistoryEntry::TYPE_LANDLORD_NOTIFICATION_SENT, $reservation, $reservation->getUser(), $historyEntryComment);
                    }

                    /** @var User $ownerUser */
                    foreach($estate->getOwnerUsers() as $ownerUser){
                        $message = \Swift_Message::newInstance()
                            ->setSubject($subject)
                            ->setFrom($configuration->getCompanyEmail(), $configuration->getCompanyName())
                            ->setReplyTo($configuration->getCompanyEmailReply(), $configuration->getCompanyName())
                            ->setTo($ownerUser->getEmail())
                            ->setBody($body)
                            ->setContentType("text/html")
                        ;
                        $this->mailer->send($message);

                        $historyEntryComment = $this->historyEntryService->newCommentLandlordNotificationEmail($ownerUser->getEmail());
                        $this->historyEntryService->newEntry(HistoryEntry::TYPE_LANDLORD_NOTIFICATION_SENT, $reservation, $reservation->getUser(), $historyEntryComment);
                    }

                    /** @var User $cleanerUser */
                    foreach($estate->getCleanerUsers() as $cleanerUser){
                        $message = \Swift_Message::newInstance()
                            ->setSubject($subject)
                            ->setFrom($configuration->getCompanyEmail(), $configuration->getCompanyName())
                            ->setReplyTo($configuration->getCompanyEmailReply(), $configuration->getCompanyName())
                            ->setTo($cleanerUser->getEmail())
                            ->setBody($body)
                            ->setContentType("text/html")
                        ;
                        $this->mailer->send($message);

                        $historyEntryComment = $this->historyEntryService->newCommentLandlordNotificationEmail($cleanerUser->getEmail());
                        $this->historyEntryService->newEntry(HistoryEntry::TYPE_LANDLORD_NOTIFICATION_SENT, $reservation, $reservation->getUser(), $historyEntryComment);
                    }

                    return true;
                }catch (\Exception $e){
                    echo $e->getMessage().PHP_EOL;
                    return false;
                }
            }
        }
        return false;
    }

    public function sendVoucherConfirmation(Voucher $voucher){
        /** @var Configuration $configuration */
        $configuration = $this->configurationService->get();
        if($configuration){
            /** @var Email $email */
            $email = $this->emailRepository->findOneBy(array(
                'type' => Email::TYPE_NEW_VOUCHER
            ));
            if($email instanceof Email){
                try{
                    $body = $this->twigEngine->render('UTTIndexBundle:Email:voucherConfirmation.html.twig', array(
                        'content' => $email->getContent(),
                        'voucher' => $voucher
                    ));

                    $subject = $email->getSubject();
                    $subject = $this->specialTextReplaceService->replaceWithConfiguration($subject);
                    $subject = $this->specialTextReplaceService->replaceWithVoucher($subject, $voucher);

                    $message = \Swift_Message::newInstance()
                        ->setSubject($subject)
                        ->setFrom($configuration->getCompanyEmail(), $configuration->getCompanyName())
                        ->setReplyTo($configuration->getCompanyEmailReply(), $configuration->getCompanyName())
                        ->setTo($voucher->getUserDataDecoded()->getEmail())
                        ->setBody($body)
                    ;
                    $message->setContentType("text/html");

                    $this->mailer->send($message);

                    $historyEntryComment = $this->voucherHistoryEntryService->newCommentConfirmationEmail();
                    $this->voucherHistoryEntryService->newEntryByLoggedUser(VoucherHistoryEntry::TYPE_VOUCHER_CONFIRMATION_RESENT, $voucher, $historyEntryComment);

                    return true;
                }catch (\Exception $e){
                    return false;
                }
            }
        }
        return false;
    }

    /*
        NEWBOOKING mail do klienta
         - składanie zamówienia przez klienta           # DONE
         - ponowne wysłanie maila potwierdzającego      # DONE
     */
    public function sendReservationConfirmation(Reservation $reservation){
        /** @var Configuration $configuration */
        $configuration = $this->configurationService->get();
        if($configuration){
            /** @var Email $email */
            $email = $this->emailRepository->findOneBy(array(
                'type' => Email::TYPE_NEW_BOOKING
            ));
            if($email instanceof Email){
                try{
                    $body = $this->twigEngine->render('UTTIndexBundle:Email:reservationConfirmation.html.twig', array(
                        'content' => $email->getContent(),
                        'reservation' => $reservation
                    ));

                    $subject = $email->getSubject();
                    $subject = $this->specialTextReplaceService->replaceWithConfiguration($subject);
                    $subject = $this->specialTextReplaceService->replaceWithReservation($subject, $reservation);

                    $message = \Swift_Message::newInstance()
                        ->setSubject($subject)
                        ->setFrom($configuration->getCompanyEmail(), $configuration->getCompanyName())
                        ->setReplyTo($configuration->getCompanyEmailReply(), $configuration->getCompanyName())
                        ->setTo($reservation->getUserDataDecoded()->getEmail())
                        ->setBody($body)
                    ;
                    $message->setContentType("text/html");

                    $this->mailer->send($message);

                    $historyEntryComment = $this->historyEntryService->newCommentConfirmationEmail();
                    $this->historyEntryService->newEntryByLoggedUser(HistoryEntry::TYPE_BOOKING_CONFIRMATION_RESENT, $reservation, $historyEntryComment);

                    return true;
                }catch (\Exception $e){
                    return false;
                }
            }
        }
        return false;
    }

    public function sendTechEmail($subject, $body) {
        $configuration = $this->configurationService->get();
        if($configuration) {
            $message = \Swift_Message::newInstance()
                ->setSubject($subject)
                ->setFrom('tech@underthethatch.co.uk')
                ->setTo(array('tech@underthethatch.co.uk', 'darek.zawadzki@frompolandwithdev.com'))
                ->setBody($body);
            $message->setContentType("text/html");

            //$this->mailer->send($message);
        }
    }

    private function sendWithSendgrid($to, $from, $fromName, $subject, $text, $html){
        $sendgrid = new SendGrid('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');
        $email = new SendGrid\Email();
        $email
            ->addTo($to)
            ->setReplyTo($from)
            ->setFrom($from)
            ->setFromName($fromName)
            ->setSubject($subject)
            ->setText($text)
            ->setHtml($html)
        ;

        $sendgrid->send($email);
    }
}
