<?php

namespace UTT\IndexBundle\Service;

use Doctrine\ORM\EntityManager;

use UTT\IndexBundle\Entity\ConfigurationPopup;

class ConfigurationService {
    protected $_em;

    public function __construct(EntityManager $em){
        $this->_em = $em;
    }

    public function get(){
        $configuration = $this->_em->getRepository('UTTIndexBundle:Configuration')->find(1);
        if($configuration){
            return $configuration;
        }

        return false;
    }

    public function getConfigurationPopup(){
        /** @var ConfigurationPopup $configurationPopup */
        $configurationPopup = $this->_em->getRepository('UTTIndexBundle:ConfigurationPopup')->findOneBy(array(
            'salePopupEnabled' => true
        ));
        if($configurationPopup instanceof ConfigurationPopup){
            return $configurationPopup;
        }

        return false;
    }

    public function getCottageOfTheWeek(){
        $cottageOfTheWeek = $this->_em->getRepository('UTTIndexBundle:CottageOfTheWeek')->find(1);
        if($cottageOfTheWeek){
            return $cottageOfTheWeek;
        }

        return false;
    }

    public function disableConfigurationPopupsExceptOne(ConfigurationPopup $configurationPopup){

    }
}