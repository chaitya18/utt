<?php

namespace UTT\IndexBundle\Service;

use Doctrine\ORM\EntityManager;

use Liip\ImagineBundle\Imagine\Cache\CacheManager;
use Liip\ImagineBundle\Imagine\Cache\Signer;
use Liip\ImagineBundle\Imagine\Data\DataManager;
use Liip\ImagineBundle\Imagine\Filter\FilterManager;

use UTT\IndexBundle\Entity\HomeBanner;

use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Liip\ImagineBundle\Exception\Binary\Loader\NotLoadableException;
use Imagine\Exception\RuntimeException;

class HomeBannerService {
    protected $_em;

    /** @var CacheManager */
    protected $cacheManager;

    /** @var Signer */
    protected $signer;

    /** @var DataManager  */
    protected $dataManager;

    /** @var FilterManager  */
    protected $filterManager;

    public function __construct(EntityManager $em, CacheManager $cacheManager, Signer $signer, DataManager $dataManager, FilterManager $filterManager){
        $this->_em = $em;
        $this->cacheManager = $cacheManager;
        $this->signer = $signer;
        $this->dataManager = $dataManager;
        $this->filterManager = $filterManager;
    }

    public function saveHomeBannerCacheImage(HomeBanner $homeBanner){
        $filter = 'home_banner';
        $path = $homeBanner->getUploadDir().'/'.$homeBanner->getImage();
        $runtimeConfig = array(
            "crop" => array(
                "start" => array(0, ($homeBanner->getImagePosition() * -1))
            )
        );

        $path = ltrim($path, '/');
        $filters = $runtimeConfig;
        $hash = $this->signer->sign($path, $runtimeConfig);

        # COPIED FROM Liip\ImagineBundle\Controller\ImagineController::filterRuntimeAction()
        try {
            if (true !== $this->signer->check($hash, $path, $filters)) {
                //throw new BadRequestHttpException(sprintf('Signed url does not pass the sign check for path "%s" and filter "%s" and runtime config %s', $path, $filter, json_encode($filters)));
                return false;
            }

            try {
                $binary = $this->dataManager->find($filter, $path);
            } catch (NotLoadableException $e) {
                //throw new NotFoundHttpException(sprintf('Source image could not be found for path "%s" and filter "%s"', $path, $filter), $e);
                return false;
            }

            $cachePrefix = 'rc/'.$hash;

            $this->cacheManager->store(
                $this->filterManager->applyFilter($binary, $filter, array(
                    'filters' => $filters,
                )),
                $cachePrefix.'/'.$path,
                $filter
            );

            return $this->cacheManager->resolve($cachePrefix.'/'.$path, $filter);
        } catch (RuntimeException $e) {
            //throw new \RuntimeException(sprintf('Unable to create image for path "%s" and filter "%s". Message was "%s"', $hash.'/'.$path, $filter, $e->getMessage()), 0, $e);
            return false;
        }
    }
}