<?php

namespace UTT\IndexBundle\Service;

use Doctrine\ORM\EntityManager;
use UTT\IndexBundle\Service\ConfigurationService;
use UTT\IndexBundle\Entity\Configuration;
use UTT\ReservationBundle\Entity\Reservation;
use UTT\ReservationBundle\Entity\Voucher;
use UTT\ReservationBundle\Factory\ReservationFactory;
use \Symfony\Component\Routing\Router;
use UTT\ReservationBundle\Entity\HistoryEntry;
use UTT\UserBundle\Entity\User;
use FOS\UserBundle\Model\UserInterface;
use UTT\ReservationBundle\Entity\DiscountCode;
use UTT\EstateBundle\Entity\Estate;
use UTT\ReservationBundle\Entity\ArrivalInstruction;
use UTT\ReservationBundle\Entity\PaymentTransaction;
use UTT\ReservationBundle\Entity\OwnerStatement;
use UTT\EstateBundle\Entity\Review;

class SpecialTextReplaceService {
    protected $_em;
    protected $configurationService;
    protected $reservationFactory;
    protected $router;
    protected $securityContext;
    protected $adminSystemUrlParam;

    public function __construct(EntityManager $em, ConfigurationService $configurationService, ReservationFactory $reservationFactory, Router $router, $securityContext, $adminSystemUrlParam){
        $this->_em = $em;
        $this->configurationService = $configurationService;
        $this->reservationFactory = $reservationFactory;
        $this->router = $router;
        $this->securityContext = $securityContext;
        $this->adminSystemUrlParam = $adminSystemUrlParam;
    }

    public function replaceWithConfiguration($text){
        /** @var Configuration $configuration */
        $configuration = $this->configurationService->get();
        if($configuration){
            $text = str_replace('%%COMPANYNAME%%', $configuration->getCompanyName(), $text);
            $text = str_replace('%%WEBSITELINK%%', $this->router->generate('utt_index_homepage', array(), true), $text);
            $text = str_replace('%%ADMIN_SYSTEM_URL%%', $this->router->generate('sonata_admin_dashboard', array(), true), $text);
            $text = str_replace('%%ADMIN_SYSTEM_URL_PARAM%%', $this->adminSystemUrlParam, $text);
        }

        return $text;
    }

    public function replaceWithEmailReason($text, $emailReason){
        $text = str_replace('%%EMAIL_REASON%%', $emailReason, $text);

        return $text;
    }

    public function replaceWithPlainPassword($text, $plainPassword){
        $text = str_replace('%%PLAIN_PASSWORD%%', $plainPassword, $text);

        return $text;
    }

    public function replaceWithVoucher($text, Voucher $voucher){
        $text = str_replace('%%VOUCHER_VALUE%%', $voucher->getValue(), $text);
        $text = str_replace('%%VOUCHER_NAME%%', $voucher->getName(), $text);
        $text = str_replace('%%VOUCHER_EXPIRES_AT%%', $voucher->getExpiresAt()->format('d-M-Y H:i'), $text);
        $text = str_replace('%%EMAILNAME%%', ($voucher->getUserDataDecoded()->getFirstName().' '.$voucher->getUserDataDecoded()->getLastName()), $text);
        $text = str_replace('%%PAYMENTINSTRUCTION%%', 'Full payment of £%%TOTALPRICE%% is required immediately to complete your booking.', $text);
        $text = str_replace('%%TOTALPRICE%%', $voucher->getTotalPrice(), $text);
        $text = str_replace('%%SELFSERVICELINK%%', $this->router->generate('utt_user_voucher_manage_auth', array(
            'voucherId' => $voucher->getId(),
            'pin' => $voucher->getPin()
        ), true), $text);

        return $text;
    }

    public function replaceWithUser($text, User $user){
        $text = str_replace('%%USER_FULL_NAME%%', $user->getFirstName().' '.$user->getLastName(), $text);
        $text = str_replace('%%USER_EMAIL%%', $user->getEmail(), $text);

        return $text;
    }

    public function replaceWithPaymentTransaction($text, PaymentTransaction $paymentTransaction){
        $text = str_replace('%%PAYMENT_AMOUNT%%', $paymentTransaction->getAmount(), $text);
        $text = str_replace('%%CHARITY_DONATION_AMOUNT%%', $paymentTransaction->getAmountCharityCharge(), $text);

        return $text;
    }

    public function replaceWithOwnerStatement($text, OwnerStatement $ownerStatement){
        $text = str_replace('%%STATEMENTMONTH%%', $ownerStatement->getMonthDate()->format('F Y'), $text);
        $text = str_replace('%%ACCOMMODATIONNAME%%', $ownerStatement->getEstate()->getName(), $text);
        $text = str_replace('%%STATEMENTLINK%%', $this->router->generate('utt_admin_owner_statement_display', array(
            'filename' => $ownerStatement->getFilename()
        ), true), $text);

        return $text;
    }

    public function replaceWithReview($text, Review $review){
        $text = str_replace('%%REVIEW_TEXT%%', $review->getComment(), $text);
        $text = str_replace('%%REVIEW_DATE%%', $review->getCreatedAt()->format('d-M-Y H:i'), $text);
        $text = str_replace('%%REVIEW_GUEST_EMAIL%%', $review->getUser()->getEmail(), $text);
        $text = str_replace('%%ACCOMMODATIONNAME%%', $review->getEstate()->getName(), $text);

        return $text;
    }

    public function replaceWithReservation($text, Reservation $reservation){
        if($reservation->getEstate()->getKeySafeCode() == "LEFT"){
            $text = str_replace('%%KEYSAFETEXT%%', 'The key will be left for you as detailed in the arrival instructions.', $text);
        }elseif ($reservation->getEstate()->getKeySafeCode() != ""){
            $text = str_replace('%%KEYSAFETEXT%%', 'This property uses a keysafe. The code is <span style="color:red; font-weight:bold; border: 2px solid red; padding:3px; font-size:16pt;">'.$reservation->getEstate()->getKeySafeCode().'</span>. You will need this code to get in. It is ESSENTIAL that you make a note of it.', $text);
        }else{
            $text = str_replace('%%KEYSAFETEXT%%', 'You need to meet someone at this property or arrange a key drop. <span style="color:red">It is important that you set your arrival time correctly in our <a href="%%SELFSERVICELINK%%">Self Service</a> section, if you are running late please do let the owner know by calling '.$reservation->getEstate()->getPhone1().'</span>. If you arrive before or after you are expected the owner may not be available to meet you.', $text);
        }

        if($this->reservationFactory->getBalanceToPay($reservation) > 0){
            $text = str_replace('%%BOOKINGSTATUSTEXT%%', 'Your booking is %%ACCOMMODATIONNAME%% from %%STARTDATE%%. You currently have an outstanding balance of %%BALANCETOPAY%%. Please quote your booking ID %%BOOKINGID%% in all correspondence with us.', $text);
        }else{
            $text = str_replace('%%BOOKINGSTATUSTEXT%%', 'Your booking starts on %%STARTDATE%%. Your account is up to date. Please quote your booking ID %%BOOKINGID%% in all correspondence with us.', $text);
        }

        if($this->reservationFactory->isFullPaymentRequired($reservation)){
            $text = str_replace('%%PAYMENTINSTRUCTION%%', 'Full payment of %%BALANCETOPAY%% is required immediately to complete your booking. Please ensure this is sent within the specified deadlines.', $text);
        }else{
            $text = str_replace('%%PAYMENTINSTRUCTION%%', 'A deposit of %%DEPOSIT%% or full payment will confirm your reservation, please pay this promptly. The final balance needs to be paid before %%BALANCEDUEDATE%%.', $text);
        }

        $text = str_replace('%%SELFSERVICELINK%%', $this->router->generate('utt_user_reservation_manage_auth', array(
            'reservationId' => $reservation->getId(),
            'pin' => $reservation->getPin()
        ), true), $text);
        $text = str_replace('%%BOOKING_REINSTATE_LINK%%', $this->router->generate('utt_user_reservation_reinstate', array(
            'reservationId' => $reservation->getId(),
            'pin' => $reservation->getPin()
        ), true), $text);
        $text = str_replace('%%BOOKINGLINK%%', $this->router->generate('utt_user_reservation_manage', array(
            'reservationId' => $reservation->getId()
        ), true), $text);

        $text = str_replace('%%BOOKINGREF%%', $reservation->getId(), $text);
        $text = str_replace('%%BOOKING_REFERRED_FLAG_NAME%%', $reservation->getReferredFlagName(), $text);
        $text = str_replace('%%BOOKINGID%%', $reservation->getId(), $text);
        $text = str_replace('%%BOOKINGADMINLINK%%', $this->router->generate('admin_utt_reservation_reservation_show', array(
            'id' => $reservation->getId()
        ), true), $text);
        $text = str_replace('%%TOTALPRICE%%', $reservation->getTotalPrice(), $text);
        $text = str_replace('%%BOOKING_USER_ADDRESS%%', $reservation->getUserDataDecoded()->getAddress(), $text);
        $text = str_replace('%%BOOKING_USER_POSTCODE%%', $reservation->getUserDataDecoded()->getPostcode(), $text);
        $text = str_replace('%%BOOKING_USER_CITY%%', $reservation->getUserDataDecoded()->getCity(), $text);
        $text = str_replace('%%BOOKING_ESTATE_ADDRESS%%', $reservation->getEstate()->getAddress(), $text);
        $text = str_replace('%%BOOKING_ESTATE_POSTCODE%%', $reservation->getEstate()->getPostcode(), $text);
        $text = str_replace('%%BALANCETOPAY%%', $this->reservationFactory->getBalanceToPay($reservation), $text);
        $text = str_replace('%%BOOKEDDATE%%', $reservation->getCreatedAt()->format('d-M-Y'), $text);
        $text = str_replace('%%STARTDATE%%', $reservation->getFromDate()->format('d-M-Y'), $text);
        $text = str_replace('%%ENDDATE%%', $reservation->getToDate()->format('d-M-Y'), $text);
        $text = str_replace('%%STATUS%%', $reservation->getStatusName(), $text);
        $text = str_replace('%%GUESTS%%', $reservation->getSleeps(), $text);
        $text = str_replace('%%EMAILNAME%%', ($reservation->getUserDataDecoded()->getFirstName().' '.$reservation->getUserDataDecoded()->getLastName()), $text);
        $text = str_replace('%%ACCOMMODATIONNAME%%', $reservation->getEstateDataDecoded()->getName(), $text);
        $text = str_replace('%%DEPOSIT%%', $reservation->getDeposit(), $text);
        $text = str_replace('%%BALANCEDUEDATE%%', $reservation->getBalanceDueDate()->format('d-M-Y'), $text);
        $text = str_replace('%%NAME%%', ($reservation->getUserDataDecoded()->getFirstName().' '.$reservation->getUserDataDecoded()->getLastName()), $text);

        // TODO
        $text = str_replace('%%ARRIVALTIME%%', $reservation->getEstate()->getCheckInTime()->format('H:i'), $text);
        $text = str_replace('%%CHECKINTIME%%', $reservation->getEstate()->getCheckInTime()->format('H:i'), $text);

        if($reservation->getEstate()->getLat() && $reservation->getEstate()->getLon() && $reservation->getEstate()->getPostcode()){
            $marker = $reservation->getEstate()->getLat().','.$reservation->getEstate()->getLon();
            $google = sprintf("http://maps.google.co.uk/maps?saddr=%s&daddr=%s", $reservation->getEstate()->getPostcode(), $marker);
            $text = str_replace("%%GOOGLEMAPS%%",$google ,$text);
        }

        $text = str_replace('%%PDFLINK%%', $this->router->generate('utt_arrival_instruction_display_by_reservation_and_pin', array(
            'reservationId' => $reservation->getId(),
            'pin' => $reservation->getPin()
        ), true), $text);

        //TODO $text = str_replace('%%BALANCEDUEDATE%%', , $text);

        return $text;
    }

    public function replaceWithDiscountCode($text, DiscountCode $discountCode){
        $text = str_replace('%%DISCOUNTEXPIRYDATE%%', $discountCode->getExpiresAt()->format('d-M-Y'), $text);
        $text = str_replace('%%DISCOUNTVOUCHERCODE%%', $discountCode->getCode(), $text);
        if($discountCode->getCodeType() == DiscountCode::CODE_TYPE_ONE_OFF_FIXED_AMOUNT_DISCOUNT || $discountCode->getCodeType() == DiscountCode::CODE_TYPE_PERSISTENT_FIXED_AMOUNT || $discountCode->getCodeType() == DiscountCode::CODE_TYPE_PAYMENT_VOUCHER){
            $text = str_replace('%%DISCOUNTVALUE%%', $discountCode->getValue().'£', $text);
        }elseif($discountCode->getCodeType() == DiscountCode::CODE_TYPE_ONE_OFF_PERCENTAGE || $discountCode->getCodeType() == DiscountCode::CODE_TYPE_PERSISTENT_PERCENTAGE){
            $text = str_replace('%%DISCOUNTVALUE%%', $discountCode->getValue().'%', $text);
        }

        $discountInstructions = '';
        if($discountCode->getHolidayMustStartBy()){
            $discountInstructions .= 'Must be used on bookings starting before '.$discountCode->getHolidayMustStartBy()->format('l jS F Y').'. ';
        }
        if($discountCode->getHolidayMustStartAt()){
            $discountInstructions .= 'Must be used on bookings starting after '.$discountCode->getHolidayMustStartAt()->format('l jS F Y').'. ';
        }

        if($discountCode->getEstates()){
            $discountInstructionsEstates = 'Can only be used at properties: ';
            /** @var Estate $estate */
            foreach($discountCode->getEstates() as $estate){
                $discountInstructionsEstates .= $estate->getName().', ';
            }
            $discountInstructions .= $discountInstructionsEstates;
        }

        $text = str_replace('%%DISCOUNTRESTRICTIONS%%', $discountInstructions, $text);

        return $text;
    }

    public function replaceWithHistoryEntry($text, HistoryEntry $historyEntry){
        $text = str_replace('%%SPECIALTEXT%%', $historyEntry->getComment(), $text);

        return $text;
    }

    public function replaceWithLoggedUser($text){
        /** @var User $user */
        $user = $this->userAuth();
        if($user){
            $text = str_replace('%%USERNAME%%', $user->getFirstName().' '.$user->getLastName(), $text);
            $text = str_replace('%%USERROLE%%', '', $text);
        }

        return $text;
    }


    private function userAuth(){
        if(!$this->securityContext->isGranted('IS_AUTHENTICATED_FULLY') ){
            return false;
        }

        $user = $this->securityContext->getToken()->getUser();
        if (!is_object($user) || !$user instanceof UserInterface) {
            return false;
        }

        return $user;
    }

    /*
%%TAG%%
%%ARRIVALINSTRUCTIONS%%
%%mar7t1n%%
%%underthethatch%%
%%lad%%
%%spot%%
%%SPECIALTEXT%%
%%LASTHISTORY%%
%%COMMENT%%
%%CHECKINTIME%%
%%ARRIVALTIME%%
%%KEYSAFETEXT%%
%%STATEMENTMONTH%%
*/
}