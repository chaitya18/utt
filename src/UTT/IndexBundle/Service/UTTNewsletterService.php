<?php

namespace UTT\IndexBundle\Service;

use Doctrine\ORM\EntityManager;

class UTTNewsletterService {
    protected $_em;
    CONST MAILCHIMP_LIST_ID = 'bf3e5ff779';
    CONST MAILCHIMP_URL = 'https://us2.api.mailchimp.com/2.0/';
    CONST MAILCHIMP_API_KEY = '6e11032d478503c76c51798f7eef97e8-us2';

    public function __construct(EntityManager $em){
        $this->_em = $em;
    }

    private function curl_download($Url){
        // is cURL installed yet?
        if (!function_exists('curl_init')){
            die('Sorry cURL is not installed!');
        }

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $Url);

        // Include header in result? (0 = yes, 1 = no)
        curl_setopt($ch, CURLOPT_HEADER, 0);

        // Should cURL return or print out the data? (true = return, false = print)
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_TIMEOUT, 10);
        $output = curl_exec($ch);
        curl_close($ch);

        return $output;
    }


    public function subscribe($email){
        $params = array(
            'apikey='.self::MAILCHIMP_API_KEY,
            'id='.self::MAILCHIMP_LIST_ID,
            'email[email]='.$email,
            'double_optin=false'
        );
        $url = self::MAILCHIMP_URL.'lists/subscribe?'.implode('&', $params);
        $response = $this->curl_download($url);
        $response = json_decode($response);

        if(isset($response->email) && isset($response->euid) && isset($response->leid) && $response->email && $response->euid && $response->leid){
            // success response
            return true;
        }elseif(isset($response->status) && $response->status == 'error' && isset($response->name)){
            if($response->name == 'List_AlreadySubscribed'){
                throw new \Exception('Your e-mail already exists on our newsletter list');
            }elseif($response->name == 'List_DoesNotExist'){
                throw new \Exception('List does not exists. Please contact to administrator.');
            }else{
                throw new \Exception('We are sorry but we cannot add your email at the moment. Please try again later.');
            }
        }else{
            throw new \Exception('We are sorry but we cannot add your email at the moment. Please try again later.');
        }
    }
}
