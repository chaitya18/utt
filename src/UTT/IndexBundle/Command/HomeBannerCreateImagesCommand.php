<?php

namespace UTT\IndexBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

use UTT\IndexBundle\Service\HomeBannerService;
use UTT\IndexBundle\Entity\HomeBanner;

class HomeBannerCreateImagesCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('homeBanner:createImages')
            ->setDescription('Create images for all homeBanners from database')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $_em = $this->getContainer()->get('doctrine')->getManager();
        $homeBanners = $_em->getRepository('UTTIndexBundle:HomeBanner')->findAll();
        if($homeBanners){
            /** @var HomeBannerService $homeBannerService */
            $homeBannerService = $this->getContainer()->get('utt.homebannerservice');

            /** @var HomeBanner $homeBanner */
            foreach($homeBanners as $homeBanner){
                $fullSrc = $homeBannerService->saveHomeBannerCacheImage($homeBanner);
                if($fullSrc){
                    $homeBanner->setFullSrc($fullSrc);
                    $_em->persist($homeBanner);
                    $_em->flush();
                }
            }

            echo 'Updated'.PHP_EOL;
        }else{
            echo 'No home banners found'.PHP_EOL;
        }
    }
}