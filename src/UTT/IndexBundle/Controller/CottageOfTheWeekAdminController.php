<?php

namespace UTT\IndexBundle\Controller;

use Sonata\AdminBundle\Controller\CRUDController as Controller;

use UTT\IndexBundle\Entity\CottageOfTheWeek;
use UTT\IndexBundle\Service\ConfigurationService;
use UTT\IndexBundle\Entity\Configuration;

class CottageOfTheWeekAdminController extends Controller
{
    public function listAction(){
        /** @var ConfigurationService $configurationService */
        $configurationService = $this->get('utt.configurationservice');
        $cottageOfTheWeek = $configurationService->getCottageOfTheWeek();
        if($cottageOfTheWeek instanceof CottageOfTheWeek){
            return $this->redirect($this->admin->generateObjectUrl('edit', $cottageOfTheWeek));
        }

        return parent::listAction();
    }
}