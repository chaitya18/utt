<?php

namespace UTT\IndexBundle\Controller;

use Sonata\AdminBundle\Controller\CRUDController as Controller;
use Symfony\Component\HttpFoundation\Request;
use UTT\IndexBundle\Entity\HomeBanner;
use UTT\IndexBundle\Entity\HomeBannerRepository;
use Doctrine\ORM\EntityManager;

class HomeBannerAdminController extends Controller
{
    public function sortAction(Request $request){
        /** @var HomeBanner $homeBanner */
        $homeBanner = $this->admin->getSubject();
        if($homeBanner){
            $this->container->get('utt.aclService')->authForHomeBannerSort();

            /** @var EntityManager $_em */
            $_em = $this->getDoctrine()->getManager();

            /** @var HomeBannerRepository $homeBannerRepository */
            $homeBannerRepository = $_em->getRepository('UTTIndexBundle:HomeBanner');
            $homeBanners = $homeBannerRepository->findAllSorted();

            if($request->isMethod('POST')){
                $homeBannerSortArray = $request->request->get('homeBannerSortArray');

                if($homeBannerSortArray){
                    /** @var HomeBanner $homeBanner */
                    foreach($homeBanners as $homeBanner){
                        foreach($homeBannerSortArray as $key => $value){
                            if($homeBanner->getId() == $value){
                                $homeBanner->setSortOrder($key);
                            }
                        }
                    }

                    $_em->flush();

                    $this->get('session')->getFlashBag()->add('success', 'Sort order changed!');

                    return $this->redirect($this->generateUrl('admin_utt_index_homebanner_sort', array(
                        'id' => $homeBanner->getId()
                    )));
                }
            }

            $twigArray = array(
                'action'   => 'show',
                'object'   => $homeBanner,
                'elements' => $this->admin->getShow(),
                // custom params
                'homeBanners' => $homeBanners
            );

            return $this->render('UTTIndexBundle:HomeBannerAdmin:sort.html.twig', $twigArray);
        }

        return $this->redirect($this->generateUrl('sonata_admin_dashboard'));
    }
}