<?php

namespace UTT\IndexBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use UTT\EstateBundle\Entity\Category;
use UTT\EstateBundle\Entity\CategoryRepository;
use UTT\IndexBundle\Entity\About;
use UTT\IndexBundle\Entity\AboutRepository;
use UTT\IndexBundle\Entity\HomeBanner;
use UTT\IndexBundle\Entity\HomeBannerRepository;
use UTT\EstateBundle\Entity\Photo;
use UTT\ReservationBundle\Entity\OfferRepository;
use UTT\IndexBundle\Entity\StaticPageRepository;
use UTT\IndexBundle\Entity\StaticPage;
use UTT\IndexBundle\Service\ConfigurationService;
use UTT\IndexBundle\Entity\Configuration;
use UTT\IndexBundle\Entity\ConfigurationPopup;
use UTT\IndexBundle\Service\UTTNewsletterService;
use Symfony\Component\HttpFoundation\Request;
use UTT\ReservationBundle\Entity\Reservation;
use UTT\ReservationBundle\Entity\ReservationRepository;
use UTT\ReservationBundle\Service\PricingService;
use Symfony\Component\HttpFoundation\Cookie;
use Symfony\Component\HttpFoundation\Response;
use UTT\ReservationBundle\Entity\CharityDonationRepository;
use UTT\ReservationBundle\Entity\PaymentTransactionRepository;
use UTT\ReservationBundle\Entity\VoucherPaymentTransactionRepository;

class IndexController extends Controller
{
    public function donationsAction(Request $request){
        /** @var CharityDonationRepository $charityDonationRepository */
        $charityDonationRepository = $this->getDoctrine()->getManager()->getRepository('UTTReservationBundle:CharityDonation');
        $charityDonations = $charityDonationRepository->findBy(array(), array('donatedAt' => 'DESC'));
        $donationsSum = $charityDonationRepository->getDonationsSum();

//        /** @var PaymentTransactionRepository $paymentTransactionRepository */
//        $paymentTransactionRepository = $this->getDoctrine()->getManager()->getRepository('UTTReservationBundle:PaymentTransaction');

//        /** @var VoucherPaymentTransactionRepository $voucherPaymentTransactionRepository */
//        $voucherPaymentTransactionRepository = $this->getDoctrine()->getManager()->getRepository('UTTReservationBundle:VoucherPaymentTransaction');

        //$charityPayments = (float) $paymentTransactionRepository->getCharityPaymentsSum();
        //$charityPayments = (float) $charityPayments + (float) $voucherPaymentTransactionRepository->getCharityPaymentsSum();

        return $this->render('UTTIndexBundle:Index:donations.html.twig', array(
            'charityDonations' => $charityDonations,
            //'charityPayments' => $charityPayments,
            'donationsSum' => $donationsSum
        ));
    }

    public function oldSystemPdfArrivalAction(Request $request){
        $reservationId = $request->query->get('bookingid');
        $externalPin = $request->query->get('pin');

        /** @var ReservationRepository $reservationRepository */
        $reservationRepository = $this->getDoctrine()->getManager()->getRepository('UTTReservationBundle:Reservation');
        /** @var Reservation $reservation */
        $reservation = $reservationRepository->findOneBy(array(
            'id' => $reservationId,
            'externalPin' => $externalPin
        ));
        if($reservation instanceof Reservation){
            return $this->redirect($this->generateUrl('utt_arrival_instruction_display_by_reservation_and_pin', array(
                'reservationId' => $reservation->getId(),
                'pin' => $reservation->getPin()
            )));
        }

        return $this->redirect($this->generateUrl('utt_index_homepage'));
    }

    public function oldSystemSelfServiceAction(Request $request){
        $reservationId = $request->query->get('bookingid');
        $reservationPin = $request->query->get('pin');

        return $this->redirect($this->generateUrl('utt_user_reservation_manage_auth', array(
            'reservationId' => $reservationId,
            'pin' => $reservationPin
        )));
    }

    public function socialMediaPluginsAction(Request $request){
        return $this->render('UTTIndexBundle:Index:socialMediaPlugins.html.twig');
    }

    public function renderNewsletterPopupAction(Request $request){
        $newsletterPopup = true;
        if($request->attributes->get('_route') == 'utt_index_newsletter_subscribe'){ $newsletterPopup = false; }
        if($request->cookies->get('newsletterPopupSeen') == true){ $newsletterPopup = false; }

        $newsletterForm = $this->get('form.factory')->createNamedBuilder('utt_newsletter_form', 'form')
            ->setAction($this->generateUrl('utt_index_newsletter_subscribe'))
            ->setMethod('post')
            ->add('email', 'text', array('required' => true, 'label' => 'Type your e-mail', 'attr' => array('class' => 'newsletter__input', 'placeholder' => 'my email')))
            ->getForm();

        return $this->render('UTTIndexBundle:Index:renderNewsletterPopup.html.twig', array(
            'newsletterPopup' => $newsletterPopup,
            'newsletterForm' => $newsletterForm->createView(),
            'currentRouteName' => $request->attributes->get('_route')
        ));
    }
    public function renderSalePopupAction(Request $request, $isPreview = false){
        $isSalePopup = false;
        $salePopupDescription = '';
        $salePopupTitle = '';
        $salePopupButtonCaption = '';
        $salePopupLink = '';
        $salePopupTimerLink = '';
        $salePopupImage = '';
        $salePopupImageUploadDir = '';

        if(!($request->cookies->get('salePopupSeen_2') == true) || $isPreview){
            /** @var ConfigurationService $configurationService */
            $configurationService = $this->get('utt.configurationservice');

            /** @var ConfigurationPopup $configurationPopup */
            $configurationPopup = $configurationService->getConfigurationPopup();
            if($configurationPopup instanceOf ConfigurationPopup){
                $expired = false;
                $salePopupExpiresAt = $configurationPopup->getSalePopupExpiresAt();
                if($salePopupExpiresAt){
                    $nowDate = new \Datetime('now');
                    if($nowDate > $salePopupExpiresAt) $expired = true;
                }

                if(($configurationPopup->getSalePopupEnabled() && !$expired) || $isPreview){
                    $isSalePopup = true;
                    $salePopupDescription = $configurationPopup->getSalePopupDescription();
                    $salePopupTitle = $configurationPopup->getSalePopupTitle();
                    $salePopupButtonCaption = $configurationPopup->getSalePopupButtonCaption();
                    $salePopupLink = $configurationPopup->getSalePopupLink();
                    $salePopupTimerLink = $configurationPopup->getSalePopupTimerLink();
                    $salePopupImage = $configurationPopup->getSalePopupImage();
                    $salePopupImageUploadDir = $configurationPopup->getUploadDir();
                }
            }
        }

        return $this->render('UTTIndexBundle:Index:renderSalePopup.html.twig', array(
            'isSalePopup' => $isSalePopup,
            'salePopupDescription' => $salePopupDescription,
            'salePopupTitle' => $salePopupTitle,
            'salePopupButtonCaption' => $salePopupButtonCaption,
            'salePopupLink' => $salePopupLink,
            'salePopupTimerLink' => $salePopupTimerLink,
            'salePopupImage' => $salePopupImage,
            'salePopupImageUploadDir' => $salePopupImageUploadDir
        ));
    }

    public function renderNewsletterSectionAction(Request $request){
        $newsletterForm = $this->get('form.factory')->createNamedBuilder('utt_newsletter_form', 'form')
            ->setAction($this->generateUrl('utt_index_newsletter_subscribe'))
            ->setMethod('post')
            ->add('email', 'text', array('required' => true, 'attr' => array('class' => 'form-control', 'placeholder' => 'Type your e-mail')))
            ->getForm();

        return $this->render('::the-project/newsletter.html.twig', array(
            'newsletterForm' => $newsletterForm->createView()
        ));
    }

    public function newsletterSubscribeAction(Request $request){
        $twigArray = array();

        /** @var UTTNewsletterService $uttNewsletterService */
        $uttNewsletterService = $this->get('utt.newsletterservice');

        $newsletterForm = $this->get('form.factory')->createNamedBuilder('utt_newsletter_form', 'form')
            ->setAction($this->generateUrl('utt_index_newsletter_subscribe'))
            ->setMethod('post')
            ->add('email', 'text', array('required' => true, 'label' => 'type your e-mail', 'attr' => array('class' => 'form-control')))
            ->getForm();

        if ($request->isMethod('POST')) {
            $newsletterForm->bind($request);
            if ($newsletterForm->isValid()) {
                $formData = $newsletterForm->getData();
                if($formData['email']){
                    $cookie = new Cookie('newsletterPopupSeen', true, time() + 3600 * 24 * 365);

                    $response = new Response();
                    $response->headers->setCookie($cookie);
                    $response->sendHeaders();

                    try{
                        $uttNewsletterService->subscribe($formData['email']);

                        $this->get('session')->getFlashBag()->add('success', 'Added to newsletter!');
                    }catch(\Exception $e){
                        $this->get('session')->getFlashBag()->add('error', $e->getMessage());
                    }
                }
            }
        }

        $twigArray['newsletterForm'] = $newsletterForm->createView();
        return $this->render('UTTIndexBundle:Index:newsletterSubscribe.html.twig', $twigArray);
    }

    public function blackFridayAction(Request $request){
        $twigArray = array();

        /** @var UTTNewsletterService $uttNewsletterService */
        $uttNewsletterService = $this->get('utt.newsletterservice');

        $newsletterForm = $this->get('form.factory')->createNamedBuilder('utt_newsletter_form', 'form')
            ->setAction($this->generateUrl('utt_index_newsletter_subscribe'))
            ->setMethod('post')
            ->add('email', 'text', array('required' => true, 'label' => 'type your e-mail', 'attr' => array('class' => 'form-control')))
            ->getForm();

        if ($request->isMethod('POST')) {
            $newsletterForm->bind($request);
            if ($newsletterForm->isValid()) {
                $formData = $newsletterForm->getData();
                if($formData['email']){
                    $cookie = new Cookie('newsletterPopupSeen', true, time() + 3600 * 24 * 365);

                    $response = new Response();
                    $response->headers->setCookie($cookie);
                    $response->sendHeaders();

                    try{
                        $uttNewsletterService->subscribe($formData['email']);

                        $this->get('session')->getFlashBag()->add('success', 'Added to newsletter!');
                    }catch(\Exception $e){
                        $this->get('session')->getFlashBag()->add('error', $e->getMessage());
                    }
                }
            }
        }

        $twigArray['newsletterForm'] = $newsletterForm->createView();
        return $this->render('UTTIndexBundle:Index:blackFriday.html.twig', $twigArray);
    }

    public function renderFooterTextAction(){
        $twigArray = array();

        /** @var ConfigurationService $configurationService */
        $configurationService = $this->get('utt.configurationservice');

        /** @var Configuration $configuration */
        $configuration = $configurationService->get();
        if($configuration instanceOf Configuration){
            $twigArray['footerText'] = $configuration->getFooterText();
        }
        return $this->render('UTTIndexBundle:Index:renderFooterText.html.twig', $twigArray);
    }

//    public function renderHomeBannerSrcAction($id){
//        $twigArray = array();
//
//        /** @var HomeBannerRepository $homeBannerRepository */
//        $homeBannerRepository = $this->getDoctrine()->getManager()->getRepository('UTTIndexBundle:HomeBanner');
//
//        /** @var HomeBanner $homeBanner */
//        $homeBanner = $homeBannerRepository->find($id);
//        if($homeBanner instanceof HomeBanner){
//            $liipImagineCacheManager = $this->container->get('liip_imagine.cache.manager');
//            $runtimeConfig = array(
//                "crop" => array(
//                    "start" => array(0, ($homeBanner->getImagePosition() * -1))
//                )
//            );
//            $src = $liipImagineCacheManager->getBrowserPath($homeBanner->getUploadDir().'/'.$homeBanner->getImage(), 'home_banner', $runtimeConfig);
//            $twigArray['src'] = $src;
//        }
//
//        return $this->render('UTTIndexBundle:Index:renderHomeBannerSrc.html.twig', $twigArray);
//    }

    public function staticPageAction($url){
        $this->get('utt.activityLogService')->createForCurrentRoute();

        $twigArray = array();

        /** @var StaticPageRepository $staticPageRepository */
        $staticPageRepository = $this->getDoctrine()->getManager()->getRepository('UTTIndexBundle:StaticPage');
        /** @var StaticPage $staticPage */
        $staticPage = $staticPageRepository->findOneBy(array(
            'url' => $url
        ));
        if(!($staticPage instanceof StaticPage)) {
            return $this->redirect($this->generateUrl('utt_index_homepage'));
        }

        $twigArray['staticPage'] = $staticPage;
        return $this->render('UTTIndexBundle:Index:staticPage.html.twig', $twigArray);
    }

    public function renderCategoriesInMenuAction(){
        $twigArray = array();

        $_em = $this->getDoctrine()->getManager();
        /** @var CategoryRepository $categoryRepository */
        $categoryRepository = $_em->getRepository('UTTEstateBundle:Category');
        $categories = $categoryRepository->getForHeaderMenuArray();
        if(is_array($categories) && count($categories) > 0){
            $twigArray['categories'] = $categories;
        }

        return $this->render('UTTIndexBundle:Index:renderCategoriesInMenu.html.twig', $twigArray);
    }

    public function renderMenuStaticPagesAction(){
        $twigArray = array();

        /** @var StaticPageRepository $staticPageRepository */
        $staticPageRepository = $this->getDoctrine()->getManager()->getRepository('UTTIndexBundle:StaticPage');
        $staticPages = $staticPageRepository->findAll();
        if($staticPages){
            $twigArray['staticPages'] = $staticPages;
        }

        return $this->render('UTTIndexBundle:Index:renderMenuStaticPages.html.twig', $twigArray);
    }

    public function renderFooterStaticPagesAction(){
        $twigArray = array();

        /** @var StaticPageRepository $staticPageRepository */
        $staticPageRepository = $this->getDoctrine()->getManager()->getRepository('UTTIndexBundle:StaticPage');
        $staticPages = $staticPageRepository->findAll();
        if($staticPages){
            $twigArray['staticPages'] = $staticPages;
        }

        return $this->render('UTTIndexBundle:Index:renderFooterStaticPages.html.twig', $twigArray);
    }

    public function indexAction(){
        $this->get('utt.activityLogService')->createForCurrentRoute();

        $twigArray = array();
        $_em = $this->getDoctrine()->getManager();

        # locations
        $locations = $_em->getRepository('UTTEstateBundle:Location')->getAllArray();
        if(is_array($locations) && count($locations) > 0){
            $twigArray['locations'] = $locations;
        }

        $categories = $this->getHomepageCategory();
        $twigArray['categories'] = $categories;

        $categoryEntity = new Category();
        $estateCategoryUploadDir = $categoryEntity->getUploadDir();
        unset($categoryEntity);

        $twigArray['estateCategoryUploadDir'] = $estateCategoryUploadDir;
        ##

        $justLaunchedEstates = $_em->getRepository('UTTEstateBundle:Estate')->getJustLaunchedArray(16);
        if(is_array($justLaunchedEstates) && count($justLaunchedEstates) > 0){
            $twigArray['justLaunchedEstates'] = $justLaunchedEstates;
        }

        $photoEntity = new Photo();
        $estatePhotoUploadDir = $photoEntity->getUploadDir();
        unset($photoEntity);
        $twigArray['estatePhotoUploadDir'] = $estatePhotoUploadDir;

        # home banners
        $homeBanners = $_em->getRepository('UTTIndexBundle:HomeBanner')->getAllArraySorted();
        if(is_array($homeBanners) && count($homeBanners) > 0){
            $twigArray['homeBanners'] = $homeBanners;
        }

        $homeBannerEntity = new HomeBanner();
        $homeBannerUploadDir = $homeBannerEntity->getUploadDir();
        unset($homeBannerEntity);

        $twigArray['homeBannerUploadDir'] = $homeBannerUploadDir;
        ##

        # abouts
        $abouts = $_em->getRepository('UTTIndexBundle:About')->getAllArray();
        if(is_array($abouts) && count($abouts) > 0){
            $twigArray['abouts'] = $abouts;
        }

        # offers

        /** @var ConfigurationService $configurationService */
        $configurationService = $this->get('utt.configurationservice');

        /** @var Configuration $configuration */
        $configuration = $configurationService->get();
        $automaticSpecialOfferMaxPrice = 0;
        if($configuration instanceof Configuration){
            $automaticSpecialOfferMaxPrice = $configuration->getAutomaticSpecialOfferMaxPrice();
        }
        /** @var OfferRepository $offerRepository */
        $offerRepository = $this->getDoctrine()->getManager()->getRepository('UTTReservationBundle:Offer');
        $offers = $offerRepository->findAvailableAtDateOffers(new \DateTime('now'), 6, false, true, $automaticSpecialOfferMaxPrice);
        $twigArray['offers'] = $offers;

        $lateAvailabilityOffers = $offerRepository->findAvailableAtDateOffers(new \DateTime('now'), 6, true, false, $automaticSpecialOfferMaxPrice);
        $twigArray['lateAvailabilityOffers'] = $lateAvailabilityOffers;

        $twigArray['allSleeps'] = $this->getAllSleeps();
        $twigArray['isFrontPage'] = true;

        $newsletterForm = $this->get('form.factory')->createNamedBuilder('utt_newsletter_form', 'form')
            ->setAction($this->generateUrl('utt_index_newsletter_subscribe'))
            ->setMethod('post')
            ->add('email', 'text', array('required' => true, 'label' => 'Type your e-mail', 'attr' => array('class' => 'form-control')))
            ->getForm();
        $twigArray['newsletterForm'] = $newsletterForm->createView();

        /** @var PricingService $pricingService */
        $pricingService = $this->get('utt.pricingservice');
        $adminCharge = $pricingService->getAdminCharge();
        $twigArray['adminCharge'] = ($adminCharge == -1 ? 0 : $adminCharge);

        return $this->render('UTTIndexBundle:Index:index.html.twig', $twigArray);
    }

    public function joinUsAction() {
        $aboutUsRepository = $this->getDoctrine()->getRepository('UTTIndexBundle:About');
        $joinUsContent = $aboutUsRepository->find(About::JOIN_US_ID);
        return $this->render('UTTIndexBundle:Index:joinUs.html.twig', array(
            'content' => $joinUsContent
        ));
    }

    public function newsletterArchiveAction()
    {
        return $this->render('UTTIndexBundle:Index:newsletterArchive.html.twig');
    }

    /**
     * @return array
     */
    protected function getHomepageCategory() {
        $entityManger = $this->getDoctrine()->getManager();
        /** @var CategoryRepository $categoryRepository */
        $categoryRepository = $entityManger->getRepository('UTTEstateBundle:Category');
        return $categoryRepository->getHomepageSorted();
    }

    /**
     * @todo this shoudl load from config
     * @return array
     */
    protected function getAllSleeps() {
        return  array(
            (object) array('id' => 2, 'name' => '2 guests'),
            (object) array('id' => 3, 'name' => '3 guests'),
            (object) array('id' => 4, 'name' => '4 guests'),
            (object) array('id' => 5, 'name' => '5 guests'),
            (object) array('id' => 6, 'name' => '6 guests'),
            (object) array('id' => 7, 'name' => '7 guests'),
            (object) array('id' => 8, 'name' => '8 guests'),
            (object) array('id' => 9, 'name' => '9 guests'),
            (object) array('id' => 10, 'name' => '10 guests'),
            (object) array('id' => 11, 'name' => '11 guests'),
            (object) array('id' => 12, 'name' => '12 guests'),
            (object) array('id' => 13, 'name' => '13 guests'),
            (object) array('id' => 14, 'name' => '14 guests'),
            (object) array('id' => 15, 'name' => '15 guests'),
            (object) array('id' => 16, 'name' => '16 guests'),
            (object) array('id' => 17, 'name' => '17 guests'),
            (object) array('id' => 18, 'name' => '18 guests'),
            (object) array('id' => 19, 'name' => '19 guests'),
            (object) array('id' => 20, 'name' => '20 guests'),
            (object) array('id' => 21, 'name' => '21 guests'),
            (object) array('id' => 22, 'name' => '22 guests'),
            (object) array('id' => 23, 'name' => '23 guests'),
            (object) array('id' => 24, 'name' => '24 guests'),
            (object) array('id' => 25, 'name' => '25 guests'),
            (object) array('id' => 26, 'name' => '26 guests'),
            (object) array('id' => 27, 'name' => '27 guests'),
            (object) array('id' => 28, 'name' => '28 guests'),
        );
    }
}
