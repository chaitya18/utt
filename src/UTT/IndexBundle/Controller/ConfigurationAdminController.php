<?php

namespace UTT\IndexBundle\Controller;

use Sonata\AdminBundle\Controller\CRUDController as Controller;

use UTT\IndexBundle\Service\ConfigurationService;
use UTT\IndexBundle\Entity\Configuration;

class ConfigurationAdminController extends Controller
{
    public function listAction(){
        /** @var ConfigurationService $configurationService */
        $configurationService = $this->get('utt.configurationservice');
        $configuration = $configurationService->get();
        if($configuration instanceof Configuration){
            return $this->redirect($this->admin->generateObjectUrl('edit', $configuration));
        }

        return parent::listAction();
    }
}