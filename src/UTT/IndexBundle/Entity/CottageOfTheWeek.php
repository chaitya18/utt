<?php

namespace UTT\IndexBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * CottageOfTheWeek
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="UTT\IndexBundle\Entity\CottageOfTheWeekRepository")
 */
class CottageOfTheWeek
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="UTT\EstateBundle\Entity\Estate")
     * @ORM\JoinColumn(name="estate_id", referencedColumnName="id", nullable=true, onDelete="SET NULL")
     */
    protected $estateOfTheWeek;

    public function __toString(){
        return '';
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set estateOfTheWeek
     *
     * @param \UTT\EstateBundle\Entity\Estate $estateOfTheWeek
     * @return CottageOfTheWeek
     */
    public function setEstateOfTheWeek(\UTT\EstateBundle\Entity\Estate $estateOfTheWeek = null)
    {
        $this->estateOfTheWeek = $estateOfTheWeek;

        return $this;
    }

    /**
     * Get estateOfTheWeek
     *
     * @return \UTT\EstateBundle\Entity\Estate 
     */
    public function getEstateOfTheWeek()
    {
        return $this->estateOfTheWeek;
    }
}
