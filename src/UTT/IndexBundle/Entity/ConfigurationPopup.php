<?php

namespace UTT\IndexBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ConfigurationPopup
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="UTT\IndexBundle\Entity\ConfigurationPopupRepository")
 */
class ConfigurationPopup
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(name="name", type="string", length=255, nullable=true)
     */
    protected $name;

##########
#######
####  Sale popup
#

    /**
     * @ORM\Column(name="sale_popup_enabled", type="boolean")
     */
    protected $salePopupEnabled = false;

    /**
     * @ORM\Column(name="sale_popup_description", type="text", nullable=true)
     */
    protected $salePopupDescription;

    /**
     * @ORM\Column(name="sale_popup_link", type="string", length=255, nullable=true)
     */
    protected $salePopupLink;

    /**
     * @ORM\Column(name="sale_popup_timer_link", type="string", length=255, nullable=true)
     */
    protected $salePopupTimerLink;

    /**
     * @ORM\Column(name="sale_popup_image", type="string", length=255, nullable=true)
     */
    protected $salePopupImage;
    public $file;

    /**
     * @ORM\Column(name="sale_popup_title", type="string", length=255, nullable=true)
     */
    protected $salePopupTitle;

    /**
     * @ORM\Column(name="sale_popup_button_caption", type="string", length=255, nullable=true)
     */
    protected $salePopupButtonCaption;

    /**
     * @ORM\Column(name="sale_popup_expires_at", type="date", nullable=true)
     */
    protected $salePopupExpiresAt;

    public function __toString(){
        if($this->getName())
            return $this->getName();
        return '';
    }

#--------------------------------------------------- ENTITY METHODS ---------------------------------------------------#

    public function getUploadDir(){
        return 'uploads/configurationPopupImages';
    }

    protected function getUploadRootDir($basepath = null){
        return $basepath.$this->getUploadDir();
    }

    public function uploadImage($basepath = null){
        if (null === $this->file) {
            return;
        }

        if (null === $basepath) {
            return;
        }
        $name =  uniqid().substr($this->file->getClientOriginalName(),-4);
        $this->file->move($this->getUploadRootDir($basepath), $name);
        $this->setSalePopupImage($name);
        $this->file = null;
    }

#################################################### ENTITY METHODS ####################################################

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return ConfigurationPopup
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set salePopupEnabled
     *
     * @param boolean $salePopupEnabled
     * @return ConfigurationPopup
     */
    public function setSalePopupEnabled($salePopupEnabled)
    {
        $this->salePopupEnabled = $salePopupEnabled;

        return $this;
    }

    /**
     * Get salePopupEnabled
     *
     * @return boolean 
     */
    public function getSalePopupEnabled()
    {
        return $this->salePopupEnabled;
    }

    /**
     * Set salePopupDescription
     *
     * @param string $salePopupDescription
     * @return ConfigurationPopup
     */
    public function setSalePopupDescription($salePopupDescription)
    {
        $this->salePopupDescription = $salePopupDescription;

        return $this;
    }

    /**
     * Get salePopupDescription
     *
     * @return string 
     */
    public function getSalePopupDescription()
    {
        return $this->salePopupDescription;
    }

    /**
     * Set salePopupLink
     *
     * @param string $salePopupLink
     * @return ConfigurationPopup
     */
    public function setSalePopupLink($salePopupLink)
    {
        $this->salePopupLink = $salePopupLink;

        return $this;
    }

    /**
     * Get salePopupLink
     *
     * @return string 
     */
    public function getSalePopupLink()
    {
        return $this->salePopupLink;
    }

    /**
     * Set salePopupTimerLink
     *
     * @param string $salePopupTimerLink
     * @return ConfigurationPopup
     */
    public function setSalePopupTimerLink($salePopupTimerLink)
    {
        $this->salePopupTimerLink = $salePopupTimerLink;

        return $this;
    }

    /**
     * Get salePopupTimerLink
     *
     * @return string 
     */
    public function getSalePopupTimerLink()
    {
        return $this->salePopupTimerLink;
    }

    /**
     * Set salePopupImage
     *
     * @param string $salePopupImage
     * @return ConfigurationPopup
     */
    public function setSalePopupImage($salePopupImage)
    {
        $this->salePopupImage = $salePopupImage;

        return $this;
    }

    /**
     * Get salePopupImage
     *
     * @return string 
     */
    public function getSalePopupImage()
    {
        return $this->salePopupImage;
    }

    /**
     * Set salePopupTitle
     *
     * @param string $salePopupTitle
     * @return ConfigurationPopup
     */
    public function setSalePopupTitle($salePopupTitle)
    {
        $this->salePopupTitle = $salePopupTitle;

        return $this;
    }

    /**
     * Get salePopupTitle
     *
     * @return string 
     */
    public function getSalePopupTitle()
    {
        return $this->salePopupTitle;
    }

    /**
     * Set salePopupExpiresAt
     *
     * @param \DateTime $salePopupExpiresAt
     * @return ConfigurationPopup
     */
    public function setSalePopupExpiresAt($salePopupExpiresAt)
    {
        $this->salePopupExpiresAt = $salePopupExpiresAt;

        return $this;
    }

    /**
     * Get salePopupExpiresAt
     *
     * @return \DateTime 
     */
    public function getSalePopupExpiresAt()
    {
        return $this->salePopupExpiresAt;
    }

    /**
     * Set salePopupButtonCaption
     *
     * @param string $salePopupButtonCaption
     * @return ConfigurationPopup
     */
    public function setSalePopupButtonCaption($salePopupButtonCaption)
    {
        $this->salePopupButtonCaption = $salePopupButtonCaption;

        return $this;
    }

    /**
     * Get salePopupButtonCaption
     *
     * @return string 
     */
    public function getSalePopupButtonCaption()
    {
        return $this->salePopupButtonCaption;
    }
}
