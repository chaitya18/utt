<?php

namespace UTT\IndexBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Email
 *
 * @ORM\Table(name="email")
 * @ORM\Entity(repositoryClass="UTT\IndexBundle\Entity\EmailRepository")
 */
class Email
{
    /* TODO
        <option value="UTTHP01"> HomePage [Area #1] </option>
     */

    const TYPE_HEADER = 1;
    const TYPE_FOOTER = 2;
    const TYPE_HISTORY = 3;
    const TYPE_NEW_BOOKING = 4;
    const TYPE_LANDLORD = 5;
    const TYPE_AUTO_CANCEL = 6;
    const TYPE_PAYMENT_DUE = 7;
    const TYPE_CANCEL_BAL_NOT_PAID = 8;
    const TYPE_ARRIVAL_DETAILS = 9;
    const TYPE_DISCOUNT_CODE_SEND = 10;
    const TYPE_THANK_YOU = 11;
    const TYPE_CHANGE_BOOKING = 12;
    const TYPE_YEAR_MAIL = 13;
    const TYPE_TWO_DAYS_VOUCHER = 14;
    const TYPE_EXTRAS_NOT_PAID = 15;
    const TYPE_AUTO_DISCOUNT = 16;
    const TYPE_ENQUIRY = 17;
    const TYPE_JOINED_MAILING_LIST = 18;
    const TYPE_AGENCY_ACCOUNTS = 19;
    const TYPE_WEB_ARRIVAL = 20;
    const TYPE_WAYS_TO_PAY = 21;
    const TYPE_RESERVATION_MANAGE_CONFIRMATION = 22;
    const TYPE_USER_WELCOME = 23;
    const TYPE_THANK_YOU_FOR_PAYMENT = 24;
    const TYPE_REFERRED_FLAG_CHANGED = 25;
    const TYPE_CHANGED_PASSWORD_INFORMATION = 26;
    const TYPE_BROKEN_STATUS_CHANGED = 27;
    const TYPE_OWNER_REVIEW_NOTIFICATION = 28;
    const TYPE_NEW_VOUCHER = 29;
    const TYPE_CHARITY_PAYMENT_THANK_YOU = 30;
    const TYPE_NO_PAYMENT_CANCELLATION_REINSTATE_EMAIL = 31;
    const TYPE_BOOKING_CANCELLED_ELERI_NOTIFICATION = 32;
    const TYPE_BEFORE_ARRIVAL_EMAIL = 33;
    const TYPE_BOOKING_PROTECT_EMAIL = 34;
    const TYPE_OWNER_BOOKINS_LIMIT_REACHED_EMAIL = 35;
    const TYPE_OWNER_BOOKINS_LIMIT_IS_GOING_TO_BE_REACHED_EMAIL = 36;
    const TYPE_LANDLORD_BOOKING_CHANGE_ESTATE = 37;


    public function getAllowedTypes(){
        $array = array(
            self::TYPE_HEADER => 'Email Common Header',
            self::TYPE_FOOTER => 'Email Common Footer',
            self::TYPE_HISTORY => 'New History Email',
            self::TYPE_NEW_BOOKING => 'New Booking Email',
            self::TYPE_LANDLORD => 'Landlord / Owner Notification',
            self::TYPE_AUTO_CANCEL => 'Auto Cancel Email No Payment',
            self::TYPE_PAYMENT_DUE => 'Balance Payment Due Reminder Email',
            self::TYPE_CANCEL_BAL_NOT_PAID => 'Auto Cancel Balance Not Paid Email',
            self::TYPE_ARRIVAL_DETAILS => 'Arrival Instructions Email',
            self::TYPE_DISCOUNT_CODE_SEND => 'Discount Code Email',
            self::TYPE_THANK_YOU => 'After sales thank you',
            self::TYPE_CHANGE_BOOKING => 'Change to a booking notification',
            self::TYPE_YEAR_MAIL => 'Wish you were back in %%ACCOMMODATIONNAME%%?',
            self::TYPE_TWO_DAYS_VOUCHER => 'Two days left to use your voucher',
            self::TYPE_EXTRAS_NOT_PAID => 'Extras not paid for Reminder',
            self::TYPE_AUTO_DISCOUNT => 'Automatic Discount Code',
            self::TYPE_ENQUIRY => 'Customer Enquiry Standard Reply',
            self::TYPE_JOINED_MAILING_LIST => 'Joined the Mailing List',
            self::TYPE_AGENCY_ACCOUNTS => 'Agency Statements',
            self::TYPE_WEB_ARRIVAL => '[WEB] Arrival Instructions',
            self::TYPE_WAYS_TO_PAY => '[WEB] Ways to Pay',
            self::TYPE_RESERVATION_MANAGE_CONFIRMATION => 'Reservation manage confirmation',
            self::TYPE_USER_WELCOME => 'User welcome e-email',
            self::TYPE_THANK_YOU_FOR_PAYMENT => 'Thank you for payment',
            self::TYPE_REFERRED_FLAG_CHANGED => 'Referred flag changed',
            self::TYPE_CHANGED_PASSWORD_INFORMATION => 'Changed password information',
            self::TYPE_BROKEN_STATUS_CHANGED => 'Broken status changed',
            self::TYPE_OWNER_REVIEW_NOTIFICATION => 'Owner review notification',
            self::TYPE_NEW_VOUCHER => 'New Voucher Email',
            self::TYPE_CHARITY_PAYMENT_THANK_YOU => 'Charity donation - thank you email',
            self::TYPE_NO_PAYMENT_CANCELLATION_REINSTATE_EMAIL => 'No payment cancellation reinstate email',
            self::TYPE_BOOKING_CANCELLED_ELERI_NOTIFICATION => 'Booking cancelled - Eleri notification',
            self::TYPE_BEFORE_ARRIVAL_EMAIL => 'Before arrival email',
            self::TYPE_BOOKING_PROTECT_EMAIL => 'Booking protect email',
            self::TYPE_OWNER_BOOKINS_LIMIT_REACHED_EMAIL => 'Owner bookings limit reached email',
            self::TYPE_OWNER_BOOKINS_LIMIT_IS_GOING_TO_BE_REACHED_EMAIL => 'Owner bookings limit is going to be reached email',
            self::TYPE_LANDLORD_BOOKING_CHANGE_ESTATE => 'Landlord / Owner Notification transfer booking',
        );
        return $array;
    }

    public function getTypeName($typeId = null){
        $array = $this->getAllowedTypes();
        if(is_null($typeId)){
            if($this->getType()) {
                return array_key_exists($this->getType(), $array) ? $array[$this->getType()] : false;
            }else{
                return false;
            }
        }
        return array_key_exists($typeId, $array) ? $array[$typeId] : false;
    }

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(name="type", type="integer", nullable=true)
     */
    protected $type;

    /**
     * @ORM\Column(name="subject", type="string", length=255, nullable=true)
     */
    protected $subject;

    /**
     * @ORM\Column(name="reply_to", type="string", length=255, nullable=true)
     */
    protected $replyTo;

    /**
     * @ORM\Column(name="content", type="text", nullable=true)
     */
    protected $content;

    public function __toString(){
        if($this->getType())
            return $this->getTypeName();
        return '';
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set type
     *
     * @param integer $type
     * @return Email
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return integer 
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set subject
     *
     * @param string $subject
     * @return Email
     */
    public function setSubject($subject)
    {
        $this->subject = $subject;

        return $this;
    }

    /**
     * Get subject
     *
     * @return string 
     */
    public function getSubject()
    {
        return $this->subject;
    }

    /**
     * Set replyTo
     *
     * @param string $replyTo
     * @return Email
     */
    public function setReplyTo($replyTo)
    {
        $this->replyTo = $replyTo;

        return $this;
    }

    /**
     * Get replyTo
     *
     * @return string 
     */
    public function getReplyTo()
    {
        return $this->replyTo;
    }

    /**
     * Set content
     *
     * @param string $content
     * @return Email
     */
    public function setContent($content)
    {
        $this->content = $content;

        return $this;
    }

    /**
     * Get content
     *
     * @return string 
     */
    public function getContent()
    {
        return $this->content;
    }
}
