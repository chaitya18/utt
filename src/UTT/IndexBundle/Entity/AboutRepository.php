<?php

namespace UTT\IndexBundle\Entity;

use Doctrine\ORM\EntityRepository;

class AboutRepository extends EntityRepository
{
    public function getAllArray(){
        $query = $this->_em->createQuery("SELECT ab FROM UTTIndexBundle:About ab ORDER BY ab.id DESC");
        return $query->getArrayResult();
    }

}
