<?php

namespace UTT\IndexBundle\Entity;

use Doctrine\ORM\EntityRepository;

class HomeBannerRepository extends EntityRepository
{
    public function getAllArraySorted(){
        $query = $this->_em->createQuery("SELECT hb FROM UTTIndexBundle:HomeBanner hb ORDER BY hb.sortOrder ASC");
        return $query->getArrayResult();
    }

    public function findAllSorted(){
        $query = $this->_em->createQuery("SELECT hb FROM UTTIndexBundle:HomeBanner hb ORDER BY hb.sortOrder ASC");
        return $query->getResult();
    }
}
