<?php

namespace UTT\IndexBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * HomeBanner
 *
 * @ORM\Table(name="home_banner")
 * @ORM\Entity(repositoryClass="UTT\IndexBundle\Entity\HomeBannerRepository")
 */
class HomeBanner
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(name="title", type="string", length=255, nullable=true)
     */
    protected $title;

    /**
     * @ORM\Column(name="font_color", type="string", length=255, nullable=true)
     */
    protected $fontColor;

    /**
     * @ORM\Column(name="url", type="string", length=255, nullable=true)
     */
    protected $url;

    /**
     * @ORM\Column(name="image", type="string", length=255, nullable=true)
     */
    protected $image;
    public $file;

    /**
     * @ORM\Column(name="image_position", type="integer")
     */
    protected $imagePosition;

    /**
     * @ORM\Column(name="full_src", type="string", length=255, nullable=true)
     */
    protected $fullSrc;

    /**
     * @ORM\Column(name="sort_order", type="integer", nullable=true)
     */
    protected $sortOrder;

#--------------------------------------------------- ENTITY METHODS ---------------------------------------------------#

    public function __construct(){
        $this->setImagePosition(0);
        $this->setFontColor('#000000');
    }

    public function __toString(){
        if($this->getTitle())
            return $this->getTitle();
        return '';
    }

    public function getUploadDir(){
        return 'uploads/homeBannerImages';
    }

    protected function getUploadRootDir($basepath = null){
        return $basepath.$this->getUploadDir();
    }

    public function uploadImage($basepath = null){
        if (null === $this->file) {
            return;
        }

        if (null === $basepath) {
            return;
        }
        $name =  uniqid().substr($this->file->getClientOriginalName(),-4);
        $this->file->move($this->getUploadRootDir($basepath), $name);
        $this->setImage($name);
        $this->file = null;
    }

#################################################### ENTITY METHODS ####################################################

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     * @return HomeBanner
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string 
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set fontColor
     *
     * @param string $fontColor
     * @return HomeBanner
     */
    public function setFontColor($fontColor)
    {
        $this->fontColor = $fontColor;

        return $this;
    }

    /**
     * Get fontColor
     *
     * @return string 
     */
    public function getFontColor()
    {
        return $this->fontColor;
    }

    /**
     * Set image
     *
     * @param string $image
     * @return HomeBanner
     */
    public function setImage($image)
    {
        $this->image = $image;

        return $this;
    }

    /**
     * Get image
     *
     * @return string 
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * Set imagePosition
     *
     * @param integer $imagePosition
     * @return HomeBanner
     */
    public function setImagePosition($imagePosition)
    {
        $this->imagePosition = $imagePosition;

        return $this;
    }

    /**
     * Get imagePosition
     *
     * @return integer 
     */
    public function getImagePosition()
    {
        return $this->imagePosition;
    }

    /**
     * Set url
     *
     * @param string $url
     * @return HomeBanner
     */
    public function setUrl($url)
    {
        $this->url = $url;

        return $this;
    }

    /**
     * Get url
     *
     * @return string 
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * Set fullSrc
     *
     * @param string $fullSrc
     * @return HomeBanner
     */
    public function setFullSrc($fullSrc)
    {
        $this->fullSrc = $fullSrc;

        return $this;
    }

    /**
     * Get fullSrc
     *
     * @return string 
     */
    public function getFullSrc()
    {
        return $this->fullSrc;
    }

    /**
     * Set sortOrder
     *
     * @param integer $sortOrder
     * @return HomeBanner
     */
    public function setSortOrder($sortOrder)
    {
        $this->sortOrder = $sortOrder;

        return $this;
    }

    /**
     * Get sortOrder
     *
     * @return integer 
     */
    public function getSortOrder()
    {
        return $this->sortOrder;
    }
}
