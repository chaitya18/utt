<?php

namespace UTT\IndexBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Configuration
 *
 * @ORM\Table(name="configuration")
 * @ORM\Entity(repositoryClass="UTT\IndexBundle\Entity\ConfigurationRepository")
 */
class Configuration
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(name="name", type="string", length=255, nullable=true)
     */
    protected $name;

    /**
     * @ORM\Column(name="additional_sleeps_percent_increase", type="decimal", scale=2, nullable=true)
     */
    protected $additionalSleepsPercentIncrease;

    /**
     * @ORM\Column(name="minimum_nights_for_flexible", type="integer")
     */
    protected $minimumNightsForFlexible;

    /**
     * @ORM\Column(name="maximum_nights_for_flexible", type="integer")
     */
    protected $maximumNightsForFlexible;

    /**
     * @ORM\Column(name="pet_price_first", type="decimal", scale=2)
     */
    protected $petPriceFirst;

    /**
     * @ORM\Column(name="pet_price_additional", type="decimal", scale=2)
     */
    protected $petPriceAdditional;

##########
#######
####  Company details
#
    /**
     * @ORM\Column(name="company_name", type="string", length=255, nullable=true)
     */
    protected $companyName;

    /**
     * @ORM\Column(name="company_short_code", type="string", length=255, nullable=true)
     */
    protected $companyShortCode;

    /**
     * @ORM\Column(name="base_commission", type="integer")
     */
    protected $baseCommission;

    /**
     * @ORM\Column(name="company_email", type="string", length=255, nullable=true)
     */
    protected $companyEmail;

    /**
     * @ORM\Column(name="company_email_reply", type="string", length=255, nullable=true)
     */
    protected $companyEmailReply;

    /**
     * @ORM\Column(name="company_phone", type="string", length=255, nullable=true)
     */
    protected $companyPhone;

    /**
     * @ORM\Column(name="company_address", type="string", length=255, nullable=true)
     */
    protected $companyAddress;

    /**
     * @ORM\Column(name="company_cheque_to", type="string", length=255, nullable=true)
     */
    protected $companyChequeTo;

    /**
     * @ORM\Column(name="company_vat_no", type="string", length=255, nullable=true)
     */
    protected $companyVatNo;

##########
#######
####  Booking options
#

    /**
     * @ORM\Column(name="months_in_calendar", type="integer")
     */
    protected $monthsInCalendar;

    /**
     * @ORM\Column(name="cheque_required", type="integer")
     */
    protected $chequeRequired;

    /**
     * @ORM\Column(name="full_payment_limit", type="integer")
     */
    protected $fullPaymentLimit;

    /**
     * @ORM\Column(name="balance_due_limit", type="integer")
     */
    protected $balanceDueLimit;

    /**
     * @ORM\Column(name="deposit_percentage", type="integer")
     */
    protected $depositPercentage;

    /**
     * @ORM\Column(name="credit_card_surcharge", type="integer")
     */
    protected $creditCardSurcharge;

    /**
     * @ORM\Column(name="no_refund_limit", type="integer")
     */
    protected $noRefundLimit;

    /**
     * @ORM\Column(name="admin_fee", type="integer")
     */
    protected $adminFee;

    /**
     * @ORM\Column(name="odd_night_multiplier", type="integer")
     */
    protected $oddNightMultiplier;

    /**
     * @ORM\Column(name="auto_cancel_option", type="integer")
     */
    protected $autoCancelOption;

    /**
     * @ORM\Column(name="payment_deadline", type="integer")
     */
    protected $paymentDeadline;

##########
#######
####  Payment options - SAGEPAY
#

    /**
     * @ORM\Column(name="sagepay_id", type="string", length=255, nullable=true)
     */
    protected $sagepayId;

    /**
     * @ORM\Column(name="sagepay_key", type="string", length=255, nullable=true)
     */
    protected $sagepayKey;

    /**
     * @ORM\Column(name="sagepay_simulator_mode", type="boolean")
     */
    protected $sagepaySimulatorMode = false;

##########
#######
####  Payment options - SmartPay
#

    /**
     * @ORM\Column(name="smart_pay_simulator_mode", type="boolean")
     */
    protected $smartPaySimulatorMode = false;

    /**
     * @ORM\Column(name="smart_pay_skin_code_reservation", type="string", length=255)
     */
    protected $smartPaySkinCodeReservation;

    /**
     * @ORM\Column(name="smart_pay_skin_code_voucher", type="string", length=255)
     */
    protected $smartPaySkinCodeVoucher;

    /**
     * @ORM\Column(name="smart_pay_merchant_account", type="string", length=255)
     */
    protected $smartPayMerchantAccount;

    /**
     * @ORM\Column(name="smart_pay_shared_secret", type="string", length=255)
     */
    protected $smartPaySharedSecret;

##########
#######
####  Website static configuration
#
    /**
     * @ORM\Column(name="footer_text", type="text", nullable=true)
     */
    protected $footerText;

##########
#######
####  Referred flag emails configuration
#

    /**
     * @ORM\Column(name="referred_flag_email_admin", type="string", length=255, nullable=true)
     */
    protected $referredFlagEmailAdmin;

    /**
     * @ORM\Column(name="referred_flag_email_lukasz_rak", type="string", length=255, nullable=true)
     */
    protected $referredFlagEmailLukaszRak;

    /**
     * @ORM\Column(name="referred_flag_email_spot", type="string", length=255, nullable=true)
     */
    protected $referredFlagEmailSpot;

    /**
     * @ORM\Column(name="referred_flag_email_greg", type="string", length=255, nullable=true)
     */
    protected $referredFlagEmailGreg;

    /**
     * @ORM\Column(name="referred_flag_email_eleri", type="string", length=255, nullable=true)
     */
    protected $referredFlagEmailEleri;

    /**
     * @ORM\Column(name="referred_flag_email_yvonne", type="string", length=255, nullable=true)
     */
    protected $referredFlagEmailYvonne;

    /**
     * @ORM\Column(name="referred_flag_email_bethan", type="string", length=255, nullable=true)
     */
    protected $referredFlagEmailBethan;

    /**
     * @ORM\Column(name="referred_flag_email_katie", type="string", length=255, nullable=true)
     */
    protected $referredFlagEmailKatie;

##########
#######
####  Automatic special offers configuration
#

    /**
     * @ORM\Column(name="automatic_special_offer_max_price", type="integer")
     */
    protected $automaticSpecialOfferMaxPrice;

    /**
     * @ORM\Column(name="automatic_special_offer_months", type="integer")
     */
    protected $automaticSpecialOfferMonths;

##########
#######
####  Analytics configuration
#

    /**
     * @ORM\Column(name="analytics_points_limit", type="integer")
     */
    protected $analyticsPointsLimit;

    /**
     * @ORM\Column(name="analytics_points_for_make_reservation_page", type="integer")
     */
    protected $analyticsPointsForMakeReservationPage;

    /**
     * @ORM\Column(name="analytics_points_for_estate_page", type="integer")
     */
    protected $analyticsPointsForEstatePage;

##########
#######
####  Changed password information email configuration
#

    /**
     * @ORM\Column(name="changed_password_information_email", type="string", length=255, nullable=true)
     */
    protected $changedPasswordInformationEmail;

    /**
     * @ORM\ManyToOne(targetEntity="UTT\EstateBundle\Entity\Estate")
     * @ORM\JoinColumn(name="estate_id", referencedColumnName="id", nullable=true, onDelete="SET NULL")
     */
    protected $estateOfTheWeek;

##########
#######
####  Voucher offer price
#

    /**
     * @ORM\Column(name="voucher_value_50_offer_price", type="decimal", scale=2)
     */
    protected $voucherValue50OfferPrice;

    /**
     * @ORM\Column(name="voucher_value_100_offer_price", type="decimal", scale=2)
     */
    protected $voucherValue100OfferPrice;

    /**
     * @ORM\Column(name="voucher_value_150_offer_price", type="decimal", scale=2)
     */
    protected $voucherValue150OfferPrice;

    /**
     * @ORM\Column(name="voucher_value_200_offer_price", type="decimal", scale=2)
     */
    protected $voucherValue200OfferPrice;

    /**
     * @ORM\Column(name="voucher_value_229_offer_price", type="decimal", scale=2)
     */
    protected $voucherValue229OfferPrice;

    /**
     * @ORM\Column(name="voucher_value_250_offer_price", type="decimal", scale=2)
     */
    protected $voucherValue250OfferPrice;

    /**
     * @ORM\Column(name="voucher_value_300_offer_price", type="decimal", scale=2)
     */
    protected $voucherValue300OfferPrice;

    /**
     * @ORM\Column(name="voucher_value_500_offer_price", type="decimal", scale=2)
     */
    protected $voucherValue500OfferPrice;

    /**
     * @ORM\Column(name="voucher_value_1000_offer_price", type="decimal", scale=2)
     */
    protected $voucherValue1000OfferPrice;

##########
#######
####  Global discount options
#

    /**
     * @ORM\Column(name="discount_period", type="integer", nullable=true)
     */
    protected $discountPeriod;

    /**
     * @ORM\Column(name="discount_value", type="decimal", scale=2, nullable=true)
     */
    protected $discountValue;

    public function __toString(){
        if($this->getName())
            return $this->getName();
        return '';
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set additionalSleepsPercentIncrease
     *
     * @param string $additionalSleepsPercentIncrease
     * @return Configuration
     */
    public function setAdditionalSleepsPercentIncrease($additionalSleepsPercentIncrease)
    {
        $this->additionalSleepsPercentIncrease = $additionalSleepsPercentIncrease;

        return $this;
    }

    /**
     * Get additionalSleepsPercentIncrease
     *
     * @return string 
     */
    public function getAdditionalSleepsPercentIncrease()
    {
        return $this->additionalSleepsPercentIncrease;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Configuration
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set minimumNightsForFlexible
     *
     * @param integer $minimumNightsForFlexible
     * @return Configuration
     */
    public function setMinimumNightsForFlexible($minimumNightsForFlexible)
    {
        $this->minimumNightsForFlexible = $minimumNightsForFlexible;

        return $this;
    }

    /**
     * Get minimumNightsForFlexible
     *
     * @return integer 
     */
    public function getMinimumNightsForFlexible()
    {
        return $this->minimumNightsForFlexible;
    }

    /**
     * Set maximumNightsForFlexible
     *
     * @param integer $maximumNightsForFlexible
     * @return Configuration
     */
    public function setMaximumNightsForFlexible($maximumNightsForFlexible)
    {
        $this->maximumNightsForFlexible = $maximumNightsForFlexible;

        return $this;
    }

    /**
     * Get maximumNightsForFlexible
     *
     * @return integer 
     */
    public function getMaximumNightsForFlexible()
    {
        return $this->maximumNightsForFlexible;
    }

    /**
     * Set petPriceFirst
     *
     * @param string $petPriceFirst
     * @return Configuration
     */
    public function setPetPriceFirst($petPriceFirst)
    {
        $this->petPriceFirst = $petPriceFirst;

        return $this;
    }

    /**
     * Get petPriceFirst
     *
     * @return string 
     */
    public function getPetPriceFirst()
    {
        return $this->petPriceFirst;
    }

    /**
     * Set petPriceAdditional
     *
     * @param string $petPriceAdditional
     * @return Configuration
     */
    public function setPetPriceAdditional($petPriceAdditional)
    {
        $this->petPriceAdditional = $petPriceAdditional;

        return $this;
    }

    /**
     * Get petPriceAdditional
     *
     * @return string 
     */
    public function getPetPriceAdditional()
    {
        return $this->petPriceAdditional;
    }

    /**
     * Set companyName
     *
     * @param string $companyName
     * @return Configuration
     */
    public function setCompanyName($companyName)
    {
        $this->companyName = $companyName;

        return $this;
    }

    /**
     * Get companyName
     *
     * @return string 
     */
    public function getCompanyName()
    {
        return $this->companyName;
    }

    /**
     * Set companyShortCode
     *
     * @param string $companyShortCode
     * @return Configuration
     */
    public function setCompanyShortCode($companyShortCode)
    {
        $this->companyShortCode = $companyShortCode;

        return $this;
    }

    /**
     * Get companyShortCode
     *
     * @return string 
     */
    public function getCompanyShortCode()
    {
        return $this->companyShortCode;
    }

    /**
     * Set baseCommission
     *
     * @param integer $baseCommission
     * @return Configuration
     */
    public function setBaseCommission($baseCommission)
    {
        $this->baseCommission = $baseCommission;

        return $this;
    }

    /**
     * Get baseCommission
     *
     * @return integer 
     */
    public function getBaseCommission()
    {
        return $this->baseCommission;
    }

    /**
     * Set companyEmail
     *
     * @param string $companyEmail
     * @return Configuration
     */
    public function setCompanyEmail($companyEmail)
    {
        $this->companyEmail = $companyEmail;

        return $this;
    }

    /**
     * Get companyEmail
     *
     * @return string 
     */
    public function getCompanyEmail()
    {
        return $this->companyEmail;
    }

    /**
     * Set companyEmailReply
     *
     * @param string $companyEmailReply
     * @return Configuration
     */
    public function setCompanyEmailReply($companyEmailReply)
    {
        $this->companyEmailReply = $companyEmailReply;

        return $this;
    }

    /**
     * Get companyEmailReply
     *
     * @return string 
     */
    public function getCompanyEmailReply()
    {
        return $this->companyEmailReply;
    }

    /**
     * Set companyPhone
     *
     * @param string $companyPhone
     * @return Configuration
     */
    public function setCompanyPhone($companyPhone)
    {
        $this->companyPhone = $companyPhone;

        return $this;
    }

    /**
     * Get companyPhone
     *
     * @return string 
     */
    public function getCompanyPhone()
    {
        return $this->companyPhone;
    }

    /**
     * Set companyAddress
     *
     * @param string $companyAddress
     * @return Configuration
     */
    public function setCompanyAddress($companyAddress)
    {
        $this->companyAddress = $companyAddress;

        return $this;
    }

    /**
     * Get companyAddress
     *
     * @return string 
     */
    public function getCompanyAddress()
    {
        return $this->companyAddress;
    }

    /**
     * Set companyChequeTo
     *
     * @param string $companyChequeTo
     * @return Configuration
     */
    public function setCompanyChequeTo($companyChequeTo)
    {
        $this->companyChequeTo = $companyChequeTo;

        return $this;
    }

    /**
     * Get companyChequeTo
     *
     * @return string 
     */
    public function getCompanyChequeTo()
    {
        return $this->companyChequeTo;
    }

    /**
     * Set companyVatNo
     *
     * @param string $companyVatNo
     * @return Configuration
     */
    public function setCompanyVatNo($companyVatNo)
    {
        $this->companyVatNo = $companyVatNo;

        return $this;
    }

    /**
     * Get companyVatNo
     *
     * @return string 
     */
    public function getCompanyVatNo()
    {
        return $this->companyVatNo;
    }

    /**
     * Set monthsInCalendar
     *
     * @param integer $monthsInCalendar
     * @return Configuration
     */
    public function setMonthsInCalendar($monthsInCalendar)
    {
        $this->monthsInCalendar = $monthsInCalendar;

        return $this;
    }

    /**
     * Get monthsInCalendar
     *
     * @return integer 
     */
    public function getMonthsInCalendar()
    {
        return $this->monthsInCalendar;
    }

    /**
     * Set chequeRequired
     *
     * @param integer $chequeRequired
     * @return Configuration
     */
    public function setChequeRequired($chequeRequired)
    {
        $this->chequeRequired = $chequeRequired;

        return $this;
    }

    /**
     * Get chequeRequired
     *
     * @return integer 
     */
    public function getChequeRequired()
    {
        return $this->chequeRequired;
    }

    /**
     * Set fullPaymentLimit
     *
     * @param integer $fullPaymentLimit
     * @return Configuration
     */
    public function setFullPaymentLimit($fullPaymentLimit)
    {
        $this->fullPaymentLimit = $fullPaymentLimit;

        return $this;
    }

    /**
     * Get fullPaymentLimit
     *
     * @return integer 
     */
    public function getFullPaymentLimit()
    {
        return $this->fullPaymentLimit;
    }

    /**
     * Set balanceDueLimit
     *
     * @param integer $balanceDueLimit
     * @return Configuration
     */
    public function setBalanceDueLimit($balanceDueLimit)
    {
        $this->balanceDueLimit = $balanceDueLimit;

        return $this;
    }

    /**
     * Get balanceDueLimit
     *
     * @return integer 
     */
    public function getBalanceDueLimit()
    {
        return $this->balanceDueLimit;
    }

    /**
     * Set depositPercentage
     *
     * @param integer $depositPercentage
     * @return Configuration
     */
    public function setDepositPercentage($depositPercentage)
    {
        $this->depositPercentage = $depositPercentage;

        return $this;
    }

    /**
     * Get depositPercentage
     *
     * @return integer 
     */
    public function getDepositPercentage()
    {
        return $this->depositPercentage;
    }

    /**
     * Set creditCardSurcharge
     *
     * @param integer $creditCardSurcharge
     * @return Configuration
     */
    public function setCreditCardSurcharge($creditCardSurcharge)
    {
        $this->creditCardSurcharge = $creditCardSurcharge;

        return $this;
    }

    /**
     * Get creditCardSurcharge
     *
     * @return integer 
     */
    public function getCreditCardSurcharge()
    {
        return $this->creditCardSurcharge;
    }

    /**
     * Set noRefundLimit
     *
     * @param integer $noRefundLimit
     * @return Configuration
     */
    public function setNoRefundLimit($noRefundLimit)
    {
        $this->noRefundLimit = $noRefundLimit;

        return $this;
    }

    /**
     * Get noRefundLimit
     *
     * @return integer 
     */
    public function getNoRefundLimit()
    {
        return $this->noRefundLimit;
    }

    /**
     * Set adminFee
     *
     * @param integer $adminFee
     * @return Configuration
     */
    public function setAdminFee($adminFee)
    {
        $this->adminFee = $adminFee;

        return $this;
    }

    /**
     * Get adminFee
     *
     * @return integer 
     */
    public function getAdminFee()
    {
        return $this->adminFee;
    }

    /**
     * Set oddNightMultiplier
     *
     * @param integer $oddNightMultiplier
     * @return Configuration
     */
    public function setOddNightMultiplier($oddNightMultiplier)
    {
        $this->oddNightMultiplier = $oddNightMultiplier;

        return $this;
    }

    /**
     * Get oddNightMultiplier
     *
     * @return integer 
     */
    public function getOddNightMultiplier()
    {
        return $this->oddNightMultiplier;
    }

    /**
     * Set autoCancelOption
     *
     * @param integer $autoCancelOption
     * @return Configuration
     */
    public function setAutoCancelOption($autoCancelOption)
    {
        $this->autoCancelOption = $autoCancelOption;

        return $this;
    }

    /**
     * Get autoCancelOption
     *
     * @return integer 
     */
    public function getAutoCancelOption()
    {
        return $this->autoCancelOption;
    }

    /**
     * Set paymentDeadline
     *
     * @param integer $paymentDeadline
     * @return Configuration
     */
    public function setPaymentDeadline($paymentDeadline)
    {
        $this->paymentDeadline = $paymentDeadline;

        return $this;
    }

    /**
     * Get paymentDeadline
     *
     * @return integer 
     */
    public function getPaymentDeadline()
    {
        return $this->paymentDeadline;
    }

    /**
     * Set sagepayId
     *
     * @param string $sagepayId
     * @return Configuration
     */
    public function setSagepayId($sagepayId)
    {
        $this->sagepayId = $sagepayId;

        return $this;
    }

    /**
     * Get sagepayId
     *
     * @return string 
     */
    public function getSagepayId()
    {
        return $this->sagepayId;
    }

    /**
     * Set sagepayKey
     *
     * @param string $sagepayKey
     * @return Configuration
     */
    public function setSagepayKey($sagepayKey)
    {
        $this->sagepayKey = $sagepayKey;

        return $this;
    }

    /**
     * Get sagepayKey
     *
     * @return string 
     */
    public function getSagepayKey()
    {
        return $this->sagepayKey;
    }

    /**
     * Set sagepaySimulatorMode
     *
     * @param boolean $sagepaySimulatorMode
     * @return Configuration
     */
    public function setSagepaySimulatorMode($sagepaySimulatorMode)
    {
        $this->sagepaySimulatorMode = $sagepaySimulatorMode;

        return $this;
    }

    /**
     * Get sagepaySimulatorMode
     *
     * @return boolean 
     */
    public function getSagepaySimulatorMode()
    {
        return $this->sagepaySimulatorMode;
    }

    /**
     * Set footerText
     *
     * @param string $footerText
     * @return Configuration
     */
    public function setFooterText($footerText)
    {
        $this->footerText = $footerText;

        return $this;
    }

    /**
     * Get footerText
     *
     * @return string 
     */
    public function getFooterText()
    {
        return $this->footerText;
    }

    /**
     * Set smartPaySimulatorMode
     *
     * @param boolean $smartPaySimulatorMode
     * @return Configuration
     */
    public function setSmartPaySimulatorMode($smartPaySimulatorMode)
    {
        $this->smartPaySimulatorMode = $smartPaySimulatorMode;

        return $this;
    }

    /**
     * Get smartPaySimulatorMode
     *
     * @return boolean 
     */
    public function getSmartPaySimulatorMode()
    {
        return $this->smartPaySimulatorMode;
    }

    /**
     * Set smartPayMerchantAccount
     *
     * @param string $smartPayMerchantAccount
     * @return Configuration
     */
    public function setSmartPayMerchantAccount($smartPayMerchantAccount)
    {
        $this->smartPayMerchantAccount = $smartPayMerchantAccount;

        return $this;
    }

    /**
     * Get smartPayMerchantAccount
     *
     * @return string 
     */
    public function getSmartPayMerchantAccount()
    {
        return $this->smartPayMerchantAccount;
    }

    /**
     * Set smartPaySharedSecret
     *
     * @param string $smartPaySharedSecret
     * @return Configuration
     */
    public function setSmartPaySharedSecret($smartPaySharedSecret)
    {
        $this->smartPaySharedSecret = $smartPaySharedSecret;

        return $this;
    }

    /**
     * Get smartPaySharedSecret
     *
     * @return string 
     */
    public function getSmartPaySharedSecret()
    {
        return $this->smartPaySharedSecret;
    }

    /**
     * Set referredFlagEmailAdmin
     *
     * @param string $referredFlagEmailAdmin
     * @return Configuration
     */
    public function setReferredFlagEmailAdmin($referredFlagEmailAdmin)
    {
        $this->referredFlagEmailAdmin = $referredFlagEmailAdmin;

        return $this;
    }

    /**
     * Get referredFlagEmailAdmin
     *
     * @return string 
     */
    public function getReferredFlagEmailAdmin()
    {
        return $this->referredFlagEmailAdmin;
    }

    /**
     * Set referredFlagEmailLukaszRak
     *
     * @param string $referredFlagEmailLukaszRak
     * @return Configuration
     */
    public function setReferredFlagEmailLukaszRak($referredFlagEmailLukaszRak)
    {
        $this->referredFlagEmailLukaszRak = $referredFlagEmailLukaszRak;

        return $this;
    }

    /**
     * Get referredFlagEmailLukaszRak
     *
     * @return string 
     */
    public function getReferredFlagEmailLukaszRak()
    {
        return $this->referredFlagEmailLukaszRak;
    }

    /**
     * Set referredFlagEmailSpot
     *
     * @param string $referredFlagEmailSpot
     * @return Configuration
     */
    public function setReferredFlagEmailSpot($referredFlagEmailSpot)
    {
        $this->referredFlagEmailSpot = $referredFlagEmailSpot;

        return $this;
    }

    /**
     * Get referredFlagEmailSpot
     *
     * @return string 
     */
    public function getReferredFlagEmailSpot()
    {
        return $this->referredFlagEmailSpot;
    }

    /**
     * Set referredFlagEmailGreg
     *
     * @param string $referredFlagEmailGreg
     * @return Configuration
     */
    public function setReferredFlagEmailGreg($referredFlagEmailGreg)
    {
        $this->referredFlagEmailGreg = $referredFlagEmailGreg;

        return $this;
    }

    /**
     * Get referredFlagEmailGreg
     *
     * @return string 
     */
    public function getReferredFlagEmailGreg()
    {
        return $this->referredFlagEmailGreg;
    }

    /**
     * Set referredFlagEmailEleri
     *
     * @param string $referredFlagEmailEleri
     * @return Configuration
     */
    public function setReferredFlagEmailEleri($referredFlagEmailEleri)
    {
        $this->referredFlagEmailEleri = $referredFlagEmailEleri;

        return $this;
    }

    /**
     * Get referredFlagEmailEleri
     *
     * @return string 
     */
    public function getReferredFlagEmailEleri()
    {
        return $this->referredFlagEmailEleri;
    }

    /**
     * Set automaticSpecialOfferMaxPrice
     *
     * @param integer $automaticSpecialOfferMaxPrice
     * @return Configuration
     */
    public function setAutomaticSpecialOfferMaxPrice($automaticSpecialOfferMaxPrice)
    {
        $this->automaticSpecialOfferMaxPrice = $automaticSpecialOfferMaxPrice;

        return $this;
    }

    /**
     * Get automaticSpecialOfferMaxPrice
     *
     * @return integer 
     */
    public function getAutomaticSpecialOfferMaxPrice()
    {
        return $this->automaticSpecialOfferMaxPrice;
    }

    /**
     * Set automaticSpecialOfferMonths
     *
     * @param integer $automaticSpecialOfferMonths
     * @return Configuration
     */
    public function setAutomaticSpecialOfferMonths($automaticSpecialOfferMonths)
    {
        $this->automaticSpecialOfferMonths = $automaticSpecialOfferMonths;

        return $this;
    }

    /**
     * Get automaticSpecialOfferMonths
     *
     * @return integer 
     */
    public function getAutomaticSpecialOfferMonths()
    {
        return $this->automaticSpecialOfferMonths;
    }

    /**
     * Set analyticsPointsLimit
     *
     * @param integer $analyticsPointsLimit
     * @return Configuration
     */
    public function setAnalyticsPointsLimit($analyticsPointsLimit)
    {
        $this->analyticsPointsLimit = $analyticsPointsLimit;

        return $this;
    }

    /**
     * Get analyticsPointsLimit
     *
     * @return integer 
     */
    public function getAnalyticsPointsLimit()
    {
        return $this->analyticsPointsLimit;
    }

    /**
     * Set analyticsPointsForMakeReservationPage
     *
     * @param integer $analyticsPointsForMakeReservationPage
     * @return Configuration
     */
    public function setAnalyticsPointsForMakeReservationPage($analyticsPointsForMakeReservationPage)
    {
        $this->analyticsPointsForMakeReservationPage = $analyticsPointsForMakeReservationPage;

        return $this;
    }

    /**
     * Get analyticsPointsForMakeReservationPage
     *
     * @return integer 
     */
    public function getAnalyticsPointsForMakeReservationPage()
    {
        return $this->analyticsPointsForMakeReservationPage;
    }

    /**
     * Set analyticsPointsForEstatePage
     *
     * @param integer $analyticsPointsForEstatePage
     * @return Configuration
     */
    public function setAnalyticsPointsForEstatePage($analyticsPointsForEstatePage)
    {
        $this->analyticsPointsForEstatePage = $analyticsPointsForEstatePage;

        return $this;
    }

    /**
     * Get analyticsPointsForEstatePage
     *
     * @return integer 
     */
    public function getAnalyticsPointsForEstatePage()
    {
        return $this->analyticsPointsForEstatePage;
    }

    /**
     * Set changedPasswordInformationEmail
     *
     * @param string $changedPasswordInformationEmail
     * @return Configuration
     */
    public function setChangedPasswordInformationEmail($changedPasswordInformationEmail)
    {
        $this->changedPasswordInformationEmail = $changedPasswordInformationEmail;

        return $this;
    }

    /**
     * Get changedPasswordInformationEmail
     *
     * @return string 
     */
    public function getChangedPasswordInformationEmail()
    {
        return $this->changedPasswordInformationEmail;
    }

    /**
     * Set smartPaySkinCodeReservation
     *
     * @param string $smartPaySkinCodeReservation
     * @return Configuration
     */
    public function setSmartPaySkinCodeReservation($smartPaySkinCodeReservation)
    {
        $this->smartPaySkinCodeReservation = $smartPaySkinCodeReservation;

        return $this;
    }

    /**
     * Get smartPaySkinCodeReservation
     *
     * @return string 
     */
    public function getSmartPaySkinCodeReservation()
    {
        return $this->smartPaySkinCodeReservation;
    }

    /**
     * Set smartPaySkinCodeVoucher
     *
     * @param string $smartPaySkinCodeVoucher
     * @return Configuration
     */
    public function setSmartPaySkinCodeVoucher($smartPaySkinCodeVoucher)
    {
        $this->smartPaySkinCodeVoucher = $smartPaySkinCodeVoucher;

        return $this;
    }

    /**
     * Get smartPaySkinCodeVoucher
     *
     * @return string 
     */
    public function getSmartPaySkinCodeVoucher()
    {
        return $this->smartPaySkinCodeVoucher;
    }

    /**
     * Set referredFlagEmailYvonne
     *
     * @param string $referredFlagEmailYvonne
     * @return Configuration
     */
    public function setReferredFlagEmailYvonne($referredFlagEmailYvonne)
    {
        $this->referredFlagEmailYvonne = $referredFlagEmailYvonne;

        return $this;
    }

    /**
     * Get referredFlagEmailYvonne
     *
     * @return string 
     */
    public function getReferredFlagEmailYvonne()
    {
        return $this->referredFlagEmailYvonne;
    }

    /**
     * Set referredFlagEmailBethan
     *
     * @param string $referredFlagEmailBethan
     * @return Configuration
     */
    public function setReferredFlagEmailBethan($referredFlagEmailBethan)
    {
        $this->referredFlagEmailBethan = $referredFlagEmailBethan;

        return $this;
    }

    /**
     * Get referredFlagEmailBethan
     *
     * @return string
     */
    public function getReferredFlagEmailBethan()
    {
        return $this->referredFlagEmailBethan;
    }

    /**
     * Set referredFlagEmailKatie
     *
     * @param string $referredFlagEmailKatie
     * @return Configuration
     */
    public function setReferredFlagEmailKatie($referredFlagEmailKatie)
    {
        $this->referredFlagEmailKatie = $referredFlagEmailKatie;

        return $this;
    }

    /**
     * Get referredFlagEmailKatie
     *
     * @return string
     */
    public function getReferredFlagEmailKatie()
    {
        return $this->referredFlagEmailKatie;
    }

    /**
     * Set estateOfTheWeek
     *
     * @param \UTT\EstateBundle\Entity\Estate $estateOfTheWeek
     * @return Configuration
     */
    public function setEstateOfTheWeek(\UTT\EstateBundle\Entity\Estate $estateOfTheWeek = null)
    {
        $this->estateOfTheWeek = $estateOfTheWeek;

        return $this;
    }

    /**
     * Get estateOfTheWeek
     *
     * @return \UTT\EstateBundle\Entity\Estate 
     */
    public function getEstateOfTheWeek()
    {
        return $this->estateOfTheWeek;
    }

    /**
     * Set voucherValue50OfferPrice
     *
     * @param string $voucherValue50OfferPrice
     * @return Configuration
     */
    public function setVoucherValue50OfferPrice($voucherValue50OfferPrice)
    {
        $this->voucherValue50OfferPrice = $voucherValue50OfferPrice;

        return $this;
    }

    /**
     * Get voucherValue50OfferPrice
     *
     * @return string 
     */
    public function getVoucherValue50OfferPrice()
    {
        return $this->voucherValue50OfferPrice;
    }

    /**
     * Set voucherValue100OfferPrice
     *
     * @param string $voucherValue100OfferPrice
     * @return Configuration
     */
    public function setVoucherValue100OfferPrice($voucherValue100OfferPrice)
    {
        $this->voucherValue100OfferPrice = $voucherValue100OfferPrice;

        return $this;
    }

    /**
     * Get voucherValue100OfferPrice
     *
     * @return string 
     */
    public function getVoucherValue100OfferPrice()
    {
        return $this->voucherValue100OfferPrice;
    }

    /**
     * Set voucherValue150OfferPrice
     *
     * @param string $voucherValue150OfferPrice
     * @return Configuration
     */
    public function setVoucherValue150OfferPrice($voucherValue150OfferPrice)
    {
        $this->voucherValue150OfferPrice = $voucherValue150OfferPrice;

        return $this;
    }

    /**
     * Get voucherValue150OfferPrice
     *
     * @return string 
     */
    public function getVoucherValue150OfferPrice()
    {
        return $this->voucherValue150OfferPrice;
    }

    /**
     * Set voucherValue200OfferPrice
     *
     * @param string $voucherValue200OfferPrice
     * @return Configuration
     */
    public function setVoucherValue200OfferPrice($voucherValue200OfferPrice)
    {
        $this->voucherValue200OfferPrice = $voucherValue200OfferPrice;

        return $this;
    }

    /**
     * Get voucherValue200OfferPrice
     *
     * @return string 
     */
    public function getVoucherValue200OfferPrice()
    {
        return $this->voucherValue200OfferPrice;
    }

    /**
     * Set voucherValue250OfferPrice
     *
     * @param string $voucherValue250OfferPrice
     * @return Configuration
     */
    public function setVoucherValue250OfferPrice($voucherValue250OfferPrice)
    {
        $this->voucherValue250OfferPrice = $voucherValue250OfferPrice;

        return $this;
    }

    /**
     * Get voucherValue250OfferPrice
     *
     * @return string 
     */
    public function getVoucherValue250OfferPrice()
    {
        return $this->voucherValue250OfferPrice;
    }

    /**
     * Set voucherValue300OfferPrice
     *
     * @param string $voucherValue300OfferPrice
     * @return Configuration
     */
    public function setVoucherValue300OfferPrice($voucherValue300OfferPrice)
    {
        $this->voucherValue300OfferPrice = $voucherValue300OfferPrice;

        return $this;
    }

    /**
     * Get voucherValue300OfferPrice
     *
     * @return string 
     */
    public function getVoucherValue300OfferPrice()
    {
        return $this->voucherValue300OfferPrice;
    }

    /**
     * Set voucherValue500OfferPrice
     *
     * @param string $voucherValue500OfferPrice
     * @return Configuration
     */
    public function setVoucherValue500OfferPrice($voucherValue500OfferPrice)
    {
        $this->voucherValue500OfferPrice = $voucherValue500OfferPrice;

        return $this;
    }

    /**
     * Get voucherValue500OfferPrice
     *
     * @return string 
     */
    public function getVoucherValue500OfferPrice()
    {
        return $this->voucherValue500OfferPrice;
    }

    /**
     * Set voucherValue1000OfferPrice
     *
     * @param string $voucherValue1000OfferPrice
     * @return Configuration
     */
    public function setVoucherValue1000OfferPrice($voucherValue1000OfferPrice)
    {
        $this->voucherValue1000OfferPrice = $voucherValue1000OfferPrice;

        return $this;
    }

    /**
     * Get voucherValue1000OfferPrice
     *
     * @return string 
     */
    public function getVoucherValue1000OfferPrice()
    {
        return $this->voucherValue1000OfferPrice;
    }

    /**
     * Set discountPeriod
     *
     * @param integer $discountPeriod
     * @return Configuration
     */
    public function setDiscountPeriod($discountPeriod)
    {
        $this->discountPeriod = $discountPeriod;

        return $this;
    }

    /**
     * Get discountPeriod
     *
     * @return integer 
     */
    public function getDiscountPeriod()
    {
        return $this->discountPeriod;
    }

    /**
     * Set discountValue
     *
     * @param string $discountValue
     * @return Configuration
     */
    public function setDiscountValue($discountValue)
    {
        $this->discountValue = $discountValue;

        return $this;
    }

    /**
     * Get discountValue
     *
     * @return string 
     */
    public function getDiscountValue()
    {
        return $this->discountValue;
    }

    /**
     * @return mixed
     */
    public function getVoucherValue229OfferPrice()
    {
        return $this->voucherValue229OfferPrice;
    }

    /**
     * @param mixed $voucherValue229OfferPrice
     */
    public function setVoucherValue229OfferPrice($voucherValue229OfferPrice)
    {
        $this->voucherValue229OfferPrice = $voucherValue229OfferPrice;
    }
}
