<?php

namespace UTT\IndexBundle\Twig;

use UTT\EstateBundle\Entity\Review;
use UTT\ReservationBundle\Entity\PaymentTransaction;
use UTT\ReservationBundle\Entity\OwnerStatement;
use UTT\ReservationBundle\Entity\Reservation;
use UTT\IndexBundle\Service\SpecialTextReplaceService;
use UTT\ReservationBundle\Entity\Voucher;
use UTT\ReservationBundle\Service\CurrencyService;
use UTT\ReservationBundle\Entity\CurrencyRate;
use \Symfony\Component\Routing\Router;
use Doctrine\ORM\EntityManager;

use UTT\IndexBundle\Entity\StaticPage;
use UTT\IndexBundle\Entity\StaticPageRepository;
use UTT\ReservationBundle\Entity\HistoryEntry;
use UTT\ReservationBundle\Entity\DiscountCode;
use UTT\UserBundle\Entity\User;
use UTT\AnalyticsBundle\Service\ReportUserService;

class UTTTwigExtension extends \Twig_Extension
{
    protected $specialTextReplaceService;
    protected $currencyService;
    protected $router;
    protected $_em;
    protected $reportUserService;

    public function __construct(SpecialTextReplaceService $specialTextReplaceService, CurrencyService $currencyService, Router $router, EntityManager $em, ReportUserService $reportUserService){
        $this->specialTextReplaceService = $specialTextReplaceService;
        $this->currencyService = $currencyService;
        $this->router = $router;
        $this->_em = $em;
        $this->reportUserService = $reportUserService;
    }

    public function getFilters(){
        return array(
            new \Twig_SimpleFilter('replace_with_email_reason', array($this, 'replaceWithEmailReason')),
            new \Twig_SimpleFilter('replace_with_reservation', array($this, 'replaceWithReservation')),
            new \Twig_SimpleFilter('replace_with_voucher', array($this, 'replaceWithVoucher')),
            new \Twig_SimpleFilter('replace_with_payment_transaction', array($this, 'replaceWithPaymentTransaction')),
            new \Twig_SimpleFilter('replace_with_owner_statement', array($this, 'replaceWithOwnerStatement')),
            new \Twig_SimpleFilter('replace_with_review', array($this, 'replaceWithReview')),
            new \Twig_SimpleFilter('replace_with_user', array($this, 'replaceWithUser')),
            new \Twig_SimpleFilter('replace_with_plain_password', array($this, 'replaceWithPlainPassword')),
            new \Twig_SimpleFilter('replace_with_history_entry', array($this, 'replaceWithHistoryEntry')),
            new \Twig_SimpleFilter('replace_with_discount_code', array($this, 'replaceWithDiscountCode')),
            new \Twig_SimpleFilter('replace_with_configuration', array($this, 'replaceWithConfiguration')),
            new \Twig_SimpleFilter('replace_with_logged_user', array($this, 'replaceWithLoggedUser')),
            new \Twig_SimpleFilter('currency_filter', array($this, 'currencyFilter')),
            new \Twig_SimpleFilter('currency_info', array($this, 'currencyInfo')),
            new \Twig_SimpleFilter('statis_page_url_terms', array($this, 'statisPageUrlTerms')),
            new \Twig_SimpleFilter('referred_flag_color', array($this, 'referredFlagColor')),
            new \Twig_SimpleFilter('month_name', array($this, 'monthName')),
            new \Twig_SimpleFilter('toArray', array($this, 'toArray')),
            new \Twig_SimpleFilter('allowed_user_for_analytics_mail', array($this, 'allowedUserForAnalyticsMail')),
        );
    }

    public function allowedUserForAnalyticsMail(User $user){
        return $this->reportUserService->allowedUserForAnalyticsMail($user);
    }

    public function toArray($object){
        if($object instanceof \StdClass){
            return (array) $object;
        }
        return false;
    }

    public function monthName($monthId){
        $dateObj   = \DateTime::createFromFormat('!m', $monthId);
        $monthName = $dateObj->format('F');
        return $monthName;
    }

    public function referredFlagColor($referredFlagId){
        if($referredFlagId == Reservation::REFERRED_FLAG_ADMIN){
            return 'orange';
        }elseif($referredFlagId == Reservation::REFERRED_FLAG_LANDLORD){
            return '#FF0099';
        }elseif($referredFlagId == Reservation::REFERRED_FLAG_LUKASZ_RAK){
            return 'blue';
        }elseif($referredFlagId == Reservation::REFERRED_FLAG_SPOT){
            return '#660099';
        }elseif($referredFlagId == Reservation::REFERRED_FLAG_GREG){
            return 'green';
        }elseif($referredFlagId == Reservation::REFERRED_FLAG_ELERI){
            return '#660099';
        }elseif($referredFlagId == Reservation::REFERRED_FLAG_YVONNE){
            return '#660099';
        }elseif($referredFlagId == Reservation::REFERRED_FLAG_BETHAN){
            return '#af071b';
        }elseif($referredFlagId == Reservation::REFERRED_FLAG_KATIE){
            return '#660099';
        }else{
            return 'black';
        }
    }

    public function statisPageUrlTerms($text){
        /** @var StaticPageRepository $staticPageRepository */
        $staticPageRepository = $this->_em->getRepository('UTTIndexBundle:StaticPage');

        /** @var StaticPage $staticPage */
        $staticPage = $staticPageRepository->findOneBy(array(
            'type' => StaticPage::TYPE_TERMS_AND_CONDITIONS
        ));
        if($staticPage instanceOf StaticPage){
            $url = $this->router->generate('utt_index_static_page', array(
                'url' => $staticPage->getUrl()
            ), true);

            return '<a href="'.$url.'">'.$text.'</a>';
        }

        return $text;
    }

    public function currencyInfo(){
        return '£';
    }

    public function currencyFilter($poundValue){
        /** @var CurrencyRate $zlotyRate */
        $zlotyRate = $this->currencyService->getCurrentCurrencyRate(CurrencyRate::CURRENCY_CODE_POLAND_ZLOTY);
        /** @var CurrencyRate $euroRate */
        $euroRate = $this->currencyService->getCurrentCurrencyRate(CurrencyRate::CURRENCY_CODE_EURO_MEMBER_COUNTRIES);
        if($zlotyRate instanceof CurrencyRate && $euroRate instanceof CurrencyRate){
            $zlotyVanue = (float) round($poundValue * $zlotyRate->getValue(), 2);
            $euroVanue = (float) round($poundValue * $euroRate->getValue(), 2);

            return '<span title="'.$zlotyVanue.' Polish Zloty; '.$euroVanue.' Euro">'.$poundValue.'</span>';
        }

        return $poundValue;
    }

    public function replaceWithEmailReason($text, $emailReason){
        return $this->specialTextReplaceService->replaceWithEmailReason($text, $emailReason);
    }

    public function replaceWithReservation($text, Reservation $reservation){
        return $this->specialTextReplaceService->replaceWithReservation($text, $reservation);
    }

    public function replaceWithVoucher($text, Voucher $voucher){
        return $this->specialTextReplaceService->replaceWithVoucher($text, $voucher);
    }

    public function replaceWithOwnerStatement($text, OwnerStatement $ownerStatement){
        return $this->specialTextReplaceService->replaceWithOwnerStatement($text, $ownerStatement);
    }

    public function replaceWithReview($text, Review $review){
        return $this->specialTextReplaceService->replaceWithReview($text, $review);
    }

    public function replaceWithPaymentTransaction($text, PaymentTransaction $paymentTransaction){
        return $this->specialTextReplaceService->replaceWithPaymentTransaction($text, $paymentTransaction);
    }

    public function replaceWithUser($text, User $user){
        return $this->specialTextReplaceService->replaceWithUser($text, $user);
    }

    public function replaceWithPlainPassword($text, $plainPassword){
        return $this->specialTextReplaceService->replaceWithPlainPassword($text, $plainPassword);
    }

    public function replaceWithHistoryEntry($text, HistoryEntry $historyEntry){
        return $this->specialTextReplaceService->replaceWithHistoryEntry($text, $historyEntry);
    }

    public function replaceWithDiscountCode($text, DiscountCode $discountCode){
        return $this->specialTextReplaceService->replaceWithDiscountCode($text, $discountCode);
    }

    public function replaceWithLoggedUser($text){
        return $this->specialTextReplaceService->replaceWithLoggedUser($text);
    }

    public function replaceWithConfiguration($text){
        return $this->specialTextReplaceService->replaceWithConfiguration($text);
    }

    public function getName(){
        return 'utt_twig_extension';
    }
}
