<?php

namespace UTT\AdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use UTT\ReservationBundle\Entity\Reservation;
use UTT\ReservationBundle\Entity\ReservationRepository;
use UTT\EstateBundle\Entity\Estate;
use UTT\AdminBundle\Service\UTTAclService;
use UTT\EstateBundle\Entity\EstateRepository;
use UTT\UserBundle\Entity\User;
use Symfony\Component\HttpFoundation\Request;
use UTT\ReservationBundle\Entity\PricingCategory;
use UTT\ReservationBundle\Entity\PricingCategoryRepository;
use Doctrine\ORM\EntityManager;

class AdminController extends Controller
{
    public function pricingCategoriesAction(Request $request){
        /** @var UTTAclService $uttAclService */
        $uttAclService = $this->container->get('utt.aclService');
        $uttAclService->authForPricingCategories();

        /** @var EntityManager $_em */
        $_em = $this->getDoctrine()->getManager();

        /** @var PricingCategoryRepository $pricingCategoryRepository */
        $pricingCategoryRepository = $_em->getRepository('UTTReservationBundle:PricingCategory');
        $pricingCategories = $pricingCategoryRepository->getForPricingCategoriesPage();

        /** @var EstateRepository $estateRepository */
        $estateRepository = $_em->getRepository('UTTEstateBundle:Estate');
        $estates = $estateRepository->getAllActive();

        $pricingCategoriesFlexible = array();
        $pricingCategoriesStandard = array();
        if($pricingCategories){
            /** @var PricingCategory $pricingCategory */
            foreach($pricingCategories as $pricingCategory){
                if($pricingCategory->isFlexible()){
                    $pricingCategoriesFlexible[] = $pricingCategory;
                }elseif($pricingCategory->isStandard()){
                    $pricingCategoriesStandard[] = $pricingCategory;
                }
            }
        }

        return $this->render('UTTAdminBundle:Admin:pricingCategories.html.twig', array(
            'admin_pool' => $this->get('sonata.admin.pool'),
            'pricingCategoriesFlexible' => $pricingCategoriesFlexible,
            'pricingCategoriesStandard' => $pricingCategoriesStandard,
            'estates' => $estates
        ));
    }

    public function allAvailabilityAction(Request $request){
        /** @var UTTAclService $uttAclService */
        $uttAclService = $this->container->get('utt.aclService');
        $uttAclService->authForAllAvailability();

        return $this->render('UTTAdminBundle:Admin:allAvailability.html.twig', array(
            'admin_pool' => $this->get('sonata.admin.pool')
        ));
    }

    public function searchReservationAction(Request $request){
        $q = $request->get('q');
        $idQuery = array();
        $nameQuery = array();


        $qArray = explode(' ', $q);
        foreach($qArray as $qRow){
            if(is_numeric($qRow) && $qRow > 0){
                $idQuery[] = $qRow;
            }else{
                $nameQuery[] = $qRow;
            }
        }

        if(isset($idQuery[0]) && $idQuery[0]){
            /** @var ReservationRepository $reservationRepository */
            $reservationRepository = $this->getDoctrine()->getManager()->getRepository('UTTReservationBundle:Reservation');
            /** @var Reservation $reservation */
            $reservation = $reservationRepository->find($idQuery[0]);
            if($reservation instanceof Reservation){
                return $this->redirect($this->generateUrl('admin_utt_reservation_reservation_show', array(
                    'id' => $reservation->getId()
                )));
            }
        }

        return $this->redirect($this->generateUrl('admin_utt_reservation_reservation_list').'?'.join('&', array(
                'filter[id][value]='.join(' ', $idQuery),
                'filter[userData][value]='.join(' ', $nameQuery)
            )));
    }

    public function renderTodayIncomeBlockAction(){
        /** @var UTTAclService $uttAclService */
        $uttAclService = $this->container->get('utt.aclService');
        /** @var ReservationRepository $reservationRepository */
        $reservationRepository = $this->getDoctrine()->getManager()->getRepository('UTTReservationBundle:Reservation');
        if($uttAclService->isGrantedSuperAdmin()){
            $income = $reservationRepository->getTodayIncomeArray();
            $uttIncome = $reservationRepository->getTodayUTTIncomeArray();
        }else{
            $income = 0;
            $uttIncome = 0;
        }

        $currentMonthSales = 0;
        $currentMonthUttIncome = 0;
        $lastYearMonthSales = 0;
        $lastYearMonthUttIncome = 0;
        $lastYearMonthSalesUntilToday = 0;
        $lastYearMonthUttIncomeUntilToday = 0;

        if($uttAclService->isGrantedSuperAdmin()){
            $nowDate = new \DateTime('now');
            $dateForMonth = new \DateTime('now');
            $dateForMonth = $dateForMonth->modify('-1 year');
            $dateForMonth->setDate($dateForMonth->format('Y'), 1, 1)->setTime(0,0,0);

            $salesByMonth = $reservationRepository->getForSalesByMonthArray($dateForMonth, null, 'ASC');
            $salesByMonthUntilToday = $reservationRepository->getForSalesByMonthUntilTodayArray(null);

            if($salesByMonth){
                foreach($salesByMonth as $item){
                    if(isset($item['month']) && $item['month']){
                        $monthString = ($nowDate->format('Y')-1).'-'.$nowDate->format('m');
                        if($monthString == $item['month']){
                            $lastYearMonthSales = $item['priceSum'];
                            $lastYearMonthUttIncome = $item['adminChargeSum'] + $item['commissionValueFull'] + $item['bookingProtectChargeSum'] * 0.4;
                            break;
                        }
                    }
                }
            }

            if($salesByMonthUntilToday){
                foreach($salesByMonthUntilToday as $item){
                    if(isset($item['yearMonth']) && $item['yearMonth']){
                        $monthString = $nowDate->format('Y').'-'.$nowDate->format('m');
                        if($monthString == $item['yearMonth']){
                            $currentMonthSales = $item['priceSum'];
                            $currentMonthUttIncome = $item['adminChargeSum'] + $item['commissionValueFull'] + $item['bookingProtectChargeSum'] * 0.4;
                            //break;
                        }

                        $monthString = ($nowDate->format('Y')-1).'-'.$nowDate->format('m');
                        if($monthString == $item['yearMonth']){
                            $lastYearMonthSalesUntilToday = $item['priceSum'];
                            $lastYearMonthUttIncomeUntilToday = $item['adminChargeSum'] + $item['commissionValueFull'] + $item['bookingProtectChargeSum'] * 0.4;
                            //break;
                        }
                    }
                }
            }
        }

        return $this->render('UTTAdminBundle:Admin:renderTodayIncomeBlock.html.twig', array(
            'income' => $income,
            'uttIncome' => $uttIncome,

            'currentMonthSales' => $currentMonthSales,
            'currentMonthUttIncome' => $currentMonthUttIncome,

            'lastYearMonthSales' => $lastYearMonthSales,
            'lastYearMonthUttIncome' => $lastYearMonthUttIncome,

            'lastYearMonthSalesUntilToday' => $lastYearMonthSalesUntilToday,
            'lastYearMonthUttIncomeUntilToday' => $lastYearMonthUttIncomeUntilToday
        ));
    }

    public function renderMostUsedBlockAction(){
        /** @var UTTAclService $uttAclService */
        $uttAclService = $this->container->get('utt.aclService');
        /** @var EstateRepository $estateRepository */
        $estateRepository = $this->getDoctrine()->getManager()->getRepository('UTTEstateBundle:Estate');

        $twigArray = array();
        if($uttAclService->isGrantedSuperAdmin()){
            $estates = $estateRepository->getAllActive();
            $twigArray['estates'] = $estates;
        }elseif($uttAclService->isGrantedOwner()){
            /** @var User $user */
            $user = $uttAclService->userAuth();
            $estates = $estateRepository->getActiveOwnedByUser($user->getId());
            $twigArray['estates'] = $estates;
        }elseif($uttAclService->isGrantedCleaner()){
            /** @var User $user */
            $user = $uttAclService->userAuth();
            $estates = $estateRepository->getActiveCleanedByUser($user->getId());
            $twigArray['estates'] = $estates;
        }

        return $this->render('UTTAdminBundle:Admin:renderMostUsedBlock.html.twig', $twigArray);
    }

    public function indexAction(){
        return $this->render('UTTAdminBundle:Admin:index.html.twig');
    }
}
