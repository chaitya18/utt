<?php

namespace UTT\AdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use UTT\ReservationBundle\Entity\Reservation;
use UTT\ReservationBundle\Entity\ReservationRepository;
use Symfony\Component\HttpFoundation\Request;
use UTT\EstateBundle\Entity\Estate;
use UTT\EstateBundle\Entity\EstateRepository;
use UTT\ReservationBundle\Entity\OwnerStatement;
use UTT\ReservationBundle\Entity\OwnerStatementRepository;
use UTT\AdminBundle\Service\UTTAclService;
use UTT\UserBundle\Entity\User;
use Symfony\Component\HttpFoundation\Response;

class AccountsController extends Controller
{
    public function renderFullStatementToPdfButtonAction($date){
        /** @var UTTAclService $uttAclService */
        $uttAclService = $this->container->get('utt.aclService');
        $uttAclService->authForAccounts();

        if(!$uttAclService->isGrantedSuperAdmin()){
            return new Response();
        }

        $twigArray = array();

        try{
            $dateObject = new \Datetime($date);

            /** @var OwnerStatementRepository $ownerStatementRepository */
            $ownerStatementRepository = $this->getDoctrine()->getManager()->getRepository('UTTReservationBundle:OwnerStatement');
            $ownerStatement = $ownerStatementRepository->getStatementsForDate($dateObject, OwnerStatement::TYPE_FULL_STATEMENT, true);
            if($ownerStatement instanceof OwnerStatement){
                $twigArray['ownerStatement'] = $ownerStatement;
            }
        }catch (\Exception $e){

        }
        $twigArray['dateString'] = $date;

        return $this->render('UTTAdminBundle:Accounts:renderFullStatementToPdfButton.html.twig', $twigArray);
    }

    public function renderSummaryToPdfButtonAction($date){
        /** @var UTTAclService $uttAclService */
        $uttAclService = $this->container->get('utt.aclService');
        $uttAclService->authForAccounts();

        if(!$uttAclService->isGrantedSuperAdmin()){
            return new Response();
        }

        $twigArray = array();

        try{
            $dateObject = new \Datetime($date);

            /** @var OwnerStatementRepository $ownerStatementRepository */
            $ownerStatementRepository = $this->getDoctrine()->getManager()->getRepository('UTTReservationBundle:OwnerStatement');
            $ownerStatement = $ownerStatementRepository->getStatementsForDate($dateObject, OwnerStatement::TYPE_FULL_SUMMARY, true);
            if($ownerStatement instanceof OwnerStatement){
                $twigArray['ownerStatement'] = $ownerStatement;
            }
        }catch (\Exception $e){

        }
        $twigArray['dateString'] = $date;

        return $this->render('UTTAdminBundle:Accounts:renderSummaryToPdfButton.html.twig', $twigArray);
    }

    public function renderFiltersAction($date, $estateId, $isSummariseSelected = false){
        /** @var UTTAclService $uttAclService */
        $uttAclService = $this->container->get('utt.aclService');
        $uttAclService->authForAccounts();

        $startDate = new \Datetime('2012-08-01');
        $endDate = new \Datetime('now');
        $endDate->modify('+1 year');
        $interval = new \DateInterval('P1M');
        $period   = new \DatePeriod($startDate, $interval, $endDate);

        $dates = array();
        /** @var \DateTime $periodDate */
        foreach ($period as $periodDate) {
            $dates[] = json_decode(json_encode(array(
                'id' => $periodDate->format('Y-m'),
                'label' => $periodDate->format('F Y'),
            )));
        }

        /** @var EstateRepository $estateRepository */
        $estateRepository = $this->getDoctrine()->getManager()->getRepository('UTTEstateBundle:Estate');

        if($uttAclService->isGrantedSuperAdmin() || $uttAclService->isGrantedAcc()){
            $estates = $estateRepository->findAll();
        }elseif($uttAclService->isGrantedOwner()){
            /** @var User $user */
            $user = $uttAclService->userAuth();
            $estates = $estateRepository->getOwnedByUser($user->getId());
        }else{
            $estates = false;
        }

        return $this->render('UTTAdminBundle:Accounts:renderFilters.html.twig', array(
            'dates' => $dates,
            'estates' => $estates,
            'dateString' => $date,
            'estateId' => $estateId,
            'isSummariseSelected' => $isSummariseSelected,
        ));
    }

    public function summariseAction(Request $request, $date){
        /** @var UTTAclService $uttAclService */
        $uttAclService = $this->container->get('utt.aclService');
        $uttAclService->authForAccounts();

        /** @var ReservationRepository $reservationRepository */
        $reservationRepository = $this->getDoctrine()->getManager()->getRepository('UTTReservationBundle:Reservation');

        /** @var EstateRepository $estateRepository */
        $estateRepository = $this->getDoctrine()->getManager()->getRepository('UTTEstateBundle:Estate');

        $isGrantedSuperAdmin = false;
        if($uttAclService->isGrantedSuperAdmin() || $uttAclService->isGrantedAcc()){
            $isGrantedSuperAdmin = true;
            $estates = $estateRepository->findAll();
        }elseif($uttAclService->isGrantedOwner()){
            /** @var User $user */
            $user = $uttAclService->userAuth();
            $estates = $estateRepository->getOwnedByUser($user->getId());
        }else{
            $estates = false;
        }

        $summaryData = array();
        /** @var Estate $estate */
        foreach($estates as $estate){
            if($uttAclService->isGrantedSuperAdmin() || $uttAclService->isGrantedAcc()){
                $reservations = $reservationRepository->getReservationsByDateStringAndEstateId($date, $estate->getId());
            }elseif($uttAclService->isGrantedOwner()){
                /** @var User $user */
                $user = $uttAclService->userAuth();
                $reservations = $reservationRepository->getReservationsByDateStringAndEstateId($date, $estate->getId(), $user->getId());
            }else{
                $reservations = false;
            }
            if($reservations){
                $summaryData[] = (object)array(
                    'estate' => $estate,
                    'reservations' => $reservations
                );
            }
        }


        return $this->render('UTTAdminBundle:Accounts:summarise.html.twig', array(
            'admin_pool' => $this->get('sonata.admin.pool'),
            'summaryData' => $summaryData,
            'dateString' => $date,
            'isGrantedSuperAdmin' => $isGrantedSuperAdmin
        ));
    }

    public function byDateAndEstateAction(Request $request, $date, $estateId){
        /** @var UTTAclService $uttAclService */
        $uttAclService = $this->container->get('utt.aclService');
        $reservationService = $this->get('utt.reservationservice');
        $uttAclService->authForAccounts();

        /** @var EstateRepository $estateRepository */
        $estateRepository = $this->getDoctrine()->getManager()->getRepository('UTTEstateBundle:Estate');

        if($estateId != null){
            $estate = $estateRepository->find($estateId);
            if(!($estate instanceof Estate)){
                return $this->redirect($this->generateUrl('utt_admin_accounts'));
            }

            $this->container->get('utt.aclService')->authEstateOwnerUser($estate);
        }

        /** @var ReservationRepository $reservationRepository */
        $reservationRepository = $this->getDoctrine()->getManager()->getRepository('UTTReservationBundle:Reservation');

        $isGrantedSuperAdmin = false;
        if($uttAclService->isGrantedSuperAdmin() || $uttAclService->isGrantedAcc()){
            $isGrantedSuperAdmin = true;
            if($estateId == null){
                $reservations = $reservationRepository->getReservationsByDateString($date);
            }else{
                $reservations = $reservationRepository->getReservationsByDateStringAndEstateId($date, $estateId);
            }
        }elseif($uttAclService->isGrantedOwner()){
            /** @var User $user */
            $user = $uttAclService->userAuth();

            if($estateId == null){
                $reservations = $reservationRepository->getReservationsByDateString($date, $user->getId());
            }else{
                $reservations = $reservationRepository->getReservationsByDateStringAndEstateId($date, $estateId, $user->getId());
            }
        }else{
            $reservations = false;
        }

        /**if($reservations) {
            $reservations = $reservationService->checkIsRefundable($reservations);
        }**/


        return $this->render('UTTAdminBundle:Accounts:byDateAndEstate.html.twig', array(
            'admin_pool' => $this->get('sonata.admin.pool'),
            'reservations' => $reservations,
            'estateId' => $estateId,
            'dateString' => $date,
            'isGrantedSuperAdmin' => $isGrantedSuperAdmin
        ));
    }

    public function indexAction(Request $request){
        /** @var UTTAclService $uttAclService */
        $uttAclService = $this->container->get('utt.aclService');
        $uttAclService->authForAccounts();

        /** @var EstateRepository $estateRepository */
        $estateRepository = $this->getDoctrine()->getManager()->getRepository('UTTEstateBundle:Estate');

        if($uttAclService->isGrantedSuperAdmin() || $uttAclService->isGrantedAcc()){
            $estate = $estateRepository->getFirstActiveEstate();
        }elseif($uttAclService->isGrantedOwner()){
            /** @var User $user */
            $user = $uttAclService->userAuth();
            $estate = $estateRepository->getFirstActiveEstateOwnedByUser($user->getId());
        }else{
            $estate = false;
        }

        if($estate instanceof Estate){
            $nowDate = new \DateTime('now');

            return $this->redirect($this->generateUrl('utt_admin_accounts_by_date_and_estate', array(
                'date' => $nowDate->format('Y-m'),
                'estateId' => $estate->getId()
            )));
        }

        return $this->redirect($this->generateUrl('sonata_admin_dashboard'));
    }
}
