<?php

namespace UTT\AdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use UTT\EstateBundle\Entity\Estate;
use UTT\EstateBundle\Entity\EstateRepository;
use UTT\AdminBundle\Service\UTTAclService;
use UTT\UserBundle\Entity\User;
use UTT\ReservationBundle\Entity\ReservationRepository;

class CleaningRotaController extends Controller
{
    /**
     * @var UTTAclService $uttAclService
     */
    protected $uttAclService;

    /**
     * @var EstateRepository $estateRepository
     */
    protected $estateRepository;

    public function byEstateAction(Request $request, $estateId)
    {
        $this->uttAclService = $this->container->get('utt.aclService');
        $this->uttAclService->authForCleaningRota();

        $this->estateRepository = $this->getDoctrine()->getManager()->getRepository('UTTEstateBundle:Estate');

        $estate = $this->estateRepository->find($estateId);
        if (!($estate instanceof Estate)) {
            return $this->redirect($this->generateUrl('utt_admin_accounts'));
        }

        $this->hasPermToEstate($estate);
        $estates = $this->getEstates();

        /** @var ReservationRepository $reservationRepository */
        $reservationRepository = $this->getDoctrine()->getManager()->getRepository('UTTReservationBundle:Reservation');
        $reservations = $reservationRepository->getForCleaningRota($estate);

        return $this->render('UTTAdminBundle:CleaningRota:byEstate.html.twig', array(
            'admin_pool' => $this->get('sonata.admin.pool'),
            'estates' => $estates,
            'estateId' => $estateId,
            'reservations' => $reservations
        ));
    }

    /**
     * @param $estate
     */
    protected function hasPermToEstate($estate) {
        try {
            $this->uttAclService->authEstateOwnerUser($estate);
        } catch (\Exception $e) {
            $this->uttAclService->authEstateCleanerUser($estate);
        }
    }

    protected function getEstates()
    {
        $estates = array();
        /** @var User $user */
        $user = $this->uttAclService->userAuth();
        if ($this->uttAclService->isGrantedCleaner()) {
            $estates = $this->estateRepository->getActiveCleanedByUser($user->getId());
        } elseif ($this->uttAclService->isGrantedOwner()) {
            $estates = $this->estateRepository->getActiveOwnedByUser($user->getId());
        } else {
            $estates = $this->estateRepository->getAllActive();
        }

        return $estates;
    }

    public function indexAction(Request $request)
    {
        /** @var UTTAclService $uttAclService */
        $this->uttAclService = $this->container->get('utt.aclService');
        $this->uttAclService->authForCleaningRota();

        /** @var EstateRepository $estateRepository */
        $this->estateRepository = $this->getDoctrine()->getManager()->getRepository('UTTEstateBundle:Estate');
        $estate = $this->getFirstCleanedEstate();

        if ($estate instanceof Estate) {
            return $this->redirect($this->generateUrl('utt_admin_cleaning_rota_by_estate', array(
                'estateId' => $estate->getId()
            )));
        }

        return $this->redirect($this->generateUrl('sonata_admin_dashboard'));
    }

    protected function getFirstCleanedEstate()
    {
        /** @var User $user */
        $user = $this->uttAclService->userAuth();
        if ($this->uttAclService->isGrantedCleaner()) {
            $estate = $this->estateRepository->getFirstActiveEstateCleanedByUser($user->getId());
        } elseif ($this->uttAclService->isGrantedOwner()) {
            $estate = $this->estateRepository->getActiveOwnedByUser($user->getId(), 1);
            $estate = $estate ? $estate[0] : $estate;
        } else {
            $estate = $this->estateRepository->getFirstActiveEstate();
        }
        return $estate;
    }
}
