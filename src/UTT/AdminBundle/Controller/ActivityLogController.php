<?php

namespace UTT\AdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use UTT\EstateBundle\Entity\Estate;
use UTT\EstateBundle\Entity\EstateRepository;
use UTT\AdminBundle\Service\UTTAclService;
use UTT\UserBundle\Entity\User;
use UTT\ReservationBundle\Entity\Reservation;
use UTT\ReservationBundle\Entity\ReservationRepository;
use UTT\UserBundle\Entity\ActivityLogRepository;
use UTT\UserBundle\Service\ActivityLogService;

class ActivityLogController extends Controller
{
    public function indexAction(Request $request){
        /** @var UTTAclService $uttAclService */
        $uttAclService = $this->container->get('utt.aclService');
        $uttAclService->authForActivityLog();

        /** @var ReservationRepository $reservationRepository */
        $reservationRepository = $this->getDoctrine()->getManager()->getRepository('UTTReservationBundle:Reservation');
        /** @var ActivityLogRepository $activityLogRepository */
        $activityLogRepository = $this->getDoctrine()->getManager()->getRepository('UTTUserBundle:ActivityLog');
        /** @var ActivityLogService $activityLogService */
        $activityLogService = $this->get('utt.activityLogService');

        $activityLog = array();
        $reservations = $reservationRepository->findForActivityLog();

        $referers = array();
        if($reservations){
            /** @var Reservation $reservation */
            foreach($reservations as $reservation){
                $trackingCookieHash = $activityLogRepository->findTrackingCookieHashBySessionId($reservation->getSessionId());
                $activityLogs = $activityLogRepository->findByReservationSessionIdTrackingCookieHash($reservation->getSessionId(), $trackingCookieHash);

                $detectedReferer = $activityLogService->detectReferer($activityLogs);
                $activityLog[] = (object)array(
                    'reservation' => $reservation,
                    'detectedReferer' => $detectedReferer,
                    //'activityLogs' => $activityLogs
                );

                if(isset($referers[$detectedReferer])){
                    $referers[$detectedReferer][] = $reservation->getId();
                }else{
                    $referers[$detectedReferer] = array($reservation->getId());
                }
            }
        }

        return $this->render('UTTAdminBundle:ActivityLog:index.html.twig', array(
            'admin_pool' => $this->get('sonata.admin.pool'),
            'activityLog' => $activityLog,
            'referers' => $referers
        ));
    }
}
