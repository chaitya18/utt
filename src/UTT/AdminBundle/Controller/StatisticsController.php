<?php

namespace UTT\AdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use UTT\ReservationBundle\Entity\ReservationRepository;
use Symfony\Component\HttpFoundation\Request;
use UTT\AdminBundle\Service\UTTAclService;
use UTT\EstateBundle\Entity\EstateRepository;
use UTT\EstateBundle\Entity\Estate;
use UTT\UserBundle\Entity\User;
use UTT\ReservationBundle\Entity\Reservation;
use Symfony\Component\HttpFoundation\JsonResponse;
use UTT\EstateBundle\Entity\EstateVisitReport;
use UTT\EstateBundle\Entity\EstateVisitReportRepository;

class StatisticsController extends Controller
{

    public function visitsOfEstatesForLatestDaysAction(Request $request, $page){
        /** @var UTTAclService $uttAclService */
        $uttAclService = $this->container->get('utt.aclService');
        $uttAclService->authForStatistics();

        /** @var EstateRepository $estateRepository */
        $estateRepository = $this->getDoctrine()->getManager()->getRepository('UTTEstateBundle:Estate');
        /** @var EstateVisitReportRepository $estateVisitReportRepository */
        $estateVisitReportRepository = $this->getDoctrine()->getManager()->getRepository('UTTEstateBundle:EstateVisitReport');

        if($uttAclService->isGrantedSuperAdmin()){
            $estates = $estateRepository->findBy(array(
                'hideFromDisplay' => Estate::HIDE_FROM_DISPLAY_VISIBLE
            ), null, 30, ($page-1) * 30);
            if($estates){
                $chartData = array();

                /** @var Estate $estate */
                foreach($estates as $estate) {
                    $daysVisits1 = $estateVisitReportRepository->findNumberOfVisitsForLatestDays($estate, 1);
                    $daysVisits7 = $estateVisitReportRepository->findNumberOfVisitsForLatestDays($estate, 7);
                    $daysVisits30 = $estateVisitReportRepository->findNumberOfVisitsForLatestDays($estate, 30);

                    $chartData[] = array(
                        'name' => $estate->getName(),
                        'daysVisits1' => $daysVisits1,
                        'daysVisits7' => $daysVisits7,
                        'daysVisits30' => $daysVisits30
                    );
                }

                $chartDataSorted = $this->array_sort($chartData, 'daysVisits1', SORT_DESC);
                $data = array();
                foreach($chartDataSorted as $item){
                    $data[] = $item;
                }

                return new JsonResponse(array('success' => true, 'data' => $data));
            }
        }

        return new JsonResponse(array('success' => false));

    }

    public function visitsOfEstatesAction(Request $request, $page){
        /** @var UTTAclService $uttAclService */
        $uttAclService = $this->container->get('utt.aclService');
        $uttAclService->authForStatistics();

        /** @var EstateRepository $estateRepository */
        $estateRepository = $this->getDoctrine()->getManager()->getRepository('UTTEstateBundle:Estate');
        /** @var EstateVisitReportRepository $estateVisitReportRepository */
        $estateVisitReportRepository = $this->getDoctrine()->getManager()->getRepository('UTTEstateBundle:EstateVisitReport');

        if($uttAclService->isGrantedSuperAdmin()){
            $estates = $estateRepository->findBy(array(
                'hideFromDisplay' => Estate::HIDE_FROM_DISPLAY_VISIBLE
            ), null, 30, ($page-1) * 30);
            if($estates){
                $chartData = array();

                /** @var Estate $estate */
                foreach($estates as $estate) {
                    $visitsArray = array();

                    $visitsSort = 0;
                    $visits = $estateVisitReportRepository->findVisitsForEstate($estate);
                    if($visits){
                        if(isset($visits[0]['sumVisit'])) $visitsSort = $visits[0]['sumVisit'];
                        foreach($visits as $visit){
                            $visitsArray[] = array(
                                'month' => $visit['dateString'],
                                'visits' => $visit['sumVisit']
                            );
                        }
                    }

                    $chartData[] = array(
                        'name' => $estate->getName(),
                        'months' => $visitsArray,
                        'visitsSort' => $visitsSort
                    );
                }

                $chartDataSorted = $this->array_sort($chartData, 'visitsSort', SORT_DESC);
                $data = array();
                foreach($chartDataSorted as $item){
                    $data[] = $item;
                }

                return new JsonResponse(array('success' => true, 'data' => $data));
            }
        }

        return new JsonResponse(array('success' => false));
    }

    public function estatesVisitsAction(Request $request, $page){
        /** @var UTTAclService $uttAclService */
        $uttAclService = $this->container->get('utt.aclService');
        $uttAclService->authForStatistics();

        return $this->render('UTTAdminBundle:Statistics:estatesVisits.html.twig', array(
            'admin_pool' => $this->get('sonata.admin.pool'),
            'page' => $page
        ));
    }

    private function array_sort($array, $on, $order=SORT_ASC){
        $new_array = array();
        $sortable_array = array();

        if (count($array) > 0) {
            foreach ($array as $k => $v) {
                if (is_array($v)) {
                    foreach ($v as $k2 => $v2) {
                        if ($k2 == $on) {
                            $sortable_array[$k] = $v2;
                        }
                    }
                } else {
                    $sortable_array[$k] = $v;
                }
            }

            switch ($order) {
                case SORT_ASC:
                    asort($sortable_array);
                    break;
                case SORT_DESC:
                    arsort($sortable_array);
                    break;
            }

            foreach ($sortable_array as $k => $v) {
                $new_array[$k] = $array[$k];
            }
        }

        return $new_array;
    }

    public function indexDataAction(Request $request, $year = null){
        /** @var UTTAclService $uttAclService */
        $uttAclService = $this->container->get('utt.aclService');
        $uttAclService->authForStatistics();

        /** @var EstateRepository $estateRepository */
        $estateRepository = $this->getDoctrine()->getManager()->getRepository('UTTEstateBundle:Estate');
        /** @var ReservationRepository $reservationRepository */
        $reservationRepository = $this->getDoctrine()->getManager()->getRepository('UTTReservationBundle:Reservation');

        if($uttAclService->isGrantedSuperAdmin()){
            $estates = $estateRepository->findAll();

            $summaryData = array();
            $nowDate = new \DateTime('now');
            $year = $year ? $year : $nowDate->format('Y');

            /** @var Estate $estate */
            foreach($estates as $estate){
                $reservations = $reservationRepository->getReservationsByYearStringAndEstateId($year, $estate->getId());

                if($reservations){
                    $paid = 0;
                    $nights = 0;

                    /** @var Reservation $reservation */
                    foreach($reservations as $reservation){
                        $paid += (float)$reservation->getPaid();
                        $nights += (int)$reservation->getNumberOfNights();
                    }

                    $summaryData[] = (object)array(
                        'paid' => $paid,
                        'nights' => $nights,
                        'estate' => $estate,
                        'reservations' => $reservations
                    );
                }
            }

            $summaryData = $this->array_sort($summaryData, 'nights', SORT_DESC);
        }else{
            $summaryData = array();
        }

        $data = array();

        foreach($summaryData as $summaryDataItem){
            $estatePaid = 0;
            $estateNights = 0;
            $estateTotal = 0;
            $estateCommissionValue = 0;
            $estateAdminCharge = 0;
            $estateExtras = 0;
            $estateToOwnersValue = 0;

            /** @var Reservation $reservation */
            foreach($summaryDataItem->reservations as $reservation){
                $estatePaid = $estatePaid + (float) $reservation->getPaid();
                $estateNights = $estateNights + $reservation->getNumberOfNights();
                $estateTotal = $estateTotal + (float) $reservation->getTotalPrice();
                $estateCommissionValue = $estateCommissionValue + $reservation->calculateCommissionValue();
                $estateAdminCharge = $estateAdminCharge + $reservation->getPriceDecoded()->getAdminCharge();
                $estateExtras = $estateExtras + $reservation->getPriceDecoded()->getAdditionalSleepsPrice();
                $estateToOwnersValue = $estateToOwnersValue + $reservation->getToOwnersValue();
            }

            $data[] = (object) array(
                'estateName' => $summaryDataItem->estate->getName(),
                'paid' => $estatePaid,
                'nights' => $estateNights,
                'total' => $estateTotal,
                'commissionValue' => $estateCommissionValue,
                'adminCharge' => $estateAdminCharge,
                'extras' => $estateExtras,
                'toOwnersValue' => $estateToOwnersValue
            );
        }

        return new JsonResponse(array('success' => true, 'data' => $data));
    }

    public function indexAction(Request $request, $year = null){
        $nowDate = new \DateTime('now');
        $year = $year ? $year : $nowDate->format('Y');

        return $this->render('UTTAdminBundle:Statistics:index.html.twig', array(
            'admin_pool' => $this->get('sonata.admin.pool'),
            'yearSelected' => $year
        ));
    }
}
