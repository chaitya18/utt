<?php

namespace UTT\AdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use UTT\AdminBundle\Service\UTTAclService;

use UTT\AnalyticsBundle\Entity\ReportUser;
use UTT\AnalyticsBundle\Entity\ReportUserRepository;
use UTT\AnalyticsBundle\Entity\ReportReferer;
use UTT\AnalyticsBundle\Entity\ReportRefererRepository;

class AnalyticsController extends Controller
{
    public function reportUserAction(Request $request){
        /** @var UTTAclService $uttAclService */
        $uttAclService = $this->container->get('utt.aclService');
        $uttAclService->authForAnalyticsReportUser();

        /** @var ReportUserRepository $reportUserRepository */
        $reportUserRepository = $this->getDoctrine()->getManager()->getRepository('UTTAnalyticsBundle:ReportUser');

        $reportUsers = $reportUserRepository->getReports();

        return $this->render('UTTAdminBundle:Analytics:reportUser.html.twig', array(
            'admin_pool' => $this->get('sonata.admin.pool'),
            'reportUsers' => $reportUsers
        ));
    }

    public function reportRefererAction(Request $request){
        /** @var UTTAclService $uttAclService */
        $uttAclService = $this->container->get('utt.aclService');
        $uttAclService->authForAnalyticsReportReferer();

        /** @var ReportRefererRepository $reportRefererRepository */
        $reportRefererRepository = $this->getDoctrine()->getManager()->getRepository('UTTAnalyticsBundle:ReportReferer');

        $reportReferers = $reportRefererRepository->findAll();
        $reportRefererAllowedTypes = ReportReferer::getAllowedRefererTypesStatic();
        $allNewsletterCampaigns = array();
        $allWebsiteCampaigns = array();
        $allDirectCampaigns = array();
        $allDiscountCampaigns = array();
        if($reportReferers){
            /** @var ReportReferer $reportReferer */
            foreach($reportReferers as $reportReferer){
                if($reportReferer->getRefererType() == ReportReferer::REFERER_TYPE_NEWSLETTER){
                    $exists = false;
                    foreach($allNewsletterCampaigns as $newsletterCampaign){
                        if($newsletterCampaign == $reportReferer->getReferer()){ $exists = true; }
                    }

                    if(!$exists){ $allNewsletterCampaigns[] = $reportReferer->getReferer(); }
                }

                if($reportReferer->getRefererType() == ReportReferer::REFERER_TYPE_WEBSITE){
                    $exists = false;
                    foreach($allWebsiteCampaigns as $websiteCampaign){
                        if($websiteCampaign == $reportReferer->getReferer()){ $exists = true; }
                    }

                    if(!$exists){ $allWebsiteCampaigns[] = $reportReferer->getReferer(); }
                }

                if($reportReferer->getRefererType() == ReportReferer::REFERER_TYPE_DIRECT){
                    $exists = false;
                    foreach($allDirectCampaigns as $directCampaign){
                        if(strtolower($directCampaign) == strtolower($reportReferer->getReservation()->getDiscountPriceDecoded()->getDiscountCode())){ $exists = true; }
                    }

                    if(!$exists){ $allDirectCampaigns[] = $reportReferer->getReservation()->getDiscountPriceDecoded()->getDiscountCode(); }
                }

                if($reportReferer->getRefererType() == ReportReferer::REFERER_TYPE_DISCOUNT_CODE){
                    $exists = false;
                    foreach($allDiscountCampaigns as $discountCampaign){
                        if($discountCampaign == $reportReferer->getReferer()){ $exists = true; }
                    }

                    if(!$exists){ $allDiscountCampaigns[] = $reportReferer->getReferer(); }
                }
            }
        }

        return $this->render('UTTAdminBundle:Analytics:reportReferer.html.twig', array(
            'admin_pool' => $this->get('sonata.admin.pool'),
            'reportReferers' => $reportReferers,
            'reportRefererAllowedTypes' => $reportRefererAllowedTypes,
            'allWebsiteCampaigns' => $allWebsiteCampaigns,
            'allDiscountCampaigns' => $allDiscountCampaigns,
            'allDirectCampaigns' => $allDirectCampaigns,
            'allNewsletterCampaigns' => $allNewsletterCampaigns
        ));
    }

}
