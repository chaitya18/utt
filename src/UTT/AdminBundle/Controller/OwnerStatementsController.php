<?php

namespace UTT\AdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use UTT\EstateBundle\Entity\EstateRepository;
use Symfony\Component\HttpFoundation\Request;
use UTT\ReservationBundle\Entity\OwnerStatement;
use UTT\ReservationBundle\Entity\OwnerStatementRepository;
use UTT\ReservationBundle\Entity\ReservationRepository;
use UTT\EstateBundle\Entity\Estate;
use UTT\ReservationBundle\Service\OwnerStatementService;
use UTT\IndexBundle\Service\ConfigurationService;
use UTT\IndexBundle\Entity\Configuration;
use UTT\QueueBundle\Service\PropertyStatementQueueService;
use UTT\QueueBundle\Service\PropertyStatementEmailQueueService;
use UTT\AdminBundle\Service\UTTAclService;
use UTT\UserBundle\Entity\User;

class OwnerStatementsController extends Controller
{
    public function emailPropertyStatementsAction(Request $request, $date){
        /** @var UTTAclService $uttAclService */
        $uttAclService = $this->container->get('utt.aclService');
        $uttAclService->authForOwnerStatementsCreate();

        try{
            $dateObject = new \Datetime($date);
        }catch (\Exception $e){
            return $this->redirect($this->generateUrl('utt_admin_owner_statements'));
        }

        /** @var OwnerStatementRepository $ownerStatementRepository */
        $ownerStatementRepository = $this->getDoctrine()->getManager()->getRepository('UTTReservationBundle:OwnerStatement');
        $reservationRepository = $this->getDoctrine()->getManager()->getRepository('UTTReservationBundle:Reservation');

        /** @var PropertyStatementEmailQueueService $propertyStatementEmailQueueService */
        $propertyStatementEmailQueueService = $this->get('utt.propertystatementemailqueueservice');

        if($uttAclService->isGrantedSuperAdmin()){
            $ownerStatements = $ownerStatementRepository->getStatementsForDate($dateObject, OwnerStatement::TYPE_PROPERTY_STATEMENT);
        }else{
            $ownerStatements = false;
        }

        if($ownerStatements){
            /** @var OwnerStatement $ownerStatement */
            foreach($ownerStatements as $ownerStatement){
                if($reservationRepository->owesMoneyToUTTbyDateStringAndEstateId($dateObject->format('Y-m'), $ownerStatement->getEstate()->getId())){
                 $propertyStatementEmailQueueService->pushToQueue($ownerStatement);
             }
            }

            $this->get('session')->getFlashBag()->add('sonata_flash_success', 'Property statements pushed to email queue.');
        }else{
            $this->get('session')->getFlashBag()->add('sonata_flash_error', 'No owner statements found');
        }

        $referer = $request->headers->get('referer');
        if(!$referer) $referer = $this->generateUrl('utt_admin_owner_statements');

        return $this->redirect($referer);
    }

    public function showStatementForOwnerTaxYearAction(Request $request, $ownerUserId, $year){
        /** @var UTTAclService $uttAclService */
        $uttAclService = $this->container->get('utt.aclService');
        //$uttAclService->authForOwnerStatementsCreate();

        if(!$uttAclService->isGrantedSuperAdmin()){
            $user = $this->getUser();
            if((int) $user->getId() !== (int) $ownerUserId){
                return $this->redirect($this->generateUrl('utt_admin_owner_statements'));
            }
        }

        /** @var ReservationRepository $reservationRepository */
        $reservationRepository = $this->getDoctrine()->getManager()->getRepository('UTTReservationBundle:Reservation');
        /** @var EstateRepository $estateRepository */
        $estateRepository = $this->getDoctrine()->getManager()->getRepository('UTTEstateBundle:Estate');
        /** @var UserRepository $userRepository */
        $userRepository = $this->getDoctrine()->getManager()->getRepository('UTTUserBundle:User');

        $estates = $estateRepository->getOwnedByUser($ownerUserId);
        $ownerUser = $userRepository->find($ownerUserId);

        $estatesArray = array();
        if($estates){
            foreach($estates as $estate){
                $reservations = $reservationRepository->getReservationsForTaxInfo($year, $estate->getId());

                $estatesArray[] = array(
                    'estate' => $estate,
                    'reservations' => $reservations,
                );
            }
        }

        return $this->render('UTTAdminBundle:OwnerStatements:showStatementForOwnerTaxInfo.html.twig', array(
            'admin_pool' => $this->get('sonata.admin.pool'),
            'year' => $year,
            'nowDate' => new \DateTime('now'),

            'ownerUser' => $ownerUser,
            'estates' => $estates,
            'estatesArray' => $estatesArray
        ));
    }

    public function showStatementForOwnerAction(Request $request, $ownerUserId, $date){
        /** @var UTTAclService $uttAclService */
        $uttAclService = $this->container->get('utt.aclService');
        $uttAclService->authForOwnerStatementsCreate();

        try{
            $dateObject = new \Datetime($date);
        }catch (\Exception $e){
            return $this->redirect($this->generateUrl('utt_admin_owner_statements'));
        }

        if(!$uttAclService->isGrantedSuperAdmin()){
            return $this->redirect($this->generateUrl('utt_admin_owner_statements'));
        }
        
        /** @var ReservationRepository $reservationRepository */
        $reservationRepository = $this->getDoctrine()->getManager()->getRepository('UTTReservationBundle:Reservation');
        /** @var EstateRepository $estateRepository */
        $estateRepository = $this->getDoctrine()->getManager()->getRepository('UTTEstateBundle:Estate');
        /** @var UserRepository $userRepository */
        $userRepository = $this->getDoctrine()->getManager()->getRepository('UTTUserBundle:User');

        $estates = $estateRepository->getOwnedByUser($ownerUserId);
        $ownerUser = $userRepository->find($ownerUserId);

        $estatesArray = array();
        if($estates){
            foreach($estates as $estate){
                $reservations = $reservationRepository->getReservationsByDateStringAndEstateId($dateObject->format('Y-m'), $estate->getId());

                $estatesArray[] = array(
                    'estate' => $estate,
                    'reservations' => $reservations,
                );
            }
        }

        return $this->render('UTTAdminBundle:OwnerStatements:showStatementForOwner.html.twig', array(
            'admin_pool' => $this->get('sonata.admin.pool'),
            'dateObject' => $dateObject,
            'nowDate' => new \DateTime('now'),

            'ownerUser' => $ownerUser,
            'estates' => $estates,
            'estatesArray' => $estatesArray
        ));
    }

    public function createPropertyStatementsAction(Request $request, $date){
        /** @var UTTAclService $uttAclService */
        $uttAclService = $this->container->get('utt.aclService');
        $uttAclService->authForOwnerStatementsCreate();

        try{
            $dateObject = new \Datetime($date);
        }catch (\Exception $e){
            return $this->redirect($this->generateUrl('utt_admin_owner_statements'));
        }

        /** @var EstateRepository $estateRepository */
        $estateRepository = $this->getDoctrine()->getManager()->getRepository('UTTEstateBundle:Estate');

        /** @var PropertyStatementQueueService $propertyStatementQueueService */
        $propertyStatementQueueService = $this->get('utt.propertystatementqueueservice');

        if($uttAclService->isGrantedSuperAdmin()){
            $estates = $estateRepository->getAllActiveOrHidden();
//        }elseif($uttAclService->isGrantedOwner()){
//            /** @var User $user */
//            $user = $uttAclService->userAuth();
//            $estates = $estateRepository->getActiveOwnedByUser($user->getId());
        }else{
            $estates = false;
        }
        if($estates){
            /** @var Estate $estate */
            foreach($estates as $estate){
                $propertyStatementQueueService->pushToQueue($estate, $dateObject);
            }

            $this->get('session')->getFlashBag()->add('sonata_flash_success', 'Property statements pushed to create queue.');
        }else{
            $this->get('session')->getFlashBag()->add('sonata_flash_error', 'No estates found');
        }

        $referer = $request->headers->get('referer');
        if(!$referer) $referer = $this->generateUrl('utt_admin_owner_statements');

        return $this->redirect($referer);
    }

//    public function createPropertyStatementAction(Request $request, $estateId, $date){
//        $this->container->get('utt.aclService')->authForOwnerStatements();
//
//        try{
//            $dateObject = new \Datetime($date);
//        }catch (\Exception $e){
//            return $this->redirect($this->generateUrl('utt_admin_owner_statements'));
//        }
//
//        $estate = $this->getDoctrine()->getManager()->getRepository('UTTEstateBundle:Estate')->find($estateId);
//        if(!($estate instanceof Estate)){
//            return $this->redirect($this->generateUrl('utt_admin_owner_statements'));
//        }
//        $this->container->get('utt.aclService')->authEstateOwnerUser($estate);
//
//        /** @var OwnerStatementService $ownerStatementService */
//        $ownerStatementService = $this->get('utt.ownerstatementservice');
//        try{
//            $ownerStatement = $ownerStatementService->generatePropertyStatementPDF($estate, $dateObject);
//
//            return $this->redirect($this->generateUrl('utt_admin_owner_statement_display', array('filename' => $ownerStatement->getFilename())));
//        }catch(\Exception $e){
//            $referer = $request->headers->get('referer');
//            if(!$referer) $referer = $this->generateUrl('utt_admin_owner_statements');
//
//            $this->get('session')->getFlashBag()->add('sonata_flash_error', $e->getMessage());
//
//            return $this->redirect($referer);
//        }
//    }

    public function createFullStatementAction(Request $request, $date){
        $this->container->get('utt.aclService')->authForFullStatementsCreate();

        try{
            $dateObject = new \Datetime($date);
        }catch (\Exception $e){
            return $this->redirect($this->generateUrl('utt_admin_owner_statements'));
        }

        /** @var ReservationRepository $reservationRepository */
        $reservationRepository = $this->getDoctrine()->getManager()->getRepository('UTTReservationBundle:Reservation');

        /** @var EstateRepository $estateRepository */
        $estateRepository = $this->getDoctrine()->getManager()->getRepository('UTTEstateBundle:Estate');
        $estates = $estateRepository->findAll();

        /** @var ConfigurationService $configurationService */
        $configurationService = $this->get('utt.configurationservice');
        /** @var Configuration $configuration */
        $configuration = $configurationService->get();
        if(!($configuration instanceof Configuration)){
            return $this->redirect($this->generateUrl('utt_admin_owner_statements'));
        }

        $summaryData = array();
        $i = 0;
        $pdfPage = 0;
        /** @var Estate $estate */
        foreach($estates as $estate){
            $reservations = $reservationRepository->getReservationsByDateStringAndEstateId($date, $estate->getId());
            if($reservations){
                $summaryData[$pdfPage][] = (object)array(
                    'estate' => $estate,
                    'reservations' => $reservations
                );

                $i++;
                if($i == 45){
                    $i = 0;
                    $pdfPage++;
                }
            }
        }

        /** @var \WhiteOctober\TCPDFBundle\Controller\TCPDFController $tcpdfController */
        $tcpdfController = $this->get("white_october.tcpdf");
        $pdf = $tcpdfController->create();
        $pdf->SetAutoPageBreak(true, 6);
        $pdf->setPrintHeader(false);
        $pdf->setPrintFooter(false);
        $pdf->SetMargins(6,6,6, true);
        $pdf->AddPage();

        $html = $this->render('UTTAdminBundle:OwnerStatements:createFullStatement.html.twig', array(
            'dateObject' => $dateObject,
            'nowDate' => new \DateTime('now'),
            'summaryData' => $summaryData,
            'companyName' => $configuration->getCompanyName()
        ))->getContent();
        $pdf->writeHTML($html);

        /** @var OwnerStatementService $ownerStatementService */
        $ownerStatementService = $this->get('utt.ownerstatementservice');
        $ownerStatementDirectory = $ownerStatementService->getFileDirectory();
        $fileName = $ownerStatementService->generateFullStatementFileName($dateObject);

        try{
            $pdf->Output($ownerStatementDirectory.$fileName, 'F');

            /** @var OwnerStatement $ownerStatement */
            $ownerStatement = $ownerStatementService->createFullStatement($dateObject, $fileName);
            if($ownerStatement instanceof OwnerStatement){
                return $this->redirect($this->generateUrl('utt_admin_owner_statement_display', array('filename' => $ownerStatement->getFilename())));
            }else{
                throw new \Exception('Full statement for '.$dateObject->format('F Y').' not created. Try again.');
            }
        }catch(\Exception $e){
            $referer = $request->headers->get('referer');
            if(!$referer) $referer = $this->generateUrl('utt_admin_accounts');

            $this->get('session')->getFlashBag()->add('sonata_flash_error', $e->getMessage());

            return $this->redirect($referer);
        }
    }

    public function createSummaryAction(Request $request, $date){
        $this->container->get('utt.aclService')->authForFullStatementsCreate();

        try{
            $dateObject = new \Datetime($date);
        }catch (\Exception $e){
            return $this->redirect($this->generateUrl('utt_admin_owner_statements'));
        }

        /** @var ReservationRepository $reservationRepository */
        $reservationRepository = $this->getDoctrine()->getManager()->getRepository('UTTReservationBundle:Reservation');

        /** @var EstateRepository $estateRepository */
        $estateRepository = $this->getDoctrine()->getManager()->getRepository('UTTEstateBundle:Estate');
        $estates = $estateRepository->findAll();

        /** @var ConfigurationService $configurationService */
        $configurationService = $this->get('utt.configurationservice');
        /** @var Configuration $configuration */
        $configuration = $configurationService->get();
        if(!($configuration instanceof Configuration)){
            return $this->redirect($this->generateUrl('utt_admin_owner_statements'));
        }

        $summaryData = array();
        $i = 0;
        $pdfPage = 0;
        /** @var Estate $estate */
        foreach($estates as $estate){
            $reservations = $reservationRepository->getReservationsByDateStringAndEstateId($date, $estate->getId());
            if($reservations){
                $summaryData[$pdfPage][] = (object)array(
                    'estate' => $estate,
                    'reservations' => $reservations
                );

                $i++;
                if($i == 45){
                    $i = 0;
                    $pdfPage++;
                }
            }
        }

        /** @var \WhiteOctober\TCPDFBundle\Controller\TCPDFController $tcpdfController */
        $tcpdfController = $this->get("white_october.tcpdf");
        $pdf = $tcpdfController->create();
        $pdf->SetAutoPageBreak(true, 6);
        $pdf->setPrintHeader(false);
        $pdf->setPrintFooter(false);
        $pdf->SetMargins(6,6,6, true);
        $pdf->AddPage();

        $html = $this->render('UTTAdminBundle:OwnerStatements:createSummary.html.twig', array(
            'dateObject' => $dateObject,
            'nowDate' => new \DateTime('now'),
            'summaryData' => $summaryData,
            'companyName' => $configuration->getCompanyName()
        ))->getContent();
        $pdf->writeHTML($html);

        /** @var OwnerStatementService $ownerStatementService */
        $ownerStatementService = $this->get('utt.ownerstatementservice');
        $ownerStatementDirectory = $ownerStatementService->getFileDirectory();
        $fileName = $ownerStatementService->generateSummaryFileName($dateObject);

        try{
            $pdf->Output($ownerStatementDirectory.$fileName, 'F');

            /** @var OwnerStatement $ownerStatement */
            $ownerStatement = $ownerStatementService->createFullSummaryStatement($dateObject, $fileName);
            if($ownerStatement instanceof OwnerStatement){
                return $this->redirect($this->generateUrl('utt_admin_owner_statement_display', array('filename' => $ownerStatement->getFilename())));
            }else{
                throw new \Exception('Summary for '.$dateObject->format('F Y').' not created. Try again.');
            }
        }catch(\Exception $e){
            $referer = $request->headers->get('referer');
            if(!$referer) $referer = $this->generateUrl('utt_admin_accounts');

            $this->get('session')->getFlashBag()->add('sonata_flash_error', $e->getMessage());

            return $this->redirect($referer);
        }
    }

    public function displayAction(Request $request, $filename){
        $this->container->get('utt.aclService')->authForOwnerStatements();

        /** @var OwnerStatementRepository $ownerStatementRepository */
        $ownerStatementRepository = $this->getDoctrine()->getManager()->getRepository('UTTReservationBundle:OwnerStatement');
        $ownerStatement = $ownerStatementRepository->findOneBy(array(
            'filename' => $filename
        ));
        if($ownerStatement instanceOf OwnerStatement){
            if($ownerStatement->getType() == OwnerStatement::TYPE_PROPERTY_STATEMENT){
                $this->container->get('utt.aclService')->authEstateOwnerUser($ownerStatement->getEstate());
            }

            $file = $ownerStatement->getUploadDir().'/'.$ownerStatement->getFilename();
	    //var_dump($file, $_SERVER["DOCUMENT_ROOT"]);exit(1);

            header('Content-type: application/pdf');
            header('Content-Disposition: inline; filename="' . $filename . '"');
            header('Content-Transfer-Encoding: binary');
            header('Accept-Ranges: bytes');
            @readfile($file);
            exit;
        }

        return $this->redirect($this->generateUrl('utt_admin_owner_statements'));
    }

    public function indexAction(Request $request, $date = null){
        /** @var UTTAclService $uttAclService */
        $uttAclService = $this->container->get('utt.aclService');
        $uttAclService->authForOwnerStatements();

        if(is_null($date)){
            $nowDate = new \Datetime('now');
            $date = $nowDate->format('Y-m');
        }

        try{
            $dateObject = new \Datetime($date);
        }catch (\Exception $e){
            return $this->redirect($this->generateUrl('utt_admin_owner_statements'));
        }

        /** @var OwnerStatementRepository $ownerStatementRepository */
        $ownerStatementRepository = $this->getDoctrine()->getManager()->getRepository('UTTReservationBundle:OwnerStatement');
        $ownerStatements = $ownerStatementRepository->getStatementsForDate($dateObject, OwnerStatement::TYPE_PROPERTY_STATEMENT);

        /** @var EstateRepository $estateRepository */
        $estateRepository = $this->getDoctrine()->getManager()->getRepository('UTTEstateBundle:Estate');

        if($uttAclService->isGrantedSuperAdmin()){
            $estates = $estateRepository->getAllActiveOrHidden();
        }elseif($uttAclService->isGrantedOwner()){
            /** @var User $user */
            $user = $uttAclService->userAuth();
            $estates = $estateRepository->getActiveOwnedByUser($user->getId());
        }else{
            $estates = false;
        }

        /** @var PropertyStatementQueueService $propertyStatementQueueService */
        $propertyStatementQueueService = $this->get('utt.propertystatementqueueservice');

        $startDate = new \Datetime('2012-08-01');
        $endDate = new \Datetime('now');
        $interval = new \DateInterval('P1M');
        $period   = new \DatePeriod($startDate, $interval, $endDate);

        $dates = array();
        /** @var \DateTime $periodDate */
        foreach ($period as $periodDate) {
            $dates[] = json_decode(json_encode(array(
                'id' => $periodDate->format('Y-m'),
                'label' => $periodDate->format('F Y'),
            )));
        }

        $owners = array();
        $ownersWithBookings = array();
        if($uttAclService->isGrantedSuperAdmin()){
            $owners = $this->getDoctrine()->getManager()->getRepository('UTTUserBundle:User')->getOwners();

            /** @var ReservationRepository $reservationRepository */
            $reservationRepository = $this->getDoctrine()->getManager()->getRepository('UTTReservationBundle:Reservation');
            /** @var EstateRepository $estateRepository */
            $estateRepository = $this->getDoctrine()->getManager()->getRepository('UTTEstateBundle:Estate');

            if($owners){
                foreach($owners as $owner){
                    $hasBookings = false;
                    $ownerEstates = $estateRepository->getOwnedByUser($owner->getId());

                    if($ownerEstates){
                        foreach($ownerEstates as $ownerEstate){
                            $reservations = $reservationRepository->getReservationsByDateStringAndEstateId($dateObject->format('Y-m'), $ownerEstate->getId());
                            if($reservations){
                                $hasBookings = true;
                                break;
                            }
                        }
                    }

                    if($hasBookings){
                        $ownersWithBookings[] = $owner;
                    }
                }
            }
        }

        return $this->render('UTTAdminBundle:OwnerStatements:index.html.twig', array(
            'admin_pool' => $this->get('sonata.admin.pool'),
            'estates' => $estates,
            'dates' => $dates,
            'dateString' => $date,
            'ownerStatements' => $ownerStatements,
            'propertyStatementQueues' => $propertyStatementQueueService->getQueueForDate($dateObject),
            'nowDate' => new \DateTime('now'),

            'owners' => $ownersWithBookings
        ));
    }
}
