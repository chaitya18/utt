<?php

namespace UTT\AdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use UTT\ReservationBundle\Entity\ReservationRepository;
use Symfony\Component\HttpFoundation\Request;
use UTT\AdminBundle\Service\UTTAclService;
use UTT\EstateBundle\Entity\EstateRepository;
use UTT\EstateBundle\Entity\Estate;
use UTT\UserBundle\Entity\User;
use UTT\ReservationBundle\Entity\Reservation;

class AnnualReportController extends Controller
{
    public function renderFiltersAction($year, $estateId){
        /** @var UTTAclService $uttAclService */
        $uttAclService = $this->container->get('utt.aclService');
        $uttAclService->authForAnnualReport();

        $startDate = new \Datetime('2012-08-01');
        $endDate = new \Datetime('now');
        $endDate->modify('+1 year');
        $interval = new \DateInterval('P1Y');
        $period   = new \DatePeriod($startDate, $interval, $endDate);

        $years = array();
        /** @var \DateTime $periodDate */
        foreach ($period as $periodDate) {
            $years[] = json_decode(json_encode(array(
                'id' => $periodDate->format('Y'),
                'label' => $periodDate->format('Y'),
            )));
        }

        /** @var EstateRepository $estateRepository */
        $estateRepository = $this->getDoctrine()->getManager()->getRepository('UTTEstateBundle:Estate');

        if($uttAclService->isGrantedSuperAdmin() || $uttAclService->isGrantedAcc()){
            $estates = $estateRepository->findAll();
        }elseif($uttAclService->isGrantedOwner()){
            /** @var User $user */
            $user = $uttAclService->userAuth();
            $estates = $estateRepository->getOwnedByUser($user->getId());
        }else{
            $estates = false;
        }

        return $this->render('UTTAdminBundle:AnnualReport:renderFilters.html.twig', array(
            'years' => $years,
            'estates' => $estates,
            'yearString' => $year,
            'estateId' => $estateId
        ));
    }

    public function byYearAndEstateAction(Request $request, $year, $estateId){
        /** @var UTTAclService $uttAclService */
        $uttAclService = $this->container->get('utt.aclService');
        $uttAclService->authForAnnualReport();

        /** @var EstateRepository $estateRepository */
        $estateRepository = $this->getDoctrine()->getManager()->getRepository('UTTEstateBundle:Estate');

        $estate = $estateRepository->find($estateId);
        if(!($estate instanceof Estate)){
            return $this->redirect($this->generateUrl('utt_admin_annual_report'));
        }

        $this->container->get('utt.aclService')->authEstateOwnerUser($estate);

        /** @var ReservationRepository $reservationRepository */
        $reservationRepository = $this->getDoctrine()->getManager()->getRepository('UTTReservationBundle:Reservation');

        $isGrantedSuperAdmin = false;
        if($uttAclService->isGrantedSuperAdmin() || $uttAclService->isGrantedAcc()){
            $isGrantedSuperAdmin = true;
            $reservations = $reservationRepository->getReservationsByYearStringAndEstateId($year, $estateId, null, false);
        }elseif($uttAclService->isGrantedOwner()){
            /** @var User $user */
            $user = $uttAclService->userAuth();

            $reservations = $reservationRepository->getReservationsByYearStringAndEstateId($year, $estateId, $user->getId(), false);
        }else{
            $reservations = false;
        }

        $reservationsArray = array();
        if($reservations){
            /** @var Reservation $reservation */
            foreach($reservations as $reservation){
                /** @var \DateTime $reservationDate */
                $reservationDate = $reservation->getFromDate();
                $reservationMonth = $reservationDate->format('m');
                if(!array_key_exists($reservationMonth, $reservationsArray)){
                    $reservationsArray[$reservationMonth] = array(
                        'month' => $reservationMonth,
                        'monthName' => $reservationDate->format('F'),
                        'reservations' => array()
                    );
                }

                $reservationsArray[$reservationMonth]['reservations'][] = $reservation;
            }
        }

        return $this->render('UTTAdminBundle:AnnualReport:byYearAndEstate.html.twig', array(
            'admin_pool' => $this->get('sonata.admin.pool'),
            'reservationsArray' => $reservationsArray,
            'estateId' => $estateId,
            'yearString' => $year,
            'isGrantedSuperAdmin' => $isGrantedSuperAdmin
        ));
    }

    public function indexAction(Request $request){
        /** @var UTTAclService $uttAclService */
        $uttAclService = $this->container->get('utt.aclService');
        $uttAclService->authForAnnualReport();

        /** @var EstateRepository $estateRepository */
        $estateRepository = $this->getDoctrine()->getManager()->getRepository('UTTEstateBundle:Estate');

        if($uttAclService->isGrantedSuperAdmin() || $uttAclService->isGrantedAcc()){
            $estate = $estateRepository->getFirstActiveEstate();
        }elseif($uttAclService->isGrantedOwner()){
            /** @var User $user */
            $user = $uttAclService->userAuth();
            $estate = $estateRepository->getFirstActiveEstateOwnedByUser($user->getId());
        }else{
            $estate = false;
        }

        if($estate instanceof Estate){
            $nowDate = new \DateTime('now');

            return $this->redirect($this->generateUrl('utt_admin_annual_report_by_year_and_estate', array(
                'year' => $nowDate->format('Y'),
                'estateId' => $estate->getId()
            )));
        }

        return $this->redirect($this->generateUrl('sonata_admin_dashboard'));
    }
}
