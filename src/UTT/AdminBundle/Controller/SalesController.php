<?php

namespace UTT\AdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use UTT\ReservationBundle\Entity\ReservationRepository;
use Symfony\Component\HttpFoundation\Request;
use UTT\AdminBundle\Service\UTTAclService;
use UTT\EstateBundle\Entity\EstateRepository;
use UTT\EstateBundle\Entity\Estate;
use UTT\UserBundle\Entity\User;
use Symfony\Component\HttpFoundation\JsonResponse;
use UTT\ReservationBundle\Entity\PaymentTransactionRepository;
use UTT\ReservationBundle\Entity\VoucherPaymentTransactionRepository;
use UTT\ReservationBundle\Entity\CharityDonationRepository;

class SalesController extends Controller
{
    public function byDateAction(Request $request, $date){
        /** @var UTTAclService $uttAclService */
        $uttAclService = $this->container->get('utt.aclService');
        $uttAclService->authForSales();

        /** @var ReservationRepository $reservationRepository */
        $reservationRepository = $this->getDoctrine()->getManager()->getRepository('UTTReservationBundle:Reservation');

        if($uttAclService->isGrantedSuperAdmin() || $uttAclService->isGrantedAcc()){
            $reservations = $reservationRepository->getReservationsByDateStringForSales($date);
        }elseif($uttAclService->isGrantedOwner()){
            /** @var User $user */
            $user = $uttAclService->userAuth();
            $reservations = $reservationRepository->getReservationsByDateStringForSales($date, $user->getId());
        }else{
            $reservations = false;
        }

        return $this->render('UTTAdminBundle:Sales:byDate.html.twig', array(
            'admin_pool' => $this->get('sonata.admin.pool'),
            'reservations' => $reservations,
            'dateString' => $date
        ));
    }

    public function salesByYearUntilTodayAction(Request $request){
        /** @var UTTAclService $uttAclService */
        $uttAclService = $this->container->get('utt.aclService');
        $uttAclService->authForSales();

        $result = array('success' => false);

        /** @var ReservationRepository $reservationRepository */
        $reservationRepository = $this->getDoctrine()->getManager()->getRepository('UTTReservationBundle:Reservation');

        if($uttAclService->isGrantedSuperAdmin() || $uttAclService->isGrantedAcc()){
            $salesByYear = $reservationRepository->getForSalesByYearArray(null, true);
        }elseif($uttAclService->isGrantedOwner()){
            /** @var User $user */
            $user = $uttAclService->userAuth();

            $salesByYear = $reservationRepository->getForSalesByYearArray($user->getId(), true);
        }else{
            $salesByYear = false;
        }

        if($salesByYear){
            $result = array('success' => true, 'sales' => $salesByYear);
        }

        return new JsonResponse($result);
    }

    public function numberOfPropertiesOverTimeAction(Request $request){
        /** @var UTTAclService $uttAclService */
        $uttAclService = $this->container->get('utt.aclService');
        $uttAclService->authForSales();

        /** @var EstateRepository $estateRepository */
        $estateRepository = $this->getDoctrine()->getManager()->getRepository('UTTEstateBundle:Estate');

        if($uttAclService->isGrantedSuperAdmin() || $uttAclService->isGrantedAcc()){
            $start = new \DateTime('now');
            $start->modify('-5 years');
            $end = new \DateTime('now');
            $interval = new \DateInterval('P3M');
            $period = new \DatePeriod($start, $interval, $end);

            $chartData = array();

            /** @var \Datetime $periodDate */
            foreach($period as $periodDate) {
                $chartData[] = array(
                    'day' => $periodDate->format('Y-m-d'),
                    'numberOfProperties' => $estateRepository->findActivePropertiesOverTime($periodDate)
                );
            }

            return new JsonResponse(array('success' => true, 'data' => $chartData));
        }

        return new JsonResponse(array('success' => false));
    }

    public function salesByMonthUntilTodayAction(Request $request){
        /** @var UTTAclService $uttAclService */
        $uttAclService = $this->container->get('utt.aclService');
        $uttAclService->authForSales();

        $result = array('success' => false);

        /** @var ReservationRepository $reservationRepository */
        $reservationRepository = $this->getDoctrine()->getManager()->getRepository('UTTReservationBundle:Reservation');

        if($uttAclService->isGrantedSuperAdmin() || $uttAclService->isGrantedAcc()){
            $salesByYear = $reservationRepository->getForSalesByMonthUntilTodayArray(null);
        }elseif($uttAclService->isGrantedOwner()){
            /** @var User $user */
            $user = $uttAclService->userAuth();

            $salesByYear = $reservationRepository->getForSalesByMonthUntilTodayArray($user->getId());
        }else{
            $salesByYear = false;
        }

        if($salesByYear){
            $result = array('success' => true, 'sales' => $salesByYear);
        }

        return new JsonResponse($result);
    }

    public function salesByMonthAction(Request $request){
        /** @var UTTAclService $uttAclService */
        $uttAclService = $this->container->get('utt.aclService');
        $uttAclService->authForSales();

        $result = array('success' => false);

        /** @var ReservationRepository $reservationRepository */
        $reservationRepository = $this->getDoctrine()->getManager()->getRepository('UTTReservationBundle:Reservation');

        $dateForMonth = new \DateTime('now');
        $dateForMonth = $dateForMonth->modify('-3 years');
        $dateForMonth->setDate($dateForMonth->format('Y'), 1, 1)->setTime(0,0,0);

        if($uttAclService->isGrantedSuperAdmin() || $uttAclService->isGrantedAcc()){
            $salesByMonth = $reservationRepository->getForSalesByMonthArray($dateForMonth, null, 'ASC');
        }elseif($uttAclService->isGrantedOwner()){
            /** @var User $user */
            $user = $uttAclService->userAuth();

            $salesByMonth = $reservationRepository->getForSalesByMonthArray($dateForMonth, $user->getId(), 'ASC');
        }else{
            $salesByMonth = false;
        }

        if($salesByMonth){
            $sales = $this->groupSales($salesByMonth,'year');
            $result = array('success' => true, 'sales' => $sales);
        }

        return new JsonResponse($result);
    }

    public function charityLiabilityAction(Request $request){
        /** @var CharityDonationRepository $charityDonationRepository */
        $charityDonationRepository = $this->getDoctrine()->getManager()->getRepository('UTTReservationBundle:CharityDonation');
        /** @var PaymentTransactionRepository $paymentTransactionRepository */
        $paymentTransactionRepository = $this->getDoctrine()->getManager()->getRepository('UTTReservationBundle:PaymentTransaction');
        /** @var VoucherPaymentTransactionRepository $voucherPaymentTransactionRepository */
        $voucherPaymentTransactionRepository = $this->getDoctrine()->getManager()->getRepository('UTTReservationBundle:VoucherPaymentTransaction');

        $charityDonations = $charityDonationRepository->getDonationsSum();

        $charityPayments = (float) $paymentTransactionRepository->getCharityPaymentsSum();
        $charityPayments = (float) $charityPayments + (float) $voucherPaymentTransactionRepository->getCharityPaymentsSum();

        return new JsonResponse(array(
            'success' => true,
            'data' => array(
                'charityDonations' => $charityDonations,
                'charityPayments' => $charityPayments
            )
        ));
    }

    public function indexAction(Request $request){
        /** @var UTTAclService $uttAclService */
        $uttAclService = $this->container->get('utt.aclService');
        $uttAclService->authForSales();

        /** @var ReservationRepository $reservationRepository */
        $reservationRepository = $this->getDoctrine()->getManager()->getRepository('UTTReservationBundle:Reservation');

        $dateForDay = new \DateTime('now');;
        $dateForDay->modify('-4 weeks');

        $dateForMonth = new \DateTime('now');
        $dateForMonth = $dateForMonth->modify('-3 years');
        $dateForMonth->setDate($dateForMonth->format('Y'), 1, 1)->setTime(0,0,0);

        if($uttAclService->isGrantedSuperAdmin() || $uttAclService->isGrantedAcc()){
            $salesByDay = $reservationRepository->getForSalesByDayArray($dateForDay);
            $salesByDayRefundDue = $reservationRepository->getForSalesByDayRefundArray($dateForDay);
            $salesByMonth = $reservationRepository->getForSalesByMonthArray($dateForMonth);
            $salesByMonthRefundDue = $reservationRepository->getForSalesByMonthRefundArray($dateForMonth);
            $salesByYear = $reservationRepository->getForSalesByYearArray();
        }elseif($uttAclService->isGrantedOwner()){
            /** @var User $user */
            $user = $uttAclService->userAuth();

            $salesByDay = $reservationRepository->getForSalesByDayArray($dateForDay, $user->getId());
            $salesByDayRefundDue = $reservationRepository->getForSalesByDayRefundArray($dateForDay, $user->getId());
            $salesByMonth = $reservationRepository->getForSalesByMonthArray($dateForMonth, $user->getId());
            $salesByMonthRefundDue = $reservationRepository->getForSalesByMonthRefundArray($dateForMonth, $user->getId());
            $salesByYear = $reservationRepository->getForSalesByYearArray($user->getId());
        }else{
            $salesByDay = false;
            $salesByDayRefundDue = false;
            $salesByMonth = false;
            $salesByYear = false;
            $salesByMonthRefundDue = false;
        }

        $newSalesByDay = array();

        $nowDate = new \DateTime('now');
        $start = $nowDate->modify('-4 weeks');
        $end = new \DateTime('now');
        $end->modify('+1 day');
        $interval = new \DateInterval('P1D');
        $period = new \DatePeriod($start, $interval, $end);

        /** @var \Datetime $periodDate */
        foreach($period as $periodDate){
            $salesFound = false;

            if($salesByDay){
                foreach($salesByDay as $daySales){
                    if($periodDate->format('Y-m-d') == $daySales['day']){
                        $newSalesByDay[] = $daySales;
                        $salesFound = true;
                    }
                }
            }

            if(!$salesFound){
                $newSalesByDay[] = array(
                    'reservationCount' => 0,
                    'priceSum' => 0,
                    'day' => $periodDate->format('Y-m-d'),
                    'nights' => 0,
                    'adminChargeSum' => 0,
                    'bookingProtectChargeSum' => 0,
                    'commissionValueSum' => 0,
                    'commissionValueFull' => 0,
                    'paidSum' => 0
                );
            }
        }

        /** @var PaymentTransactionRepository $paymentTransactionRepository */
        $paymentTransactionRepository = $this->getDoctrine()->getManager()->getRepository('UTTReservationBundle:PaymentTransaction');
        $charityByDay = $paymentTransactionRepository->getCharityPaymentsByDayArray();
        $charityByMonth = $paymentTransactionRepository->getCharityPaymentsByMonthArray();

        /** @var VoucherPaymentTransactionRepository $voucherPaymentTransactionRepository */
        $voucherPaymentTransactionRepository = $this->getDoctrine()->getManager()->getRepository('UTTReservationBundle:VoucherPaymentTransaction');
        $voucherCharityByDay = $voucherPaymentTransactionRepository->getCharityPaymentsByDayArray();
        $voucherCharityByMonth = $voucherPaymentTransactionRepository->getCharityPaymentsByMonthArray();

        $liabilityValue = $reservationRepository->getPaidValueForFutureBookings();

        /** @var CharityDonationRepository $charityDonationRepository */
        $charityDonationRepository = $this->getDoctrine()->getManager()->getRepository('UTTReservationBundle:CharityDonation');
        $charityDonations = $charityDonationRepository->getDonationsSum();

        $charityPayments = (float) $paymentTransactionRepository->getCharityPaymentsSum();
        $charityPayments = (float) $charityPayments + (float) $voucherPaymentTransactionRepository->getCharityPaymentsSum();
        if($salesByDayRefundDue) {
            $newSalesByDay = $this->supplementByDayRefundDue($newSalesByDay, $salesByDayRefundDue);
        }

        if($salesByMonthRefundDue) {
            $salesByMonth = $this->supplementByYearMonthRefundDue($salesByMonth, $salesByMonthRefundDue);
        }

        return $this->render('UTTAdminBundle:Sales:index.html.twig', array(
            'admin_pool' => $this->get('sonata.admin.pool'),
            'salesByDay' => array_reverse($newSalesByDay),
            'salesByMonth' => $salesByMonth,
            'salesByYear' => $salesByYear,

            'charityByDay' => $charityByDay,
            'charityByMonth' => $charityByMonth,
            'voucherCharityByDay' => $voucherCharityByDay,
            'voucherCharityByMonth' => $voucherCharityByMonth,

            'liabilityValue' => $liabilityValue,

            'charityDonations' => $charityDonations,
            'charityPayments' => $charityPayments
        ));
    }

    protected function supplementByDayRefundDue($salesByDay, $salesByDayRefundDue)
    {
        $salesByDayRefundDue = $this->groupSalesByDate($salesByDayRefundDue);
        $sales = array();
        foreach ($salesByDay as $itemSales) {
            if (isset($itemSales['day'])) {
                $day = $itemSales['day'];
                if (isset($salesByDayRefundDue[$day])) {
                    $itemSales['refundDueTotal'] = $salesByDayRefundDue[$day];
                }
                $sales[] = $itemSales;
            }
        }
        return $sales;
    }


    protected function supplementByYearMonthRefundDue($salesByMonth, $salesByMonthRefundDue)
    {
        $salesByMonthRefundDue = $this->groupSales($salesByMonthRefundDue);
        $sales = array();
        foreach ($salesByMonth as $itemSales) {
            if (isset($itemSales['month'])) {
                $monthArray = explode('-', $itemSales['month']);
                $year = (int)$monthArray[0];
                $month = (int)$monthArray[1];
                if (isset($salesByMonthRefundDue[$year][$month])) {
                    $itemSales['refundDueTotal'] = $salesByMonthRefundDue[$year][$month];
                }
                $sales[] = $itemSales;
            }
        }
        return $sales;
    }

    protected function groupSalesByDate($salesInput)
    {
        $sales = array();
        foreach ($salesInput as $salesItem) {
            if (isset($salesItem['day'])) {
                $day = $salesItem['day'];
                $sales[$day] = $salesItem;
            }
        }
        ksort($sales);
        return $sales;
    }

    /**
     * @param $salesByMonth
     * @return array
     */
    protected function groupSales($salesByMonth, $by = 'month')
    {
        $sales = array();
        foreach ($salesByMonth as $salesItem) {
            if (isset($salesItem['month'])) {
                $monthArray = explode('-', $salesItem['month']);
                $year = (int)$monthArray[0];
                $month = (int)$monthArray[1];

                if (!isset($sales[$year])) {
                    $sales[$year] = array();
                }
                if ($by === 'month') {
                    $sales[$year][$month] = $salesItem;
                } else {
                    $sales[$year][] = $salesItem;
                }

            }
        }
        ksort($sales);
        return $sales;
    }
}
