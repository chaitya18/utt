<?php

namespace UTT\AdminBundle\Block;

use Symfony\Component\HttpFoundation\Response;

use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Validator\ErrorElement;

use Sonata\BlockBundle\Model\BlockInterface;
use Sonata\BlockBundle\Block\BaseBlockService;
use Sonata\BlockBundle\Block\BlockContextInterface;

use Symfony\Bundle\FrameworkBundle\Templating\EngineInterface;
use Doctrine\ORM\EntityManager;

use UTT\ReservationBundle\Entity\Reservation;
use UTT\ReservationBundle\Entity\ReservationRepository;
use UTT\AdminBundle\Service\UTTAclService;
use UTT\UserBundle\Entity\User;

class BookingActionListBlockService extends BaseBlockService
{
    protected $_em;
    protected $uttAclService;

    public function __construct($name, EngineInterface $templating, EntityManager $em, UTTAclService $uttAclService)
    {
        $this->_em = $em;
        $this->uttAclService = $uttAclService;
        parent::__construct($name, $templating);
    }

    public function getName()
    {
        return 'Booking Action List';
    }

    public function getDefaultSettings()
    {
        return array();
    }

    public function validateBlock(ErrorElement $errorElement, BlockInterface $block)
    {
    }

    public function buildEditForm(FormMapper $formMapper, BlockInterface $block)
    {
    }

    public function execute(BlockContextInterface $blockContext, Response $response = null)
    {
        // merge settings
        $settings = array_merge($this->getDefaultSettings(), $blockContext->getSettings());

        /** @var ReservationRepository $reservationRepository */
        $reservationRepository = $this->_em->getRepository('UTTReservationBundle:Reservation');

        if($this->uttAclService->isGrantedOwner()){
            /** @var User $user */
            $user = $this->uttAclService->userAuth();
            $reservations = $reservationRepository->getActionListReservations(20, $user->getId());
        }elseif($this->uttAclService->isGrantedCleaner()){
            $reservations = null;
        }else{
            $reservations = $reservationRepository->getActionListReservations(20);
        }

        return $this->renderResponse('UTTAdminBundle:Admin:block_booking_action_list.html.twig', array(
            'block'     => $blockContext->getBlock(),
            'settings'  => $settings,
            'reservations' => $reservations,
            'referredFlags' => Reservation::getAllowedReferredFlagsStatic()
        ), $response);
    }
}