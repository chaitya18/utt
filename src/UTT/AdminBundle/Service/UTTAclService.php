<?php

namespace UTT\AdminBundle\Service;

use UTT\EstateBundle\Entity\Estate;
use UTT\IndexBundle\Entity\HomeBanner;
use UTT\ReservationBundle\Entity\Reservation;
use UTT\UserBundle\Entity\User;
use FOS\UserBundle\Model\UserInterface;
use Symfony\Component\Security\Core\SecurityContext;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Symfony\Component\Security\Acl\Domain\ObjectIdentity;
use Symfony\Component\Security\Acl\Domain\UserSecurityIdentity;
use Symfony\Component\Security\Acl\Permission\MaskBuilder;

class UTTAclService {
    protected $securityContext;
    protected $securityAclProvider;

    public function __construct(SecurityContext $securityContext, $securityAclProvider){
        $this->securityContext = $securityContext;
        $this->securityAclProvider = $securityAclProvider;
    }

    public function getSecurityContext() {
        return $this->securityContext;
    }

    public function userAuth($throwException = true){
        if(!$this->securityContext->isGranted('IS_AUTHENTICATED_FULLY') ){
            if($throwException){
                throw new AccessDeniedException();
            }else{
                return false;
            }
        }

        $user = $this->securityContext->getToken()->getUser();
        if (!is_object($user) || !$user instanceof UserInterface) {
            if($throwException){
                throw new AccessDeniedException();
            }else{
                return false;
            }
        }

        return $user;
    }

    public function authForUserWelcomeResend(){
        if(!$this->securityContext->isGranted('ROLE_UTT_USER_ADMIN_USER_WELCOME_RESEND')){
            throw new AccessDeniedException();
        }
    }

    public function authForEstateCalendar(Estate $estate){
        if(!$this->securityContext->isGranted('ROLE_UTT_ESTATE_ADMIN_ESTATE_CALENDAR')){
            throw new AccessDeniedException();
        }

        $this->authEstateOwnerUser($estate);
        $this->authEstateCleanerUser($estate);
    }

    public function authForEstatePhotos(Estate $estate){
        if(!$this->securityContext->isGranted('ROLE_UTT_ESTATE_ADMIN_ESTATE_PHOTOS')){
            throw new AccessDeniedException();
        }

        $this->authEstateOwnerUser($estate);
        $this->authEstateCleanerUser($estate);
    }

    public function authForHomeBannerSort(){
        if(!$this->securityContext->isGranted('ROLE_UTT_INDEX_ADMIN_HOMEBANNER_SORT')){
            throw new AccessDeniedException();
        }
    }

    public function isGrantedCleaner(){
        if($this->securityContext->isGranted('ROLE_ADMIN_CLEANER')){
            return true;
        }

        return false;
    }

    public function isGrantedOwner(){
        if($this->securityContext->isGranted('ROLE_ADMIN_OWNER')){
            return true;
        }

        return false;
    }

    public function isGrantedAcc(){
        if($this->securityContext->isGranted('ROLE_ADMIN_ACC')){
            return true;
        }

        return false;
    }

    public function isGrantedSuperAdmin(){
        if($this->securityContext->isGranted('ROLE_SUPER_ADMIN')){
            return true;
        }

        return false;
    }

    public function authForFullStatementsCreate(){
        if(!$this->securityContext->isGranted('ROLE_UTT_ADMIN_FULL_STATEMENTS_CREATE')){
            throw new AccessDeniedException();
        }
    }

    public function authForSales(){
        if(!$this->securityContext->isGranted('ROLE_UTT_ADMIN_SALES')){
            throw new AccessDeniedException();
        }
    }

    public function authForAllAvailability(){
        if(!$this->securityContext->isGranted('ROLE_UTT_ADMIN_ALL_AVAILABILITY')){
            throw new AccessDeniedException();
        }
    }

    public function authForPricingCategories(){
        if(!$this->securityContext->isGranted('ROLE_UTT_ADMIN_PRICING_CATEGORIES')){
            throw new AccessDeniedException();
        }
    }

    public function authForCleaningRota(){
        if(!$this->securityContext->isGranted('ROLE_UTT_ADMIN_CLEANING_ROTA') && !$this->securityContext->isGranted('ROLE_UTT_ADMIN_OWNER_STATEMENTS')){
            throw new AccessDeniedException();
        }
    }

    public function authForActivityLog(){
        if(!$this->securityContext->isGranted('ROLE_UTT_ADMIN_ACTIVITY_LOG')){
            throw new AccessDeniedException();
        }
    }

    public function authForAnalyticsReportUser(){
        if(!$this->securityContext->isGranted('ROLE_UTT_ADMIN_ANALYTICS_REPORT_USER')){
            throw new AccessDeniedException();
        }
    }

    public function authForAnalyticsReportReferer(){
        if(!$this->securityContext->isGranted('ROLE_UTT_ADMIN_ANALYTICS_REPORT_REFERER')){
            throw new AccessDeniedException();
        }
    }

    public function authForAccounts(){
        if(!$this->securityContext->isGranted('ROLE_UTT_ADMIN_ACCOUNTS')){
            throw new AccessDeniedException();
        }
    }

    public function authForAnnualReport(){
        if(!$this->securityContext->isGranted('ROLE_UTT_ADMIN_ANNUAL_REPORT')){
            throw new AccessDeniedException();
        }
    }

    public function authForStatistics(){
        if(!$this->securityContext->isGranted('ROLE_UTT_ADMIN_STATISTICS')){
            throw new AccessDeniedException();
        }
    }

    public function authForOwnerStatements(){
        if(!$this->securityContext->isGranted('ROLE_UTT_ADMIN_OWNER_STATEMENTS')){
            throw new AccessDeniedException();
        }
    }

    public function authForOwnerStatementsCreate(){
        if(!$this->securityContext->isGranted('ROLE_UTT_ADMIN_OWNER_STATEMENTS_CREATE')){
            throw new AccessDeniedException();
        }
    }

    public function authForEstateGeneratePricing(Estate $estate){
        if(!$this->securityContext->isGranted('ROLE_UTT_ESTATE_ADMIN_ESTATE_GENERATE_PRICING')){
            throw new AccessDeniedException();
        }

        $this->authEstateOwnerUser($estate);
    }

    public function authForReservationRequote(Reservation $reservation){
        if(!$this->securityContext->isGranted('ROLE_UTT_RESERVATION_ADMIN_RESERVATION_REQUOTE')){
            throw new AccessDeniedException();
        }

        $this->authEstateOwnerUser($reservation->getEstate());
    }

    public function authForReservationCancel(Reservation $reservation){
        if(!$this->securityContext->isGranted('ROLE_UTT_RESERVATION_ADMIN_RESERVATION_CANCEL')){
            throw new AccessDeniedException();
        }

        $this->authEstateOwnerUser($reservation->getEstate());
    }

    public function authForReservationReinstate(Reservation $reservation){
        if(!$this->securityContext->isGranted('ROLE_UTT_RESERVATION_ADMIN_RESERVATION_REINSTATE')){
            throw new AccessDeniedException();
        }

        $this->authEstateOwnerUser($reservation->getEstate());
    }

    public function authEstateOwnerUser(Estate $estate){
        if($this->securityContext->isGranted('ROLE_ADMIN_OWNER')){
            /** @var User $user */
            $user = $this->userAuth();

            $existsInCollection = false;
            /** @var User $ownerUser */
            foreach($estate->getOwnerUsers() as $ownerUser){
                if($ownerUser->getId() == $user->getId()){
                    $existsInCollection = true;
                }
            }
            if(!$existsInCollection) { throw new AccessDeniedException(); }
        }
    }

    public function authEstateCleanerUser(Estate $estate){
        if($this->securityContext->isGranted('ROLE_ADMIN_CLEANER')){
            /** @var User $user */
            $user = $this->userAuth();

            $existsInCollection = false;
            /** @var User $cleanerUser */
            foreach($estate->getCleanerUsers() as $cleanerUser){
                if($cleanerUser->getId() == $user->getId()){
                    $existsInCollection = true;
                }
            }
            if(!$existsInCollection) { throw new AccessDeniedException(); }
        }
    }

    public function updateObjectAcl($object){
        $objectIdentity = ObjectIdentity::fromDomainObject($object);
        $acl = $this->securityAclProvider->createAcl($objectIdentity);
        return true;
    }

}