var discountCodeEstatesSelectAll = {
    init: function(){
        var ob = $('select.initDiscountCodeEstatesSelectAll');
        if (ob.length > 0){
            $('div.initDiscountCodeEstatesSelectAll').before('<input type="checkbox" id="discountCodeEstatesSelectAll"> <label for="discountCodeEstatesSelectAll">Select All</label>');

            $("#discountCodeEstatesSelectAll").click(function(){
                if($("#discountCodeEstatesSelectAll").is(':checked') ){
                    ob.find('option').prop("selected","selected");
                    ob.trigger("change");
                }else{
                    ob.find('option').removeAttr("selected");
                    ob.trigger("change");
                }
            });
        }
    }
}