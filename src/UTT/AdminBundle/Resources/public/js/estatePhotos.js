var estatePhotos = {
    facebookImporter: {
        init: function(){
            var importerInput = $('.estatePhotosFacebookImporter');
            var facebookAccessTokenInput = $('.estatePhotosFacebookAccessToken');

            if (importerInput.length > 0 ){
                importerInput.after('<button id="estatePhotosFacebookImporterRun" class="btn btn-success btn-xs">Import from Facebook</button>');

                $(document).on('click', '#estatePhotosFacebookImporterRun', function(e){
                    e.preventDefault();
                    var self = $(this);
                    self.attr('disabled', 'disabled');

                    $.ajax({
                        type: "GET",
                        dataType: "json",
                        url: Routing.generate('utt_estate_import_photos_from_facebook_album', {
                            'facebookAlbumId': importerInput.val(),
                            'facebookAccessToken': facebookAccessTokenInput.val()
                        }, true),
                        success: function(data){
                            if(data){
                                if(data.success == true && data.objects){
                                    for(var i in data.objects){
                                        estatePhotos.addPhoto(data.objects[i].id, data.objects[i].fileName, data.objects[i].isHighlighted, data.objects[i].fileNameOrigin);
                                    }
                                    importerInput.val('');
                                    facebookAccessTokenInput.val('');
                                }else{ }
                            }else{ }
                            self.removeAttr('disabled');
                        },
                        error: function(xhr, err) {
                            self.removeAttr('disabled');
                        }
                    });
                })
            }
        }
    },

    init: function(){
        if ($('.estatePhotosUploader').length > 0){
            estatePhotos.buildElementsStructure();
            estatePhotos.initUploader();
            if($('.sonataFormEstateId').val() != ''){
                estatePhotos.loadEstatePhotosIds($('.sonataFormEstateId').val());
            }

            $(document).on('click', '#estatePhotosPreviews > div > button[type=button].removePhoto', function(){
                estatePhotos.deletePhoto($(this).parent().find('img'));
            });
            $(document).on('click', '#estatePhotosPreviews > div > button[type=button].rotateLeft', function(){
                estatePhotos.rotateLeft($(this).parent().find('img'));
            });
            $(document).on('click', '#estatePhotosPreviews > div > button[type=button].rotateRight', function(){
                estatePhotos.rotateRight($(this).parent().find('img'));
            });
            $(document).on('click', '#estatePhotosPreviews > div > button[type=button].setAsHighlightedPhoto', function(){
                estatePhotos.setAsHighlightedPhoto($(this).parent().find('img'));
            });
            $(document).on('click', '#estatePhotosPreviews > div > button[type=button].savePhotoTitle', function(){
                estatePhotos.savePhotoTitle($(this).parent().find('img'));
            });
            $(document).on('click', '#estatePhotosPreviews > div > button[type=button].setAsMainPhoto', function(){
                estatePhotos.setDefault($(this).parent().find('img'));
            });


            estatePhotos.facebookImporter.init();
        }
    },
    buildElementsStructure: function(){
        $('.estatePhotosUploader').hide();
        $('.estatePhotosUploader').parent().append('' +
            '<form id="upload" method="post" action="' + Routing.generate('utt_estate_upload_photo',{}, true) + '" enctype="multipart/form-data">' +
                '<div id="drop">' +
                    'Drop new images here or..<br />' +
                    '<a>Click to upload new photos [png jpg jpeg gif]</a>' +
                    '<input type="file" name="upl" multiple />' +
                '</div>' +
                '<ul></ul>' +
            '</form>' +
            '<div id="estatePhotosIdsContainer"></div>' +
            '<div id="estatePhotosSortOrderIdsContainer"></div>' +
            '<input type="hidden" name="estatePhotosUploaderRun" value="1" />' +
            '<div id="estateHighlightedPhotosIdsContainer"></div>' +
            '<div id="estatePhotosPreviews"></div>');
        $('.photoMainContainer').attr('name', 'photoMainContainer').attr('id', 'photoMainContainer');
        $("#estatePhotosPreviews").on("sortstop", function(event, ui) {
            estatePhotos.updatePhotosSortOrder();
        });
    },
    initUploader: function(){
        var ul = $('#upload ul');

        $('#drop a').click(function(){
            $(this).parent().find('input').click();
        });

        $('#upload').fileupload({
            dropZone: $('#drop'),
            add: function (e, data) {

                var tpl = $('<li class="working"><input type="text" value="0" data-width="48" data-height="48"'+
                    ' data-fgColor="#0788a5" data-readOnly="1" data-bgColor="#3e4043" /><p></p><span></span></li>');

                tpl.find('p').text(data.files[0].name).append('<i>' + formatFileSize(data.files[0].size) + '</i>');
                data.context = tpl.appendTo(ul);
                tpl.find('input').knob();

                tpl.find('span').click(function(){

                    if(tpl.hasClass('working')){
                        jqXHR.abort();
                    }

                    tpl.fadeOut(function(){
                        tpl.remove();
                    });

                });

                var jqXHR = data.submit();
            },

            progress: function(e, data){
                var progress = parseInt(data.loaded / data.total * 100, 10);
                data.context.find('input').val(progress).change();

                if(progress == 100){
                    data.context.removeClass('working');
                }
            },

            done: function(e, data){
                if(data.result){
                    var dataParsed = jQuery.parseJSON(data.result);
                    if(dataParsed.success == 1){
                        estatePhotos.addPhoto(dataParsed.object.id, dataParsed.object.fileName, false, dataParsed.object.fileNameOrigin);
                    }
                }
            },

            fail:function(e, data){
                data.context.addClass('error');
            }

        });

        $(document).on('drop dragover', function (e) {
            e.preventDefault();
        });

        function formatFileSize(bytes) {
            if (typeof bytes !== 'number') {
                return '';
            }

            if (bytes >= 1000000000) {
                return (bytes / 1000000000).toFixed(2) + ' GB';
            }

            if (bytes >= 1000000) {
                return (bytes / 1000000).toFixed(2) + ' MB';
            }

            return (bytes / 1000).toFixed(2) + ' KB';
        }
    },
    updatePhotosSortOrder: function(){
        $('#estatePhotosSortOrderIdsContainer').html('');
        $('#estatePhotosPreviews').find('img').each(function(){
            var photoId = $(this).attr('data-photo-id');
            console.log(photoId);
            $('#estatePhotosSortOrderIdsContainer').append('<input type="hidden" name="estatePhotosSortOrderIds[]" value="'+ photoId +'" />');
        });
    },
    loadEstatePhotosIds: function(estateId){
        $.ajax({
            type: "GET",
            dataType: "json",
            url: Routing.generate('utt_estate_ajax_get_photos_by_estate_id', {
                'estateId': estateId
            }, true),
            success: function(data){
                if(data){
                    if(data.success == true && data.objects){
                        for(var i in data.objects){
                            estatePhotos.addPhoto(data.objects[i].id, data.objects[i].fileName, data.objects[i].isHighlighted, data.objects[i].fileNameOrigin, data.objects[i].title);
                        }

                        if($('#photoMainContainer.photoMainContainer').val() != ''){
                            estatePhotos.setDefault(estatePhotos.getPhotoObjectById($('#photoMainContainer.photoMainContainer').val()));
                        }
                        $('#estatePhotosPreviews').sortable();
                    }else{ }
                }else{ }
            },
            error: function(xhr, err) {

            }
        });
    },
    addPhoto: function(photoId, filePath, isHighlighted, filePathOrigin, title, addAfterObject){
        if(title == undefined){ title = ''; }
        $('#estatePhotosIdsContainer').prepend('<input type="hidden" name="estatePhotosIds[]" value="'+ photoId +'" />');

        var setHighlightedButtonClass = 'btn-info';
        var setHighlightedButtonHtml = 'Set as highlighted';
        if(isHighlighted == true){
            $('#estateHighlightedPhotosIdsContainer').append('<input type="hidden" name="estateHighlightedPhotosIds[]" value="'+ photoId +'" />');
            setHighlightedButtonClass = 'btn-warning';
            setHighlightedButtonHtml = 'Unset highlighted';
        }

        var newPhotoHtml = '' +
            '<div style="display: none;">' +
            '<a target="_blank" href="/' + filePathOrigin + '"><img src="'+ filePath +'" data-photo-id="'+ photoId +'" /></a>' +
            '<button class="btn btn-danger btn-xs rotateLeft" type="button">L</button>' +
            '<button class="btn btn-danger btn-xs rotateRight" type="button">R</button>' +
            '<button class="btn btn-danger btn-xs removePhoto" type="button">Remove</button>' +
            '<button class="btn ' + setHighlightedButtonClass + ' btn-xs setAsHighlightedPhoto" type="button">' + setHighlightedButtonHtml + '</button>' +
            '<button class="btn btn-xs btn-default setAsMainPhoto" type="button">Default</button>' +
            '<textarea name="estatePhotosTitles[]">' + title + '</textarea>' +
            '<button class="btn btn-xs btn-default savePhotoTitle" type="button">Save</button>' +
            '</div>';

        if(addAfterObject == undefined){
            $('#estatePhotosPreviews').prepend(newPhotoHtml);
        }else{
            addAfterObject.after(newPhotoHtml);
        }
        $('#estatePhotosPreviews div:hidden img').load(function(){
            $(this).parent().parent().fadeIn('normal');
            if($('.photoMainContainer').val() == ''){
                estatePhotos.setDefault(estatePhotos.getFirstPhotoObject());
            }
        });
        estatePhotos.updatePhotosSortOrder();
    },
    deletePhoto: function(photoObject){
        var photoId = photoObject.attr('data-photo-id');
        $('#estatePhotosIdsContainer').find('input[value="' + photoId + '"]').remove();
        photoObject.parent().parent().fadeOut('normal', function(){
            photoObject.parent().parent().remove();

            if(photoId == $('.photoMainContainer').val()){
                estatePhotos.setDefault(estatePhotos.getFirstPhotoObject());
            }
            estatePhotos.updatePhotosSortOrder();
        });
    },
    rotate: function(photoObject, rotateType){
        var photoId = photoObject.attr('data-photo-id');

        $.ajax({
            type: "GET",
            dataType: "json",
            url: Routing.generate('utt_estate_photo_rotate', {
                'photoId': photoId,
                'rotateType': rotateType
            }, true),
            success: function(data){
                if(data){
                    if(data.success == true && data.object){
                        photoObject.parent().parent().fadeOut('normal', function(){
                            estatePhotos.addPhoto(data.object.id, data.object.fileName, data.object.isHighlighted, data.object.fileNameOrigin, data.object.title, photoObject.parent().parent());

                            if(photoId == $('.photoMainContainer').val()){
                                estatePhotos.setDefault(estatePhotos.getPhotoObjectById(data.object.id));
                            }

                            $('#estatePhotosIdsContainer').find('input[value="' + photoId + '"]').remove();
                            photoObject.parent().parent().remove();
                        });
                    }else{ }
                }else{ }
            },
            error: function(xhr, err) {

            }
        });
    },
    rotateLeft: function(photoObject){
        estatePhotos.rotate(photoObject, 'left');
    },
    rotateRight: function(photoObject){
        estatePhotos.rotate(photoObject, 'right');
    },
    savePhotoTitle: function(photoObject){
        var photoId = photoObject.attr('data-photo-id');
        var title = photoObject.parent().parent().find('textarea').val();

        $.ajax({
            type: "POST",
            dataType: "json",
            url: Routing.generate('utt_estate_ajax_save_photo_title', {}, true),
            data: {
                'photoId': photoId,
                'title': title
            },
            success: function(data){
                if(data){
                    if(data.success == true){
                        alert('Saved');
                    }else{ }
                }else{ }
            },
            error: function(xhr, err) {

            }
        });
    },
    setAsHighlightedPhoto: function(photoObject){
        var photoId = photoObject.attr('data-photo-id');
        var buttonObject = photoObject.parent().parent().find('.setAsHighlightedPhoto');

        var imgIdObject = $('#estateHighlightedPhotosIdsContainer').find('input[value="' + photoId + '"]');
        if(imgIdObject.length > 0){
            imgIdObject.remove();
            buttonObject.removeClass('btn-warning').addClass('btn-info');
            buttonObject.html('Set as highlighted')
        }else{
            $('#estateHighlightedPhotosIdsContainer').append('<input type="hidden" name="estateHighlightedPhotosIds[]" value="'+ photoId +'" />');
            buttonObject.removeClass('btn-info').addClass('btn-warning');
            buttonObject.html('Unset highlighted')
        }
    },
    getPhotoObjectById: function(photoId){
        var photoObject = null;

        $('#estatePhotosPreviews').find('img').each(function(){
            if($(this).attr('data-photo-id') == photoId){
                photoObject = $(this);
            }
        });

        return photoObject;
    },
    getFirstPhotoObject: function(){
        var firstPhotoId = $('#estatePhotosIdsContainer input').val();
        return estatePhotos.getPhotoObjectById(firstPhotoId);
    },
    setDefault: function(photoObject){
        if(is_null(photoObject)){
            $('.photoMainContainer').val('');
        }else{
            var photoId = photoObject.attr('data-photo-id');

            $('.photoMainContainer').val(photoId);
            $('#estatePhotosPreviews').find('.setAsMainPhoto').removeClass('btn-primary').addClass('btn-default').addClass('btn-xs');
            photoObject.parent().parent().find('.setAsMainPhoto').removeClass('btn-default').addClass('btn-primary').removeClass('btn-xs');
        }
    }
};
function is_null (mixed_var) {
    return (mixed_var === null);
}