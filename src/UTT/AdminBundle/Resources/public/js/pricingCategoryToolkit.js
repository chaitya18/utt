var pricingCategoryToolkit = {
    round: function(value){
        value = Math.ceil((eval(value)));
        return value.toFixed(2);
    },
    round49: function(value){
        value = Math.ceil((eval(value) + 1) / 5) * 5 -1;
        return value.toFixed(2);
    },
    round9: function(value){
        value = Math.ceil((eval(value) + 1) / 10) * 10 -1;
        return value.toFixed(2);
    },
    nudge: function(value){
        value = eval(value) + 1;
        return value.toFixed(2);
    },
    increase5: function(value){
        value = Math.round((eval(value) * 105/100) *100) / 100;
        return value.toFixed(2);
    },
    increase10: function(value){
        value = Math.round((eval(value) * 110/100) *100) / 100;
        return value.toFixed(2);
    },
    decrease5: function(value){
        value = Math.round((eval(value) * 95/100) *100) / 100;
        return value.toFixed(2);
    },
    decrease10: function(value){
        value = Math.round((eval(value) * 90/100) *100) / 100;
        return value.toFixed(2);
    },

    init: function(){
        if ($('.initPricingCategoryToolkit').length > 0){
            var container = $('.initPricingCategoryToolkit').parent();
            $('.initPricingCategoryToolkit').hide();

            container.append('<button id="toolkit_button_round" class="btn btn-info">Round £x.00</button> ');
            container.append('<button id="toolkit_button_round49" class="btn btn-info">Round £4/9</button> ');
            container.append('<button id="toolkit_button_round9" class="btn btn-info">Round £9.00</button> ');
            container.append('<button id="toolkit_button_nudge" class="btn btn-info">Nudge Prices</button> ');
            container.append('<button id="toolkit_button_increase5" class="btn btn-info">Increase Prices 5%</button> ');
            container.append('<button id="toolkit_button_increase10" class="btn btn-info">Increase Prices 10%</button> ');
            container.append('<button id="toolkit_button_decrease5" class="btn btn-info">Decrease Prices 5%</button> ');
            container.append('<button id="toolkit_button_decrease10" class="btn btn-info">Decrease Prices 10%</button> ');

            $(document).on('click', '#toolkit_button_round', function(e){
                e.preventDefault();

                $('input[id$="_priceStandingCharge"]').each(function(){ $(this).val(pricingCategoryToolkit.round($(this).val())); });
                $('input[id$="_priceFridaySaturday"]').each(function(){ $(this).val(pricingCategoryToolkit.round($(this).val())); });
                $('input[id$="_priceSundayThursday"]').each(function(){ $(this).val(pricingCategoryToolkit.round($(this).val())); });
                $('input[id$="_price7Nights"]').each(function(){ $(this).val(pricingCategoryToolkit.round($(this).val())); });
                $('input[id$="_priceMondayFriday"]').each(function(){ $(this).val(pricingCategoryToolkit.round($(this).val())); });
                $('input[id$="_priceFridayMonday"]').each(function(){ $(this).val(pricingCategoryToolkit.round($(this).val())); });
            });

            $(document).on('click', '#toolkit_button_round49', function(e){
                e.preventDefault();

                $('input[id$="_priceStandingCharge"]').each(function(){ $(this).val(pricingCategoryToolkit.round49($(this).val())); });
                $('input[id$="_priceFridaySaturday"]').each(function(){ $(this).val(pricingCategoryToolkit.round49($(this).val())); });
                $('input[id$="_priceSundayThursday"]').each(function(){ $(this).val(pricingCategoryToolkit.round49($(this).val())); });
                $('input[id$="_price7Nights"]').each(function(){ $(this).val(pricingCategoryToolkit.round49($(this).val())); });
                $('input[id$="_priceMondayFriday"]').each(function(){ $(this).val(pricingCategoryToolkit.round49($(this).val())); });
                $('input[id$="_priceFridayMonday"]').each(function(){ $(this).val(pricingCategoryToolkit.round49($(this).val())); });
            });

            $(document).on('click', '#toolkit_button_round9', function(e){
                e.preventDefault();

                $('input[id$="_priceStandingCharge"]').each(function(){ $(this).val(pricingCategoryToolkit.round9($(this).val())); });
                $('input[id$="_priceFridaySaturday"]').each(function(){ $(this).val(pricingCategoryToolkit.round9($(this).val())); });
                $('input[id$="_priceSundayThursday"]').each(function(){ $(this).val(pricingCategoryToolkit.round9($(this).val())); });
                $('input[id$="_price7Nights"]').each(function(){ $(this).val(pricingCategoryToolkit.round9($(this).val())); });
                $('input[id$="_priceMondayFriday"]').each(function(){ $(this).val(pricingCategoryToolkit.round9($(this).val())); });
                $('input[id$="_priceFridayMonday"]').each(function(){ $(this).val(pricingCategoryToolkit.round9($(this).val())); });
            });

            $(document).on('click', '#toolkit_button_nudge', function(e){
                e.preventDefault();

                $('input[id$="_priceStandingCharge"]').each(function(){ $(this).val(pricingCategoryToolkit.nudge($(this).val())); });
                $('input[id$="_priceFridaySaturday"]').each(function(){ $(this).val(pricingCategoryToolkit.nudge($(this).val())); });
                $('input[id$="_priceSundayThursday"]').each(function(){ $(this).val(pricingCategoryToolkit.nudge($(this).val())); });
                $('input[id$="_price7Nights"]').each(function(){ $(this).val(pricingCategoryToolkit.nudge($(this).val())); });
                $('input[id$="_priceMondayFriday"]').each(function(){ $(this).val(pricingCategoryToolkit.nudge($(this).val())); });
                $('input[id$="_priceFridayMonday"]').each(function(){ $(this).val(pricingCategoryToolkit.nudge($(this).val())); });
            });

            $(document).on('click', '#toolkit_button_increase5', function(e){
                e.preventDefault();

                $('input[id$="_priceStandingCharge"]').each(function(){ $(this).val(pricingCategoryToolkit.increase5($(this).val())); });
                $('input[id$="_priceFridaySaturday"]').each(function(){ $(this).val(pricingCategoryToolkit.increase5($(this).val())); });
                $('input[id$="_priceSundayThursday"]').each(function(){ $(this).val(pricingCategoryToolkit.increase5($(this).val())); });
                $('input[id$="_price7Nights"]').each(function(){ $(this).val(pricingCategoryToolkit.increase5($(this).val())); });
                $('input[id$="_priceMondayFriday"]').each(function(){ $(this).val(pricingCategoryToolkit.increase5($(this).val())); });
                $('input[id$="_priceFridayMonday"]').each(function(){ $(this).val(pricingCategoryToolkit.increase5($(this).val())); });
            });

            $(document).on('click', '#toolkit_button_increase10', function(e){
                e.preventDefault();

                $('input[id$="_priceStandingCharge"]').each(function(){ $(this).val(pricingCategoryToolkit.increase10($(this).val())); });
                $('input[id$="_priceFridaySaturday"]').each(function(){ $(this).val(pricingCategoryToolkit.increase10($(this).val())); });
                $('input[id$="_priceSundayThursday"]').each(function(){ $(this).val(pricingCategoryToolkit.increase10($(this).val())); });
                $('input[id$="_price7Nights"]').each(function(){ $(this).val(pricingCategoryToolkit.increase10($(this).val())); });
                $('input[id$="_priceMondayFriday"]').each(function(){ $(this).val(pricingCategoryToolkit.increase10($(this).val())); });
                $('input[id$="_priceFridayMonday"]').each(function(){ $(this).val(pricingCategoryToolkit.increase10($(this).val())); });
            });

            $(document).on('click', '#toolkit_button_decrease5', function(e){
                e.preventDefault();

                $('input[id$="_priceStandingCharge"]').each(function(){ $(this).val(pricingCategoryToolkit.decrease5($(this).val())); });
                $('input[id$="_priceFridaySaturday"]').each(function(){ $(this).val(pricingCategoryToolkit.decrease5($(this).val())); });
                $('input[id$="_priceSundayThursday"]').each(function(){ $(this).val(pricingCategoryToolkit.decrease5($(this).val())); });
                $('input[id$="_price7Nights"]').each(function(){ $(this).val(pricingCategoryToolkit.decrease5($(this).val())); });
                $('input[id$="_priceMondayFriday"]').each(function(){ $(this).val(pricingCategoryToolkit.decrease5($(this).val())); });
                $('input[id$="_priceFridayMonday"]').each(function(){ $(this).val(pricingCategoryToolkit.decrease5($(this).val())); });
            });

            $(document).on('click', '#toolkit_button_decrease10', function(e){
                e.preventDefault();

                $('input[id$="_priceStandingCharge"]').each(function(){ $(this).val(pricingCategoryToolkit.decrease10($(this).val())); });
                $('input[id$="_priceFridaySaturday"]').each(function(){ $(this).val(pricingCategoryToolkit.decrease10($(this).val())); });
                $('input[id$="_priceSundayThursday"]').each(function(){ $(this).val(pricingCategoryToolkit.decrease10($(this).val())); });
                $('input[id$="_price7Nights"]').each(function(){ $(this).val(pricingCategoryToolkit.decrease10($(this).val())); });
                $('input[id$="_priceMondayFriday"]').each(function(){ $(this).val(pricingCategoryToolkit.decrease10($(this).val())); });
                $('input[id$="_priceFridayMonday"]').each(function(){ $(this).val(pricingCategoryToolkit.decrease10($(this).val())); });
            });
        }
    }
};