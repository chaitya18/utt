var estateCategoryImage = {
    init: function(){
        if ($('.estateCategoryImagePreview').length > 0){
            var image = $('.estateCategoryImagePreview');
            image.hide();
            if(image.val() != ''){
                image.after('<img id="estateCategoryImagePreviewAdminImg" src="/' + image.attr('data-upload-dir') + '/' + image.val() + '"/>');
            }
        }
    }
}