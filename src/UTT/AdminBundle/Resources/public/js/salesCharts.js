var salesCharts = {
    labels: ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"],
    colors: {
        pastYears: {
            fillColor: "rgba(220,220,220,0.5)",
            strokeColor: "rgba(220,220,220,0.8)",
            highlightFill: "rgba(220,220,220,0.75)",
            highlightStroke: "rgba(220,220,220,1)"
        },
        previousYear: {
            fillColor: "rgba(136,140,115,0.5)",
            strokeColor: "rgba(136,140,115,0.8)",
            highlightFill: "rgba(136,140,115,0.75)",
            highlightStroke: "rgba(92,95,76,1)"
        },
        currentYear: {
            fillColor: "rgba(135,27,9,0.5)",
            strokeColor: "rgba(135,27,9,0.8)",
            highlightFill: "rgba(135,27,9,0.75)",
            highlightStroke: "rgba(106,21,7,1)"
        }
    },
    init: function(){
        $.ajax({
            type: "GET",
            dataType: "json",
            url: Routing.generate('utt_admin_sales_by_month', {}, true),
            success: function(data){
                if(data){
                    if(data.success == true && data.sales){
                        salesCharts.refreshCharts(data.sales);
                    }else{ }
                }else{ }
            },
            error: function(xhr, err) {
            }
        });
        $.ajax({
            type: "GET",
            dataType: "json",
            url: Routing.generate('utt_admin_sales_by_year_until_today', {}, true),
            success: function(data){
                if(data){
                    if(data.success == true && data.sales){
                        salesCharts.refreshUntilTodayChart(data.sales);
                    }else{ }
                }else{ }
            },
            error: function(xhr, err) {
            }
        });
        $.ajax({
            type: "GET",
            dataType: "json",
            url: Routing.generate('utt_admin_sales_by_month_until_today', {}, true),
            success: function(data){
                if(data){
                    if(data.success == true && data.sales){
                        salesCharts.refreshMonthUntilTodayChart(data.sales);
                    }else{ }
                }else{ }
            },
            error: function(xhr, err) {
            }
        });
    },
    refreshUntilTodayChart: function(data){
        var labels = [];

        var currentYearValue = 0;
        var currentYearValue_uttIncome = 0;
        var previousYearValue = 0;
        var previousYearValue_uttIncome = 0;

        var priceSumDatasetData = [];
        var priceSumDatasetData_uttIncome = [];
        for(var i in data){
            if(data[i].year > new Date().getFullYear()-5){
                labels.push(data[i].year);
                var uttIncome = parseFloat(data[i]['adminChargeSum']) + parseFloat(data[i]['commissionValueFull']) + parseFloat(data[i]['bookingProtectChargeSum']) * 0.4;
                priceSumDatasetData.push(data[i]['priceSum']);
                priceSumDatasetData_uttIncome.push(uttIncome);

                if(data[i].year == new Date().getFullYear()){
                    currentYearValue = parseFloat(data[i]['priceSum']);
                    currentYearValue_uttIncome = uttIncome;
                }
                if(data[i].year == new Date().getFullYear()-1){
                    previousYearValue = parseFloat(data[i]['priceSum']);
                    previousYearValue_uttIncome = uttIncome;
                }
            }
        }

        if(currentYearValue > previousYearValue){
            $('#untilTodayChartText').html('Today we are overall £' + Math.round(parseFloat(currentYearValue) - parseFloat(previousYearValue)) + ' up by same day last year.');
            $('#untilTodayChartText').css('color', 'green');
        }else{
            $('#untilTodayChartText').html('Today we are overall £' + Math.round(parseFloat(previousYearValue) - parseFloat(currentYearValue)) + ' down by same day last year.');
            $('#untilTodayChartText').css('color', 'red');
        }

        if(currentYearValue_uttIncome > previousYearValue_uttIncome){
            $('#untilTodayChartText_uttIncome').html('Today we are overall £' + Math.round(parseFloat(currentYearValue_uttIncome) - parseFloat(previousYearValue_uttIncome)) + ' up by same day last year.');
            $('#untilTodayChartText_uttIncome').css('color', 'green');
        }else{
            $('#untilTodayChartText_uttIncome').html('Today we are overall £' + Math.round(parseFloat(previousYearValue_uttIncome) - parseFloat(currentYearValue_uttIncome)) + ' down by same day last year.');
            $('#untilTodayChartText_uttIncome').css('color', 'red');
        }

        var priceSumDataset = {
            label: '', data: priceSumDatasetData, fillColor: salesCharts.colors.currentYear.fillColor, strokeColor: salesCharts.colors.currentYear.strokeColor, highlightFill: salesCharts.colors.currentYear.highlightFill, highlightStroke: salesCharts.colors.currentYear.highlightStroke
        };
        var priceSumDataset_uttIncome = {
            label: '', data: priceSumDatasetData_uttIncome, fillColor: salesCharts.colors.currentYear.fillColor, strokeColor: salesCharts.colors.currentYear.strokeColor, highlightFill: salesCharts.colors.currentYear.highlightFill, highlightStroke: salesCharts.colors.currentYear.highlightStroke
        };

        var ctx = document.getElementById('untilTodayChart').getContext("2d");
        var ctx_uttIncome = document.getElementById('untilTodayChart_uttIncome').getContext("2d");
        var chart = new Chart(ctx).Bar({
            labels: labels, datasets: [priceSumDataset]
        }, {
            responsive: true,
            tooltipTemplate: "<%= '£' + Math.round(value).toLocaleString('en-US', {minimumFractionDigits: 0}) %>"
        });
        var chart_uttIncome = new Chart(ctx_uttIncome).Bar({
            labels: labels, datasets: [priceSumDataset_uttIncome]
        }, {
            responsive: true,
            tooltipTemplate: "<%= '£' + Math.round(value).toLocaleString('en-US', {minimumFractionDigits: 0}) %>"
        });
    },
    refreshMonthUntilTodayChart: function(data){
        var labels = [];

        var currentYearValue = 0;
        var currentYearValue_uttIncome = 0;
        var previousYearValue = 0;
        var previousYearValue_uttIncome = 0;

        var priceSumDatasetData = [];
        var priceSumDatasetData_uttIncome = [];
        for(var i in data){
            if(data[i].year > new Date().getFullYear()-5 && parseInt(data[i].month) - 1 == parseInt(new Date().getMonth())){
                labels.push(data[i].yearMonth);
                var uttIncome = parseFloat(data[i]['adminChargeSum']) + parseFloat(data[i]['commissionValueFull']) + parseFloat(data[i]['bookingProtectChargeSum']) * 0.4;
                priceSumDatasetData.push(data[i]['priceSum']);
                priceSumDatasetData_uttIncome.push(uttIncome);

                if(data[i].year == new Date().getFullYear()){
                    currentYearValue = parseFloat(data[i]['priceSum']);
                    currentYearValue_uttIncome = parseFloat(uttIncome);
                }
                if(data[i].year == new Date().getFullYear()-1){
                    previousYearValue = parseFloat(data[i]['priceSum']);
                    previousYearValue_uttIncome = parseFloat(uttIncome);
                }
            }
        }

        if(currentYearValue > previousYearValue){
            $('.monthUntilTodayChartText').html('Today we are £' + Math.round(parseFloat(currentYearValue) - parseFloat(previousYearValue)) + ' up by same day this month last year.');
            $('.monthUntilTodayChartText').css('color', 'green');
        }else{
            $('.monthUntilTodayChartText').html('Today we are £' + Math.round(parseFloat(previousYearValue) - parseFloat(currentYearValue)) + ' down by same day this month last year.');
            $('.monthUntilTodayChartText').css('color', 'red');
        }
        if(currentYearValue_uttIncome > previousYearValue_uttIncome){
            $('.monthUntilTodayChartText_uttIncome').html('Today we are £' + Math.round(parseFloat(currentYearValue_uttIncome) - parseFloat(previousYearValue_uttIncome)) + ' up by same day this month last year.');
            $('.monthUntilTodayChartText_uttIncome').css('color', 'green');
        }else{
            $('.monthUntilTodayChartText_uttIncome').html('Today we are £' + Math.round(parseFloat(previousYearValue_uttIncome) - parseFloat(currentYearValue_uttIncome)) + ' down by same day this month last year.');
            $('.monthUntilTodayChartText_uttIncome').css('color', 'red');
        }

        var priceSumDataset = {
            label: '', data: priceSumDatasetData, fillColor: salesCharts.colors.currentYear.fillColor, strokeColor: salesCharts.colors.currentYear.strokeColor, highlightFill: salesCharts.colors.currentYear.highlightFill, highlightStroke: salesCharts.colors.currentYear.highlightStroke
        };
        var priceSumDataset_uttIncome = {
            label: '', data: priceSumDatasetData_uttIncome, fillColor: salesCharts.colors.currentYear.fillColor, strokeColor: salesCharts.colors.currentYear.strokeColor, highlightFill: salesCharts.colors.currentYear.highlightFill, highlightStroke: salesCharts.colors.currentYear.highlightStroke
        };

        var ctx = document.getElementById('monthUntilTodayChart').getContext("2d");
        var ctx_uttIncome = document.getElementById('monthUntilTodayChart_uttIncome').getContext("2d");
        var chart = new Chart(ctx).Bar({
            labels: labels, datasets: [priceSumDataset]
        }, {
            responsive: true,
            tooltipTemplate: "<%= '£' + Math.round(value).toLocaleString('en-US', {minimumFractionDigits: 0}) %>"
        });
        var chart_uttIncome = new Chart(ctx_uttIncome).Bar({
            labels: labels, datasets: [priceSumDataset_uttIncome]
        }, {
            responsive: true,
            tooltipTemplate: "<%= '£' + Math.round(value).toLocaleString('en-US', {minimumFractionDigits: 0}) %>"
        });
    },
    refreshCharts: function(data){
        var priceSumDatasets = [];
        var reservationCountDatasets = [];
        var nightsDatasets = [];
        for(var year in data){
            var colors = {};
            if(year == new Date().getFullYear()) {
                colors = salesCharts.colors.currentYear;
            }else if(year == new Date().getFullYear() -1){
                colors = salesCharts.colors.previousYear;
            }else{
                colors = salesCharts.colors.pastYears;
            }

            var priceSumDataset = {
                label: year, data: [], fillColor: colors.fillColor, strokeColor: colors.strokeColor, highlightFill: colors.highlightFill, highlightStroke: colors.highlightStroke
            };

            var reservationCountDataset = {
                label: year, data: [], fillColor: colors.fillColor, strokeColor: colors.strokeColor, highlightFill: colors.highlightFill, highlightStroke: colors.highlightStroke
            };

            var nightsDataset = {
                label: year, data: [], fillColor: colors.fillColor, strokeColor: colors.strokeColor, highlightFill: colors.highlightFill, highlightStroke: colors.highlightStroke
            };

            var priceSumDatasetData = [];
            var reservationCountDatasetData = [];
            var nightsDatasetData = [];

            for(var i in data[year]){
                priceSumDatasetData.push(data[year][i]['priceSum']);
                reservationCountDatasetData.push(data[year][i]['reservationCount']);
                nightsDatasetData.push(data[year][i]['nights']);
            }

            priceSumDataset.data = priceSumDatasetData;
            priceSumDatasets.push(priceSumDataset);
            reservationCountDataset.data = reservationCountDatasetData;
            reservationCountDatasets.push(reservationCountDataset);
            nightsDataset.data = nightsDatasetData;
            nightsDatasets.push(nightsDataset);
        }

        var ctx = document.getElementById('priceSumChart').getContext("2d");
        var priceSumChart = new Chart(ctx).Bar({
            labels: salesCharts.labels, datasets: priceSumDatasets
        }, {
            responsive: true,
            multiTooltipTemplate: "<%= '£' + Math.round(value).toLocaleString('en-US', {minimumFractionDigits: 0}) %>"
        });

        var ctx2 = document.getElementById('reservationCountChart').getContext("2d");
        var reservationCountChart = new Chart(ctx2).Bar({
            labels: salesCharts.labels, datasets: reservationCountDatasets
        }, {responsive: true});

        var ctx3 = document.getElementById('nightsChart').getContext("2d");
        var nightsChart = new Chart(ctx3).Bar({
            labels: salesCharts.labels, datasets: nightsDatasets
        }, {responsive: true});
    }
};

salesCharts.init();