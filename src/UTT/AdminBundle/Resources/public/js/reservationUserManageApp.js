var reservationUserManageApp = angular.module('reservationUserManageApp', []);

reservationUserManageApp.factory('userDataFactory', function($http) {
    var userData = {
        firstName: '',
        lastName: '',
        email: '',
        phone: '',
        address: '',
        city: '',
        postcode: '',
        country: ''
    };

    var setUserFirstName = function(data) { userData.firstName = data; };
    var getUserFirstName = function() { return userData.firstName; };

    var setUserLastName = function(data) { userData.lastName = data; };
    var getUserLastName = function() { return userData.lastName; };

    var setUserEmail = function(data) { userData.email = data; };
    var getUserEmail = function() { return userData.email; };

    var setUserPhone = function(data) { userData.phone = data; };
    var getUserPhone = function() { return userData.phone; };

    var setUserAddress = function(data) { userData.address = data; };
    var getUserAddress = function() { return userData.address; };

    var setUserCity = function(data) { userData.city = data; };
    var getUserCity = function() { return userData.city; };

    var setUserPostcode = function(data) { userData.postcode = data; };
    var getUserPostcode = function() { return userData.postcode; };

    var setUserCountry = function(data) { userData.country = data; };
    var getUserCountry = function() { return userData.country; };

    return {
        setUserFirstName: setUserFirstName,
        getUserFirstName: getUserFirstName,
        setUserLastName: setUserLastName,
        getUserLastName: getUserLastName,
        setUserEmail: setUserEmail,
        getUserEmail: getUserEmail,
        setUserPhone: setUserPhone,
        getUserPhone: getUserPhone,
        setUserAddress: setUserAddress,
        getUserAddress: getUserAddress,
        setUserCity: setUserCity,
        getUserCity: getUserCity,
        setUserPostcode: setUserPostcode,
        getUserPostcode: getUserPostcode,
        setUserCountry: setUserCountry,
        getUserCountry: getUserCountry,

        setData: function(data){
            if(typeof data.firstName === typeof undefined){}else{
                setUserFirstName(data.firstName);
            }
            if(typeof data.lastName === typeof undefined){}else{
                setUserLastName(data.lastName);
            }
            if(typeof data.email === typeof undefined){}else{
                setUserEmail(data.email);
            }
            if(typeof data.phone === typeof undefined){}else{
                setUserPhone(data.phone);
            }
            if(typeof data.address === typeof undefined){}else{
                setUserAddress(data.address);
            }
            if(typeof data.city === typeof undefined){}else{
                setUserCity(data.city);
            }
            if(typeof data.postcode === typeof undefined){}else{
                setUserPostcode(data.postcode);
            }
            if(typeof data.country === typeof undefined){}else{
                setUserCountry(data.country);
            }
        },
        getData: function(){
            return userData;
        }
    }
});


reservationUserManageApp.controller("reservationUserManageAppCtrl", ['$scope', '$http', 'userDataFactory', function($scope, $http, userDataFactory){
    $http.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';
    $http.defaults.headers.post["Content-Type"] = "application/x-www-form-urlencoded";

    $scope.initUserData = function(json){
        var userData = angular.fromJson(json);
        userDataFactory.setData(userData);
        userDataApply();
    };

    $scope.changeUserFirstName = function(firstName){
        userDataFactory.setUserFirstName(firstName);
        userDataApply();
    };
    $scope.changeUserLastName = function(lastName){
        userDataFactory.setUserLastName(lastName);
        userDataApply();
    };
    $scope.changeUserEmail = function(email){
        userDataFactory.setUserEmail(email);
        userDataApply();
    };
    $scope.changeUserPhone = function(phone){
        userDataFactory.setUserPhone(phone);
        userDataApply();
    };
    $scope.changeUserAddress = function(address){
        userDataFactory.setUserAddress(address);
        userDataApply();
    };
    $scope.changeUserCity = function(city){
        userDataFactory.setUserCity(city);
        userDataApply();
    };
    $scope.changeUserPostcode = function(postcode){
        userDataFactory.setUserPostcode(postcode);
        userDataApply();
    };
    $scope.changeUserCountry = function(country){
        userDataFactory.setUserCountry(country);
        userDataApply();
    };

    var userDataApply = function(){
        $scope.userData = userDataFactory.getData();
    };

    $scope.editMode = false;
    $scope.toggleEditMode = function(){
        if($scope.editMode == true){
            $scope.editMode = false;
        }else{
            $scope.editMode = true;
        }
    };
}]);

angular.bootstrap(document.getElementById("reservationUserManageAppHandler"),["reservationUserManageApp"]);