var offerImage = {
    init: function(){
        if ($('.offerImagePreview').length > 0){
            var image = $('.offerImagePreview');
            image.hide();
            if(image.val() != ''){
                image.after('<img id="offerImagePreviewAdminImg" src="/' + image.attr('data-upload-dir') + '/' + image.val() + '"/>');
            }
        }
    }
}