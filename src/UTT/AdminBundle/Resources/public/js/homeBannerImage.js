var homeBannerImage = {
    init: function(){
        if ($('.homeBannerImagePreview').length > 0){
            var image = $('.homeBannerImagePreview');
            image.hide();

            image.parent().addClass('imageWrap');

            if(image.val() != ''){
                image.after('<img id="homeBannerImagePreviewAdminImg" src="/' + image.attr('data-upload-dir') + '/' + image.val() + '"/>');

                image.parent().imagedrag({
                    input: ".homeBannerImagePosition",
                    position: $('.homeBannerImagePosition').val(),
                    attribute: "value"
                });

                image.parent().append('<div class="logoPreview"><img src="/bundles/uttindex/images/logos/with-text-1.png"><div class="utt-name">Under the Thatch</div><div class="utt-subname">holiday homes you\'ll <span class="specjal">love</span></div></div>');
                image.parent().append('<div id="main-slider-handler"><div class="slider-search-panel">' +
'                   <form class="navbar-form">' +
    '                   <div class="float-right">' +
    '                       <button class="btn btn-search" type="button">search</button>' +
    '                   </div>' +
    '                   <div class="fields">' +
        '                   <div class="col-xs-4">' +
        '                       <select class="form-control" name="location"></select>' +
        '                   </div>' +
        '                   <div class="col-xs-8 padding0">' +
            '                   <div class="overflow-auto">' +
            '                       <div class="col-xs-4">' +
            '                           <input type="text" class="search-panel-from-date form-control" placeholder="arrive" name="dateFrom">' +
            '                       </div>' +
            '                       <div class="col-xs-4">' +
            '                           <input type="text" class="search-panel-to-date form-control" placeholder="depart" name="dateTo">' +
            '                       </div>' +
            '                       <div class="col-xs-4">' +
            '                           <select class="form-control" name="sleeps"></select>' +
            '                       </div>' +
            '                   </div>' +
            '                   <div class="col-xs-12">' +
            '                       <label class="checkbox-inline no-date-checkbox">' +
            '                           <input type="checkbox" value="option1" class="search-panel-no-date">search without dates' +
            '                       </label>' +
            '                   </div>' +
        '                   </div>' +
    '                   </div>' +
'                   </form>' +
'                   </div></div>');
            }
        }
    }
}