var selectPricingCategory = {
    init: function(){
        $('.initSelectPricingCategory').change(function(){
            var url = Routing.generate('utt_reservation_pricingcategory_admin_edit', { 'id': $(this).val() }, true);

            window.location.href = url;
        });
    }
};