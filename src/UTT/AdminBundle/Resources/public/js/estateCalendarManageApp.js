var estateCalendarManageApp = angular.module('estateCalendarManageApp', ['ui.bootstrap']);

estateCalendarManageApp.factory('calendarFactory', function() {
    var calendarData =  angular.fromJson($calendarData);
    var months = [
        { 'id': 1, 'name': 'January', 'emptyDays': [], 'days': [] },
        { 'id': 2, 'name': 'February', 'emptyDays': [], 'days': [] },
        { 'id': 3, 'name': 'March', 'emptyDays': [], 'days': [] },
        { 'id': 4, 'name': 'April', 'emptyDays': [], 'days': [] },
        { 'id': 5, 'name': 'May',  'emptyDays': [], 'days': [] },
        { 'id': 6, 'name': 'June', 'emptyDays': [], 'days': [] },
        { 'id': 7, 'name': 'July',  'emptyDays': [], 'days': [] },
        { 'id': 8, 'name': 'August', 'emptyDays': [], 'days': [] },
        { 'id': 9, 'name': 'September', 'emptyDays': [], 'days': [] },
        { 'id': 10, 'name': 'October', 'emptyDays': [], 'days': [] },
        { 'id': 11, 'name': 'November', 'emptyDays': [], 'days': [] },
        { 'id': 12, 'name': 'December', 'emptyDays': [], 'days': [] },

    ];
    var isManageAllowed = $isManageAllowed;

    var generateMonthToYear = function(year, fromMonth, toMonth) {
        var result = [];
        angular.forEach(months, function(month) {
            if(fromMonth <= month.id && toMonth >= month.id) {
                var newMonth = Object.assign({}, month);
                newMonth.year = year;
                result.push(newMonth);
            }
        })
        return result;
    };

    return {
        getMonths: function(){
            var lastYear, actualYear, newYear;
            var now = new Date();
            var year = now.getFullYear();
            var month = now.getMonth() + 1;
            if(month >= 2 && month < 4) {
                lastYear = [];
                actualYear =  generateMonthToYear(year, 1, 12);
                newYear = generateMonthToYear(year+1, 1, 6);
            } else if(month < 2) {
                lastYear = generateMonthToYear(year-1, 12-month, 12);
                actualYear = generateMonthToYear(year, 1, 12);
                newYear = generateMonthToYear(year+1, 1, 6);
            } else {
                lastYear = [];
                actualYear = generateMonthToYear(year, month-2, 12);
                newYear = generateMonthToYear( year+1, 1, 12);
            }
            return lastYear.concat(actualYear).concat(newYear);
        },
        getCalendarData: function(){
            return calendarData;
        },
        isManageAllowed: function(){
            if(isManageAllowed == 'true'){
                return true;
            }else{
                return false;
            }
        }
    }
});

estateCalendarManageApp.factory('reservationsFactory', function(){
    var reservationsData = angular.fromJson($reservationsData);

    var getReservationForDate = function(date){
        var reservation = false;

        angular.forEach(reservationsData, function(item){
            if(date >= item.fromDate && date <= item.toDate){
                reservation = item;
            }
        });

        return reservation;
    };

    var addReservation = function(reservation){
        reservationsData.push(reservation);
    };

    return {
        getReservationsData: function(){
            return reservationsData;
        },
        getReservationForDate: getReservationForDate,
        addReservation: addReservation
    }
});

estateCalendarManageApp.factory('offersFactory', function(){
    var offersData = angular.fromJson($offersData);

    var addOffer = function(offer){
        offersData.push(offer);
    };

    var getOfferForDate = function(date){
        var offer = false;

        angular.forEach(offersData, function(item){
            if(date >= item.validFrom && date <= item.validTo){
                offer = item;
            }
        });

        return offer;
    };

    return {
        getOffersData: function(){
            return offersData;
        },
        getOfferForDate: getOfferForDate,
        addOffer: addOffer
    }
});

estateCalendarManageApp.factory('priceFactory', function() {
    var pricingData = angular.fromJson($pricingData);
    var adminCharge =  parseFloat($adminCharge);

    var parseDate = function(str) {
        return new Date(str);
    }

    var dayDiff = function(first, second) {
        return (second-first)/(1000*60*60*24)
    };

    return {
        calculateFlexible: function(days){
            if(days.length > 2){
                var price = 0;
                angular.forEach(days, function(day){
                    if(days.indexOf(day) < days.length -1){
                        angular.forEach(pricingData, function(pricing){
                            if(day.date >= pricing.fromDate && day.date <= pricing.toDate){
                                var index = days.indexOf(day);
                                if(index == 0){
                                    //price = parseFloat(price) + parseFloat(pricing.initialPrice);
                                    //price = parseFloat(pricing.initialPrice);
                                    price = parseFloat(pricing.initialPrice) + parseFloat(adminCharge);
                                }

                                price = parseFloat(price) + parseFloat(pricing.price);
                            }
                        });
                    }
                });

                if(price > 0){ return price.toFixed(2); }
            }

            return -1;
        },
        calculateStandard: function(day, daysCount){
            var found = false;
            angular.forEach(pricingData, function(pricing){
                var diff = dayDiff(parseDate(pricing.fromDate), parseDate(pricing.toDate));
                if(day.date >= pricing.fromDate && day.date <= pricing.toDate && diff == daysCount){
                    //found = pricing;
                    found = {};
                    angular.copy(pricing, found);
                    found.price = parseFloat(found.price) + parseFloat(adminCharge);
                }
            });

            return found;
        }
    }
});

var ModalInstanceCtrl = function ($scope, $modalInstance, apiService, reservationsFactory, offersFactory, fromDate, toDate, price, estateShortName) {
    $scope.fromDate = fromDate;
    $scope.toDate = toDate;
    $scope.estateShortName = estateShortName;

    $scope.sleeps = null;
    $scope.setSleeps = function(sleeps){
        $scope.sleeps = sleeps;
    };

    $scope.sleepsChoices = [];
    $scope.initSleeps = function(maxSleeps){
        $scope.sleepsChoices = [];
        for(var i=2; i<=maxSleeps; i++){
            $scope.sleepsChoices.push({ 'id': i });
        }

        $scope.sleeps = $scope.sleepsChoices[0];
    };

    $scope.price = price;
    $scope.setPrice = function(price){
        $scope.price = price;
    };

    $scope.isFullyPaid = false;
    $scope.setIsFullyPaid = function(isFullyPaid){
        $scope.isFullyPaid = isFullyPaid;
    };

    $scope.offerName = '';
    $scope.setOfferName = function(offerName){
        $scope.offerName = offerName;
    };
    $scope.offerDescription = '';
    $scope.setOfferDescription = function(offerDescription){
        $scope.offerDescription = offerDescription;
    };

    $scope.guestFirstName = '';
    $scope.setGuestFirstName = function(guestFirstName){
        $scope.guestFirstName = guestFirstName;
    };

    $scope.guestLastName = '';
    $scope.setGuestLastName = function(guestLastName){
        $scope.guestLastName = guestLastName;
    };

    $scope.guestEmail = '';
    $scope.setGuestEmail = function(guestEmail){
        $scope.guestEmail = guestEmail;
    };

    $scope.reason = '';
    $scope.setReason = function(reason){
        $scope.reason = reason;
    };

    $scope.activeForm = 'owner_booking';
    $scope.setActiveForm = function(activeForm){
        $scope.activeForm = activeForm;
    };
    $scope.isActiveForm = function(activeForm){
        if($scope.activeForm == activeForm){
            return true;
        }
        return false;
    };

    $scope.submitLoading = false;

    $scope.submitForm = function () {
        $scope.submitLoading = true;
        $scope.isRequiredError = false;
        if($scope.isActiveForm('owner_booking') || $scope.isActiveForm('dates_not_for_sale') || $scope.isActiveForm('other_agency')){
            var params = {
                'estateShortName': $scope.estateShortName,
                'fromDate': $scope.fromDate,
                'toDate': $scope.toDate,
                'blockedStatus': $scope.activeForm,
                'reason': $scope.reason,
                'sleeps': $scope.sleeps.id
            };

            apiService.bookBlocked(params, function(result){
                if(result){
                    reservationsFactory.addReservation(result);
                    $modalInstance.close({ 'booked': true, 'blocked': true });
                }
            });
        }else if($scope.isActiveForm('special_offer')){
            var params = {
                'estateShortName': $scope.estateShortName,
                'fromDate': $scope.fromDate,
                'toDate': $scope.toDate,
                'price': $scope.price,
                'offerName': $scope.offerName,
                'offerDescription': $scope.offerDescription,
            };

            apiService.createOffer(params, function(result){
                if(result){
                    offersFactory.addOffer(result);
                    $modalInstance.close({ 'availableOffer': true });
                }
            });
        }else if($scope.isActiveForm('telephone_booking')){
            if(!($scope.guestFirstName && $scope.guestLastName && $scope.guestEmail)){
                $scope.isRequiredError = true;
                $scope.submitLoading = false;
            }else{
                var params = {
                    'estateShortName': $scope.estateShortName,
                    'fromDate': $scope.fromDate,
                    'toDate': $scope.toDate,
                    'guestFirstName': $scope.guestFirstName,
                    'guestLastName': $scope.guestLastName,
                    'guestEmail': $scope.guestEmail,
                    'price': $scope.price,
                    'isFullyPaid': $scope.isFullyPaid ? 1 : 0
                };

                apiService.telephoneBooking(params, function(result){
                    if(result){
                        reservationsFactory.addReservation(result);
                        $modalInstance.close({ 'booked': true, 'reserved': true });
                    }else{
                        $scope.isRequiredError = true;
                        $scope.submitLoading = false;
                    }
                });
            }
        }else if($scope.isActiveForm('groupon_booking')){
            if(!($scope.guestFirstName && $scope.guestLastName && $scope.guestEmail)){
                $scope.isRequiredError = true;
                $scope.submitLoading = false;
            }else{
                var params = {
                    'estateShortName': $scope.estateShortName,
                    'fromDate': $scope.fromDate,
                    'toDate': $scope.toDate,
                    'guestFirstName': $scope.guestFirstName,
                    'guestLastName': $scope.guestLastName,
                    'guestEmail': $scope.guestEmail
                };

                apiService.grouponBooking(params, function(result){
                    if(result){
                        reservationsFactory.addReservation(result);
                        $modalInstance.close({ 'booked': true, 'reserved': true });
                    }else{
                        $scope.isRequiredError = true;
                        $scope.submitLoading = false;
                    }
                });
            }
        }
    };

    $scope.cancelForm = function () {
        $modalInstance.dismiss('cancel');
    };
};

estateCalendarManageApp.factory('apiService', function($http) {
    $http.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';
    $http.defaults.headers.post["Content-Type"] = "application/x-www-form-urlencoded";

    return {
        bookBlocked: function(params, callback){
            $http({
                method: 'POST',
                url: Routing.generate('utt_reservation_book_blocked'),
                data: $.param(params),
                headers: {'Content-Type':'application/x-www-form-urlencoded; charset=UTF-8'}
            })
                .success(function(data, status, headers, config) {
                    var dataOb = angular.fromJson(data);
                    if(!(typeof dataOb.success === typeof undefined) && dataOb.success == true){
                        callback(dataOb.reservation);
                    }else{
                        callback(false);
                    }
                })
                .error(function(data, status, headers, config) {
                    callback(false);
                });
        },
        telephoneBooking: function(params, callback){
            $http({
                method: 'POST',
                url: Routing.generate('utt_reservation_telephone_booking'),
                data: $.param(params),
                headers: {'Content-Type':'application/x-www-form-urlencoded; charset=UTF-8'}
            })
                .success(function(data, status, headers, config) {
                    var dataOb = angular.fromJson(data);
                    if(!(typeof dataOb.success === typeof undefined) && dataOb.success == true){
                        callback(dataOb.reservation);
                    }else{
                        callback(false);
                    }
                })
                .error(function(data, status, headers, config) {
                    callback(false);
                });
        },
        grouponBooking: function(params, callback){
            $http({
                method: 'POST',
                url: Routing.generate('utt_reservation_groupon_booking'),
                data: $.param(params),
                headers: {'Content-Type':'application/x-www-form-urlencoded; charset=UTF-8'}
            })
                .success(function(data, status, headers, config) {
                    var dataOb = angular.fromJson(data);
                    if(!(typeof dataOb.success === typeof undefined) && dataOb.success == true){
                        callback(dataOb.reservation);
                    }else{
                        callback(false);
                    }
                })
                .error(function(data, status, headers, config) {
                    callback(false);
                });
        },
        createOffer: function(params, callback){
            $http({
                method: 'POST',
                url: Routing.generate('utt_reservation_create_offer_for_estate'),
                data: $.param(params),
                headers: {'Content-Type':'application/x-www-form-urlencoded; charset=UTF-8'}
            })
                .success(function(data, status, headers, config) {
                    var dataOb = angular.fromJson(data);
                    if(!(typeof dataOb.success === typeof undefined) && dataOb.success == true){
                        callback(dataOb.offer);
                    }else{
                        callback(false);
                    }
                })
                .error(function(data, status, headers, config) {
                    callback(false);
                });
        },
    }
});

estateCalendarManageApp.controller('estateCalendarManageAppCtrl', ['$scope', '$window', 'calendarFactory', 'priceFactory', 'offersFactory', '$modal', 'reservationsFactory', 'apiService', '$filter', function($scope, $window, calendarFactory, priceFactory, offersFactory, $modal, reservationsFactory, apiService, $filter){
    $scope.calendarData = calendarFactory.getCalendarData();

    var applyCalendar = function(){
        var months = [];
        angular.copy(calendarFactory.getMonths(), months);

        angular.forEach($scope.calendarData.days, function(price){
            angular.forEach(months, function(month){
                if(price.month == month.id && price.year == month.year){
                    var index = months.indexOf(month);
                    months[index].days.push(price);

                    if(price.day == '01' && price.dayOfWeek != undefined){
                        for(var i=1; i<price.dayOfWeek; i++){
                            months[index].emptyDays.push({});
                        }
                    }
                }
            });
        });

        $scope.months = months;
    };

    $scope.tooltip = '';

    $scope.showSelectedPriceTooltip = function(){
        if($scope.tooltip){
            return $scope.tooltip;
        }
    };
    $scope.fromDateIndex = null;
    $scope.toDateIndex = null;
    $scope.calculatedPrice = 0;

    $scope.estateShortName = '';
    $scope.setEstateShortName = function(estateShortName){
        $scope.estateShortName = estateShortName;
    };

    $scope.mouseOverDay = function(day){
        $scope.tooltip = 'Not available';

        var index = $scope.calendarData.days.indexOf(day);
        if($scope.calendarData.days[index].reserved == true){
            $scope.tooltip = '<div><h4>Reserved</h4></div>';

            var reservation = reservationsFactory.getReservationForDate(day.date);
            if(reservation){
                $scope.tooltip = '<div style="padding: 5px;"><h5>Reserved</h5><h4>' + day.date + '</h4><div>booking id: ' + reservation.id + '</div><div>arrive: ' + $filter('date')(reservation.fromDate, 'd MMM yyyy') + '</div><div>depart: ' + $filter('date')(reservation.toDate, 'd MMM yyyy') + '</div><h5>' + reservation.status + '</h5><div>' + (reservation.userData.email ? reservation.userData.email : '') + '</div><div>' + (reservation.userData.name ? reservation.userData.name : '') + '</div></div>';
            }
        }else if($scope.calendarData.days[index].booked == true){
            $scope.tooltip = '<div><h4>Booked</h4></div>';

            var reservation = reservationsFactory.getReservationForDate(day.date);
            if(reservation){
                $scope.tooltip = '<div style="padding: 5px;"><h5>Booked</h5><h4>' + day.date + '</h4><div>booking id: ' + reservation.id + '</div><div>arrive: ' + $filter('date')(reservation.fromDate, 'd MMM yyyy') + '</div><div>depart: ' + $filter('date')(reservation.toDate, 'd MMM yyyy') + '</div><h5>' + reservation.status + '</h5><div>' + (reservation.userData.email ? reservation.userData.email : '') + '</div><div>' + (reservation.userData.name ? reservation.userData.name : '') + '</div></div>';
            }
        }else if($scope.calendarData.days[index].availableOffer == true){
            $scope.tooltip = '<div><h4>Available offer</h4></div>';

            var offer = offersFactory.getOfferForDate(day.date);
            if(offer){
                $scope.tooltip = '<div style="padding: 5px;"><h5>Available offer</h5><h4>' + day.date + '</h4><div>offer name: ' + offer.name + '</div><div>valid from: ' + $filter('date')(offer.validFrom, 'd MMM yyyy') + '</div><div>valid to: ' + $filter('date')(offer.validTo, 'd MMM yyyy') + '</div><h5>value: ' + offer.value + '</h5></div>';
            }
        }else if($scope.calendarData.days[index].available == true){
            $scope.tooltip = 'Available';
        }
    };

    $scope.selectDays = function(day){
        if(!calendarFactory.isManageAllowed()){ return false; }

        var index = $scope.calendarData.days.indexOf(day);
        var oneNightBookingClicked = false;
        if(
            $scope.fromDateIndex == null &&
            $scope.calendarData.days[index-1].available == true &&
            $scope.calendarData.days[index-1].booked == false &&
            $scope.calendarData.days[index].available == true &&
            $scope.calendarData.days[index].booked == true &&
            $scope.calendarData.days[index+1].available == true &&
            $scope.calendarData.days[index+1].booked == true
        ){
            oneNightBookingClicked = true;
        }

        if($scope.calendarData.days[index].available == true && !oneNightBookingClicked){
            if($scope.fromDateIndex && $scope.toDateIndex){
                if($scope.fromDateIndex == index) {
                    $scope.calendarData.days[$scope.fromDateIndex].selected = false;
                    $scope.fromDateIndex = null;
                }else if($scope.toDateIndex == index){
                    $scope.calendarData.days[$scope.toDateIndex].selected = false;
                    $scope.toDateIndex = null;
                }else {
                    $scope.calendarData.days[$scope.toDateIndex].selected = false;
                    $scope.toDateIndex = index;
                    $scope.calendarData.days[$scope.toDateIndex].selected = true;
                }
            }else if($scope.fromDateIndex){
                if($scope.fromDateIndex == index) {
                    $scope.calendarData.days[$scope.fromDateIndex].selected = false;
                    $scope.fromDateIndex = null;
                }else{
                    $scope.toDateIndex = index;
                    $scope.calendarData.days[$scope.toDateIndex].selected = true;
                }
            }else if($scope.toDateIndex){
                if($scope.toDateIndex == index) {
                    $scope.calendarData.days[$scope.toDateIndex].selected = false;
                    $scope.toDateIndex = null;
                }else{
                    $scope.fromDateIndex = index;
                    $scope.calendarData.days[$scope.fromDateIndex].selected = true;
                }
            }else{
                $scope.fromDateIndex = index;
                $scope.calendarData.days[$scope.fromDateIndex].selected = true;
            }

            if($scope.fromDateIndex && $scope.toDateIndex){
                if($scope.toDateIndex < $scope.fromDateIndex){
                    var fromDate = $scope.fromDateIndex;
                    var toDate = $scope.toDateIndex;

                    $scope.fromDateIndex = toDate;
                    $scope.toDateIndex = fromDate;
                }
            }

            var selectedIndexes = [];
            angular.forEach($scope.calendarData.days, function(op){
                var index = $scope.calendarData.days.indexOf(op);
                if($scope.fromDateIndex && $scope.toDateIndex){
                    if(index >= $scope.fromDateIndex && index <= $scope.toDateIndex){
                        $scope.calendarData.days[index].selected = true;
                        selectedIndexes.push(index);
                    }else{
                        $scope.calendarData.days[index].selected = false;
                    }
                }else{
                    if($scope.fromDateIndex == index || $scope.toDateIndex == index){ }else{
                        $scope.calendarData.days[index].selected = false;
                    }
                }
            });

            if($scope.fromDateIndex && $scope.toDateIndex){
                if($scope.calendarData.type == 'flexible'){
                    var days = [];
                    angular.forEach(selectedIndexes, function(sindex){
                        days.push($scope.calendarData.days[sindex]);
                    });

                    var price = priceFactory.calculateFlexible(days);
                    if(price != -1){
                        $scope.calculatedPrice = price;
                    }else{
                        $scope.calculatedPrice = 0;
                    }
                }else if($scope.calendarData.type == 'standard'){
                    var calculated = priceFactory.calculateStandard(day, selectedIndexes.length-1);
                    if(calculated){
                        $scope.calculatedPrice = calculated.price;
                    }else{
                        $scope.calculatedPrice = 0;
                    }
                }

                // modal
                var modalInstance = $modal.open({
                    templateUrl: 'modalContent.html',
                    controller: ModalInstanceCtrl,
                    size: 'lg',
                    resolve: {
                        fromDate: function () {
                            return $scope.calendarData.days[$scope.fromDateIndex].date;
                        },
                        toDate: function () {
                            return $scope.calendarData.days[$scope.toDateIndex].date;
                        },
                        price: function(){
                            return $scope.calculatedPrice;
                        },
                        estateShortName: function(){
                            return $scope.estateShortName;
                        }
                    }
                });

                modalInstance.result.then(function (args) {
                    angular.forEach($scope.calendarData.days, function(op){
                        var index = $scope.calendarData.days.indexOf(op);
                        $scope.calendarData.days[index].selected = false;
                        if(args.booked != undefined && args.booked && index >= $scope.fromDateIndex && index <= $scope.toDateIndex){
                            $scope.calendarData.days[index].available = false;
                            $scope.calendarData.days[index].booked = true;

                            if(index == $scope.fromDateIndex){
                                $scope.calendarData.days[index].bookedStart = true;
                                $scope.calendarData.days[index].available = true;
                            }
                            if(index == $scope.toDateIndex){
                                $scope.calendarData.days[index].bookedEnd = true;
                                $scope.calendarData.days[index].available = true;
                            }
                        }
                        if(args.reserved != undefined && args.reserved && index >= $scope.fromDateIndex && index <= $scope.toDateIndex){
                            $scope.calendarData.days[index].reserved = true;

                            if(index == $scope.fromDateIndex){
                                $scope.calendarData.days[index].reservedStart = true;
                                $scope.calendarData.days[index].available = true;
                            }
                            if(index == $scope.toDateIndex){
                                $scope.calendarData.days[index].reservedEnd = true;
                                $scope.calendarData.days[index].available = true;
                            }
                        }
                        if(args.blocked != undefined && args.blocked && index >= $scope.fromDateIndex && index <= $scope.toDateIndex){
                            $scope.calendarData.days[index].blocked = true;

                            if(index == $scope.fromDateIndex){
                                $scope.calendarData.days[index].blockedStart = true;
                                $scope.calendarData.days[index].available = true;
                            }
                            if(index == $scope.toDateIndex){
                                $scope.calendarData.days[index].blockedEnd = true;
                                $scope.calendarData.days[index].available = true;
                            }
                        }
                        if(args.availableOffer != undefined && args.availableOffer && index >= $scope.fromDateIndex && index <= $scope.toDateIndex){
                            $scope.calendarData.days[index].availableOffer = true;
                        }
                    });

                    angular.forEach($scope.calendarData.days, function(op) {
                        var index = $scope.calendarData.days.indexOf(op);

                        if($scope.calendarData.days[index].bookedStart == true && $scope.calendarData.days[index].bookedEnd == true){
                            $scope.calendarData.days[index].available = false;
                        }
                    });

                    $scope.fromDateIndex = null;
                    $scope.toDateIndex = null;
                }, function () {
                    angular.forEach($scope.calendarData.days, function(op){
                        var index = $scope.calendarData.days.indexOf(op);
                        $scope.calendarData.days[index].selected = false;
                    });

                    $scope.fromDateIndex = null;
                    $scope.toDateIndex = null;
                });
            }
        }else if($scope.calendarData.days[index].booked == true){
            var reservation = reservationsFactory.getReservationForDate(day.date);
            if(reservation){
                $window.location.href = reservation.url;
            }
        }else if($scope.calendarData.days[index].reserved == true){
            var reservation = reservationsFactory.getReservationForDate(day.date);
            if(reservation){
                $window.location.href = reservation.url;
            }
        }else if($scope.calendarData.days[index].availableOffer == true){
            var offer = offersFactory.getOfferForDate(day.date);
            if(offer){
                $window.location.href = offer.url;
            }
        }
    };

    applyCalendar();
}]);

angular.bootstrap(document.getElementById("estateCalendarManageAppHandler"),["estateCalendarManageApp"]);
