var charityDonationImage = {
    init: function(){
        if ($('.charityDonationImagePreview').length > 0){
            var icon = $('.charityDonationImagePreview');
            icon.hide();
            if(icon.val() != ''){
                icon.after('<img id="charityDonationImagePreviewAdminImg" src="/' + icon.attr('data-upload-dir') + '/' + icon.val() + '"/>');
            }
        }
    }
}