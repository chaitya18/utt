/*$(window).load(function() {
    (function($, window, undefined){*/
        // Private vars start
        // Private vars end
const TYPE_FLEXIBLE_BOOKING = 2;
const TYPE_STANDARD_M_F = 3;

        pricingManagement = window.pricingManagement || {};
        pricingManagement.estate = function(sName, oHandler, oModal)
        {
            self        = this;
            defaultYear = new Date().getFullYear();
            iCalType    = null;
            oCalHandler = oHandler;
            oCalModal   = oModal;
            sShortName  = sName;
            oPricingLineClass = 
            {
                'standard-1': 'priceLine-standardShort',
                'standard-2': 'priceLine-standardLongMn',
                'standard-3': 'priceLine-standardLongFr',
                'flex-1'    : 'priceLine-flexi'
            };
            oSeasons = {
                'Minimum': '#ccccff',
                'Low': '#ccffcc',
                'Medium': '#ffffcc',
                'High': '#ff99cc',
                'Peak': '#ffcc99',
                'Maximum': '#ff6666'
            };
            iOneDay = 24*60*60*1000;
            
            this.start = function(calType)
            {
                iCalType = calType;
                $('button.prev-year').click(function()
                {
                    defaultYear--;
                    self.resetCalendar(defaultYear);
                });
                $('button.next-year').click(function()
                {
                    defaultYear++;
                    self.resetCalendar(defaultYear);
                });
                self.resetCalendar(defaultYear);
            }
            this.resetCalendar = function(year)
            {
                $('.loader').show();
                var oCalendar   = $('#estatePricingManagement');
                oCalendar.empty();
                $.ajax(
                {
                    type: "POST",
                    dataType: "html",
                    url: Routing.generate('utt_reservation_get_pricing_manage_calendar', { 'year': year }),
                    data: {},
                    success: function(html)
                    {
                        oCalendar.html(html);
                        if (iCalType == TYPE_FLEXIBLE_BOOKING)
                        {
                            oCalendar.find('.' + oPricingLineClass['standard-1']).empty();
                            oCalendar.find('.' + oPricingLineClass['standard-2']).empty();
                            oCalendar.find('.' + oPricingLineClass['standard-3']).empty();
                        }
                        else if (iCalType == TYPE_STANDARD_M_F)
                        {
                            oCalendar.find('.' + oPricingLineClass['flex-1']).empty();
                        }
                        self.startSetPricing();
                    },
                    error: function(xhr, err) {oCalendar.html('Can not load calendar.');}
                });
            }
            this.startSetPricing = function()
            {
                $.ajax(
                {
                    type: "POST",
                    dataType: "json",
                    url: Routing.generate('utt_reservation_get_pricing', { 'estateShortName': sShortName }),
                    data: {},
                    success: function(jAnswer){
//jAnswer = {"success":true,"pricingList":[{"fromDate":"2014-03-14","toDate":"2014-03-15","price":"20.00","initialPrice":"50.00"},{"fromDate":"2014-03-16","toDate":"2014-03-20","price":"23.00","initialPrice":"123.00"},{"fromDate":"2014-03-21","toDate":"2014-03-22","price":"20.00","initialPrice":"50.00"},{"fromDate":"2014-03-23","toDate":"2014-03-27","price":"20.00","initialPrice":"50.00"},{"fromDate":"2014-03-28","toDate":"2014-03-29","price":"20.00","initialPrice":"50.00"},{"fromDate":"2014-03-30","toDate":"2014-04-03","price":"20.00","initialPrice":"50.00"},{"fromDate":"2014-04-04","toDate":"2014-04-05","price":"20.00","initialPrice":"50.00"},{"fromDate":"2014-04-06","toDate":"2014-04-10","price":"20.00","initialPrice":"50.00"},{"fromDate":"2014-04-11","toDate":"2014-04-12","price":"20.00","initialPrice":"50.00"},{"fromDate":"2014-04-13","toDate":"2014-04-17","price":"20.00","initialPrice":"50.00"},{"fromDate":"2014-04-18","toDate":"2014-04-19","price":"20.00","initialPrice":"50.00"},{"fromDate":"2014-04-20","toDate":"2014-04-24","price":"20.00","initialPrice":"50.00"},{"fromDate":"2014-04-25","toDate":"2014-04-26","price":"20.00","initialPrice":"50.00"},{"fromDate":"2014-04-27","toDate":"2014-05-01","price":"20.00","initialPrice":"50.00"},{"fromDate":"2014-05-02","toDate":"2014-05-03","price":"20.00","initialPrice":"50.00"},{"fromDate":"2014-05-04","toDate":"2014-05-08","price":"20.00","initialPrice":"50.00"},{"fromDate":"2014-05-09","toDate":"2014-05-10","price":"20.00","initialPrice":"50.00"},{"fromDate":"2014-05-11","toDate":"2014-05-15","price":"20.00","initialPrice":"50.00"},{"fromDate":"2014-05-16","toDate":"2014-05-17","price":"20.00","initialPrice":"50.00"},{"fromDate":"2014-05-18","toDate":"2014-05-22","price":"20.00","initialPrice":"50.00"},{"fromDate":"2014-05-23","toDate":"2014-05-24","price":"20.00","initialPrice":"50.00"},{"fromDate":"2014-05-25","toDate":"2014-05-29","price":"20.00","initialPrice":"50.00"},{"fromDate":"2014-05-30","toDate":"2014-05-31","price":"20.00","initialPrice":"50.00"},{"fromDate":"2014-06-01","toDate":"2014-06-05","price":"20.00","initialPrice":"50.00"},{"fromDate":"2014-06-06","toDate":"2014-06-07","price":"20.00","initialPrice":"50.00"},{"fromDate":"2014-06-08","toDate":"2014-06-12","price":"20.00","initialPrice":"50.00"},{"fromDate":"2014-06-13","toDate":"2014-06-14","price":"20.00","initialPrice":"50.00"},{"fromDate":"2014-06-15","toDate":"2014-06-19","price":"20.00","initialPrice":"50.00"},{"fromDate":"2014-06-20","toDate":"2014-06-21","price":"20.00","initialPrice":"50.00"},{"fromDate":"2014-06-22","toDate":"2014-06-26","price":"20.00","initialPrice":"50.00"},{"fromDate":"2014-06-27","toDate":"2014-06-28","price":"20.00","initialPrice":"50.00"},{"fromDate":"2014-06-29","toDate":"2014-07-03","price":"20.00","initialPrice":"50.00"},{"fromDate":"2014-07-04","toDate":"2014-07-05","price":"20.00","initialPrice":"50.00"},{"fromDate":"2014-07-06","toDate":"2014-07-10","price":"20.00","initialPrice":"50.00"},{"fromDate":"2014-07-11","toDate":"2014-07-12","price":"20.00","initialPrice":"50.00"},{"fromDate":"2014-07-13","toDate":"2014-07-17","price":"20.00","initialPrice":"50.00"},{"fromDate":"2014-07-18","toDate":"2014-07-19","price":"20.00","initialPrice":"50.00"},{"fromDate":"2014-07-20","toDate":"2014-07-24","price":"20.00","initialPrice":"50.00"},{"fromDate":"2014-07-25","toDate":"2014-07-26","price":"20.00","initialPrice":"50.00"},{"fromDate":"2014-07-27","toDate":"2014-07-31","price":"20.00","initialPrice":"50.00"},{"fromDate":"2014-08-01","toDate":"2014-08-02","price":"20.00","initialPrice":"50.00"},{"fromDate":"2014-08-03","toDate":"2014-08-07","price":"20.00","initialPrice":"50.00"},{"fromDate":"2014-08-08","toDate":"2014-08-09","price":"20.00","initialPrice":"50.00"},{"fromDate":"2014-08-10","toDate":"2014-08-14","price":"20.00","initialPrice":"50.00"},{"fromDate":"2014-08-15","toDate":"2014-08-16","price":"20.00","initialPrice":"50.00"},{"fromDate":"2014-08-17","toDate":"2014-08-21","price":"20.00","initialPrice":"50.00"},{"fromDate":"2014-08-22","toDate":"2014-08-23","price":"20.00","initialPrice":"50.00"},{"fromDate":"2014-08-24","toDate":"2014-08-28","price":"20.00","initialPrice":"50.00"},{"fromDate":"2014-08-29","toDate":"2014-08-30","price":"20.00","initialPrice":"50.00"},{"fromDate":"2014-08-31","toDate":"2014-09-04","price":"20.00","initialPrice":"50.00"},{"fromDate":"2014-09-05","toDate":"2014-09-06","price":"20.00","initialPrice":"50.00"},{"fromDate":"2014-09-07","toDate":"2014-09-11","price":"20.00","initialPrice":"50.00"},{"fromDate":"2014-09-12","toDate":"2014-09-13","price":"20.00","initialPrice":"50.00"},{"fromDate":"2014-09-14","toDate":"2014-09-18","price":"20.00","initialPrice":"50.00"},{"fromDate":"2014-09-19","toDate":"2014-09-20","price":"20.00","initialPrice":"50.00"},{"fromDate":"2014-09-21","toDate":"2014-09-25","price":"20.00","initialPrice":"50.00"},{"fromDate":"2014-09-26","toDate":"2014-09-27","price":"20.00","initialPrice":"50.00"},{"fromDate":"2014-09-28","toDate":"2014-10-02","price":"20.00","initialPrice":"50.00"},{"fromDate":"2014-10-03","toDate":"2014-10-04","price":"20.00","initialPrice":"50.00"},{"fromDate":"2014-10-05","toDate":"2014-10-09","price":"20.00","initialPrice":"50.00"},{"fromDate":"2014-10-10","toDate":"2014-10-11","price":"20.00","initialPrice":"50.00"},{"fromDate":"2014-10-12","toDate":"2014-10-16","price":"20.00","initialPrice":"50.00"},{"fromDate":"2014-10-17","toDate":"2014-10-18","price":"20.00","initialPrice":"50.00"},{"fromDate":"2014-10-19","toDate":"2014-10-23","price":"20.00","initialPrice":"50.00"},{"fromDate":"2014-10-24","toDate":"2014-10-25","price":"20.00","initialPrice":"50.00"},{"fromDate":"2014-10-26","toDate":"2014-10-30","price":"20.00","initialPrice":"50.00"},{"fromDate":"2014-10-31","toDate":"2014-11-01","price":"20.00","initialPrice":"50.00"},{"fromDate":"2014-11-02","toDate":"2014-11-06","price":"20.00","initialPrice":"50.00"},{"fromDate":"2014-11-07","toDate":"2014-11-08","price":"20.00","initialPrice":"50.00"},{"fromDate":"2014-11-09","toDate":"2014-11-13","price":"20.00","initialPrice":"50.00"},{"fromDate":"2014-11-14","toDate":"2014-11-15","price":"20.00","initialPrice":"50.00"},{"fromDate":"2014-11-16","toDate":"2014-11-20","price":"20.00","initialPrice":"50.00"},{"fromDate":"2014-11-21","toDate":"2014-11-22","price":"20.00","initialPrice":"50.00"},{"fromDate":"2014-11-23","toDate":"2014-11-27","price":"20.00","initialPrice":"50.00"},{"fromDate":"2014-11-28","toDate":"2014-11-29","price":"20.00","initialPrice":"50.00"},{"fromDate":"2014-11-30","toDate":"2014-12-04","price":"20.00","initialPrice":"50.00"},{"fromDate":"2014-12-05","toDate":"2014-12-06","price":"20.00","initialPrice":"50.00"},{"fromDate":"2014-12-07","toDate":"2014-12-11","price":"20.00","initialPrice":"50.00"},{"fromDate":"2014-12-12","toDate":"2014-12-13","price":"20.00","initialPrice":"50.00"},{"fromDate":"2014-12-14","toDate":"2014-12-18","price":"20.00","initialPrice":"50.00"},{"fromDate":"2014-12-19","toDate":"2014-12-20","price":"20.00","initialPrice":"50.00"},{"fromDate":"2014-12-21","toDate":"2014-12-25","price":"20.00","initialPrice":"50.00"},{"fromDate":"2014-12-26","toDate":"2014-12-27","price":"20.00","initialPrice":"50.00"}]};
//jAnswer = {"success":true,"pricingList":[{"fromDate":"2014-03-14","toDate":"2014-03-17","price":"100.00"},{"fromDate":"2014-03-14","toDate":"2014-03-21","price":"100.00"},{"fromDate":"2014-03-17","toDate":"2014-03-24","price":"100.00"},{"fromDate":"2014-03-17","toDate":"2014-03-21","price":"100.00"},{"fromDate":"2014-03-21","toDate":"2014-03-28","price":"100.00"},{"fromDate":"2014-03-21","toDate":"2014-03-24","price":"100.00"},{"fromDate":"2014-03-24","toDate":"2014-03-31","price":"100.00"},{"fromDate":"2014-03-24","toDate":"2014-03-28","price":"100.00"},{"fromDate":"2014-03-28","toDate":"2014-03-31","price":"100.00"},{"fromDate":"2014-03-28","toDate":"2014-04-04","price":"100.00"},{"fromDate":"2014-03-31","toDate":"2014-04-04","price":"100.00"},{"fromDate":"2014-03-31","toDate":"2014-04-07","price":"100.00"},{"fromDate":"2014-04-04","toDate":"2014-04-07","price":"100.00"},{"fromDate":"2014-04-04","toDate":"2014-04-11","price":"100.00"},{"fromDate":"2014-04-07","toDate":"2014-04-14","price":"100.00"},{"fromDate":"2014-04-07","toDate":"2014-04-11","price":"100.00"},{"fromDate":"2014-04-11","toDate":"2014-04-18","price":"100.00"},{"fromDate":"2014-04-11","toDate":"2014-04-14","price":"100.00"},{"fromDate":"2014-04-14","toDate":"2014-04-21","price":"100.00"},{"fromDate":"2014-04-14","toDate":"2014-04-18","price":"100.00"},{"fromDate":"2014-04-18","toDate":"2014-04-25","price":"100.00"},{"fromDate":"2014-04-18","toDate":"2014-04-21","price":"100.00"},{"fromDate":"2014-04-21","toDate":"2014-04-25","price":"100.00"},{"fromDate":"2014-04-21","toDate":"2014-04-28","price":"100.00"},{"fromDate":"2014-04-25","toDate":"2014-05-02","price":"100.00"},{"fromDate":"2014-04-25","toDate":"2014-04-28","price":"100.00"},{"fromDate":"2014-04-28","toDate":"2014-05-05","price":"100.00"},{"fromDate":"2014-04-28","toDate":"2014-05-02","price":"100.00"},{"fromDate":"2014-05-02","toDate":"2014-05-09","price":"100.00"},{"fromDate":"2014-05-02","toDate":"2014-05-05","price":"100.00"},{"fromDate":"2014-05-05","toDate":"2014-05-12","price":"100.00"},{"fromDate":"2014-05-05","toDate":"2014-05-09","price":"100.00"},{"fromDate":"2014-05-09","toDate":"2014-05-16","price":"100.00"},{"fromDate":"2014-05-09","toDate":"2014-05-12","price":"100.00"},{"fromDate":"2014-05-12","toDate":"2014-05-19","price":"100.00"},{"fromDate":"2014-05-12","toDate":"2014-05-16","price":"100.00"},{"fromDate":"2014-05-16","toDate":"2014-05-23","price":"100.00"},{"fromDate":"2014-05-16","toDate":"2014-05-19","price":"100.00"},{"fromDate":"2014-05-19","toDate":"2014-05-23","price":"100.00"},{"fromDate":"2014-05-19","toDate":"2014-05-26","price":"100.00"},{"fromDate":"2014-05-23","toDate":"2014-05-30","price":"100.00"},{"fromDate":"2014-05-23","toDate":"2014-05-26","price":"100.00"},{"fromDate":"2014-05-26","toDate":"2014-06-02","price":"100.00"},{"fromDate":"2014-05-26","toDate":"2014-05-30","price":"100.00"},{"fromDate":"2014-05-30","toDate":"2014-06-02","price":"100.00"},{"fromDate":"2014-05-30","toDate":"2014-06-06","price":"100.00"},{"fromDate":"2014-06-02","toDate":"2014-06-06","price":"100.00"},{"fromDate":"2014-06-02","toDate":"2014-06-09","price":"100.00"},{"fromDate":"2014-06-06","toDate":"2014-06-13","price":"100.00"},{"fromDate":"2014-06-06","toDate":"2014-06-09","price":"100.00"},{"fromDate":"2014-06-09","toDate":"2014-06-13","price":"100.00"},{"fromDate":"2014-06-09","toDate":"2014-06-16","price":"100.00"},{"fromDate":"2014-06-13","toDate":"2014-06-20","price":"100.00"},{"fromDate":"2014-06-13","toDate":"2014-06-16","price":"100.00"},{"fromDate":"2014-06-16","toDate":"2014-06-20","price":"100.00"},{"fromDate":"2014-06-16","toDate":"2014-06-23","price":"100.00"},{"fromDate":"2014-06-20","toDate":"2014-06-27","price":"100.00"},{"fromDate":"2014-06-20","toDate":"2014-06-23","price":"100.00"},{"fromDate":"2014-06-23","toDate":"2014-06-30","price":"100.00"},{"fromDate":"2014-06-23","toDate":"2014-06-27","price":"100.00"},{"fromDate":"2014-06-27","toDate":"2014-07-04","price":"100.00"},{"fromDate":"2014-06-27","toDate":"2014-06-30","price":"100.00"},{"fromDate":"2014-06-30","toDate":"2014-07-07","price":"100.00"},{"fromDate":"2014-06-30","toDate":"2014-07-04","price":"100.00"},{"fromDate":"2014-07-04","toDate":"2014-07-07","price":"100.00"},{"fromDate":"2014-07-04","toDate":"2014-07-11","price":"100.00"},{"fromDate":"2014-07-07","toDate":"2014-07-11","price":"100.00"},{"fromDate":"2014-07-07","toDate":"2014-07-14","price":"100.00"},{"fromDate":"2014-07-11","toDate":"2014-07-14","price":"100.00"},{"fromDate":"2014-07-11","toDate":"2014-07-18","price":"100.00"},{"fromDate":"2014-07-14","toDate":"2014-07-18","price":"100.00"},{"fromDate":"2014-07-14","toDate":"2014-07-21","price":"100.00"},{"fromDate":"2014-07-18","toDate":"2014-07-25","price":"100.00"},{"fromDate":"2014-07-18","toDate":"2014-07-21","price":"100.00"},{"fromDate":"2014-07-21","toDate":"2014-07-28","price":"100.00"},{"fromDate":"2014-07-21","toDate":"2014-07-25","price":"100.00"},{"fromDate":"2014-07-25","toDate":"2014-08-01","price":"100.00"},{"fromDate":"2014-07-25","toDate":"2014-07-28","price":"100.00"},{"fromDate":"2014-07-28","toDate":"2014-08-04","price":"100.00"},{"fromDate":"2014-07-28","toDate":"2014-08-01","price":"100.00"},{"fromDate":"2014-08-01","toDate":"2014-08-04","price":"100.00"},{"fromDate":"2014-08-01","toDate":"2014-08-08","price":"100.00"},{"fromDate":"2014-08-04","toDate":"2014-08-11","price":"100.00"},{"fromDate":"2014-08-04","toDate":"2014-08-08","price":"100.00"},{"fromDate":"2014-08-08","toDate":"2014-08-11","price":"100.00"},{"fromDate":"2014-08-08","toDate":"2014-08-15","price":"100.00"},{"fromDate":"2014-08-11","toDate":"2014-08-18","price":"100.00"},{"fromDate":"2014-08-11","toDate":"2014-08-15","price":"100.00"},{"fromDate":"2014-08-15","toDate":"2014-08-22","price":"100.00"},{"fromDate":"2014-08-15","toDate":"2014-08-18","price":"100.00"},{"fromDate":"2014-08-18","toDate":"2014-08-25","price":"100.00"},{"fromDate":"2014-08-18","toDate":"2014-08-22","price":"100.00"},{"fromDate":"2014-08-22","toDate":"2014-08-29","price":"100.00"},{"fromDate":"2014-08-22","toDate":"2014-08-25","price":"100.00"},{"fromDate":"2014-08-25","toDate":"2014-09-01","price":"100.00"},{"fromDate":"2014-08-25","toDate":"2014-08-29","price":"100.00"},{"fromDate":"2014-08-29","toDate":"2014-09-05","price":"100.00"},{"fromDate":"2014-08-29","toDate":"2014-09-01","price":"100.00"},{"fromDate":"2014-09-01","toDate":"2014-09-08","price":"100.00"},{"fromDate":"2014-09-01","toDate":"2014-09-05","price":"100.00"},{"fromDate":"2014-09-05","toDate":"2014-09-12","price":"100.00"},{"fromDate":"2014-09-05","toDate":"2014-09-08","price":"100.00"},{"fromDate":"2014-09-08","toDate":"2014-09-15","price":"100.00"},{"fromDate":"2014-09-08","toDate":"2014-09-12","price":"100.00"},{"fromDate":"2014-09-12","toDate":"2014-09-15","price":"100.00"},{"fromDate":"2014-09-12","toDate":"2014-09-19","price":"100.00"},{"fromDate":"2014-09-15","toDate":"2014-09-19","price":"100.00"},{"fromDate":"2014-09-15","toDate":"2014-09-22","price":"100.00"},{"fromDate":"2014-09-19","toDate":"2014-09-26","price":"100.00"},{"fromDate":"2014-09-19","toDate":"2014-09-22","price":"100.00"},{"fromDate":"2014-09-22","toDate":"2014-09-26","price":"100.00"},{"fromDate":"2014-09-22","toDate":"2014-09-29","price":"100.00"},{"fromDate":"2014-09-26","toDate":"2014-10-03","price":"100.00"},{"fromDate":"2014-09-26","toDate":"2014-09-29","price":"100.00"},{"fromDate":"2014-09-29","toDate":"2014-10-06","price":"100.00"},{"fromDate":"2014-09-29","toDate":"2014-10-03","price":"100.00"},{"fromDate":"2014-10-03","toDate":"2014-10-06","price":"100.00"},{"fromDate":"2014-10-03","toDate":"2014-10-10","price":"100.00"},{"fromDate":"2014-10-06","toDate":"2014-10-13","price":"100.00"},{"fromDate":"2014-10-06","toDate":"2014-10-10","price":"100.00"},{"fromDate":"2014-10-10","toDate":"2014-10-13","price":"100.00"},{"fromDate":"2014-10-10","toDate":"2014-10-17","price":"100.00"},{"fromDate":"2014-10-13","toDate":"2014-10-20","price":"100.00"},{"fromDate":"2014-10-13","toDate":"2014-10-17","price":"100.00"},{"fromDate":"2014-10-17","toDate":"2014-10-24","price":"100.00"},{"fromDate":"2014-10-17","toDate":"2014-10-20","price":"100.00"},{"fromDate":"2014-10-20","toDate":"2014-10-24","price":"100.00"},{"fromDate":"2014-10-20","toDate":"2014-10-27","price":"100.00"},{"fromDate":"2014-10-24","toDate":"2014-10-31","price":"100.00"},{"fromDate":"2014-10-24","toDate":"2014-10-27","price":"100.00"},{"fromDate":"2014-10-27","toDate":"2014-11-03","price":"100.00"},{"fromDate":"2014-10-27","toDate":"2014-10-31","price":"100.00"},{"fromDate":"2014-10-31","toDate":"2014-11-07","price":"100.00"},{"fromDate":"2014-10-31","toDate":"2014-11-03","price":"100.00"},{"fromDate":"2014-11-03","toDate":"2014-11-07","price":"100.00"},{"fromDate":"2014-11-03","toDate":"2014-11-10","price":"100.00"},{"fromDate":"2014-11-07","toDate":"2014-11-10","price":"100.00"},{"fromDate":"2014-11-07","toDate":"2014-11-14","price":"100.00"},{"fromDate":"2014-11-10","toDate":"2014-11-14","price":"100.00"},{"fromDate":"2014-11-10","toDate":"2014-11-17","price":"100.00"},{"fromDate":"2014-11-14","toDate":"2014-11-17","price":"100.00"},{"fromDate":"2014-11-14","toDate":"2014-11-21","price":"100.00"},{"fromDate":"2014-11-17","toDate":"2014-11-24","price":"100.00"},{"fromDate":"2014-11-17","toDate":"2014-11-21","price":"100.00"},{"fromDate":"2014-11-21","toDate":"2014-11-28","price":"100.00"},{"fromDate":"2014-11-21","toDate":"2014-11-24","price":"100.00"},{"fromDate":"2014-11-24","toDate":"2014-11-28","price":"100.00"},{"fromDate":"2014-11-24","toDate":"2014-12-01","price":"100.00"},{"fromDate":"2014-11-28","toDate":"2014-12-01","price":"100.00"},{"fromDate":"2014-11-28","toDate":"2014-12-05","price":"100.00"},{"fromDate":"2014-12-01","toDate":"2014-12-05","price":"100.00"},{"fromDate":"2014-12-01","toDate":"2014-12-08","price":"100.00"},{"fromDate":"2014-12-05","toDate":"2014-12-08","price":"100.00"},{"fromDate":"2014-12-05","toDate":"2014-12-12","price":"100.00"},{"fromDate":"2014-12-08","toDate":"2014-12-12","price":"100.00"},{"fromDate":"2014-12-08","toDate":"2014-12-15","price":"100.00"},{"fromDate":"2014-12-12","toDate":"2014-12-19","price":"100.00"},{"fromDate":"2014-12-12","toDate":"2014-12-15","price":"100.00"},{"fromDate":"2014-12-15","toDate":"2014-12-19","price":"100.00"},{"fromDate":"2014-12-15","toDate":"2014-12-22","price":"100.00"},{"fromDate":"2014-12-19","toDate":"2014-12-26","price":"100.00"},{"fromDate":"2014-12-19","toDate":"2014-12-22","price":"100.00"},{"fromDate":"2014-12-22","toDate":"2014-12-26","price":"100.00"},{"fromDate":"2014-12-22","toDate":"2014-12-29","price":"100.00"},{"fromDate":"2014-12-26","toDate":"2014-12-29","price":"100.00"}]};
                        if(jAnswer && jAnswer.success)
                        {
                            if ( jAnswer.pricingList )
                            {
                                jAnswer.pricingList.forEach(function(record) 
                                {
                                    self.setPricingDays(record);
                                });
                                $("[rel='tooltip']").tooltip();
                                console.log('pricing ready to set');
                            }
                        }
                        $('.loader').hide();
                    },
                    error: function(xhr, err) {}
                });
                
                $.ajax(
                {
                    type: "POST",
                    dataType: "json",
                    url: Routing.generate('utt_reservation_get_pricing_seasons', { 'estateShortName': sShortName }),
                    data: {},
                    success: function(jAnswer)
                    {
                        if(jAnswer && jAnswer.success)
                        {
                            if ( jAnswer.pricingSeasons )
                            {
                                $.each(oSeasons, function(index, value) 
                                {
                                    $('#pricings-seasons-legend').append('<div class="season-one"><div class="background" style="background-color: ' + value + '"></div><div class="name">' + index + '</div></div>');
                                });     
                                
                                jAnswer.pricingSeasons.forEach(function(record) 
                                {
                                    self.setPricingSeasons(record);
                                });
                                console.log('pricing seasons ready to set');
                            }
                        }
                        //console.log(jAnswer);
                    },
                    error: function(xhr, err) {}
                });
            }
            this.setPricingSeasons = function(oObj)
            {
                var startDay  = new Date(Date.parse(oObj.fromDate));
                var endDay    = new Date(Date.parse(oObj.toDate));
                var iDiffDays = Math.round(Math.abs((startDay.getTime() - endDay.getTime())/(iOneDay)));
                
                for ( var i = 0; i <= iDiffDays; i++ )
                {
                    var month = startDay.getMonth() + 1;
                    if ( month < 10 ) month = '0' + month;
                    var day = startDay.getDate();
                    if ( day < 10 ) day = '0' + day;
                    
                    var handler = oCalHandler.find('[day="' + day + '-' + month + '-' + startDay.getFullYear() + '"]');
                    handler.find('.day-details div:first').each(function()
                    {
                        $(this).css('background-color', oSeasons[oObj.typeName]);
                    });
                    
                    startDay.setDate( startDay.getDate() + 1 );
                }
            }
            this.setPricingDays = function(oObj)
            {
                var startDay  = new Date(Date.parse(oObj.fromDate));
                var endDay    = new Date(Date.parse(oObj.toDate));
                var iDiffDays = Math.round(Math.abs((startDay.getTime() - endDay.getTime())/(iOneDay)));
                var sType     = self.returnLineType(oObj);
                
                for ( var i = 0; i <= iDiffDays; i++ )
                {
                    var month = startDay.getMonth() + 1;
                    if ( month < 10 ) month = '0' + month;
                    var day = startDay.getDate();
                    if ( day < 10 ) day = '0' + day;
                    
                    oCalHandler.find('[day="' + day + '-' + month + '-' + startDay.getFullYear() + '"]').find('.' + oPricingLineClass[sType]).each(function()
                    {
                        var oElem = $(this);
                        if( sType == 'flex-1' )
                        {
                            oElem.find('.part-1')
                                .attr('data-toggle', 'modal')
                                .attr('data-target', '#my-pricing-modal')
                                .click(function(){self.showPricingDetails(oObj, this)});
                            oElem.find('.part-2')
                                .attr('data-toggle', 'modal')
                                .attr('data-target', '#my-pricing-modal')
                                .click(function(){self.showPricingDetails(oObj, this)});
                            
                            var tmpInicial = ( typeof oObj.initialPrice != "undefined" ) ? ' (inicial: ' + oObj.initialPrice + ')' : '';
                            oElem.attr('title', oObj.fromDate + ' : ' + oObj.toDate + ' = ' + oObj.price + tmpInicial);
                        }
                        else
                        {
                            if ( i == 0 )
                            {
                                oElem.find('.part-2')
                                    .attr('data-toggle', 'modal')
                                    .attr('data-target', '#my-pricing-modal')
                                    .click(function(){self.showPricingDetails(oObj, this)});

                                var tmpInicial = ( typeof oObj.initialPrice != "undefined" ) ? ' (inicial: ' + oObj.initialPrice + ')' : '';
                                oElem.attr('title', oObj.fromDate + ' : ' + oObj.toDate + ' = ' + oObj.price + tmpInicial);
                            }
                            else if ( i == iDiffDays )
                            {
                                oElem.find('.part-1')
                                    .attr('data-toggle', 'modal')
                                    .attr('data-target', '#my-pricing-modal')
                                    .click(function(){self.showPricingDetails(oObj, this)});
                            }
                            else
                            {
                                oElem.find('.part-1')
                                    .attr('data-toggle', 'modal')
                                    .attr('data-target', '#my-pricing-modal')
                                    .click(function(){self.showPricingDetails(oObj, this)});
                                oElem.find('.part-2')
                                    .attr('data-toggle', 'modal')
                                    .attr('data-target', '#my-pricing-modal')
                                    .click(function(){self.showPricingDetails(oObj, this)});

                                var tmpInicial = ( typeof oObj.initialPrice != "undefined" ) ? ' (inicial: ' + oObj.initialPrice + ')' : '';
                                oElem.attr('title', oObj.fromDate + ' : ' + oObj.toDate + ' = ' + oObj.price + tmpInicial);
                            }
                        }
                    });
                    
                    startDay.setDate( startDay.getDate() + 1 );
                }
            }
            this.showPricingDetails = function(oObj, oHandler)
            {
                var tmp = oCalModal.find('.initialPrice');
                tmp.val(-1);
                tmp.parent().hide();
                
                for (var prop in oObj)
                {
                    if(oObj.hasOwnProperty(prop))
                    {
                        if (prop == 'price')
                        {
                            oCalModal.find('.' + prop).val(oObj[prop]);
                        }
                        else if (prop == 'initialPrice')
                        {
                            //var tmp = oCalModal.find('.initialPrice');
                            tmp.val(oObj[prop]);
                            tmp.parent().show();
                        }
                        else
                        {
                            oCalModal.find('.' + prop).html(oObj[prop]);
                        }
                    }
                }

                oCalModal.find('.save-this').unbind().click(function()
                {
                    oObj.price = oCalModal.find('.price').val();
                    if ( typeof oObj.initialPrice != "undefined" )
                        oObj.initialPrice = oCalModal.find('.initialPrice').val();
                    
                    $(oHandler).unbind().click(function(){self.showPricingDetails(oObj, oHandler)});
                    self.resetPricing(oObj, 0);
                });
                oCalModal.find('.save-season').unbind().click(function()
                {
                    oObj.price = oCalModal.find('.price').val();
                    if ( typeof oObj.initialPrice != "undefined" )
                        oObj.initialPrice = oCalModal.find('.initialPrice').val();
                    
                    $(oHandler).unbind().click(function(){self.showPricingDetails(oObj, oHandler)});
                    self.resetPricing(oObj, 1);
                });
                oCalModal.find('.save-type').unbind().click(function()
                {
                    oObj.price = oCalModal.find('.price').val();
                    if ( typeof oObj.initialPrice != "undefined" )
                        oObj.initialPrice = oCalModal.find('.initialPrice').val();
                    
                    $(oHandler).unbind().click(function(){self.showPricingDetails(oObj, oHandler)});
                    self.resetPricing(oObj, 2);
                });
            }
            this.resetPricing = function(oObj, bForNexts)
            {
                var startDay  = new Date(Date.parse(oObj.fromDate));
                var endDay    = new Date(Date.parse(oObj.toDate));
                var iDiffDays = Math.round(Math.abs((startDay.getTime() - endDay.getTime())/(iOneDay)));
                var sType     = self.returnLineType(oObj);
                var tmpInicial = ( typeof oObj.initialPrice != "undefined" ) ? ' (inicial: ' + oObj.initialPrice + ')' : '';

                for ( var i = 0; i <= iDiffDays; i++ )
                {
                    var month = startDay.getMonth() + 1;
                    if ( month < 10 ) month = '0' + month;
                    var day = startDay.getDate();
                    if ( day < 10 ) day = '0' + day;

                    oCalHandler
                        .find('[day="' + day + '-' + month + '-' + startDay.getFullYear() + '"]')
                        .find('[rel="tooltip"]')
                        .attr('data-original-title', oObj.fromDate + ' : ' + oObj.toDate + ' = ' + oObj.price + tmpInicial)
                        .attr('title', oObj.fromDate + ' : ' + oObj.toDate + ' = ' + oObj.price + tmpInicial);
                        
                    startDay.setDate( startDay.getDate() + 1 );
                }
                    
                var url;
                if ( typeof oObj.initialPrice != "undefined" )
                {
                    url = Routing.generate('utt_reservation_pricing_update_flexible', { 'estateShortName': sShortName, 'fromDate': oObj.fromDate, 'toDate': oObj.toDate, 'price': oObj.price, 'initialPrice': oObj.initialPrice });
                }
                else
                {
                    url = Routing.generate('utt_reservation_pricing_update_standard', { 'estateShortName': sShortName, 'fromDate': oObj.fromDate, 'toDate': oObj.toDate, 'price': oObj.price });
                }
                
                $.ajax(
                {
                    type: "POST",
                    dataType: "json",
                    url: url,
                    data: {},
                    success: function(jAnswer){
                        if(jAnswer && jAnswer.success)
                        {
                            oCalModal.modal('hide');
                        }
                    },
                    error: function(xhr, err) {}
                });
            }
            this.returnLineType = function(oObj)
            {
                if( typeof oObj.initialPrice != "undefined" )
                {
                    return 'flex-1';
                }
                
                var startDay    = new Date(Date.parse(oObj.fromDate));
                var endDay      = new Date(Date.parse(oObj.toDate));
                if ( startDay.getDay() == 1 && endDay.getDay() == 1 )
                {
                    return 'standard-2';
                }
                else if ( startDay.getDay() == 5 && endDay.getDay() == 5 )
                {
                    return 'standard-3';
                }
                else
                {
                    return 'standard-1';
                }
            }
        }
    /*})(jQuery, window);
});*/