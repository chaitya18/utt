var adminStatisticsApp = angular.module('adminStatisticsApp', []);

adminStatisticsApp.factory('apiService', function($http) {
    $http.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';
    $http.defaults.headers.post["Content-Type"] = "application/x-www-form-urlencoded";

    return {
        getData: function(year, callback, callbackError){
            $http({
                method: 'GET',
                url: Routing.generate('utt_admin_statistics_data', {'year': year}),
                headers: {'Content-Type':'application/x-www-form-urlencoded; charset=UTF-8'}
            })
                .success(function(data, status, headers, config) {
                    var dataOb = angular.fromJson(data);
                    if(!(typeof dataOb.success === typeof undefined) && dataOb.success == true && !(typeof dataOb.data === typeof undefined)){
                        callback(dataOb.data);
                    }else{
                        callbackError(false);
                    }
                })
                .error(function(data, status, headers, config) {
                    callbackError(false);
                });
        },
    }
});

adminStatisticsApp.controller('adminStatisticsAppCtrl', ['$scope', '$window', 'apiService', function($scope, $window, apiService){

    $scope.data = [];
    $scope.sortType     = 'paid';
    $scope.sortReverse  = true;
    $scope.searchQuery   = '';

    apiService.getData($yearSelected, function(result){
        $scope.data = result;
    }, function(){

    });

}]);

angular.bootstrap(document.getElementById("adminStatisticsAppHandler"),["adminStatisticsApp"]);