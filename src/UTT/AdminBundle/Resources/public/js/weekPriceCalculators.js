var weekPriceCalculators = {
    init: function(){
        if ($('.initWeekPriceCalculator').length > 0){
            $('.initWeekPriceCalculator').each(function(){
                var $output = $(this);

                weekPriceCalculators.refresh($output, 7);

                $output.parent().parent().find('input[id$="_priceStandingCharge"]').on('change', function(){
                    weekPriceCalculators.refresh($output, 7);
                });
                $output.parent().parent().find('input[id$="_priceSundayThursday"]').on('change', function(){
                    weekPriceCalculators.refresh($output, 7);
                });
                $output.parent().parent().find('input[id$="_priceFridaySaturday"]').on('change', function(){
                    weekPriceCalculators.refresh($output, 7);
                });
            });
        }
        if ($('.initWeekendPriceCalculator').length > 0){
            $('.initWeekendPriceCalculator').each(function(){
                var $output = $(this);

                weekPriceCalculators.refresh($output, 3);

                $output.parent().parent().find('input[id$="_priceStandingCharge"]').on('change', function(){
                    weekPriceCalculators.refresh($output, 3);
                });
                $output.parent().parent().find('input[id$="_priceSundayThursday"]').on('change', function(){
                    weekPriceCalculators.refresh($output, 3);
                });
                $output.parent().parent().find('input[id$="_priceFridaySaturday"]').on('change', function(){
                    weekPriceCalculators.refresh($output, 3);
                });
            });
        }
    },
    refresh: function($output, type){
        var $standingCharge = false;
        var $priceSunThu = false;
        var $priceFriSat = false;

        $output.parent().parent().find('input[id$="_priceStandingCharge"]').each(function(){
            $standingCharge = $(this);
        });
        $output.parent().parent().find('input[id$="_priceSundayThursday"]').each(function(){
            $priceSunThu = $(this);
        });
        $output.parent().parent().find('input[id$="_priceFridaySaturday"]').each(function(){
            $priceFriSat = $(this);
        });

        if($standingCharge && $priceSunThu && $priceFriSat){
            if(type == 7){
                var newPrice = parseFloat($standingCharge.val()) + (parseFloat($priceSunThu.val()) * 5) + (parseFloat($priceFriSat.val()) * 2);
                $output.val(newPrice.toFixed(2));
            }else if(type == 3){
                var newPrice = parseFloat($standingCharge.val()) + (parseFloat($priceFriSat.val()) * 2) + (parseFloat($priceSunThu.val()));
                $output.val(newPrice.toFixed(2));
            }
        }
    }
}