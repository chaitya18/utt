$(window).load(function() {
    estatePhotos.init();
    estateFeatureIcon.init();
    charityDonationImage.init();
    estateCategoryImage.init();
    homeBannerImage.init();
    homeBannersSortOrder.init();
    offerImage.init();
    weekPriceCalculators.init();
    pricingCategoryToolkit.init();
    pricingCategoryYearLine.init();
    selectPricingCategory.init();
    discountCodeEstatesSelectAll.init();

    $(".colorPicker").spectrum({
        preferredFormat: "hex"
    });

    if($('#propertyRedirectSelect select').length > 0){
        var redirectSelect = $('#propertyRedirectSelect select');
        redirectSelect.change(function(){
            window.location.href = Routing.generate('utt_estate_estate_admin_edit', {'id': $(this).val()});
        });
    }

    if($('#propertyCalendarRedirectSelect select').length > 0){
        var redirectSelect = $('#propertyCalendarRedirectSelect select');
        redirectSelect.change(function(){
            window.location.href = Routing.generate('utt_estate_estate_admin_calendar', {'id': $(this).val()});
        });
    }

    if($('#propertyPhotosRedirectSelect select').length > 0){
        var redirectSelect = $('#propertyPhotosRedirectSelect select');
        redirectSelect.change(function(){
            window.location.href = Routing.generate('utt_estate_estate_admin_photos', {'id': $(this).val()});
        });
    }

    if($('#ownerStatementsFilterDate').length > 0){
        var redirectSelect = $('#ownerStatementsFilterDate');
        redirectSelect.change(function(){
            window.location.href = Routing.generate('utt_admin_owner_statements', {'date': $(this).val()});
        });
    }

    if($('#accountsFilters').length > 0 && $('#accountsFilterDate').length > 0 && $('#accountsFilterEstate').length > 0){
        var accountsFilterDateSelect = $('#accountsFilterDate');
        var accountsFilterEstateSelect = $('#accountsFilterEstate');

        if(accountsFilterDateSelect.val()){
            accountsFilterDateSelect.change(function(){
                if(accountsFilterEstateSelect.val() == 'summarise'){
                    window.location.href = Routing.generate('utt_admin_accounts_summarise', {
                        'date': accountsFilterDateSelect.val()
                    });
                }else{
                    window.location.href = Routing.generate('utt_admin_accounts_by_date_and_estate', {
                        'date': accountsFilterDateSelect.val(),
                        'estateId': accountsFilterEstateSelect.val()
                    });
                }
            });
            accountsFilterEstateSelect.change(function(){
                if(accountsFilterEstateSelect.val() == 'summarise'){
                    window.location.href = Routing.generate('utt_admin_accounts_summarise', {
                        'date': accountsFilterDateSelect.val()
                    });
                }else{
                    window.location.href = Routing.generate('utt_admin_accounts_by_date_and_estate', {
                        'date': accountsFilterDateSelect.val(),
                        'estateId': accountsFilterEstateSelect.val()
                    });
                }
            });
        }
    }

    if($('#annualReportFilters').length > 0 && $('#annualReportFilterYear').length > 0 && $('#annualReportFilterEstate').length > 0){
        var annualReportFilterYearSelect = $('#annualReportFilterYear');
        var annualReportFilterEstateSelect = $('#annualReportFilterEstate');

        if(annualReportFilterYearSelect.val()){
            annualReportFilterYearSelect.change(function(){
                window.location.href = Routing.generate('utt_admin_annual_report_by_year_and_estate', {
                    'year': annualReportFilterYearSelect.val(),
                    'estateId': annualReportFilterEstateSelect.val()
                });
            });
            annualReportFilterEstateSelect.change(function(){
                window.location.href = Routing.generate('utt_admin_annual_report_by_year_and_estate', {
                    'year': annualReportFilterYearSelect.val(),
                    'estateId': annualReportFilterEstateSelect.val()
                });
            });
        }
    }

    if($('#cleaningRotaFilters').length > 0 && $('#cleaningRotaFilterEstate').length > 0){
        var cleaningRotaFilterEstateSelect = $('#cleaningRotaFilterEstate');

        if(cleaningRotaFilterEstateSelect.val()){
            cleaningRotaFilterEstateSelect.change(function(){
                window.location.href = Routing.generate('utt_admin_cleaning_rota_by_estate', {
                    'estateId': cleaningRotaFilterEstateSelect.val()
                });
            });
        }
    }

    if($('#editFormTabs').length > 0){
        $('#editFormTabs').tabs({
            active: $.cookie("editFormTabsActive"),
            activate: function( event, ui ) {
                var active = $('#editFormTabs').tabs("option", "active");
                $.cookie("editFormTabsActive", active);
            }
        });
        $.cookie("editFormTabsActive", 0);
    }

    if($('.categoriesSeparatorInit').length > 0){
        var separatorPreferred = $('.categoriesSeparatorInit').attr('data-separator-preferred');
        var i = 0;
        if(separatorPreferred > 0){
            $('.categoriesSeparatorInit li').each(function(){
                i++;

                if(i == separatorPreferred){
                    $(this).after('<li>------------------------------------------------------------</li>');

                    return true;
                }
            });
        }
    };

    if($('#charity-donation-liability').length > 0){
        $.ajax({
            type: "GET",
            dataType: "json",
            url: Routing.generate('utt_admin_charity_liability', {}, true),
            success: function(data){
                if(data){
                    if(data.success == true && data.data){
                        $('#charity-donation-liability .charityPayments').html(parseFloat(data.data.charityPayments).toFixed(2));
                        $('#charity-donation-liability .charityDonations').html(parseFloat(data.data.charityDonations).toFixed(2));
                        $('#charity-donation-liability .charityLiability').html(parseFloat(parseFloat(data.data.charityDonations) - parseFloat(data.data.charityPayments)).toFixed(2));
                        $('#charity-donation-liability').show();
                    }else{ }
                }else{ }
            },
            error: function(xhr, err) {
            }
        });
    }

    if($('#resendArrivalInstructionMailButton').length > 0){
        var arrivalInstructionMailButton = $('#resendArrivalInstructionMailButton');

        arrivalInstructionMailButton.click(function(e){
            e.preventDefault();

            var confirmResult = confirm("Are you sure?");
            if(confirmResult == true){
                window.location.href = arrivalInstructionMailButton.attr('href');
            }
        });
    }
});

// TODO sprawdzić czy nie da sie tego inaczej zrobić bo to jest workaround
$.fn.confirmExit = function() {
    return true;
};