var statisticsCharts = {
    labels: ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"],
    colors: {
        pastYears: {
            fillColor: "rgba(220,220,220,0.5)",
            strokeColor: "rgba(220,220,220,0.8)",
            highlightFill: "rgba(220,220,220,0.75)",
            highlightStroke: "rgba(220,220,220,1)"
        },
        previousYear: {
            fillColor: "rgba(136,140,115,0.5)",
            strokeColor: "rgba(136,140,115,0.8)",
            highlightFill: "rgba(136,140,115,0.75)",
            highlightStroke: "rgba(92,95,76,1)"
        },
        currentYear: {
            fillColor: "rgba(135,27,9,0.5)",
            strokeColor: "rgba(135,27,9,0.8)",
            highlightFill: "rgba(135,27,9,0.75)",
            highlightStroke: "rgba(106,21,7,1)"
        }
    },
    init: function(){
        $.ajax({
            type: "GET",
            dataType: "json",
            url: Routing.generate('utt_admin_number_of_properties_over_time', {}, true),
            success: function(data){
                if(data){
                    if(data.success == true && data.data){
                        statisticsCharts.refreshLivePropertiesChart(data.data);
                    }else{ }
                }else{ }
            },
            error: function(xhr, err) {
            }
        });

        if($('#statisticsFilterYear').length > 0){
            var statisticsFilterYearSelect = $('#statisticsFilterYear');

            if(statisticsFilterYearSelect.val()){
                statisticsFilterYearSelect.change(function(){
                    window.location.href = Routing.generate('utt_admin_statistics', {
                        'year': statisticsFilterYearSelect.val()
                    });
                });
            }
        }
    },
    refreshLivePropertiesChart: function(data){
        var labels = [];

        var datasetData = [];
        for(var i in data){
            labels.push(data[i].day);
            datasetData.push(data[i].numberOfProperties);
        }

        var dataset = {
            label: '', data: datasetData, fillColor: statisticsCharts.colors.currentYear.fillColor, strokeColor: statisticsCharts.colors.currentYear.strokeColor, highlightFill: statisticsCharts.colors.currentYear.highlightFill, highlightStroke: statisticsCharts.colors.currentYear.highlightStroke
        };

        var ctx = document.getElementById('livePropertiesChart').getContext("2d");
        var chart = new Chart(ctx).Bar({
            labels: labels, datasets: [dataset]
        }, {
            responsive: true
        });
    },
};

statisticsCharts.init();