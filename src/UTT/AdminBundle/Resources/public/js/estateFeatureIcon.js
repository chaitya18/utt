var estateFeatureIcon = {
    init: function(){
        if ($('.estateFeatureIconPreview').length > 0){
            var icon = $('.estateFeatureIconPreview');
            icon.hide();
            if(icon.val() != ''){
                icon.after('<img id="estateFeatureIconPreviewAdminImg" src="/' + icon.attr('data-upload-dir') + '/' + icon.val() + '"/>');
            }
        }
    }
}