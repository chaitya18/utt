var estatesVisitsChart = {
    labels: ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"],
    colors: {
        pastYears: {
            fillColor: "rgba(220,220,220,0.5)",
            strokeColor: "rgba(220,220,220,0.8)",
            highlightFill: "rgba(220,220,220,0.75)",
            highlightStroke: "rgba(220,220,220,1)"
        },
        previousYear: {
            fillColor: "rgba(136,140,115,0.5)",
            strokeColor: "rgba(136,140,115,0.8)",
            highlightFill: "rgba(136,140,115,0.75)",
            highlightStroke: "rgba(92,95,76,1)"
        },
        currentYear: {
            fillColor: "rgba(135,27,9,0.5)",
            strokeColor: "rgba(135,27,9,0.8)",
            highlightFill: "rgba(135,27,9,0.75)",
            highlightStroke: "rgba(106,21,7,1)"
        }
    },
    init: function(){
        var page = $page;

        $.ajax({
            type: "GET",
            dataType: "json",
            url: Routing.generate('utt_admin_visits_of_estates', {
                'page': page
            }, true),
            success: function(data){
                if(data){
                    if(data.success == true && data.data){
                        estatesVisitsChart.refreshEstatesVisitsChart(data.data);
                    }else{ }
                }else{ }
            },
            error: function(xhr, err) {
            }
        });
        $.ajax({
            type: "GET",
            dataType: "json",
            url: Routing.generate('utt_admin_visits_of_estates_for_latest_days', {
                'page': page
            }, true),
            success: function(data){
                if(data){
                    if(data.success == true && data.data){
                        estatesVisitsChart.refreshEstatesVisitsForLatestDaysChart(data.data);
                    }else{ }
                }else{ }
            },
            error: function(xhr, err) {
            }
        });
    },
    refreshEstatesVisitsChart: function(data){
        var labels = [];

        var datasetData1 = [];
        var datasetData2 = [];
        var datasetData3 = [];
        for(var i in data){
            labels.push(data[i].name);
            if(data[i].months[0]){ datasetData1.push(data[i].months[0].visits); }else{ datasetData1.push(0); }
            if(data[i].months[1]){ datasetData2.push(data[i].months[1].visits); }else{ datasetData2.push(0); }
            if(data[i].months[2]){ datasetData3.push(data[i].months[2].visits); }else{ datasetData3.push(0); }
        }

        var dataset1 = {
            label: '', data: datasetData1, fillColor: estatesVisitsChart.colors.currentYear.fillColor, strokeColor: estatesVisitsChart.colors.currentYear.strokeColor, highlightFill: estatesVisitsChart.colors.currentYear.highlightFill, highlightStroke: estatesVisitsChart.colors.currentYear.highlightStroke
        };
        var dataset2 = {
            label: '', data: datasetData2, fillColor: estatesVisitsChart.colors.previousYear.fillColor, strokeColor: estatesVisitsChart.colors.previousYear.strokeColor, highlightFill: estatesVisitsChart.colors.previousYear.highlightFill, highlightStroke: estatesVisitsChart.colors.previousYear.highlightStroke
        };
        var dataset3 = {
            label: '', data: datasetData3, fillColor: estatesVisitsChart.colors.pastYears.fillColor, strokeColor: estatesVisitsChart.colors.pastYears.strokeColor, highlightFill: estatesVisitsChart.colors.pastYears.highlightFill, highlightStroke: estatesVisitsChart.colors.pastYears.highlightStroke
        };

        var ctx = document.getElementById('estatesVisitsChart').getContext("2d");
        var chart = new Chart(ctx).Bar({
            labels: labels, datasets: [dataset3, dataset2, dataset1]
        }, {
            responsive: true
        });
    },
    refreshEstatesVisitsForLatestDaysChart: function(data){
        var labels = [];

        var datasetData1 = [];
        var datasetData7 = [];
        var datasetData30 = [];
        for(var i in data){
            labels.push(data[i].name);
            datasetData1.push(data[i].daysVisits1);
            datasetData7.push(data[i].daysVisits7);
            datasetData30.push(data[i].daysVisits30);
        }

        var dataset1 = {
            label: '', data: datasetData1, fillColor: estatesVisitsChart.colors.currentYear.fillColor, strokeColor: estatesVisitsChart.colors.currentYear.strokeColor, highlightFill: estatesVisitsChart.colors.currentYear.highlightFill, highlightStroke: estatesVisitsChart.colors.currentYear.highlightStroke
        };
        var dataset7 = {
            label: '', data: datasetData7, fillColor: estatesVisitsChart.colors.currentYear.fillColor, strokeColor: estatesVisitsChart.colors.currentYear.strokeColor, highlightFill: estatesVisitsChart.colors.currentYear.highlightFill, highlightStroke: estatesVisitsChart.colors.currentYear.highlightStroke
        };
        var dataset30 = {
            label: '', data: datasetData30, fillColor: estatesVisitsChart.colors.previousYear.fillColor, strokeColor: estatesVisitsChart.colors.previousYear.strokeColor, highlightFill: estatesVisitsChart.colors.previousYear.highlightFill, highlightStroke: estatesVisitsChart.colors.previousYear.highlightStroke
        };

        var ctx = document.getElementById('estatesVisitsForLatestDaysChart').getContext("2d");
        var chart = new Chart(ctx).Bar({
            labels: labels, datasets: [dataset30, dataset7, dataset1]
        }, {
            responsive: true
        });
    },
};

estatesVisitsChart.init();