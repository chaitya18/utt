var pricingCategoryYearLine = {
    init: function(){
        if ($('.initPricingCategoryYearLine').length > 0){
            var year = $('.pricingCategoryYear').val();

            $('.pricingCategoryYear').each(function(){
                if(year != $(this).val()){
                    year = $(this).val();
                    $(this).parent().parent().css('border-top', 'solid 3px black');
                }
            });
        }
    }
};