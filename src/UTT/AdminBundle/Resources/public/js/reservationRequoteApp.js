jQuery(function($){
    $.datepicker.setDefaults($.datepicker.regional['en-GB']);
});

var reservationRequoteApp = angular.module('reservationRequoteApp', []);

reservationRequoteApp.factory('discountPriceFactory', function() {
    var discountCodePrice = 0;

    var setDiscountCodePrice = function(price){ discountCodePrice = price; };
    var getDiscountCodePrice = function(){ return discountCodePrice; };

    return {
        setDiscountCodePrice: setDiscountCodePrice,
        getDiscountCodePrice: getDiscountCodePrice,

        getPrice: function(){
            return {
                discountCodePrice: getDiscountCodePrice(),
                total: function(){
                    return parseFloat(getDiscountCodePrice());
                }
            }
        }
    }
});

reservationRequoteApp.factory('priceFactory', function() {
    var basePrice = 0;
    var additionalSleepsPrice = 0;
    var adminCharge = 0;
    var creditCardCharge = 0;
    var bookingProtectCharge = 0;
    var charityCharge = 0;
    var petsPrice = 0;

    var setBasePrice = function(price){ basePrice = price; };
    var getBasePrice = function(){ return basePrice; };
    var setAdditionalSleepsPrice = function(price) { additionalSleepsPrice = price; };
    var getAdditionalSleepsPrice = function(){ return additionalSleepsPrice; };
    var setAdminCharge = function(price) { adminCharge = price; };
    var getAdminCharge = function(){ return adminCharge; };
    var setCreditCardCharge = function(price) { creditCardCharge = price; };
    var getCreditCardCharge = function(){ return creditCardCharge; };
    var setBookingProtectCharge = function(price) { bookingProtectCharge = price; };
    var getBookingProtectCharge = function(){ return bookingProtectCharge; };
    var setCharityCharge = function(price) { charityCharge = price; };
    var getCharityCharge = function(){ return charityCharge; };
    var setPetsPrice = function(price){ petsPrice = price; };
    var getPetsPrice = function(){ return petsPrice; };
   
    return {
        setBasePrice: setBasePrice,
        getBasePrice: getBasePrice,
        setAdditionalSleepsPrice: setAdditionalSleepsPrice,
        getAdditionalSleepsPrice: getAdditionalSleepsPrice,
        setAdminCharge: setAdminCharge,
        getAdminCharge: getAdminCharge,
        setCreditCardCharge: setCreditCardCharge,
        getCreditCardCharge: getCreditCardCharge,
        setBookingProtectCharge: setBookingProtectCharge,
        getBookingProtectCharge: getBookingProtectCharge,
        setCharityCharge: setCharityCharge,
        getCharityCharge: getCharityCharge,
        setPetsPrice: setPetsPrice,
        getPetsPrice: getPetsPrice,
       
        getPrice: function(){
            return {
                basePrice: getBasePrice(),
                additionalSleepsPrice: getAdditionalSleepsPrice(),
                adminCharge: getAdminCharge(),
                creditCardCharge: getCreditCardCharge(),
                bookingProtectCharge: getBookingProtectCharge(),
                charityCharge: getCharityCharge(),
                petsPrice: getPetsPrice(),
               
                total: function(){
                    return parseFloat(getBasePrice()) + parseFloat(getAdditionalSleepsPrice()) + parseFloat(getAdminCharge()) + parseFloat(getCreditCardCharge()) + parseFloat(getBookingProtectCharge()) + parseFloat(getCharityCharge()) + parseFloat(getPetsPrice());
                }
            }
        }
    }
});

reservationRequoteApp.factory('apiService', function($http) {
    $http.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';
    $http.defaults.headers.post["Content-Type"] = "application/x-www-form-urlencoded";

    return {
        getPrice: function(params, callback){
            $http({
                method: 'POST',
                url: Routing.generate('utt_reservation_manage_price'),
                data: $.param(params),
                headers: {'Content-Type':'application/x-www-form-urlencoded; charset=UTF-8'}
            })
                .success(function(data, status, headers, config) {
                    var dataOb = angular.fromJson(data);
                    if(!(typeof dataOb.success === typeof undefined) && dataOb.success == true && !(typeof dataOb.newReservationPrice === typeof undefined)){
                        callback(dataOb.newReservationPrice);
                    }else{
                        callback(false);
                    }
                })
                .error(function(data, status, headers, config) {
                    callback(false);
                });
        },
        requoteSubmit: function(params, callback){
            $http({
                method: 'POST',
                url: $('#reservationRequoteForm').attr('action'),
                data: $.param(params),
                headers: {'Content-Type':'application/x-www-form-urlencoded; charset=UTF-8'}
            })
                .success(function(data, status, headers, config) {
                    var dataOb = angular.fromJson(data);
                    if(!(typeof dataOb.success === typeof undefined) && dataOb.success == true){
                       
                        callback(true);
                    }else{
                        callback(false);
                    }
                })
                .error(function(data, status, headers, config) {
                    callback(false);
                });
        },
        getActiveEstates: function(callback){
            $http({
                method: 'GET',
                url: Routing.generate('utt_estate_get_active_estates'),
                headers: {'Content-Type':'application/x-www-form-urlencoded; charset=UTF-8'}
            })
                .success(function(data, status, headers, config) {
                    var dataOb = angular.fromJson(data);
                    if(!(typeof dataOb.success === typeof undefined) && dataOb.success == true && !(typeof dataOb.estates === typeof undefined)){
                        callback(dataOb.estates);
                    }else{
                        callback(false);
                    }
                })
                .error(function(data, status, headers, config) {
                    callback(false);
                });
        }
    }
});

reservationRequoteApp.factory('filterFactory', function() {
    var maxSleeps = false;
    var maxSleepsChildren = false;
    var maxPets = false;
    var maxInfants = false;

    var estateChoices = [];
    var sleepsChoices = [];
    var sleepsChildrenChoices = [];
    var petsChoices = [];
    var infantsChoices = [];
    var getEstateChoices = function() { return estateChoices; };
    var getSleepsChoices = function(){ return sleepsChoices; };
    var getSleepsChildrenChoices = function(){ return sleepsChildrenChoices; };
    var getPetsChoices = function(){ return petsChoices; };
    var getInfantsChoices = function(){ return infantsChoices; };

    var estate = false;
    var estateShortName = false;
    var sleeps = false;
    var sleepsChildren = false;
    var pets = false;
    var infants = false;
    var fromDate = null;
    var toDate = null;
    var setEstate = function(estateItem){ estate = estateItem; };
    var getEstate = function(){ return estate; };
    var setEstateShortName = function(estateItem){ estateShortName = estateItem; };
    var getEstateShortName = function(){ return estateShortName; };
    var setSleeps = function(sleepsItem){ sleeps = sleepsItem; };
    var getSleeps = function(){ return sleeps; };
    var setSleepsChildren = function(sleepsChildrenItem){ sleepsChildren = sleepsChildrenItem; };
    var getSleepsChildren = function(){ return sleepsChildren; };
    var setPets = function(petsItem){ pets = petsItem; };
    var getPets = function() { return pets; };
    var setInfants = function(infantsItem){ infants = infantsItem; };
    var getInfants = function() { return infants; };
    var setFromDate = function(date) { fromDate = date; };
    var getFromDate = function() { return fromDate; };
    var setToDate = function(date) { toDate = date; };
    var getToDate = function() { return toDate; };

    var initEstates = function(estates){
        estateChoices = estates;
    };

    var initSleeps = function(sleeps){
        maxSleeps = sleeps;

        sleepsChoices = [];
        for(var i=2; i<=maxSleeps; i++){
            sleepsChoices.push({ 'id': i });
        }
    };

    var initSleepsChildren = function(sleepsChildren){
        maxSleepsChildren = sleepsChildren;

        sleepsChildrenChoices = [];
        for(var i=0; i<=maxSleepsChildren; i++){
            sleepsChildrenChoices.push({ 'id': i });
        }
    };

    var initPets = function(pets){
        maxPets = pets;

        petsChoices = [];
        for(var i=0; i<=maxPets; i++){
            petsChoices.push({ 'id': i });
        }
    };

    var initInfants = function(infants){
        maxInfants = infants;

        infantsChoices = [];
        for(var i=0; i<=maxInfants; i++){
            infantsChoices.push({ 'id': i });
        }
    };
    return {
        setEstate: setEstate,
        getEstate: getEstate,
        setSleeps: setSleeps,
        getSleeps: getSleeps,
        setSleepsChildren: setSleepsChildren,
        getSleepsChildren: getSleepsChildren,
        getPets: getPets,
        setPets: setPets,
        getInfants: getInfants,
        setInfants: setInfants,
        setFromDate: setFromDate,
        getFromDate: getFromDate,
        setToDate: setToDate,
        getToDate: getToDate,
        setEstateShortName: setEstateShortName,
        getEstateShortName: getEstateShortName,

        getSleepsTotal: function(){
            return getSleeps().id;
        },
        getSleepsChildrenTotal: function(){
            return getSleepsChildren().id;
        },
        getPetsTotal: function(){
            return getPets().id;
        },
        getInfantsTotal: function(){
            return getInfants().id;
        },

        getEstateChoices: getEstateChoices,
        getSleepsChoices: getSleepsChoices,
        getSleepsChildrenChoices: getSleepsChildrenChoices,
        getPetsChoices: getPetsChoices,
        getInfantsChoices: getInfantsChoices,
        initEstates: initEstates,
        initSleeps: initSleeps,
        initSleepsChildren: initSleepsChildren,
        initPets: initPets,
        initInfants: initInfants
    }
});

reservationRequoteApp.controller("reservationRequoteAppCtrl", ['$scope', '$http', 'filterFactory', 'priceFactory', 'apiService', 'discountPriceFactory', '$window', function($scope, $http, filterFactory, priceFactory, apiService, discountPriceFactory, $window){
    $http.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';
    $http.defaults.headers.post["Content-Type"] = "application/x-www-form-urlencoded";

    $scope.initPrice = function(json){
        var price = angular.fromJson(json);

        if(typeof price.basePrice === typeof undefined){}else{
            priceFactory.setBasePrice(price.basePrice);
        }
        if(typeof price.additionalSleepsPrice === typeof undefined){}else{
            priceFactory.setAdditionalSleepsPrice(price.additionalSleepsPrice);
        }
        if(typeof price.adminCharge === typeof undefined){}else{
            priceFactory.setAdminCharge(price.adminCharge);
        }
        if(typeof price.creditCardCharge === typeof undefined){}else{
            priceFactory.setCreditCardCharge(price.creditCardCharge);
        }
        if(typeof price.bookingProtectCharge === typeof undefined){}else{
            priceFactory.setBookingProtectCharge(price.bookingProtectCharge);
        }
        if(typeof price.charityCharge === typeof undefined){}else{
            priceFactory.setCharityCharge(price.charityCharge);
        }
        if(typeof price.petsPrice === typeof undefined){}else{
            priceFactory.setPetsPrice(price.petsPrice);
        }
       
        $scope.basePrice = priceFactory.getBasePrice();
        $scope.additionalSleepsPrice = priceFactory.getAdditionalSleepsPrice();
        $scope.adminCharge = priceFactory.getAdminCharge();
        $scope.creditCardCharge = priceFactory.getCreditCardCharge();
        $scope.bookingProtectCharge = priceFactory.getBookingProtectCharge();
        $scope.charityCharge = priceFactory.getCharityCharge();
        $scope.petsPrice = priceFactory.getPetsPrice();
    };

    $scope.initDiscountPrice = function(json){
        var discountPrice = angular.fromJson(json);

        if(typeof discountPrice.discountCodePrice === typeof undefined){}else{
            discountPriceFactory.setDiscountCodePrice(discountPrice.discountCodePrice);
        }

        $scope.discountCodePrice = discountPriceFactory.getDiscountCodePrice();
    };

    $scope.initFilterEstate = function(reservationEstateShortName){
        filterFactory.setEstateShortName(reservationEstateShortName);
        apiService.getActiveEstates(function(allEstates){
            if(allEstates){
                filterFactory.initEstates(allEstates);

                var estateChoices = filterFactory.getEstateChoices();
                $scope.estateChoices = estateChoices;
                angular.forEach(estateChoices, function(choice){
                    if(choice.shortName == reservationEstateShortName){
                        filterFactory.setEstate(choice);
                    }
                });

                $scope.estate = filterFactory.getEstate();
            }
        });
    };

    $scope.initFilterSleeps = function(maxSleeps, reservationSleeps){
        filterFactory.initSleeps(maxSleeps);

        var sleepsChoices = filterFactory.getSleepsChoices();
        $scope.sleepsChoices = sleepsChoices;
        angular.forEach(sleepsChoices, function(choice){
            if(choice.id == reservationSleeps){
                filterFactory.setSleeps(choice);
            }
        });

        $scope.sleeps = filterFactory.getSleeps();
    };

    $scope.initFilterSleepsChildren = function(maxSleepsChildren, reservationSleepsChildren){
        filterFactory.initSleepsChildren(maxSleepsChildren);

        var sleepsChildrenChoices = filterFactory.getSleepsChildrenChoices();
        $scope.sleepsChildrenChoices = sleepsChildrenChoices;
        angular.forEach(sleepsChildrenChoices, function(choice){
            if(choice.id == reservationSleepsChildren){
                filterFactory.setSleepsChildren(choice);
            }
        });

        $scope.sleepsChildren = filterFactory.getSleepsChildren();
    };

    $scope.initFilterPets = function(maxPets, reservationPets){
        filterFactory.initPets(maxPets);

        var petsChoices = filterFactory.getPetsChoices();
        $scope.petsChoices = petsChoices;
        angular.forEach(petsChoices, function(choice){
            if(choice.id == reservationPets){
                filterFactory.setPets(choice);
            }
        });

        $scope.pets = filterFactory.getPets();
    };

    $scope.initFilterInfants = function(maxInfants, reservationInfants){
        filterFactory.initInfants(maxInfants);

        var infantsChoices = filterFactory.getInfantsChoices();
        $scope.infantsChoices = infantsChoices;
        angular.forEach(infantsChoices, function(choice){
            if(choice.id == reservationInfants){
                filterFactory.setInfants(choice);
            }
        });

        $scope.infants = filterFactory.getInfants();
    };

    $scope.initFilterFromDate = function(fromDate){
        filterFactory.setFromDate(fromDate);

        $scope.fromDate = filterFactory.getFromDate();
    };

    $scope.initFilterToDate = function(toDate){
        filterFactory.setToDate(toDate);

        $scope.toDate = filterFactory.getToDate();
    };

    $scope.estateChoices = [];
    $scope.sleepsChoices = [];
    $scope.sleepsChildrenChoices = [];
    $scope.petsChoices = [];
    $scope.infantsChoices = [];

    $scope.estate = false;
    $scope.sleeps = false;
    $scope.sleepsChildren = false;
    $scope.pets = false;
    $scope.infants = false;
    $scope.fromDate = null;
    $scope.toDate = null;



    $scope.basePrice = 0;
    $scope.additionalSleepsPrice = 0;
    $scope.adminCharge = 0;
    $scope.creditCardCharge = 0;
    $scope.bookingProtectCharge = 0;
    $scope.charityCharge = 0;
    $scope.petsPrice = 0;

    $scope.setBasePrice = function(price){
        priceFactory.setBasePrice(price);
        $scope.allowUpdate = true;
    };

    $scope.setAdditionalSleepsPrice = function(price){
        priceFactory.setAdditionalSleepsPrice(price);
        $scope.allowUpdate = true;
    };

    $scope.setAdminCharge = function(price){
        priceFactory.setAdminCharge(price);
        $scope.allowUpdate = true;
    };

    $scope.setCreditCardCharge = function(price){
        priceFactory.setCreditCardCharge(price);
        $scope.allowUpdate = true;
    };

    $scope.setBookingProtectCharge = function(price){
        priceFactory.setBookingProtectCharge(price);
        $scope.allowUpdate = true;
    };

    $scope.setCharityCharge = function(price){
        priceFactory.setCharityCharge(price);
        $scope.allowUpdate = true;
    };

    $scope.setPetsPrice = function(price){
        priceFactory.setPetsPrice(price);
        $scope.allowUpdate = true;
    };




    $scope.discountCodePrice = 0;
    $scope.setDiscountCodePrice = function(price){
        discountPriceFactory.setDiscountCodePrice(price);
        $scope.allowUpdate = true;
    };


    $scope.totalPrice = function(){
        return parseFloat(parseFloat(priceFactory.getPrice().total()) - parseFloat(discountPriceFactory.getPrice().total())).toFixed(2);
    };


    $scope.setEstate = function(estate){
        filterFactory.setEstate(estate.shortName);

        filterFactory.initSleeps(estate.maxSleeps);
        $scope.sleepsChoices = filterFactory.getSleepsChoices();

        filterFactory.initSleepsChildren(estate.maxSleeps);
        $scope.sleepsChildrenChoices = filterFactory.getSleepsChildrenChoices();

        filterFactory.initPets(estate.maxPets);
        $scope.petsChoices = filterFactory.getPetsChoices();

        filterFactory.initInfants(estate.maxInfants);
        $scope.infantsChoices = filterFactory.getInfantsChoices();

        formApply();
    };

    $scope.setSleeps = function(sleeps){
        filterFactory.setSleeps(sleeps);
        if(filterFactory.getSleepsChildrenTotal() <= filterFactory.getSleepsTotal()){
            $scope.initFilterSleepsChildren(filterFactory.getSleepsTotal(), filterFactory.getSleepsChildrenTotal());
        }else{
            $scope.initFilterSleepsChildren(filterFactory.getSleepsTotal(), 0);
        }
        formApply();
    };

    $scope.setSleepsChildren = function(sleepsChildren){
        filterFactory.setSleepsChildren(sleepsChildren);
        formApply();
    };

    $scope.setPets = function(pets){
        filterFactory.setPets(pets);
        formApply();
    };

    $scope.setInfants = function(infants){
        filterFactory.setInfants(infants);
        formApply();
    };

    $scope.setFromDate = function(fromDate){
        filterFactory.setFromDate(fromDate);
        formApply();
    };

    $scope.setToDate = function(toDate){
        filterFactory.setToDate(toDate);
        formApply();
    };

    var formApply = function(){
        $scope.estate = filterFactory.getEstate();
        $scope.estateShortName = filterFactory.getEstateShortName();
        $scope.sleeps = filterFactory.getSleeps();
        $scope.pets = filterFactory.getPets();
        $scope.infants = filterFactory.getInfants();
        $scope.fromDate = filterFactory.getFromDate();
        $scope.toDate = filterFactory.getToDate();

        var httpData = {
            reservationId: $scope.reservationId,
            estateShortName: filterFactory.getEstateShortName(),
            sleeps: filterFactory.getSleepsTotal(),
            pets: filterFactory.getPetsTotal(),
            infants: filterFactory.getInfantsTotal(),
            fromDate: filterFactory.getFromDate(),
            toDate: filterFactory.getToDate()
        };
        $scope.showRequoteLoader = true;
        apiService.getPrice(httpData, function(result){
            $scope.showRequoteLoader = false;
            if(result){
                if(typeof result.basePrice === typeof undefined){}else{
                    priceFactory.setBasePrice(result.basePrice);
                }
                if(typeof result.additionalSleepsPrice === typeof undefined){}else{
                    priceFactory.setAdditionalSleepsPrice(result.additionalSleepsPrice);
                }
                if(typeof result.adminCharge === typeof undefined){}else{
                    priceFactory.setAdminCharge(result.adminCharge);
                }
                if(typeof result.creditCardCharge === typeof undefined){}else{
                    priceFactory.setCreditCardCharge(result.creditCardCharge);
                }
                if(typeof result.bookingProtectCharge === typeof undefined){}else{
                    priceFactory.setBookingProtectCharge(result.bookingProtectCharge);
                }
                if(typeof result.charityCharge === typeof undefined){}else{
                    priceFactory.setCharityCharge(result.charityCharge);
                }
                if(typeof result.petsPrice === typeof undefined){}else{
                    priceFactory.setPetsPrice(result.petsPrice);
                }
                $scope.basePrice = priceFactory.getBasePrice();
                $scope.additionalSleepsPrice = priceFactory.getAdditionalSleepsPrice();
                $scope.adminCharge = priceFactory.getAdminCharge();
                $scope.creditCardCharge = priceFactory.getCreditCardCharge();
                $scope.bookingProtectCharge = priceFactory.getBookingProtectCharge();
                $scope.charityCharge = priceFactory.getCharityCharge();
                $scope.petsPrice = priceFactory.getPetsPrice();

                $scope.allowUpdate = true;
                createResponseMessage('Price calculated. Confirm to requote.');
            }else{
                $scope.allowUpdate = false;
                createResponseMessage(false, 'Price can not been calculated for selected parameters. Please change parameters and try again.');
            }
         });
    };

    $scope.responseMessageSuccess = false;
    $scope.responseMessageError = false;
    var createResponseMessage = function(success, error){
        $scope.responseMessageSuccess = false;
        $scope.responseMessageError = false;

        if(!(typeof success === typeof undefined) && success){
            $scope.responseMessageSuccess = success;
        }
        if(!(typeof error === typeof undefined) && error){
            $scope.responseMessageError = error;
        }
    };

    $scope.requoteSubmit = function(){

        var httpData = {
            reservationId: $scope.reservationId,
            estateShortName: filterFactory.getEstateShortName(),
            fromDate: filterFactory.getFromDate(),
            toDate: filterFactory.getToDate(),
            sleeps: filterFactory.getSleepsTotal(),
            sleepsChildren: filterFactory.getSleepsChildrenTotal(),
            pets: filterFactory.getPetsTotal(),
            infants: filterFactory.getInfantsTotal(),
            price: priceFactory.getPrice(),
            discountPrice: discountPriceFactory.getPrice(),
            sendHistoryEmail: $scope.requoteSendHistoryEmail.value,
        };

        $scope.showRequoteLoader = true;
        apiService.requoteSubmit(httpData, function(result){
            $scope.showRequoteLoader = false;
            if(result){
                createResponseMessage('Reservation requoted');
            }
        });
    };

    $scope.requoteSendHistoryEmailChoices = [
        {'value': 0, 'label': 'Do NOT send customer e-mail'},
        {'value': 1, 'label': 'Send customer e-mail'}
    ];
    $scope.requoteSendHistoryEmail = $scope.requoteSendHistoryEmailChoices[0];
    $scope.setRequoteSendHistoryEmail = function(requoteSendHistoryEmail){
        $scope.requoteSendHistoryEmail = requoteSendHistoryEmail;
    };

    $scope.reservationId = '';
    $scope.setReservationId = function(reservationId){
        $scope.reservationId = reservationId;
    };

    $scope.allowUpdate = false;
    $scope.showRequoteLoader = false;

}]);

reservationRequoteApp.directive('datepicker', function() {
    return {
        restrict: 'A',
        require : 'ngModel',
        link : function (scope, element, attrs, ngModelCtrl) {
            $(function(){
                element.datepicker({
                    dateFormat: 'yy-mm-dd',
                    minDate: new Date(),
                    onSelect: function (date) {
                        ngModelCtrl.$setViewValue(date);
                        scope.$apply();
                    }
                });
            });
        }
    }
});

reservationRequoteApp.directive("datetimepicker", function(){
    return {
        restrict: "A",
        require: "ngModel",
        link: function (scope, element, attrs, modelCtrl) {
            element.datetimepicker();
            modelCtrl.$formatters.push(function(input){
                if (!input) return;
                return moment(input).format("YYYY-MM-DD");
            });
            modelCtrl.$parsers.push(function(input){
                if (!input) return;
                return moment(input).format("YYYY-MM-DD");
            });
        }
    }});

angular.bootstrap(document.getElementById("reservationRequoteAppHandler"),["reservationRequoteApp"]);