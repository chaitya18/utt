<?php

namespace UTT\ReservationBundle\Service;

use Doctrine\ORM\EntityManager;
use UTT\IndexBundle\Service\ConfigurationService;
use UTT\ReservationBundle\Entity\CurrencyRate;

class CurrencyService {
    protected $_em;
    protected $configurationService;

    public function __construct(EntityManager $em, ConfigurationService $configurationService){
        $this->_em = $em;
        $this->configurationService = $configurationService;
    }

    public function createCurrencyRate($currencyCode, \Datetime $date, $value){
        $currencyRate = new CurrencyRate();
        $currencyRate->setCurrencyCode($currencyCode);
        $currencyRate->setRateDate($date);
        $currencyRate->setValue($value);

        try{
            $this->_em->persist($currencyRate);
            $this->_em->flush();

            return $currencyRate;
        }catch(\Exception $e){
            return false;
        }
    }

    public function getCurrencyRate($currencyCode, \Datetime $date){
        /** @var CurrencyRate $currencyRate */
        $currencyRate = $this->_em->getRepository('UTTReservationBundle:CurrencyRate')->findOneBy(array(
            'currencyCode' => $currencyCode,
            'rateDate' => $date
        ));
        if($currencyRate instanceof CurrencyRate){
            return $currencyRate;
        }

        return false;
    }

    public function getCurrentCurrencyRate($currencyCode){
        return $this->getCurrencyRate($currencyCode, new \DateTime('now'));
    }

    public function formatPrice($price, $currencyCode){
        /** @var CurrencyRate $currencyRate */
        $currencyRate = $this->getCurrentCurrencyRate($currencyCode);
        if($currencyRate instanceof CurrencyRate){
            $price = $price * $currencyRate->getValue();
            return $price;
        }

        return -1;
    }

}