<?php

namespace UTT\ReservationBundle\Service;

use Doctrine\ORM\EntityManager;
use UTT\EstateBundle\Entity\Estate;
use UTT\ReservationBundle\Entity\Pricing;
use UTT\ReservationBundle\Entity\PricingRepository;
use UTT\IndexBundle\Service\ConfigurationService;
use UTT\IndexBundle\Entity\Configuration;
use UTT\ReservationBundle\Entity\Offer;
use UTT\ReservationBundle\Entity\OfferRepository;
use UTT\ReservationBundle\Entity\PricingCategory;
use UTT\ReservationBundle\Entity\PricingSeasonRepository;
use UTT\ReservationBundle\Entity\PricingSeason;
use UTT\ReservationBundle\Entity\PricingCategoryPriceFlexible;
use UTT\ReservationBundle\Entity\PricingCategoryPriceStandard;
use UTT\ReservationBundle\Factory\PricingCategoryFactory;
use UTT\ReservationBundle\Service\BookingProtectService;

class PricingService {
    /** @var  Configuration */
    private $configuration;

    protected $_em;
    protected $configurationService;
    protected $pricingCategoryFactory;
    protected $bookingProtectService;

    public function __construct(EntityManager $em, ConfigurationService $configurationService, PricingCategoryFactory $pricingCategoryFactory, BookingProtectService $bookingProtectService){
        $this->_em = $em;
        $this->configurationService = $configurationService;
        $this->pricingCategoryFactory = $pricingCategoryFactory;
        $this->bookingProtectService = $bookingProtectService;

        $this->configuration = $this->configurationService->get();
    }

    private function getAllDayDates2($year, $dayOfWeek='monday', $modifyAdditional = false, $modifyAdditionalOnly = false) {
        $start = new \DateTime( sprintf('%04d-01-01', $year) );//new \DateTime('now');
        $start->modify($dayOfWeek);
        $end = clone $start;
        $end->modify('+3 years');
        //$end = new \DateTime( sprintf('%04d-12-31', $year) );
        //$end->modify('+1 day');
        $interval = new \DateInterval('P1W');
        $period   = new \DatePeriod($start, $interval, $end);

        $dates = array();
        foreach ($period as $date) {
            $date1 = clone $date;
            $date2 = clone $date;

            if(!$modifyAdditionalOnly){
                $dates[] = (object)array(
                    'dateFrom' => new \DateTime($date1->format('Y-m-d')),
                    'dateTo' => new \DateTime($date1->modify('+2 week')->format('Y-m-d')),
                );
            }

            if($modifyAdditional){
                $dates[] = (object)array(
                    'dateFrom' => new \DateTime($date2->format('Y-m-d')),
                    'dateTo' => new \DateTime($date2->modify('+1 week')->modify($modifyAdditional)->format('Y-m-d')),
                );
            }
        }
        return $dates;
    }

    private function getAllDayDates($year, $dayOfWeek='monday', $modifyAdditional = false, $modifyAdditionalOnly = false) {
        $start = new \DateTime( sprintf('%04d-01-01', $year) );//new \DateTime('now');
        $start->modify($dayOfWeek);
        $end = clone $start;
        $end->modify('+3 years');
        //$end = new \DateTime( sprintf('%04d-12-31', $year) );
        //$end->modify('+1 day');
        $interval = new \DateInterval('P1W');
        $period   = new \DatePeriod($start, $interval, $end);

        $dates = array();
        foreach ($period as $date) {
            $date1 = clone $date;
            $date2 = clone $date;

            if(!$modifyAdditionalOnly){
                $dates[] = (object)array(
                    'dateFrom' => new \DateTime($date1->format('Y-m-d')),
                    'dateTo' => new \DateTime($date1->modify('+1 week')->format('Y-m-d')),
                );
            }

            if($modifyAdditional){
                $dates[] = (object)array(
                    'dateFrom' => new \DateTime($date2->format('Y-m-d')),
                    'dateTo' => new \DateTime($date2->modify($modifyAdditional)->format('Y-m-d')),
                );
            }
        }
        return $dates;
    }

    public function clearPricingForEstate(Estate $estate){
        /** @var PricingRepository $pricingRepository */
        $pricingRepository = $this->_em->getRepository('UTTReservationBundle:Pricing');
        $pricings = $pricingRepository->findBy(array(
            'estate' => $estate->getId()
        ));
        if($pricings){
            /** @var Pricing $pricing */
            foreach($pricings as $pricing){
                $this->_em->remove($pricing);
            }

            $this->_em->flush();
        }
    }

    private function generatePricingForEstate(Estate $estate, $type, $pricingDates, PricingCategory $pricingCategory){
        /** @var PricingSeasonRepository $pricingSeasonRepository */
        $pricingSeasonRepository = $this->_em->getRepository('UTTReservationBundle:PricingSeason');
        $pricingSeasons = $pricingSeasonRepository->findAll();

        // add missing pricing dates to estate
        if($pricingDates && count($pricingDates) > 0){
            foreach($pricingDates as $pricingDate){
                $pricingSeason = false;

                # pobierz sezon w którym jest data
                /** @var PricingSeason $item */
                foreach($pricingSeasons as $item){
                    if($pricingDate->dateFrom >= $item->getFromDate() && $pricingDate->dateFrom <= $item->getToDate()){
                        $pricingSeason = $item;
                        break;
                    }
                }

                if($pricingSeason instanceof PricingSeason){
                    $pricing = new Pricing();
                    $pricing->setType($type);
                    $pricing->setEstate($estate);

                    if($type == Pricing::TYPE_STANDARD_M_F){
                        $priceStandard = $this->pricingCategoryFactory->getPriceStandardForYearAndType($pricingCategory, $pricingSeason->getType(), $pricingDate->dateFrom);
                        if($priceStandard instanceof PricingCategoryPriceStandard){
                            $pricing->setPrice($this->pricingCategoryFactory->getPriceValueForPriceStandard($priceStandard, $pricingDate->dateFrom, $pricingDate->dateTo));
                        }
                    }elseif($type == Pricing::TYPE_FLEXIBLE_BOOKING){
                        $priceFlexible = $this->pricingCategoryFactory->getPriceFlexibleForYearAndType($pricingCategory, $pricingSeason->getType(), $pricingDate->dateFrom);
                        if($priceFlexible instanceof PricingCategoryPriceFlexible){
                            $pricing->setPrice($this->pricingCategoryFactory->getPriceValueForPriceFlexible($priceFlexible, $pricingDate->dateFrom, $pricingDate->dateTo));
                            $pricing->setInitialPrice($priceFlexible->getPriceStandingCharge());
                        }
                    }

                    $pricing->setFromDate($pricingDate->dateFrom);
                    $pricing->setToDate($pricingDate->dateTo);

                    $this->_em->persist($pricing);
                }
            }

            $this->_em->flush();
        }
    }

    public function generateStandardPricingForEstate(Estate $estate, PricingCategory $pricingCategory){
        $nowDate = new \DateTime('now');
        $datesStartsAtMonday = $this->getAllDayDates($nowDate->format('Y'), 'monday', 'friday');
        $datesStartsAtMonday2 = $this->getAllDayDates2($nowDate->format('Y'), 'monday', 'friday');
        $datesStartsAtFriday = $this->getAllDayDates($nowDate->format('Y'), 'friday', 'monday');
        $datesStartsAtFriday2 = $this->getAllDayDates2($nowDate->format('Y'), 'friday', 'monday');
        $pricingDates = array_merge($datesStartsAtMonday, $datesStartsAtFriday, $datesStartsAtMonday2, $datesStartsAtFriday2);

        $this->clearPricingForEstate($estate);
        $this->generatePricingForEstate($estate, Pricing::TYPE_STANDARD_M_F, $pricingDates, $pricingCategory);
    }

    public function generateFlexiblePricingForEstate(Estate $estate, PricingCategory $pricingCategory){
        $nowDate = new \DateTime('now');
        $datesStartsAtSunday = $this->getAllDayDates($nowDate->format('Y'), 'sunday', 'thursday', true);
        $datesStartsAtFriday = $this->getAllDayDates($nowDate->format('Y'), 'friday', 'saturday', true);
        $pricingDates = array_merge($datesStartsAtSunday, $datesStartsAtFriday);

        $this->clearPricingForEstate($estate);
        $this->generatePricingForEstate($estate, Pricing::TYPE_FLEXIBLE_BOOKING, $pricingDates, $pricingCategory);
    }

    public function calculateAdditionalSleepsPrice(Estate $estate, $basePrice, $sleeps){
        if((int) $sleeps >= 1 && (int) $sleeps <= $estate->getSleeps()){
            if($sleeps <= $estate->getMinimumGuests()){
                return 0;
            }else{
                /** @var Configuration $configuration */
                $configuration = $this->configurationService->get();
                if($configuration){

//                    echo $basePrice.PHP_EOL;
//                    echo $sleeps.PHP_EOL;
//                    echo $estate->getMinimumGuests().PHP_EOL;
//                    echo $configuration->getAdditionalSleepsPercentIncrease().PHP_EOL;
//                    echo ($sleeps - $estate->getMinimumGuests()).PHP_EOL;
//                    echo (($sleeps - $estate->getMinimumGuests()) * $configuration->getAdditionalSleepsPercentIncrease() / 100).PHP_EOL;
//                    echo ($basePrice * (($sleeps - $estate->getMinimumGuests()) * $configuration->getAdditionalSleepsPercentIncrease() / 100)).PHP_EOL;
//                    exit;


                    $price = $basePrice * (($sleeps - $estate->getMinimumGuests()) * $configuration->getAdditionalSleepsPercentIncrease() / 100);
                    return round($price, 2);
                }
            }
        }

        return -1;
    }

    public function calculateAdditionalSleepsPriceByEstateArray(array $estate, $basePrice, $sleeps){
        if(isset($estate['sleeps']) && isset($estate['minimumGuests'])){
            if((int) $sleeps >= 1 && (int) $sleeps <= (int) $estate['sleeps']){
                if($sleeps <= $estate['minimumGuests']){
                    return 0;
                }else{
                    if($this->configuration){
                        $price = $basePrice * (($sleeps - $estate['minimumGuests']) * $this->configuration->getAdditionalSleepsPercentIncrease() / 100);
                        return round($price, 2);
                    }
                }
            }
        }

        return -1;
    }

    public function getAdditionalSleepsPercentIncrease(){
        if($this->configuration instanceof Configuration){
            return round($this->configuration->getAdditionalSleepsPercentIncrease(), 2);
        }

        return -1;
    }

    public function calculateAdminCharge($basePrice, $additionalSleepsPrice, $petsPrice)
    {
        $adminCharge = $this->getAdminCharge();
        if($adminCharge != -1) {
            $adminCharge /= 100;
            return ($basePrice + $additionalSleepsPrice + $petsPrice) * $adminCharge;
        }
        return -1;
    }

    public function getAdminCharge(){
        /** @var Configuration $configuration */
        $configuration = $this->configurationService->get();
        if($configuration instanceof Configuration){
            return round($configuration->getAdminFee(), 2);
        }

        return -1;
    }

    public function calculateCreditCardCharge($total){
        /** @var Configuration $configuration */
        $configuration = $this->configurationService->get();
        if($configuration instanceof Configuration){
            $price = $total * $configuration->getCreditCardSurcharge() / 100;
            $subPrice = $price * $configuration->getCreditCardSurcharge() / 100;
            return round($price + $subPrice, 2);
        }

        return -1;
    }

    public function calculateBookingProtectCharge($total, \DateTime $fromDate){
        $price = $this->bookingProtectService->getBookingProtectPrice($total, $fromDate);
        if($price != -1) return $price;

        return 0;
    }

    public function calculateInfantsPrice(Estate $estate, $infants){
        if($infants <= $estate->getMaxInfantsTotal()){
            return 0;
        }
        return -1;
    }

    public function calculatePetsPrice(Estate $estate, $pets){
        /** @var Configuration $configuration */
        $configuration = $this->configurationService->get();

        if($pets <= $estate->getMaxPetsTotal()){
            if($pets == 0){
                return 0;
            }elseif($pets == 1){
                if($configuration){
                    if($estate->getPets() == Estate::PETS_WELCOME_MAX_1_DOG || $estate->getPets() == Estate::PETS_WELCOME_MAX_2_DOGS || $estate->getPets() == Estate::PETS_WELCOME_MAX_3_DOGS || $estate->getPets() == Estate::PETS_WELCOME_MORE_THAN_3_CONTRACT_US_UPON_BOOKING){
                        return (float) $configuration->getPetPriceFirst();
                    }
                }
            }elseif($pets == 2){
                if($configuration){
                    if($estate->getPets() == Estate::PETS_WELCOME_MAX_2_DOGS || $estate->getPets() == Estate::PETS_WELCOME_MAX_3_DOGS || $estate->getPets() == Estate::PETS_WELCOME_MORE_THAN_3_CONTRACT_US_UPON_BOOKING){
                        return (float) $configuration->getPetPriceFirst() + (float) $configuration->getPetPriceAdditional();
                    }
                }
            }elseif($pets == 3){
                if($configuration){
                    if($estate->getPets() == Estate::PETS_WELCOME_MAX_3_DOGS || $estate->getPets() == Estate::PETS_WELCOME_MORE_THAN_3_CONTRACT_US_UPON_BOOKING){
                        return (float) $configuration->getPetPriceFirst() + (float) $configuration->getPetPriceAdditional() + (float) $configuration->getPetPriceAdditional();
                    }
                }
            }elseif($pets > 3){
                if($configuration){
                    if($estate->getPets() == Estate::PETS_WELCOME_MORE_THAN_3_CONTRACT_US_UPON_BOOKING){
                        return (float) $configuration->getPetPriceFirst() + ($pets - 1) * ((float) $configuration->getPetPriceAdditional());
                    }
                }
            }
        }

        return -1;
    }

    public function getOfferPrice(Estate $estate, \Datetime $fromDate, \Datetime $toDate, $forSearchEngine = false){
        /** @var OfferRepository $offerRepository */
        $offerRepository = $this->_em->getRepository('UTTReservationBundle:Offer');

        /** @var Offer $offer */
        $offer = $offerRepository->findOneByEstateAndDates($estate, $fromDate, $toDate, $forSearchEngine);
        if($offer){
            return $offer->getValue();
        }

        return -1;
    }

    public function findLowestPrice(Estate $estate){
        $yearDate = new \DateTime('now');
        $yearDate->setDate($yearDate->format('Y'), $yearDate->format('m'), ($yearDate->format('d') == 1 ? 1 : $yearDate->format('d') -1));
        $yearsInFuture = 2;
        $pricingList = $this->getPricingListForEstateForYear($estate, $yearDate, $yearsInFuture);
        if($pricingList){
            /** @var OfferRepository $offerRepository */
            $offerRepository = $this->_em->getRepository('UTTReservationBundle:Offer');
            /** @var Offer $lowestPriceOffer */
            $lowestPriceOffer = $offerRepository->findOneLowestPriceWeekOfferForEstate($estate);

            if($estate->getType() == Estate::TYPE_FLEXIBLE_BOOKING){
                $start = new \DateTime( sprintf('%04d-%d-01', $yearDate->format('Y'), $yearDate->format('m')) );
                $end = new \DateTime( sprintf('%04d-12-31', (int)$yearDate->format('Y') + ($yearsInFuture-1)) );
                $interval = new \DateInterval('P1D');
                $globalPeriod   = new \DatePeriod($start, $interval, $end);

                $periods = array();
                foreach ($globalPeriod as $globalPeriodDate) {
                    $periodStart = clone $globalPeriodDate;
                    $periodEnd = clone $globalPeriodDate;
                    $periodEnd->modify('+7 days');

                    if($periodStart <= new \DateTime('now')) continue;
                    if($periodEnd >= $end) continue;

                    $periodInterval = new \DateInterval('P1D');
                    $period = new \DatePeriod($periodStart, $periodInterval, $periodEnd);

                    $periods[] = $period;
                }

                $lowestPrice = null;
                /** @var \DatePeriod $period */
                foreach($periods as $period){
                    $basePrice = $this->flexiblePeriodBasePrice($period, $pricingList);
                    //if($basePrice != -1){
                    if($basePrice > 0){
                        if(is_null($lowestPrice)){ $lowestPrice = $basePrice; }

                        if($basePrice < $lowestPrice){
                            $lowestPrice = $basePrice;
                        }
                    }
                }

                if(!is_null(($lowestPrice))){
                    if($lowestPriceOffer instanceof Offer){
                        if($lowestPriceOffer->getValue() < $lowestPrice){
                            if($lowestPriceOffer->getValue() > 0) $lowestPrice = $lowestPriceOffer->getValue();
                        }
                    }

                    $adminCharge = $this->getAdminCharge();
                    if($adminCharge != -1){
                        return $lowestPrice + $adminCharge;
                    }else{
                        return $lowestPrice;
                    }
                }
            }elseif($estate->getType() == Estate::TYPE_STANDARD_M_F){
                $lowestPrice = $pricingList[0]->getPrice();
                /** @var Pricing $pricing */
                foreach($pricingList as $pricing){
                    $dateDiff = $pricing->getFromDate()->diff($pricing->getToDate());
                    if($pricing->getPrice() < $lowestPrice && $dateDiff->days == 7){
                        if($pricing->getPrice() > 0) $lowestPrice = $pricing->getPrice();
                    }
                }

                if($lowestPriceOffer instanceof Offer){
                    if($lowestPriceOffer->getValue() < $lowestPrice){
                        if($lowestPriceOffer->getValue() > 0) $lowestPrice = $lowestPriceOffer->getValue();
                    }
                }

                $adminCharge = $this->getAdminCharge();
                if($adminCharge != -1){
                    return $lowestPrice + $adminCharge;
                }else{
                    return $lowestPrice;
                }
            }
        }

        return -1;
    }

    public function findHighestPrice(Estate $estate){
        $yearDate = new \DateTime('now');
        $yearDate->setDate($yearDate->format('Y'), $yearDate->format('m'), ($yearDate->format('d') == 1 ? 1 : $yearDate->format('d') -1));
        $yearsInFuture = 2;
        $pricingList = $this->getPricingListForEstateForYear($estate, $yearDate, $yearsInFuture);
        if($pricingList){
            /** @var OfferRepository $offerRepository */
            $offerRepository = $this->_em->getRepository('UTTReservationBundle:Offer');
            /** @var Offer $highestPriceOffer */
            $highestPriceOffer = $offerRepository->findOneHighestPriceWeekOfferForEstate($estate);

            if($estate->getType() == Estate::TYPE_FLEXIBLE_BOOKING){
                $start = new \DateTime( sprintf('%04d-%d-01', $yearDate->format('Y'), $yearDate->format('m')) );
                $end = new \DateTime( sprintf('%04d-12-31', (int)$yearDate->format('Y') + ($yearsInFuture-1)) );
                $end->modify('+1 day');
                $interval = new \DateInterval('P1D');
                $globalPeriod   = new \DatePeriod($start, $interval, $end);

                $periods = array();
                foreach ($globalPeriod as $globalPeriodDate) {
                    $periodStart = clone $globalPeriodDate;
                    $periodEnd = clone $globalPeriodDate;
                    $periodEnd->modify('+7 days');

                    if($periodStart <= new \DateTime('now')) continue;
                    if($periodEnd >= $end) continue;

                    $periodInterval = new \DateInterval('P1D');
                    $period = new \DatePeriod($periodStart, $periodInterval, $periodEnd);

                    $periods[] = $period;
                }

                $highestPrice = null;
                foreach($periods as $period){
                    $basePrice = $this->flexiblePeriodBasePrice($period, $pricingList);
                    if($basePrice != -1){
                        if(is_null($highestPrice)){ $highestPrice = $basePrice; }

                        if($basePrice > $highestPrice){
                            $highestPrice = $basePrice;
                        }
                    }
                }

                if(!is_null(($highestPrice))){
                    if($highestPriceOffer instanceof Offer){
                        if($highestPriceOffer->getValue() > $highestPrice){
                            $highestPrice = $highestPriceOffer->getValue();
                        }
                    }

                    $adminCharge = $this->getAdminCharge();
                    if($adminCharge != -1){
                        return $highestPrice + $adminCharge;
                    }else{
                        return $highestPrice;
                    }
                }
            }elseif($estate->getType() == Estate::TYPE_STANDARD_M_F){
                $highestPrice = $pricingList[0]->getPrice();
                /** @var Pricing $pricing */
                foreach($pricingList as $pricing){
                    $dateDiff = $pricing->getFromDate()->diff($pricing->getToDate());
                    if($pricing->getPrice() > $highestPrice && $dateDiff->days == 7){
                        $highestPrice = $pricing->getPrice();
                    }
                }

                if($highestPriceOffer instanceof Offer){
                    if($highestPriceOffer->getValue() > $highestPrice){
                        $highestPrice = $highestPriceOffer->getValue();
                    }
                }


                $adminCharge = $this->getAdminCharge();
                if($adminCharge != -1){
                    return $highestPrice + $adminCharge;
                }else{
                    return $highestPrice;
                }
            }
        }

        return -1;
    }

    public function findLowestPriceByPricingCategory(Estate $estate)
    {
        $adminCharge = $this->getAdminCharge();
        if($adminCharge == -1) {
            $adminCharge = 0;
        }

        if($estate->getType() == Estate::TYPE_STANDARD_M_F){
            $priceStandardRepository = $this->_em->getRepository('UTTReservationBundle:PricingCategoryPriceStandard');
            $lowestPrice = $priceStandardRepository->getLowestPriceForCategoryStartingAtYear($estate->getPricingCategory(), date('Y'));
        } else if($estate->getType() == Estate::TYPE_FLEXIBLE_BOOKING){
            $priceFlexibeRepository = $this->_em->getRepository('UTTReservationBundle:PricingCategoryPriceFlexible');
            $lowestPrice = $priceFlexibeRepository->getLowestPriceForCategoryStartingAtYear($estate->getPricingCategory(), date('Y'));
        }

        if(!empty($lowestPrice)) {
            return $lowestPrice * (1 + ($adminCharge/100));
        }

        return -1;
    }

    public function flexiblePeriodBasePrice(\DatePeriod $period, $flexiblePricing){
        $basePrice = -1;
        foreach($period as $date){
            /** @var Pricing $pricing */
            foreach($flexiblePricing as $pricing){
                if($date >= $pricing->getFromDate() && $date <= $pricing->getToDate()){
                    if($basePrice == -1){
                        $basePrice = (float) $pricing->getInitialPrice();
                    }
                    $basePrice = (float) $basePrice + (float)$pricing->getPrice();
                    break;
                }
            }
        }

        return $basePrice;
    }

    public function getBasePrice(Estate $estate, \Datetime $fromDate, \Datetime $toDate, $forManagePrice = false, $forSearchEngine = false){
        $nowDate = new \DateTime('now');
        $fromDateZeroHour = clone $fromDate;
        $fromDateZeroHour->setTime($nowDate->format('H'),$nowDate->format('i'),$nowDate->format('s'));
        if(!($fromDateZeroHour >= $nowDate && $toDate >= $nowDate && $toDate > $fromDateZeroHour)){
            return -1;
        }
        if($estate->getMin7NightsPricingSeasons()){
            $diff = $fromDate->diff($toDate);
            if($diff->days < 7){
                /** @var PricingSeason $pricingSeason */
                foreach($estate->getMin7NightsPricingSeasons() as $pricingSeason){
                    if($fromDate >= $pricingSeason->getFromDate() && $fromDate <= $pricingSeason->getToDate()){
                        return -1;
                    }
                }
            }
        }

        /** @var PricingRepository $pricingRepository */
        $pricingRepository = $this->_em->getRepository('UTTReservationBundle:Pricing');

        if($estate->getType() == Estate::TYPE_FLEXIBLE_BOOKING){
                // check if number of nights between dates is bigger or equal like in configuration

                /** @var Configuration $configuration */
                $configuration = $this->configurationService->get();
                $diff = $fromDate->diff($toDate);
                if($diff->days >= $configuration->getMinimumNightsForFlexible() && $diff->days <= $configuration->getMaximumNightsForFlexible()){
                    $flexiblePricing = $pricingRepository->getFlexiblePricing($estate, $fromDate, $toDate);
                    if($flexiblePricing){
                        $start = clone $fromDate;
                        $end = clone $toDate;
                        //$end->modify('+1 day');

                        $interval = new \DateInterval('P1D');
                        $period   = new \DatePeriod($start, $interval, $end);

#                        $basePrice = -1;
#                        foreach($period as $key => $date){
#                            /** @var Pricing $pricing */
#                            foreach($flexiblePricing as $pricing){
#                                if($date >= $pricing->getFromDate() && $date <= $pricing->getToDate()){
#                                    if($key == 0){
#                                        $basePrice = $pricing->getInitialPrice();
#                                    }
#                                    $basePrice = $basePrice + (float) $pricing->getPrice();
#                                    break;
#                                }
#                            }
#                        }

                        $basePrice = $this->flexiblePeriodBasePrice($period, $flexiblePricing);
                        if($basePrice != -1) return round($basePrice, 2);
                    }
                }

        }elseif($estate->getType() == Estate::TYPE_STANDARD_M_F){
            if($forManagePrice == true){
                /** @var Pricing $standardPricing */
                $standardPricing = $pricingRepository->getStandardPricingForManagePrice($estate, $fromDate, $toDate);
            }else{
                /** @var Pricing $standardPricing */
                $standardPricing = $pricingRepository->getStandardPricing($estate, $fromDate, $toDate);
            }

            if($standardPricing){
                $basePrice = $standardPricing->getPrice();
                if($forSearchEngine) return round($basePrice, 2);

                $daysDiff = $fromDate->diff($toDate);
                $pricingDaysDiff = $standardPricing->getFromDate()->diff($standardPricing->getToDate());
                if($daysDiff->days > $pricingDaysDiff->days){
                    $extraDays = $daysDiff->days - $pricingDaysDiff->days;
                    $extraPrice = $basePrice * 0.25 * $extraDays;

                    $basePrice = (float)$basePrice + (float)$extraPrice;
                }elseif($daysDiff->days < $pricingDaysDiff->days){
                    if($pricingDaysDiff->days == 4){
                        if($daysDiff->days == 2){ $basePrice = 0.66 * $basePrice; }
                        if($daysDiff->days == 3){ $basePrice = 0.85 * $basePrice; }
                    }
                    if($pricingDaysDiff->days == 3){
                        if($daysDiff->days == 2){ $basePrice = 0.75 * $basePrice; }
                    }
                }

                return round($basePrice, 2);
            }
        }

        return -1;
    }

    public function getPricingListForEstateForYear(Estate $estate, \Datetime $fromDate = null, $years = 1){
        /** @var PricingRepository $pricingRepository */
        $pricingRepository = $this->_em->getRepository('UTTReservationBundle:Pricing');

        if(!($fromDate instanceof \Datetime)){
            $nowDate = new \DateTime('now');
            $fromDate = new \DateTime( sprintf('%04d-01-01', $nowDate->format('Y')));
        }
        $toDate = new \DateTime( sprintf('%04d-12-31', (int)$fromDate->format('Y') + ($years-1) ));

        $pricingList = $pricingRepository->getPricingList($estate, $fromDate, $toDate);
        if($pricingList){
            return $pricingList;
        }

        return false;
    }
########################################################################################################################
/*
 * Estate pricing calendar tool kit methods
 */

    public function increasePricingForEstateForYearFromDate(Estate $estate, $increasePercent, \Datetime $fromDate){
        $pricingList = $this->getPricingListForEstateForYear($estate, $fromDate);
        if($pricingList){
            /** @var Pricing $pricing */
            foreach($pricingList as $pricing){
                $newPrice = (float) $pricing->getPrice() + ((float) $pricing->getPrice() * (float) $increasePercent / 100);
                if($newPrice > 0) $pricing->setPrice($newPrice);

                if($pricing->getType() == Pricing::TYPE_FLEXIBLE_BOOKING){
                    $newInitialPrice = (float) $pricing->getInitialPrice() + ((float) $pricing->getInitialPrice() * (int) $increasePercent / 100);
                    if($newInitialPrice > 0) $pricing->setInitialPrice($newInitialPrice);
                }
            }
            $this->_em->flush();

            return true;
        }

        return false;
    }

    public function decreasePricingForEstateForYearFromDate(Estate $estate, $decreasePercent, \Datetime $fromDate){
        $pricingList = $this->getPricingListForEstateForYear($estate, $fromDate);
        if($pricingList){
            /** @var Pricing $pricing */
            foreach($pricingList as $pricing){
                $newPrice = (float) $pricing->getPrice() - ((float) $pricing->getPrice() * (float) $decreasePercent / 100);
                if($newPrice > 0) $pricing->setPrice($newPrice);

                if($pricing->getType() == Pricing::TYPE_FLEXIBLE_BOOKING){
                    $newInitialPrice = (float) $pricing->getInitialPrice() - ((float) $pricing->getInitialPrice() * (int) $decreasePercent / 100);
                    if($newInitialPrice > 0) $pricing->setInitialPrice($newInitialPrice);
                }
            }
            $this->_em->flush();

            return true;
        }

        return false;
    }

    public function roundPricingForEstateForYearFromDate(Estate $estate, \Datetime $fromDate){
        $pricingList = $this->getPricingListForEstateForYear($estate, $fromDate);
        if($pricingList){
            /** @var Pricing $pricing */
            foreach($pricingList as $pricing){
                $pricing->setPrice(round($pricing->getPrice()));

                if($pricing->getType() == Pricing::TYPE_FLEXIBLE_BOOKING){
                    $pricing->setInitialPrice(round($pricing->getInitialPrice()));
                }
            }
            $this->_em->flush();

            return true;
        }

        return false;
    }
}
