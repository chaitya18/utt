<?php

namespace UTT\ReservationBundle\Service;

use Doctrine\ORM\EntityManager;
use UTT\IndexBundle\Service\ConfigurationService;
use UTT\UserBundle\Entity\User;
use UTT\ReservationBundle\Entity\Reservation;
use UTT\ReservationBundle\Entity\HistoryEntry;
use FOS\UserBundle\Model\UserInterface;
use Symfony\Component\Security\Core\SecurityContext;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;

class HistoryEntryService {
    protected $_em;
    protected $configurationService;
    protected $securityContext;

    public function __construct(EntityManager $em, ConfigurationService $configurationService, SecurityContext $securityContext){
        $this->_em = $em;
        $this->configurationService = $configurationService;
        $this->securityContext = $securityContext;
    }

    public function userAuth($throwException = true){
        if(!$this->securityContext->isGranted('IS_AUTHENTICATED_FULLY') ){
            if($throwException){
                throw new AccessDeniedException();
            }else{
                return false;
            }
        }

        $user = $this->securityContext->getToken()->getUser();
        if (!is_object($user) || !$user instanceof UserInterface) {
            if($throwException){
                throw new AccessDeniedException();
            }else{
                return false;
            }
        }

        return $user;
    }

    public function newCommentThankYouEmailSent(){
        $comment = 'Thank you email sent.';
        return $comment;
    }

    public function newCommentDiscountAfterReview($discountCode, \Datetime $discountExpiresDate){
        $comment = sprintf("Discount code in return of review created %s and sent to the customer. This code expires on %s.", $discountCode, $discountExpiresDate->format('d-M-y'));
        return $comment;
    }

    public function newCommentManualCancelDiscount($discountCode, \Datetime $discountExpiresDate){
        $comment = sprintf("You can use the rebooking code %s to transfer your credit. This must be used in the discount box on a new booking. This code expires on %s.", $discountCode, $discountExpiresDate->format('d-M-y'));
        return $comment;
    }

    public function newCommentManualCancel($daysToHoliday, $refund){
        if ($refund >= 20.00){
            $comment = sprintf("You have cancelled your holiday at %s days, a refund of %s is due.", $daysToHoliday, number_format($refund,2));
        }else{
            $comment = sprintf("You have cancelled your holiday at %s days, no refund is due.", $daysToHoliday);
        }
        return $comment;
    }

    public function newCommentNewBlockBooking($estateName, \Datetime $fromDate, \Datetime $toDate){
        $comment = sprintf("New block booking: %s - %s to %s.",
            $estateName, $fromDate->format('d-M-y'), $toDate->format('d-M-y'));
        return $comment;
    }

    public function newCommentNewGrouponBooking($estateName, \Datetime $fromDate, \Datetime $toDate){
        $comment = sprintf("New Groupon booking: %s - %s to %s.",
            $estateName, $fromDate->format('d-M-y'), $toDate->format('d-M-y'));
        return $comment;
    }

    public function newCommentNewAirbnbBooking($estateName, \Datetime $fromDate, \Datetime $toDate){
        $comment = sprintf("New Airbnb booking - imported: %s - %s to %s.",
            $estateName, $fromDate->format('d-M-y'), $toDate->format('d-M-y'));
        return $comment;
    }

    public function newCommentAirbnbBookingUpdated($estateName, \Datetime $fromDate, \Datetime $toDate, $sleeps){
        $comment = sprintf("Airbnb booking updated %s - %s to %s, %s guests",
            $estateName, $fromDate->format('d-M-y'), $toDate->format('d-M-y'), $sleeps);
        return $comment;
    }

    public function newCommenAirbnbCancelled(){
        $comment = 'Airbnb booking cancelled.';
        return $comment;
    }

    public function newCommentNewTelephoneBooking($estateName, \Datetime $fromDate, \Datetime $toDate){
        $comment = sprintf("New telephone booking: %s - %s to %s.",
            $estateName, $fromDate->format('d-M-y'), $toDate->format('d-M-y'));
        return $comment;
    }

    public function newCommentNewBooking($estateName, \Datetime $fromDate, \Datetime $toDate, $sleeps, $infants, $pets, $totalPrice, $discountValueText = ''){
        $comment = sprintf("New Booking: %s - %s to %s, %s guests, %s infants, %s pets. Total price %s. %s",
            $estateName, $fromDate->format('d-M-y'), $toDate->format('d-M-y'), $sleeps, $infants, $pets, number_format($totalPrice, 2), $discountValueText);
        return $comment;
    }

    public function newCommentCardPaymentDuplicated($cartTypeName, $amount, $authResult, $paymentMethod, $pspReference){
        $comment = sprintf("Duplicated card Payment: %s, card type: %s, authResult: %s, paymentMethod: %s, pspReference: %s", $amount, $cartTypeName, $authResult, $paymentMethod, $pspReference);

        return $comment;
    }

    public function newCommentCardPayment($reservationTotalPrice, $cartTypeName, $amount, $authResult, $paymentMethod, $pspReference){
        if($amount == $reservationTotalPrice){
            $comment = sprintf("Card Payment: %s, card type: %s, authResult: %s, paymentMethod: %s, pspReference: %s", $amount, $cartTypeName, $authResult, $paymentMethod, $pspReference);
        }else{
            //$comment = sprintf("Card Payment: %s, authResult: %s, paymentMethod: %s, pspReference: %s (linked booking credited with %s)", $amount, $authResult, $paymentMethod, $pspReference, number_format($reservationTotalPrice, 2));
            $comment = sprintf("Card Payment: %s, card type: %s, authResult: %s, paymentMethod: %s, pspReference: %s (deposit paid)", $amount, $cartTypeName, $authResult, $paymentMethod, $pspReference);
        }

        return $comment;
    }

    public function newCommentArrivalInstructionEmail(){
        $comment = 'Arrival Instructions sent by email';
        return $comment;
    }

    public function newCommentConfirmationEmail(){
        $comment = 'Confirmation email sent by email';
        return $comment;
    }

    public function newCommentLandlordNotificationEmail($email){
        $comment = 'Landlord notification email sent to '.$email;
        return $comment;
    }

    public function newCommentNoPaymentCancellationReinstate(){
        $comment = 'No payment cancellation email with reinstate option sent.';
        return $comment;
    }

    public function newCommentAutoCancelNoPayment(){
        $comment = 'Automatic cancellation due to non payment';
        return $comment;
    }

    public function newCommentManualAddPayment($amount){
        $comment = "Thank you, we can confirm receipt of " . number_format($amount,2) . ", this has been added to your account.";
        return $comment;
    }

    public function newCommentVoucherPayment($amount){
        $comment = "Thank you, voucher payment " . number_format($amount,2) . " has been added to your reservation.";
        return $comment;
    }

    public function newCommentManualRefundPayment($amount){
        $comment = "We have refunded you " . number_format(abs($amount),2) . ".";
        return $comment;
    }

    public function newCommentAutoCancelOverdueBalancePayment(){
        $comment = 'Automatic cancellation due to overdue balance payment';
        return $comment;
    }

    public function newCommentAutoCancelOverdueBalancePaymentReminder(){
        $comment = 'Sent reminder for: Automatic cancellation due to overdue balance payment';
        return $comment;
    }

    public function newCommentReinstate(){
        $comment = 'We have reinstated your holiday.';
        return $comment;
    }

    public function newCommentAdminCancelRefundOther(){
        $comment = 'We have cancelled your holiday, we will make a credit note.';
        return $comment;
    }

    public function newCommentAdminCancelCustomer($refund){
        $comment = 'We have cancelled your holiday because you have asked us to cancel it. Refund '.$refund;
        return $comment;
    }

    public function newCommentAdminCancelByUs($refund){
        $comment = 'We have cancelled your holiday because we are unable to meet your requests. Refund '.$refund;
        return $comment;
    }

    public function newCommentAdminCancelFullRefund($refund){
        $comment = 'We have cancelled your holiday because you have asked us to cancel it. Refund '.$refund;
        return $comment;
    }

    public function newCommentAdminCancelNoRefund(){
        $comment = 'We have cancelled your holiday. Refund: NONE';
        return $comment;
    }

    public function newCommentHoldForPayment($hours, $reason){
        $comment = 'Hold for payment. '.$hours.' hours. Reason: '.$reason;
        return $comment;
    }

    public function newCommentAdminCancelNotPaidNoPayment(){
        $comment = 'We have cancelled your holiday, because you have not completed the payment process. Refund: NONE';
        return $comment;
    }

    public function newCommentAdminCancelNotPaidWithPayment(){
        $comment = 'We have cancelled your holiday, because you have not paid your balance by our stated balance due date. Refund: NONE';
        return $comment;
    }

    public function newCommentCardFailed($authResult){
        $comment = sprintf("Payment Failed: authResult: %s", $authResult);

        return $comment;
    }

    public function newCommentRequote($estateName, \Datetime $fromDate, \Datetime $toDate, $sleeps, $infants, $pets, $totalPrice, $discountValueText = ''){
        $comment = sprintf("Holiday Changed: %s - %s to %s, %s guests, %s infants, %s pets. Total price %s. %s",
            $estateName, $fromDate->format('d-M-y'), $toDate->format('d-M-y'), $sleeps, $infants, $pets, number_format($totalPrice, 2), $discountValueText);
        return $comment;
    }

    public function newCommentChangeBooking($estateName, \Datetime $fromDate, \Datetime $toDate, $sleeps, $infants, $pets, $totalPrice, $discountValueText = ''){
        $comment = sprintf("Holiday Changed: %s - %s to %s, %s guests, %s infants, %s pets. Total price %s. %s",
            $estateName, $fromDate->format('d-M-y'), $toDate->format('d-M-y'), $sleeps, $infants, $pets, number_format($totalPrice, 2), $discountValueText);
        return $comment;
    }

    public function newCommentReferredFlagChanged($referredFlagName){
        $comment = sprintf("Referred flag changed to %s", $referredFlagName);

        return $comment;
    }

    public function newCommentReferredFlagCleared(){
        $comment = sprintf("Referred flag cleared.");

        return $comment;
    }

    public function newEntryByLoggedUser($typeId, Reservation $reservation, $comment){
        $user = $this->userAuth(false);
        if($user instanceof User){
            return $this->newEntry($typeId, $reservation, $user, $comment);
        }

        return false;
    }

    public function newEntry($typeId, Reservation $reservation, User $user, $comment){
        $historyEntry = new HistoryEntry();
        if($historyEntry->isValidType($typeId) && $reservation instanceof Reservation && $user instanceof User){
            $historyEntry->setType($typeId);
            $historyEntry->setReservation($reservation);
            $historyEntry->setUser($user);
            $historyEntry->setComment($comment);

            try{
                $this->_em->persist($historyEntry);
                $this->_em->flush();

                return $historyEntry;
            }catch (\Exception $e){
                return false;
            }
        }

        return false;
    }

}