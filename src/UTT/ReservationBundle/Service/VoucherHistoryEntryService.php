<?php

namespace UTT\ReservationBundle\Service;

use Doctrine\ORM\EntityManager;
use UTT\IndexBundle\Service\ConfigurationService;
use UTT\ReservationBundle\Entity\DiscountCode;
use UTT\ReservationBundle\Entity\Voucher;
use UTT\ReservationBundle\Entity\VoucherHistoryEntry;
use UTT\UserBundle\Entity\User;
use FOS\UserBundle\Model\UserInterface;
use Symfony\Component\Security\Core\SecurityContext;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;

class VoucherHistoryEntryService {
    protected $_em;
    protected $configurationService;
    protected $securityContext;

    public function __construct(EntityManager $em, ConfigurationService $configurationService, SecurityContext $securityContext){
        $this->_em = $em;
        $this->configurationService = $configurationService;
        $this->securityContext = $securityContext;
    }

    public function userAuth($throwException = true){
        if(!$this->securityContext->isGranted('IS_AUTHENTICATED_FULLY') ){
            if($throwException){
                throw new AccessDeniedException();
            }else{
                return false;
            }
        }

        $user = $this->securityContext->getToken()->getUser();
        if (!is_object($user) || !$user instanceof UserInterface) {
            if($throwException){
                throw new AccessDeniedException();
            }else{
                return false;
            }
        }

        return $user;
    }

    public function newCommentNewVoucher($voucherValue, $voucherName, \Datetime $expiresAt){
        $comment = sprintf("New Voucher, value: %s, name %s, expires at %s",
            $voucherValue, $voucherName, $expiresAt->format('d-M-y'));
        return $comment;
    }

    public function newCommentCardPayment($cartTypeName, $amount, $authResult, $paymentMethod, $pspReference){
        $comment = sprintf("Card Payment: %s, card type: %s, authResult: %s, paymentMethod: %s, pspReference: %s", $amount, $cartTypeName, $authResult, $paymentMethod, $pspReference);

        return $comment;
    }

    public function newCommentConfirmationEmail(){
        $comment = 'Confirmation email sent by email';
        return $comment;
    }

    public function newCommentCardFailed($authResult){
        $comment = sprintf("Payment Failed: authResult: %s", $authResult);

        return $comment;
    }

    public function newCommentVoucherDiscount(DiscountCode $discountCode){
        $comment = sprintf("Voucher discount code created %s and sent to the customer. This code expires on %s.", $discountCode->getCode(), $discountCode->getExpiresAt()->format('d-M-y'));
        return $comment;
    }

    public function newEntryByLoggedUser($typeId, Voucher $voucher, $comment){
        $user = $this->userAuth(false);
        if($user instanceof User){
            return $this->newEntry($typeId, $voucher, $user, $comment);
        }

        return false;
    }

    public function newEntry($typeId, Voucher $voucher, User $user, $comment){
        $voucherHistoryEntry = new VoucherHistoryEntry();
        if($voucherHistoryEntry->isValidType($typeId) && $voucher instanceof Voucher && $user instanceof User){
            $voucherHistoryEntry->setType($typeId);
            $voucherHistoryEntry->setVoucher($voucher);
            $voucherHistoryEntry->setUser($user);
            $voucherHistoryEntry->setComment($comment);

            try{
                $this->_em->persist($voucherHistoryEntry);
                $this->_em->flush();

                return $voucherHistoryEntry;
            }catch (\Exception $e){
                return false;
            }
        }

        return false;
    }

}