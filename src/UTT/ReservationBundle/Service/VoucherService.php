<?php

namespace UTT\ReservationBundle\Service;

use Doctrine\ORM\EntityManager;
use UTT\ReservationBundle\Entity\VoucherHistoryEntry;
use UTT\ReservationBundle\Service\DiscountCodeService;
use Symfony\Component\HttpFoundation\Session\Session;
use UTT\ReservationBundle\Model\VoucherData;
use UTT\ReservationBundle\Model\VoucherDataPrice;
use UTT\ReservationBundle\Model\VoucherDataUserData;
use UTT\UserBundle\Entity\User;
use UTT\ReservationBundle\Entity\DiscountCode;

use UTT\IndexBundle\Service\EmailService;
use UTT\ReservationBundle\Entity\Voucher;
use UTT\ReservationBundle\Factory\VoucherFactory;

use UTT\UserBundle\Entity\UserRepository;
use \WhiteOctober\TCPDFBundle\Controller\TCPDFController;
use \Symfony\Bundle\TwigBundle\TwigEngine;
use UTT\ReservationBundle\Service\VoucherHistoryEntryService;
use UTT\IndexBundle\Entity\Configuration;

class VoucherService {
    protected $_em;
    protected $discountCodeService;
    protected $session;
    protected $voucherFactory;
    protected $emailService;
    protected $tcpdfController;
    protected $twigEngine;
    protected $voucherHistoryEntryService;

    public function __construct(EntityManager $em, DiscountCodeService $discountCodeService, Session $session, VoucherFactory $voucherFactory, EmailService $emailService, TCPDFController $tcpdfController, TwigEngine $twigEngine, VoucherHistoryEntryService $voucherHistoryEntryService){
        $this->_em = $em;
        $this->discountCodeService = $discountCodeService;
        $this->session = $session;
        $this->voucherFactory = $voucherFactory;
        $this->emailService = $emailService;
        $this->tcpdfController = $tcpdfController;
        $this->twigEngine = $twigEngine;
        $this->voucherHistoryEntryService = $voucherHistoryEntryService;

    }

    public function newVoucherPrice($value, Configuration $configuration = null){
        if(Voucher::isAllowedValueStatic($value)){
            $price = new VoucherDataPrice();
            $price->setBasePrice($value);
            $price->setTotal($value);

            if($configuration instanceof Configuration){
                if($value == Voucher::VALUE_50){
                    $price->setBasePrice($configuration->getVoucherValue50OfferPrice());
                    $price->setTotal($configuration->getVoucherValue50OfferPrice());
                }elseif($value == Voucher::VALUE_100){
                    $price->setBasePrice($configuration->getVoucherValue100OfferPrice());
                    $price->setTotal($configuration->getVoucherValue100OfferPrice());
                }elseif($value == Voucher::VALUE_150){
                    $price->setBasePrice($configuration->getVoucherValue150OfferPrice());
                    $price->setTotal($configuration->getVoucherValue150OfferPrice());
                }elseif($value == Voucher::VALUE_200){
                    $price->setBasePrice($configuration->getVoucherValue200OfferPrice());
                    $price->setTotal($configuration->getVoucherValue200OfferPrice());
                }elseif($value == Voucher::VALUE_229) {
                    $price->setBasePrice($configuration->getVoucherValue229OfferPrice());
                    $price->setTotal($configuration->getVoucherValue229OfferPrice());
                }elseif($value == Voucher::VALUE_250){
                    $price->setBasePrice($configuration->getVoucherValue250OfferPrice());
                    $price->setTotal($configuration->getVoucherValue250OfferPrice());
                }elseif($value == Voucher::VALUE_300){
                    $price->setBasePrice($configuration->getVoucherValue300OfferPrice());
                    $price->setTotal($configuration->getVoucherValue300OfferPrice());
                }elseif($value == Voucher::VALUE_500){
                    $price->setBasePrice($configuration->getVoucherValue500OfferPrice());
                    $price->setTotal($configuration->getVoucherValue500OfferPrice());
                }elseif($value == Voucher::VALUE_1000){
                    $price->setBasePrice($configuration->getVoucherValue1000OfferPrice());
                    $price->setTotal($configuration->getVoucherValue1000OfferPrice());
                }
            }

            return $price;
        }

        return false;
    }

    public function book($voucherValue, $voucherName, User $user, VoucherDataPrice $price, VoucherDataUserData $userData){
        $voucher = $this->voucherFactory->create($voucherValue, $voucherName, $user, $price, $userData);
        $voucher->setStatus(Voucher::STATUS_RESERVED);

        try{
            $this->_em->persist($voucher);
            $this->_em->flush();

            $this->emailService->sendVoucherConfirmation($voucher);

            $voucherHistoryEntryComment = $this->voucherHistoryEntryService->newCommentNewVoucher($voucher->getValue(), $voucher->getName(), $voucher->getExpiresAt());
            $this->voucherHistoryEntryService->newEntry(VoucherHistoryEntry::TYPE_NEW_VOUCHER, $voucher, $user, $voucherHistoryEntryComment);

            $this->removeVoucherData();
            return $voucher;
        }catch (\Exception $e){
            $this->removeVoucherData();
            return false;
        }
    }

    /**
     * @param DiscountCode $discountCode
     * @return object|null
     */
    protected function getUserByDiscountCode(DiscountCode $discountCode) {
        $user = null;
        $emailsString = str_replace(' ', '', $discountCode->getEmails());
        $emailsArray = explode(';',$emailsString);
        if(isset($emailsArray[0])) {
            $userRepository = $this->_em->getRepository('UTTUserBundle:User');
            $user = $userRepository->findOneBy(array('emailCanonical' => strtolower($emailsArray[0])));
        }
        return $user;
    }

    /**
     * @param DiscountCode $discountCode
     * @return object|null
     */
    protected function getVoucherByDiscountCode(DiscountCode $discountCode) {
        $voucherRepository = $this->_em->getRepository('UTTReservationBundle:Voucher');
        return $voucherRepository->findOneBy(array('discountCode' => $discountCode));
    }



    /**
     * @param DiscountCode $discountCode
     * @return bool|Voucher
     */
    public function createByAdminFromDiscountCode(DiscountCode $discountCode, $user = null) {

        $voucher = $this->getVoucherByDiscountCode($discountCode);
        if(!$user) {
            $user = $this->getUserByDiscountCode($discountCode);
        }

        if($user instanceOf User){

            $voucher = $this->voucherFactory->createFromDiscountCodeAndUser($user, $discountCode, $voucher);

            try{
                if($voucher &&  $voucher->getId()) {
                    $this->_em->flush($voucher);
                } else {
                    $this->_em->persist($voucher);
                    $this->_em->flush();
                }

                $voucherHistoryEntryComment = $this->voucherHistoryEntryService->newCommentNewVoucher($voucher->getValue(), $voucher->getName(), $voucher->getExpiresAt());
                $this->voucherHistoryEntryService->newEntry(VoucherHistoryEntry::TYPE_VOUCHER_DISCOUNT_CODE, $voucher, $user, $voucherHistoryEntryComment);

                return $voucher;
            }catch (\Exception $e){
                return false;
            }
        }
    }



    public function saveVoucherData($value, $name, array $userData, array $price){

        $voucherData = $this->buildVoucherDataFromArray($value, $name, $userData, $price);
        $this->session->set('uttVoucherData', $voucherData);
        return true;
    }

    protected function buildVoucherDataFromArray($value, $name, array $userData, array $price)
    {
        $voucherDataUserData = new VoucherDataUserData();
        $userData = (object)$userData;
        if(isset($userData->firstName)) $voucherDataUserData->setFirstName($userData->firstName);
        if(isset($userData->lastName)) $voucherDataUserData->setLastName($userData->lastName);
        if(isset($userData->email)) $voucherDataUserData->setEmail($userData->email);
        if(isset($userData->phone)) $voucherDataUserData->setPhone($userData->phone);
        if(isset($userData->address)) $voucherDataUserData->setAddress($userData->address);
        if(isset($userData->city)) $voucherDataUserData->setCity($userData->city);
        if(isset($userData->postcode)) $voucherDataUserData->setPostcode($userData->postcode);
        if(isset($userData->country)) $voucherDataUserData->setCountry($userData->country);

        $voucherDataPrice = new VoucherDataPrice();
        $price = (object)$price;
        if(isset($price->basePrice)) $voucherDataPrice->setBasePrice($price->basePrice);
        if(isset($price->creditCardCharge)) $voucherDataPrice->setCreditCardCharge($price->creditCardCharge);
        if(isset($price->charityCharge)) $voucherDataPrice->setCharityCharge($price->charityCharge);
        if(isset($price->total)) $voucherDataPrice->setTotal($price->total);

        $voucherData = new VoucherData();
        $voucherData->setValue($value);
        $voucherData->setName($name);
        $voucherData->setUserData($voucherDataUserData);
        $voucherData->setPrice($voucherDataPrice);

        return $voucherData;


    }


    public function getVoucherData(){
        if($this->session->has('uttVoucherData')){
            $data = $this->session->get('uttVoucherData');
            if($data instanceof VoucherData){
                return $data;
            }
        }

        return false;
    }

    public function removeVoucherData(){
        if($this->session->has('uttVoucherData')){
            $this->session->remove('uttVoucherData');
        }

        // check if removed
        if(!$this->session->has('uttVoucherData')){
            return true;
        }
        return false;
    }

    public function generateVoucherPDF(Voucher $voucher){
        $pdf = $this->tcpdfController->create();
        $pdf->SetAutoPageBreak(true, 6);
        $pdf->setPrintHeader(false);
        $pdf->setPrintFooter(false);
        $pdf->SetMargins(6,6,6, true);
        $pdf->AddPage();
        $pdf->Image('bundles/uttreservation/images/voucher-template.png', 0, 0, 210, 84, '', '', '', false, 300, '', false, false, 0);

        $html = $this->twigEngine->render('UTTReservationBundle:Voucher:voucherPdf.html.twig', array(
            'voucherValue' => $voucher->getValueName(),
            'voucherName' => $voucher->getName(),
            'expiresAt' => $voucher->getExpiresAt(),
            'voucherDiscountCode' => $voucher->getDiscountCode()->getCode()
        ));
        $pdf->writeHTML($html);

        $pdf->Output('ouycer.pdf', 'I');
        exit;
    }

}