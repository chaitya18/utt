<?php

namespace UTT\ReservationBundle\Service;

use Doctrine\ORM\EntityManager;
use UTT\IndexBundle\Service\ConfigurationService;
use UTT\ReservationBundle\Entity\OwnerStatement;
use UTT\EstateBundle\Entity\Estate;
use UTT\ReservationBundle\Entity\ReservationRepository;
use UTT\IndexBundle\Entity\Configuration;
use \WhiteOctober\TCPDFBundle\Controller\TCPDFController;
use \Symfony\Bundle\TwigBundle\TwigEngine;
use UTT\QueueBundle\Service\PropertyStatementQueueService;

class OwnerStatementService {
    protected $_em;
    protected $configurationService;
    protected $agencyConfigId;
    protected $tcpdfController;
    protected $twigEngine;
    protected $propertyStatementQueueService;
    protected $kernelRootDir;

    /** @var  Configuration */
    private $systemConfiguration;

    public function __construct(EntityManager $em, ConfigurationService $configurationService, $agencyConfigId, TCPDFController $tcpdfController, TwigEngine $twigEngine, PropertyStatementQueueService $propertyStatementQueueService, $kernelRootDir){
        $this->_em = $em;
        $this->configurationService = $configurationService;
        $this->agencyConfigId = $agencyConfigId;
        $this->tcpdfController = $tcpdfController;
        $this->twigEngine = $twigEngine;
        $this->propertyStatementQueueService = $propertyStatementQueueService;
        $this->kernelRootDir = $kernelRootDir;

        $this->getConfiguration();
    }

    private function getConfiguration(){
        $this->systemConfiguration = $this->configurationService->get();
        if(!($this->systemConfiguration instanceof Configuration)){
            throw new \Exception('System configuration not found.');
        }
    }

    public function generatePropertyStatementPDF(Estate $estate, \Datetime $date, $forCommand = false){
        /** @var ReservationRepository $reservationRepository */
        $reservationRepository = $this->_em->getRepository('UTTReservationBundle:Reservation');
        $reservations = $reservationRepository->getReservationsByDateStringAndEstateId($date->format('Y-m'), $estate->getId());
        if(!$reservations) {
            $this->propertyStatementQueueService->removeFromQueue($estate, $date);
            
            return false;
        }

        $pdf = $this->tcpdfController->create();
        $pdf->SetAutoPageBreak(true, 6);
        $pdf->setPrintHeader(false);
        $pdf->setPrintFooter(false);
        $pdf->SetMargins(6,6,6, true);
        $pdf->AddPage();

        $html = $this->twigEngine->render('UTTAdminBundle:OwnerStatements:createPropertyStatement.html.twig', array(
            'dateObject' => $date,
            'nowDate' => new \DateTime('now'),
            'estate' => $estate,
            'reservations' => $reservations,
            'companyVatNo' => $this->systemConfiguration->getCompanyVatNo(),
            'companyName' => $this->systemConfiguration->getCompanyName()
        ));
        $pdf->writeHTML($html);

        $ownerStatementDirectory = $this->getFileDirectory($forCommand);
        $fileName = $this->generatePropertyStatementFilName($estate->getId(), $date);

        $pdf->Output($ownerStatementDirectory.$fileName, 'F');

        /** @var OwnerStatement $ownerStatement */
        $ownerStatement = $this->createPropertyStatement($estate, $date, $fileName);
        if(!($ownerStatement instanceof OwnerStatement)){
            throw new \Exception('Property statement for '.$date->format('F Y').' not created. Try again.');
        }

        $this->propertyStatementQueueService->removeFromQueue($estate, $date);

        return $ownerStatement;
    }

    private function create(\Datetime $monthDate, $fileName, $type, Estate $estate = null){
        $ownerStatement = $this->_em->getRepository('UTTReservationBundle:OwnerStatement')->findOneBy(array(
            'filename' => $fileName
        ));
        if(!($ownerStatement instanceof OwnerStatement)){
            $ownerStatement = new OwnerStatement();
        }

        $ownerStatement->setMonthDate($monthDate);
        $ownerStatement->setFilename($fileName);
        $ownerStatement->setType($type);

        if($estate instanceof Estate){
            $ownerStatement->setEstate($estate);
        }

        try {
            $this->_em->persist($ownerStatement);
            $this->_em->flush();

            return $ownerStatement;
        }catch (\Exception $e){
            return false;
        }
    }

    public function createPropertyStatement(Estate $estate, \Datetime $monthDate, $fileName){
        return $this->create($monthDate, $fileName, OwnerStatement::TYPE_PROPERTY_STATEMENT, $estate);
    }

    public function createFullStatement(\Datetime $monthDate, $fileName){
        return $this->create($monthDate, $fileName, OwnerStatement::TYPE_FULL_STATEMENT);
    }

    public function createFullSummaryStatement(\Datetime $monthDate, $fileName){
        return $this->create($monthDate, $fileName, OwnerStatement::TYPE_FULL_SUMMARY);
    }

    public function generatePropertyStatementFilName($estateId, \Datetime $date){
        return $estateId.'-'.$date->format('Y-m').'-'.md5($estateId.'-'.$date->format('Y-m')).'.pdf';
    }

    public function generateFullStatementFileName(\Datetime $date){
        return 'FULL-'.$this->agencyConfigId.'-'.$date->format('Y-m').'-'.md5('FULL-'.$this->agencyConfigId.'-'.$date->format('Y-m')).'.pdf';
    }

    public function generateSummaryFileName(\Datetime $date){
        return 'SUMMARY-'.$this->agencyConfigId.'-'.$date->format('Y-m').'-'.md5('SUMMARY-'.$this->agencyConfigId.'-'.$date->format('Y-m')).'.pdf';
    }

    public function getFileDirectory($forCommand = false){
        $photo = new OwnerStatement();

        if($forCommand){
            $fileDirectory = rtrim($this->kernelRootDir.'/../web/'.$photo->getUploadDir(),'/').'/';
        }else{
            $fileDirectory = rtrim($photo->getUploadDir(),'/').'/';
        }

        if(!file_exists($fileDirectory)) mkdir($fileDirectory, 0777);

        return $fileDirectory;
    }


}