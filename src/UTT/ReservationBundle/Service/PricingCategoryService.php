<?php

namespace UTT\ReservationBundle\Service;

use Doctrine\ORM\EntityManager;
use UTT\EstateBundle\Entity\Estate;
use UTT\ReservationBundle\Entity\Pricing;
use UTT\ReservationBundle\Entity\PricingRepository;
use UTT\IndexBundle\Service\ConfigurationService;
use UTT\IndexBundle\Entity\Configuration;
use UTT\ReservationBundle\Entity\Offer;
use UTT\ReservationBundle\Entity\OfferRepository;
use UTT\ReservationBundle\Entity\PricingCategory;
use UTT\ReservationBundle\Entity\PricingSeasonRepository;
use UTT\ReservationBundle\Entity\PricingSeason;
use UTT\ReservationBundle\Entity\PricingCategoryPriceFlexible;
use UTT\ReservationBundle\Entity\PricingCategoryPriceStandard;


class PricingCategoryService {
    protected $_em;

    public function __construct(EntityManager $em){
        $this->_em = $em;
    }

    private function formatPriceByOption($price, $option){
        if($option == 'increase5'){
            $price = (float) $price + ((float) $price * 5 / 100);
        }elseif($option == 'increase10'){
            $price = (float) $price + ((float) $price * 10 / 100);
        }elseif($option == 'decrease5'){
            $price = (float) $price - ((float) $price * 5 / 100);
        }elseif($option == 'decrease10'){
            $price = (float) $price - ((float) $price * 10 / 100);
        }
        return $price;
    }

    public function importPricesForFlexible(PricingCategory $pricingCategory, $year, PricingCategory $importFromPricingCategory, $generatePricesOption = null){
        foreach(PricingCategoryPriceFlexible::getAllowedTypesStatic() as $key => $value){
            /** @var PricingCategoryPriceFlexible $importPrice */
            foreach($importFromPricingCategory->getPricesFlexible() as $importPrice){
                if($importPrice->getYear() == $year && $importPrice->getType() == $key){
                    /** @var PricingCategoryPriceFlexible $price */
                    foreach($pricingCategory->getPricesFlexible() as $price){
                        if($price->getYear() == $year && $price->getType() == $key){
                            $this->_em->remove($price);
                        }
                    }

                    $newPrice = new PricingCategoryPriceFlexible();
                    $newPrice->setType($key);
                    $newPrice->setYear($year);
                    $newPrice->setPriceStandingCharge($this->formatPriceByOption($importPrice->getPriceStandingCharge(), $generatePricesOption));
                    $newPrice->setPriceSundayThursday($this->formatPriceByOption($importPrice->getPriceSundayThursday(), $generatePricesOption));
                    $newPrice->setPriceFridaySaturday($this->formatPriceByOption($importPrice->getPriceFridaySaturday(), $generatePricesOption));
                    $newPrice->setPricingCategory($pricingCategory);

                    $this->_em->persist($newPrice);
                }
            }
        }
        $this->_em->flush();
    }

    public function generateNewPricesForFlexible(PricingCategory $pricingCategory, $year, $generatePricesOption = null){
        foreach(PricingCategoryPriceFlexible::getAllowedTypesStatic() as $key => $value){
            $lastYearPrice = false;
            /** @var PricingCategoryPriceFlexible $price */
            foreach($pricingCategory->getPricesFlexible() as $price){
                if($price->getYear() == $year && $price->getType() == $key){
                    $this->_em->remove($price);
                }elseif($price->getYear() == ($year-1) && $price->getType() == $key){
                    $lastYearPrice = $price;
                }
            }
            $newPrice = new PricingCategoryPriceFlexible();
            $newPrice->setType($key);
            $newPrice->setYear($year);
            if($lastYearPrice instanceof PricingCategoryPriceFlexible){
                $newPrice->setPriceStandingCharge($this->formatPriceByOption($lastYearPrice->getPriceStandingCharge(), $generatePricesOption));
                $newPrice->setPriceSundayThursday($this->formatPriceByOption($lastYearPrice->getPriceSundayThursday(), $generatePricesOption));
                $newPrice->setPriceFridaySaturday($this->formatPriceByOption($lastYearPrice->getPriceFridaySaturday(), $generatePricesOption));
            }else{
                $newPrice->setPriceStandingCharge(0);
                $newPrice->setPriceSundayThursday(0);
                $newPrice->setPriceFridaySaturday(0);
            }
            $newPrice->setPricingCategory($pricingCategory);

            $this->_em->persist($newPrice);
        }

        $this->_em->flush();
    }

    public function importPricesForStandard(PricingCategory $pricingCategory, $year, PricingCategory $importFromPricingCategory, $generatePricesOption = null){
        foreach(PricingCategoryPriceStandard::getAllowedTypesStatic() as $key => $value){
            /** @var PricingCategoryPriceStandard $importPrice */
            foreach($importFromPricingCategory->getPricesStandard() as $importPrice){
                if($importPrice->getYear() == $year && $importPrice->getType() == $key){
                    /** @var PricingCategoryPriceStandard $price */
                    foreach($pricingCategory->getPricesStandard() as $price){
                        if($price->getYear() == $year && $price->getType() == $key){
                            $this->_em->remove($price);
                        }
                    }

                    $newPrice = new PricingCategoryPriceStandard();
                    $newPrice->setType($key);
                    $newPrice->setYear($year);
                    $newPrice->setPrice7Nights($this->formatPriceByOption($importPrice->getPrice7Nights(), $generatePricesOption));
                    $newPrice->setPriceMondayFriday($this->formatPriceByOption($importPrice->getPriceMondayFriday(), $generatePricesOption));
                    $newPrice->setPriceFridayMonday($this->formatPriceByOption($importPrice->getPriceFridayMonday(), $generatePricesOption));
                    $newPrice->setPricingCategory($pricingCategory);

                    $this->_em->persist($newPrice);
                }
            }
        }
        $this->_em->flush();
    }

    public function generateNewPricesForStandard(PricingCategory $pricingCategory, $year, $generatePricesOption = null){
        foreach(PricingCategoryPriceStandard::getAllowedTypesStatic() as $key => $value){
            $lastYearPrice = false;
            /** @var PricingCategoryPriceStandard $price */
            foreach($pricingCategory->getPricesStandard() as $price){
                if($price->getYear() == $year && $price->getType() == $key){
                    $this->_em->remove($price);
                }elseif($price->getYear() == ($year-1) && $price->getType() == $key){
                    $lastYearPrice = $price;
                }
            }
            $newPrice = new PricingCategoryPriceStandard();
            $newPrice->setType($key);
            $newPrice->setYear($year);
            if($lastYearPrice instanceof PricingCategoryPriceStandard){
                $newPrice->setPrice7Nights($this->formatPriceByOption($lastYearPrice->getPrice7Nights(), $generatePricesOption));
                $newPrice->setPriceMondayFriday($this->formatPriceByOption($lastYearPrice->getPriceMondayFriday(), $generatePricesOption));
                $newPrice->setPriceFridayMonday($this->formatPriceByOption($lastYearPrice->getPriceFridayMonday(), $generatePricesOption));
            }else{
                $newPrice->setPrice7Nights(0);
                $newPrice->setPriceMondayFriday(0);
                $newPrice->setPriceFridayMonday(0);
            }
            $newPrice->setPricingCategory($pricingCategory);

            $this->_em->persist($newPrice);
        }

        $this->_em->flush();
    }

    public function getYearsFromPricingCategory(PricingCategory $pricingCategory){
        $yearArray = array();
        if($pricingCategory->getType() == PricingCategory::TYPE_FLEXIBLE_BOOKING){
            if($pricingCategory->getPricesFlexible()){
                /** @var PricingCategoryPriceFlexible $priceFlexible */
                foreach($pricingCategory->getPricesFlexible() as $priceFlexible){
                    if(!in_array($priceFlexible->getYear(), $yearArray)){
                        $yearArray[] = $priceFlexible->getYear();
                    }
                }
            }
        }elseif($pricingCategory->getType() == PricingCategory::TYPE_STANDARD_M_F){
            if($pricingCategory->getPricesStandard()){
                /** @var PricingCategoryPriceStandard $priceStandard */
                foreach($pricingCategory->getPricesStandard() as $priceStandard){
                    if(!in_array($priceStandard->getYear(), $yearArray)){
                        $yearArray[] = $priceStandard->getYear();
                    }
                }
            }
        }

        return $yearArray;
    }


}