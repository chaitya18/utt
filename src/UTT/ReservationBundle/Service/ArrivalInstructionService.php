<?php

namespace UTT\ReservationBundle\Service;

use Doctrine\ORM\EntityManager;
use UTT\IndexBundle\Service\ConfigurationService;
use UTT\EstateBundle\Entity\Estate;
use UTT\ReservationBundle\Entity\Reservation;
use UTT\ReservationBundle\Entity\ReservationRepository;
use UTT\IndexBundle\Entity\Configuration;
use \WhiteOctober\TCPDFBundle\Controller\TCPDFController;
use \Symfony\Bundle\TwigBundle\TwigEngine;
use UTT\QueueBundle\Service\PropertyStatementQueueService;
use UTT\IndexBundle\Entity\Email;
use UTT\IndexBundle\Entity\EmailRepository;
use UTT\ReservationBundle\Entity\ArrivalInstruction;
use UTT\ReservationBundle\Entity\ArrivalInstructionRepository;

class ArrivalInstructionService {
    protected $_em;
    protected $configurationService;
    protected $agencyConfigId;
    protected $tcpdfController;
    protected $twigEngine;
    protected $propertyStatementQueueService;
    protected $kernelRootDir;

    /** @var EmailRepository  */
    protected $emailRepository;

    /** @var  Configuration */
    private $systemConfiguration;

    public function __construct(EntityManager $em, ConfigurationService $configurationService, $agencyConfigId, TCPDFController $tcpdfController, TwigEngine $twigEngine, PropertyStatementQueueService $propertyStatementQueueService, $kernelRootDir){
        $this->_em = $em;
        $this->configurationService = $configurationService;
        $this->agencyConfigId = $agencyConfigId;
        $this->tcpdfController = $tcpdfController;
        $this->twigEngine = $twigEngine;
        $this->propertyStatementQueueService = $propertyStatementQueueService;
        $this->kernelRootDir = $kernelRootDir;

        $this->emailRepository = $this->_em->getRepository('UTTIndexBundle:Email');

        $this->getConfiguration();
    }

    private function getConfiguration(){
        $this->systemConfiguration = $this->configurationService->get();
        if(!($this->systemConfiguration instanceof Configuration)){
            throw new \Exception('System configuration not found.');
        }
    }

    public function getArrivalInstructionContentForWeb(){
        /** @var Email $email */
        $email = $this->emailRepository->findOneBy(array(
            'type' => Email::TYPE_WEB_ARRIVAL
        ));
        if($email instanceof Email){
            return $email->getContent();
        }

        return true;
    }

    public function findOrCreate(Reservation $reservation){
        /** @var ArrivalInstructionRepository $arrivalInstructionRepository */
        $arrivalInstructionRepository = $this->_em->getRepository('UTTReservationBundle:ArrivalInstruction');
        /** @var ArrivalInstruction $arrivalInstruction */
        $arrivalInstruction = $arrivalInstructionRepository->findOneBy(array(
            'reservation' => $reservation->getId()
        ));

        if($arrivalInstruction instanceof ArrivalInstruction){
            //return $arrivalInstruction;
        }

        try{
            /** @var ArrivalInstruction $arrivalInstruction */
            $arrivalInstruction = $this->generateArrivalInstructionForReservationPDF($reservation);

            if($arrivalInstruction instanceof ArrivalInstruction){ return $arrivalInstruction; }
        }catch(\Exception $e){
            return false;
        }

        return false;
    }

    public function generateArrivalInstructionForReservationPDF(Reservation $reservation){
        $pdf = $this->tcpdfController->create();
        $pdf->SetAutoPageBreak(true, 6);
        $pdf->setPrintHeader(false);
        $pdf->setPrintFooter(false);
        $pdf->SetMargins(6,6,6, true);
        $pdf->AddPage();

        $html = $this->twigEngine->render('UTTReservationBundle:ArrivalInstruction:createArrivalInstructionForReservation.html.twig', array(
            'nowDate' => new \DateTime('now'),
            'reservation' => $reservation,
            'companyVatNo' => $this->systemConfiguration->getCompanyVatNo(),
            'companyName' => $this->systemConfiguration->getCompanyName()
        ));
        $pdf->writeHTML($html);

        $arrivalInstructionDirectory = $this->getFileDirectory();
        $fileName = $this->generateArrivalInstructionForReservationFilName($reservation);

        $pdf->Output($arrivalInstructionDirectory.$fileName, 'F');

        /** @var ArrivalInstruction $arrivalInstruction */
        $arrivalInstruction = $this->createArrivalInstructionForReservation($reservation, $fileName);
        if(!($arrivalInstruction instanceof ArrivalInstruction)){
            throw new \Exception('Arrival instructions for reservation '.$reservation->getId().' not created. Try again.');
        }

        return $arrivalInstruction;
    }

    public function createArrivalInstructionForReservation(Reservation $reservation, $fileName, $externalFileName = null){
        $arrivalInstruction = $this->_em->getRepository('UTTReservationBundle:ArrivalInstruction')->findOneBy(array(
            'filename' => $fileName
        ));
        if(!($arrivalInstruction instanceof ArrivalInstruction)){
            $arrivalInstruction = new ArrivalInstruction();
        }

        $arrivalInstruction->setFilename($fileName);
        $arrivalInstruction->setExternalFilename($externalFileName);
        $arrivalInstruction->setType(ArrivalInstruction::TYPE_FOR_RESERVATION);
        $arrivalInstruction->setReservation($reservation);

        try {
            $this->_em->persist($arrivalInstruction);
            $this->_em->flush();

            return $arrivalInstruction;
        }catch (\Exception $e){
            return false;
        }
    }

    public function generateArrivalInstructionForReservationFilName(Reservation $reservation){
        return 'arrivalinstructions-'.$reservation->getId().$reservation->getPin().'.pdf';
    }

    public function generateExternalArrivalInstructionForReservationFilName(Reservation $reservation){
        return 'arrivalinstructions-'.$reservation->getId().$reservation->getExternalPin().'.pdf';
    }

    public function getImportFromFileDirectory($forCommand = false){
        if($forCommand){
            $fileDirectory = rtrim($this->kernelRootDir.'/../web/'.'import/arrival-instructions','/').'/';
        }else{
            $fileDirectory = rtrim('import/arrival-instructions','/').'/';
        }

        if(!file_exists($fileDirectory)) mkdir($fileDirectory, 0777);

        return $fileDirectory;
    }

    public function getFileDirectory($forCommand = false){
        $photo = new ArrivalInstruction();

        if($forCommand){
            $fileDirectory = rtrim($this->kernelRootDir.'/../web/'.$photo->getUploadDir(),'/').'/';
        }else{
            $fileDirectory = rtrim($photo->getUploadDir(),'/').'/';
        }

        if(!file_exists($fileDirectory)) mkdir($fileDirectory, 0777);

        return $fileDirectory;
    }

}