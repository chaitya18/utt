<?php

namespace UTT\ReservationBundle\Service;

use UTT\ReservationBundle\Entity\Reservation;

class InsureHubAuthTokenGenerator
{
    public function generateToken($vendorId, $apiKey)
    {
        $formattedVendorId = str_replace('-', '', strtolower($vendorId));
        $formattedApiKey = str_replace('-', '', strtolower($apiKey));
        $date = gmdate('dmY');

        return base64_encode(hash_hmac('sha256', $formattedVendorId.$date, $formattedApiKey, true));
    }
}
class InsureHubOfferingRequestor
{
    public $url;

    public function __construct($offeringRequestUrl) {
        $this->url = $offeringRequestUrl;
    }

    public function requestOffering($request)
    {
        $tokenGenerator = new InsureHubAuthTokenGenerator();
        $authToken = $tokenGenerator->generateToken($request->vendorId, $request->apiKey);

        $ch = curl_init($this->url);

        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($request));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, [
            'Content-Type: application/json',
            'X-InsuranceHub-VendorId: '.$request->vendorId,
            'X-InsuranceHub-AuthToken: '.$authToken]);

        $response = curl_exec($ch);

        curl_close($ch);

        return json_decode($response);
    }
}
class InsureHubRequest implements \JsonSerializable
{
    public $vendorId;
    public $apiKey;
    public $vendorRequestReference;
    public $products = [];
    public $premiumAsSummary = false;

    public function jsonSerialize() {
        return [
            'vendorId' => $this->vendorId,
            'vendorRequestReference' => $this->vendorRequestReference,
            'products' => $this->products,
            'premiumAsSummary' => $this->premiumAsSummary
        ];
    }
}
class InsureHubProduct implements \JsonSerializable
{
    public $categoryCode;
    public $price;
    public $completionDate;

    public function jsonSerialize() {
        return [
            'categoryCode' => $this->categoryCode,
            'price' => $this->price,
            'completionDate' => $this->completionDate->format(\DateTime::ISO8601)
        ];
    }
}

class InsureHubOffering implements \JsonSerializable
{
    public $vendorId;
    public $offeringId;
    public $vendorSaleReference;
    public $customerSurname;
    public $customerForename;
    public $sales = [];

    public function jsonSerialize() {
        return [
            'vendorId' => $this->vendorId,
            'offeringId' => $this->offeringId,
            'vendorSaleReference' => $this->vendorSaleReference,
            'customerSurname' => $this->customerSurname,
            'customerForename' => $this->customerForename,
            'sales' => $this->sales,
        ];
    }
}

class InsureHubOfferingSale implements \JsonSerializable
{
    public $productOfferingId;
    public $Sold = true;

    public function jsonSerialize() {
        return [
            'productOfferingId' => $this->productOfferingId,
            'Sold' => $this->Sold,
        ];
    }
}

class BookingProtectService {
    const testVendorId = '1184c7d94b6a45ed8a283a19f8dd8e5e';
    const testApiKey = '846d6be70c7640e5be48a31a23a0c1a0';
    const testUrlQuote = 'http://uat.quote.insure-hub.net/quote';
    const testUrlSales = 'http://uat.sales.insure-hub.net/sales';

    const liveVendorId = '9dd9626e74c54667bc1d382bd0a94985';
    const liveApiKey = '0b3a8daa2a604fac9654012e00bbf6ab';
    const liveUrlQuote = 'https://quote.insure-hub.net/quote';
    const liveUrlSales = 'https://sales.insure-hub.net/sales';

    protected $bookingProtectLiveMode;
    protected $vendorId;
    protected $apiKey;
    protected $urlQuote;
    protected $urlSales;

    public function __construct($bookingProtectLiveMode){
        $this->bookingProtectLiveMode = $bookingProtectLiveMode;

        if($this->bookingProtectLiveMode){
            $this->vendorId = self::liveVendorId;
            $this->apiKey = self::liveApiKey;
            $this->urlQuote = self::liveUrlQuote;
            $this->urlSales = self::liveUrlSales;
        }else{
            $this->vendorId = self::testVendorId;
            $this->apiKey = self::testApiKey;
            $this->urlQuote = self::testUrlQuote;
            $this->urlSales = self::testUrlSales;
        }
    }

    public function callOffering($total, \DateTime $completionDate){
        $request = new InsureHubRequest();
        $request->vendorId = $this->vendorId;
        $request->apiKey = $this->apiKey;

        $product1 = new InsureHubProduct();
        $product1->categoryCode = 'TKT';
        $product1->price = $total;
        $product1->completionDate = $completionDate;//new \DateTime();
        $request->products = [$product1];

        $offeringRequestUrl = $this->urlQuote;
        $requestor = new InsureHubOfferingRequestor($offeringRequestUrl);
        return $requestor->requestOffering($request);
    }

    public function getBookingProtectPrice($total, \DateTime $fromDate){
        $offering = $this->callOffering($total, $fromDate);
        if(isset($offering->productOfferings)){
            if(isset($offering->productOfferings[0]) && isset($offering->productOfferings[0]->premium)){
                return $offering->productOfferings[0]->premium;
            }
        }

        return -1;
    }

    public function callSale(Reservation $reservation){
        $offering = $this->callOffering((float) $reservation->getTotalPrice() - (float) $reservation->getPriceDecoded()->getBookingProtectCharge(), $reservation->getFromDate());

        $request = new InsureHubOffering();
        $request->vendorId = $this->vendorId;
        $request->apiKey = $this->apiKey;
        $request->offeringId = $offering->id;
        $request->vendorSaleReference = (string) $reservation->getId();
        $request->customerSurname = $reservation->getUserDataDecoded()->getLastName();
        $request->customerForename = $reservation->getUserDataDecoded()->getFirstName();

        foreach($offering->productOfferings as $productOffering){
            $sale = new InsureHubOfferingSale();
            if($reservation->getIsBookingProtect()){
                $sale->Sold = true;
            }else{
                $sale->Sold = false;
            }
            $sale->productOfferingId = $productOffering->id;
            $request->sales[] = $sale;
        }

        $offeringRequestUrl = $this->urlSales;
        $requestor = new InsureHubOfferingRequestor($offeringRequestUrl);
        $requestor->requestOffering($request);

        return true;
    }

}