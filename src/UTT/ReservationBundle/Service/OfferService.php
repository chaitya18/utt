<?php

namespace UTT\ReservationBundle\Service;

use Doctrine\ORM\EntityManager;
use UTT\EstateBundle\Entity\Estate;
use UTT\ReservationBundle\Entity\Offer;
use UTT\ReservationBundle\Entity\OfferRepository;
use \Symfony\Component\Routing\Router;
use UTT\ReservationBundle\Service\PricingService;
use UTT\ReservationBundle\Service\ReservationService;
use UTT\AdminBundle\Service\UTTAclService;
use UTT\IndexBundle\Service\ConfigurationService;
use UTT\IndexBundle\Entity\Configuration;
use UTT\ReservationBundle\Entity\Pricing;

class OfferService {
    protected $_em;
    protected $router;
    protected $pricingService;
    protected $reservationService;
    protected $uttAclService;
    protected $configurationService;

    public function __construct(EntityManager $em, Router $router, PricingService $pricingService, ReservationService $reservationService, UTTAclService $uttAclService, ConfigurationService $configurationService){
        $this->_em = $em;
        $this->router = $router;
        $this->pricingService = $pricingService;
        $this->reservationService = $reservationService;
        $this->uttAclService = $uttAclService;
        $this->configurationService = $configurationService;
    }

    public function create(Estate $estate, $type, \Datetime $validFrom, \Datetime $validTo, $value, $name, $description){
        $offer = new Offer();
        $offer->addEstate($estate);
        $offer->setType($type);
        $offer->setValidFrom($validFrom);
        $offer->setValidTo($validTo);
        $offer->setValue($value);
        $offer->setName($name);
        $offer->setDescription($description);
        $offer->setDescriptionShort($description);

        try{
            $this->_em->persist($offer);
            $this->_em->flush();

            return $offer;
        }catch (\Exception $e){
            return false;
        }
    }

    public function offerResponseArray(Offer $offer, $addAdminUrl = true){
        $response = array(
            'name' => $offer->getName(),
            'validFrom' => $offer->getValidFrom()->format('Y-m-d'),
            'validTo' => $offer->getValidTo()->format('Y-m-d'),
            'value' => $offer->getValue()
        );
        if($addAdminUrl){ $response['url'] = $this->router->generate('admin_utt_reservation_offer_edit', array('id' => $offer->getId()), true); }
        return $response;
    }

    public function clearAutomaticSpecialOffers(Estate $estate){
        /** @var OfferRepository $offerRepository */
        $offerRepository = $this->_em->getRepository('UTTReservationBundle:Offer');
        $offers = $offerRepository->findAutomaticSpecialOffersForEstate($estate);
        if($offers){
            foreach($offers as $offer){
                $this->_em->remove($offer);
            }
            $this->_em->flush();

            return true;
        }

        return false;
    }

    public function updateAutomaticSpecialOffers(Estate $estate){
        $this->clearAutomaticSpecialOffers($estate);
        return true;

        /** @var Configuration $configuration */
        $configuration = $this->configurationService->get();
        if($configuration instanceof Configuration){
            $yearDate = new \DateTime('now');
            $yearDate->setDate($yearDate->format('Y'), $yearDate->format('m'), ($yearDate->format('d') == 1 ? 1 : $yearDate->format('d') -1));
            $yearsInFuture = 1;
            $pricingList = $this->pricingService->getPricingListForEstateForYear($estate, $yearDate, $yearsInFuture);
            if($pricingList){
                $specialOfferPrice = null;
                $specialOfferDateFrom = null;
                $specialOfferDateTo = null;

                if($estate->getType() == Estate::TYPE_FLEXIBLE_BOOKING){
                    $start = new \DateTime( sprintf('%04d-%d-%d', $yearDate->format('Y'), $yearDate->format('m'), $yearDate->format('d')) );
                    if($estate->getDiscountPeriod()){
                        $start->modify('+'.$estate->getDiscountPeriod().' days');
                        $start->modify('+1 day');
                    }
                    $end = new \DateTime( sprintf('%04d-12-31', (int)$yearDate->format('Y')) );
                    if($configuration->getAutomaticSpecialOfferMonths()){
                        $end = clone $start;
                        $end->modify('+'.$configuration->getAutomaticSpecialOfferMonths().' months');
                    }

                    $interval = new \DateInterval('P1D');
                    $globalPeriod   = new \DatePeriod($start, $interval, $end);

                    $periods = array();
                    foreach ($globalPeriod as $globalPeriodDate) {
                        $periodStart = clone $globalPeriodDate;
                        $periodEnd = clone $globalPeriodDate;
                        $periodEnd->modify('+2 days'); // find only in 2 nights periods - because 2 nights are always the cheapest

                        if($periodStart <= new \DateTime('now')) continue;
                        if($periodEnd >= $end) continue;

                        $periodInterval = new \DateInterval('P1D');
                        $period = new \DatePeriod($periodStart, $periodInterval, $periodEnd);

                        $periods[] = $period;
                    }

                    /** @var \DatePeriod $period */
                    foreach($periods as $period){
                        if($this->reservationService->isAvailableForBooking($estate, $period->start->setTime(0,0,0), $period->end->setTime(0,0,0))){
                            $basePrice = $this->pricingService->flexiblePeriodBasePrice($period, $pricingList);
                            if($basePrice != -1){
                                if(is_null($specialOfferPrice)){
                                    $specialOfferPrice = $basePrice;
                                    $specialOfferDateFrom = $period->start;
                                    $specialOfferDateTo = $period->end;
                                }

                                if($basePrice < $specialOfferPrice){
                                    $specialOfferPrice = $basePrice;
                                    $specialOfferDateFrom = $period->start;
                                    $specialOfferDateTo = $period->end;
                                }
                            }
                        }
                    }
                }elseif($estate->getType() == Estate::TYPE_STANDARD_M_F){
                    /** @var Pricing $startPricing */
                    $startPricing = $pricingList[0];
                    if($this->reservationService->isAvailableForBooking($estate, $startPricing->getFromDate(), $startPricing->getToDate())){
                        $specialOfferPrice = $startPricing->getPrice();
                        $specialOfferDateFrom = $startPricing->getFromDate();
                        $specialOfferDateTo = $startPricing->getToDate();
                    }

                    /** @var Pricing $pricing */
                    foreach($pricingList as $pricing){
                        if($this->reservationService->isAvailableForBooking($estate, $pricing->getFromDate(), $pricing->getToDate())){
                            if($pricing->getPrice() < $specialOfferPrice){
                                $specialOfferPrice = $pricing->getPrice();
                                $specialOfferDateFrom = $pricing->getFromDate();
                                $specialOfferDateTo = $pricing->getToDate();
                            }
                        }
                    }
                }

                if(!is_null(($specialOfferPrice)) && $specialOfferDateFrom instanceof \DateTime && $specialOfferDateTo instanceof \DateTime){
                    $newOffer = $this->create(
                        $estate,
                        Offer::TYPE_AUTOMATIC_SPECIAL_OFFER,
                        $specialOfferDateFrom->setTime(0,0,0),
                        $specialOfferDateTo->setTime(0,0,0),
                        $specialOfferPrice,
                        'Special offer!',
                        'The cheapest holiday in '.$estate->getName()
                    );
                    if($newOffer instanceof Offer){
                        $this->uttAclService->updateObjectAcl($newOffer);

                        return true;
                    }
                }
            }
        }

        return false;
    }

    public function updateLateAvailabilityOffers(Estate $estate){
        /** @var OfferRepository $offerRepository */
        $offerRepository = $this->_em->getRepository('UTTReservationBundle:Offer');
        $offers = $offerRepository->findLateAvailabilityOffersForEstate($estate);
        if($offers){
            foreach($offers as $offer){
                $this->_em->remove($offer);
            }
            $this->_em->flush();
        }

        $estateDiscountPeriod = $estate->getDiscountPeriod();//6;
        $estateDiscountValue = $estate->getDiscountValue();//15

        if($estateDiscountPeriod > 0 && $estateDiscountValue){
            $maxEnd = new \DateTime('now');
            $maxEnd->modify('+'.$estateDiscountPeriod.' days');

            $nightsArray = false;
            if($estate->getType() == Estate::TYPE_FLEXIBLE_BOOKING){
                $nightsArray = array(2,3,4,5,6,7,8,9,10,11,12,13,14);
            }elseif($estate->getType() == Estate::TYPE_STANDARD_M_F){
                $nightsArray = array(3,4,7,2);
            }

            if(is_array($nightsArray) && count($nightsArray) > 0){
                $globStart = new \DateTime('now');
                //$globStart->setTime(0,0,0);
                //$globStart->modify('-1 day');
                $globEnd = clone $globStart;
                $globEnd->modify('+'.$estateDiscountPeriod.' days');
                $interval = new \DateInterval('P1D');
                $period   = new \DatePeriod($globStart, $interval, $globEnd);

                $standardCreatedOfferDates = array();

                /** @var \DateTime $date */
                foreach($period as $date){
                    foreach($nightsArray as $value){
                        if($estate->getType() == Estate::TYPE_STANDARD_M_F && $value == 2){
                            $continue = false;
                            foreach($standardCreatedOfferDates as $standardCreatedOfferDate){
                                if($date >= $standardCreatedOfferDate->from && $date <= $standardCreatedOfferDate->to){
                                    $continue = true;
                                    break;
                                }
                            }
                            if($continue) continue;
                        }

                        $startDate = clone $date;
                        if($estate->getType() == Estate::TYPE_STANDARD_M_F) $startDate->setTime(0,0,0);
                        $endDate = clone $startDate;
                        $endDate->modify('+'.$value.' days');
                        if($estate->getType() == Estate::TYPE_STANDARD_M_F) $endDate->setTime(0,0,0);
                        if($endDate <= $maxEnd){
                            if($this->reservationService->isAvailableForBooking($estate, $startDate->setTime(0,0,0), $endDate->setTime(0,0,0))){
                                $basePrice = $this->pricingService->getBasePrice($estate, $startDate, $endDate);
                                if($basePrice != -1){
                                    $offerValue = $basePrice - $basePrice * ($estateDiscountValue) / 100;
                                    $offerValue = round($offerValue, 2);

                                    if($estate->getType() == Estate::TYPE_STANDARD_M_F) {
                                        print_r($startDate);
                                        print_r($endDate);
                                        print_r($startDate->diff($endDate));
                                        print_r($offerValue);
                                        echo '-==========';
                                        //continue;
                                    }

                                    $newOffer = $this->create($estate, Offer::TYPE_LATE_AVAILABILITY, $startDate->setTime(0,0,0), $endDate->setTime(0,0,0), $offerValue, 'Late availability - '.$value.' days', 'Late availability - '.$value.' days for '.$estate->getName().' estate.');
                                    if($newOffer instanceof Offer){
                                        if($estate->getType() == Estate::TYPE_STANDARD_M_F && $value > 2){
                                            $standardCreatedOfferDates[] = (object) array( 'from' => $newOffer->getValidFrom(), 'to' => $newOffer->getValidTo() );
                                        }
                                        $this->uttAclService->updateObjectAcl($newOffer);
                                    }
                                }
                            }
                        }
                    }
                }
            }

            return true;
        }

        return false;
    }
}