<?php

namespace UTT\ReservationBundle\Service;

use Doctrine\ORM\EntityManager;
use UTT\IndexBundle\Service\ConfigurationService;
use UTT\ReservationBundle\Entity\DiscountCode;
use UTT\ReservationBundle\Entity\DiscountCodeRepository;
use UTT\ReservationBundle\Model\ReservationDataPrice;
use UTT\ReservationBundle\Model\ReservationDataDiscountPrice;
use UTT\EstateBundle\Entity\Estate;
use UTT\EstateBundle\Entity\EstateRepository;
use UTT\ReservationBundle\Entity\Reservation;
use UTT\ReservationBundle\Entity\Voucher;

class DiscountCodeService {
    protected $_em;
    protected $configurationService;

    public function __construct(EntityManager $em, ConfigurationService $configurationService){
        $this->_em = $em;
        $this->configurationService = $configurationService;
    }

    public function createRefundCodeAfterCancel(Reservation $reservation, $refund){
        $startBy = new \DateTime('now');
        $startBy->modify('+1 year');

        $expiresAt = new \DateTime('now');
        $expiresAt->modify('+10 days');

        $discountCode = new DiscountCode();
        $discountCode->addEstate($reservation->getEstate());
        $discountCode->setCode('RB-'.$reservation->getId().'-'.uniqid());
        $discountCode->setCodeType(DiscountCode::CODE_TYPE_ONE_OFF_FIXED_AMOUNT_DISCOUNT);
        //$expiresAt = clone $reservation->getFromDate();
        $discountCode->setExpiresAt($expiresAt);
        $discountCode->setHolidayMustStartAt(null);
        $discountCode->setHolidayMustStartBy($startBy);
        $discountCode->setValue($refund);

        $this->_em->persist($discountCode);
        $this->_em->flush();

        return $discountCode;
    }

    public function createRefundVoucherCode(Reservation $reservation, $refund) {

        $expiresAt = new \DateTime('now');
        $expiresAt->modify('last day of december this year');

        $discountCode = new DiscountCode();
        $discountCode->setIsAllEstates(1);
        $discountCode->setCode('VO-'.uniqid());
        $discountCode->setCodeType(DiscountCode::CODE_TYPE_PAYMENT_VOUCHER);

        $discountCode->setExpiresAt($expiresAt);
        $discountCode->setHolidayMustStartAt(null);
        $discountCode->setHolidayMustStartBy(null);
        $discountCode->setValue($refund);
        $discountCode->setEmails($reservation->getUser()->getEmailCanonical());
        $discountCode->setReason('Refund after payed by voucher - booking - '.$reservation->getId());

        $this->_em->persist($discountCode);
        $this->_em->flush();

        return $discountCode;
    }

    public function createReportUserSessionDiscount(array $estates){
        $startBy = new \DateTime('now');
        $startBy->modify('+1 year');
        $expiresAt = new \DateTime('now');
        $expiresAt->modify('+3 days');

        $discountCode = new DiscountCode();
        if($estates){
            /** @var Estate $estate */
            foreach($estates as $estate){
                if($estate instanceof Estate){
                    $discountCode->addEstate($estate);
                }else{
                    return false;
                }
            }
        }
        $discountCode->setCode('AR-'.uniqid());
        $discountCode->setCodeType(DiscountCode::CODE_TYPE_ONE_OFF_FIXED_AMOUNT_DISCOUNT);
        $discountCode->setExpiresAt($expiresAt);
        $discountCode->setHolidayMustStartAt(null);
        $discountCode->setHolidayMustStartBy($startBy);
        $discountCode->setValue(10);

        $this->_em->persist($discountCode);
        $this->_em->flush();

        return $discountCode;
    }

    public function createBookingReviewDiscount(){
        $startBy = new \DateTime('now');
        $startBy->modify('+1 year');
        $expiresAt = new \DateTime('now');
        $expiresAt->modify('+14 days');

        $discountCode = new DiscountCode();
        $discountCode->setIsAllEstates(true);
        $discountCode->setCode('REV-'.uniqid());
        $discountCode->setCodeType(DiscountCode::CODE_TYPE_ONE_OFF_FIXED_AMOUNT_DISCOUNT);
        $discountCode->setExpiresAt($expiresAt);
        $discountCode->setHolidayMustStartAt(null);
        $discountCode->setHolidayMustStartBy($startBy);
        $discountCode->setValue(20);

        $this->_em->persist($discountCode);
        $this->_em->flush();

        return $discountCode;
    }

    public function createVoucherDiscount(Voucher $voucher){
        $discountCode = new DiscountCode();
        $discountCode->setIsAllEstates(true);
        $discountCode->setCode('VO-'.uniqid());
        $discountCode->setCodeType(DiscountCode::CODE_TYPE_PAYMENT_VOUCHER);
        $discountCode->setExpiresAt($voucher->getExpiresAt());
        $discountCode->setHolidayMustStartAt(null);
        $discountCode->setHolidayMustStartBy($voucher->getExpiresAt());
        $discountCode->setValue($voucher->getValue());

        $this->_em->persist($discountCode);
        $this->_em->flush();

        return $discountCode;
    }

    private function getTotalForDiscountPrice(ReservationDataDiscountPrice $discountPrice){
        $total = $discountPrice->getDiscountCodePrice();
        return (float) $total;
    }

    public function getDiscountCode($estate, \Datetime $fromDate, \Datetime $toDate, $code){
        /** @var DiscountCodeRepository $discountCodeRepository */
        $discountCodeRepository = $this->_em->getRepository('UTTReservationBundle:DiscountCode');

        /** @var DiscountCode $discountCode */
        $discountCode = $discountCodeRepository->findOneActiveByEstateAndCode($estate, $fromDate, $toDate, $code);
        if($discountCode instanceof DiscountCode){
            return $discountCode;
        }
        return false;
    }

    public function getDiscountPriceForParams(ReservationDataPrice $price, Estate $estate, \Datetime $fromDate, \Datetime $toDate, $code){
        $estateDiscountPeriod = $estate->getDiscountPeriod();//6
        $estateDiscountValue = $estate->getDiscountValue();//15

        if($estateDiscountPeriod > 0 && $estateDiscountValue) {
            $maxEnd = new \DateTime('now');
            $maxEnd->modify('+' . $estateDiscountPeriod . ' days');

            if($fromDate <= $maxEnd && $toDate <= $maxEnd){
                return false;
            }
        }

            /** @var DiscountCode $discountCode */
        $discountCode = $this->getDiscountCode($estate, $fromDate, $toDate, $code);
        if($discountCode){
            $discountPrice = new ReservationDataDiscountPrice();

            switch ($discountCode->getCodeType()) {
                case DiscountCode::CODE_TYPE_ONE_OFF_FIXED_AMOUNT_DISCOUNT:
                    $discountPrice->setDiscountCodePrice($discountCode->getValue());
                    break;
                case DiscountCode::CODE_TYPE_PERSISTENT_FIXED_AMOUNT:
                    $discountPrice->setDiscountCodePrice($discountCode->getValue());
                    break;
                case DiscountCode::CODE_TYPE_ONE_OFF_PERCENTAGE:
                    $discountPrice->setDiscountCodePrice((float)($price->getBasePrice() * $discountCode->getValue()) / 100);
                    break;
                case DiscountCode::CODE_TYPE_PERSISTENT_PERCENTAGE:
                    $discountPrice->setDiscountCodePrice((float)($price->getBasePrice() * $discountCode->getValue()) / 100);
                    break;
                case DiscountCode::CODE_TYPE_PAYMENT_VOUCHER:
                    return false;
                    break;
                default:
                    $discountPrice->setDiscountCodePrice(0);
            }

            $discountPrice->setDiscountCode($code);
            $discountPrice->setTotal($this->getTotalForDiscountPrice($discountPrice));
            return $discountPrice;
        }

        return false;
    }

    public function useDiscountCode(DiscountCode $discountCode){
        if(
            $discountCode->getCodeType() == DiscountCode::CODE_TYPE_ONE_OFF_FIXED_AMOUNT_DISCOUNT ||
            $discountCode->getCodeType() == DiscountCode::CODE_TYPE_ONE_OFF_PERCENTAGE ||
            $discountCode->getCodeType() == DiscountCode::CODE_TYPE_PAYMENT_VOUCHER
        ){
            $discountCode->setIsActive(false);
            $discountCode->setIsRedeemed(true);

            $this->_em->persist($discountCode);
            $this->_em->flush();
        }

        return true;
    }

    public function remove(DiscountCode $discountCode){
        $this->_em->remove($discountCode);
        $this->_em->flush();

        return true;
    }
}