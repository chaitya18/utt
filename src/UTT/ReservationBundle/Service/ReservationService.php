<?php

namespace UTT\ReservationBundle\Service;

use Doctrine\ORM\EntityManager;
use UTT\EstateBundle\Entity\Estate;
use UTT\IndexBundle\Entity\Email;
use UTT\ReservationBundle\Entity\ReservationRepository;
use UTT\ReservationBundle\Service\DiscountCodeService;
use UTT\ReservationBundle\Service\PricingService;
use UTT\ReservationBundle\Entity\Reservation;
use Symfony\Component\HttpFoundation\Session\Session;
use UTT\ReservationBundle\Model\ReservationData;
use UTT\ReservationBundle\Model\ReservationChangeData;
use UTT\ReservationBundle\Model\ReservationDataPrice;
use UTT\ReservationBundle\Model\ReservationDataDiscountPrice;
use UTT\ReservationBundle\Model\ReservationDataUserData;
use UTT\UserBundle\Entity\User;
use UTT\ReservationBundle\Entity\DiscountCode;

use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\Encoder\XmlEncoder;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\GetSetMethodNormalizer;
use UTT\ReservationBundle\Model\ReservationDataEstateData;
use UTT\ReservationBundle\Factory\ReservationFactory;
use UTT\IndexBundle\Service\EmailService;
use UTT\ReservationBundle\Service\HistoryEntryService;
use UTT\ReservationBundle\Entity\HistoryEntry;
use UTT\IndexBundle\Entity\Configuration;
use \Symfony\Component\Routing\Router;
use UTT\AdminBundle\Service\UTTAclService;
use UTT\ReservationBundle\Entity\Voucher;
use UTT\ReservationBundle\Service\BookingProtectService;

class ReservationService {
    protected $_em;
    protected $pricingService;
    protected $discountCodeService;
    protected $session;
    protected $reservationFactory;
    protected $emailService;
    protected $historyEntryService;
    protected $router;
    protected $uttAclService;
    protected $bookingProtectService;
    protected $voucherService;

    public function __construct(EntityManager $em, PricingService $pricingService, DiscountCodeService $discountCodeService, Session $session, ReservationFactory $reservationFactory, EmailService $emailService, HistoryEntryService $historyEntryService, Router $router, UTTAclService $uttAclService, BookingProtectService $bookingProtectService, VoucherService $voucherService){
        $this->_em = $em;
        $this->pricingService = $pricingService;
        $this->discountCodeService = $discountCodeService;
        $this->session = $session;
        $this->reservationFactory = $reservationFactory;
        $this->emailService = $emailService;
        $this->historyEntryService = $historyEntryService;
        $this->router = $router;
        $this->uttAclService = $uttAclService;
        $this->bookingProtectService = $bookingProtectService;
        $this->voucherService = $voucherService;
    }

    public function referredFlagChange(Reservation $reservation, User $user, $referredFlag, $emailReason = false){
        if(!(array_key_exists($referredFlag, $reservation->getAllowedReferredFlags()) || $referredFlag == null)){ return false; }

        $reservation->setReferredFlag($referredFlag);
        $reservation->setReferredFlagDate(new \Datetime('now'));

        try{
            $this->_em->persist($reservation);
            $this->_em->flush();

            if($reservation->getReferredFlag()){
                $historyEntryComment = $this->historyEntryService->newCommentReferredFlagChanged($reservation->getReferredFlagName());
            }else{
                $historyEntryComment = $this->historyEntryService->newCommentReferredFlagCleared();
            }
            $newEntry = $this->historyEntryService->newEntry(HistoryEntry::TYPE_REFERRED_FLAG_CHANGED, $reservation, $user, $historyEntryComment);
            if($newEntry instanceof HistoryEntry){
                $this->emailService->sendReferredFlagChangedEmail($reservation, $emailReason);
            }

            return true;
        }catch (\Exception $e){
            return false;
        }
    }

    public function assignNewUserData(Reservation $reservation, ReservationDataUserData $userData){
        // serializer init
        $encoders = array(new XmlEncoder(), new JsonEncoder());
        $normalizers = array(new GetSetMethodNormalizer());
        $serializer = new Serializer($normalizers, $encoders);

        $reservation->setUserData($serializer->serialize($userData, 'json'));

        try{
            $this->_em->persist($reservation);
            $this->_em->flush();

            return true;
        }catch (\Exception $e){

            return false;
        }
    }

    public function change(Reservation $reservation, User $user,  \Datetime $fromDate, \Datetime $toDate, $sleeps, $sleepsChildren, $pets, $infants, ReservationDataPrice $price, ReservationDataUserData $userData){
        if(!$this->isAvailableForBooking($reservation->getEstate(), $fromDate, $toDate, $reservation->getId())) return false;

        if($user->getId() == $reservation->getUser()->getId()){
            // serializer init
            $encoders = array(new XmlEncoder(), new JsonEncoder());
            $normalizers = array(new GetSetMethodNormalizer());
            $serializer = new Serializer($normalizers, $encoders);

            // reservation create
            $reservation->setFromDate($fromDate);
            $reservation->setToDate($toDate);
            //$reservation->setStatus(Reservation::STATUS_RESERVED);
            $reservation->setSleeps($sleeps);
            $reservation->setSleepsChildren($sleepsChildren);
            $reservation->setPets($pets);
            $reservation->setInfants($infants);

            $reservation->setTotalPrice((float)$price->getTotal());

            $reservation->setPrice($serializer->serialize($price, 'json'));
            $reservation->setUserData($serializer->serialize($userData, 'json'));
            $reservation->setDiscountPrice($serializer->serialize(new ReservationDataDiscountPrice(), 'json'));

            $reservation = $this->reservationFactory->assignDeposit($reservation);
            $this->updateBalanceDueDate($reservation);

            $reservation = $this->reservationFactory->updateReservationStatusAfterChanges($reservation);

            $reservation->setCommissionValue($reservation->calculateCommissionValue());

            try{
                $this->_em->persist($reservation);
                $this->_em->flush();

                $historyEntryComment = $this->historyEntryService->newCommentChangeBooking($reservation->getEstate()->getName(), $reservation->getFromDate(), $reservation->getToDate(), $reservation->getSleeps(), $reservation->getInfants(), $reservation->getPets(), $reservation->getTotalPrice());
                $newEntry = $this->historyEntryService->newEntry(HistoryEntry::TYPE_SELF_SERVICE_HOLIDAY_OPTIONS_CHANGED_BY_GUEST, $reservation, $user, $historyEntryComment);
                if($newEntry instanceof HistoryEntry){
                    $this->emailService->sendHistoryEmail($reservation, $newEntry);
                }

                if($reservation->getStatus() == Reservation::STATUS_PART_PAID || $reservation->getStatus() == Reservation::STATUS_FULLY_PAID){
                    $this->emailService->sendForLandlord($reservation, $historyEntryComment);
                }

                $this->removeReservationData();
                return true;
            }catch (\Exception $e){
                $this->removeReservationData();
                return false;
            }
        }

        return false;
    }

    public function requote(Reservation $reservation, Estate $estate, \Datetime $fromDate, \Datetime $toDate, $sleeps, $sleepsChildren, $pets, $infants, ReservationDataPrice $price, ReservationDataDiscountPrice $discountPrice, $sendHistoryEmail, User $user){
        if(!$this->isAvailableForBooking($estate, $fromDate, $toDate, $reservation->getId())) return false;

        $prevReservation = clone $reservation;
        $changeEstate = false;
        // serializer init
        $encoders = array(new XmlEncoder(), new JsonEncoder());
        $normalizers = array(new GetSetMethodNormalizer());
        $serializer = new Serializer($normalizers, $encoders);

        $reservation->setEstate($estate);
        if($reservation->getEstate()->getId() !== $prevReservation->getEstate()->getId()) {
            $changeEstate = true;
        }
        $reservation->setFromDate($fromDate);
        $reservation->setToDate($toDate);
        $reservation->setSleeps($sleeps);
        $reservation->setSleepsChildren($sleepsChildren);
        $reservation->setPets($pets);
        $reservation->setInfants($infants);

        $reservation->setTotalPrice((float)$price->getTotal() - (float)$discountPrice->getTotal());

        $reservation->setPrice($serializer->serialize($price, 'json'));
        $reservation->setDiscountPrice($serializer->serialize($discountPrice, 'json'));

        $estateData = new \StdClass();
        $estateData->name = $estate->getName();
        $reservation->setEstateData($serializer->serialize(new ReservationDataEstateData($estateData), 'json'));

        $reservation = $this->reservationFactory->assignDeposit($reservation);
        $this->updateBalanceDueDate($reservation);

        $reservation = $this->reservationFactory->updateReservationStatusAfterChanges($reservation);

        $reservation->setCommissionValue($reservation->calculateCommissionValue());

        try{
            $this->_em->persist($reservation);
            $this->_em->flush();

            $historyEntryComment = $this->historyEntryService->newCommentRequote($reservation->getEstate()->getName(), $reservation->getFromDate(), $reservation->getToDate(), $reservation->getSleeps(), $reservation->getInfants(), $reservation->getPets(), $reservation->getTotalPrice());
            $newEntry = $this->historyEntryService->newEntry(HistoryEntry::TYPE_HOLIDAY_CHANGED_REQUOTE, $reservation, $user, $historyEntryComment);
            if($sendHistoryEmail == true && $newEntry instanceof HistoryEntry){
                $this->emailService->sendHistoryEmail($reservation, $newEntry);
            }

            if($reservation->getStatus() == Reservation::STATUS_PART_PAID || $reservation->getStatus() == Reservation::STATUS_FULLY_PAID){
                $this->emailService->sendForLandlord($reservation, $historyEntryComment);
                if($changeEstate) {
                    $this->emailService->sendForLandlord($reservation, false, Email::TYPE_LANDLORD_BOOKING_CHANGE_ESTATE, $prevReservation->getEstate());
                }
            }

            return true;
        }catch (\Exception $e){
            return false;
        }
    }

    protected function updateBalanceDueDate($reservation) {
        $today = new \DateTime('now');
        $interval = $today->diff($reservation->getBalanceDueDate());
        $days = (int)$interval->format('%R%a days');
        if($days <= 0)  {
            $balanceDueDate = $today->modify('+1 day');
            $reservation->setBalanceDueDate( $balanceDueDate);
        }
    }

    public function reservationResponseArray(Reservation $reservation){
        return array(
            'id' => $reservation->getId(),
            'fromDate' => $reservation->getFromDate()->format('Y-m-d'),
            'toDate' => $reservation->getToDate()->format('Y-m-d'),
            'url' => $this->router->generate('admin_utt_reservation_reservation_show', array('id' => $reservation->getId()), true),
            'status' => $reservation->getStatusName(),
            'userData' => array(
                'email' => $reservation->getUserDataDecoded()->email,
                //'name' => str_replace("'", ' ', $reservation->getUserDataDecoded()->firstName.' '.$reservation->getUserDataDecoded()->lastName)
                'name' => ''
            )
        );
    }

    public function telephoneBooking(Estate $estate, User $user, \Datetime $fromDate, \Datetime $toDate, ReservationDataPrice $price, ReservationDataUserData $userData, $isFullyPaid = false, $manager = null){
        if(!$this->isAvailableForBooking($estate, $fromDate, $toDate)) return false;

        $reservation = $this->reservationFactory->create($estate, $user, $fromDate, $toDate, $estate->getMinimumGuests(), 0, 0, 0, $price, $userData, new ReservationDataDiscountPrice());
        $reservation->setStatus(Reservation::STATUS_RESERVED);
        if($manager) {
            $reservation->setManagerId($manager->getId());
        }

        if($isFullyPaid) $reservation->setStatus(Reservation::STATUS_FULLY_PAID);

        $reservation = $this->reservationFactory->assignDeposit($reservation);

        try{
            $this->_em->persist($reservation);
            $this->_em->flush();

            $this->emailService->sendReservationConfirmation($reservation);

            $historyEntryComment = $this->historyEntryService->newCommentNewTelephoneBooking($reservation->getEstate()->getName(), $reservation->getFromDate(), $reservation->getToDate(), $reservation->getSleeps(), $reservation->getInfants(), $reservation->getPets(), $reservation->getTotalPrice());
            $this->historyEntryService->newEntry(HistoryEntry::TYPE_NEW_TELEPHONE_BOOKING, $reservation, $user, $historyEntryComment);

//            if($reservation->getStatus() == Reservation::STATUS_PART_PAID || $reservation->getStatus() == Reservation::STATUS_FULLY_PAID){
//                $this->emailService->sendForLandlord($reservation, $historyEntryComment);
//            }

            $this->uttAclService->updateObjectAcl($reservation);

            return $reservation;
        }catch (\Exception $e){
            return false;
        }
    }

    public function grouponBooking(Estate $estate, User $user, \Datetime $fromDate, \Datetime $toDate, ReservationDataUserData $userData, $manager = null){
        if(!$this->isAvailableForBooking($estate, $fromDate, $toDate)) return false;

        $reservation = $this->reservationFactory->create($estate, $user, $fromDate, $toDate, $estate->getMinimumGuests(), 0, 0, 0, new ReservationDataPrice(), $userData, new ReservationDataDiscountPrice());
        $reservation->setStatus(Reservation::STATUS_FULLY_PAID);
        if($manager) {
            $reservation->setManagerId($manager->getId());
        }

        try{
            $this->_em->persist($reservation);
            $this->_em->flush();

            $historyEntryComment = $this->historyEntryService->newCommentNewGrouponBooking($reservation->getEstate()->getName(), $reservation->getFromDate(), $reservation->getToDate());
            $this->historyEntryService->newEntry(HistoryEntry::TYPE_NEW_GROUPON_BOOKING, $reservation, $user, $historyEntryComment);

            $this->emailService->sendForLandlord($reservation, $historyEntryComment);
//            if($reservation->getStatus() == Reservation::STATUS_PART_PAID || $reservation->getStatus() == Reservation::STATUS_FULLY_PAID){
//                $this->emailService->sendForLandlord($reservation, $historyEntryComment);
//            }

            $this->uttAclService->updateObjectAcl($reservation);

            return $reservation;
        }catch (\Exception $e){
            return false;
        }
    }

    public function airbnbBooking(Estate $estate, User $user, \Datetime $fromDate, \Datetime $toDate, $reason){
        if(!$this->isAvailableForBooking($estate, $fromDate, $toDate)) return false;

        $reservation = $this->reservationFactory->create($estate, $user, $fromDate, $toDate, $estate->getMinimumGuests(), 0, 0, 0, new ReservationDataPrice(), new ReservationDataUserData(), new ReservationDataDiscountPrice());
        $reservation->setStatus(Reservation::STATUS_BLOCKED_AIRBNB);
        $reservation->setNote($reason);

        try{
            $this->_em->persist($reservation);
            $this->_em->flush();

            $historyEntryComment = $this->historyEntryService->newCommentNewAirbnbBooking($reservation->getEstate()->getName(), $reservation->getFromDate(), $reservation->getToDate());
            $this->historyEntryService->newEntry(HistoryEntry::TYPE_NEW_AIRBNB_BOOKING, $reservation, $user, $historyEntryComment);

            $this->emailService->sendForLandlord($reservation, $historyEntryComment);

            $this->uttAclService->updateObjectAcl($reservation);

            return $reservation;
        }catch (\Exception $e){
            return false;
        }
    }

    public function airbnbBookingUpdate(Reservation $reservation, \Datetime $fromDate, \Datetime $toDate, $sleeps){
        if(!$this->isAvailableForBooking($reservation->getEstate(), $fromDate, $toDate, $reservation->getId())) return false;

        $reservation->setFromDate($fromDate);
        $reservation->setToDate($toDate);
        $reservation->setSleeps($sleeps);

        $this->_em->flush();

        $historyEntryComment = $this->historyEntryService->newCommentAirbnbBookingUpdated($reservation->getEstate()->getName(), $reservation->getFromDate(), $reservation->getToDate(), $reservation->getSleeps());
        $this->historyEntryService->newEntry(HistoryEntry::TYPE_AIRBNB_BOOKING_CHANGED, $reservation, $reservation->getUser(), $historyEntryComment);

        $nowDate = new \DateTime('now');
        if($reservation->getFromDate() > $nowDate){
            $this->emailService->sendForLandlord($reservation, $historyEntryComment);
        }

        return $reservation;
    }

    public function bookBlocked(Estate $estate, User $user, \Datetime $fromDate, \Datetime $toDate, $status, $reason, $sleeps, $isSendForLandlord = true){
        if(!$this->isAvailableForBooking($estate, $fromDate, $toDate)) return false;

        $reservation = $this->reservationFactory->create($estate, $user, $fromDate, $toDate, $sleeps, 0, 0, 0, new ReservationDataPrice(), new ReservationDataUserData(), new ReservationDataDiscountPrice());
        $reservation->setStatus($status);
        $reservation->setNote($reason);

        try{
            $this->_em->persist($reservation);
            $this->_em->flush();

            $historyEntryComment = $this->historyEntryService->newCommentNewBlockBooking($reservation->getEstate()->getName(), $reservation->getFromDate(), $reservation->getToDate());
            $this->historyEntryService->newEntry(HistoryEntry::TYPE_NEW_BLOCK_BOOKING, $reservation, $user, $historyEntryComment);

            if($isSendForLandlord) $this->emailService->sendForLandlord($reservation, $historyEntryComment);
//            if($reservation->getStatus() == Reservation::STATUS_PART_PAID || $reservation->getStatus() == Reservation::STATUS_FULLY_PAID){
//                $this->emailService->sendForLandlord($reservation, $historyEntryComment);
//            }

            $this->uttAclService->updateObjectAcl($reservation);

            return $reservation;
        }catch (\Exception $e){
            throw $e;
        }
    }

    public function book(Estate $estate, User $user, \Datetime $fromDate, \Datetime $toDate, $sleeps, $sleepsChildren, $pets, $infants, ReservationDataPrice $price, ReservationDataUserData $userData, ReservationDataDiscountPrice $discountPrice, $code, $isBookingProtect){
        $reservation = $this->reservationFactory->create($estate, $user, $fromDate, $toDate, $sleeps, $sleepsChildren, $pets, $infants, $price, $userData, $discountPrice);
        $reservation->setStatus(Reservation::STATUS_RESERVED);
        $reservation->setSessionId($this->session->getId());
        $reservation->setIsBookingProtect($isBookingProtect);

        $reservation = $this->reservationFactory->assignDeposit($reservation);

        try{
            $this->_em->persist($reservation);
            $this->_em->flush();

            if($code){
                /** @var DiscountCode $discountCode */
                $discountCode = $this->discountCodeService->getDiscountCode($estate, $fromDate, $toDate, $code);
                if($discountCode instanceof DiscountCode){
                    $this->discountCodeService->useDiscountCode($discountCode);
                }
            }

            $this->emailService->sendReservationConfirmation($reservation);

            $historyEntryComment = $this->historyEntryService->newCommentNewBooking($reservation->getEstate()->getName(), $reservation->getFromDate(), $reservation->getToDate(), $reservation->getSleeps(), $reservation->getInfants(), $reservation->getPets(), $reservation->getTotalPrice());
            $this->historyEntryService->newEntry(HistoryEntry::TYPE_NEW_BOOKING, $reservation, $user, $historyEntryComment);

            $this->uttAclService->updateObjectAcl($reservation);

            $this->removeReservationData();

            try{
                $this->bookingProtectService->callSale($reservation);
            }catch (\Exception $e){}

            if($reservation->getIsBookingProtect()){
                $this->emailService->sendReservationBookingProtectEmail($reservation);
            }

            return $reservation;
        }catch (\Exception $e){
            $this->removeReservationData();
            return false;
        }
    }

    public function reinstate(Reservation $reservation, User $user){
        if($this->isAvailableForBooking($reservation->getEstate(), $reservation->getFromDate(), $reservation->getToDate())) {
            try {
                $reservation->setStatus(Reservation::STATUS_RESERVED);
                $reservation = $this->reservationFactory->reservationStatusUpdate($reservation);

                $this->_em->persist($reservation);
                $this->_em->flush();

                $historyEntryComment = $this->historyEntryService->newCommentReinstate();
                $newEntry = $this->historyEntryService->newEntry(HistoryEntry::TYPE_WE_HAVE_REINSTATED_YOUR_HOLIDAY, $reservation, $user, $historyEntryComment);
                if($newEntry instanceof HistoryEntry){
                    $this->emailService->sendHistoryEmail($reservation, $newEntry);
                }
                if($reservation->getStatus() == Reservation::STATUS_PART_PAID || $reservation->getStatus() == Reservation::STATUS_FULLY_PAID){
                    $this->emailService->sendForLandlord($reservation, $historyEntryComment);
                }

                return true;
            }catch (\Exception $e){
                return false;
            }
        }

        return false;
    }

    public function holdForPayment(Reservation $reservation, $user, $hours, $reason){
        $historyEntryComment = $this->historyEntryService->newCommentHoldForPayment($hours, $reason);
        $newEntry = $this->historyEntryService->newEntry(HistoryEntry::TYPE_HOLD_FOR_PAYMENT, $reservation, $user, $historyEntryComment);
        if($newEntry instanceof HistoryEntry){
            $nowDate = new \DateTime('now');
            $nowDate->modify('+'.$hours.' hours');
            $reservation->setHoldingForPaymentUntilDate($nowDate);
            $reservation->setIsHoldingForPayment(true);

            $this->_em->persist($reservation);
            $this->_em->flush();

            return true;
        }

        return false;
    }

## reservation guest new comment - START
    public function newHistoryCommentFromGuest($type, $reservation, $user, $historyEntryComment, $sendHistoryEmail = false, $sendForLandlord = false){
        if($historyEntryComment == ''){ return false; }

        try{
            $historyEntryComment = 'Guest Note: '.$historyEntryComment;

            $newEntry = $this->historyEntryService->newEntry($type, $reservation, $user, $historyEntryComment);
            if($sendHistoryEmail == true && $newEntry instanceof HistoryEntry && $reservation instanceof Reservation){
                $this->emailService->sendHistoryEmail($reservation, $newEntry);

                if($sendForLandlord == true){
                    $this->emailService->sendForLandlord($reservation, $historyEntryComment);
                }

                $this->referredFlagChange($reservation, $reservation->getUser(), Reservation::REFERRED_FLAG_ADMIN, $historyEntryComment);
            }

            return true;
        }catch (\Exception $e){
            return false;
        }
    }

    public function newHistoryCommentFromGuestToAgent($reservation, $user, $historyEntryComment){
        return $this->newHistoryCommentFromGuest(HistoryEntry::TYPE_SELF_SERVICE_GUEST_NOTE, $reservation, $user, $historyEntryComment, true, true);
    }

    public function newHistoryCommentFromGuestToOwner($reservation, $user, $historyEntryComment){
        return $this->newHistoryCommentFromGuest(HistoryEntry::TYPE_SELF_SERVICE_GUEST_NOTE, $reservation, $user, $historyEntryComment, true, true);
    }

    public function newHistoryCommentFromGuestHousekeeping($reservation, $user, $historyEntryComment){
        return $this->newHistoryCommentFromGuest(HistoryEntry::TYPE_GUEST_HOUSEKEEPING_INSTRUCTION, $reservation, $user, $historyEntryComment, true, true);
    }

## reservation guest new comment - START

## reservation new comment - START
    public function newHistoryCommentFromAdmin($type, $reservation, $user, $historyEntryComment, $sendHistoryEmail = false){
        try{
            $newEntry = $this->historyEntryService->newEntry($type, $reservation, $user, $historyEntryComment);
            if($sendHistoryEmail == true && $newEntry instanceof HistoryEntry){
                $this->emailService->sendHistoryEmail($reservation, $newEntry);
            }

            return true;
        }catch (\Exception $e){
            return false;
        }
    }

    public function newHistoryCommentFromAdminEmail($reservation, $user, $historyEntryComment){
        return $this->newHistoryCommentFromAdmin(HistoryEntry::TYPE_COMMENT_TO_GUEST_EMAILED, $reservation, $user, $historyEntryComment, true);
    }

    public function newHistoryCommentFromAdminAdd($reservation, $user, $historyEntryComment){
        return $this->newHistoryCommentFromAdmin(HistoryEntry::TYPE_SILENT_COMMENT_TO_HISTORY, $reservation, $user, $historyEntryComment);
    }

    public function newHistoryCommentFromAdminHousekeeping($reservation, $user, $historyEntryComment){
        return $this->newHistoryCommentFromAdmin(HistoryEntry::TYPE_LANDLORT_HOUSEKEEPING_INSTRUCTION, $reservation, $user, $historyEntryComment);
    }

    public function newHistoryCommentFromAdminInternal($reservation, $user, $historyEntryComment){
        return $this->newHistoryCommentFromAdmin(HistoryEntry::TYPE_INTERNAL_COMMENT_HIDDEN_FROM_GUEST, $reservation, $user, $historyEntryComment);
    }
## reservation new comment - END

    public function cancel(Reservation $reservation, $refundDue = false, $noPayment = false, $refund = true){
        if($reservation instanceof Reservation) {
            if ($refundDue == true) {
                $reservation->setStatus(Reservation::STATUS_CANCELLED_REFUND_DUE);
            }elseif(!$refund) {
                $reservation->setStatus(Reservation::STATUS_CANCELLED_NO_REFUND);
            }else{
                $reservation->setStatus(Reservation::STATUS_CANCELLED);
            }

            if($noPayment == true) $reservation->setNoPaymentCancellationReinstateDate(new \DateTime('now'));

            try{
                $this->_em->persist($reservation);
                $this->_em->flush();

                return true;
            }catch (\Exception $e){
                return false;
            }
        }

        return false;
    }

########################################################################################################################
########################################################################################################################
#######################################        CANCELATIONS          ###################################################
    public function airbnbCancel(Reservation $reservation){
        if($reservation->getStatus() == Reservation::STATUS_BLOCKED_AIRBNB){
            $cancelled = $this->cancel($reservation);
            if($cancelled){
                $historyEntryComment = $this->historyEntryService->newCommenAirbnbCancelled();
                $this->historyEntryService->newEntry(HistoryEntry::TYPE_CANCELLATION, $reservation, $reservation->getUser(), $historyEntryComment);

                $nowDate = new \DateTime('now');
                if($reservation->getFromDate() > $nowDate){
                    //$this->emailService->sendForLandlord($reservation, $historyEntryComment);
                }

                return true;
            }
        }

        return false;
    }

    public function adminCancelRefundOther(Reservation $reservation, User $user){
        $reservationPreviousStatus = $reservation->getStatus();
        $cancelled = $this->cancel($reservation, true);
        if($cancelled){
            $historyEntryComment = $this->historyEntryService->newCommentAdminCancelRefundOther();
            $newEntry = $this->historyEntryService->newEntry(HistoryEntry::TYPE_CANCELLATION, $reservation, $user, $historyEntryComment);
            if($newEntry instanceof HistoryEntry){
                $this->emailService->sendHistoryEmail($reservation, $newEntry);
            }
            if($reservationPreviousStatus == Reservation::STATUS_PART_PAID || $reservationPreviousStatus == Reservation::STATUS_FULLY_PAID){
                $this->emailService->sendForLandlord($reservation, $historyEntryComment);
            }

            return $cancelled;
        }
        return false;
    }

    public function adminCancelCustomer(Reservation $reservation, User $user, $refund){
        $reservationPreviousStatus = $reservation->getStatus();
        $cancelled = $this->cancel($reservation, true);
        if($cancelled){
            $historyEntryComment = $this->historyEntryService->newCommentAdminCancelCustomer($refund);
            $newEntry = $this->historyEntryService->newEntry(HistoryEntry::TYPE_CANCELLATION, $reservation, $user, $historyEntryComment);
            if($newEntry instanceof HistoryEntry){
                $this->emailService->sendHistoryEmail($reservation, $newEntry);
            }
            if($reservationPreviousStatus == Reservation::STATUS_PART_PAID || $reservationPreviousStatus == Reservation::STATUS_FULLY_PAID){
                $this->emailService->sendForLandlord($reservation, $historyEntryComment);
            }

            return $cancelled;
        }
        return false;
    }

    public function adminCancelByUs(Reservation $reservation, User $user){
        $reservationPreviousStatus = $reservation->getStatus();
        $cancelled = $this->cancel($reservation, true);
        if($cancelled){
            $refund = number_format((float)$reservation->getPaid(), 2);

            $historyEntryComment = $this->historyEntryService->newCommentAdminCancelByUs($refund);
            $newEntry = $this->historyEntryService->newEntry(HistoryEntry::TYPE_CANCELLATION, $reservation, $user, $historyEntryComment);
            if($newEntry instanceof HistoryEntry){
                $this->emailService->sendHistoryEmail($reservation, $newEntry);
            }
            if($reservationPreviousStatus == Reservation::STATUS_PART_PAID || $reservationPreviousStatus == Reservation::STATUS_FULLY_PAID){
                $this->emailService->sendForLandlord($reservation, $historyEntryComment);
            }

            return $cancelled;
        }
        return false;
    }

    public function adminCancelFullRefund(Reservation $reservation, User $user){
        $reservationPreviousStatus = $reservation->getStatus();
        $cancelled = $this->cancel($reservation, true);
        if($cancelled){
            $refund = number_format((float)$reservation->getPaid(), 2);

            $historyEntryComment = $this->historyEntryService->newCommentAdminCancelFullRefund($refund);
            $newEntry = $this->historyEntryService->newEntry(HistoryEntry::TYPE_CANCELLATION, $reservation, $user, $historyEntryComment);
            if($newEntry instanceof HistoryEntry){
                $this->emailService->sendHistoryEmail($reservation, $newEntry);
            }
            if($reservationPreviousStatus == Reservation::STATUS_PART_PAID || $reservationPreviousStatus == Reservation::STATUS_FULLY_PAID){
                $this->emailService->sendForLandlord($reservation, $historyEntryComment);
            }

            return $cancelled;
        }
        return false;
    }

    public function adminCancelNoRefund(Reservation $reservation, User $user){
        $reservationPreviousStatus = $reservation->getStatus();
        $cancelled = $this->cancel($reservation, false, false, false);
        if($cancelled){
            $historyEntryComment = $this->historyEntryService->newCommentAdminCancelNoRefund();
            $newEntry = $this->historyEntryService->newEntry(HistoryEntry::TYPE_CANCELLATION, $reservation, $user, $historyEntryComment);
            if($newEntry instanceof HistoryEntry){
                $this->emailService->sendHistoryEmail($reservation, $newEntry);
            }
            if($reservationPreviousStatus == Reservation::STATUS_PART_PAID || $reservationPreviousStatus == Reservation::STATUS_FULLY_PAID){
                $this->emailService->sendForLandlord($reservation, $historyEntryComment);
            }

            return $cancelled;
        }
        return false;
    }

    public function adminCancelNotPaid(Reservation $reservation, User $user){
        $reservationPreviousStatus = $reservation->getStatus();
        $cancelled = $this->cancel($reservation);
        if($cancelled){
            if((float)$reservation->getPaid() > 0){
                $this->emailService->sendAutoCancelOverdueBalancePayment($reservation);
                $historyEntryComment = $this->historyEntryService->newCommentAdminCancelNotPaidWithPayment();
            }else{
                $this->emailService->sendAutoCancelNoPayment($reservation);
                $historyEntryComment = $this->historyEntryService->newCommentAdminCancelNotPaidNoPayment();
            }
            $this->historyEntryService->newEntry(HistoryEntry::TYPE_CANCELLATION, $reservation, $user, $historyEntryComment);
            if($reservationPreviousStatus == Reservation::STATUS_PART_PAID || $reservationPreviousStatus == Reservation::STATUS_FULLY_PAID){
                $this->emailService->sendForLandlord($reservation, $historyEntryComment);
            }

            return $cancelled;
        }

        return false;
    }
#######################################     CANCELATIONS - end       ###################################################
########################################################################################################################
########################################################################################################################

    public function manualCancel(Reservation $reservation, User $user){
        if($user->getId() == $reservation->getUser()->getId()){
            $reservationPreviousStatus = $reservation->getStatus();
            $cancelled = $this->cancel($reservation);
            $getSubEstatesArray = $this->_em->getRepository('UTTEstateBundle:Estate')->createQueryBuilder('e')
            ->select('e')
            ->where('e.isMultiproperty = TRUE AND e.hideFromDisplay = :hideFromDisplay AND e.id=:estate_id')
            ->setParameter('hideFromDisplay',Estate::HIDE_FROM_DISPLAY_VISIBLE)
            ->setParameter('estate_id',$reservation->getEstate()->getId())
            ->getQuery();
            $getEsateArr = $getSubEstatesArray->getResult();
            if($getEsateArr){
                $subEstatesIds = array();
            }else{
                $subEstatesIds =null;
            }
            if($getEsateArr){
                foreach($getEsateArr as $eStateArr){
                    $subEstatesData = $eStateArr->getSubEstates();
                        foreach($subEstatesData as $eStateData){
                            $subEstatesIds[] = $eStateData->getId();
                    }   
            }
            $getReservedPropertyData = array();
            foreach($subEstatesIds as $id){
                $getReservationArr = $this->_em->getRepository('UTTReservationBundle:Reservation')->createQueryBuilder('r')
                ->select('r.id')
                ->where('r.estate=:estateId AND r.user=:userId AND r.status=:status')
                ->setParameter('estateId',$id)
                ->setParameter('userId',$reservation->getUser()->getId())
                ->setParameter('status',Reservation::STATUS_BLOCKED_MULTUPROPERTY)
                ->getQuery();
                $getReservedPropertyData[] =$getReservationArr->getResult();
            }
            for ($i=0; $i <sizeof($getReservedPropertyData) ; $i++) { 
                foreach($getReservedPropertyData[$i] as $reservedData){
                    $getSubEstateReservationIds[] = $reservedData['id'];
                }
            }
            foreach($getSubEstateReservationIds as $ReservationIds){
                $changeStatusOfSubestate = $this->_em->getRepository('UTTReservationBundle:Reservation')->find($ReservationIds);
                $changeStatusOfSubestate->setStatus(Reservation::STATUS_CANCELLED);
                $this->_em->flush();
            }
            }
    
            if($cancelled){
                $nowDate = new \DateTime('now');
                $refund = 0;
                if($reservationPreviousStatus == Reservation::STATUS_PART_PAID || $reservationPreviousStatus == Reservation::STATUS_FULLY_PAID){
                    if($this->reservationFactory->isRefundable($reservation)){
                        if((float)$reservation->getPaid() > (float)$reservation->getDeposit()){
                            $refund = number_format((float)$reservation->getPaid() - (float)$reservation->getDeposit(), 2);

                            // create discount code for reservation
                            $discountCode = $this->discountCodeService->createRefundCodeAfterCancel($reservation, $refund);

                            $historyEntryComment = $this->historyEntryService->newCommentManualCancelDiscount($discountCode->getCode(), $discountCode->getExpiresAt());
                            $this->historyEntryService->newEntry(HistoryEntry::TYPE_SELF_SERVICE_GUEST_CANCELLED, $reservation, $reservation->getUser(), $historyEntryComment);
                        }
                    } else {
                        $cancelled = $this->cancel($reservation, false, false, false);
                    }
                }

                $interval = $nowDate->diff($reservation->getFromDate());
                $historyEntryComment = $this->historyEntryService->newCommentManualCancel($interval->days, $refund);
                $this->historyEntryService->newEntry(HistoryEntry::TYPE_SELF_SERVICE_GUEST_CANCELLED, $reservation, $reservation->getUser(), $historyEntryComment);

                if($reservationPreviousStatus == Reservation::STATUS_PART_PAID || $reservationPreviousStatus == Reservation::STATUS_FULLY_PAID || $reservationPreviousStatus == Reservation::STATUS_BLOCKED_AIRBNB){
                    $this->emailService->sendForLandlord($reservation, $historyEntryComment);
                }

                //$this->emailService->sendBookingCancelledEleriNotificationEmail($reservation);
                $this->referredFlagChange($reservation, $reservation->getUser(), Reservation::REFERRED_FLAG_KATIE, 'Manual cancel.');
            }
            return $cancelled;
        }

        return false;
    }

    public function autoCancel(Reservation $reservation){
        if($this->reservationFactory->isPaymentDeadlineExceeded($reservation)){
            if($reservation->getStatus() == Reservation::STATUS_RESERVED){
                return $this->autoCancelNoPayment($reservation);
            }elseif($reservation->getStatus() == Reservation::STATUS_PART_PAID){
                if($this->reservationFactory->isBalancePaymentOverdue($reservation)){
                    return $this->autoCancelOverdueBalancePayment($reservation);
                }
            }
        }

        return false;
    }

    public function autoCancelReminder(Reservation $reservation){
        if($this->reservationFactory->isPaymentDeadlineExceeded($reservation)){
            if($reservation->getStatus() == Reservation::STATUS_PART_PAID){
                if($this->reservationFactory->isBalancePaymentOverdueForReminder($reservation)){
                    $this->emailService->sendAutoCancelOverdueBalancePaymentReminder($reservation);

                    $historyEntryComment = $this->historyEntryService->newCommentAutoCancelOverdueBalancePaymentReminder();
                    $this->historyEntryService->newEntry(HistoryEntry::TYPE_BALANCE_DUE_REMINDER_EMAIL, $reservation, $reservation->getUser(), $historyEntryComment);

                    return true;
                }
            }
        }

        return false;
    }

    public function autoCancelNoPayment(Reservation $reservation){
        $reservationPreviousStatus = $reservation->getStatus();
        $cancelled = $this->cancel($reservation, false, true);
        if($cancelled){
            $historyEntryComment = $this->historyEntryService->newCommentAutoCancelNoPayment();
            $this->historyEntryService->newEntry(HistoryEntry::TYPE_AUTOMATIC_CANCELLATION_DUE_TO_NON_PAYMENT, $reservation, $reservation->getUser(), $historyEntryComment);

            $this->emailService->sendAutoCancelNoPayment($reservation);
            if($reservationPreviousStatus == Reservation::STATUS_PART_PAID || $reservationPreviousStatus == Reservation::STATUS_FULLY_PAID){
                $this->emailService->sendForLandlord($reservation, $historyEntryComment);
            }

            //$this->emailService->sendBookingCancelledEleriNotificationEmail($reservation);
            $this->referredFlagChange($reservation, $reservation->getUser(), Reservation::REFERRED_FLAG_KATIE, 'Auto cancel - no payment.');
        }
        return $cancelled;
    }

    public function autoCancelOverdueBalancePayment(Reservation $reservation){
        $reservationPreviousStatus = $reservation->getStatus();
        $cancelled = $this->cancel($reservation, false, true);
        if($cancelled){
            $historyEntryComment = $this->historyEntryService->newCommentAutoCancelOverdueBalancePayment();
            $this->historyEntryService->newEntry(HistoryEntry::TYPE_AUTOMATIC_CANCELLATION_DUE_TO_OVERDUE_BALANCE_PAYMENT, $reservation, $reservation->getUser(), $historyEntryComment);

            $this->emailService->sendAutoCancelOverdueBalancePayment($reservation);
            if($reservationPreviousStatus == Reservation::STATUS_PART_PAID || $reservationPreviousStatus == Reservation::STATUS_FULLY_PAID){
                $this->emailService->sendForLandlord($reservation, $historyEntryComment);
            }

            //$this->emailService->sendBookingCancelledEleriNotificationEmail($reservation);
            $this->referredFlagChange($reservation, $reservation->getUser(), Reservation::REFERRED_FLAG_KATIE);
        }
        return $cancelled;
    }

    public function saveReservationChangeData($reservationId, $fromDate, $toDate, $sleeps, $sleepsChildren, $pets, $infants, array $userData, array $price){
        $reservationDataUserData = new ReservationDataUserData();
        $userData = (object)$userData;
        if(isset($userData->firstName)) $reservationDataUserData->setFirstName($userData->firstName);
        if(isset($userData->lastName)) $reservationDataUserData->setLastName($userData->lastName);
        if(isset($userData->email)) $reservationDataUserData->setEmail($userData->email);
        if(isset($userData->phone)) $reservationDataUserData->setPhone($userData->phone);
        if(isset($userData->address)) $reservationDataUserData->setAddress($userData->address);
        if(isset($userData->city)) $reservationDataUserData->setCity($userData->city);
        if(isset($userData->postcode)) $reservationDataUserData->setPostcode($userData->postcode);
        if(isset($userData->country)) $reservationDataUserData->setCountry($userData->country);

        $reservationDataPrice = new ReservationDataPrice();
        $price = (object)$price;
        if(isset($price->basePrice)) $reservationDataPrice->setBasePrice($price->basePrice);
        if(isset($price->additionalSleepsPrice)) $reservationDataPrice->setAdditionalSleepsPrice($price->additionalSleepsPrice);
        if(isset($price->adminCharge)) $reservationDataPrice->setAdminCharge($price->adminCharge);
        if(isset($price->creditCardCharge)) $reservationDataPrice->setCreditCardCharge($price->creditCardCharge);
        if(isset($price->bookingProtectCharge)) $reservationDataPrice->setBookingProtectCharge($price->bookingProtectCharge);
        if(isset($price->charityCharge)) $reservationDataPrice->setCharityCharge($price->charityCharge);
        if(isset($price->petsPrice)) $reservationDataPrice->setPetsPrice($price->petsPrice);
        if(isset($price->total)) $reservationDataPrice->setTotal($price->total);

        $reservationData = new ReservationChangeData();
        $reservationData->setReservationId($reservationId);
        $reservationData->setFromDate($fromDate);
        $reservationData->setToDate($toDate);
        $reservationData->setSleeps($sleeps);
        $reservationData->setSleepsChildren($sleepsChildren);
        $reservationData->setPets($pets);
        $reservationData->setInfants($infants);
        $reservationData->setUserData($reservationDataUserData);
        $reservationData->setPrice($reservationDataPrice);

        $this->session->set('uttReservationChangeData', $reservationData);
        return true;
    }

    public function saveReservationData($estateShortName, $fromDate, $toDate, $sleeps, $sleepsChildren, $pets, $infants, array $userData, array $price, array $discountPrice, $code, $isBookingProtect = false){
        $reservationDataUserData = new ReservationDataUserData();
        $userData = (object)$userData;
        if(isset($userData->firstName)) $reservationDataUserData->setFirstName($userData->firstName);
        if(isset($userData->lastName)) $reservationDataUserData->setLastName($userData->lastName);
        if(isset($userData->email)) $reservationDataUserData->setEmail($userData->email);
        if(isset($userData->phone)) $reservationDataUserData->setPhone($userData->phone);
        if(isset($userData->address)) $reservationDataUserData->setAddress($userData->address);
        if(isset($userData->city)) $reservationDataUserData->setCity($userData->city);
        if(isset($userData->postcode)) $reservationDataUserData->setPostcode($userData->postcode);
        if(isset($userData->country)) $reservationDataUserData->setCountry($userData->country);

        $reservationDataPrice = new ReservationDataPrice();
        $price = (object)$price;
        if(isset($price->basePrice)) $reservationDataPrice->setBasePrice($price->basePrice);
        if(isset($price->additionalSleepsPrice)) $reservationDataPrice->setAdditionalSleepsPrice($price->additionalSleepsPrice);
        if(isset($price->adminCharge)) $reservationDataPrice->setAdminCharge($price->adminCharge);
        if(isset($price->creditCardCharge)) $reservationDataPrice->setCreditCardCharge($price->creditCardCharge);
        if(isset($price->bookingProtectCharge)) $reservationDataPrice->setBookingProtectCharge($price->bookingProtectCharge);
        if(isset($price->charityCharge)) $reservationDataPrice->setCharityCharge($price->charityCharge);
        if(isset($price->petsPrice)) $reservationDataPrice->setPetsPrice($price->petsPrice);
        if(isset($price->total)) $reservationDataPrice->setTotal($price->total);

        $reservationDataDiscountPrice = new ReservationDataDiscountPrice();
        $discountPrice = (object)$discountPrice;
        if(isset($discountPrice->discountCodePrice)) $reservationDataDiscountPrice->setDiscountCodePrice($discountPrice->discountCodePrice);
        if(isset($discountPrice->discountCode)) $reservationDataDiscountPrice->setDiscountCode($discountPrice->discountCode);
        if(isset($discountPrice->total)) $reservationDataDiscountPrice->setTotal($discountPrice->total);

        $reservationData = new ReservationData();
        $reservationData->setEstateShortName($estateShortName);
        $reservationData->setFromDate($fromDate);
        $reservationData->setToDate($toDate);
        $reservationData->setSleeps($sleeps);
        $reservationData->setSleepsChildren($sleepsChildren);
        $reservationData->setPets($pets);
        $reservationData->setInfants($infants);
        $reservationData->setUserData($reservationDataUserData);
        $reservationData->setPrice($reservationDataPrice);
        $reservationData->setDiscountPrice($reservationDataDiscountPrice);
        $reservationData->setCode($code);
        $reservationData->setIsBookingProtect($isBookingProtect);

        $this->session->set('uttReservationData', $reservationData);
        return true;
    }

    public function payWithVoucher(Reservation $reservation, Voucher $voucher){
        $voucherDiscountCode = $voucher->getDiscountCode();

        if(
            $reservation->getPaidWithVoucher() == null &&
            $voucherDiscountCode &&
            $voucherDiscountCode->getCodeType() == DiscountCode::CODE_TYPE_PAYMENT_VOUCHER &&
            $voucherDiscountCode->getIsActive()
        ){
            $reservation = $this->payByVoucherToReservation($reservation, $voucher);
            if($reservation instanceOf Reservation){

                $historyEntryComment = $this->historyEntryService->newCommentVoucherPayment($voucher->getTotalPrice());
                $newEntry = $this->historyEntryService->newEntry(HistoryEntry::TYPE_VOUCHER_PAYMENT, $reservation, $reservation->getUser(), $historyEntryComment);
                if($newEntry instanceof HistoryEntry){
                    $this->emailService->sendHistoryEmail($reservation, $newEntry);
                }
                $this->emailService->sendForLandlord($reservation, 'Booking payment');

                return $reservation;
            }
        }

        return false;
    }

    /**
     * @param Reservation $reservation
     * @param Voucher $voucher
     * @return bool|Reservation
     */
    protected function payByVoucherToReservation(Reservation $reservation, Voucher $voucher)
    {
        $refund = 0;
        if((float)$reservation->getPaid() + (float)$voucher->getTotalPrice() > (float)$reservation->getTotalPrice()){
            $refund = (float)$reservation->getPaid() + (float)$voucher->getTotalPrice() - (float)$reservation->getTotalPrice();
            $reservation = $this->reservationFactory->addPaymentToReservation($reservation, $reservation->getTotalPrice());
        } else {
            $reservation = $this->reservationFactory->addPaymentToReservation($reservation, $voucher->getTotalPrice(), true);
        }

        if($reservation instanceOf Reservation){
            $reservation->setPaidWithVoucher($voucher);

            $this->_em->persist($reservation);
            $this->_em->flush();

            $this->discountCodeService->useDiscountCode($voucher->getDiscountCode());

            if($refund > 0) {
                $discountCode = $this->discountCodeService->createRefundVoucherCode($reservation, $refund);
                $this->voucherService->createByAdminFromDiscountCode($discountCode, $reservation->getUser());
            }

        }


        return $reservation;

    }

    public function manualAddPayment(Reservation $reservation, $amount){
        $reservation = $this->reservationFactory->addPaymentToReservation($reservation, $amount, true);
        if($reservation instanceOf Reservation){
            $this->_em->persist($reservation);
            $this->_em->flush();

            $historyEntryComment = $this->historyEntryService->newCommentManualAddPayment($amount);
            $newEntry = $this->historyEntryService->newEntry(HistoryEntry::TYPE_MANUAL_CREDIT, $reservation, $reservation->getUser(), $historyEntryComment);
            if($newEntry instanceof HistoryEntry){
                $this->emailService->sendHistoryEmail($reservation, $newEntry);
            }

            return $reservation;
        }

        return false;
    }

    public function manualRefundPayment(Reservation $reservation, $amount){
        $reservation = $this->reservationFactory->refundPaymentToReservation($reservation, $amount);
        if($reservation instanceOf Reservation){
            $this->_em->persist($reservation);
            $this->_em->flush();

            $historyEntryComment = $this->historyEntryService->newCommentManualRefundPayment($amount);
            $newEntry = $this->historyEntryService->newEntry(HistoryEntry::TYPE_MANUAL_REFUND, $reservation, $reservation->getUser(), $historyEntryComment);
            if($newEntry instanceof HistoryEntry){
                $this->emailService->sendHistoryEmail($reservation, $newEntry);
            }

            return $reservation;
        }

        return false;
    }

    public function getReservationData(){
        if($this->session->has('uttReservationData')){
            $data = $this->session->get('uttReservationData');
            if($data instanceof ReservationData){
                return $data;
            }
        }

        return false;
    }
    public function getReservationChangeData(){
        if($this->session->has('uttReservationChangeData')){
            $data = $this->session->get('uttReservationChangeData');
            if($data instanceof ReservationChangeData){
                return $data;
            }
        }

        return false;
    }

    public function removeReservationData(){
        if($this->session->has('uttReservationData')){
            $this->session->remove('uttReservationData');
        }

        // check if removed
        if(!$this->session->has('uttReservationData')){
            return true;
        }
        return false;
    }
    public function removeReservationChangeData(){
        if($this->session->has('uttReservationChangeData')){
            $this->session->remove('uttReservationChangeData');
        }

        // check if removed
        if(!$this->session->has('uttReservationChangeData')){
            return true;
        }
        return false;
    }

    public function getReservations(Estate $estate, \Datetime $fromDate, \Datetime $toDate){
        /** @var ReservationRepository $reservationRepository */
        $reservationRepository = $this->_em->getRepository('UTTReservationBundle:Reservation');

        if($estate->getType() == Estate::TYPE_FLEXIBLE_BOOKING){
            return $reservationRepository->getFlexibleActiveReservationsBetweenDates($estate, $fromDate, $toDate);
        }elseif($estate->getType() == Estate::TYPE_STANDARD_M_F){
            return $reservationRepository->getFlexibleActiveReservationsBetweenDates($estate, $fromDate, $toDate);
        }

        return false;
    }

    private function getEmptyPrice(){
        $price = new ReservationDataPrice();
        return $price;
    }

    public function isBlockedForArriveOrDepart(\Datetime $date){
        $year =  $date->format('Y');
        $month =  $date->format('m');
        $day =  $date->format('d');

        if($month == 12 && $day == 25){ return true; }
        if($month == 12 && $day == 26){ return true; }
        if($month == 01 && $day == 01){ return true; }
        if($year == 2015 && $month == 04 && $day == 05){ return true; }
        if($year == 2016 && $month == 03 && $day == 27){ return true; }

        //$easterDate = new \Datetime(date("Y-m-d",easter_date($date->format('Y'))));
        //if($month == $easterDate->format('m') && $day == $easterDate->format('d')){ return true; }

        return false;
    }

    public function getPriceForParams(Estate $estate, \Datetime $fromDate, \Datetime $toDate, $sleeps, $pets, $infants, $forManagePrice = false, $forSearchEngine = false, $isBookingProtect = false, $forcedBasePrice = false){
        if(!$forcedBasePrice){
            $basePrice = $this->pricingService->getBasePrice($estate, $fromDate, $toDate, $forManagePrice, $forSearchEngine);
        }else{
            $basePrice = $forcedBasePrice;
        }
        $additionalSleepsPrice = $this->pricingService->calculateAdditionalSleepsPrice($estate, $basePrice, $sleeps);
        $petsPrice = $this->pricingService->calculatePetsPrice($estate, $pets);
        $infantsPrice = $this->pricingService->calculateInfantsPrice($estate, $infants);
        $adminCharge = $this->pricingService->calculateAdminCharge($basePrice, $additionalSleepsPrice, $petsPrice);

        if($basePrice != -1 && $additionalSleepsPrice != -1 && $adminCharge != -1 && $petsPrice != -1 && $infantsPrice != -1){
            $price = new ReservationDataPrice();

            if(!$forcedBasePrice){
                $offerPrice = $this->pricingService->getOfferPrice($estate, $fromDate, $toDate, $forSearchEngine);
                if($offerPrice != -1) {
                    $basePrice = $offerPrice;
                    $price->setIsSpecialOffer(true);
                    //recalculate admin charge
                    $adminCharge = $this->pricingService->calculateAdminCharge($basePrice, $additionalSleepsPrice, $petsPrice);
                }
            }

            $price->setBasePrice($basePrice);
            $price->setAdditionalSleepsPrice($additionalSleepsPrice);
            $price->setPetsPrice($petsPrice);
            $price->setAdminCharge($adminCharge);
            $price->setTotal($basePrice + $additionalSleepsPrice + $adminCharge + $petsPrice);

            if($isBookingProtect){
                $bookingProtectCharge = $this->pricingService->calculateBookingProtectCharge($price->getTotal(), $fromDate);
                $price->setBookingProtectCharge($bookingProtectCharge);
                $price->setTotal($price->getTotal() + $price->getBookingProtectCharge());
            }

            return $price;
        }

        return false;
    }

    public function assignBookingProtectCharge(Reservation $reservation){
        if($reservation->isAllowedForBookingProtection()){
            $bookingProtectCharge = $this->pricingService->calculateBookingProtectCharge($reservation->getTotalPrice(), $reservation->getFromDate());
            if($bookingProtectCharge){
                $reservation = $this->reservationFactory->addBookingProtectCharge($reservation, $bookingProtectCharge);
                $reservation->setIsBookingProtect(true);

                $this->_em->persist($reservation);
                $this->_em->flush();

                $this->bookingProtectService->callSale($reservation);

                return true;
            }
        }

        return false;
    }

    public function isAvailableForBooking(Estate $estate, \Datetime $fromDate, \Datetime $toDate, $exceptReservationId = null){
        $reservations = $this->getReservations($estate, $fromDate, $toDate);

        if($reservations){
            /** @var Reservation $reservation */
            foreach($reservations as $reservation){
                if(!is_null($exceptReservationId) && $reservation->getId() == $exceptReservationId){ continue; }

                if(!($reservation->getFromDate() == $toDate || $reservation->getToDate() == $fromDate)){
                    return false; // this dates are not available
                }
            }
        }

        return true;
    }

    public function manageReservationPrice(Reservation $reservation, Estate $estate, User $user,  \Datetime $fromDate, \Datetime $toDate, $sleeps, $pets, $infants){
        if($user->getId() == $reservation->getUser()->getId()){
            if($reservation->isBlocked()){
                $price = $this->getEmptyPrice();
            }else{
                $forcedBasePrice = false;

                if($reservation->getFromDate()->format('Y-m-d') == $fromDate->format('Y-m-d')){
                    if($reservation->getToDate()->format('Y-m-d') == $toDate->format('Y-m-d')){
                        $forcedBasePrice = $reservation->getPriceDecoded()->basePrice;
                    }
                }

                $price = $this->getPriceForParams($estate, $fromDate, $toDate, $sleeps, $pets, $infants, true, false, $reservation->getIsBookingProtect(), $forcedBasePrice);
            }

            if($price instanceof ReservationDataPrice){
                if($this->isAvailableForBooking($estate, $fromDate, $toDate, $reservation->getId())){
                    $reservationCreditCardCharge = (float) $reservation->getPriceDecoded()->getCreditCardCharge();
                    $price->setCreditCardCharge($reservationCreditCardCharge);

                    $reservationCharityCharge = (float) $reservation->getPriceDecoded()->getCharityCharge();
                    $price->setCharityCharge($reservationCharityCharge);
                    return $price;
                }
            }
        }
        return false;
    }

    public function newReservationPrice(Estate $estate, \Datetime $fromDate, \Datetime $toDate, $sleeps, $pets, $infants, $isBookingProtect = false){
        $price = $this->getPriceForParams($estate, $fromDate, $toDate, $sleeps, $pets, $infants, false, false, $isBookingProtect);

        if($price instanceof ReservationDataPrice){
            if($this->isAvailableForBooking($estate, $fromDate, $toDate)){
                return $price;
            }
        }

        return false;
    }

    public function checkIsRefundable(array $reservations) {
        foreach ($reservations as $reservation) {
            if($this->reservationFactory->isCancelAndNoRefund($reservation)) {
                $reservation->setStatus(Reservation::STATUS_CANCELLED_NO_REFUND);
            }
        }

        return $reservations;
    }
    
    /**
     * @param $noticePeriod
     * @param $fromDate
     * @return bool
     */
    public function validateNoticePeriod($noticePeriod, $fromDate) {
        $timeZone = new \DateTimeZone('Europe/London');
        $fromDateOb = new \DateTime($fromDate, $timeZone);
        $today  = new \DateTime(date('Y-m-d'), $timeZone);
        $todayHourDate = new \DateTime('now', $timeZone);
        $todayHour = (int) $todayHourDate->format('H');
        $daysDiff = (int) $today->diff($fromDateOb)->format('%r%a');

        if($daysDiff < 0) {
            return false;
        }

        if(Estate::NOTICE_PERIOD_UP_TO_3PM_TODAY === $noticePeriod && $daysDiff == 0 && $todayHour > 15) {
            return false;
        }

        if(Estate::NOTICE_PERIOD_UP_TO_10AM_TODAY === $noticePeriod && $daysDiff == 0 && $todayHour > 10) {
            return false;
        }

        if((Estate::NOTICE_PERIOD_UP_TO_8PM_DAY_BEFORE === $noticePeriod ||
            Estate::NOTICE_PERIOD_NEXT_CHANGE_OVER_OR_8PM_NIGHT_BEFORE === $noticePeriod ) &&
            ($daysDiff === 0 || ($daysDiff === 1 && $todayHour > 20))) {
            return false;
        }

        if(Estate::NOTICE_PERIOD_MIN_2_DAYS_NOTICE === $noticePeriod && $daysDiff < 2) {
            return false;
        }

        if(Estate::NOTICE_PERIOD_MIN_3_DAYS_NOTICE === $noticePeriod && $daysDiff < 3) {
            return false;
        }

        if(Estate::NOTICE_PERIOD_MIN_4_DAYS_NOTICE === $noticePeriod && $daysDiff < 4) {
            return false;
        }

        if(Estate::NOTICE_PERIOD_MIN_5_DAYS_NOTICE === $noticePeriod && $daysDiff < 5) {
            return false;
        }

        return true;

    }

    /**
     * @param Estate $estate
     * @param \Datetime $dateFrom
     * @param \Datetime $dateTo
     * @return bool
     */
    public function validateReservationDate(Estate $estate,\Datetime $dateFrom, \Datetime $dateTo) {
        $dateFromDay = (int) $dateFrom->format('N');
        $dateToDay = (int) $dateTo->format('N');
        if($estate->getType() === Estate::TYPE_STANDARD_M_F) {
             return ($dateFromDay === 1 || $dateFromDay === 5) && ($dateToDay === 5 || $dateToDay === 1);
        }
        return true;
    }
    
}
