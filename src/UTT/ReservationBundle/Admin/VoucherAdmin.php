<?php

namespace UTT\ReservationBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Show\ShowMapper;

use Sonata\AdminBundle\Route\RouteCollection;

use Knp\Menu\ItemInterface as MenuItemInterface;

class VoucherAdmin extends Admin
{
    /**
     * @param \Sonata\AdminBundle\Show\ShowMapper $showMapper
     *
     * @return void
     */
    protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper
            ->with('voucher ref: '.$this->getSubject()->getId())
            ->end()
            ->with('voucher')
                ->add('id', null, array('label' => 'voucher ref'))
                ->add('pin', null, array('label' => 'voucher PIN'))
                ->add('discountCode', null, array('label' => 'discount code'))
                ->add('value', null, array('label' => 'value'))
                ->add('name', null, array('label' => 'name'))
                ->add('createdAt', null, array('label' => 'created at', 'format' => 'l, j F Y, g:i'))
                ->add('expiresAt', null, array('label' => 'expires at', 'format' => 'l, j F Y'))
                ->add('status', null, array('label' => 'status', 'template' => 'UTTReservationBundle:VoucherAdmin:show_type_status.html.twig'))
            ->end()
            ->with('pricing')
                ->add('paid', null, array('label' => 'paid'))
                ->add('totalPrice', null, array('label' => 'total price'))
                ->add('_toPay', null, array('label' => 'to pay', 'template' => 'UTTReservationBundle:VoucherAdmin:show_type_to_pay.html.twig'))
                ->add('price', null, array('label' => 'price', 'template' => 'UTTReservationBundle:VoucherAdmin:show_type_price.html.twig'))
            ->end()
            ->with('customer')
                ->add('user', null, array('label' => 'user'))
                ->add('userData', null, array('label' => 'details', 'template' => 'UTTReservationBundle:VoucherAdmin:show_type_user_data.html.twig'))
            ->end()
            ->with('history')
                ->add('voucherHistoryEntries', null , array('label' => 'history'))
    ;
    }

    /**
     * @param \Sonata\AdminBundle\Datagrid\ListMapper $listMapper
     *
     * @return void
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('id', null, array('label' => 'ID'))
            ->add('value', null, array('label' => 'value'))
            ->add('name', null, array('label' => 'name'))
            ->add('createdAt', null, array('label' => 'created at'))
            ->add('expiresAt', null, array('label' => 'expires at'))
            ->add('_userName', null, array('label' => 'customer name', 'template' => 'UTTReservationBundle:VoucherAdmin:list_type_user_name.html.twig'))
            ->add('_status', null, array('label' => 'status', 'template' => 'UTTReservationBundle:VoucherAdmin:list_type_status_name.html.twig'))
            ->add('_action', 'actions', array(
                'actions' => array(
                    'view' => array(),
                    'edit' => array(),
                    'delete' => array(),
                )
            ))
        ;
    }

    /**
     * @param \Sonata\AdminBundle\Datagrid\DatagridMapper $datagridMapper
     *
     * @return void
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {

    }

    /**
     * Remove possibility to delete object.
     *
     * @param RouteCollection $collection
     */
    public function configureRoutes(RouteCollection $collection)
    {
        $collection->remove('delete');
        $collection->remove('edit');
        $collection->remove('batch');
        $collection->remove('export');
        $collection->remove('acl');
        $collection->remove('create');
    }

}

?>
