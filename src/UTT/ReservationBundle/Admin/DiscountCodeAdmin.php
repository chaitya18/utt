<?php

namespace UTT\ReservationBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Show\ShowMapper;

use Sonata\AdminBundle\Route\RouteCollection;

use Knp\Menu\ItemInterface as MenuItemInterface;
use UTT\AdminBundle\Service\UTTAclService;
use UTT\IndexBundle\Service\EmailService;
use UTT\ReservationBundle\Entity\DiscountCode;
use UTT\ReservationBundle\Service\VoucherService;
use UTT\UserBundle\Entity\User;

class DiscountCodeAdmin extends Admin
{
    /** @var  UTTAclService */
    private $uttAclService;

    public function setUttAclService(UTTAclService $uttAclService){
        $this->uttAclService = $uttAclService;
    }

    /** @var  EmailService */
    private $emailService;

    public function setEmailService(EmailService $emailService){
        $this->emailService = $emailService;
    }

    /**
     * @var VoucherService
     */
    private $voucherService;

    public function setVoucherService(VoucherService $voucherService){
        $this->voucherService = $voucherService;
    }

    /**
     * {@inheritdoc}
     */
    public function getFilterParameters()
    {
        $this->datagridValues = array_merge(array(
            '_sort_order' => 'DESC',
            '_sort_by' => 'id',
        ), $this->datagridValues );
        return parent::getFilterParameters();
    }

    public function createQuery($context = 'list')
    {
        $user = $this->uttAclService->userAuth();
        if($user && $this->uttAclService->isGrantedOwner()){
            $query = parent::createQuery($context);
            $query->innerJoin($query->getRootAlias().'.estates', 'eeee');
            $query->innerJoin('eeee.ownerUsers', 'ououou', 'WITH', $query->expr()->in('ououou.id', ':userId'));
            $query->setParameter('userId', $user->getId());

            return $query;
        }

        return parent::createQuery($context);
    }

    /**
     * @param \Sonata\AdminBundle\Form\FormMapper $formMapper
     *
     * @return void
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $codeTypeChoices = $this->getSubject()->getAllowedCodeTypes();

        $formMapper
            ->with('general information')
                //->add('id', null, array('required' => false, 'label' => 'ID', 'disabled' => true))
                ->add('isActive', null, array('required' => false, 'label' => 'is active?'))
                ->add('code', null, array('required' => true, 'label' => 'code', 'attr' => array('class' => 'form-control')))
                ->add('codeType', 'choice', array('required' => true, 'label' => 'code type', 'choices' => $codeTypeChoices, 'attr' => array('class' => 'form-control')))
                ->add('value', null, array('required' => true, 'label' => 'value', 'attr' => array('class' => 'form-control')))
                ->add('expiresAt', null, array('required' => true, 'label' => 'expires by', 'years' => range(date('Y'), date('Y')+3)))
        ;
        if($this->uttAclService->isGrantedOwner()){
            $user = $this->uttAclService->userAuth();
            $formMapper
                //->add('estates', null, array('required' => true, 'label' => 'properties', 'attr' => array('class' => 'initDiscountCodeEstatesSelectAll form-control'), 'choices' => $this->getConfigurationPool()->getContainer()->get('doctrine')->getEntityManager()->getRepository("UTTEstateBundle:Estate")->getActiveOwnedByUser($user->getId())));
                ->add('estates', null, array('required' => false, 'label' => 'properties', 'attr' => array('class' => 'form-control'), 'choices' => $this->getConfigurationPool()->getContainer()->get('doctrine')->getEntityManager()->getRepository("UTTEstateBundle:Estate")->getActiveOwnedByUser($user->getId())));
        }else{
            $formMapper
                //->add('estates', null, array('required' => true, 'label' => 'properties', 'attr' => array('class' => 'initDiscountCodeEstatesSelectAll form-control'), 'choices' => $this->getConfigurationPool()->getContainer()->get('doctrine')->getEntityManager()->getRepository("UTTEstateBundle:Estate")->getAllActive()));
                ->add('estates', null, array('required' => false, 'label' => 'properties', 'attr' => array('class' => 'form-control'), 'choices' => $this->getConfigurationPool()->getContainer()->get('doctrine')->getEntityManager()->getRepository("UTTEstateBundle:Estate")->getAllActive()))
                ->add('isAllEstates', null, array('required' => false, 'label' => 'all estates?'));
        }
        $formMapper
                ->add('holidayMustStartAt', null, array('required' => false, 'label' => 'holiday must start after', 'years' => range(date('Y'), date('Y')+3)))
                ->add('holidayMustStartBy', null, array('required' => true, 'label' => 'holiday must start by', 'years' => range(date('Y'), date('Y')+3)))
                ->add('emails', null, array('required' => false, 'label' => 'emails (use ; as a separator)', 'attr' => array('class' => 'form-control')))
                ->add('reason', null, array('required' => true, 'label' => 'reason', 'attr' => array('class' => 'form-control')))
            ->end()
        ;
    }

    /**
     * @param \Sonata\AdminBundle\Datagrid\ListMapper $listMapper
     *
     * @return void
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            //->addIdentifier('id', null, array('label' => 'ID'))
            ->addIdentifier('code', null, array('label' => 'code'))
            ->add('isActive', null, array('label' => 'is active?'))
            ->add('isRedeemed', null, array('label' => 'is edeemed?'))
            ->add('codeType', null, array('label' => 'code type', 'template' => 'UTTReservationBundle:DiscountCodeAdmin:list_type_code_type.html.twig'))
            ->add('value', null, array('label' => 'value'))
            ->add('expiresAt', null, array('label' => 'expires by'))
            //->add('estates', null, array('label' => 'properties'))
            ->add('holidayMustStartAt', null, array('label' => 'holiday must start after'))
            ->add('holidayMustStartBy', null, array('label' => 'holiday must start by'))
            ->add('createdAt', null, array('label' => 'created at'))
            ->add('createdByUser', null, array('label' => 'created by'))
            ->add('reason', null, array('label' => 'reason'))
            ->add('_action', 'actions', array(
                'actions' => array(
                    'view' => array(),
                    'edit' => array(),
                    'delete' => array(),
                )
            ))
        ;
    }

    /**
     * @param \Sonata\AdminBundle\Datagrid\DatagridMapper $datagridMapper
     *
     * @return void
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('codeType', 'doctrine_orm_string', array(), 'choice', array('choices' => DiscountCode::getAllowedCodeTypesStatic()));
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function prePersist($object){
        /** @var DiscountCode $object */

        $user = $this->uttAclService->userAuth();
        if($user instanceof User){
            $object->setCreatedByUser($user);
        }

        $this->sendDiscountCodeEmails($object);
    }

    /**
     * {@inheritdoc}
     */
    public function postPersist($object)
    {
        if($object->getCodeType() === DiscountCode::CODE_TYPE_PAYMENT_VOUCHER) {
            $this->voucherService->createByAdminFromDiscountCode($object);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function postUpdate($object)
    {
        if($object->getCodeType() === DiscountCode::CODE_TYPE_PAYMENT_VOUCHER) {
            $this->voucherService->createByAdminFromDiscountCode($object);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function preUpdate($object){
        $this->sendDiscountCodeEmails($object);
    }

    private function sendDiscountCodeEmails($object){
        $emailsString = $object->getEmails();
        if($emailsString){
            $emailsString = str_replace(' ', '', $emailsString);
            $emailsArray = explode(';',$emailsString);
            if(is_array($emailsArray) && count($emailsArray) > 0){
                foreach($emailsArray as $email){
                    $this->emailService->sendDiscountCodeEmail($email, $object);
                }
            }
        }
    }

    /**
     * Remove possibility to delete object.
     *
     * @param RouteCollection $collection
     */
    public function configureRoutes(RouteCollection $collection)
    {
        $collection->remove('batch');
        $collection->remove('acl');
        $collection->remove('export');
        $collection->remove('show');
    }

}

?>
