<?php

namespace UTT\ReservationBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Show\ShowMapper;

use Sonata\AdminBundle\Route\RouteCollection;

use Knp\Menu\ItemInterface as MenuItemInterface;
use UTT\ReservationBundle\Entity\PricingCategory;

class PricingCategoryPriceFlexibleAdmin extends Admin
{
    /**
     * {@inheritdoc}
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $typeChoices = $this->getSubject()->getAllowedTypes();

        $formMapper
            ->with('general information')
                ->add('year', null, array('required' => true, 'label' => 'year', 'disabled' => true, 'attr' => array('class' => 'pricingCategoryYear')))
                ->add('type', 'choice', array('required' => true, 'label' => 'type', 'choices' => $typeChoices, 'disabled' => true))
                ->add('priceStandingCharge', null, array('required' => true, 'label' => 'price standing charge'))
                ->add('priceSundayThursday', null, array('required' => true, 'label' => 'price sunday-thursday'))
                ->add('priceFridaySaturday', null, array('required' => true, 'label' => 'price friday-saturday'))
                ->add('_1', 'text', array('mapped' => false, 'required' => false, 'label' => '7 nights price', 'attr' => array('class' => 'initWeekPriceCalculator'), 'disabled' => true))
                ->add('_2', 'text', array('mapped' => false, 'required' => false, 'label' => 'weekend price(fri-mon)', 'attr' => array('class' => 'initWeekendPriceCalculator'), 'disabled' => true))
                ->add('pricingCategory', null, array('required' => true, 'label' => 'pricing category', 'disabled' => true))
            ->end()
        ;
    }

    /**
     * {@inheritdoc}
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            //->addIdentifier('id', null, array('label' => 'ID'))
            ->add('_action', 'actions', array(
                'actions' => array(
                    'view' => array(),
                    'edit' => array(),
                    'delete' => array(),
                )
            ))
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function configureRoutes(RouteCollection $collection)
    {
        $collection->remove('create');
        $collection->remove('delete');
        $collection->remove('list');
        $collection->remove('acl');
    }

}

?>
