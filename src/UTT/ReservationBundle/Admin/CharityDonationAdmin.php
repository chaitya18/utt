<?php

namespace UTT\ReservationBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Show\ShowMapper;

use Sonata\AdminBundle\Route\RouteCollection;

use Knp\Menu\ItemInterface as MenuItemInterface;

class CharityDonationAdmin extends Admin
{
    /**
     * {@inheritdoc}
     */
    public function getFilterParameters()
    {
        $this->datagridValues = array_merge(array(
            '_sort_order' => 'DESC',
            '_sort_by' => 'donatedAt',
        ), $this->datagridValues );
        return parent::getFilterParameters();
    }

    /**
     * @param \Sonata\AdminBundle\Form\FormMapper $formMapper
     *
     * @return void
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->with('company information')
                ->add('donatedAt', null, array('required' => true, 'data' => new \DateTime('now'), 'label' => 'donated at'))
                ->add('paid', null, array('required' => true, 'label' => 'paid'))
                ->add('url', null, array('required' => false, 'label' => 'url'))
                ->add('description', null, array('required' => true, 'label' => 'description', 'attr' => array('rows' => '8')))
                ->add('file', 'file', array('required' => true, 'label' => 'image'))
                ->add('image', null, array('label' => 'image preview', 'attr' => array('class' => 'charityDonationImagePreview', 'data-upload-dir' => $this->getSubject()->getUploadDir())))
            ->end()
        ;
    }

    /**
     * @param \Sonata\AdminBundle\Datagrid\ListMapper $listMapper
     *
     * @return void
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('donatedAt', null, array('label' => 'donated at'))
            ->add('paid', null, array('label' => 'paid'))
            ->add('_action', 'actions', array(
                'actions' => array(
                    'view' => array(),
                    'edit' => array(),
                    'delete' => array(),
                )
            ))
        ;
    }

    /**
     * Remove possibility to delete object.
     *
     * @param RouteCollection $collection
     */
    public function configureRoutes(RouteCollection $collection)
    {
        $collection->remove('show');
        $collection->remove('batch');
        $collection->remove('export');
        $collection->remove('acl');
    }

    public function prePersist($block){
        $this->saveImage($block);
    }

    public function preUpdate($block){
        $this->saveImage($block);
    }

    public function saveImage($object){
        $basepath = $this->getRequest()->getBasePath();
        $object->uploadImage($basepath);
    }
}

?>
