<?php

namespace UTT\ReservationBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Show\ShowMapper;

use Sonata\AdminBundle\Route\RouteCollection;

use Knp\Menu\ItemInterface as MenuItemInterface;
use UTT\ReservationBundle\Entity\PricingCategory;

class PricingCategoryPriceStandardAdmin extends Admin
{
    /**
     * {@inheritdoc}
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $typeChoices = $this->getSubject()->getAllowedTypes();

        $formMapper
            ->with('general information')
                ->add('year', null, array('required' => true, 'label' => 'year', 'disabled' => true, 'attr' => array('class' => 'pricingCategoryYear')))
                ->add('type', 'choice', array('required' => true, 'label' => 'type', 'choices' => $typeChoices, 'disabled' => true))
                ->add('price7Nights', null, array('required' => true, 'label' => 'price 7 nights'))
                ->add('priceMondayFriday', null, array('required' => true, 'label' => 'price monday-friday'))
                ->add('priceFridayMonday', null, array('required' => true, 'label' => 'price friday-monday'))
                ->add('pricingCategory', null, array('required' => true, 'label' => 'pricing category', 'disabled' => true))
            ->end()
        ;
    }

    /**
     * {@inheritdoc}
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            //->addIdentifier('id', null, array('label' => 'ID'))
            ->add('_action', 'actions', array(
                'actions' => array(
                    'view' => array(),
                    'edit' => array(),
                    'delete' => array(),
                )
            ))
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function configureRoutes(RouteCollection $collection)
    {
        $collection->remove('create');
        $collection->remove('delete');
        $collection->remove('list');
        $collection->remove('acl');
    }

}

?>
