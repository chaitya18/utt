<?php

namespace UTT\ReservationBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Show\ShowMapper;

use Sonata\AdminBundle\Route\RouteCollection;

use Knp\Menu\ItemInterface as MenuItemInterface;

class HistoryEntryAdmin extends Admin
{
    /**
     * {@inheritdoc}
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->add('createdAt', null, array('label' => 'created at'))
            ->add('user', null, array('label' => 'created by user'))
            ->add('type', null, array('label' => 'entry type', 'template' => 'UTTReservationBundle:HistoryEntryAdmin:list_type_type.html.twig'))
            ->add('comment', null, array('label' => 'comment'))
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function getFilterParameters()
    {
        $this->datagridValues = array_merge(array(
            '_sort_order' => 'DESC',
            '_sort_by' => 'createdAt',
        ), $this->datagridValues );
        return parent::getFilterParameters();
    }

    /**
     * {@inheritdoc}
     */
    public function configureRoutes(RouteCollection $collection)
    {
        $collection->clearExcept(array('list'));
    }

}

?>
