<?php

namespace UTT\ReservationBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Show\ShowMapper;

use Sonata\AdminBundle\Route\RouteCollection;

use Knp\Menu\ItemInterface as MenuItemInterface;
use UTT\AdminBundle\Service\UTTAclService;
use UTT\ReservationBundle\Entity\Reservation;

class ReservationAdmin extends Admin
{
    /** @var  UTTAclService */
    private $uttAclService;

    public function setUttAclService(UTTAclService $uttAclService){
        $this->uttAclService = $uttAclService;
    }

    /**
     * {@inheritdoc}
     */
    public function getFilterParameters()
    {
        $this->datagridValues = array_merge(array(
            '_sort_order' => 'DESC',
            '_sort_by' => 'fromDate',
        ), $this->datagridValues );
        return parent::getFilterParameters();
    }

    public function createQuery($context = 'list')
    {
        $user = $this->uttAclService->userAuth();
        if($user && $this->uttAclService->isGrantedOwner()){
            $query = parent::createQuery($context);
            $query->innerJoin($query->getRootAlias().'.estate', 'eeee');
            $query->innerJoin('eeee.ownerUsers', 'ououou', 'WITH', $query->expr()->in('ououou.id', ':userId'));
            $query->setParameter('userId', $user->getId());

            return $query;
        }elseif($user && $this->uttAclService->isGrantedCleaner()){
            $query = parent::createQuery($context);
            $query->innerJoin($query->getRootAlias().'.estate', 'eeee');
            $query->innerJoin('eeee.cleanerUsers', 'ououou', 'WITH', $query->expr()->in('ououou.id', ':userId'));
            $query->setParameter('userId', $user->getId());

            return $query;
        }

        return parent::createQuery($context);
    }

    /**
     * @param \Sonata\AdminBundle\Show\ShowMapper $showMapper
     *
     * @return void
     */
    protected function configureShowFields(ShowMapper $showMapper)
    {
        $user = $this->uttAclService->userAuth();

        $showMapper
            ->with('booking ref: '.number_format($this->getSubject()->getId(), 0, ' ', ' '))
            ->with('holiday')
                ->add('estate', null, array('label' => 'property', 'template' => 'UTTReservationBundle:ReservationAdmin:show_type_estate.html.twig'))
                ->add('fromDate', null, array('label' => 'arrive', 'format' => 'l, j F Y'))
                ->add('toDate', null, array('label' => 'depart', 'format' => 'l, j F Y'))
                ->add('_numberOfNights', null, array('label' => 'nights', 'template' => 'UTTReservationBundle:ReservationAdmin:show_type_numberOfNights.html.twig'))
                ->add('sleeps', null, array('label' => 'sleeps', 'template' => 'UTTReservationBundle:ReservationAdmin:show_type_sleeps.html.twig'));
        if(!$this->getSubject()->isBlocked()){
            $showMapper
                    ->add('pets', null, array('label' => 'pets'))
                    ->add('infants', null, array('label' => 'infants'))
                    ;
        }
        if($this->uttAclService->isGrantedSuperAdmin() || $this->getSubject()->isBlocked()){
            $showMapper
                ->add('requote', null, array('label' => 'requote', 'template' => 'UTTReservationBundle:ReservationAdmin:show_type_requote.html.twig'));
        }
        $showMapper->with('booking')
            ->add('id', null, array('label' => 'booking ref'))
            ->add('pin', null, array('label' => 'booking PIN'))
            ->add('status', null, array('label' => 'status', 'template' => 'UTTReservationBundle:ReservationAdmin:show_type_status.html.twig'))
            ->add('ipAddress', null, array('label' => 'ip address'))
            ->add('createdAt', null, array('label' => 'booking date', 'format' => 'l, j F Y, g:i'))
            ->add('note', null, array('label' => 'note/reason'))
        ;

        if(!($this->getSubject()->isBlocked() || $this->getSubject()->isCancelled()) && $this->uttAclService->isGrantedSuperAdmin()){
            $showMapper
                ->add('_otherBookings', null, array('label' => 'other bookings', 'template' => 'UTTReservationBundle:ReservationAdmin:show_type_otherBookings.html.twig'));
        }

        $showMapper
            ->with('customer')
                ->add('user', null, array('label' => 'user'));
        if(!$this->getSubject()->isBlocked()){
            $showMapper
                    ->add('userData', null, array('label' => 'details', 'template' => 'UTTReservationBundle:ReservationAdmin:show_type_user_data.html.twig'));
        }
        if(!$this->getSubject()->isBlocked()){
            $showMapper
                ->with('pricing');

            if($this->uttAclService->isGrantedSuperAdmin()){
                $showMapper
                    ->add('_transaction', null, array('label' => 'transaction', 'template' => 'UTTReservationBundle:ReservationAdmin:show_type_transaction.html.twig'))
                    ->add('paid', null, array('label' => 'paid'))
                    ->add('paidWithVoucher', null, array('label' => 'paid with voucher'))
                ;
            }

            if($this->uttAclService->isGrantedSuperAdmin()){
                $showMapper
                    ->add('totalPrice', null, array('label' => 'total price'));
            }else{
                $showMapper
                    ->add('ownersTotalPrice', null, array('label' => 'total price'));
            }

            if($this->uttAclService->isGrantedSuperAdmin()){
                $showMapper
                    ->add('_toPay', null, array('label' => 'to pay', 'template' => 'UTTReservationBundle:ReservationAdmin:show_type_to_pay.html.twig'))
                    ->add('price', null, array('label' => 'price', 'template' => 'UTTReservationBundle:ReservationAdmin:show_type_price.html.twig'))
                    ->add('discountPrice', null, array('label' => 'discount price', 'template' => 'UTTReservationBundle:ReservationAdmin:show_type_discount_price.html.twig'))
                ;
            }
            $showMapper
                    ->add('deposit', null, array('label' => 'deposit'))
                    ->add('balanceDueDate', null, array('label' => 'balance due date'));
        }

        if($this->uttAclService->isGrantedSuperAdmin()){
            if(!$this->getSubject()->isBlocked()){
                $showMapper
                    ->with('referred flag')
                        ->add('_referredFlag', null, array('label' => 'referred flag', 'template' => 'UTTReservationBundle:ReservationAdmin:show_type_referredFlag.html.twig'))
                    ->with('actions & history')
                        ->add('_actions', null, array('label' => 'actions', 'template' => 'UTTReservationBundle:ReservationAdmin:show_type_actions.html.twig'))
                        ->add('historyEntries', null , array('label' => 'history'))
                ;
            }else{
                $showMapper
                    ->with('referred flag')
                        ->add('_referredFlag', null, array('label' => 'referred flag', 'template' => 'UTTReservationBundle:ReservationAdmin:show_type_referredFlag.html.twig'))
                    ->with('actions & history')
                        ->add('_actions', null, array('label' => 'actions', 'template' => 'UTTReservationBundle:ReservationAdmin:show_type_actions_blocked.html.twig'))
                        ->add('historyEntries', null , array('label' => 'history'))
                ;
            }
        } else {

            $showMapper
                ->with('referred flag')
                ->add('_referredFlag', null, array(
                    'label' => 'referred flag',
                    'template' => 'UTTReservationBundle:ReservationAdmin:show_type_referredFlag.html.twig'
                ));

            if ($this->isAllowCancelByOwner($user)) {
                $showMapper->with('actions & history')
                    ->add('_actions', null, array(
                        'label' => 'actions',
                        'template' => 'UTTReservationBundle:ReservationAdmin:show_type_actions_blocked.html.twig'
                    ));
            }

            $showMapper->add('_historyEntriesOwner', null, array(
                'label' => 'history',
                'template' => 'UTTReservationBundle:ReservationAdmin:show_type_historyEntriesOwner.html.twig'
            ));

        }
    }

    /**
     * @param $user
     * @return bool
     */
    protected function isAllowCancelByOwner($user)
    {
        /* allow cancel when status STATUS_BLOCKED_OWNER or STATUS_BLOCKED_NOT_FOR_SALE */
        return (in_array($this->getSubject()->getStatus(), array(
            Reservation::STATUS_BLOCKED_OWNER,
            Reservation::STATUS_BLOCKED_NOT_FOR_SALE,
            Reservation::STATUS_BLOCKED_OTHER_AGENCY
        )) || $this->getSubject()->getManagerId() === $user->getId());
    }

    /**
     * @param \Sonata\AdminBundle\Form\FormMapper $formMapper
     *
     * @return void
     */

    /*

    protected function configureFormFields(FormMapper $formMapper)
    {
        $statusChoices = $this->getSubject()->getAllowedStatuses();

        $formMapper
            ->with('General information')
                ->add('id', null, array('required' => false, 'label' => 'ID', 'disabled' => true))
                ->add('status', 'choice', array('required' => true, 'label' => 'Status', 'choices' => $statusChoices))
                ->add('estate', null, array('required' => false, 'label' => 'Estate'))
                ->add('user', null, array('required' => false, 'label' => 'User'))
                ->add('fromDate', null, array('required' => true, 'label' => 'From Date'))
                ->add('toDate', null, array('required' => true, 'label' => 'To Date'))
                //->add('createdAt', null, array('required' => false, 'label' => 'booking date', 'disabled' => true))
                ->add('totalPrice', null, array('required' => true, 'label' => 'Total Price'))
                ->add('paid', null, array('required' => false, 'label' => 'Paid'))
            ->end()
        ;
    }

    */
    /**
     * @param \Sonata\AdminBundle\Datagrid\ListMapper $listMapper
     *
     * @return void
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('id', null, array('label' => 'ID'))
            ->add('estate', null, array('label' => 'property name'))
            ->add('fromDate', null, array('label' => 'from date'))
            ->add('toDate', null, array('label' => 'to date'))
            ->add('_userName', null, array('label' => 'name', 'template' => 'UTTReservationBundle:ReservationAdmin:list_type_user_name.html.twig'))
            ->add('sleeps', null, array('label' => 'guests'))
            ->add('_nights', null, array('label' => 'nights', 'template' => 'UTTReservationBundle:ReservationAdmin:list_type_nights.html.twig'))
            ->add('_status', null, array('label' => 'status', 'template' => 'UTTReservationBundle:ReservationAdmin:list_type_status_name.html.twig'))
            ->add('_action', 'actions', array(
                'actions' => array(
                    'view' => array(),
                    'edit' => array(),
                    'delete' => array(),
                )
            ))
        ;
    }

    /**
     * @param \Sonata\AdminBundle\Datagrid\DatagridMapper $datagridMapper
     *
     * @return void
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {

        $datagridMapper
            ->add('id', null, array('label' => 'ID'))
            ->add('userData', null, array('label' => 'user data'))
            ->add('userName', 'doctrine_orm_callback',
                array(
                    'callback' => array($this, 'getSearchFilterUserData'),
                    'label' => 'user firstname and surname'
                )
            )
            ->add('estate', null, array('label' => 'property name'));
    }

    public function getSearchFilterUserData($queryBuilder, $alias, $field, $value) {
        if (!$value['value']) {
            return;
        }
        $valueFormated = preg_replace('/\s+/', '', $value['value']);
        $queryBuilder
            ->andWhere("CONCAT(JSON_UNQUOTE(JSON_EXTRACT(".$alias.".userData, :firstName)),JSON_UNQUOTE(JSON_EXTRACT(".$alias.".userData, :lastName))) LIKE :value ")
            ->setParameter('firstName', '$.firstName')
            ->setParameter('lastName', '$.lastName')
            ->setParameter('value', '%'.$valueFormated.'%');

        return true;
    }

    /**
     * Remove possibility to delete object.
     *
     * @param RouteCollection $collection
     */
    public function configureRoutes(RouteCollection $collection)
    {
        $collection->remove('delete');
        $collection->remove('edit');
        $collection->remove('batch');
        $collection->remove('export');
        $collection->remove('acl');
        $collection->remove('create');
        $collection->add('requote', $this->getRouterIdParameter().'/requote');
        $collection->add('cancel', $this->getRouterIdParameter().'/cancel');
        $collection->add('reinstate', $this->getRouterIdParameter().'/reinstate');
    }

}

?>
