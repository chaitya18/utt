<?php

namespace UTT\ReservationBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Show\ShowMapper;

use Sonata\AdminBundle\Route\RouteCollection;

use Knp\Menu\ItemInterface as MenuItemInterface;
use UTT\AdminBundle\Service\UTTAclService;

class OfferAdmin extends Admin
{
    /** @var  UTTAclService */
    private $uttAclService;

    public function setUttAclService(UTTAclService $uttAclService){
        $this->uttAclService = $uttAclService;
    }

    public function createQuery($context = 'list')
    {
        $user = $this->uttAclService->userAuth();
        if($user && $this->uttAclService->isGrantedOwner()){
            $query = parent::createQuery($context);
            $query->innerJoin($query->getRootAlias().'.estates', 'eeee');
            $query->innerJoin('eeee.ownerUsers', 'ououou', 'WITH', $query->expr()->in('ououou.id', ':userId'));
            $query->setParameter('userId', $user->getId());

            return $query;
        }

        return parent::createQuery($context);
    }

    /**
     * @param \Sonata\AdminBundle\Form\FormMapper $formMapper
     *
     * @return void
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $typeChoices = $this->getSubject()->getAllowedTypes();
        $statusChoices = $this->getSubject()->getAllowedStatuses();
        $formMapper
            ->with('general information')
                //->add('id', null, array('required' => false, 'label' => 'ID', 'disabled' => true))
                ->add('type', 'choice', array('required' => true, 'label' => 'type', 'choices' => $typeChoices, 'attr' => array('class' => 'form-control')))
                ->add('status', 'choice', array('required' => true, 'label' => 'status', 'choices' => $statusChoices, 'attr' => array('class' => 'form-control')))
                ->add('name', null, array('required' => true, 'label' => 'name', 'attr' => array('class' => 'form-control')))
                ->add('descriptionShort', null, array('required' => true, 'label' => 'description short', 'attr' => array('class' => 'form-control')))
                ->add('description', 'ckeditor', array('required' => true, 'label' => 'description'))
                ->add('validFrom', null, array('required' => true, 'label' => 'valid from'))
                ->add('validTo', null, array('required' => true, 'label' => 'valid to'))
        ;
        if($this->uttAclService->isGrantedOwner()){
            $user = $this->uttAclService->userAuth();
            $formMapper
                ->add('estates', null, array('required' => true, 'label' => 'estates', 'choices' => $this->getConfigurationPool()->getContainer()->get('doctrine')->getEntityManager()->getRepository("UTTEstateBundle:Estate")->getActiveOwnedByUser($user->getId())));
        }else{
            $formMapper
                ->add('estates', null, array('required' => true, 'label' => 'estates'));
        }
        $formMapper
                ->add('value', null, array('required' => true, 'label' => 'value', 'attr' => array('class' => 'form-control')))
                ->add('file', 'file', array('required' => false, 'label' => 'image'))
                ->add('image', null, array('label' => 'image preview', 'attr' => array('class' => 'offerImagePreview', 'data-upload-dir' => $this->getSubject()->getUploadDir())))
            ->end()
        ;
    }

    /**
     * @param \Sonata\AdminBundle\Datagrid\ListMapper $listMapper
     *
     * @return void
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('id', null, array('label' => 'ID'))
            ->addIdentifier('name', null, array('label' => 'name'))
            ->add('descriptionShort', null, array('label' => 'description short'))
            ->add('validFrom', null, array('label' => 'valid from'))
            ->add('validTo', null, array('label' => 'valid to'))
            ->add('estates', null, array('label' => 'estates'))
            ->add('value', null, array('label' => 'value'))
            ->add('_action', 'actions', array(
                'actions' => array(
                    'view' => array(),
                    'edit' => array(),
                    'delete' => array(),
                )
            ))
        ;
    }

    /**
     * @param \Sonata\AdminBundle\Datagrid\DatagridMapper $datagridMapper
     *
     * @return void
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('id', null, array('label' => 'ID'))
            ->add('name', null, array('label' => 'name'))
            ->add('estates', null, array('label' => 'property name'))
        ;
    }

    /**
     * Remove possibility to delete object.
     *
     * @param RouteCollection $collection
     */
    public function configureRoutes(RouteCollection $collection)
    {
        $collection->remove('batch');
        $collection->remove('acl');
        $collection->remove('export');
        $collection->remove('show');
    }

    public function prePersist($block){
        $this->saveImage($block);
    }

    public function preUpdate($block){
        $this->saveImage($block);
    }

    public function saveImage($object){
        $basepath = $this->getRequest()->getBasePath();
        $object->uploadImage($basepath);
    }

}

?>
