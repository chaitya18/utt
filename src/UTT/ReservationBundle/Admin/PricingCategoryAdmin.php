<?php

namespace UTT\ReservationBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Show\ShowMapper;

use Sonata\AdminBundle\Route\RouteCollection;

use Knp\Menu\ItemInterface as MenuItemInterface;
use UTT\ReservationBundle\Entity\PricingCategory;
use UTT\ReservationBundle\Entity\PricingCategoryPriceStandardRepository;
use UTT\ReservationBundle\Entity\PricingCategoryPriceFlexibleRepository;
use Doctrine\ORM\EntityManager;
use UTT\EstateBundle\Entity\Estate;
use UTT\EstateBundle\Entity\EstateRepository;
use UTT\QueueBundle\Service\PricingGenerateQueueService;
use UTT\QueueBundle\Entity\PricingGenerateQueue;

class PricingCategoryAdmin extends Admin
{
    /**
     * {@inheritdoc}
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $typeChoices = $this->getSubject()->getAllowedTypes();
        $currentYear = date('Y');

        $pricingCategoryRepository = $this->getConfigurationPool()->getContainer()->get('doctrine')->getManager()->getRepository('UTTReservationBundle:PricingCategory');
        $pricingCategories = $pricingCategoryRepository->findAll();
        $pricingCategoriesChoices = array();
        foreach($pricingCategories as $pricingCategory){
            $pricingCategoriesChoices[$pricingCategory->getId()] = $pricingCategory->getName();
        }

        $formMapper
            ->with('select pricing category')
                ->add('_selectPricingCategory', 'choice', array('mapped' => false, 'required' => false, 'attr' => array('class' => 'form-control initSelectPricingCategory'), 'choices' => $pricingCategoriesChoices))
            ->with('general information')
                //->add('id', null, array('required' => false, 'label' => 'ID', 'disabled' => true))
                ->add('name', null, array('required' => true, 'label' => 'name'))
                ->add('type', 'choice', array('required' => true, 'label' => 'type', 'disabled' => true, 'choices' => $typeChoices))
            ->end()
        ;

        $objectType = $this->getSubject()->getType();

        $_em = $this->getConfigurationPool()->getContainer()->get('doctrine')->getManager();

        if($objectType == PricingCategory::TYPE_STANDARD_M_F){
            $dataStandard = $_em->getRepository('UTTReservationBundle:PricingCategoryPriceStandard')->getByPricingCategoryOrderByYear($this->getSubject()->getId(), $currentYear);

            $formMapper->add('pricesStandard', 'sonata_type_collection', array(
                'required' => true,
                'label' => 'prices standard',
                'type_options' => array(
                    'delete' => false
                ),
                'data' => ($dataStandard ? $dataStandard : array())
            ), array(
                'edit' => 'inline',
                'inline' => 'table',
                'sortable' => 'position',
            ));
        }

        if($objectType == PricingCategory::TYPE_FLEXIBLE_BOOKING){
            $dataFlexible = $_em->getRepository('UTTReservationBundle:PricingCategoryPriceFlexible')->getByPricingCategoryOrderByYear($this->getSubject()->getId(), $currentYear);

            $formMapper->add('pricesFlexible', 'sonata_type_collection', array(
                'required' => true,
                'label' => 'prices flexible',
                'type_options' => array(
                    'delete' => false
                ),
                'data' => ($dataFlexible ? $dataFlexible : array())
            ), array(
                'edit' => 'inline',
                'inline' => 'table',
                'sortable' => 'position',
            ));
        }

        $formMapper->add('_toolkit', 'text', array('mapped' => false, 'required' => false, 'label' => 'toolkit', 'attr' => array('class' => 'initPricingCategoryToolkit initPricingCategoryYearLine')));

        $nowDate = new \DateTime('now');
        $year1 = $nowDate->format('Y');
        $year2 = $nowDate->modify('+1 year')->format('Y');
        $year3 = $nowDate->modify('+1 year')->format('Y');
        $year4 = $nowDate->modify('+1 year')->format('Y');
        $year5 = $nowDate->modify('+1 year')->format('Y');


        $formMapper
            ->with('generate prices for this price category')
                ->add('generatePricesRun', 'checkbox', array('mapped' => false, 'required' => false, 'label' => 'generate for..'))
                ->add('generatePricesYear', 'choice', array('mapped' => false, 'required' => false, 'label' => 'select years', 'choices' => array(
                    $year1 => $year1,
                    $year2 => $year2,
                    $year3 => $year3,
                    $year4 => $year4,
                    $year5 => $year5,
                ), 'expanded' => true))
                ->add('generatePricesOption', 'choice', array('mapped' => false, 'required' => false, 'label' => 'generate price option', 'attr' => array('class' => 'form-control'), 'choices' => array(
                    'increase5' => 'Increase 5',
                    'increase10' => 'Increase 10',
                    'decrease5' => 'Decrease 5',
                    'decrease10' => 'Decrease 10',
                )))
                ->add('generatePricesImportFrom', 'entity', array('mapped' => false, 'required' => false, 'label' => 'import from', 'attr' => array('class' => 'form-control'),
                    'class' => 'UTTReservationBundle:PricingCategory',
                    'choices' => $this->getConfigurationPool()->getContainer()->get('doctrine')->getEntityManager()->getRepository("UTTReservationBundle:PricingCategory")->findBy(array(
                        'type' => $objectType,
                    ))
                ))
            ->end()
        ;
    }

    /**
     * {@inheritdoc}
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            //->addIdentifier('id', null, array('label' => 'ID'))
            ->add('name', null, array('label' => 'name'))
            ->add('_action', 'actions', array(
                'actions' => array(
                    'view' => array(),
                    'edit' => array(),
                    'delete' => array(),
                )
            ))
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function configureRoutes(RouteCollection $collection)
    {
        $collection->remove('create');
        $collection->remove('show');
        $collection->remove('delete');
        $collection->remove('acl');
    }

    public function preUpdate($object){
        $this->pricingGenerateCreateQueue();
    }

    public function prePersist($object){
        $this->pricingGenerateCreateQueue();
    }

    private function pricingGenerateCreateQueue(){
        /** @var EntityManager $_em */
        $_em = $this->getConfigurationPool()->getContainer()->get('doctrine')->getManager();
        /** @var EstateRepository $estateRepository */
        $estateRepository = $_em->getRepository('UTTEstateBundle:Estate');
        /** @var PricingGenerateQueueService $pricingGenerateQueueService */
        $pricingGenerateQueueService = $this->getConfigurationPool()->getContainer()->get('utt.pricinggeneratequeueservice');

        $estates = $estateRepository->getAllActive();
        if($estates){
            /** @var Estate $estate */
            foreach($estates as $estate){
                $pricingGenerateQueueService->pushToQueue($estate);
            }
        }
    }
}

?>
