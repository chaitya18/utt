<?php

namespace UTT\ReservationBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Show\ShowMapper;

use Sonata\AdminBundle\Route\RouteCollection;

use Knp\Menu\ItemInterface as MenuItemInterface;

use UTT\ReservationBundle\Entity\PricingCategory;
use UTT\ReservationBundle\Entity\PricingCategoryPriceStandardRepository;
use UTT\ReservationBundle\Entity\PricingCategoryPriceFlexibleRepository;
use Doctrine\ORM\EntityManager;
use UTT\EstateBundle\Entity\Estate;
use UTT\EstateBundle\Entity\EstateRepository;
use UTT\QueueBundle\Service\PricingGenerateQueueService;
use UTT\QueueBundle\Entity\PricingGenerateQueue;

class PricingSeasonAdmin extends Admin
{
    /**
     * {@inheritdoc}
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $typeChoices = $this->getSubject()->getAllowedTypes();
        $nameChoices = $this->getSubject()->getAllowedNames();

        $formMapper
            ->with('general information')
                //->add('id', null, array('required' => false, 'label' => 'ID', 'disabled' => true))
                ->add('name', 'choice', array('required' => true, 'label' => 'name', 'choices' => $nameChoices))
                ->add('type', 'choice', array('required' => true, 'label' => 'type', 'choices' => $typeChoices))
                ->add('fromDate', null, array('required' => true, 'label' => 'from date'))
                ->add('toDate', null, array('required' => true, 'label' => 'to date'))
            ->end()
        ;
    }

    /**
     * {@inheritdoc}
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            //->addIdentifier('id', null, array('label' => 'ID'))
            ->add('fromDate', null, array('label' => 'from date'))
            ->add('toDate', null, array('label' => 'to date'))
            ->add('name', null, array('label' => 'name', 'template' => 'UTTReservationBundle:PricingSeasonAdmin:list_type_name.html.twig'))
            ->add('type', null, array('label' => 'type', 'template' => 'UTTReservationBundle:PricingSeasonAdmin:list_type_type.html.twig'))
            ->add('_action', 'actions', array(
                'actions' => array(
                    'view' => array(),
                    'edit' => array(),
                    'delete' => array(),
                )
            ))
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function configureRoutes(RouteCollection $collection)
    {
        $collection->remove('show');
        $collection->remove('acl');
    }

    public function preUpdate($object){
        $this->pricingGenerateCreateQueue();
    }

    private function pricingGenerateCreateQueue(){
        /** @var EntityManager $_em */
        $_em = $this->getConfigurationPool()->getContainer()->get('doctrine')->getManager();
        /** @var EstateRepository $estateRepository */
        $estateRepository = $_em->getRepository('UTTEstateBundle:Estate');
        /** @var PricingGenerateQueueService $pricingGenerateQueueService */
        $pricingGenerateQueueService = $this->getConfigurationPool()->getContainer()->get('utt.pricinggeneratequeueservice');

        $estates = $estateRepository->getAllActive();
        if($estates){
            /** @var Estate $estate */
            foreach($estates as $estate){
                $pricingGenerateQueueService->pushToQueue($estate);
            }
        }
    }
}

?>
