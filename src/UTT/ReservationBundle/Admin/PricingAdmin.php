<?php

namespace UTT\ReservationBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Show\ShowMapper;

use Sonata\AdminBundle\Route\RouteCollection;

use Knp\Menu\ItemInterface as MenuItemInterface;

class PricingAdmin extends Admin
{
    /**
     * @param \Sonata\AdminBundle\Show\ShowMapper $showMapper
     *
     * @return void
     */
    protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper
            ->with('general information')
                //->add('id', null, array('label' => 'ID'))
                ->add('estate', null, array('label' => 'estate'))
                ->add('fromDate', null, array('label' => 'from date'))
                ->add('toDate', null, array('label' => 'to date'))
                ->add('price', null, array('label' => 'price'))
                ->add('initialPrice', null, array('label' => 'standing charge'))
            ->end()
        ;
    }

    /**
     * @param \Sonata\AdminBundle\Form\FormMapper $formMapper
     *
     * @return void
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->with('general information')
                //->add('id', null, array('required' => false, 'label' => 'ID', 'disabled' => true))
                ->add('estate', null, array('required' => true, 'label' => 'estate'))
                ->add('fromDate', null, array('required' => true, 'label' => 'from date'))
                ->add('toDate', null, array('required' => true, 'label' => 'to date'))
                ->add('price', null, array('required' => true, 'label' => 'price'))
                ->add('initialPrice', null, array('required' => true, 'label' => 'standing charge'))
            ->end()
        ;
    }

    /**
     * @param \Sonata\AdminBundle\Datagrid\ListMapper $listMapper
     *
     * @return void
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            //->addIdentifier('id', null, array('label' => 'ID'))
            ->add('estate', null, array('label' => 'estate'))
            ->add('fromDate', null, array('label' => 'from date'))
            ->add('toDate', null, array('label' => 'to date'))
            ->add('price', null, array('label' => 'price'))
            ->add('initialPrice', null, array('label' => 'standing charge'))
            ->add('_action', 'actions', array(
                'actions' => array(
                    'view' => array(),
                    'edit' => array(),
                    'delete' => array(),
                )
            ))
        ;
    }

    /**
     * @param \Sonata\AdminBundle\Datagrid\DatagridMapper $datagridMapper
     *
     * @return void
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        /*
        $datagridMapper
            ->add('id', null, array('label' => 'ID'))
            ->add('code', null, array('label' => 'code'))
        ;
        */
    }

    /**
     * Remove possibility to delete object.
     *
     * @param RouteCollection $collection
     */
    public function configureRoutes(RouteCollection $collection)
    {
        $collection->remove('create');
        $collection->remove('delete');
        $collection->remove('list');
        $collection->remove('acl');
    }

}

?>
