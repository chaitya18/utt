<?php

namespace UTT\ReservationBundle\Payment;

use Doctrine\ORM\EntityManager;
use UTT\IndexBundle\Service\ConfigurationService;
use UTT\ReservationBundle\Entity\Reservation;
use UTT\IndexBundle\Entity\Configuration;
use \Symfony\Component\Routing\Router;
use UTT\ReservationBundle\Entity\PaymentTransaction;
use UTT\ReservationBundle\Entity\PaymentTransactionRepository;
use UTT\ReservationBundle\Entity\Voucher;
use UTT\ReservationBundle\Entity\VoucherPaymentTransaction;
use UTT\ReservationBundle\Entity\VoucherPaymentTransactionRepository;


class SmartPayFormService {
    protected $_em;
    protected $configurationService;
    protected $router;

    CONST urlSimulator = 'https://test.barclaycardsmartpay.com/hpp/pay.shtml';
    CONST urlLive = 'https://live.barclaycardsmartpay.com/hpp/pay.shtml';
    CONST currencyCode = 'GBP';
    CONST shopperLocale = 'en_GB';

    private $url;
    private $skinCodeReservation;
    private $skinCodeVoucher;
    private $merchantAccount;
    private $sharedSecret;

    public function __construct(EntityManager $em, ConfigurationService $configurationService, Router $router){
        $this->_em = $em;
        $this->configurationService = $configurationService;
        $this->router = $router;

        $this->getConfigurationParams();
    }

    public function getConfigurationParams(){
        /** @var Configuration $configuration */
        $configuration = $this->configurationService->get();
        if($configuration instanceOf Configuration){
            // get url
            if($configuration->getSmartPaySimulatorMode() == true){
                $this->url = self::urlSimulator;
            }else{
                $this->url = self::urlLive;
            }

            // get payment params
            if($configuration->getSmartPaySkinCodeReservation() && $configuration->getSmartPaySkinCodeVoucher() && $configuration->getSmartPayMerchantAccount() && $configuration->getSmartPaySharedSecret()){
                $this->skinCodeReservation = $configuration->getSmartPaySkinCodeReservation();
                $this->skinCodeVoucher = $configuration->getSmartPaySkinCodeVoucher();
                $this->merchantAccount = $configuration->getSmartPayMerchantAccount();
                $this->sharedSecret = $configuration->getSmartPaySharedSecret();
            }else{
                throw new \Exception('Invalid payment configuration');
            }
        }else{
            throw new \Exception('Invalid payment configuration');
        }
    }

    public function getUrl(){
        return $this->url;
    }

    public function getCurrencyCode(){
        return self::currencyCode;
    }

    public function getShopperLocale(){
        return self::shopperLocale;
    }

    public function getSkinCodeReservation(){
        return $this->skinCodeReservation;
    }

    public function getSkinCodeVoucher(){
        return $this->skinCodeVoucher;
    }

    public function getMerchantAccount(){
        return $this->merchantAccount;
    }

    public function getSharedSecret(){
        return $this->sharedSecret;
    }

    public function generateTransactionCodeReservation(Reservation $reservation){
        $nowDate = new \DateTime('now');
        $transactionCode = $reservation->getId().'_'.substr(md5(uniqid().$reservation->getId().'UTT'.$nowDate->getTimestamp()), 0, 16);

        return $transactionCode;
    }

    public function generateTransactionCodeVoucher(Voucher $voucher){
        $nowDate = new \DateTime('now');
        $transactionCode = $voucher->getId().'_'.substr(md5(uniqid().$voucher->getId().'UTT'.$nowDate->getTimestamp()), 0, 16);

        return $transactionCode;
    }

    public function generateOrderDataReservation(Reservation $reservation){
        // todo renderować html rezerwacaji

        return base64_encode(gzencode($reservation->getId()));
    }

    public function generateOrderDataVoucher(Voucher $voucher){
        return base64_encode(gzencode($voucher->getValue()));
    }

    public function generateMerchantSigReservation($paymentAmount, $shipBeforeDate, $merchantReference, $sessionValidity, $shopperEmail, $shopperReference){
        return $this->generateMerchantSig($this->skinCodeReservation, $paymentAmount, $shipBeforeDate, $merchantReference, $sessionValidity, $shopperEmail, $shopperReference);
    }

    public function generateMerchantSigVoucher($paymentAmount, $shipBeforeDate, $merchantReference, $sessionValidity, $shopperEmail, $shopperReference){
        return $this->generateMerchantSig($this->skinCodeVoucher, $paymentAmount, $shipBeforeDate, $merchantReference, $sessionValidity, $shopperEmail, $shopperReference);
    }

    public function generateMerchantSig($skinCode, $paymentAmount, $shipBeforeDate, $merchantReference, $sessionValidity, $shopperEmail, $shopperReference){
        $hmacData = $paymentAmount.
            self::currencyCode.
            $shipBeforeDate.
            $merchantReference.
            $skinCode.
            $this->merchantAccount.
            $sessionValidity.
            $shopperEmail.
            $shopperReference;

        return base64_encode(hash_hmac('sha1', $hmacData, $this->sharedSecret, true));
    }

    public function findOrCreateVoucherPaymentTransactionByCode($code){
        /** @var VoucherPaymentTransactionRepository $voucherPaymentTransactionRepository */
        $voucherPaymentTransactionRepository = $this->_em->getRepository('UTTReservationBundle:VoucherPaymentTransaction');

        /** @var VoucherPaymentTransaction $voucherPaymentTransaction */
        $voucherPaymentTransaction = $voucherPaymentTransactionRepository->findOneBy(array(
            'code' => $code
        ));
        if(!($voucherPaymentTransaction instanceof VoucherPaymentTransaction)){
            $voucherPaymentTransaction = new VoucherPaymentTransaction();
        }

        return $voucherPaymentTransaction;
    }

    public function findOrCreatePaymentTransactionByCode($code){
        /** @var PaymentTransactionRepository $paymentTransactionRepository */
        $paymentTransactionRepository = $this->_em->getRepository('UTTReservationBundle:PaymentTransaction');

        /** @var PaymentTransaction $paymentTransaction */
        $paymentTransaction = $paymentTransactionRepository->findOneBy(array(
            'code' => $code
        ));
        if(!($paymentTransaction instanceof PaymentTransaction)){
            $paymentTransaction = new PaymentTransaction();
        }

        return $paymentTransaction;
    }

    public function assignResponseParamsToVoucherPaymentTransaction(VoucherPaymentTransaction $voucherPaymentTransaction, $authResult, $pspReference, $merchantReference, $merchantSig, $paymentMethod, $shopperLocale, $merchantReturnData){
        $voucherPaymentTransaction->setSmartPayAuthResult($authResult);
        $voucherPaymentTransaction->setSmartPayPspReference($pspReference);
        $voucherPaymentTransaction->setSmartPayMerchantReference($merchantReference);
        $voucherPaymentTransaction->setSmartPayMerchantSig($merchantSig);
        $voucherPaymentTransaction->setSmartPayPaymentMethod($paymentMethod);
        $voucherPaymentTransaction->setSmartPayShopperLocale($shopperLocale);
        $voucherPaymentTransaction->setSmartPayMerchantReturnData($merchantReturnData);

        return $voucherPaymentTransaction;
    }

    public function assignResponseParamsToPaymentTransaction(PaymentTransaction $paymentTransaction, $authResult, $pspReference, $merchantReference, $merchantSig, $paymentMethod, $shopperLocale, $merchantReturnData){
        $paymentTransaction->setSmartPayAuthResult($authResult);
        $paymentTransaction->setSmartPayPspReference($pspReference);
        $paymentTransaction->setSmartPayMerchantReference($merchantReference);
        $paymentTransaction->setSmartPayMerchantSig($merchantSig);
        $paymentTransaction->setSmartPayPaymentMethod($paymentMethod);
        $paymentTransaction->setSmartPayShopperLocale($shopperLocale);
        $paymentTransaction->setSmartPayMerchantReturnData($merchantReturnData);

        return $paymentTransaction;
    }
}