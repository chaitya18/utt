<?php

namespace UTT\ReservationBundle\Payment\Worldpay;


use UTT\ReservationBundle\Entity\HistoryEntry;
use UTT\ReservationBundle\Entity\PaymentTransaction;
use UTT\ReservationBundle\Entity\Reservation;
use UTT\ReservationBundle\Entity\ReservationRepository;
use UTT\ReservationBundle\Payment\Exception\CreateTransactionException;
use UTT\ReservationBundle\Payment\Exception\DuplicationTransactionException;
use UTT\ReservationBundle\Payment\Exception\NoResultException;
use UTT\ReservationBundle\Payment\Worldpay\Result\Transaction;
use UTT\ReservationBundle\Service\ArrivalInstructionService;
use UTT\ReservationBundle\Service\HistoryEntryService;
use UTT\IndexBundle\Service\EmailService;
use UTT\ReservationBundle\Factory\ReservationFactory;
use UTT\UserBundle\Entity\User;

class TransactionService
{
    protected $paymentTransactionService;

    protected $reservationRepository;

    protected $historyEntryService;

    protected $emailService;

    protected $arrivalInstructionService;

    protected $worldPayConfig;

    protected $paymentResult;

    protected $reservationFactory;


    /**
     * TransactionService constructor.
     * @param \UTT\ReservationBundle\Payment\TransactionService $paymentTransactionService
     * @param ReservationRepository $reservationRepository
     * @param HistoryEntryService $historyEntryService
     * @param EmailService $emailService
     * @param ArrivalInstructionService $arrivalInstructionService
     */
    public function __construct(
        \UTT\ReservationBundle\Payment\TransactionService $paymentTransactionService,
        ReservationRepository $reservationRepository,
        HistoryEntryService $historyEntryService,
        EmailService $emailService,
        ArrivalInstructionService $arrivalInstructionService,
        WorldPayConfig $worldPayConfig,
        ReservationFactory $reservationFactory
    ) {
        $this->paymentTransactionService = $paymentTransactionService;
        $this->reservationRepository = $reservationRepository;
        $this->historyEntryService = $historyEntryService;
        $this->emailService = $emailService;
        $this->arrivalInstructionService = $arrivalInstructionService;
        $this->worldPayConfig = $worldPayConfig;
        $this->reservationFactory = $reservationFactory;
    }


    public function handle(Result $paymentResult)
    {
        try {
            $paymentTransaction = $this->getPaymentTransaction($paymentResult->getCartId());
            if ($this->isValidPaymentResult($paymentResult) && $this->isValidPaymentTransaction($paymentTransaction)) {
                $resultTransaction = $paymentResult->getTransaction();
                if($resultTransaction->isAuthorized()){
                    $this->authorizedPayment($paymentTransaction, $paymentResult);
                }

                if($resultTransaction->isCancelled()){
                    $this->cancelledPayment($paymentTransaction, $resultTransaction);
                }
            }

        } catch (NoResultException $e) {
            throw new CreateTransactionException($e->getMessage());
        }
    }

    /**
     * @param PaymentTransaction $paymentTransaction
     * @param Result $paymentResult
     * @throws DuplicationTransactionException
     */
    public function authorizedPayment(PaymentTransaction $paymentTransaction, Result $paymentResult)
    {
        $this->checkDuplication($paymentTransaction, $paymentResult->getTransaction());
        $this->createTransaction($paymentTransaction, $paymentResult);
        $reservation = $this->updateReservation($paymentTransaction);
        $this->sendThankYouInfo($paymentTransaction);
        $this->addInfoAboutPayment($paymentTransaction);
        $this->sendInfoToLandlord($reservation);
        $this->sendReservationArrivalInstruction($reservation);
    }

    public function cancelledPayment(PaymentTransaction $paymentTransaction, Transaction $resultTransaction)
    {
        $paymentTransaction->setStatus(PaymentTransaction::STATUS_NOT_SUCCESS);
        $this->paymentTransactionService->persist($paymentTransaction);

        /** @var Reservation $reservation */
        $reservation = $paymentTransaction->getReservation();
        if($reservation instanceof Reservation){
            $user = $reservation->getUser();
            if($user){
                $historyEntryComment = $this->historyEntryService->newCommentCardFailed($resultTransaction->getRawAuthMessage());
                $this->historyEntryService->newEntry(HistoryEntry::TYPE_CARD_FAILED, $reservation, $user, $historyEntryComment);
            }
        }
    }

    public function getPaymentTransaction($cartId)
    {
        try {
            return $this->paymentTransactionService->findByCode($cartId);
        } catch (NoResultException $e) {
            throw new CreateTransactionException('Payment '.$cartId.' no exist - '.$e->getMessage(), 404);
        }
    }

    /**
     * @param PaymentTransaction $paymentTransaction
     * @param Transaction $resultTransaction
     * @throws DuplicationTransactionException
     */
    protected function checkDuplication(PaymentTransaction $paymentTransaction, Transaction $resultTransaction)
    {
        if ($this->isDuplication($paymentTransaction, $resultTransaction)) {
            $this->addInfoAboutDuplication($paymentTransaction, $resultTransaction);
            throw new DuplicationTransactionException('Transaction is exist code: '.$paymentTransaction->getCode(), 422);
        }
    }

    /**
     * @param PaymentTransaction $paymentTransaction
     * @param Transaction $resultTransaction
     * @return bool
     */
    protected function isDuplication(PaymentTransaction $paymentTransaction, Transaction $resultTransaction)
    {
        return $paymentTransaction->getSmartPayPspReference() === (string) $resultTransaction->getTransId()
            && $paymentTransaction->getStatus() == PaymentTransaction::STATUS_SUCCESS;

    }

    /**
     * @param PaymentTransaction $paymentTransaction
     * @return bool
     * @throws CreateTransactionException
     */
    protected function isValidPaymentTransaction(PaymentTransaction $paymentTransaction)
    {
        if($paymentTransaction->getReservation() instanceof Reservation) {
            return true;
        }

        throw new CreateTransactionException('No exist reservation for payment code: '.$paymentTransaction->getCode(), 422);
    }

    /**
     * @param Result $paymentResult
     * @return bool
     * @throws CreateTransactionException
     */
    protected function isValidPaymentResult(Result $paymentResult)
    {
        if($paymentResult->getSecret() === $this->worldPayConfig->getSecret()){
            return true;
        }
        throw new CreateTransactionException('Transaction password is incorrect: '.$paymentResult->getCartId(), 422);
    }

    /**
     * @param PaymentTransaction $paymentTransaction
     */
    protected function addInfoAboutDuplication(PaymentTransaction $paymentTransaction)
    {
        $reservation = $paymentTransaction->getReservation();
        $historyEntryComment = $this->historyEntryService->newCommentCardPaymentDuplicated($paymentTransaction->getTypeName(),
            $paymentTransaction->getAmount(), $paymentTransaction->getSmartPayAuthResult(),
            $paymentTransaction->getSmartPayPaymentMethod(), $paymentTransaction->getSmartPayPspReference());
        $this->historyEntryService->newEntry(HistoryEntry::TYPE_CARD_PAYMENT, $reservation, $reservation->getUser(),
            $historyEntryComment);

    }

    /**
     * @param PaymentTransaction $paymentTransaction
     */
    protected function sendThankYouInfo(PaymentTransaction $paymentTransaction)
    {
        $reservation = $paymentTransaction->getReservation();
        $this->emailService->sendThankYouForPaymentEmail($reservation, $paymentTransaction);
        if ($paymentTransaction->getAmountCharityCharge() > 0) {
            $this->emailService->sendThankYouForCharityPaymentEmail($reservation, $paymentTransaction);
        }
    }

    /**
     * @param PaymentTransaction $paymentTransaction
     * @param Result $result
     */
    protected function createTransaction(PaymentTransaction $paymentTransaction, Result $result)
    {
        $paymentTransactionFactory = new PaymentTransactionFactory($this->paymentTransactionService);
        $paymentTransaction = $paymentTransactionFactory->fromResult($paymentTransaction, $result->getTransaction(),
            $result->getCartId(), 'en_GB');
        $this->paymentTransactionService->persist($paymentTransaction);

    }

    /**
     * @param PaymentTransaction $paymentTransaction
     * @return mixed
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    protected function updateReservation(PaymentTransaction $paymentTransaction)
    {
        $reservation = $paymentTransaction->getReservation();
        $reservation = $this->reservationFactory->addCreditCardCharge($reservation,
            $paymentTransaction->getAmountCreditCardCharge());
        $reservation = $this->reservationFactory->addCharityCharge($reservation,
            $paymentTransaction->getAmountCharityCharge());
        $reservation = $this->reservationFactory->addPaymentToReservation($reservation, $paymentTransaction->getAmount());
        $this->reservationRepository->persist($reservation);
        $paymentTransaction->setReservation($reservation);
        return $reservation;

    }

    /**
     * @param PaymentTransaction $paymentTransaction
     */
    protected function addInfoAboutPayment(PaymentTransaction $paymentTransaction)
    {
        $reservation = $paymentTransaction->getReservation();
        $user = $reservation->getUser();

        if ($user instanceof User) {
            $historyEntryComment = $this->historyEntryService->newCommentCardPayment($reservation->getTotalPrice(),
                $paymentTransaction->getTypeName(), $paymentTransaction->getAmount(),
                $paymentTransaction->getSmartPayAuthResult(), $paymentTransaction->getSmartPayPaymentMethod(),
                $paymentTransaction->getSmartPayPspReference());
            $this->historyEntryService->newEntry(HistoryEntry::TYPE_CARD_PAYMENT, $reservation, $user,
                $historyEntryComment);
        }
    }

    /**
     * @param Reservation $reservation
     */
    protected function sendInfoToLandlord(Reservation $reservation)
    {
        $this->emailService->sendForLandlord($reservation, 'Booking payment');
    }

    /**
     * @param Reservation $reservation
     */
    protected function sendReservationArrivalInstruction(Reservation $reservation)
    {
        if($reservation->getPaid() == $reservation->getTotalPrice()){
            $user = $reservation->getUser();
            try{
                $this->arrivalInstructionService->findOrCreate($reservation);
                $sent = $this->emailService->sendReservationArrivalInstructionEmail($reservation);

                if($sent && $user instanceof User){
                    $historyEntryComment = $this->historyEntryService->newCommentArrivalInstructionEmail();
                    $this->historyEntryService->newEntry(HistoryEntry::TYPE_ARRIVAL_INSTRUCTIONS, $reservation, $user, $historyEntryComment);
                }

            }catch(\Exception $e){
                $historyEntryComment = "ARRIVAL INSTRUCTIONS - ERROR ".$e->getMessage();
                $this->historyEntryService->newEntry(HistoryEntry::TYPE_ARRIVAL_INSTRUCTIONS, $reservation, $user, $historyEntryComment);
            }
        }
    }
}