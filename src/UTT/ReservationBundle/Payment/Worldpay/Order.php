<?php

namespace UTT\ReservationBundle\Payment\Worldpay;



use UTT\ReservationBundle\Payment\Price;

class Order
{
    /**
     * @var int
     */
    protected $testMode;

    /**
     * @var int
     */
    protected $installationId;

    /**
     * @var string
     */
    protected $cartId;

    /**
     * @var Price
     */
    protected $price;

    /**
     * @var string
     */
    protected $currency;
    /**
     * +5 days
     * @var int
     */
    protected $authValidTo;
    /**
     * @var string
     */
    protected $signature;

    /**
     * M_userId
     * @var
     */
    protected $userId;
    /**
     * @var string
     */
    protected $merchantAccount;
    /**
     * @var string
     */
    protected $customerEmail;

    /**
     * @return int
     */
    public function getTestMode()
    {
        return $this->testMode;
    }

    /**
     * @param int $testMode
     * @return Order
     */
    public function setTestMode($testMode)
    {
        $this->testMode = $testMode;
        return $this;
    }

    /**
     * @return int
     */
    public function getInstallationId()
    {
        return $this->installationId;
    }

    /**
     * @param int $installationId
     * @return Order
     */
    public function setInstallationId($installationId)
    {
        $this->installationId = $installationId;
        return $this;
    }

    /**
     * @return string
     */
    public function getCartId()
    {
        return $this->cartId;
    }

    /**
     * @param string $cartId
     * @return Order
     */
    public function setCartId($cartId)
    {
        $this->cartId = $cartId;
        return $this;
    }

    /**
     * @return Price
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * @param Price $price
     * @return Order
     */
    public function setPrice($price)
    {
        $this->price = $price;
        return $this;
    }

    /**
     * @return string
     */
    public function getCurrency()
    {
        return $this->currency;
    }

    /**
     * @param string $currency
     * @return Order
     */
    public function setCurrency($currency)
    {
        $this->currency = $currency;
        return $this;
    }

    /**
     * @return int
     */
    public function getAuthValidTo()
    {
        return $this->authValidTo;
    }

    /**
     * @param int $authValidTo
     * @return Order
     */
    public function setAuthValidTo($authValidTo)
    {
        $this->authValidTo = $authValidTo;
        return $this;
    }

    /**
     * @return string
     */
    public function getSignature()
    {
        return $this->signature;
    }

    /**
     * @param string $signature
     * @return Order
     */
    public function setSignature($signature)
    {
        $this->signature = $signature;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getUserId()
    {
        return $this->userId;
    }

    /**
     * @param mixed $userId
     * @return Order
     */
    public function setUserId($userId)
    {
        $this->userId = $userId;
        return $this;
    }

    /**
     * @return string
     */
    public function getMerchantAccount()
    {
        return $this->merchantAccount;
    }

    /**
     * @param string $merchantAccount
     * @return Order
     */
    public function setMerchantAccount($merchantAccount)
    {
        $this->merchantAccount = $merchantAccount;
        return $this;
    }

    /**
     * @return string
     */
    public function getCustomerEmail()
    {
        return $this->customerEmail;
    }

    /**
     * @param string $customerEmail
     * @return Order
     */
    public function setCustomerEmail($customerEmail)
    {
        $this->customerEmail = $customerEmail;
        return $this;
    }
}