<?php

namespace UTT\ReservationBundle\Payment\Worldpay\Result;



class Transaction
{
    const STATUS_AUTHORISED = 'Y';
    const STATUS_CANCELLED = 'C';


    /**
     * @var int
     */
    protected $transId;
    /**
     * @var string
     */
    protected $transStatus;
    /**
     * @var int
     */
    protected $transTime;

    /**
     * @var string
     */
    protected $cardType;
    /**
     * 1 - test 0 - production
     * @var int
     */
    protected $type = 1;

    /**
     * @var string
     */
    protected $rawAuthMessage;

    /**
     * Transaction constructor.
     * @param int $transId
     * @param string $transStatus
     * @param int $transTime
     * @param string $cardType
     */
    public function __construct($transId, $transStatus, $transTime, $cardType)
    {
        $this->transId = $transId;
        $this->transStatus = $transStatus;
        $this->transTime = $transTime;
        $this->cardType = $cardType;
    }


    /**
     * @return int
     */
    public function getTransId()
    {
        return $this->transId;
    }

    /**
     * @return string
     */
    public function getTransStatus()
    {
        return $this->transStatus;
    }

    /**
     * @return bool
     */
    public function isAuthorized()
    {
        return self::STATUS_AUTHORISED === $this->getTransStatus();
    }

    /**
     * @return bool
     */
    public function isCancelled()
    {
        return self::STATUS_CANCELLED === $this->getTransStatus();
    }

    /**
     * @return int
     */
    public function getTransTime()
    {
        return $this->transTime;
    }

    /**
     * @return string
     */
    public function getCardType()
    {
        return $this->cardType;
    }

    /**
     * @return int
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param int $type
     * @return Transaction
     */
    public function setType($type)
    {
        $this->type = $type;
        return $this;
    }

    /**
     * @return string
     */
    public function getRawAuthMessage()
    {
        return $this->rawAuthMessage;
    }

    /**
     * @param string $rawAuthMessage
     * @return Transaction
     */
    public function setRawAuthMessage($rawAuthMessage)
    {
        $this->rawAuthMessage = $rawAuthMessage;
        return $this;
    }

}