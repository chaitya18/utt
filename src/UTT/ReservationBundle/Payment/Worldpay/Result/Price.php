<?php

namespace UTT\ReservationBundle\Payment\Worldpay\Result;



class Price
{

    protected $amount;

    /**
     * @var string
     */
    protected $currency;

    /**
     * Price constructor.
     * @param $amount
     * @param string $currency
     */
    public function __construct($amount, $currency)
    {
        $this->amount = $amount;
        $this->currency = $currency;
    }

    /**
     * @return mixed
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * @return string
     */
    public function getCurrency()
    {
        return $this->currency;
    }




}