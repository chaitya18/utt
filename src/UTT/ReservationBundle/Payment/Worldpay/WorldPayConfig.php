<?php

namespace UTT\ReservationBundle\Payment\Worldpay;


class WorldPayConfig
{
    protected $configuration;
    protected $url;
    protected $installationId;
    protected $merchantAccount;
    protected $secret;
    protected $testMode;
    protected $currencyCode = 'GBP';
    protected $locale = 'en_GB';
    protected $authValidToDays = 5;

    public function __construct($configuration)
    {
        /**
         * @todo change to parameterbag
         */
        $this->configuration = $configuration;

        $this->buildConfigurationParams();
    }

    /**
     *
     */
    protected function buildConfigurationParams()
    {

        if ($this->configuration['test_mode'] > 0) {
            $this->onTestMode();
        } else {
            $this->url = $this->configuration['url_prod'];
            $this->testMode = 0;
        }

        $this->currencyCode = $this->configuration['currency_code'];
        $this->locale = $this->configuration['locale'];
        $this->installationId = $this->configuration['installation_id'];
        $this->merchantAccount = $this->configuration['merchant_account'];
        $this->secret = $this->configuration['secret'];
        $this->authValidToDays = $this->configuration['auth_valid_to_days'];

    }

    /**
     * @return mixed
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * @return mixed
     */
    public function getInstallationId()
    {
        return $this->installationId;
    }

    /**
     * @return mixed
     */
    public function getMerchantAccount()
    {
        return $this->merchantAccount;
    }

    /**
     * @return mixed
     */
    public function getSecret()
    {
        return $this->secret;
    }

    /**
     * @return mixed
     */
    public function getTestMode()
    {
        return $this->testMode;
    }


    public function onTestMode()
    {
        $this->url = $this->configuration['url_test'];
        $this->testMode = 100;
    }

    /**
     * @return mixed
     */
    public function getCurrencyCode()
    {
        return $this->currencyCode;
    }

    /**
     * @return mixed
     */
    public function getLocale()
    {
        return $this->locale;
    }

    /**
     * @return int
     */
    public function getAuthValidToDays()
    {
        return $this->authValidToDays;
    }

}