<?php

namespace UTT\ReservationBundle\Payment\Worldpay;


use UTT\ReservationBundle\Payment\Exception\InvalidDataException;
use UTT\ReservationBundle\Payment\Worldpay\Result\Customer;
use UTT\ReservationBundle\Payment\Worldpay\Result\Price;
use UTT\ReservationBundle\Payment\Worldpay\Result\Transaction;

class Result
{
    /**
     * @var Transaction
     */
    protected $transaction;
    /**
     * @var \UTT\ReservationBundle\Payment\Worldpay\Result\Price
     */
    protected $price;

    /**
     * @var int
     */
    protected $installationId;

    /**
     * @var string
     */
    protected $cartId;

    /**
     * M_userId
     * @var Customer
     */
    protected $customer;
    /**
     * @var string
     */
    protected $secret;

    /**
     * Result constructor.
     * @param Transaction $transaction
     * @param Price $price
     * @param Customer $customer
     * @param $installationId
     * @param $cartId
     * @param null $secret
     */
    public function __construct(
        Transaction $transaction,
        Result\Price $price,
        Customer $customer,
        $installationId,
        $cartId,
        $secret = null
    ) {
        $this->transaction = $transaction;
        $this->price = $price;
        $this->installationId = $installationId;
        $this->cartId = $cartId;
        $this->customer = $customer;
        $this->secret = $secret;
    }


    public static function createFromRequest(array $data)
    {
        /**
         * a:46:{s:7:"country";s:2:"PL";s:8:"authCost";s:6:"280.95";s:7:"msgType";s:10:"authResult";s:8:
         * "routeKey";s:15:"VISA_CREDIT-SSL";s:7:"transId";s:10:"3265462524";s:12:"countryMatch";s:1:"S";s:14:
         * "rawAuthMessage";s:21:"cardbe.msg.authrforised";s:12:"authCurrency";s:3:"GBP";s:7:
         * "charenc";s:5:"UTF-8";s:8:"compName";s:20:"Under The Thatch ltd";s:11:"rawAuthCode";s:1:"A";s:12:
         * "amountString";s:12:"&#163;280.95";s:12:"installation";s:7:"1424214";s:8:"currency";s:3:"GBP";s:3:"tel";s:9:"122803344";s:3:
         * "fax";s:0:"";s:4:"lang";s:2:"pl";s:13:"countryString";s:6:"Poland";s:5:"email";s:16:"jon.z@interia.pl";s:11:
         * "transStatus";s:1:"Y";s:11:"_SP_charEnc";s:5:"UTF-8";s:6:"amount";s:6:"280.95";s:7:"address";s:13:"test 11 test";s:9:"transTime";s:13:"1619182870608";s:4:
         * "cost";s:6:"280.95";s:4:"town";s:4:"test";s:8:"address3";s:0:"";s:8:"address2";s:2:"11";s:8:"address1";s:4:"
         * test";s:6:"cartId";s:22:"74172_b24c2cf7c8fa56cc";s:8:"postcode";s:6:"22-222";s:9:"ipAddress";s:14:"91.218.210.104";s:8:
         * "cardType";s:4:"Visa";s:10:"authAmount";s:6:"280.95";s:8:"M_userId";s:5:"22212";s:8:
         * "authMode";s:1:"A";s:6:"instId";s:7:"1424214";s:14:"displayAddress";s:13:"test 11 test";s:3:"AAV";s:5:"00000";s:8:
         * "testMode";s:3:"100";s:4:"name";s:10:"test testd";s:10:"callbackPW";s:0:"";s:6:"region";s:0:"";s:3:
         * "AVS";s:4:"1111";s:4:"desc";s:0:"";s:16:"authAmountString";s:12:"&#163;280.95";}
         */
        self::validate($data);
        $price = new Price($data['authAmount'], $data['authCurrency']);
        $transaction = new Transaction($data['transId'], $data['transStatus'], $data['transTime'], $data['cardType']);
        $transaction->setType((int)$data['testMode'] === 100 ? 1: 0);
        $transaction->setRawAuthMessage($data['rawAuthMessage']);
        $customer = new Customer($data['M_userId'], $data['email']);
        return new self($transaction, $price, $customer, $data['instId'], $data['cartId'], $data['callbackPW'] );

    }

    /**
     * @param array $data
     * @throws InvalidDataException
     */
    protected static function validate(array $data)
    {
        if(!(isset($data['authAmount']) && (int) $data['authAmount'] > 0))
        {
            throw new InvalidDataException('Invalid field authAmount:'.$data['authAmount'].' code:'.$data['cartId'], 422);
        }

        if(empty($data['authCurrency']))
        {
            throw new InvalidDataException('Invalid field authCurrency:'.$data['authCurrency'].' code:'.$data['cartId'], 422);
        }

        if(empty($data['transId']) || empty($data['transStatus']))
        {
            throw new InvalidDataException('Invalid fields transaction:'.$data['transId'].' code:'.$data['cartId'], 422);
        }
    }

    /**
     * @return Transaction
     */
    public function getTransaction()
    {
        return $this->transaction;
    }

    /**
     * @return Price
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * @return int
     */
    public function getInstallationId()
    {
        return $this->installationId;
    }

    /**
     * @return string
     */
    public function getCartId()
    {
        return $this->cartId;
    }

    /**
     * @return Customer
     */
    public function getCustomer()
    {
        return $this->customer;
    }

    /**
     * @return string
     */
    public function getSecret()
    {
        return $this->secret;
    }

}