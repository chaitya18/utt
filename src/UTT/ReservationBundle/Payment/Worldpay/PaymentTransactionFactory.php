<?php

namespace UTT\ReservationBundle\Payment\Worldpay;

use UTT\ReservationBundle\Entity\PaymentTransaction;
use UTT\ReservationBundle\Payment\TransactionService as PaymentTransactionService;
use UTT\ReservationBundle\Payment\Worldpay\Result\Transaction;


class PaymentTransactionFactory
{
    /**
     * @var string[]
     */
    protected $transactionStatusMap = array(
        'Y' => 'AUTHORISED',
        'C' => 'CANCELLED'
    );

    protected $transactionService;


    public function __construct(PaymentTransactionService $transactionService)
    {
        $this->transactionService = $transactionService;
    }


    /**
     * @param PaymentTransaction $paymentTransaction
     * @param Transaction $transaction
     * @param $cartId
     * @param $locale
     * @return PaymentTransaction
     */
    public function fromResult(
        PaymentTransaction $paymentTransaction,
        Transaction $transaction,
        $cartId,
        $locale
        
    ) {
        $paymentTransaction->setSmartPayAuthResult($this->mapTransactionStatus($transaction->getTransStatus()));
        $paymentTransaction->setSmartPayPspReference($transaction->getTransId());
        $paymentTransaction->setSmartPayMerchantReference($cartId);
        $paymentTransaction->setSmartPayPaymentMethod(strtolower($transaction->getCardType()));
        $paymentTransaction->setSmartPayShopperLocale($locale);
        $paymentTransaction->setPaymentProvider(PaymentTransaction::PAYMENT_PROVIDER_WORLDPAY);
        $this->buildStatus($paymentTransaction);

        return $paymentTransaction;
    }



    /**
     * @param PaymentTransaction $paymentTransaction
     */
    protected function buildStatus(PaymentTransaction $paymentTransaction)
    {
        if($paymentTransaction->isPaymentAuthSuccess()){
            $paymentTransaction->setStatus(PaymentTransaction::STATUS_SUCCESS);
        } else {
            $paymentTransaction->setStatus(PaymentTransaction::STATUS_NOT_SUCCESS);
        }
    }

    /**
     * @param $status
     * @return string|null
     */
    protected function mapTransactionStatus($status)
    {
        return isset($this->transactionStatusMap[$status]) ? $this->transactionStatusMap[$status]: null;
    }
}