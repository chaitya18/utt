<?php

namespace UTT\ReservationBundle\Payment\Worldpay\Voucher;


use UTT\ReservationBundle\Entity\DiscountCode;
use UTT\ReservationBundle\Entity\Voucher;
use UTT\ReservationBundle\Entity\VoucherHistoryEntry;
use UTT\ReservationBundle\Entity\VoucherPaymentTransaction;
use UTT\ReservationBundle\Entity\VoucherRepository;
use UTT\ReservationBundle\Factory\VoucherFactory;
use UTT\ReservationBundle\Payment\Exception\CreateTransactionException;
use UTT\ReservationBundle\Payment\Exception\DuplicationTransactionException;
use UTT\ReservationBundle\Payment\Exception\NoResultException;
use UTT\ReservationBundle\Payment\Worldpay\Result;
use UTT\ReservationBundle\Payment\Worldpay\Result\Transaction;
use UTT\ReservationBundle\Payment\Worldpay\WorldPayConfig;
use UTT\ReservationBundle\Service\DiscountCodeService;
use UTT\IndexBundle\Service\EmailService;
use UTT\ReservationBundle\Service\VoucherHistoryEntryService;
use UTT\UserBundle\Entity\User;

class TransactionService
{
    protected $paymentTransactionService;

    protected $historyEntryService;

    protected $voucherFactory;

    protected $voucherRepository;

    protected $emailService;

    protected $discountCodeService;

    protected $worldPayConfig;

    /**
     * TransactionService constructor.
     * @param \UTT\ReservationBundle\Payment\TransactionService $paymentTransactionService
     * @param VoucherFactory $voucherFactory
     * @param VoucherRepository $voucherRepository
     * @param VoucherHistoryEntryService $historyEntryService
     * @param EmailService $emailService
     * @param DiscountCodeService $discountCodeService
     * @param WorldPayConfig $worldPayConfig
     */
    public function __construct(
        \UTT\ReservationBundle\Payment\TransactionService $paymentTransactionService,
        VoucherFactory $voucherFactory,
        VoucherRepository $voucherRepository,
        VoucherHistoryEntryService $historyEntryService,
        EmailService $emailService,
        DiscountCodeService $discountCodeService,
        WorldPayConfig $worldPayConfig
    ) {
        $this->paymentTransactionService = $paymentTransactionService;
        $this->historyEntryService = $historyEntryService;
        $this->voucherFactory = $voucherFactory;
        $this->voucherRepository = $voucherRepository;
        $this->emailService = $emailService;
        $this->discountCodeService = $discountCodeService;
        $this->worldPayConfig = $worldPayConfig;
    }


    public function handle(Result $paymentResult)
    {
        try {
            $paymentTransaction = $this->getPaymentTransaction($paymentResult->getCartId());
            if ($this->isValidPaymentResult($paymentResult) && $this->isValidPaymentTransaction($paymentTransaction)) {
                $resultTransaction = $paymentResult->getTransaction();
                if($resultTransaction->isAuthorized()){
                    $this->authorizedPayment($paymentTransaction, $paymentResult);
                }

                if($resultTransaction->isCancelled()){
                    $this->cancelledPayment($paymentTransaction, $resultTransaction);
                }
            }

        } catch (NoResultException $e) {
            throw new CreateTransactionException($e->getMessage());
        }
    }

    /**
     * @param VoucherPaymentTransaction $paymentTransaction
     * @param Result $paymentResult
     * @throws DuplicationTransactionException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function authorizedPayment(VoucherPaymentTransaction $paymentTransaction, Result $paymentResult)
    {
        $this->checkDuplication($paymentTransaction, $paymentResult->getTransaction());
        $this->createTransaction($paymentTransaction, $paymentResult);
        $this->addInfoAboutPayment($paymentTransaction);
        $voucher = $this->updateVoucher($paymentTransaction);
        $this->createVoucherDiscount($voucher);
        $this->voucherRepository->persist($voucher);
        if($voucher->isDiscount())
        {
            $this->sendInfoAboutVoucherDiscount($voucher);
        }
    }

    public function cancelledPayment(VoucherPaymentTransaction $paymentTransaction, Transaction $resultTransaction)
    {
        $paymentTransaction->setStatus(VoucherPaymentTransaction::STATUS_NOT_SUCCESS);
        $this->paymentTransactionService->persistVoucher($paymentTransaction);

        $voucher = $paymentTransaction->getVoucher();
        if($voucher instanceof Voucher){
            $user = $voucher->getUser();
            if($user){
                $historyEntryComment = $this->historyEntryService->newCommentCardFailed($resultTransaction->getRawAuthMessage());
                $this->historyEntryService->newEntry(VoucherHistoryEntry::TYPE_CARD_FAILED, $voucher, $user, $historyEntryComment);
            }
        }
    }

    public function getPaymentTransaction($cartId)
    {
        try {
            return $this->paymentTransactionService->findVoucherByCode($cartId);
        } catch (NoResultException $e) {
            throw new CreateTransactionException('Payment '.$cartId.' no exist - '.$e->getMessage(), 404);
        }
    }

    /**
     * @param VoucherPaymentTransaction $paymentTransaction
     * @param Transaction $resultTransaction
     * @throws DuplicationTransactionException
     */
    protected function checkDuplication(VoucherPaymentTransaction $paymentTransaction, Transaction $resultTransaction)
    {
        if ($this->isDuplication($paymentTransaction, $resultTransaction)) {
            throw new DuplicationTransactionException('Transaction is exist code: '.$paymentTransaction->getCode(), 422);
        }
    }

    /**
     * @param VoucherPaymentTransaction $paymentTransaction
     * @param Transaction $resultTransaction
     * @return bool
     */
    protected function isDuplication(VoucherPaymentTransaction $paymentTransaction, Transaction $resultTransaction)
    {
        return $paymentTransaction->getSmartPayPspReference() === (string) $resultTransaction->getTransId()
            && $paymentTransaction->getStatus() == VoucherPaymentTransaction::STATUS_SUCCESS;

    }

    /**
     * @param VoucherPaymentTransaction $paymentTransaction
     * @return bool
     * @throws CreateTransactionException
     */
    protected function isValidPaymentTransaction(VoucherPaymentTransaction $paymentTransaction)
    {
        if($paymentTransaction->getVoucher() instanceof Voucher) {
            return true;
        }

        throw new CreateTransactionException('No exist voucher for payment code: '.$paymentTransaction->getCode(), 422);
    }

    /**
     * @param Result $paymentResult
     * @return bool
     * @throws CreateTransactionException
     */
    protected function isValidPaymentResult(Result $paymentResult)
    {
        if($paymentResult->getSecret() === $this->worldPayConfig->getSecret()){
            return true;
        }
        throw new CreateTransactionException('Transaction password is incorrect: '.$paymentResult->getCartId(), 422);
    }

    /**
     * @param VoucherPaymentTransaction $paymentTransaction
     * @param Result $result
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    protected function createTransaction(VoucherPaymentTransaction $paymentTransaction, Result $result)
    {
        $paymentTransactionFactory = new PaymentTransactionFactory();
        $paymentTransaction = $paymentTransactionFactory->fromResult($paymentTransaction, $result->getTransaction(),
            $result->getCartId(), 'en_GB');
        $this->paymentTransactionService->persistVoucher($paymentTransaction);

    }

    /**
     * @param VoucherPaymentTransaction $paymentTransaction
     * @return false|Voucher
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    protected function updateVoucher(VoucherPaymentTransaction $paymentTransaction)
    {
        $voucher = $paymentTransaction->getVoucher();
        $voucher = $this->voucherFactory->addCreditCardCharge($voucher, $paymentTransaction->getAmountCreditCardCharge());
        $voucher = $this->voucherFactory->addCharityCharge($voucher, $paymentTransaction->getAmountCharityCharge());
        $voucher = $this->voucherFactory->addPaymentToVoucher($voucher, $paymentTransaction->getAmount());
        $paymentTransaction->setVoucher($voucher);
        return $voucher;
    }

    protected function createVoucherDiscount(Voucher $voucher)
    {
        //var_dump($voucher->getPaid(), $voucher->getTotalPrice());
        if($voucher->getPaid() >= $voucher->getTotalPrice()){
            $discountCode = $this->discountCodeService->createVoucherDiscount($voucher);
            if($discountCode instanceof DiscountCode){
                $voucher->setDiscountCode($discountCode);
            }
        }
        return $voucher;
    }

    protected function sendInfoAboutVoucherDiscount(Voucher $voucher)
    {
        $user = $voucher->getUser();
        $discountCode = $voucher->getDiscountCode();
        $emailSent = $this->emailService->sendDiscountCodeEmail($user->getEmail(), $discountCode);
        if($emailSent)
        {
            $voucherHistoryEntryComment = $this->historyEntryService->newCommentVoucherDiscount($discountCode);
            $this->historyEntryService->newEntry(VoucherHistoryEntry::TYPE_VOUCHER_DISCOUNT_CODE, $voucher, $user, $voucherHistoryEntryComment);
        }
    }

    protected function addInfoAboutPayment(VoucherPaymentTransaction $paymentTransaction)
    {
        $voucher = $paymentTransaction->getVoucher();
        $user = $voucher->getUser();
        $voucherHistoryEntryComment = $this->historyEntryService->newCommentCardPayment($voucher->getTotalPrice(), $paymentTransaction->getTypeName(),
            $paymentTransaction->getAmount(), $paymentTransaction->getSmartPayAuthResult(), $paymentTransaction->getSmartPayPaymentMethod(),
            $paymentTransaction->getSmartPayPspReference());
        $this->historyEntryService->newEntry(VoucherHistoryEntry::TYPE_CARD_PAYMENT, $voucher, $user, $voucherHistoryEntryComment);
    }

}