<?php

namespace UTT\ReservationBundle\Payment\Worldpay\Voucher;

use UTT\ReservationBundle\Entity\VoucherPaymentTransaction;
use UTT\ReservationBundle\Payment\Worldpay\Result\Transaction;


class PaymentTransactionFactory
{
    /**
     * @var string[]
     */
    protected $transactionStatusMap = array(
        'Y' => 'AUTHORISED',
        'C' => 'CANCELLED'
    );


    /**
     * @param VoucherPaymentTransaction $paymentTransaction
     * @param Transaction $transaction
     * @param $cartId
     * @param $locale
     * @return VoucherPaymentTransaction
     */
    public function fromResult(
        VoucherPaymentTransaction $paymentTransaction,
        Transaction $transaction,
        $cartId,
        $locale
        
    ) {
        $paymentTransaction->setSmartPayAuthResult($this->mapTransactionStatus($transaction->getTransStatus()));
        $paymentTransaction->setSmartPayPspReference($transaction->getTransId());
        $paymentTransaction->setSmartPayMerchantReference($cartId);
        $paymentTransaction->setSmartPayPaymentMethod(strtolower($transaction->getCardType()));
        $paymentTransaction->setSmartPayShopperLocale($locale);
        $paymentTransaction->setPaymentProvider(VoucherPaymentTransaction::PAYMENT_PROVIDER_WORLDPAY);
        $this->buildStatus($paymentTransaction);

        return $paymentTransaction;
    }



    /**
     * @param PaymentTransaction $paymentTransaction
     */
    protected function buildStatus(VoucherPaymentTransaction $paymentTransaction)
    {
        if($paymentTransaction->isPaymentAuthSuccess()){
            $paymentTransaction->setStatus(VoucherPaymentTransaction::STATUS_SUCCESS);
        } else {
            $paymentTransaction->setStatus(VoucherPaymentTransaction::STATUS_NOT_SUCCESS);
        }
    }

    /**
     * @param $status
     * @return string|null
     */
    protected function mapTransactionStatus($status)
    {
        return isset($this->transactionStatusMap[$status]) ? $this->transactionStatusMap[$status]: null;
    }
}