<?php

namespace UTT\ReservationBundle\Payment\Worldpay;


use UTT\ReservationBundle\Entity\Reservation;
use UTT\ReservationBundle\Entity\Voucher;
use UTT\ReservationBundle\Payment\Price;

class OrderFactory
{
    /**
     * @var WorldPayConfig
     */
    protected $config;

    /**
     * OrderFactory constructor.
     * @param WorldPayConfig $config
     */
    public function __construct(WorldPayConfig $config)
    {
        $this->config = $config;
    }

    /**
     * @param Reservation $reservation
     * @param Price $price
     * @return Order
     */
    public function createFromReservation(Reservation $reservation, Price $price)
    {
        $order = new Order();
        $order->setUserId($reservation->getUser()->getId());
        $order->setMerchantAccount($this->config->getMerchantAccount());
        $order->setCartId($this->generateCartIdFromReservation($reservation));
        $order->setPrice($price);
        $order->setCurrency($this->config->getCurrencyCode());
        $order->setTestMode($this->config->getTestMode());
        $order->setInstallationId($this->config->getInstallationId());
        $order->setCustomerEmail($reservation->getUserDataDecoded()->email);
        $data = new \DateTime();
        // $data->modify('+' . $this->config->getAuthValidToDays() . ' day');
        $order->setAuthValidTo($data->getTimestamp());
        $order->setSignature($this->generateOrderSig($order));
        return $order;

    }

    /**
     * @param Voucher $voucher
     * @param Price $price
     * @return Order
     */
    public function createFromVoucher(Voucher $voucher, Price $price)
    {
        $order = new Order();
        $order->setUserId($voucher->getUser()->getId());
        $order->setMerchantAccount($this->config->getMerchantAccount());
        $order->setCartId($this->generateCartIdFromVoucher($voucher));
        $order->setPrice($price);
        $order->setCurrency($this->config->getCurrencyCode());
        $order->setTestMode($this->config->getTestMode());
        $order->setInstallationId($this->config->getInstallationId());
        $order->setCustomerEmail($voucher->getUserDataDecoded()->email);
        $data = new \DateTime();
        $data->modify('+' . $this->config->getAuthValidToDays() . ' day');
        $order->setAuthValidTo($data->getTimestamp());
        $order->setSignature($this->generateOrderSig($order));
        return $order;

    }

    /**
     * @param Reservation $reservation
     * @return string
     */
    protected function generateCartIdFromReservation(Reservation $reservation)
    {
        $nowDate = new \DateTime('now');
        return $reservation->getId() . '_' . substr(md5(uniqid() . $reservation->getId() . 'UTT' . $nowDate->getTimestamp()),
                0, 16);

    }

    /**
     * @param Voucher $voucher
     * @return string
     */
    protected function generateCartIdFromVoucher(Voucher $voucher)
    {
        $nowDate = new \DateTime('now');
        return $voucher->getId().'_'.substr(md5(uniqid().$voucher->getId().'UTT'.$nowDate->getTimestamp()), 0, 16);
    }

    /**
     * @param Order $order
     * @return string
     */
    protected function generateOrderSig(Order $order)
    {
        return md5($this->config->getSecret() . ':' . $order->getInstallationId() . ':' . $order->getPrice()->getTotal() . ':' . $order->getCurrency() . ':' . $order->getCartId() . ':' . $order->getUserId());
    }
}