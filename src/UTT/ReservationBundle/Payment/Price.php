<?php

namespace UTT\ReservationBundle\Payment;



class Price
{
    protected $total;

    protected $creditCardCharge;

    protected $charityCharge;

    /**
     * Price constructor.
     * @param $total
     * @param $creditCardCharge
     * @param $charityCharge
     */
    public function __construct($total, $creditCardCharge, $charityCharge)
    {
        $this->total = $total;
        $this->creditCardCharge = $creditCardCharge;
        $this->charityCharge = $charityCharge;
    }

    /**
     * @return mixed
     */
    public function getTotal()
    {
        return $this->total;
    }

    /**
     * @return mixed
     */
    public function getCreditCardCharge()
    {
        return $this->creditCardCharge;
    }

    /**
     * @return mixed
     */
    public function getCharityCharge()
    {
        return $this->charityCharge;
    }

    /**
     * @return int
     */
    public function getTotalInt()
    {
        return $this->total * 100;
    }


}