<?php

namespace UTT\ReservationBundle\Payment;

use Doctrine\ORM\EntityManager;
use UTT\ReservationBundle\Entity\PaymentTransaction;
use UTT\ReservationBundle\Entity\PaymentTransactionRepository;
use UTT\ReservationBundle\Entity\VoucherPaymentTransaction;
use UTT\ReservationBundle\Entity\VoucherPaymentTransactionRepository;
use UTT\ReservationBundle\Payment\Exception\NoResultException;


class TransactionService
{
    protected $em;

    public function __construct(EntityManager $em)
    {
        $this->em = $em;

    }

    /**
     * @param $code
     * @return PaymentTransaction
     * @throws NoResultException
     */
    public function findByCode($code)
    {
        /** @var PaymentTransactionRepository $paymentTransactionRepository */
        $paymentTransactionRepository = $this->em->getRepository('UTTReservationBundle:PaymentTransaction');

        /** @var PaymentTransaction $paymentTransaction */
        $paymentTransaction = $paymentTransactionRepository->findOneBy(array(
            'code' => $code
        ));

        if (!($paymentTransaction instanceof PaymentTransaction)) {
            throw new NoResultException('Payment Transaction no exist', 404);
        }

        return $paymentTransaction;
    }

    /**
     * @param $code
     * @return VoucherPaymentTransaction
     * @throws NoResultException
     */
    public function findVoucherByCode($code)
    {
        /** @var VoucherPaymentTransactionRepository $voucherPaymentTransactionRepository */
        $voucherPaymentTransactionRepository = $this->em->getRepository('UTTReservationBundle:VoucherPaymentTransaction');

        /** @var VoucherPaymentTransaction $voucherPaymentTransaction */
        $voucherPaymentTransaction = $voucherPaymentTransactionRepository->findOneBy(array(
            'code' => $code
        ));
        if(!($voucherPaymentTransaction instanceof VoucherPaymentTransaction)){
            throw new NoResultException('Payment Transaction no exist', 404);
        }

        return $voucherPaymentTransaction;
    }

    /**
     * @param PaymentTransaction $paymentTransaction
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function persist(PaymentTransaction $paymentTransaction)
    {
        $this->em->persist($paymentTransaction);
        $this->em->flush();
    }

    /**
     * @param VoucherPaymentTransaction $paymentTransaction
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function persistVoucher(VoucherPaymentTransaction $paymentTransaction)
    {
        $this->em->persist($paymentTransaction);
        $this->em->flush();
    }



}