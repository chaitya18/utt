<?php

namespace UTT\ReservationBundle\Payment;


use UTT\ReservationBundle\Entity\Reservation;
use UTT\ReservationBundle\Entity\Voucher;
use UTT\ReservationBundle\Factory\ReservationFactory;
use UTT\ReservationBundle\Service\PricingService;

class PriceFactory
{
    /**
     * @var WorldPayConfig
     */
    protected $config;
    /**
     * @var ReservationFactory
     */
    protected $reservationFactory;

    protected $pricingService;


    public function __construct(ReservationFactory $reservationFactory, PricingService $pricingService)
    {
        $this->reservationFactory = $reservationFactory;
        $this->pricingService = $pricingService;
    }

    /**
     * @param Reservation $reservation
     * @param bool $applyCreditCardCharge
     * @param bool $charityCharge
     * @return Price
     */
    public function createFromReservation(Reservation $reservation, $applyCreditCardCharge = true, $charityCharge = 0)
    {

            if ($this->reservationFactory->isFullPaymentRequired($reservation)) {
                $toPay = $reservation->getTotalPrice() - $reservation->getPaid();
            } else {
                if ($reservation->getPaid() == 0) {
                    $toPay = $reservation->getDeposit();
                } else {
                    $toPay = $reservation->getTotalPrice() - $reservation->getPaid();
                }
            }

            $creditCardCharge = 0;
            if ($applyCreditCardCharge) {
                $creditCardCharge = $this->pricingService->calculateCreditCardCharge($toPay);
                $toPay = (float)$toPay + (float)$creditCardCharge;
            }

            if ($charityCharge > 0) {
                $toPay = (float)$toPay + (float)$charityCharge;
            }

            return new Price($toPay, $creditCardCharge, $charityCharge);
    }

    /**
     * @param Voucher $voucher
     * @param int $charityCharge
     * @return Price
     */
    public function createFromVoucher(Voucher $voucher, $charityCharge = 0)
    {
        $toPay = $voucher->getTotalPrice() - $voucher->getPaid();

        if($charityCharge > 0){
            $toPay = (float)$toPay + (float)$charityCharge;
        }
        return new Price($toPay, 0, $charityCharge);

    }
}