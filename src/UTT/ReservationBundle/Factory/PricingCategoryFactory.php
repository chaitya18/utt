<?php

namespace UTT\ReservationBundle\Factory;

use Doctrine\ORM\EntityManager;

use UTT\ReservationBundle\Entity\PricingCategory;
use UTT\ReservationBundle\Entity\PricingCategoryPriceStandard;
use UTT\ReservationBundle\Entity\PricingCategoryPriceFlexible;

class PricingCategoryFactory {
    protected $_em;

    public function __construct(EntityManager $em){
        $this->_em = $em;
    }

    public function getPriceStandardForYearAndType(PricingCategory $pricingCategory, $seasonType, \Datetime $date){
        /** @var PricingCategoryPriceStandard $price */
        foreach($pricingCategory->getPricesStandard() as $price){
            if($price->getYear() == $date->format('Y') && $price->getType() == $seasonType){
                return $price;
            }
        }

        return false;
    }

    public function getPriceFlexibleForYearAndType(PricingCategory $pricingCategory, $seasonType, \Datetime $date){
        /** @var PricingCategoryPriceFlexible $price */
        foreach($pricingCategory->getPricesFlexible() as $price){
            if($price->getYear() == $date->format('Y') && $price->getType() == $seasonType){
                return $price;
            }
        }

        return false;
    }

    public function getPriceValueForPriceFlexible(PricingCategoryPriceFlexible $price, \Datetime $dateFrom, \Datetime $dateTo){
        $dateFromDay = $dateFrom->format('N');
        $dateToDay = $dateTo->format('N');

        if($dateFromDay == 5 && $dateToDay == 6){
            return $price->getPriceFridaySaturday();
        }elseif($dateFromDay == 7 && $dateToDay == 4){
            return $price->getPriceSundayThursday();
        }

        return 0;
    }

    public function getPriceValueForPriceStandard(PricingCategoryPriceStandard $price, \Datetime $dateFrom, \Datetime $dateTo){
        $dateFromDay = $dateFrom->format('N');
        $dateToDay = $dateTo->format('N');
        $datesDiff = $dateTo->diff($dateFrom);

        $priceValue = 0;
        if($datesDiff->days > 7){
            $priceValue = $price->getPrice7Nights();
        }

        if($dateFromDay == 1 && $dateToDay == 5){
            return (float)$priceValue + (float)$price->getPriceMondayFriday();
        }elseif($dateFromDay == 5 && $dateToDay == 1){
            return (float)$priceValue + (float)$price->getPriceFridayMonday();
        }elseif(($dateFromDay == 1 && $dateToDay == 1)||($dateFromDay == 5 && $dateToDay == 5)){
            return (float)$priceValue + (float)$price->getPrice7Nights();
        }

        return 0;
    }
}