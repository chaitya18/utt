<?php

namespace UTT\ReservationBundle\Factory;

use Doctrine\ORM\EntityManager;
use UTT\ReservationBundle\Entity\Reservation;
use UTT\IndexBundle\Entity\Configuration;
use UTT\IndexBundle\Service\ConfigurationService;
use Symfony\Component\HttpFoundation\Session\Session;

use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\Encoder\XmlEncoder;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\GetSetMethodNormalizer;

use UTT\UserBundle\Entity\User;
use UTT\EstateBundle\Entity\Estate;
use UTT\ReservationBundle\Model\ReservationDataPrice;
use UTT\ReservationBundle\Model\ReservationDataUserData;
use UTT\ReservationBundle\Model\ReservationDataDiscountPrice;
use UTT\ReservationBundle\Model\ReservationDataEstateData;

class ReservationFactory {
    protected $_em;
    protected $configurationService;

    public function __construct(EntityManager $em, ConfigurationService $configurationService){
        $this->_em = $em;
        $this->configurationService = $configurationService;
    }

    public function assignDeposit(Reservation $reservation){
        if(!$this->isFullPaymentRequired($reservation)){
            $deposit = $this->calculateDeposit($reservation);
            if($deposit) $reservation->setDeposit($deposit);

            $balanceDueDate = $this->generateBalanceDueDate($reservation);
            if($balanceDueDate) $reservation->setBalanceDueDate($balanceDueDate);
        }

        return $reservation;
    }

    public function updateReservationStatusAfterChanges(Reservation $reservation){
        if($reservation->getStatus() == Reservation::STATUS_FULLY_PAID){
            if($reservation->getPaid() > 0 && $reservation->getPaid() < $reservation->getTotalPrice()){
                $reservation->setStatus(Reservation::STATUS_PART_PAID);
            }
        }

        return $reservation;
    }

    public function create(Estate $estate, User $user, \Datetime $fromDate, \Datetime $toDate, $sleeps, $sleepsChildren, $pets, $infants, ReservationDataPrice $price, ReservationDataUserData $userData, ReservationDataDiscountPrice $discountPrice){
        $encoders = array(new XmlEncoder(), new JsonEncoder());
        $normalizers = array(new GetSetMethodNormalizer());
        $serializer = new Serializer($normalizers, $encoders);

        $reservation = new Reservation();
        $reservation->setEstate($estate);
        $reservation->setUser($user);
        $reservation->setFromDate($fromDate);
        $reservation->setToDate($toDate);
        $reservation->setSleeps($sleeps);
        $reservation->setSleepsChildren($sleepsChildren);
        $reservation->setPets($pets);
        $reservation->setInfants($infants);

        $reservation->setTotalPrice((float)$price->getTotal() - (float)$discountPrice->getTotal());

        $reservation->setPrice($serializer->serialize($price, 'json'));
        $reservation->setUserData($serializer->serialize($userData, 'json'));
        $reservation->setDiscountPrice($serializer->serialize($discountPrice, 'json'));

        $estateData = new \StdClass();
        $estateData->name = $estate->getName();
        $reservation->setEstateData($serializer->serialize(new ReservationDataEstateData($estateData), 'json'));

        $reservation->setCommission($estate->getUttCommissionRate());
        $reservation->setCommissionValue($reservation->calculateCommissionValue());

        return $reservation;
    }

    public function isRefundable(Reservation $reservation){
        /** @var Configuration $configuration */
        $configuration = $this->configurationService->get();
        if($configuration instanceof Configuration){
            $nowDate = new \DateTime('now');
            $interval = $nowDate->diff($reservation->getFromDate());

            if($interval->days > (int)$configuration->getNoRefundLimit()){
                return true;
            }
        }

        return false;
    }

    public function isCancelAndNoRefund(Reservation $reservation){
        /** @var Configuration $configuration */
        if($reservation->isCancelAndNoRefund()) {
            return true;
        }
        $configuration = $this->configurationService->get();
        if($configuration instanceof Configuration && $reservation->isCancelled()){
            $updateDate = $reservation->getUpdatedAt();
            $interval = $updateDate->diff($reservation->getFromDate());

            if($interval->days < (int)$configuration->getNoRefundLimit()){
                return true;
            }
        }

        return false;
    }

    public function isPaymentDeadlineExceeded(Reservation $reservation){
        /** @var Configuration $configuration */
        $configuration = $this->configurationService->get();
        if($configuration instanceof Configuration){
            if($configuration->getPaymentDeadline()){
                $paymentDeadline = clone $reservation->getCreatedAt();
                $paymentDeadline->modify('+'.(int)$configuration->getPaymentDeadline().' hours');

                $nowDate = new \DateTime('now');
                if($nowDate > $paymentDeadline){
                    return true;
                }
            }
        }

        return false;
    }

    public function isBalancePaymentOverdue(Reservation $reservation){
        $nowDate = new \DateTime('now');
        $nowDate->modify('-1 day');

        if($nowDate > $reservation->getBalanceDueDate()){
            return true;
        }

        return false;
    }

    public function isBalancePaymentOverdueForReminder(Reservation $reservation){
        $nowDate = new \DateTime('now');
        $nowDate->modify('+3 days');

        if($nowDate > $reservation->getBalanceDueDate()){
            return true;
        }

        $fromDate = new \DateTime('now');$fromDate->modify('+8 days');
        $toDate = new \DateTime('now');$toDate->modify('+7 days');
        if($fromDate > $reservation->getBalanceDueDate() && $toDate < $reservation->getBalanceDueDate()){
            return true;
        }

        return false;
    }

    public function isFullPaymentRequired(Reservation $reservation){
        /** @var Configuration $configuration */
        $configuration = $this->configurationService->get();
        if($configuration instanceof Configuration){
            if($configuration->getFullPaymentLimit()){
                $fullPaymentLimitDate = clone $reservation->getCreatedAt();
                $fullPaymentLimitDate->modify('+'.(int)$configuration->getFullPaymentLimit().' days');
                if($reservation->getFromDate() <= $fullPaymentLimitDate){
                    return true;
                }

            }
        }

        return false;
    }

    public function generateBalanceDueDate(Reservation $reservation){
        /** @var Configuration $configuration */
        $configuration = $this->configurationService->get();
        if($configuration instanceof Configuration){
            if($configuration->getBalanceDueLimit()){
                $balanceDueDate = clone $reservation->getFromDate();
                return $balanceDueDate->modify('-'.(int)$configuration->getBalanceDueLimit().' days');
            }
        }

        return false;
    }

    public function calculateDeposit(Reservation $reservation){
        /** @var Configuration $configuration */
        $configuration = $this->configurationService->get();
        if($configuration instanceof Configuration){
            if($configuration->getDepositPercentage()){
                $priceDecoded = $reservation->getPriceDecoded();
                $price = ((float) $priceDecoded->getBasePrice() + (float) $priceDecoded->getAdditionalSleepsPrice() + (float) $priceDecoded->getPetsPrice());

                return (float) ($price * $configuration->getDepositPercentage() / 100) + (float) $priceDecoded->getAdminCharge() + (float) $priceDecoded->getBookingProtectCharge();
                //return (float) (((float)$reservation->getTotalPrice()) * $configuration->getDepositPercentage() / 100) + (float) $priceDecoded->getAdminCharge() + (float) $priceDecoded->getBookingProtectCharge();
            }
        }

        return false;
    }

    public function getBalanceToPay(Reservation $reservation){
        return (float)($reservation->getTotalPrice() - $reservation->getPaid());
    }

    public function reservationStatusUpdate(Reservation $reservation){

        //if reservation is canceled dont change status based on manual refund (paid column)
        if($reservation->isCancelled()) {
            return $this->checkCanceledRefundStatus($reservation);
        }

        if($reservation->getPaid() > 0 && $reservation->getPaid() < $reservation->getTotalPrice()){
            $reservation->setStatus(Reservation::STATUS_PART_PAID);
        }elseif($reservation->getPaid() == $reservation->getTotalPrice()){
            $reservation->setStatus(Reservation::STATUS_FULLY_PAID);
        }elseif($reservation->getPaid() == 0){
            $reservation->setStatus(Reservation::STATUS_RESERVED);
        }

        return $reservation;
    }

    private function checkCanceledRefundStatus(Reservation $reservation)
    {
        //if status is STATUS_CANCELLED_REFUND_DUE and paid equals 0 - set status to canceled
        if(round($reservation->getPaid(), 2) == 0.00) {
            $reservation->setStatus(Reservation::STATUS_CANCELLED);
        }

        return $reservation;
    }

    public function addPaymentToReservation(Reservation $reservation, $amount, $verifyWithTotal = false){
        if($verifyWithTotal){
            if((float)$reservation->getPaid() + (float)$amount > (float)$reservation->getTotalPrice()){
                return false;
            }
        }

        $reservation->setPaid((float)$reservation->getPaid() + (float)$amount);
        $reservation = $this->reservationStatusUpdate($reservation);
        $reservation->setCommissionValue($reservation->calculateCommissionValue());

        return $reservation;
    }

    public function refundPaymentToReservation(Reservation $reservation, $amount){
        if($reservation->getPaid() - $amount >= 0){
            $reservation->setPaid($reservation->getPaid() - $amount);
            if($reservation->getStatus() == Reservation::STATUS_FULLY_PAID){
                $reservation->setTotalPrice($reservation->getPaid());
            }

            $reservation = $this->reservationStatusUpdate($reservation);

            return $reservation;
        }
        return false;
    }

    public function addBookingProtectCharge(Reservation $reservation, $bookingProtectCharge){
        // serializer init
        $encoders = array(new XmlEncoder(), new JsonEncoder());
        $normalizers = array(new GetSetMethodNormalizer());
        $serializer = new Serializer($normalizers, $encoders);

        $price = $reservation->getPriceDecoded();
        $price->setBookingProtectCharge($bookingProtectCharge);
        $price->setTotal((float)$price->getTotal() + (float)$bookingProtectCharge);
        $reservation->setPrice($serializer->serialize($price, 'json'));

        $reservation->setTotalPrice((float)$reservation->getTotalPrice() + (float)$bookingProtectCharge);

        return $reservation;
    }

    public function addCreditCardCharge(Reservation $reservation, $amountCreditCardCharge){
        // serializer init
        $encoders = array(new XmlEncoder(), new JsonEncoder());
        $normalizers = array(new GetSetMethodNormalizer());
        $serializer = new Serializer($normalizers, $encoders);

        $price = $reservation->getPriceDecoded();
        $price->setCreditCardCharge($amountCreditCardCharge);
        $price->setTotal((float)$price->getTotal() + (float)$amountCreditCardCharge);
        $reservation->setPrice($serializer->serialize($price, 'json'));

        $reservation->setTotalPrice((float)$reservation->getTotalPrice() + (float)$amountCreditCardCharge);

        return $reservation;
    }

    public function addCharityCharge(Reservation $reservation, $amountCharityCharge){
        // serializer init
        $encoders = array(new XmlEncoder(), new JsonEncoder());
        $normalizers = array(new GetSetMethodNormalizer());
        $serializer = new Serializer($normalizers, $encoders);

        $price = $reservation->getPriceDecoded();
        $price->setCharityCharge($price->getCharityCharge() + $amountCharityCharge);
        $price->setTotal((float)$price->getTotal() + (float)$amountCharityCharge);
        $reservation->setPrice($serializer->serialize($price, 'json'));

        $reservation->setTotalPrice((float)$reservation->getTotalPrice() + (float)$amountCharityCharge);

        return $reservation;
    }
}