<?php

namespace UTT\ReservationBundle\Factory;

use Doctrine\ORM\EntityManager;
use UTT\ReservationBundle\Entity\DiscountCode;
use UTT\ReservationBundle\Entity\Reservation;
use UTT\IndexBundle\Entity\Configuration;
use UTT\IndexBundle\Service\ConfigurationService;
use Symfony\Component\HttpFoundation\Session\Session;

use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\Encoder\XmlEncoder;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\GetSetMethodNormalizer;

use UTT\UserBundle\Entity\User;
use UTT\ReservationBundle\Entity\Voucher;
use UTT\EstateBundle\Entity\Estate;
use UTT\ReservationBundle\Model\VoucherData;
use UTT\ReservationBundle\Model\VoucherDataPrice;
use UTT\ReservationBundle\Model\VoucherDataUserData;

class VoucherFactory {
    protected $_em;
    protected $configurationService;

    public function __construct(EntityManager $em, ConfigurationService $configurationService){
        $this->_em = $em;
        $this->configurationService = $configurationService;
    }

    /**
     * @return Serializer
     */
    protected function getSerializer()
    {
        $encoders = array(new XmlEncoder(), new JsonEncoder());
        $normalizers = array(new GetSetMethodNormalizer());
        return new Serializer($normalizers, $encoders);
    }

    public function create($voucherValue, $voucherName, User $user, VoucherDataPrice $price, VoucherDataUserData $userData){

        $serializer = $this->getSerializer();

        $voucher = new Voucher();
        $voucher->setValue($voucherValue);
        $voucher->setName($voucherName);
        $voucher->setUser($user);

        $voucher->setTotalPrice((float)$price->getTotal());

        $voucher->setPrice($serializer->serialize($price, 'json'));
        $voucher->setUserData($serializer->serialize($userData, 'json'));

        $expiresAtDate = new \DateTime('now');
        $expiresAtDate->modify('+2 years');
        $voucher->setExpiresAt($expiresAtDate);

        return $voucher;
    }

    /**
     * @param User $user
     * @param DiscountCode $discountCode
     * @param null $voucher
     * @return Voucher|null
     */
    public function createFromDiscountCodeAndUser(User $user, DiscountCode $discountCode, $voucher = null)
    {
        if(!$voucher) {
            $voucher = new Voucher();
        }
        $price = $this->buildVoucherDataPriceFromDiscountCode($discountCode);
        $userData = $this->buildVoucherUserDataFromUser($user);
        $serializer = $this->getSerializer();


        $voucher->setValue($discountCode->getValue());
        $voucher->setName($discountCode->getCode());
        $voucher->setUser($user);

        $voucher->setTotalPrice((float)$price->getTotal());
        $voucher->setPaid($price->getTotal());

        $voucher->setPrice($serializer->serialize($price, 'json'));
        $voucher->setUserData($serializer->serialize($userData, 'json'));
        $voucher->setExpiresAt($discountCode->getExpiresAt());
        $voucher->setStatus(Voucher::STATUS_FULLY_PAID);
        $voucher->setDiscountCode($discountCode);

        return $voucher;
    }

    public function voucherStatusUpdate(Voucher $voucher){
        if($voucher->getPaid() > 0 && $voucher->getPaid() < $voucher->getTotalPrice()){
            $voucher->setStatus(Voucher::STATUS_PART_PAID);
        }elseif($voucher->getPaid() == $voucher->getTotalPrice()){
            $voucher->setStatus(Voucher::STATUS_FULLY_PAID);
        }elseif($voucher->getPaid() == 0){
            $voucher->setStatus(Voucher::STATUS_RESERVED);
        }

        return $voucher;
    }

    public function addPaymentToVoucher(Voucher $voucher, $amount, $verifyWithTotal = false){
        if($verifyWithTotal){
            if((float)$voucher->getPaid() + (float)$amount > (float)$voucher->getTotalPrice()){
                return false;
            }
        }

        $voucher->setPaid((float)$voucher->getPaid() + (float)$amount);
        $voucher = $this->voucherStatusUpdate($voucher);

        return $voucher;
    }


    public function addCreditCardCharge(Voucher $voucher, $amountCreditCardCharge){
        // serializer init
        $encoders = array(new XmlEncoder(), new JsonEncoder());
        $normalizers = array(new GetSetMethodNormalizer());
        $serializer = new Serializer($normalizers, $encoders);

        $price = $voucher->getPriceDecoded();
        $price->setCreditCardCharge($amountCreditCardCharge);
        $price->setTotal((float)$price->getTotal() + (float)$amountCreditCardCharge);
        $voucher->setPrice($serializer->serialize($price, 'json'));

        $voucher->setTotalPrice((float)$voucher->getTotalPrice() + (float)$amountCreditCardCharge);

        return $voucher;
    }

    public function addCharityCharge(Voucher $voucher, $amountCharityCharge){
        // serializer init
        $encoders = array(new XmlEncoder(), new JsonEncoder());
        $normalizers = array(new GetSetMethodNormalizer());
        $serializer = new Serializer($normalizers, $encoders);

        $price = $voucher->getPriceDecoded();
        $price->setCharityCharge($price->getCharityCharge() + $amountCharityCharge);
        $price->setTotal((float)$price->getTotal() + (float)$amountCharityCharge);
        $voucher->setPrice($serializer->serialize($price, 'json'));

        $voucher->setTotalPrice((float)$voucher->getTotalPrice() + (float)$amountCharityCharge);

        return $voucher;
    }

    /**
     * @param User $user
     * @return VoucherDataUserData
     */
    protected function buildVoucherUserDataFromUser(User $user) {
        $voucherDataUserData = new VoucherDataUserData();
        $voucherDataUserData->setFirstName($user->getFirstName());
        $voucherDataUserData->setLastName($user->getLastName());
        $voucherDataUserData->setEmail($user->getEmailCanonical());
        $voucherDataUserData->setPhone($user->getPhone());
        $voucherDataUserData->setAddress($user->getAddress());
        $voucherDataUserData->setCity($user->getCity());
        $voucherDataUserData->setPostcode($user->getPostcode());
        //$voucherDataUserData->setCountry($user->getCountry());
        return $voucherDataUserData;
    }

    /**
     * @param $value
     * @param $name
     * @param VoucherDataUserData $userData
     * @param VoucherDataPrice $price
     * @return VoucherData
     */
    protected function buildVoucherData($value, $name, VoucherDataUserData $userData, VoucherDataPrice $price) {
        $voucherData = new VoucherData();
        $voucherData->setValue($value);
        $voucherData->setName($name);
        $voucherData->setUserData($userData);
        $voucherData->setPrice($price);
        return $voucherData;
    }

    /**
     * @param DiscountCode $discountCode
     * @return VoucherDataPrice
     */
    protected function buildVoucherDataPriceFromDiscountCode(DiscountCode $discountCode)
    {
        $voucherDataPrice = new VoucherDataPrice();
        $voucherDataPrice->setBasePrice($discountCode->getValue());
        $voucherDataPrice->setTotal($discountCode->getValue());
        return $voucherDataPrice;
    }
}