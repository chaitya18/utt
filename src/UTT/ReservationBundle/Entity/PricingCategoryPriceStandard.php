<?php

namespace UTT\ReservationBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * PricingCategoryPriceStandard
 *
 * @ORM\Table(name="pricing_category_price_standard")
 * @ORM\Entity(repositoryClass="UTT\ReservationBundle\Entity\PricingCategoryPriceStandardRepository")
 */
class PricingCategoryPriceStandard
{
    const TYPE_MAXIMUM = 1;
    const TYPE_PEAK = 2;
    const TYPE_HIGH = 3;
    const TYPE_MEDIUM = 4;
    const TYPE_LOW = 5;
    const TYPE_MINIMUM = 6;

    public function getAllowedTypes(){
        $array = array(
            self::TYPE_MAXIMUM => 'Maximum',
            self::TYPE_PEAK => 'Peak',
            self::TYPE_HIGH => 'High',
            self::TYPE_MEDIUM => 'Medium',
            self::TYPE_LOW => 'Low',
            self::TYPE_MINIMUM => 'Minimum',
        );
        return $array;
    }

    public static function getAllowedTypesStatic(){
        $array = array(
            self::TYPE_MAXIMUM => 'Maximum',
            self::TYPE_PEAK => 'Peak',
            self::TYPE_HIGH => 'High',
            self::TYPE_MEDIUM => 'Medium',
            self::TYPE_LOW => 'Low',
            self::TYPE_MINIMUM => 'Minimum',
        );
        return $array;
    }

    public function getTypeName($typeId = null){
        $array = $this->getAllowedTypes();
        if(is_null($typeId)){
            if($this->getType()) {
                return array_key_exists($this->getType(), $array) ? $array[$this->getType()] : false;
            }else{
                return false;
            }
        }
        return array_key_exists($typeId, $array) ? $array[$typeId] : false;
    }

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(name="type", type="integer")
     */
    protected $type;

    /**
     * @ORM\Column(name="year", type="integer")
     */
    protected $year;

    /**
     * @ORM\Column(name="price_7_nights", type="decimal", scale=2, nullable=true)
     */
    protected $price7Nights;

    /**
     * @ORM\Column(name="price_monday_friday", type="decimal", scale=2, nullable=true)
     */
    protected $priceMondayFriday;

    /**
     * @ORM\Column(name="price_friday_monday", type="decimal", scale=2, nullable=true)
     */
    protected $priceFridayMonday;

    /**
     * @ORM\ManyToOne(targetEntity="PricingCategory", inversedBy="pricesStandard")
     * @ORM\JoinColumn(name="pricing_category_id", referencedColumnName="id", nullable=false, onDelete="CASCADE")
     */
    protected $pricingCategory;

    public function __toString(){
        if($this->getId())
            return (string) $this->getId();
        return '';
    }


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set type
     *
     * @param integer $type
     * @return PricingCategoryPriceStandard
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return integer 
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set year
     *
     * @param integer $year
     * @return PricingCategoryPriceStandard
     */
    public function setYear($year)
    {
        $this->year = $year;

        return $this;
    }

    /**
     * Get year
     *
     * @return integer 
     */
    public function getYear()
    {
        return $this->year;
    }

    /**
     * Set price7Nights
     *
     * @param string $price7Nights
     * @return PricingCategoryPriceStandard
     */
    public function setPrice7Nights($price7Nights)
    {
        $this->price7Nights = $price7Nights;

        return $this;
    }

    /**
     * Get price7Nights
     *
     * @return string 
     */
    public function getPrice7Nights()
    {
        return $this->price7Nights;
    }

    /**
     * Set priceMondayFriday
     *
     * @param string $priceMondayFriday
     * @return PricingCategoryPriceStandard
     */
    public function setPriceMondayFriday($priceMondayFriday)
    {
        $this->priceMondayFriday = $priceMondayFriday;

        return $this;
    }

    /**
     * Get priceMondayFriday
     *
     * @return string 
     */
    public function getPriceMondayFriday()
    {
        return $this->priceMondayFriday;
    }

    /**
     * Set priceFridayMonday
     *
     * @param string $priceFridayMonday
     * @return PricingCategoryPriceStandard
     */
    public function setPriceFridayMonday($priceFridayMonday)
    {
        $this->priceFridayMonday = $priceFridayMonday;

        return $this;
    }

    /**
     * Get priceFridayMonday
     *
     * @return string 
     */
    public function getPriceFridayMonday()
    {
        return $this->priceFridayMonday;
    }

    /**
     * Set pricingCategory
     *
     * @param \UTT\ReservationBundle\Entity\PricingCategory $pricingCategory
     * @return PricingCategoryPriceStandard
     */
    public function setPricingCategory(\UTT\ReservationBundle\Entity\PricingCategory $pricingCategory)
    {
        $this->pricingCategory = $pricingCategory;

        return $this;
    }

    /**
     * Get pricingCategory
     *
     * @return \UTT\ReservationBundle\Entity\PricingCategory 
     */
    public function getPricingCategory()
    {
        return $this->pricingCategory;
    }
}
