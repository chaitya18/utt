<?php

namespace UTT\ReservationBundle\Entity;

use Doctrine\ORM\EntityRepository;
use UTT\EstateBundle\Entity\Estate;

class DiscountCodeRepository extends EntityRepository
{
    public function findExpired(){
        $query = $this->_em->createQuery("
            SELECT dc
            FROM UTTReservationBundle:DiscountCode dc
            WHERE dc.expiresAt < :nowDate
        ");
        $query->setParameter('nowDate', new \Datetime('now'));

        $result = $query->getResult();
        if(count($result) > 0) return $result;

        return false;
    }

    public function findOneActiveByEstateAndCode(Estate $estate, \Datetime $fromDate, \Datetime $toDate, $code){
        $query = $this->_em->createQuery("
            SELECT dc
            FROM UTTReservationBundle:DiscountCode dc
            LEFT JOIN dc.estates e
            WHERE
                dc.expiresAt > :nowDate AND
                (dc.holidayMustStartAt <= :fromDate OR dc.holidayMustStartAt IS NULL) AND
                dc.holidayMustStartBy >= :fromDate AND
                dc.isActive = 1 AND dc.code = :code AND (e.id IN (:estates) OR dc.isAllEstates = 1)
        ");
        $query->setParameters(array(
            'estates' => array($estate->getId()),
            'code' => $code,
            'nowDate' => new \Datetime('now'),
            'fromDate' => $fromDate
        ));
        $query->setMaxResults(1);

        $result = $query->getResult();
        if(count($result) > 0) return $result[0];

        return false;
    }

}
