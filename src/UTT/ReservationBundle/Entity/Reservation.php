<?php

namespace UTT\ReservationBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use UTT\ReservationBundle\Model\ReservationDataUserData;
use UTT\ReservationBundle\Model\ReservationDataPrice;
use UTT\ReservationBundle\Model\ReservationDataDiscountPrice;
use UTT\ReservationBundle\Model\ReservationDataEstateData;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Reservation
 *
 * @ORM\Table(name="reservation")
 * @ORM\Entity(repositoryClass="UTT\ReservationBundle\Entity\ReservationRepository")
 * @ORM\HasLifecycleCallbacks
 */
class Reservation
{
    const STATUS_RESERVED = 1;
    //const STATUS_BLOCK_BOOKING = 2;
    const STATUS_FULLY_PAID = 3;
    const STATUS_PART_PAID = 4;
    const STATUS_CANCELLED = 5;

    const STATUS_RESERVED_TELEPHONE = 6;

    const STATUS_BLOCKED_OWNER = 7;
    const STATUS_BLOCKED_NOT_FOR_SALE = 8;
    const STATUS_BLOCKED_OTHER_AGENCY = 9;

    const STATUS_TEMPORARY_BOOKING = 10;
    const STATUS_CANCELLED_REFUND_DUE = 11;
    const STATUS_GROUPON_BOOKING = 12;

    const STATUS_BLOCKED_AIRBNB = 13;
    const STATUS_BLOCKED_MULTUPROPERTY = 14;
    const STATUS_CANCELLED_NO_REFUND = 15;

    public function getAllowedStatuses(){
        $array = array(
            self::STATUS_RESERVED => 'Reserved',
            //self::STATUS_BLOCK_BOOKING => 'Block booking'
            self::STATUS_FULLY_PAID => 'Fully paid',
            self::STATUS_PART_PAID => 'Part paid',
            self::STATUS_CANCELLED => 'Cancelled',
            self::STATUS_RESERVED_TELEPHONE => 'Telephone booking',
            self::STATUS_BLOCKED_OWNER => 'Blocked out - Owner booking',
            self::STATUS_BLOCKED_NOT_FOR_SALE => 'Blocked out - Dates not for sale',
            self::STATUS_BLOCKED_OTHER_AGENCY => 'Blocked out - Other agency',
            self::STATUS_TEMPORARY_BOOKING => 'Temporary booking',
            self::STATUS_CANCELLED_REFUND_DUE => 'Cancelled - Refund Due',
            self::STATUS_GROUPON_BOOKING => 'Groupon booking',
            self::STATUS_BLOCKED_AIRBNB => 'Airbnb booking',
            self::STATUS_BLOCKED_MULTUPROPERTY => 'Multiproperty booking',
            self::STATUS_CANCELLED_NO_REFUND  => 'Cancelled - no Refund',
        );
        return $array;
    }

    public function getAllowedStatusesForUser(){
        $array = array(
            self::STATUS_RESERVED => 'Reserved',
            //self::STATUS_BLOCK_BOOKING => 'Block booking'
            self::STATUS_FULLY_PAID => 'Fully paid',
            self::STATUS_PART_PAID => 'Part paid',
            self::STATUS_CANCELLED => 'Cancelled',
            self::STATUS_RESERVED_TELEPHONE => 'Telephone booking',
            self::STATUS_BLOCKED_OWNER => 'Blocked out - Owner booking',
            self::STATUS_BLOCKED_NOT_FOR_SALE => 'Blocked out - Dates not for sale',
            self::STATUS_BLOCKED_OTHER_AGENCY => 'Blocked out - Other agency',
            self::STATUS_TEMPORARY_BOOKING => 'Temporary booking',
            self::STATUS_CANCELLED_REFUND_DUE => 'Cancelled - credit note',
            self::STATUS_GROUPON_BOOKING => 'Groupon booking',
            self::STATUS_BLOCKED_AIRBNB => 'Airbnb booking',
            self::STATUS_BLOCKED_MULTUPROPERTY => 'Multiproperty booking',
            self::STATUS_CANCELLED_NO_REFUND  => 'Cancelled - no Refund',
        );
        return $array;
    }

    public function isAllowedCancel(){
        $nowDate = new \DateTime('now');
        $nowDate->setTime(0,0,0);
        if($this->getFromDate() < $nowDate){ return false; }

        return $this->isCancelled();
    }

    public function isAllowedResendConfirmationMail(){
        $nowDate = new \DateTime('now');
        $nowDate->setTime(0,0,0);
        if($this->getFromDate() < $nowDate){ return false; }

        if($this->getStatus() == self::STATUS_RESERVED){
            return true;
        }

        return false;
    }

    public function isAllowedResendArrivalInstructionMail(){
        $nowDate = new \DateTime('now');
        $nowDate->setTime(0,0,0);
        if($this->getFromDate() < $nowDate){ return false; }

        if($this->getStatus() == self::STATUS_FULLY_PAID){
            return true;
        }

        return false;
    }

    public function isAllowedToPay(){
        //$nowDate = new \DateTime('now');
        //$nowDate->setTime(0,0,0);
        //if($this->getFromDate() < $nowDate){ return false; }

        if($this->getStatus() == self::STATUS_RESERVED || $this->getStatus() == self::STATUS_PART_PAID){
            return true;
        }

        return false;
    }

    public function isAllowedForBookingProtection(){
        if(!$this->getIsBookingProtect() && $this->getStatus() == Reservation::STATUS_PART_PAID){
            return true;
        }

        return false;
    }

    public function isAllowedToEdit(){
        $nowDate = new \DateTime('now');
        $nowDate->setTime(0,0,0);
        if($this->getFromDate() < $nowDate){ return false; }

        if($this->getStatus() == self::STATUS_RESERVED || $this->getStatus() == self::STATUS_PART_PAID || $this->getStatus() == self::STATUS_FULLY_PAID){
            return true;
        }

        return false;
    }

    public function isCancelled(){
        return $this->getStatus() === self::STATUS_CANCELLED_NO_REFUND
            || $this->getStatus() === self::STATUS_CANCELLED || $this->getStatus() === self::STATUS_CANCELLED_REFUND_DUE;
    }

    public function isBlocked(){
        $status = $this->getStatus();
        if($status){
            if($status == self::STATUS_BLOCKED_OWNER || $status == self::STATUS_BLOCKED_NOT_FOR_SALE || $status == self::STATUS_BLOCKED_OTHER_AGENCY || $status == self::STATUS_BLOCKED_AIRBNB || $status == self::STATUS_BLOCKED_MULTUPROPERTY){
                return true;
            }
        }

        return false;
    }

    public function isCompletedBooking(){
        $nowDate = new \DateTime('now');
        if($this->getToDate() < $nowDate){
            return true;
        }

        return false;
    }

    public function isNormalBooking(){
        if($this->getStatus() == self::STATUS_FULLY_PAID || $this->getStatus() == self::STATUS_PART_PAID || $this->getStatus() == self::STATUS_RESERVED){
            return true;
        }

        return false;
    }

    public function isCancelAndNoRefund(){
        return $this->getStatus() === self::STATUS_CANCELLED_NO_REFUND;
    }

    public function getNumberOfNights(){
        return $this->getFromDate()->diff($this->getToDate())->days;
    }

    public function getStatusName($statusId = null){
        $array = $this->getAllowedStatuses();
        if(is_null($statusId)){
            if($this->getStatus()) {
                return array_key_exists($this->getStatus(), $array) ? $array[$this->getStatus()] : false;
            }else{
                return false;
            }
        }
        return array_key_exists($statusId, $array) ? $array[$statusId] : false;
    }

    public function getStatusNameForUser($statusId = null){
        $array = $this->getAllowedStatusesForUser();
        if(is_null($statusId)){
            if($this->getStatus()) {
                return array_key_exists($this->getStatus(), $array) ? $array[$this->getStatus()] : false;
            }else{
                return false;
            }
        }
        return array_key_exists($statusId, $array) ? $array[$statusId] : false;
    }

    const REFERRED_FLAG_ADMIN = 1;
    const REFERRED_FLAG_LANDLORD = 2;
    const REFERRED_FLAG_LUKASZ_RAK = 3;
    const REFERRED_FLAG_SPOT = 4;
    const REFERRED_FLAG_GREG = 5;
    const REFERRED_FLAG_ELERI = 6;
    const REFERRED_FLAG_YVONNE = 7;
    const REFERRED_FLAG_BETHAN = 8;
    const REFERRED_FLAG_KATIE = 9;

    public function getAllowedReferredFlags(){
        $array = array(
            self::REFERRED_FLAG_ADMIN => 'Admin team',
            self::REFERRED_FLAG_LANDLORD => 'Landlord',
            self::REFERRED_FLAG_LUKASZ_RAK => 'Łukasz Rak',
            self::REFERRED_FLAG_SPOT => 'Spot',
            self::REFERRED_FLAG_GREG => 'Greg',
            self::REFERRED_FLAG_ELERI => 'Eleri',
            self::REFERRED_FLAG_YVONNE => 'Yvonne',
            self::REFERRED_FLAG_BETHAN => 'Bethan',
            self::REFERRED_FLAG_KATIE => 'Katie'
        );
        return $array;
    }

    public static function getAllowedReferredFlagsStatic(){
        $array = array(
            self::REFERRED_FLAG_ADMIN => 'Admin team',
            self::REFERRED_FLAG_LANDLORD => 'Landlord',
            self::REFERRED_FLAG_LUKASZ_RAK => 'Łukasz Rak',
            //self::REFERRED_FLAG_SPOT => 'Spot',
            self::REFERRED_FLAG_GREG => 'Greg',
            //self::REFERRED_FLAG_ELERI => 'Eleri',
            self::REFERRED_FLAG_YVONNE => 'Yvonne',
            self::REFERRED_FLAG_BETHAN => 'Bethan',
            self::REFERRED_FLAG_KATIE => 'Katie'
        );
        return $array;
    }

    public function getReferredFlagName($referredFlagId = null){
        $array = $this->getAllowedReferredFlags();
        if(is_null($referredFlagId)){
            if($this->getReferredFlag()) {
                return array_key_exists($this->getReferredFlag(), $array) ? $array[$this->getReferredFlag()] : false;
            }else{
                return false;
            }
        }
        return array_key_exists($referredFlagId, $array) ? $array[$referredFlagId] : false;
    }

    public function __construct(){
        $this->setDeposit(0);
        $this->setCommission(0);
        $this->setCreatedAt(new \DateTime('now'));
        $this->setUpdatedAt(new \DateTime('now'));
        $this->setBalanceDueDate(new \DateTime('now'));
        $this->historyEntries = new ArrayCollection();
        $this->paymentTransactions = new ArrayCollection();

        $random = rand(100000000, 999999999);
        $timestamp = $this->getCreatedAt()->getTimestamp();

        $this->setPin((int)$timestamp + (int)$random);
        $this->setIsBookingProtect(false);
        $this->setIsHoldingForPayment(false);
    }

    /**
     * @ORM\PreUpdate()
     */
    public function preUpdate(){
        $this->setUpdatedAt(new \DateTime('now'));
    }

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(name="external_id", type="integer", nullable=true)
     */
    protected $externalId;

    /**
     * @ORM\Column(name="status", type="integer")
     */
    protected $status;

    /**
     * @ORM\ManyToOne(targetEntity="UTT\EstateBundle\Entity\Estate")
     * @ORM\JoinColumn(name="estate_id", referencedColumnName="id", nullable=true, onDelete="SET NULL")
     */
    protected $estate;

    /**
     * @ORM\ManyToOne(targetEntity="UTT\UserBundle\Entity\User")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id", nullable=true, onDelete="SET NULL")
     */
    protected $user;

    /**
     * @ORM\Column(name="from_date", type="date")
     */
    protected $fromDate;

    /**
     * @ORM\Column(name="to_date", type="date")
     */
    protected $toDate;

    /**
     * @ORM\Column(name="sleeps", type="integer")
     */
    protected $sleeps;

    /**
     * @ORM\Column(name="sleeps_children", type="integer")
     */
    protected $sleepsChildren;

    /**
     * @ORM\Column(name="pets", type="integer", nullable=true)
     */
    protected $pets;

    /**
     * @ORM\Column(name="infants", type="integer", nullable=true)
     */
    protected $infants;

    /**
     * @ORM\Column(name="created_at", type="datetime")
     */
    protected $createdAt;

    /**
     * @ORM\Column(name="updated_at", type="datetime")
     */
    protected $updatedAt;

    /**
     * @ORM\Column(name="paid", type="decimal", scale=2, nullable=true)
     */
    protected $paid;

    /**
     * @ORM\Column(name="total_price", type="decimal", scale=2, nullable=true)
     */
    protected $totalPrice;

    /**
     * @ORM\Column(name="price", type="text")
     */
    protected $price;

    /**
     * @ORM\Column(name="discount_price", type="text")
     */
    protected $discountPrice;

    /**
     * @ORM\Column(name="user_data", type="text")
     */
    protected $userData;

    /**
     * @ORM\Column(name="estate_data", type="text")
     */
    protected $estateData;

    /**
     * @ORM\Column(name="deposit", type="decimal", scale=2)
     */
    protected $deposit;

    /**
     * @ORM\Column(name="balance_due_date", type="date")
     */
    protected $balanceDueDate;

    /**
     * @ORM\OneToMany(targetEntity="HistoryEntry", mappedBy="reservation", cascade={"persist"})
     */
    protected $historyEntries;

    /**
     * @ORM\OneToMany(targetEntity="PaymentTransaction", mappedBy="reservation", cascade={"persist"})
     */
    protected $paymentTransactions;

    /**
     * @ORM\Column(name="note", type="text", nullable=true)
     */
    protected $note;

    /**
     * @ORM\Column(name="external_data", type="text", nullable=true)
     */
    protected $externalData;

    /**
     * @ORM\Column(name="commission", type="integer")
     */
    protected $commission;

    /**
     * @ORM\Column(name="commission_value", type="decimal", scale=2, nullable=true)
     */
    protected $commissionValue;

    /**
     * @ORM\Column(name="ip_address", type="string", length=255, nullable=true)
     */
    protected $ipAddress;

    /**
     * @ORM\Column(name="pin", type="string", length=255)
     */
    protected $pin;

    /**
     * @ORM\Column(name="external_pin", type="string", length=255, nullable=true)
     */
    protected $externalPin;

    /**
     * @ORM\Column(name="referred_flag", type="integer", nullable=true)
     */
    protected $referredFlag;

    /**
     * @ORM\Column(name="referred_flag_date", type="datetime", nullable=true)
     */
    protected $referredFlagDate;

    /**
     * @ORM\Column(name="session_id", type="string", length=255, nullable=true)
     */
    protected $sessionId;

    /**
     * @ORM\Column(name="is_thank_you_sent", type="boolean")
     */
    protected $isThankYouSent = false;

    /**
     * @ORM\Column(name="no_payment_cancellation_reinstate_date", type="datetime", nullable=true)
     */
    protected $noPaymentCancellationReinstateDate;

    /**
     * @ORM\Column(name="is_no_payment_cancellation_reinstate_sent", type="boolean")
     */
    protected $isNoPaymentCancellationReinstateSent = false;

    /**
     * @ORM\Column(name="is_1_hour_no_payment_cancellation_reinstate_sent", type="boolean")
     */
    protected $is1hourNoPaymentCancellationReinstateSent = false;

    /**
     * @ORM\ManyToOne(targetEntity="UTT\ReservationBundle\Entity\Voucher")
     * @ORM\JoinColumn(name="voucher_id", referencedColumnName="id", nullable=true, onDelete="SET NULL")
     */
    protected $paidWithVoucher;

    /**
     * @ORM\Column(name="is_booking_protect", type="boolean")
     */
    protected $isBookingProtect = false;

    /**
     * @ORM\Column(name="is_holding_for_payment", type="boolean")
     */
    protected $isHoldingForPayment = false;

    /**
     * @ORM\Column(name="holding_for_payment_until_date", type="datetime", nullable=true)
     */
    protected $holdingForPaymentUntilDate;
    /**
     * @ORM\Column(name="manager_id",  type="integer", nullable=true)
     */
    protected $managerId;

    public function __toString(){
        if($this->getId())
            return (string) $this->getId();
        return '';
    }

    public function getVatRate(){
        $startdate = $this->getFromDate()->getTimestamp();
        if ($startdate >= mktime(0,0,0,5,1,2013))
            $vatrate = 0;
        elseif ($startdate >= mktime(0,0,0,1,1,2011))
            $vatrate = 20;
        elseif ($startdate >= mktime(0,0,0,1,1,2010))
            $vatrate = 17.5;
        elseif ($startdate >= mktime(0,0,0,12,1,2008))
            $vatrate = 15;
        else
            $vatrate = 17.5;
        return $vatrate;
    }

    public function calculateCommissionValue(){
        $vatrate = $this->getVatRate();
        $holidayprice = $this->getPaid() - $this->getPriceDecoded()->getAdminCharge() - $this->getPriceDecoded()->getCreditCardCharge() - $this->getPriceDecoded()->getBookingProtectCharge() - $this->getPriceDecoded()->getCharityCharge();

        // Work out what agency should be taking
        $comission = ceil($holidayprice * $this->getCommission())/100;
        $comissionvat = floor($comission * $vatrate)/100;

        $ownervalue = $holidayprice - $comission - $comissionvat;

        // See if anything over paid which becomes charges
        // This no longer applies UTT will not be keeping this money any more!
        $charges = max(0,$this->getPaid() - $ownervalue - $comission - $comissionvat);

        // Work back what's actually left on the account to take as comission
        $prevatcomission = max(0,$this->getPaid() - $ownervalue - $charges);
        $comissionvat = floor($prevatcomission * (1-(100/(100+$vatrate)))*100)/100;

        return $prevatcomission - $comissionvat;

        //return ($this->paid - $this->getPriceDecoded()->getAdminCharge()) * $this->commission / 100;
    }

    public function getOwnersTotalPrice(){
        return $this->getTotalPrice() - $this->getPriceDecoded()->getAdminCharge() - $this->getPriceDecoded()->getCreditCardCharge() - $this->getPriceDecoded()->getBookingProtectCharge() - $this->getPriceDecoded()->getCharityCharge();
    }

    public function getCommissionVatValue(){
        //return $this->calculateCommissionValue() * $this->getVatRate() / 100;


        $vatrate = $this->getVatRate();
        $holidayprice = $this->getPaid() - $this->getPriceDecoded()->getAdminCharge() - $this->getPriceDecoded()->getCreditCardCharge() - $this->getPriceDecoded()->getBookingProtectCharge() - $this->getPriceDecoded()->getCharityCharge();

        // Work out what agency should be taking
        $comission = ceil($holidayprice * $this->getCommission())/100;
        $comissionvat = floor($comission * $vatrate)/100;

        $ownervalue = $holidayprice - $comission - $comissionvat;

        // See if anything over paid which becomes charges
        // This no longer applies UTT will not be keeping this money any more!
        $charges = max(0,$this->getPaid() - $ownervalue - $comission - $comissionvat);

        // Work back what's actually left on the account to take as comission
        $prevatcomission = max(0,$this->getPaid() - $ownervalue - $charges);
        $comissionvat = floor($prevatcomission * (1-(100/(100+$vatrate)))*100)/100;
        return $comissionvat;
    }

    public function getToOwnersValue(){
        if($this->isNormalBooking() || $this->isCancelAndNoRefund()){
            if((int)$this->paid === 0) {
                return 0;
            }
            return $this->paid - $this->calculateCommissionValue() - $this->getCommissionVatValue() - $this->getPriceDecoded()->getAdminCharge() - $this->getPriceDecoded()->getCreditCardCharge() - $this->getPriceDecoded()->getBookingProtectCharge() - $this->getPriceDecoded()->getCharityCharge();
        }else{
            return 0;
        }
    }

    public function getSleepsAdults(){
        return $this->getSleeps() - $this->getSleepsChildren();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set id
     *
     * @param integer $id
     * @return Reservation
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Set status
     *
     * @param integer $status
     * @return Reservation
     */
    public function setStatus($status)
    {
        $this->status = (int) $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return integer 
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return Reservation
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime 
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set paid
     *
     * @param string $paid
     * @return Reservation
     */
    public function setPaid($paid)
    {
        $this->paid = $paid;

        return $this;
    }

    /**
     * Get paid
     *
     * @return string 
     */
    public function getPaid()
    {
        return $this->paid;
    }

    /**
     * Set estate
     *
     * @param \UTT\EstateBundle\Entity\Estate $estate
     * @return Reservation
     */
    public function setEstate(\UTT\EstateBundle\Entity\Estate $estate = null)
    {
        $this->estate = $estate;

        return $this;
    }

    /**
     * Get estate
     *
     * @return \UTT\EstateBundle\Entity\Estate 
     */
    public function getEstate()
    {
        return $this->estate;
    }

    /**
     * Set user
     *
     * @param \UTT\UserBundle\Entity\User $user
     * @return Reservation
     */
    public function setUser(\UTT\UserBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \UTT\UserBundle\Entity\User 
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set sleeps
     *
     * @param integer $sleeps
     * @return Reservation
     */
    public function setSleeps($sleeps)
    {
        $this->sleeps = $sleeps;

        return $this;
    }

    /**
     * Get sleeps
     *
     * @return integer 
     */
    public function getSleeps()
    {
        return $this->sleeps;
    }

    /**
     * Set pets
     *
     * @param integer $pets
     * @return Reservation
     */
    public function setPets($pets)
    {
        $this->pets = $pets;

        return $this;
    }

    /**
     * Get pets
     *
     * @return integer 
     */
    public function getPets()
    {
        return $this->pets;
    }

    /**
     * Set infants
     *
     * @param integer $infants
     * @return Reservation
     */
    public function setInfants($infants)
    {
        $this->infants = $infants;

        return $this;
    }

    /**
     * Get infants
     *
     * @return integer 
     */
    public function getInfants()
    {
        return $this->infants;
    }

    /**
     * Set userData
     *
     * @param string $userData
     * @return Reservation
     */
    public function setUserData($userData)
    {
        $this->userData = $userData;

        return $this;
    }

    /**
     * Get userData
     *
     * @return string 
     */
    public function getUserData()
    {
        return $this->userData;
    }

    /**
     * @return false|string
     */
    public function getUserDataEscaped()
    {
        $userData = $this->getUserDataDecoded();
        $userData = json_encode($userData, JSON_HEX_APOS);
        return $userData;
    }

    /**
     * Get userData
     *
     * @return ReservationDataUserData
     */
    public function getUserDataDecoded()
    {
        return new ReservationDataUserData(json_decode($this->userData));
    }

    /**
     * Set totalPrice
     *
     * @param string $totalPrice
     * @return Reservation
     */
    public function setTotalPrice($totalPrice)
    {
        $this->totalPrice = $totalPrice;

        return $this;
    }

    /**
     * Get totalPrice
     *
     * @return string 
     */
    public function getTotalPrice()
    {
        return $this->totalPrice;
    }

    /**
     * Set price
     *
     * @param string $price
     * @return Reservation
     */
    public function setPrice($price)
    {
        $this->price = $price;

        return $this;
    }

    /**
     * Get price
     *
     * @return string 
     */
    public function getPrice()
    {
        return $this->price;
    }
    /**
     * Get price
     *
     * @return ReservationDataPrice
     */
    public function getPriceDecoded()
    {
        return new ReservationDataPrice(json_decode($this->price));
    }

    /**
     * Set discountPrice
     *
     * @param string $discountPrice
     * @return Reservation
     */
    public function setDiscountPrice($discountPrice)
    {
        $this->discountPrice = $discountPrice;

        return $this;
    }

    /**
     * Get discountPrice
     *
     * @return string 
     */
    public function getDiscountPrice()
    {
        return $this->discountPrice;
    }

    /**
     * Get discountPrice
     *
     * @return ReservationDataDiscountPrice
     */
    public function getDiscountPriceDecoded()
    {
        return new ReservationDataDiscountPrice(json_decode($this->discountPrice));
    }

    /**
     * Set estateData
     *
     * @param string $estateData
     * @return Reservation
     */
    public function setEstateData($estateData)
    {
        $this->estateData = $estateData;

        return $this;
    }

    /**
     * Get estateData
     *
     * @return string 
     */
    public function getEstateData()
    {
        return $this->estateData;
    }

    /**
     * Get estateData
     *
     * @return ReservationDataEstateData
     */
    public function getEstateDataDecoded()
    {
        return new ReservationDataEstateData(json_decode($this->estateData));
    }

    /**
     * Set deposit
     *
     * @param string $deposit
     * @return Reservation
     */
    public function setDeposit($deposit)
    {
        $this->deposit = $deposit;

        return $this;
    }

    /**
     * Get deposit
     *
     * @return string 
     */
    public function getDeposit()
    {
        return $this->deposit;
    }

    /**
     * Set balanceDueDate
     *
     * @param \DateTime $balanceDueDate
     * @return Reservation
     */
    public function setBalanceDueDate($balanceDueDate)
    {
        $this->balanceDueDate = $balanceDueDate;

        return $this;
    }

    /**
     * Get balanceDueDate
     *
     * @return \DateTime 
     */
    public function getBalanceDueDate()
    {
        return $this->balanceDueDate;
    }

    /**
     * Set fromDate
     *
     * @param \DateTime $fromDate
     * @return Reservation
     */
    public function setFromDate($fromDate)
    {
        $this->fromDate = $fromDate;

        return $this;
    }

    /**
     * Get fromDate
     *
     * @return \DateTime 
     */
    public function getFromDate()
    {
        return $this->fromDate;
    }

    /**
     * Set toDate
     *
     * @param \DateTime $toDate
     * @return Reservation
     */
    public function setToDate($toDate)
    {
        $this->toDate = $toDate;

        return $this;
    }

    /**
     * Get toDate
     *
     * @return \DateTime 
     */
    public function getToDate()
    {
        return $this->toDate;
    }

    /**
     * Add historyEntries
     *
     * @param \UTT\ReservationBundle\Entity\HistoryEntry $historyEntries
     * @return Reservation
     */
    public function addHistoryEntry(\UTT\ReservationBundle\Entity\HistoryEntry $historyEntries)
    {
        $this->historyEntries[] = $historyEntries;

        return $this;
    }

    /**
     * Remove historyEntries
     *
     * @param \UTT\ReservationBundle\Entity\HistoryEntry $historyEntries
     */
    public function removeHistoryEntry(\UTT\ReservationBundle\Entity\HistoryEntry $historyEntries)
    {
        $this->historyEntries->removeElement($historyEntries);
    }

    /**
     * Get historyEntries
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getHistoryEntries()
    {
        return $this->historyEntries;
    }

    /**
     * Add paymentTransactions
     *
     * @param \UTT\ReservationBundle\Entity\PaymentTransaction $paymentTransactions
     * @return Reservation
     */
    public function addPaymentTransaction(\UTT\ReservationBundle\Entity\PaymentTransaction $paymentTransactions)
    {
        $this->paymentTransactions[] = $paymentTransactions;

        return $this;
    }

    /**
     * Remove paymentTransactions
     *
     * @param \UTT\ReservationBundle\Entity\PaymentTransaction $paymentTransactions
     */
    public function removePaymentTransaction(\UTT\ReservationBundle\Entity\PaymentTransaction $paymentTransactions)
    {
        $this->paymentTransactions->removeElement($paymentTransactions);
    }

    /**
     * Get paymentTransactions
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getPaymentTransactions()
    {
        return $this->paymentTransactions;
    }

    /**
     * Set note
     *
     * @param string $note
     * @return Reservation
     */
    public function setNote($note)
    {
        $this->note = $note;

        return $this;
    }

    /**
     * Get note
     *
     * @return string 
     */
    public function getNote()
    {
        return $this->note;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     * @return Reservation
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime 
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set externalId
     *
     * @param integer $externalId
     * @return Reservation
     */
    public function setExternalId($externalId)
    {
        $this->externalId = $externalId;

        return $this;
    }

    /**
     * Get externalId
     *
     * @return integer 
     */
    public function getExternalId()
    {
        return $this->externalId;
    }

    /**
     * Set externalData
     *
     * @param string $externalData
     * @return Reservation
     */
    public function setExternalData($externalData)
    {
        $this->externalData = $externalData;

        return $this;
    }

    /**
     * Get externalData
     *
     * @return string 
     */
    public function getExternalData()
    {
        return $this->externalData;
    }

    /**
     * Set commission
     *
     * @param integer $commission
     * @return Reservation
     */
    public function setCommission($commission)
    {
        $this->commission = $commission;

        return $this;
    }

    /**
     * Get commission
     *
     * @return integer 
     */
    public function getCommission()
    {
        return $this->commission;
    }

    /**
     * Set ipAddress
     *
     * @param string $ipAddress
     * @return Reservation
     */
    public function setIpAddress($ipAddress)
    {
        $this->ipAddress = $ipAddress;

        return $this;
    }

    /**
     * Get ipAddress
     *
     * @return string 
     */
    public function getIpAddress()
    {
        return $this->ipAddress;
    }

    /**
     * Set pin
     *
     * @param string $pin
     * @return Reservation
     */
    public function setPin($pin)
    {
        $this->pin = $pin;

        return $this;
    }

    /**
     * Get pin
     *
     * @return string 
     */
    public function getPin()
    {
        return $this->pin;
    }

    /**
     * Set externalPin
     *
     * @param string $externalPin
     * @return Reservation
     */
    public function setExternalPin($externalPin)
    {
        $this->externalPin = $externalPin;

        return $this;
    }

    /**
     * Get externalPin
     *
     * @return string 
     */
    public function getExternalPin()
    {
        return $this->externalPin;
    }

    /**
     * Set referredFlag
     *
     * @param integer $referredFlag
     * @return Reservation
     */
    public function setReferredFlag($referredFlag)
    {
        $this->referredFlag = $referredFlag;

        return $this;
    }

    /**
     * Get referredFlag
     *
     * @return integer 
     */
    public function getReferredFlag()
    {
        return $this->referredFlag;
    }

    /**
     * Set referredFlagDate
     *
     * @param \DateTime $referredFlagDate
     * @return Reservation
     */
    public function setReferredFlagDate($referredFlagDate)
    {
        $this->referredFlagDate = $referredFlagDate;

        return $this;
    }

    /**
     * Get referredFlagDate
     *
     * @return \DateTime 
     */
    public function getReferredFlagDate()
    {
        return $this->referredFlagDate;
    }

    /**
     * Set sleepsChildren
     *
     * @param integer $sleepsChildren
     * @return Reservation
     */
    public function setSleepsChildren($sleepsChildren)
    {
        $this->sleepsChildren = $sleepsChildren;

        return $this;
    }

    /**
     * Get sleepsChildren
     *
     * @return integer 
     */
    public function getSleepsChildren()
    {
        return $this->sleepsChildren;
    }

    /**
     * Set sessionId
     *
     * @param string $sessionId
     * @return Reservation
     */
    public function setSessionId($sessionId)
    {
        $this->sessionId = $sessionId;

        return $this;
    }

    /**
     * Get sessionId
     *
     * @return string 
     */
    public function getSessionId()
    {
        return $this->sessionId;
    }

    /**
     * Set isThankYouSent
     *
     * @param boolean $isThankYouSent
     * @return Reservation
     */
    public function setIsThankYouSent($isThankYouSent)
    {
        $this->isThankYouSent = $isThankYouSent;

        return $this;
    }

    /**
     * Get isThankYouSent
     *
     * @return boolean 
     */
    public function getIsThankYouSent()
    {
        return $this->isThankYouSent;
    }

    /**
     * Set commissionValue
     *
     * @param string $commissionValue
     * @return Reservation
     */
    public function setCommissionValue($commissionValue)
    {
        $this->commissionValue = $commissionValue;

        return $this;
    }

    /**
     * Get commissionValue
     *
     * @return string 
     */
    public function getCommissionValue()
    {
        return $this->commissionValue;
    }

    /**
     * Set noPaymentCancellationReinstateDate
     *
     * @param \DateTime $noPaymentCancellationReinstateDate
     * @return Reservation
     */
    public function setNoPaymentCancellationReinstateDate($noPaymentCancellationReinstateDate)
    {
        $this->noPaymentCancellationReinstateDate = $noPaymentCancellationReinstateDate;

        return $this;
    }

    /**
     * Get noPaymentCancellationReinstateDate
     *
     * @return \DateTime 
     */
    public function getNoPaymentCancellationReinstateDate()
    {
        return $this->noPaymentCancellationReinstateDate;
    }

    /**
     * Set isNoPaymentCancellationReinstateSent
     *
     * @param boolean $isNoPaymentCancellationReinstateSent
     * @return Reservation
     */
    public function setIsNoPaymentCancellationReinstateSent($isNoPaymentCancellationReinstateSent)
    {
        $this->isNoPaymentCancellationReinstateSent = $isNoPaymentCancellationReinstateSent;

        return $this;
    }

    /**
     * Get isNoPaymentCancellationReinstateSent
     *
     * @return boolean 
     */
    public function getIsNoPaymentCancellationReinstateSent()
    {
        return $this->isNoPaymentCancellationReinstateSent;
    }

    /**
     * Set paidWithVoucher
     *
     * @param \UTT\ReservationBundle\Entity\Voucher $paidWithVoucher
     * @return Reservation
     */
    public function setPaidWithVoucher(\UTT\ReservationBundle\Entity\Voucher $paidWithVoucher = null)
    {
        $this->paidWithVoucher = $paidWithVoucher;

        return $this;
    }

    /**
     * Get paidWithVoucher
     *
     * @return \UTT\ReservationBundle\Entity\Voucher 
     */
    public function getPaidWithVoucher()
    {
        return $this->paidWithVoucher;
    }

    /**
     * Set is1hourNoPaymentCancellationReinstateSent
     *
     * @param boolean $is1hourNoPaymentCancellationReinstateSent
     * @return Reservation
     */
    public function setIs1hourNoPaymentCancellationReinstateSent($is1hourNoPaymentCancellationReinstateSent)
    {
        $this->is1hourNoPaymentCancellationReinstateSent = $is1hourNoPaymentCancellationReinstateSent;

        return $this;
    }

    /**
     * Get is1hourNoPaymentCancellationReinstateSent
     *
     * @return boolean 
     */
    public function getIs1hourNoPaymentCancellationReinstateSent()
    {
        return $this->is1hourNoPaymentCancellationReinstateSent;
    }

    /**
     * Set isBookingProtect
     *
     * @param boolean $isBookingProtect
     * @return Reservation
     */
    public function setIsBookingProtect($isBookingProtect)
    {
        $this->isBookingProtect = $isBookingProtect;

        return $this;
    }

    /**
     * Get isBookingProtect
     *
     * @return boolean 
     */
    public function getIsBookingProtect()
    {
        return $this->isBookingProtect;
    }

    /**
     * Set isHoldingForPayment
     *
     * @param boolean $isHoldingForPayment
     * @return Reservation
     */
    public function setIsHoldingForPayment($isHoldingForPayment)
    {
        $this->isHoldingForPayment = $isHoldingForPayment;

        return $this;
    }

    /**
     * Get isHoldingForPayment
     *
     * @return boolean 
     */
    public function getIsHoldingForPayment()
    {
        return $this->isHoldingForPayment;
    }

    /**
     * Set holdingForPaymentUntilDate
     *
     * @param \DateTime $holdingForPaymentUntilDate
     * @return Reservation
     */
    public function setHoldingForPaymentUntilDate($holdingForPaymentUntilDate)
    {
        $this->holdingForPaymentUntilDate = $holdingForPaymentUntilDate;

        return $this;
    }

    /**
     * Get holdingForPaymentUntilDate
     *
     * @return \DateTime 
     */
    public function getHoldingForPaymentUntilDate()
    {
        return $this->holdingForPaymentUntilDate;
    }

    /**
     * @return int
     */
    public function getManagerId()
    {
        return $this->managerId;
    }

    /**
     * @param $managerId
     * @return $this
     */
    public function setManagerId($managerId)
    {
        $this->managerId = $managerId;
        return $this;
    }


}
