<?php

namespace UTT\ReservationBundle\Entity;

use Doctrine\ORM\EntityRepository;

class PaymentTransactionRepository extends EntityRepository
{
    public function getCharityPaymentsByMonthArray(){
        $minDate = new \DateTime('now');
        $minDate->modify('-12 months');

        $query = $this->_em->createQuery("
            SELECT
              sum(pt.amountCharityCharge) as charityCharge,
              SUBSTRING(pt.createdAt, 1, 7) as month
            FROM UTTReservationBundle:PaymentTransaction pt
            WHERE SUBSTRING(pt.createdAt, 1, 7) > :minDate AND pt.amountCharityCharge > 0 AND pt.status = :statusSuccess
            GROUP BY month
            ORDER BY month DESC
        ");
        $query->setParameter('statusSuccess', PaymentTransaction::STATUS_SUCCESS);
        $query->setParameter('minDate', $minDate);

        $result = $query->getArrayResult();
        if(count($result) > 0) return $result;

        return false;
    }

    public function getCharityPaymentsByDayArray(){
        $minDate = new \DateTime('now');
        $minDate->modify('-30 days');

        $query = $this->_em->createQuery("
            SELECT
              sum(pt.amountCharityCharge) as charityCharge,
              SUBSTRING(pt.createdAt, 1, 10) as day
            FROM UTTReservationBundle:PaymentTransaction pt
            WHERE SUBSTRING(pt.createdAt, 1, 10) > :minDate AND pt.amountCharityCharge > 0 AND pt.status = :statusSuccess
            GROUP BY day
            ORDER BY day DESC
        ");
        $query->setParameter('statusSuccess', PaymentTransaction::STATUS_SUCCESS);
        $query->setParameter('minDate', $minDate);

        $result = $query->getArrayResult();
        if(count($result) > 0) return $result;

        return false;
    }

    public function getCharityPaymentsSum(){
        $query = $this->_em->createQuery("SELECT sum(pt.amountCharityCharge) as charityCharge FROM UTTReservationBundle:PaymentTransaction pt WHERE pt.status = :statusSuccess");
        $query->setParameter('statusSuccess', PaymentTransaction::STATUS_SUCCESS);

        $result = $query->getArrayResult();
        if(isset($result[0]) && isset($result[0]['charityCharge'])){
            return $result[0]['charityCharge'];
        }

        return 0;
    }
}
