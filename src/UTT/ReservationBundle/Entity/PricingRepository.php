<?php

namespace UTT\ReservationBundle\Entity;

use Doctrine\ORM\EntityRepository;
use UTT\EstateBundle\Entity\Estate;
use UTT\ReservationBundle\Entity\Pricing;

class PricingRepository extends EntityRepository
{
    public function getPricingList(Estate $estate, \Datetime $fromDate, \Datetime $toDate){
        if(!($estate->getType() == Estate::TYPE_FLEXIBLE_BOOKING || $estate->getType() == Estate::TYPE_STANDARD_M_F)){ return false; }

        $query = $this->_em->createQuery("
            SELECT p
            FROM UTTReservationBundle:Pricing p
            WHERE
                (
                  (
                    p.type = :pricingTypeFlexible
                  )
                  OR
                  (
                    p.type = :pricingTypeStandard AND
                    p.fromDate >= :fromDate AND
                    p.toDate >= :fromDate
                  )
                ) AND
                p.type = :pricingType AND
                p.estate = :estate AND
                (
                    (p.fromDate >= :fromDate OR p.toDate >= :fromDate) AND (p.fromDate <= :toDate OR p.toDate <= :toDate)
                )
            ORDER BY p.fromDate ASC");
        $query->setParameters(array(
            'estate' => $estate->getId(),
            'fromDate' => $fromDate,
            'toDate' => $toDate,
            'pricingTypeFlexible' => Pricing::TYPE_FLEXIBLE_BOOKING,
            'pricingTypeStandard' => Pricing::TYPE_STANDARD_M_F
        ));

        if($estate->getType() == Estate::TYPE_FLEXIBLE_BOOKING){
            $query->setParameter('pricingType', Pricing::TYPE_FLEXIBLE_BOOKING);
        }elseif($estate->getType() == Estate::TYPE_STANDARD_M_F){
            $query->setParameter('pricingType', Pricing::TYPE_STANDARD_M_F);
        }

        $result = $query->getResult();
        if(count($result) > 0) return $result;

        return false;
    }

    public function getFlexiblePricing(Estate $estate, \Datetime $fromDate, \Datetime $toDate){
        if(!($estate->getType() == Estate::TYPE_FLEXIBLE_BOOKING)){ return false; }

        $query = $this->_em->createQuery("
            SELECT p
            FROM UTTReservationBundle:Pricing p
            WHERE
                p.type = :pricingType AND
                p.estate = :estate AND
                (
                    (p.fromDate >= :fromDate AND p.fromDate <= :toDate) OR
                    (p.toDate >= :fromDate AND p.toDate <= :toDate) OR
                    (p.fromDate <= :fromDate AND p.fromDate <= :toDate AND p.toDate >= :fromDate AND p.toDate >= :toDate)
                )
            ORDER BY p.fromDate ASC");
        $query->setParameters(array(
            'pricingType' => Pricing::TYPE_FLEXIBLE_BOOKING,
            'estate' => $estate->getId(),
            'fromDate' => $fromDate,
            'toDate' => $toDate
        ));

        $result = $query->getResult();
        if(count($result) > 0) return $result;

        return false;
    }

    public function getStandardPricing(Estate $estate, \Datetime $fromDate, \Datetime $toDate){
        if(!($estate->getType() == Estate::TYPE_STANDARD_M_F)){ return false; }

        $query = $this->_em->createQuery("
            SELECT p as pricing, DATE_DIFF(p.toDate, p.fromDate) as dateDiff
            FROM UTTReservationBundle:Pricing p
            WHERE
                p.type = :pricingType AND
                p.estate = :estate AND
                (
                    (p.fromDate = :fromDate AND p.toDate >= :toDate) OR
                    (p.fromDate <= :fromDate AND p.toDate = :toDate) OR
                    (p.fromDate <= :fromDate AND p.toDate >= :toDate)
                )
            ORDER BY dateDiff ASC");

        $query->setParameters(array(
            'pricingType' => Pricing::TYPE_STANDARD_M_F,
            'estate' => $estate->getId(),
            'fromDate' => $fromDate,
            'toDate' => $toDate
        ));
        $query->setMaxResults(1);

        if($query->getResult()){
            $result = $query->getSingleResult();
            if($result) return $result['pricing'];
        }

        return false;
        /*
        $pricing = $this->_em->getRepository('UTTReservationBundle:Pricing')->findOneBy(array(
            'type' => Pricing::TYPE_STANDARD_M_F,
            'estate' => $estate->getId(),
            'fromDate' => $fromDate,
            'toDate' => $toDate
        ));

        if($pricing) return $pricing;

        return false;
        */
    }

    public function getStandardPricingForManagePrice(Estate $estate, \Datetime $fromDate, \Datetime $toDate){
        if(!($estate->getType() == Estate::TYPE_STANDARD_M_F)){ return false; }

        $query = $this->_em->createQuery("
            SELECT p
            FROM UTTReservationBundle:Pricing p
            WHERE
                p.type = :pricingType AND
                p.estate = :estate AND
                (
                    (p.fromDate = :fromDate AND p.toDate <= :toDate)
                )
            ORDER BY p.fromDate ASC");

        $query->setParameters(array(
            'pricingType' => Pricing::TYPE_STANDARD_M_F,
            'estate' => $estate->getId(),
            'fromDate' => $fromDate,
            'toDate' => $toDate
        ));
        $query->setMaxResults(1);

        if($query->getResult()){
            $result = $query->getSingleResult();
            if($result) return $result;
        }

        return false;
    }
}
