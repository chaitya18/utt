<?php

namespace UTT\ReservationBundle\Entity;

use Doctrine\ORM\EntityRepository;
use UTT\ReservationBundle\Entity\OwnerStatement;

class OwnerStatementRepository extends EntityRepository
{
    public function getStatementsForDate(\Datetime $monthDate, $statementType, $returnSingle = false){
        $query = $this->_em->createQuery("
            SELECT os FROM UTTReservationBundle:OwnerStatement os
            WHERE SUBSTRING(os.monthDate, 1, 7) = SUBSTRING(:monthDate, 1, 7) AND os.type = :statementType
            ");
        $query->setParameters(array(
            'monthDate' => $monthDate,
            'statementType' => $statementType
        ));

        $result = $query->getResult();
        if(count($result) > 0){
            if($returnSingle){
                return $result[0];
            }else{
                return $result;
            }
        }

        return false;
    }
}
