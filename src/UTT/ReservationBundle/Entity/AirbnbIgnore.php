<?php

namespace UTT\ReservationBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * AirbnbIgnore
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="UTT\ReservationBundle\Entity\AirbnbIgnoreRepository")
 */
class AirbnbIgnore
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="UTT\EstateBundle\Entity\Estate")
     * @ORM\JoinColumn(name="estate_id", referencedColumnName="id", nullable=true, onDelete="CASCADE")
     */
    protected $estate;

    /**
     * @ORM\Column(name="from_date", type="date")
     */
    protected $fromDate;

    /**
     * @ORM\Column(name="to_date", type="date")
     */
    protected $toDate;

    public function areDatesEqual(\DateTime $fromDate, \DateTime $toDate){
        if($fromDate->format('m') == $this->getFromDate()->format('m')){
            if($fromDate->format('d') == $this->getFromDate()->format('d')){
                if($fromDate->format('y') == $this->getFromDate()->format('y')){
                    if($toDate->format('m') == $this->getToDate()->format('m')){
                        if($toDate->format('d') == $this->getToDate()->format('d')){
                            if($toDate->format('y') == $this->getToDate()->format('y')){
                                return true;
                            }
                        }
                    }
                }
            }
        }

        return false;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set fromDate
     *
     * @param \DateTime $fromDate
     * @return AirbnbIgnore
     */
    public function setFromDate($fromDate)
    {
        $this->fromDate = $fromDate;

        return $this;
    }

    /**
     * Get fromDate
     *
     * @return \DateTime 
     */
    public function getFromDate()
    {
        return $this->fromDate;
    }

    /**
     * Set toDate
     *
     * @param \DateTime $toDate
     * @return AirbnbIgnore
     */
    public function setToDate($toDate)
    {
        $this->toDate = $toDate;

        return $this;
    }

    /**
     * Get toDate
     *
     * @return \DateTime 
     */
    public function getToDate()
    {
        return $this->toDate;
    }

    /**
     * Set estate
     *
     * @param \UTT\EstateBundle\Entity\Estate $estate
     * @return AirbnbIgnore
     */
    public function setEstate(\UTT\EstateBundle\Entity\Estate $estate = null)
    {
        $this->estate = $estate;

        return $this;
    }

    /**
     * Get estate
     *
     * @return \UTT\EstateBundle\Entity\Estate 
     */
    public function getEstate()
    {
        return $this->estate;
    }
}
