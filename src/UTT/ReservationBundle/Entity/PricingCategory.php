<?php

namespace UTT\ReservationBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * PricingCategory
 *
 * @ORM\Table(name="pricing_category")
 * @ORM\Entity(repositoryClass="UTT\ReservationBundle\Entity\PricingCategoryRepository")
 */
class PricingCategory
{
    public function __construct() {
        $this->pricesStandard = new ArrayCollection();
    }

    const TYPE_FLEXIBLE_BOOKING = 1;
    const TYPE_STANDARD_M_F = 2;

    public function getAllowedTypes(){
        $array = array(
            self::TYPE_FLEXIBLE_BOOKING => 'Flexible booking',
            self::TYPE_STANDARD_M_F => 'Standard (M-F)'
        );
        return $array;
    }

    public function getTypeName($typeId = null){
        $array = $this->getAllowedTypes();
        if(is_null($typeId)){
            if($this->getType()) {
                return array_key_exists($this->getType(), $array) ? $array[$this->getType()] : false;
            }else{
                return false;
            }
        }
        return array_key_exists($typeId, $array) ? $array[$typeId] : false;
    }

    public function isStandard(){
        if($this->getType() == self::TYPE_STANDARD_M_F){
            return true;
        }

        return false;
    }

    public function isFlexible(){
        if($this->getType() == self::TYPE_FLEXIBLE_BOOKING){
            return true;
        }

        return false;
    }

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(name="name", type="string", length=12)
     */
    protected $name;

    /**
     * @ORM\Column(name="type", type="integer")
     */
    protected $type;

    /**
     * @ORM\OneToMany(targetEntity="PricingCategoryPriceStandard", mappedBy="pricingCategory", cascade={"persist"})
     */
    protected $pricesStandard;

    /**
     * @ORM\OneToMany(targetEntity="PricingCategoryPriceFlexible", mappedBy="pricingCategory", cascade={"persist"})
     */
    protected $pricesFlexible;

    public function __toString(){
        if($this->getName())
            return $this->getName();
        return '';
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return PricingCategory
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set type
     *
     * @param integer $type
     * @return PricingCategory
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return integer 
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Add pricesStandard
     *
     * @param \UTT\ReservationBundle\Entity\PricingCategoryPriceStandard $pricesStandard
     * @return PricingCategory
     */
    public function addPricesStandard(\UTT\ReservationBundle\Entity\PricingCategoryPriceStandard $pricesStandard)
    {
        $this->pricesStandard[] = $pricesStandard;

        return $this;
    }

    /**
     * Remove pricesStandard
     *
     * @param \UTT\ReservationBundle\Entity\PricingCategoryPriceStandard $pricesStandard
     */
    public function removePricesStandard(\UTT\ReservationBundle\Entity\PricingCategoryPriceStandard $pricesStandard)
    {
        $this->pricesStandard->removeElement($pricesStandard);
    }

    /**
     * Get pricesStandard
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getPricesStandard()
    {
        return $this->pricesStandard;
    }

    /**
     * Add pricesFlexible
     *
     * @param \UTT\ReservationBundle\Entity\PricingCategoryPriceFlexible $pricesFlexible
     * @return PricingCategory
     */
    public function addPricesFlexible(\UTT\ReservationBundle\Entity\PricingCategoryPriceFlexible $pricesFlexible)
    {
        $this->pricesFlexible[] = $pricesFlexible;

        return $this;
    }

    /**
     * Remove pricesFlexible
     *
     * @param \UTT\ReservationBundle\Entity\PricingCategoryPriceFlexible $pricesFlexible
     */
    public function removePricesFlexible(\UTT\ReservationBundle\Entity\PricingCategoryPriceFlexible $pricesFlexible)
    {
        $this->pricesFlexible->removeElement($pricesFlexible);
    }

    /**
     * Get pricesFlexible
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getPricesFlexible()
    {
        return $this->pricesFlexible;
    }
}
