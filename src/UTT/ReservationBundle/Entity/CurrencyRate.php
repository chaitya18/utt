<?php

namespace UTT\ReservationBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * CurrencyRate
 *
 * @ORM\Table(name="currency_rate")
 * @ORM\Entity(repositoryClass="UTT\ReservationBundle\Entity\CurrencyRateRepository")
 */
class CurrencyRate
{
    CONST CURRENCY_CODE_UNITED_KINGDOM_POUND = 'GBP';
    CONST CURRENCY_CODE_EURO_MEMBER_COUNTRIES = 'EUR';
    CONST CURRENCY_CODE_POLAND_ZLOTY = 'PLN';

    public function getAllowedCurrencyCodes(){
        $array = array(
            self::CURRENCY_CODE_UNITED_KINGDOM_POUND => 'United Kingdom Pound',
            self::CURRENCY_CODE_EURO_MEMBER_COUNTRIES => 'Euro Member Countries',
            self::CURRENCY_CODE_POLAND_ZLOTY => 'Poland Zloty'
        );
        return $array;
    }

    static function getAllowedCurrencyCodesStatic(){
        $array = array(
            self::CURRENCY_CODE_UNITED_KINGDOM_POUND => 'United Kingdom Pound',
            self::CURRENCY_CODE_EURO_MEMBER_COUNTRIES => 'Euro Member Countries',
            self::CURRENCY_CODE_POLAND_ZLOTY => 'Poland Zloty'
        );
        return $array;
    }

    public function getCurrencyCodeName($currencyCode = null){
        $array = $this->getAllowedCurrencyCodes();
        if(is_null($currencyCode)){
            if($this->getCurrencyCode()) {
                return array_key_exists($this->getCurrencyCode(), $array) ? $array[$this->getCurrencyCode()] : false;
            }else{
                return false;
            }
        }
        return array_key_exists($currencyCode, $array) ? $array[$currencyCode] : false;
    }

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(name="currency_code", type="string", nullable=false)
     */
    protected $currencyCode;

    /**
     * @ORM\Column(name="rate_date", type="date", nullable=false)
     */
    protected $rateDate;

    /**
     * @ORM\Column(name="value", type="decimal", scale=5, nullable=true)
     */
    protected $value;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set currencyCode
     *
     * @param string $currencyCode
     * @return CurrencyRate
     */
    public function setCurrencyCode($currencyCode)
    {
        $this->currencyCode = $currencyCode;

        return $this;
    }

    /**
     * Get currencyCode
     *
     * @return string 
     */
    public function getCurrencyCode()
    {
        return $this->currencyCode;
    }

    /**
     * Set rateDate
     *
     * @param \DateTime $rateDate
     * @return CurrencyRate
     */
    public function setRateDate($rateDate)
    {
        $this->rateDate = $rateDate;

        return $this;
    }

    /**
     * Get rateDate
     *
     * @return \DateTime 
     */
    public function getRateDate()
    {
        return $this->rateDate;
    }

    /**
     * Set value
     *
     * @param string $value
     * @return CurrencyRate
     */
    public function setValue($value)
    {
        $this->value = $value;

        return $this;
    }

    /**
     * Get value
     *
     * @return string 
     */
    public function getValue()
    {
        return $this->value;
    }
}
