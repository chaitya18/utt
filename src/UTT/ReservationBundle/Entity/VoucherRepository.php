<?php

namespace UTT\ReservationBundle\Entity;

use Doctrine\ORM\EntityRepository;

class VoucherRepository extends EntityRepository
{
    public function findVoucherByCode($code){
        $query = $this->_em->createQuery("
            SELECT v, vdc
            FROM UTTReservationBundle:Voucher v
            LEFT JOIN v.discountCode vdc
            WHERE vdc.code = :voucherCode
        ");
        $query->setParameter('voucherCode', $code);
        $query->setMaxResults(1);

        $result = $query->getResult();
        if(count($result) > 0) return $query->getSingleResult();

        return false;
    }

    public function getLatestByUserId($userId, $limit = 3){
        $query = $this->_em->createQuery("
            SELECT v
            FROM UTTReservationBundle:Voucher v
            WHERE
                v.user = :userId
            ORDER BY v.id DESC");
        $query->setParameters(array(
            'userId' => $userId
        ));

        if((integer) $limit){ $query->setMaxResults((integer) $limit); }

        $result = $query->getResult();
        if(count($result) > 0) return $result;

        return false;
    }

    /**
     * @param Voucher $voucher
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function persist(Voucher $voucher)
    {
        $this->_em->persist($voucher);
        $this->_em->flush();
    }
}
