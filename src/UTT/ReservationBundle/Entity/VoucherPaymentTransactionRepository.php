<?php

namespace UTT\ReservationBundle\Entity;

use Doctrine\ORM\EntityRepository;

class VoucherPaymentTransactionRepository extends EntityRepository
{
    public function getCharityPaymentsByMonthArray(){
        $minDate = new \DateTime('now');
        $minDate->modify('-12 months');

        $query = $this->_em->createQuery("
            SELECT
              sum(vpt.amountCharityCharge) as charityCharge,
              SUBSTRING(vpt.createdAt, 1, 7) as month
            FROM UTTReservationBundle:VoucherPaymentTransaction vpt
            WHERE SUBSTRING(vpt.createdAt, 1, 7) > :minDate AND vpt.amountCharityCharge > 0 AND vpt.status = :statusSuccess
            GROUP BY month
            ORDER BY month DESC
        ");
        $query->setParameter('statusSuccess', VoucherPaymentTransaction::STATUS_SUCCESS);
        $query->setParameter('minDate', $minDate);

        $result = $query->getArrayResult();
        if(count($result) > 0) return $result;

        return false;
    }

    public function getCharityPaymentsByDayArray(){
        $minDate = new \DateTime('now');
        $minDate->modify('-30 days');

        $query = $this->_em->createQuery("
            SELECT
              sum(vpt.amountCharityCharge) as charityCharge,
              SUBSTRING(vpt.createdAt, 1, 10) as day
            FROM UTTReservationBundle:VoucherPaymentTransaction vpt
            WHERE SUBSTRING(vpt.createdAt, 1, 10) > :minDate AND vpt.amountCharityCharge > 0 AND vpt.status = :statusSuccess
            GROUP BY day
            ORDER BY day DESC
        ");
        $query->setParameter('statusSuccess', VoucherPaymentTransaction::STATUS_SUCCESS);
        $query->setParameter('minDate', $minDate);

        $result = $query->getArrayResult();
        if(count($result) > 0) return $result;

        return false;
    }

    public function getCharityPaymentsSum(){
        $query = $this->_em->createQuery("SELECT sum(vpt.amountCharityCharge) as charityCharge FROM UTTReservationBundle:VoucherPaymentTransaction vpt WHERE vpt.status = :statusSuccess");
        $query->setParameter('statusSuccess', VoucherPaymentTransaction::STATUS_SUCCESS);

        $result = $query->getArrayResult();
        if(isset($result[0]) && isset($result[0]['charityCharge'])){
            return $result[0]['charityCharge'];
        }

        return 0;
    }
}
