<?php

namespace UTT\ReservationBundle\Entity;

use Doctrine\ORM\EntityRepository;
use UTT\EstateBundle\Entity\Estate;
use UTT\ReservationBundle\Entity\Reservation;

class ReservationRepository extends EntityRepository
{
    public function getActionListReservations($limit = 10, $ownerUserId = null){
        $queryOwnerUserId = 'WHERE r.referredFlag IS NOT NULL';
        if(!is_null($ownerUserId)){
            $queryOwnerUserId = 'LEFT JOIN r.estate e LEFT JOIN e.ownerUsers ou WHERE ou.id = :userId AND r.referredFlag IS NOT NULL';
        }

        $query = $this->_em->createQuery("
            SELECT r
            FROM UTTReservationBundle:Reservation r
            ".$queryOwnerUserId."
            ORDER BY r.updatedAt DESC");
        if(!is_null($ownerUserId)){ $query->setParameter('userId', $ownerUserId); }

        if((integer) $limit){ $query->setMaxResults((integer) $limit); }

        $result = $query->getResult();
        if(count($result) > 0) return $result;

        return false;
    }

    public function getLatelyUpdatedReservations($limit = 10, $ownerUserId = null){
        $queryOwnerUserId = '';
        if(!is_null($ownerUserId)){
            $queryOwnerUserId = 'LEFT JOIN r.estate e LEFT JOIN e.ownerUsers ou WHERE ou.id = :userId';
        }

        $query = $this->_em->createQuery("
            SELECT r
            FROM UTTReservationBundle:Reservation r
            ".$queryOwnerUserId."
            ORDER BY r.updatedAt DESC");
        if(!is_null($ownerUserId)){ $query->setParameter('userId', $ownerUserId); }

        if((integer) $limit){ $query->setMaxResults((integer) $limit); }

        $result = $query->getResult();
        if(count($result) > 0) return $result;

        return false;
    }

    public function getUpcomingReservations($limit = 10, $ownerUserId = null){
        $queryOwnerUserIdJoin = '';
        $queryOwnerUserId = '';
        if(!is_null($ownerUserId)){
            $queryOwnerUserIdJoin = 'LEFT JOIN r.estate e LEFT JOIN e.ownerUsers ou';
            $queryOwnerUserId = 'ou.id = :userId AND';
        }

        $query = $this->_em->createQuery("
            SELECT r
            FROM UTTReservationBundle:Reservation r
            ".$queryOwnerUserIdJoin." WHERE ".$queryOwnerUserId." r.fromDate >= :nowDate
            ORDER BY r.fromDate ASC
            ");
        $nowDate = new \DateTime('now');
        $nowDate->setTime(0,0,0);
        $query->setParameter('nowDate', $nowDate);
        if(!is_null($ownerUserId)){ $query->setParameter('userId', $ownerUserId); }

        if((integer) $limit){ $query->setMaxResults((integer) $limit); }

        $result = $query->getResult();
        if(count($result) > 0) return $result;

        return false;
    }

    public function getTodayIncomeArray(){
        $query = $this->_em->createQuery("
            SELECT sum(r.totalPrice) as summ FROM UTTReservationBundle:Reservation r
            WHERE SUBSTRING(r.createdAt, 1, 10) = SUBSTRING(:nowDate, 1, 10) AND (r.status = :statusFullyPaid OR r.status = :statusPartPaid OR r.status = :statusReserved)");
        $query->setParameter('nowDate', new \DateTime('now'));
        $query->setParameter('statusFullyPaid', Reservation::STATUS_FULLY_PAID);
        $query->setParameter('statusPartPaid', Reservation::STATUS_PART_PAID);
        $query->setParameter('statusReserved', Reservation::STATUS_RESERVED);

        $result = $query->getSingleScalarResult();
        if($result) return $result;

        return false;
    }

    public function getTodayUTTIncomeArray(){
        $query = $this->_em->createQuery("
            SELECT
              SUM(SUBSTRING(r.price, LOCATE('adminCharge\":\"', r.price ) + 14, LOCATE('\",\"creditCardCharge', r.price ) - 14 - LOCATE('adminCharge\":\"', r.price ))) as adminChargeSum,
              SUM(SUBSTRING(r.price, LOCATE('bookingProtectCharge\":\"', r.price ) + 23, LOCATE('\",\"charityCharge', r.price ) - 23 - LOCATE('bookingProtectCharge\":\"', r.price ))) as bookingProtectChargeSum,
              sum(r.totalPrice * r.commission / 100) as commissionValueFull
            FROM UTTReservationBundle:Reservation r
            WHERE
              SUBSTRING(r.createdAt, 1, 10) = SUBSTRING(:nowDate, 1, 10) AND
              (r.status = :statusFullyPaid OR r.status = :statusPartPaid)");
        $query->setParameter('nowDate', new \DateTime('now'));
        $query->setParameter('statusFullyPaid', Reservation::STATUS_FULLY_PAID);
        $query->setParameter('statusPartPaid', Reservation::STATUS_PART_PAID);

        $result = $query->getArrayResult();
        if($result) {
            if(isset($result[0])){
                if(isset($result[0]['adminChargeSum']) && isset($result[0]['bookingProtectChargeSum']) && isset($result[0]['commissionValueFull'])){
                    return (float) $result[0]['adminChargeSum'] + (float) $result[0]['bookingProtectChargeSum'] * 0.4 + (float) $result[0]['commissionValueFull'];
                }
            }
        }

        return false;
    }

    public function getReservationsByDateStringForSales($dateString, $ownerUserId = null){
        $queryOwnerUserIdJoin = '';
        $queryOwnerUserId = '';
        if(!is_null($ownerUserId)){
            $queryOwnerUserIdJoin = 'LEFT JOIN r.estate e LEFT JOIN e.ownerUsers ou';
            $queryOwnerUserId = 'ou.id = :userId AND';
        }

        $query = $this->_em->createQuery("
            SELECT r FROM UTTReservationBundle:Reservation r
            ".$queryOwnerUserIdJoin." WHERE ".$queryOwnerUserId." SUBSTRING(r.createdAt, 1, 10) LIKE :dateString");
        $query->setParameter('dateString', $dateString.'%');
        if(!is_null($ownerUserId)){ $query->setParameter('userId', $ownerUserId); }

        $result = $query->getResult();
        if(count($result) > 0) return $result;

        return false;
    }

    public function getReservationsByDateString($dateString, $ownerUserId = null){
        $queryOwnerUserIdJoin = '';
        $queryOwnerUserId = '';
        if(!is_null($ownerUserId)){
            $queryOwnerUserIdJoin = 'LEFT JOIN r.estate e LEFT JOIN e.ownerUsers ou';
            $queryOwnerUserId = 'ou.id = :userId AND';
        }

        $query = $this->_em->createQuery("
            SELECT r FROM UTTReservationBundle:Reservation r
            ".$queryOwnerUserIdJoin." WHERE ".$queryOwnerUserId." SUBSTRING(r.fromDate, 1, 10) LIKE :dateString ORDER BY r.fromDate");
        $query->setParameter('dateString', $dateString.'%');
        if(!is_null($ownerUserId)){ $query->setParameter('userId', $ownerUserId); }

        $result = $query->getResult();
        if(count($result) > 0) return $result;

        return false;
    }

    public function getReservationsForTaxInfo($year, $estateId){
        $dateFrom = new \DateTime();
        $dateFrom->setDate($year-1, 4, 5);

        $dateTo = new \DateTime();
        $dateTo->setDate($year, 4, 5);

        $query = $this->_em->createQuery("
            SELECT r FROM UTTReservationBundle:Reservation r
            WHERE r.fromDate >= :dateFrom AND r.fromDate <= :dateTo AND r.estate = :estateId ORDER BY r.fromDate");
        $query->setParameter('dateFrom', $dateFrom);
        $query->setParameter('dateTo', $dateTo);
        $query->setParameter('estateId', $estateId);

        $result = $query->getResult();
        if(count($result) > 0) return $result;

        return false;
    }

    public function getReservationsByDateStringAndEstateId($dateString, $estateId, $ownerUserId = null){
        $queryOwnerUserIdJoin = '';
        $queryOwnerUserId = '';
        if(!is_null($ownerUserId)){
            $queryOwnerUserIdJoin = 'LEFT JOIN r.estate e LEFT JOIN e.ownerUsers ou';
            $queryOwnerUserId = 'ou.id = :userId AND';
        }

        $query = $this->_em->createQuery("
            SELECT r FROM UTTReservationBundle:Reservation r
            ".$queryOwnerUserIdJoin." WHERE ".$queryOwnerUserId." SUBSTRING(r.fromDate, 1, 10) LIKE :dateString AND r.estate = :estateId ORDER BY r.fromDate");
        $query->setParameter('dateString', $dateString.'%');
        $query->setParameter('estateId', $estateId);
        if(!is_null($ownerUserId)){ $query->setParameter('userId', $ownerUserId); }

        $result = $query->getResult();
        if(count($result) > 0) return $result;

        return false;
    }

    public function getReservationsByYearStringAndEstateId($yearString, $estateId, $ownerUserId = null, $realBookingsOnly = true){
        $queryOwnerUserIdJoin = '';
        $queryOwnerUserId = '';
        if(!is_null($ownerUserId)){
            $queryOwnerUserIdJoin = 'LEFT JOIN r.estate e LEFT JOIN e.ownerUsers ou';
            $queryOwnerUserId = 'ou.id = :userId AND';
        }

        if($realBookingsOnly){
            $query = $this->_em->createQuery("
            SELECT r FROM UTTReservationBundle:Reservation r
            ".$queryOwnerUserIdJoin." WHERE ".$queryOwnerUserId." SUBSTRING(r.fromDate, 1, 4) LIKE :yearString AND r.estate = :estateId AND (r.status = :statusFullyPaid OR r.status = :statusPartPaid) ORDER BY r.fromDate ASC");
            $query->setParameter('statusFullyPaid', Reservation::STATUS_FULLY_PAID);
            $query->setParameter('statusPartPaid', Reservation::STATUS_PART_PAID);
        }else{
            $query = $this->_em->createQuery("
            SELECT r FROM UTTReservationBundle:Reservation r
            ".$queryOwnerUserIdJoin." WHERE ".$queryOwnerUserId." SUBSTRING(r.fromDate, 1, 4) LIKE :yearString AND r.estate = :estateId ORDER BY r.fromDate ASC");
        }
        $query->setParameter('yearString', $yearString.'%');
        $query->setParameter('estateId', $estateId);
        if(!is_null($ownerUserId)){ $query->setParameter('userId', $ownerUserId); }

        $result = $query->getResult();
        if(count($result) > 0) return $result;

        return false;
    }

    public function getPaidValueForFutureBookings(){
        $nowDate = new \DateTime('now');

        $query = $this->_em->createQuery("
            SELECT sum(r.paid) as paidSum
            FROM UTTReservationBundle:Reservation r
            WHERE
                r.fromDate > :nowDate AND
                (r.status = :statusFullyPaid OR r.status = :statusPartPaid)
        ");
        $query->setParameter('statusFullyPaid', Reservation::STATUS_FULLY_PAID);
        $query->setParameter('statusPartPaid', Reservation::STATUS_PART_PAID);
        $query->setParameter('nowDate', $nowDate);

        $result = $query->getArrayResult();
        if(count($result) > 0){
            if(isset($result[0]) && isset($result[0]['paidSum'])) return $result[0]['paidSum'];
        }

        return 0;
    }

    public function getForSalesByYearArray($ownerUserId = null, $untilToday = false){
        $queryOwnerUserIdJoin = '';
        $queryOwnerUserId = '';
        if(!is_null($ownerUserId)){
            $queryOwnerUserIdJoin = 'LEFT JOIN r.estate e LEFT JOIN e.ownerUsers ou';
            $queryOwnerUserId = 'ou.id = :userId AND';
        }

        $startDate = new \DateTime('now');
        $startDate->setDate($startDate->format('Y'), 1, 1);
        $nowDate = new \DateTime('now');
        $days = $startDate->diff($nowDate)->days;

        $untilTodayWhere = '';
        if($untilToday) $untilTodayWhere = "DATE_DIFF(r.createdAt, CONCAT(SUBSTRING(r.createdAt, 1, 4), '-01-01')) <= :days AND";

        $query = $this->_em->createQuery("
            SELECT
              count(r) as reservationCount,
              sum(r.totalPrice) as priceSum,
              SUBSTRING(r.createdAt, 1, 4) as year,
              sum(DATE_DIFF(r.toDate, r.fromDate)) as nights,
              SUM(SUBSTRING(r.price, LOCATE('adminCharge\":\"', r.price ) + 14, LOCATE('\",\"creditCardCharge', r.price ) - 14 - LOCATE('adminCharge\":\"', r.price ))) as adminChargeSum,
              SUM(SUBSTRING(r.price, LOCATE('bookingProtectCharge\":\"', r.price ) + 23, LOCATE('\",\"charityCharge', r.price ) - 23 - LOCATE('bookingProtectCharge\":\"', r.price ))) as bookingProtectChargeSum,
              sum(r.commissionValue) as commissionValueSum,
              sum(r.totalPrice * r.commission / 100) as commissionValueFull,
              sum(r.paid) as paidSum
            FROM UTTReservationBundle:Reservation r ".$queryOwnerUserIdJoin." WHERE ".$untilTodayWhere." ".$queryOwnerUserId." (r.status = :statusFullyPaid OR r.status = :statusPartPaid) GROUP BY year ORDER BY year DESC");
        $query->setParameter('statusFullyPaid', Reservation::STATUS_FULLY_PAID);
        $query->setParameter('statusPartPaid', Reservation::STATUS_PART_PAID);
        //$query->setParameter('statusReserved', Reservation::STATUS_RESERVED);
        if(!is_null($ownerUserId)){ $query->setParameter('userId', $ownerUserId); }
        if($untilToday){ $query->setParameter('days', $days); }

        $result = $query->getArrayResult();
        if(count($result) > 0) return $result;

        return false;
    }

    public function getForSalesByMonthUntilTodayArray($ownerUserId = null){
        $queryOwnerUserIdJoin = '';
        $queryOwnerUserId = '';
        if(!is_null($ownerUserId)){
            $queryOwnerUserIdJoin = 'LEFT JOIN r.estate e LEFT JOIN e.ownerUsers ou';
            $queryOwnerUserId = 'ou.id = :userId AND';
        }

        $startDate = new \DateTime('now');
        $startDate->setDate($startDate->format('Y'), $startDate->format('m'), 1);
        $nowDate = new \DateTime('now');
        $days = $startDate->diff($nowDate)->days;

        $query = $this->_em->createQuery("
            SELECT
              count(r) as reservationCount,
              sum(r.totalPrice) as priceSum,
              SUBSTRING(r.createdAt, 1, 7) as yearMonth,
              SUBSTRING(r.createdAt, 1, 4) as year,
              SUBSTRING(r.createdAt, 6, 2) as month,
              SUM(SUBSTRING(r.price, LOCATE('adminCharge\":\"', r.price ) + 14, LOCATE('\",\"creditCardCharge', r.price ) - 14 - LOCATE('adminCharge\":\"', r.price ))) as adminChargeSum,
              SUM(SUBSTRING(r.price, LOCATE('bookingProtectCharge\":\"', r.price ) + 23, LOCATE('\",\"charityCharge', r.price ) - 23 - LOCATE('bookingProtectCharge\":\"', r.price ))) as bookingProtectChargeSum,
              sum(r.totalPrice * r.commission / 100) as commissionValueFull,
              sum(DATE_DIFF(r.toDate, r.fromDate)) as nights
            FROM UTTReservationBundle:Reservation r ".$queryOwnerUserIdJoin."
            WHERE
                DATE_DIFF(r.createdAt, CONCAT(SUBSTRING(r.createdAt, 1, 7), '-01-01')) <= :days AND
                ".$queryOwnerUserId."
                (r.status = :statusFullyPaid OR r.status = :statusPartPaid)
            GROUP BY yearMonth ORDER BY yearMonth DESC");
        $query->setParameter('statusFullyPaid', Reservation::STATUS_FULLY_PAID);
        $query->setParameter('statusPartPaid', Reservation::STATUS_PART_PAID);
        //$query->setParameter('statusReserved', Reservation::STATUS_RESERVED);
        if(!is_null($ownerUserId)){ $query->setParameter('userId', $ownerUserId); }
        $query->setParameter('days', $days);

        $result = $query->getArrayResult();
        if(count($result) > 0) return $result;

        return false;
    }

    public function getForSalesByMonthArray(\DateTime $fromDate, $ownerUserId = null, $sort = 'DESC'){
        $queryOwnerUserIdJoin = '';
        $queryOwnerUserId = '';
        if(!is_null($ownerUserId)){
            $queryOwnerUserIdJoin = 'LEFT JOIN r.estate e LEFT JOIN e.ownerUsers ou';
            $queryOwnerUserId = 'ou.id = :userId AND';
        }

        $query = $this->_em->createQuery("
            SELECT
              count(r) as reservationCount,
              sum(r.totalPrice) as priceSum,
              SUBSTRING(r.createdAt, 1, 7) as month,
              sum(DATE_DIFF(r.toDate, r.fromDate)) as nights,
              SUM(SUBSTRING(r.price, LOCATE('adminCharge\":\"', r.price ) + 14, LOCATE('\",\"creditCardCharge', r.price ) - 14 - LOCATE('adminCharge\":\"', r.price ))) as adminChargeSum,
              SUM(SUBSTRING(r.price, LOCATE('bookingProtectCharge\":\"', r.price ) + 23, LOCATE('\",\"charityCharge', r.price ) - 23 - LOCATE('bookingProtectCharge\":\"', r.price ))) as bookingProtectChargeSum,
              sum(r.commissionValue) as commissionValueSum,
              sum(r.totalPrice * r.commission / 100) as commissionValueFull,
              sum(r.paid) as paidSum
            FROM UTTReservationBundle:Reservation r ".$queryOwnerUserIdJoin." WHERE ".$queryOwnerUserId." (r.status = :statusFullyPaid OR r.status = :statusPartPaid) AND r.createdAt >= :fromDate GROUP BY month ORDER BY month ".$sort);
        $query->setParameter('fromDate', $fromDate);
        $query->setParameter('statusFullyPaid', Reservation::STATUS_FULLY_PAID);
        $query->setParameter('statusPartPaid', Reservation::STATUS_PART_PAID);
        //$query->setParameter('statusReserved', Reservation::STATUS_RESERVED);
        if(!is_null($ownerUserId)){ $query->setParameter('userId', $ownerUserId); }

        $result = $query->getArrayResult();
        if(count($result) > 0) return $result;

        return false;
    }

    public function getForSalesByMonthRefundArray(\DateTime $fromDate, $ownerUserId = null, $sort = 'DESC'){
        $queryOwnerUserIdJoin = '';
        $queryOwnerUserId = '';
        if(!is_null($ownerUserId)){
            $queryOwnerUserIdJoin = 'LEFT JOIN r.estate e LEFT JOIN e.ownerUsers ou';
            $queryOwnerUserId = 'ou.id = :userId AND';
        }

        $query = $this->_em->createQuery("
            SELECT
              count(r) as reservationCount,
              sum(r.totalPrice) as priceSum,
              SUBSTRING(r.createdAt, 1, 7) as month,
              sum(DATE_DIFF(r.toDate, r.fromDate)) as nights,
              SUM(SUBSTRING(r.price, LOCATE('adminCharge\":\"', r.price ) + 14, LOCATE('\",\"creditCardCharge', r.price ) - 14 - LOCATE('adminCharge\":\"', r.price ))) as adminChargeSum,
              SUM(SUBSTRING(r.price, LOCATE('bookingProtectCharge\":\"', r.price ) + 23, LOCATE('\",\"charityCharge', r.price ) - 23 - LOCATE('bookingProtectCharge\":\"', r.price ))) as bookingProtectChargeSum,
              sum(r.commissionValue) as commissionValueSum,
              sum(r.totalPrice * r.commission / 100) as commissionValueFull,
              sum(r.paid) as paidSum
            FROM UTTReservationBundle:Reservation r ".$queryOwnerUserIdJoin." WHERE ".$queryOwnerUserId." r.status = :statusRefundDue AND r.createdAt >= :fromDate GROUP BY month ORDER BY month ".$sort);
        $query->setParameter('fromDate', $fromDate);
        $query->setParameter('statusRefundDue', Reservation::STATUS_CANCELLED_REFUND_DUE);
        if(!is_null($ownerUserId)){ $query->setParameter('userId', $ownerUserId); }

        $result = $query->getArrayResult();
        if(count($result) > 0) {
            return $result;
        }

        return false;
    }

    public function getForSalesByDayArray(\DateTime $fromDate, $ownerUserId = null){
        $queryOwnerUserIdJoin = '';
        $queryOwnerUserId = '';
        if(!is_null($ownerUserId)){
            $queryOwnerUserIdJoin = 'LEFT JOIN r.estate e LEFT JOIN e.ownerUsers ou';
            $queryOwnerUserId = 'ou.id = :userId AND';
        }

        $query = $this->_em->createQuery("
            SELECT
              count(r) as reservationCount,
              sum(r.totalPrice) as priceSum,
              SUBSTRING(r.createdAt, 1, 10) as day,
              sum(DATE_DIFF(r.toDate, r.fromDate)) as nights,
              SUM(SUBSTRING(r.price, LOCATE('adminCharge\":\"', r.price ) + 14, LOCATE('\",\"creditCardCharge', r.price ) - 14 - LOCATE('adminCharge\":\"', r.price ))) as adminChargeSum,
              SUM(SUBSTRING(r.price, LOCATE('bookingProtectCharge\":\"', r.price ) + 23, LOCATE('\",\"charityCharge', r.price ) - 23 - LOCATE('bookingProtectCharge\":\"', r.price ))) as bookingProtectChargeSum,
              sum(r.commissionValue) as commissionValueSum,
              sum(r.totalPrice * r.commission / 100) as commissionValueFull,
              sum(r.paid) as paidSum
            FROM UTTReservationBundle:Reservation r ".$queryOwnerUserIdJoin." WHERE ".$queryOwnerUserId." (r.status = :statusFullyPaid OR r.status = :statusPartPaid) AND r.createdAt >= :fromDate GROUP BY day ORDER BY day DESC");
        $query->setParameter('fromDate', $fromDate);
        $query->setParameter('statusFullyPaid', Reservation::STATUS_FULLY_PAID);
        $query->setParameter('statusPartPaid', Reservation::STATUS_PART_PAID);
        //$query->setParameter('statusReserved', Reservation::STATUS_RESERVED);
        if(!is_null($ownerUserId)){ $query->setParameter('userId', $ownerUserId); }

        $result = $query->getArrayResult();
        if(count($result) > 0) return $result;

        return false;
    }

    public function getForSalesByDayRefundArray(\DateTime $fromDate, $ownerUserId = null){
        $queryOwnerUserIdJoin = '';
        $queryOwnerUserId = '';
        if(!is_null($ownerUserId)){
            $queryOwnerUserIdJoin = 'LEFT JOIN r.estate e LEFT JOIN e.ownerUsers ou';
            $queryOwnerUserId = 'ou.id = :userId AND';
        }

        $query = $this->_em->createQuery("
            SELECT
              count(r) as reservationCount,
              sum(r.totalPrice) as priceSum,
              SUBSTRING(r.createdAt, 1, 10) as day,
              sum(DATE_DIFF(r.toDate, r.fromDate)) as nights,
              SUM(SUBSTRING(r.price, LOCATE('adminCharge\":\"', r.price ) + 14, LOCATE('\",\"creditCardCharge', r.price ) - 14 - LOCATE('adminCharge\":\"', r.price ))) as adminChargeSum,
              SUM(SUBSTRING(r.price, LOCATE('bookingProtectCharge\":\"', r.price ) + 23, LOCATE('\",\"charityCharge', r.price ) - 23 - LOCATE('bookingProtectCharge\":\"', r.price ))) as bookingProtectChargeSum,
              sum(r.commissionValue) as commissionValueSum,
              sum(r.totalPrice * r.commission / 100) as commissionValueFull,
              sum(r.paid) as paidSum
            FROM UTTReservationBundle:Reservation r ".$queryOwnerUserIdJoin." WHERE ".$queryOwnerUserId." r.status = :statusRefundDue AND r.createdAt >= :fromDate GROUP BY day ORDER BY day DESC");
        $query->setParameter('fromDate', $fromDate);
        $query->setParameter('statusRefundDue', Reservation::STATUS_CANCELLED_REFUND_DUE);
        if(!is_null($ownerUserId)){ $query->setParameter('userId', $ownerUserId); }

        $result = $query->getArrayResult();
        if(count($result) > 0) return $result;

        return false;
    }

    public function getForCleaningRota(Estate $estate, $daysFromNow = 105){
        $maxFromDate = new \DateTime('now');
        $maxFromDate->modify('+ '.$daysFromNow.' days');

        $query = $this->_em->createQuery("
            SELECT r
            FROM UTTReservationBundle:Reservation r WHERE
              r.estate = :estateId AND
              r.toDate > :minFromDate AND
              r.fromDate < :maxFromDate AND
              r.status != :statusCancelled AND
              r.status != :statusCancelledRefundDue AND
              r.status != :statusCancelledNoRefund
            ORDER BY r.fromDate ASC
            ");
        $query->setParameter('estateId', $estate->getId());
        $query->setParameter('minFromDate', new \DateTime('now'));
        $query->setParameter('maxFromDate', $maxFromDate);
        $query->setParameter('statusCancelled', Reservation::STATUS_CANCELLED);
        $query->setParameter('statusCancelledRefundDue', Reservation::STATUS_CANCELLED_REFUND_DUE);
        $query->setParameter('statusCancelledNoRefund', Reservation::STATUS_CANCELLED_NO_REFUND);

        $result = $query->getResult();
        if(count($result) > 0) return $result;

        return false;
    }

    public function getForNoPaymentCancellationReinstate(){
        $query = $this->_em->createQuery("
            SELECT r
            FROM UTTReservationBundle:Reservation r
            WHERE
                r.status IN (:statuses) AND
                r.noPaymentCancellationReinstateDate IS NOT NULL AND
                r.noPaymentCancellationReinstateDate <= :yesterdayDate AND
                r.isNoPaymentCancellationReinstateSent != TRUE
            ORDER BY r.id DESC");
        $query->setParameter('statuses', array(
            Reservation::STATUS_CANCELLED
        ));
        $nowDate = new \DateTime('now');
        $nowDate->modify('-1 day');

        $query->setParameter('yesterdayDate', $nowDate);

        $result = $query->getResult();
        if(count($result) > 0) return $result;

        return false;
    }

    public function getFor1HourNoPaymentCancellationReinstate(){
        $query = $this->_em->createQuery("
            SELECT r
            FROM UTTReservationBundle:Reservation r
            WHERE
                r.status IN (:statuses) AND
                r.noPaymentCancellationReinstateDate IS NOT NULL AND
                r.noPaymentCancellationReinstateDate <= :yesterdayDate AND
                r.is1hourNoPaymentCancellationReinstateSent != TRUE
            ORDER BY r.id DESC");
        $query->setParameter('statuses', array(
            Reservation::STATUS_CANCELLED
        ));
        $nowDate = new \DateTime('now');
        $nowDate->modify('-1 hour');

        $query->setParameter('yesterdayDate', $nowDate);

        $result = $query->getResult();
        if(count($result) > 0) return $result;

        return false;
    }

    public function getNotPaidReservations(){
        $query = $this->_em->createQuery("
            SELECT r
            FROM UTTReservationBundle:Reservation r
            WHERE
                r.status IN (:statuses) AND r.createdAt >= :minCreatedAtDate AND r.referredFlag IS NULL AND
                (r.isHoldingForPayment != TRUE OR (r.isHoldingForPayment = TRUE AND r.holdingForPaymentUntilDate < :nowDate))
            ORDER BY r.id DESC");
        $query->setParameter('statuses', array(
            Reservation::STATUS_RESERVED,
            Reservation::STATUS_PART_PAID
        ));
        $query->setParameter('minCreatedAtDate', new \Datetime('2013-01-01'));
        $query->setParameter('nowDate', new \DateTime('now'));

        $result = $query->getResult();
        if(count($result) > 0) return $result;

        return false;
    }

    public function getReservationsWithNoCommissionValue($limit = 10){
        $query = $this->_em->createQuery("
            SELECT r
            FROM UTTReservationBundle:Reservation r
            WHERE
                r.commissionValue IS NULL
            ORDER BY r.id DESC");
        $query->setMaxResults($limit);

        $result = $query->getResult();
        if(count($result) > 0) return $result;

        return false;
    }

    public function getReservationsMadeInNewSystem(){
        $query = $this->_em->createQuery("
            SELECT r
            FROM UTTReservationBundle:Reservation r
            WHERE
                r.createdAt >= :minCreatedAtDate
            ORDER BY r.id DESC");
        $query->setParameter('minCreatedAtDate', new \Datetime('2014-10-16'));

        $result = $query->getResult();
        if(count($result) > 0) return $result;

        return false;
    }

    public function getReservationsMadeForEstateIn2018($estateId){
        $minCreatedAtDate = new \Datetime('2018-11-01');

        $query = $this->_em->createQuery("
            SELECT r
            FROM UTTReservationBundle:Reservation r
            WHERE
                r.estate = :estateId AND
                r.fromDate >= :minCreatedAtDate
            ORDER BY r.id DESC");
        $query->setParameter('minCreatedAtDate', $minCreatedAtDate);
        $query->setParameter('estateId', $estateId);

        $result = $query->getResult();
        if(count($result) > 0) return $result;

        return false;
    }

    public function getReservationsWithBrokenStatus(){
        $query = $this->_em->createQuery("
            SELECT r
            FROM UTTReservationBundle:Reservation r
            WHERE
                r.createdAt >= :minCreatedAtDate AND
                r.paid = r.totalPrice AND
                r.paid > 0 AND
                (r.status = :statusPartPaid OR r.status = :statusReserved)
            ORDER BY r.id DESC");
        $query->setParameter('minCreatedAtDate', new \Datetime('2014-10-16'));
        $query->setParameter('statusPartPaid', Reservation::STATUS_PART_PAID);
        $query->setParameter('statusReserved', Reservation::STATUS_RESERVED);

        $result = $query->getResult();
        if(count($result) > 0) return $result;

        return false;
    }

    public function getReservationForThankYouReminder($limit = 3){
        $nowDate = new \DateTime('now');

        $query = $this->_em->createQuery("
            SELECT r
            FROM UTTReservationBundle:Reservation r
            WHERE
                r.createdAt >= :minCreatedAtDate AND
                r.status = :statusFullyPaid AND
                r.toDate < :maxToDate AND
                r.isThankYouSent != 1
            ORDER BY r.id DESC");
        $query->setParameter('minCreatedAtDate', new \Datetime('2014-10-16'));
        $query->setParameter('statusFullyPaid', Reservation::STATUS_FULLY_PAID);
        $query->setParameter('maxToDate', $nowDate);

        if((integer) $limit){ $query->setMaxResults((integer) $limit); }

        $result = $query->getResult();
        if(count($result) > 0) return $result;

        return false;
    }

    public function getReservationsForAirbnbExportSync(Estate $estate){
        $nowDate = new \DateTime('now');

        $query = $this->_em->createQuery("
            SELECT r
            FROM UTTReservationBundle:Reservation r
            WHERE
                r.status != :statusCancelled AND
                r.status != :statusCancelledRefundDue AND
                r.status != :statusBlockedMultiproperty AND
                r.status != :statusCancelledNoRefund AND
                r.estate = :estate AND
                r.toDate > :nowDate
            ORDER BY r.fromDate ASC");
        $query->setParameters(array(
            'statusCancelled' => Reservation::STATUS_CANCELLED,
            'statusCancelledRefundDue' => Reservation::STATUS_CANCELLED_REFUND_DUE,
            'statusBlockedMultiproperty' => Reservation::STATUS_BLOCKED_MULTUPROPERTY,
            'statusCancelledNoRefund' => Reservation::STATUS_CANCELLED_NO_REFUND,
            'estate' => $estate->getId(),
            'nowDate' => $nowDate
        ));

        $result = $query->getResult();
        if(count($result) > 0) return $result;

        return false;
    }

    public function findAirbnbReservation($key){
        $query = $this->_em->createQuery("
            SELECT r
            FROM UTTReservationBundle:Reservation r
            WHERE
                r.note like :key
            ORDER BY r.id DESC");
        $query->setParameter('key', '%'.$key.'%');

        $result = $query->getResult();
        if(count($result) > 0) return $result[0];

        return false;
    }

    public function findAirbnbReservationForEstate($key, Estate $estate){
        $query = $this->_em->createQuery("
            SELECT r
            FROM UTTReservationBundle:Reservation r
            WHERE
                r.note like :key AND
                r.estate = :estate
            ORDER BY r.id DESC");
        $query->setParameter('key', '%'.$key.'%');
        $query->setParameter('estate', $estate->getId());

        $result = $query->getResult();
        if(count($result) > 0) return $result[0];

        return false;
    }

    public function findAirbnbReservations(Estate $estate){
        $query = $this->_em->createQuery("
            SELECT r
            FROM UTTReservationBundle:Reservation r
            WHERE
                r.status = :statusAirbnb AND
                r.estate = :estate
            ORDER BY r.fromDate ASC");
        $query->setParameters(array(
            'statusAirbnb' => Reservation::STATUS_BLOCKED_AIRBNB,
            'estate' => $estate->getId()
        ));

        $result = $query->getResult();
        if(count($result) > 0) return $result;

        return false;
    }

    public function getFlexibleActiveReservationsBetweenDates(Estate $estate, \Datetime $fromDate, \Datetime $toDate){
        $query = $this->_em->createQuery("
            SELECT r
            FROM UTTReservationBundle:Reservation r
            WHERE
                r.status != :statusCancelled AND
                r.status != :statusCancelledRefundDue AND
                r.status != :statusCancelledNoRefund AND
                r.estate = :estate AND
                (
                    (r.fromDate >= :fromDate AND r.fromDate <= :toDate) OR
                    (r.toDate >= :fromDate AND r.toDate <= :toDate) OR
                    (r.fromDate <= :fromDate AND r.fromDate <= :toDate AND r.toDate >= :fromDate AND r.toDate >= :toDate)
                )
            ORDER BY r.fromDate ASC");
        $query->setParameters(array(
            'statusCancelled' => Reservation::STATUS_CANCELLED,
            'statusCancelledRefundDue' => Reservation::STATUS_CANCELLED_REFUND_DUE,
            'statusCancelledNoRefund' => Reservation::STATUS_CANCELLED_NO_REFUND,
            'estate' => $estate->getId(),
            'fromDate' => $fromDate,
            'toDate' => $toDate
        ));

        $result = $query->getResult();
        if(count($result) > 0) return $result;

        return false;
    }

    public function getOneLatestByEstate(Estate $estate){
        $query = $this->_em->createQuery("
            SELECT r
            FROM UTTReservationBundle:Reservation r
            WHERE
                r.estate = :estateId
            ORDER BY r.id DESC");
        $query->setParameters(array(
            'estateId' => $estate->getId()
        ));

        $result = $query->getResult();
        if(count($result) > 0) return $result[0];

        return false;
    }

    public function getLatestByUserId($userId, $limit = 3){
        $query = $this->_em->createQuery("
            SELECT r
            FROM UTTReservationBundle:Reservation r
            WHERE
                r.user = :userId
            ORDER BY r.id DESC");
        $query->setParameters(array(
            'userId' => $userId
        ));

        if((integer) $limit){ $query->setMaxResults((integer) $limit); }

        $result = $query->getResult();
        if(count($result) > 0) return $result;

        return false;
    }

    public function getLimitedByPage($page = 1){
        $query = $this->_em->createQuery("SELECT r FROM UTTReservationBundle:Reservation r ORDER BY r.id ASC");

        $query->setMaxResults(1000);
        $query->setFirstResult(($page-1) * 1000);

        $result = $query->getResult();
        if(count($result) > 0) return $result;

        return false;
    }

    public function findForActivityLog(){
        $query = $this->_em->createQuery("SELECT r FROM UTTReservationBundle:Reservation r WHERE r.sessionId IS NOT NULL ORDER BY r.id DESC");

        $result = $query->getResult();
        if(count($result) > 0) return $result;

        return false;
    }

    /**
     * get commision value
     * @param string $dateString can be in format "Y-m"
     * @param int $estateId
     * @return float|int|mixed
     */
    public function getCommissionValueForDateAndEstateId($dateString, $estateId)
    {
        $commissionValue = 0;
        $reservations = $this->getReservationsByDateStringAndEstateId($dateString, $estateId);
        /** @var Reservation $reservation */
        foreach($reservations as $reservation) {
            if($reservation->isNormalBooking()){
                $commissionValue += $reservation->calculateCommissionValue();
            }
        }

        return $commissionValue;
    }

    /**
     * @param string $dateString
     * @param int $estateId
     * @return bool
     */
    public function owesMoneyToUTTbyDateStringAndEstateId($dateString, $estateId)
    {
        return !empty($this->getCommissionValueForDateAndEstateId($dateString, $estateId));
    }

    /**
     * @param \UTT\ReservationBundle\Entity\Reservation $reservation
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function persist(Reservation $reservation)
    {
        $this->_em->persist($reservation);
        $this->_em->flush();
    }
}
