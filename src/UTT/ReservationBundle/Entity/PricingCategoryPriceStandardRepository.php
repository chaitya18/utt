<?php

namespace UTT\ReservationBundle\Entity;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query;

class PricingCategoryPriceStandardRepository extends EntityRepository
{
    public function getByPricingCategoryOrderByYear($pricingCategory, $year = 2018){
        $query = $this->_em->createQuery("
            SELECT pcp FROM UTTReservationBundle:PricingCategoryPriceStandard pcp WHERE pcp.pricingCategory = :pricingCategory and pcp.year >= :year ORDER BY pcp.year ASC, pcp.type ASC");
        $query->setParameter('pricingCategory', $pricingCategory);
        $query->setParameter('year', $year);

        $result = $query->getResult();
        if(count($result) > 0) return $result;

        return false;
    }

    /**
     * Return lowest price for pricing category and year
     * @param $pricingCategory
     * @param $startYear
     * @return int
     */
    public function getLowestPriceForCategoryStartingAtYear($pricingCategory, $startYear)
    {
        $query = $this->_em->createQuery("
            select MIN(pcp.price7Nights) from UTTReservationBundle:PricingCategoryPriceStandard pcp 
            WHERE pcp.pricingCategory = :pricingCategory and pcp.year >= :startYear and pcp.type = :type");
        $query->setParameter('startYear', $startYear);
        $query->setParameter('type', PricingCategoryPriceStandard::TYPE_MINIMUM);
        $query->setParameter('pricingCategory', $pricingCategory);

        return $query->getResult(Query::HYDRATE_SINGLE_SCALAR);
    }
}
