<?php

namespace UTT\ReservationBundle\Entity;

use Doctrine\ORM\EntityRepository;
use UTT\EstateBundle\Entity\Estate;

class OfferRepository extends EntityRepository
{
    public function findAvailableAtDateOffers(\Datetime $date, $limit = 10, $lateAvailability = true, $notLateAvailability = true, $automaticSpecialOfferMaxPrice = 0){
        $query = $this->_em->createQuery("
            SELECT o
            FROM UTTReservationBundle:Offer o
            LEFT JOIN o.estates e
            WHERE
                o.validTo >= :dateParam AND
                o.type IN (:offerTypes) AND
                (
                  (o.type = :offerTypeAutomaticSpecialOffer AND o.value <= :automaticSpecialOfferMaxPrice) OR
                  (o.type != :offerTypeAutomaticSpecialOffer)
                )
            GROUP BY e.id
            ORDER BY o.value ASC");

        $offerTypes = array();
        if($notLateAvailability){
            $offerTypes[] = Offer::TYPE_GENERAL;
            $offerTypes[] = Offer::TYPE_SPECIFIC;
            $offerTypes[] = Offer::TYPE_ADVERTISING;
            $offerTypes[] = Offer::TYPE_AUTOMATIC_SPECIAL_OFFER;
        }
        if($lateAvailability){
            $offerTypes[] = Offer::TYPE_LATE_AVAILABILITY;
        }
        $query->setParameters(array(
            'dateParam' => $date,
            'offerTypes' => $offerTypes,
            'offerTypeAutomaticSpecialOffer' => Offer::TYPE_AUTOMATIC_SPECIAL_OFFER,
            'automaticSpecialOfferMaxPrice' => $automaticSpecialOfferMaxPrice
        ));

        if((integer) $limit){ $query->setMaxResults((integer) $limit); }

        $result = $query->getResult();
        if(count($result) > 0) return $result;

        return false;
    }

    public function findAvailableForEstateBetweenDates(Estate $estate, \Datetime $fromDate, \Datetime $toDate){
        $query = $this->_em->createQuery("
            SELECT o
            FROM UTTReservationBundle:Offer o
            LEFT JOIN o.estates e
            WHERE
                o.validFrom >= :fromDate AND o.validFrom <= :toDate AND
                o.validTo >= :fromDate AND o.validTo <= :toDate AND
                e.id IN (:estates) AND
                o.type != :type
            ORDER BY o.value DESC");
        $query->setParameters(array(
            'fromDate' => $fromDate,
            'toDate' => $toDate,
            'estates' => array($estate->getId()),
            'type' => Offer::TYPE_ADVERTISING
        ));

        $result = $query->getResult();
        if(count($result) > 0) return $result;

        return false;
    }

    public function findOneByEstateAndDates(Estate $estate, \Datetime $fromDate, \Datetime $toDate, $forSearchEngine = false){
        $queryWhere = 'o.validFrom = :fromDate AND o.validTo = :toDate';
        $queryOrderBy = '';
        if($forSearchEngine){
            $queryWhere = '((o.validFrom = :fromDate AND o.validTo >= :toDate) OR (o.validFrom <= :fromDate AND o.validTo = :toDate))';
            $queryOrderBy = 'o.validTo ASC,';
        }

        $query = $this->_em->createQuery("
            SELECT o
            FROM UTTReservationBundle:Offer o
            LEFT JOIN o.estates e
            WHERE
                ".$queryWhere." AND e.id IN (:estates) AND o.type != :type
            ORDER BY
                ".$queryOrderBy." o.value ASC");
        $query->setParameters(array(
            'fromDate' => $fromDate,
            'toDate' => $toDate,
            'estates' => array($estate->getId()),
            'type' => Offer::TYPE_ADVERTISING
        ));
        $query->setMaxResults(1);

        $result = $query->getResult();
        if(count($result) > 0) return $result[0];

        return false;
    }

    public function findLateAvailabilityOffersForEstate(Estate $estate){
        $query = $this->_em->createQuery("
            SELECT o
            FROM UTTReservationBundle:Offer o
            LEFT JOIN o.estates e
            WHERE o.type = :type AND e.id IN (:estates)
            ");
        $query->setParameters(array(
            'estates' => array($estate->getId()),
            'type' => Offer::TYPE_LATE_AVAILABILITY
        ));

        $result = $query->getResult();
        if(count($result) > 0) return $result;

        return false;
    }

    public function findAutomaticSpecialOffersForEstate(Estate $estate){
        $query = $this->_em->createQuery("
            SELECT o
            FROM UTTReservationBundle:Offer o
            LEFT JOIN o.estates e
            WHERE o.type = :type AND e.id IN (:estates)
            ");
        $query->setParameters(array(
            'estates' => array($estate->getId()),
            'type' => Offer::TYPE_AUTOMATIC_SPECIAL_OFFER
        ));

        $result = $query->getResult();
        if(count($result) > 0) return $result;

        return false;
    }

    public function findOneLowestPriceWeekOfferForEstate(Estate $estate){
        $query = $this->_em->createQuery("
            SELECT o
            FROM UTTReservationBundle:Offer o
            LEFT JOIN o.estates e
            WHERE
                e.id IN (:estates) AND DATE_DIFF(o.validTo, o.validFrom) = 7
            ORDER BY o.value ASC");
        $query->setParameters(array(
            'estates' => array($estate->getId())
        ));
        $query->setMaxResults(1);

        $result = $query->getResult();
        if(count($result) > 0) return $result[0];

        return false;
    }

    public function findOneHighestPriceWeekOfferForEstate(Estate $estate){
        $query = $this->_em->createQuery("
            SELECT o
            FROM UTTReservationBundle:Offer o
            LEFT JOIN o.estates e
            WHERE
                e.id IN (:estates) AND DATE_DIFF(o.validTo, o.validFrom) = 7
            ORDER BY o.value DESC");
        $query->setParameters(array(
            'estates' => array($estate->getId())
        ));
        $query->setMaxResults(1);

        $result = $query->getResult();
        if(count($result) > 0) return $result[0];

        return false;
    }


}
