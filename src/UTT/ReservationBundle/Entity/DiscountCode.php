<?php

namespace UTT\ReservationBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * DiscountCode
 *
 * @ORM\Table(name="discount_code")
 * @ORM\Entity(repositoryClass="UTT\ReservationBundle\Entity\DiscountCodeRepository")
 * @UniqueEntity(fields="code", message="This code is already in use")
 */
class DiscountCode
{
    const CODE_TYPE_ONE_OFF_PERCENTAGE = 1;
    const CODE_TYPE_ONE_OFF_FIXED_AMOUNT_DISCOUNT = 2;
//    const CODE_TYPE_PRE_SOLD_VOUCHER_FIXED_AMOUNT = 3;
    const CODE_TYPE_PERSISTENT_PERCENTAGE = 4;
    const CODE_TYPE_PERSISTENT_FIXED_AMOUNT = 5;
    const CODE_TYPE_PAYMENT_VOUCHER = 6;

    static public function getAllowedCodeTypesStatic(){
        $array = array(
            self::CODE_TYPE_ONE_OFF_PERCENTAGE => 'One off - Percentage',
            self::CODE_TYPE_ONE_OFF_FIXED_AMOUNT_DISCOUNT => 'One off - Fixed Amount Discount',
//            self::CODE_TYPE_PRE_SOLD_VOUCHER_FIXED_AMOUNT => 'Pre Sold Voucher - Fixed Amount',
            self::CODE_TYPE_PERSISTENT_PERCENTAGE => 'Persistent - Percentage',
            self::CODE_TYPE_PERSISTENT_FIXED_AMOUNT => 'Persistent - Fixed Amount',
            self::CODE_TYPE_PAYMENT_VOUCHER => 'Payment voucher',
        );
        return $array;
    }

    public function getAllowedCodeTypes(){
        return self::getAllowedCodeTypesStatic();
    }

    public function getCodeTypeName($codeTypeId = null){
        $array = $this->getAllowedCodeTypes();
        if(is_null($codeTypeId)){
            if($this->getCodeType()) {
                return array_key_exists($this->getCodeType(), $array) ? $array[$this->getCodeType()] : false;
            }else{
                return false;
            }
        }
        return array_key_exists($codeTypeId, $array) ? $array[$codeTypeId] : false;
    }
########################################################################################################################

    public function __construct() {
        $this->estates = new ArrayCollection();
        $this->setCreatedAt(new \DateTime('now'));
    }

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(name="code", type="string", length=255, nullable=true)
     */
    protected $code;

    /**
     * @ORM\Column(name="code_type", type="integer", nullable=true)
     */
    protected $codeType;

    /**
     * @ORM\Column(name="value", type="decimal", scale=2, nullable=true)
     */
    protected $value;

    /**
     * @ORM\Column(name="expires_at", type="date", nullable=true)
     */
    protected $expiresAt;

    /**
     * @ORM\Column(name="is_active", type="boolean")
     */
    protected $isActive = true;

    /**
     * @ORM\ManyToMany(targetEntity="UTT\EstateBundle\Entity\Estate")
     * @ORM\JoinTable(name="discount_codes_estates",
     *      joinColumns={@ORM\JoinColumn(name="discount_code_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="estate_id", referencedColumnName="id")}
     *      )
     */
    protected $estates;

    /**
     * @ORM\Column(name="is_all_estates", type="boolean")
     */
    protected $isAllEstates = false;

    /**
     * @ORM\Column(name="holiday_must_start_at", type="date", nullable=true)
     */
    protected $holidayMustStartAt;

    /**
     * @ORM\Column(name="holiday_must_start_by", type="date", nullable=true)
     */
    protected $holidayMustStartBy;

    /**
     * @ORM\Column(name="emails", type="text", nullable=true)
     */
    protected $emails;

    /**
     * @ORM\Column(name="is_redeemed", type="boolean")
     */
    protected $isRedeemed = false;

    /**
     * @ORM\Column(name="created_at", type="datetime")
     */
    protected $createdAt;

    /**
     * @ORM\ManyToOne(targetEntity="UTT\UserBundle\Entity\User")
     * @ORM\JoinColumn(name="created_by_user_id", referencedColumnName="id", nullable=true, onDelete="SET NULL")
     */
    protected $createdByUser;

    /**
     * @ORM\Column(name="reason", type="text", nullable=true)
     */
    protected $reason;

    public function __toString(){
        if($this->getCode())
            return $this->getCode();
        return '';
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set code
     *
     * @param string $code
     * @return DiscountCode
     */
    public function setCode($code)
    {
        $this->code = $code;

        return $this;
    }

    /**
     * Get code
     *
     * @return string 
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * Set codeType
     *
     * @param integer $codeType
     * @return DiscountCode
     */
    public function setCodeType($codeType)
    {
        $this->codeType = $codeType;

        return $this;
    }

    /**
     * Get codeType
     *
     * @return integer 
     */
    public function getCodeType()
    {
        return $this->codeType;
    }

    /**
     * Set emails
     *
     * @param string $emails
     * @return DiscountCode
     */
    public function setEmails($emails)
    {
        $this->emails = $emails;

        return $this;
    }

    /**
     * Get emails
     *
     * @return string 
     */
    public function getEmails()
    {
        return $this->emails;
    }

    /**
     * Add estates
     *
     * @param \UTT\EstateBundle\Entity\Estate $estates
     * @return DiscountCode
     */
    public function addEstate(\UTT\EstateBundle\Entity\Estate $estates)
    {
        $this->estates[] = $estates;

        return $this;
    }

    /**
     * Remove estates
     *
     * @param \UTT\EstateBundle\Entity\Estate $estates
     */
    public function removeEstate(\UTT\EstateBundle\Entity\Estate $estates)
    {
        $this->estates->removeElement($estates);
    }

    /**
     * Get estates
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getEstates()
    {
        return $this->estates;
    }

    /**
     * Set isActive
     *
     * @param boolean $isActive
     * @return DiscountCode
     */
    public function setIsActive($isActive)
    {
        $this->isActive = $isActive;

        return $this;
    }

    /**
     * Get isActive
     *
     * @return boolean 
     */
    public function getIsActive()
    {
        return $this->isActive;
    }

    /**
     * Set expiresAt
     *
     * @param \DateTime $expiresAt
     * @return DiscountCode
     */
    public function setExpiresAt($expiresAt)
    {
        $this->expiresAt = $expiresAt;

        return $this;
    }

    /**
     * Get expiresAt
     *
     * @return \DateTime 
     */
    public function getExpiresAt()
    {
        return $this->expiresAt;
    }

    /**
     * Set value
     *
     * @param string $value
     * @return DiscountCode
     */
    public function setValue($value)
    {
        $this->value = $value;

        return $this;
    }

    /**
     * Get value
     *
     * @return string 
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * Set holidayMustStartBy
     *
     * @param \DateTime $holidayMustStartBy
     * @return DiscountCode
     */
    public function setHolidayMustStartBy($holidayMustStartBy)
    {
        $this->holidayMustStartBy = $holidayMustStartBy;

        return $this;
    }

    /**
     * Get holidayMustStartBy
     *
     * @return \DateTime 
     */
    public function getHolidayMustStartBy()
    {
        return $this->holidayMustStartBy;
    }

    /**
     * Set isAllEstates
     *
     * @param boolean $isAllEstates
     * @return DiscountCode
     */
    public function setIsAllEstates($isAllEstates)
    {
        $this->isAllEstates = $isAllEstates;

        return $this;
    }

    /**
     * Get isAllEstates
     *
     * @return boolean 
     */
    public function getIsAllEstates()
    {
        return $this->isAllEstates;
    }

    /**
     * Set isRedeemed
     *
     * @param boolean $isRedeemed
     * @return DiscountCode
     */
    public function setIsRedeemed($isRedeemed)
    {
        $this->isRedeemed = $isRedeemed;

        return $this;
    }

    /**
     * Get isRedeemed
     *
     * @return boolean 
     */
    public function getIsRedeemed()
    {
        return $this->isRedeemed;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return DiscountCode
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime 
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set reason
     *
     * @param string $reason
     * @return DiscountCode
     */
    public function setReason($reason)
    {
        $this->reason = $reason;

        return $this;
    }

    /**
     * Get reason
     *
     * @return string 
     */
    public function getReason()
    {
        return $this->reason;
    }

    /**
     * Set createdByUser
     *
     * @param \UTT\UserBundle\Entity\User $createdByUser
     * @return DiscountCode
     */
    public function setCreatedByUser(\UTT\UserBundle\Entity\User $createdByUser = null)
    {
        $this->createdByUser = $createdByUser;

        return $this;
    }

    /**
     * Get createdByUser
     *
     * @return \UTT\UserBundle\Entity\User 
     */
    public function getCreatedByUser()
    {
        return $this->createdByUser;
    }

    /**
     * Set holidayMustStartAt
     *
     * @param \DateTime $holidayMustStartAt
     * @return DiscountCode
     */
    public function setHolidayMustStartAt($holidayMustStartAt)
    {
        $this->holidayMustStartAt = $holidayMustStartAt;

        return $this;
    }

    /**
     * Get holidayMustStartAt
     *
     * @return \DateTime 
     */
    public function getHolidayMustStartAt()
    {
        return $this->holidayMustStartAt;
    }
}
