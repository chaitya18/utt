<?php

namespace UTT\ReservationBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * CharityDonation
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="UTT\ReservationBundle\Entity\CharityDonationRepository")
 */
class CharityDonation
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(name="donated_at", type="date")
     */
    protected $donatedAt;

    /**
     * @ORM\Column(name="paid", type="decimal", scale=2, nullable=true)
     */
    protected $paid;

    /**
     * @ORM\Column(name="url", type="string", length=255, nullable=true)
     */
    protected $url;

    /**
     * @ORM\Column(name="description", type="text", nullable=true)
     */
    protected $description;

    /**
     * @ORM\Column(name="image", type="string", length=255, nullable=true)
     */
    protected $image;
    public $file;

    #--------------------------------------------------- ENTITY METHODS ---------------------------------------------------#

    public function getUploadDir(){
        return 'uploads/charityDomainImages';
    }

    protected function getUploadRootDir($basepath = null){
        return $basepath.$this->getUploadDir();
    }

    public function uploadImage($basepath = null){
        if (null === $this->file) {
            return;
        }

        if (null === $basepath) {
            return;
        }
        $name =  uniqid().substr($this->file->getClientOriginalName(),-4);
        $this->file->move($this->getUploadRootDir($basepath), $name);
        $this->setImage($name);
        $this->file = null;
    }

#################################################### ENTITY METHODS ####################################################

    public function __toString(){
        if($this->getDonatedAt()){
            return $this->getDonatedAt()->format('d-M-y');
        }else{
            return '';
        }
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set donatedAt
     *
     * @param \DateTime $donatedAt
     * @return CharityDonation
     */
    public function setDonatedAt($donatedAt)
    {
        $this->donatedAt = $donatedAt;

        return $this;
    }

    /**
     * Get donatedAt
     *
     * @return \DateTime 
     */
    public function getDonatedAt()
    {
        return $this->donatedAt;
    }

    /**
     * Set paid
     *
     * @param string $paid
     * @return CharityDonation
     */
    public function setPaid($paid)
    {
        $this->paid = $paid;

        return $this;
    }

    /**
     * Get paid
     *
     * @return string 
     */
    public function getPaid()
    {
        return $this->paid;
    }

    /**
     * Set url
     *
     * @param string $url
     * @return CharityDonation
     */
    public function setUrl($url)
    {
        $this->url = $url;

        return $this;
    }

    /**
     * Get url
     *
     * @return string 
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return CharityDonation
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set image
     *
     * @param string $image
     * @return CharityDonation
     */
    public function setImage($image)
    {
        $this->image = $image;

        return $this;
    }

    /**
     * Get image
     *
     * @return string 
     */
    public function getImage()
    {
        return $this->image;
    }
}
