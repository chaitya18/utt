<?php

namespace UTT\ReservationBundle\Entity;

use Doctrine\ORM\EntityRepository;

class PricingCategoryRepository extends EntityRepository
{
    public function getForPricingCategoriesPage(){
        $query = $this->_em->createQuery("
            SELECT pc, pcs, pcf
            FROM UTTReservationBundle:PricingCategory pc
            LEFT JOIN pc.pricesStandard pcs
            LEFT JOIN pc.pricesFlexible pcf

            ORDER BY pc.id ASC");

        $result = $query->getResult();
        if(count($result) > 0) return $result;

        return false;
    }
}
