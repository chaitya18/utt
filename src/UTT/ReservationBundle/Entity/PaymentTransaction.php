<?php

namespace UTT\ReservationBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * PaymentTransaction
 *
 * @ORM\Table(name="payment_transaction")
 * @ORM\Entity(repositoryClass="UTT\ReservationBundle\Entity\PaymentTransactionRepository")
 */
class PaymentTransaction
{
    public function __construct(){
        $this->setCreatedAt(new \DateTime('now'));
    }

    const STATUS_PREPARED = 1;
    const STATUS_SUCCESS = 2;
    const STATUS_NOT_SUCCESS = 3;

    const PAYMENT_AUTH_SUCCESS = 'AUTHORISED';
    const PAYMENT_AUTH_CANCELLED = 'CANCELLED';

    const PAYMENT_PROVIDER_WORLDPAY = 2;
    const PAYMENT_PROVIDER_SMARTPAY = 1;


    public function getAllowedStatuses(){
        $array = array(
            self::STATUS_PREPARED => 'Prepared',
            self::STATUS_SUCCESS => 'Success',
            self::STATUS_NOT_SUCCESS => 'Not success',
        );
        return $array;
    }

    public function getStatusName($statusId = null){
        $array = $this->getAllowedStatuses();
        if(is_null($statusId)){
            if($this->getStatus()) {
                return array_key_exists($this->getStatus(), $array) ? $array[$this->getStatus()] : false;
            }else{
                return false;
            }
        }
        return array_key_exists($statusId, $array) ? $array[$statusId] : false;
    }

    const TYPE_DEBIT_CARD = 1;
    const TYPE_CREDIT_CARD = 2;

    public function getAllowedTypes(){
        $array = array(
            self::TYPE_DEBIT_CARD => 'Debit card',
            self::TYPE_CREDIT_CARD => 'Credit card',
        );
        return $array;
    }

    public function getTypeName($typeId = null){
        $array = $this->getAllowedTypes();
        if(is_null($typeId)){
            if($this->getType()) {
                return array_key_exists($this->getType(), $array) ? $array[$this->getType()] : false;
            }else{
                return false;
            }
        }
        return array_key_exists($typeId, $array) ? $array[$typeId] : false;
    }

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(name="status", type="integer")
     */
    protected $status;

    /**
     * @ORM\Column(name="code", type="string", length=255, nullable=true)
     */
    protected $code;

    /**
     * @ORM\ManyToOne(targetEntity="Reservation", inversedBy="paymentTransactions")
     * @ORM\JoinColumn(name="reservation_id", referencedColumnName="id", nullable=true, onDelete="SET NULL")
     */
    protected $reservation;

    /**
     * @ORM\Column(name="created_at", type="datetime")
     */
    protected $createdAt;

    /**
     * @ORM\Column(name="amount", type="decimal", scale=2, nullable=true)
     */
    protected $amount;

    /**
     * @ORM\Column(name="amount_credit_card_charge", type="decimal", scale=2, nullable=true)
     */
    protected $amountCreditCardCharge;

    /**
     * @ORM\Column(name="amount_charity_charge", type="decimal", scale=2, nullable=true)
     */
    protected $amountCharityCharge;

    /**
     * @ORM\Column(name="type", type="integer", nullable=true)
     */
    protected $type;

    /**
     * 1, null - smart_pay 2- worldPay
     * @ORM\Column(name="payment_provider", type="integer", nullable=true)
     */
    protected $paymentProvider;

    /**
     * @ORM\Column(name="smart_pay_AuthResult", type="string", length=255, nullable=true)
     */
    protected $smartPayAuthResult;

    /**
     * @ORM\Column(name="smart_pay_pspReference", type="string", length=255, nullable=true)
     */
    protected $smartPayPspReference;

    /**
     * @ORM\Column(name="smart_pay_merchantReference", type="string", length=255, nullable=true)
     */
    protected $smartPayMerchantReference;

    /**
     * @ORM\Column(name="smart_pay_merchantSig", type="string", length=255, nullable=true)
     */
    protected $smartPayMerchantSig;

    /**
     * @ORM\Column(name="smart_pay_paymentMethod", type="string", length=255, nullable=true)
     */
    protected $smartPayPaymentMethod;

    /**
     * @ORM\Column(name="smart_pay_shopperLocale", type="string", length=255, nullable=true)
     */
    protected $smartPayShopperLocale;

    /**
     * @ORM\Column(name="smart_pay_merchantReturnData", type="string", length=255, nullable=true)
     */
    protected $smartPayMerchantReturnData;

    /**

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return PaymentTransaction
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime 
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set amount
     *
     * @param string $amount
     * @return PaymentTransaction
     */
    public function setAmount($amount)
    {
        $this->amount = $amount;

        return $this;
    }

    /**
     * Get amount
     *
     * @return string 
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * Set reservation
     *
     * @param \UTT\ReservationBundle\Entity\Reservation $reservation
     * @return PaymentTransaction
     */
    public function setReservation(\UTT\ReservationBundle\Entity\Reservation $reservation = null)
    {
        $this->reservation = $reservation;

        return $this;
    }

    /**
     * Get reservation
     *
     * @return \UTT\ReservationBundle\Entity\Reservation 
     */
    public function getReservation()
    {
        return $this->reservation;
    }

    /**
     * Set code
     *
     * @param string $code
     * @return PaymentTransaction
     */
    public function setCode($code)
    {
        $this->code = $code;

        return $this;
    }

    /**
     * Get code
     *
     * @return string 
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * Set status
     *
     * @param integer $status
     * @return PaymentTransaction
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return integer 
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set smartPayAuthResult
     *
     * @param string $smartPayAuthResult
     * @return PaymentTransaction
     */
    public function setSmartPayAuthResult($smartPayAuthResult)
    {
        $this->smartPayAuthResult = $smartPayAuthResult;

        return $this;
    }

    /**
     * Get smartPayAuthResult
     *
     * @return string 
     */
    public function getSmartPayAuthResult()
    {
        return $this->smartPayAuthResult;
    }

    /**
     * Set smartPayPspReference
     *
     * @param string $smartPayPspReference
     * @return PaymentTransaction
     */
    public function setSmartPayPspReference($smartPayPspReference)
    {
        $this->smartPayPspReference = $smartPayPspReference;

        return $this;
    }

    /**
     * Get smartPayPspReference
     *
     * @return string 
     */
    public function getSmartPayPspReference()
    {
        return $this->smartPayPspReference;
    }

    /**
     * Set smartPayMerchantReference
     *
     * @param string $smartPayMerchantReference
     * @return PaymentTransaction
     */
    public function setSmartPayMerchantReference($smartPayMerchantReference)
    {
        $this->smartPayMerchantReference = $smartPayMerchantReference;

        return $this;
    }

    /**
     * Get smartPayMerchantReference
     *
     * @return string 
     */
    public function getSmartPayMerchantReference()
    {
        return $this->smartPayMerchantReference;
    }

    /**
     * Set smartPayMerchantSig
     *
     * @param string $smartPayMerchantSig
     * @return PaymentTransaction
     */
    public function setSmartPayMerchantSig($smartPayMerchantSig)
    {
        $this->smartPayMerchantSig = $smartPayMerchantSig;

        return $this;
    }

    /**
     * Get smartPayMerchantSig
     *
     * @return string 
     */
    public function getSmartPayMerchantSig()
    {
        return $this->smartPayMerchantSig;
    }

    /**
     * Set smartPayPaymentMethod
     *
     * @param string $smartPayPaymentMethod
     * @return PaymentTransaction
     */
    public function setSmartPayPaymentMethod($smartPayPaymentMethod)
    {
        $this->smartPayPaymentMethod = $smartPayPaymentMethod;

        return $this;
    }

    /**
     * Get smartPayPaymentMethod
     *
     * @return string 
     */
    public function getSmartPayPaymentMethod()
    {
        return $this->smartPayPaymentMethod;
    }

    /**
     * Set smartPayShopperLocale
     *
     * @param string $smartPayShopperLocale
     * @return PaymentTransaction
     */
    public function setSmartPayShopperLocale($smartPayShopperLocale)
    {
        $this->smartPayShopperLocale = $smartPayShopperLocale;

        return $this;
    }

    /**
     * Get smartPayShopperLocale
     *
     * @return string 
     */
    public function getSmartPayShopperLocale()
    {
        return $this->smartPayShopperLocale;
    }

    /**
     * Set smartPayMerchantReturnData
     *
     * @param string $smartPayMerchantReturnData
     * @return PaymentTransaction
     */
    public function setSmartPayMerchantReturnData($smartPayMerchantReturnData)
    {
        $this->smartPayMerchantReturnData = $smartPayMerchantReturnData;

        return $this;
    }

    /**
     * Get smartPayMerchantReturnData
     *
     * @return string 
     */
    public function getSmartPayMerchantReturnData()
    {
        return $this->smartPayMerchantReturnData;
    }

    /**
     * Set amountCreditCardCharge
     *
     * @param string $amountCreditCardCharge
     * @return PaymentTransaction
     */
    public function setAmountCreditCardCharge($amountCreditCardCharge)
    {
        $this->amountCreditCardCharge = $amountCreditCardCharge;

        return $this;
    }

    /**
     * Get amountCreditCardCharge
     *
     * @return string 
     */
    public function getAmountCreditCardCharge()
    {
        return $this->amountCreditCardCharge;
    }

    /**
     * Set type
     *
     * @param integer $type
     * @return PaymentTransaction
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return integer 
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set amountCharityCharge
     *
     * @param string $amountCharityCharge
     * @return PaymentTransaction
     */
    public function setAmountCharityCharge($amountCharityCharge)
    {
        $this->amountCharityCharge = $amountCharityCharge;

        return $this;
    }

    /**
     * Get amountCharityCharge
     *
     * @return string 
     */
    public function getAmountCharityCharge()
    {
        return $this->amountCharityCharge;
    }

    /**
     * @return mixed
     */
    public function getPaymentProvider()
    {
        return $this->paymentProvider;
    }

    /**
     * @param mixed $paymentProvider
     * @return PaymentTransaction
     */
    public function setPaymentProvider($paymentProvider)
    {
        $this->paymentProvider = $paymentProvider;
        return $this;
    }

    /**
     * @return bool
     */
    public function isPaymentAuthSuccess()
    {
        return $this->getSmartPayAuthResult() === self::PAYMENT_AUTH_SUCCESS;
    }

}
