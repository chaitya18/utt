<?php

namespace UTT\ReservationBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * OwnerStatement
 *
 * @ORM\Table(name="owner_statement")
 * @ORM\Entity(repositoryClass="UTT\ReservationBundle\Entity\OwnerStatementRepository")
 */
class OwnerStatement
{
    const TYPE_PROPERTY_STATEMENT = 1;
    const TYPE_FULL_STATEMENT = 2;
    const TYPE_FULL_SUMMARY = 3;

    public function getAllowedTypes(){
        $array = array(
            self::TYPE_PROPERTY_STATEMENT => 'Property statement',
            self::TYPE_FULL_STATEMENT => 'Full statement',
            self::TYPE_FULL_SUMMARY => 'Full summary'
        );
        return $array;
    }

    public static function getAllowedTypesStatic(){
        $array = array(
            self::TYPE_PROPERTY_STATEMENT => 'Property statement',
            self::TYPE_FULL_STATEMENT => 'Full statement',
            self::TYPE_FULL_SUMMARY => 'Full summary'
        );
        return $array;
    }


    public function getTypeName($typeId = null){
        $array = $this->getAllowedTypes();
        if(is_null($typeId)){
            if($this->getType()) {
                return array_key_exists($this->getType(), $array) ? $array[$this->getType()] : false;
            }else{
                return false;
            }
        }
        return array_key_exists($typeId, $array) ? $array[$typeId] : false;
    }

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(name="type", type="integer")
     */
    protected $type;

    /**
     * @ORM\Column(name="monthDate", type="date")
     */
    protected $monthDate;

    /**
     * @ORM\ManyToOne(targetEntity="UTT\EstateBundle\Entity\Estate")
     * @ORM\JoinColumn(name="estate_id", referencedColumnName="id", nullable=true, onDelete="CASCADE")
     */
    protected $estate;

    /**
     * @ORM\Column(name="filename", type="string", length=255)
     */
    protected $filename;

    public function getUploadDir()
    {
        return 'uploads/ownerStatements';
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set type
     *
     * @param integer $type
     * @return OwnerStatement
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return integer 
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set filename
     *
     * @param string $filename
     * @return OwnerStatement
     */
    public function setFilename($filename)
    {
        $this->filename = $filename;

        return $this;
    }

    /**
     * Get filename
     *
     * @return string 
     */
    public function getFilename()
    {
        return $this->filename;
    }

    /**
     * Set estate
     *
     * @param \UTT\EstateBundle\Entity\Estate $estate
     * @return OwnerStatement
     */
    public function setEstate(\UTT\EstateBundle\Entity\Estate $estate = null)
    {
        $this->estate = $estate;

        return $this;
    }

    /**
     * Get estate
     *
     * @return \UTT\EstateBundle\Entity\Estate 
     */
    public function getEstate()
    {
        return $this->estate;
    }

    /**
     * Set monthDate
     *
     * @param \DateTime $monthDate
     * @return OwnerStatement
     */
    public function setMonthDate($monthDate)
    {
        $this->monthDate = $monthDate;

        return $this;
    }

    /**
     * Get monthDate
     *
     * @return \DateTime 
     */
    public function getMonthDate()
    {
        return $this->monthDate;
    }
}
