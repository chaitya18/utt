<?php

namespace UTT\ReservationBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * PricingCategoryPriceFlexible
 *
 * @ORM\Table(name="pricing_category_price_flexible")
 * @ORM\Entity(repositoryClass="UTT\ReservationBundle\Entity\PricingCategoryPriceFlexibleRepository")
 */
class PricingCategoryPriceFlexible
{
    const TYPE_MAXIMUM = 1;
    const TYPE_PEAK = 2;
    const TYPE_HIGH = 3;
    const TYPE_MEDIUM = 4;
    const TYPE_LOW = 5;
    const TYPE_MINIMUM = 6;

    public function getAllowedTypes(){
        $array = array(
            self::TYPE_MAXIMUM => 'Maximum',
            self::TYPE_PEAK => 'Peak',
            self::TYPE_HIGH => 'High',
            self::TYPE_MEDIUM => 'Medium',
            self::TYPE_LOW => 'Low',
            self::TYPE_MINIMUM => 'Minimum',
        );
        return $array;
    }

    public static function getAllowedTypesStatic(){
        $array = array(
            self::TYPE_MAXIMUM => 'Maximum',
            self::TYPE_PEAK => 'Peak',
            self::TYPE_HIGH => 'High',
            self::TYPE_MEDIUM => 'Medium',
            self::TYPE_LOW => 'Low',
            self::TYPE_MINIMUM => 'Minimum',
        );
        return $array;
    }

    public function getTypeName($typeId = null){
        $array = $this->getAllowedTypes();
        if(is_null($typeId)){
            if($this->getType()) {
                return array_key_exists($this->getType(), $array) ? $array[$this->getType()] : false;
            }else{
                return false;
            }
        }
        return array_key_exists($typeId, $array) ? $array[$typeId] : false;
    }

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(name="type", type="integer")
     */
    protected $type;

    /**
     * @ORM\Column(name="year", type="integer")
     */
    protected $year;

    /**
     * @ORM\Column(name="price_standing_charge", type="decimal", scale=2, nullable=true)
     */
    protected $priceStandingCharge;

    /**
     * @ORM\Column(name="price_sunday_thursday", type="decimal", scale=2, nullable=true)
     */
    protected $priceSundayThursday;

    /**
     * @ORM\Column(name="price_friday_saturday", type="decimal", scale=2, nullable=true)
     */
    protected $priceFridaySaturday;

    /**
     * @ORM\ManyToOne(targetEntity="PricingCategory", inversedBy="pricesFlexible")
     * @ORM\JoinColumn(name="pricing_category_id", referencedColumnName="id", nullable=false, onDelete="CASCADE")
     */
    protected $pricingCategory;

    public function __toString(){
        if($this->getId())
            return (string) $this->getId();
        return '';
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set type
     *
     * @param integer $type
     * @return PricingCategoryPriceFlexible
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return integer 
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set year
     *
     * @param integer $year
     * @return PricingCategoryPriceFlexible
     */
    public function setYear($year)
    {
        $this->year = $year;

        return $this;
    }

    /**
     * Get year
     *
     * @return integer 
     */
    public function getYear()
    {
        return $this->year;
    }

    /**
     * Set priceSundayThursday
     *
     * @param string $priceSundayThursday
     * @return PricingCategoryPriceFlexible
     */
    public function setPriceSundayThursday($priceSundayThursday)
    {
        $this->priceSundayThursday = $priceSundayThursday;

        return $this;
    }

    /**
     * Get priceSundayThursday
     *
     * @return string 
     */
    public function getPriceSundayThursday()
    {
        return $this->priceSundayThursday;
    }

    /**
     * Set priceFridaySaturday
     *
     * @param string $priceFridaySaturday
     * @return PricingCategoryPriceFlexible
     */
    public function setPriceFridaySaturday($priceFridaySaturday)
    {
        $this->priceFridaySaturday = $priceFridaySaturday;

        return $this;
    }

    /**
     * Get priceFridaySaturday
     *
     * @return string 
     */
    public function getPriceFridaySaturday()
    {
        return $this->priceFridaySaturday;
    }

    /**
     * Set pricingCategory
     *
     * @param \UTT\ReservationBundle\Entity\PricingCategory $pricingCategory
     * @return PricingCategoryPriceFlexible
     */
    public function setPricingCategory(\UTT\ReservationBundle\Entity\PricingCategory $pricingCategory)
    {
        $this->pricingCategory = $pricingCategory;

        return $this;
    }

    /**
     * Get pricingCategory
     *
     * @return \UTT\ReservationBundle\Entity\PricingCategory 
     */
    public function getPricingCategory()
    {
        return $this->pricingCategory;
    }

    /**
     * Set priceStandingCharge
     *
     * @param string $priceStandingCharge
     * @return PricingCategoryPriceFlexible
     */
    public function setPriceStandingCharge($priceStandingCharge)
    {
        $this->priceStandingCharge = $priceStandingCharge;

        return $this;
    }

    /**
     * Get priceStandingCharge
     *
     * @return string 
     */
    public function getPriceStandingCharge()
    {
        return $this->priceStandingCharge;
    }
}
