<?php

namespace UTT\ReservationBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

use UTT\ReservationBundle\Model\VoucherDataPrice;
use UTT\ReservationBundle\Model\VoucherDataUserData;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Voucher
 *
 * @ORM\Table(name="voucher")
 * @ORM\Entity(repositoryClass="UTT\ReservationBundle\Entity\VoucherRepository")
 */
class Voucher
{
    const STATUS_RESERVED = 1;
    const STATUS_FULLY_PAID = 3;
    const STATUS_PART_PAID = 4;

    public function getAllowedStatuses(){
        $array = array(
            self::STATUS_RESERVED => 'Reserved',
            self::STATUS_FULLY_PAID => 'Fully paid',
            self::STATUS_PART_PAID => 'Part paid',
        );
        return $array;
    }

    public function isAllowedToPdf(){
        if($this->getPaid() == $this->getTotalPrice() && $this->getDiscountCode() instanceof DiscountCode){
            return true;
        }

        return false;
    }

    public function isAllowedToPay(){
        //$nowDate = new \DateTime('now');
        //$nowDate->setTime(0,0,0);
        //if($this->getFromDate() < $nowDate){ return false; }

        if($this->getStatus() == self::STATUS_RESERVED || $this->getStatus() == self::STATUS_PART_PAID){
            return true;
        }

        return false;
    }

    public function getStatusName($statusId = null){
        $array = $this->getAllowedStatuses();
        if(is_null($statusId)){
            if($this->getStatus()) {
                return array_key_exists($this->getStatus(), $array) ? $array[$this->getStatus()] : false;
            }else{
                return false;
            }
        }
        return array_key_exists($statusId, $array) ? $array[$statusId] : false;
    }

    const VALUE_50 = 50;
    const VALUE_100 = 100;
    const VALUE_150 = 150;
    const VALUE_200 = 200;
    const VALUE_229 = 229;
    const VALUE_250 = 250;
    const VALUE_300 = 300;
    const VALUE_500 = 500;
    const VALUE_1000 = 1000;

    public function getAllowedValues(){
        $array = array(
            self::VALUE_50 => '50',
            self::VALUE_100 => '100',
            self::VALUE_150 => '150',
            self::VALUE_229 => '229',
            self::VALUE_200 => '200',
            self::VALUE_250 => '250',
            self::VALUE_300 => '300',
            self::VALUE_500 => '500',
            self::VALUE_1000 => '1000',
        );
        return $array;
    }

    public static function getAllowedValuesStatic(){
        $array = array(
            self::VALUE_50 => '50',
            self::VALUE_100 => '100',
            self::VALUE_150 => '150',
            self::VALUE_200 => '200',
            self::VALUE_229 => '229',
            self::VALUE_250 => '250',
            self::VALUE_300 => '300',
            self::VALUE_500 => '500',
            self::VALUE_1000 => '1000',
        );
        return $array;
    }

    public static function isAllowedValueStatic($valueId){
        $array = self::getAllowedValuesStatic();
        return array_key_exists($valueId, $array) ? true : false;
    }

    public function getValueName($valueId = null){
        $array = $this->getAllowedValues();
        if(is_null($valueId)){
            if($this->getValue()) {
                return array_key_exists($this->getValue(), $array) ? $array[$this->getValue()] : false;
            }else{
                return false;
            }
        }
        return array_key_exists($valueId, $array) ? $array[$valueId] : false;
    }

    public function __construct(){
        $this->setCreatedAt(new \DateTime('now'));
        $this->setUpdatedAt(new \DateTime('now'));
        $this->voucherHistoryEntries = new ArrayCollection();
        $this->voucherPaymentTransactions = new ArrayCollection();

        $random = rand(100000000, 999999999);
        $timestamp = $this->getCreatedAt()->getTimestamp();

        $this->setPin((int)$timestamp + (int)$random);
    }

    /**
     * @ORM\PreUpdate()
     */
    public function preUpdate(){
        $this->setUpdatedAt(new \DateTime('now'));
    }

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(name="status", type="integer")
     */
    protected $status;

    /**
     * @ORM\Column(name="created_at", type="datetime")
     */
    protected $createdAt;

    /**
     * @ORM\Column(name="updated_at", type="datetime")
     */
    protected $updatedAt;

    /**
     * @ORM\Column(name="value", type="integer")
     */
    protected $value;

    /**
     * @ORM\Column(name="expires_at", type="date")
     */
    protected $expiresAt;

    /**
     * @ORM\Column(name="name", type="string", length=255, nullable=true)
     */
    protected $name;

    /**
     * @ORM\Column(name="filename", type="string", length=255, nullable=true)
     */
    protected $filename;

    /**
     * @ORM\ManyToOne(targetEntity="UTT\UserBundle\Entity\User")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id", nullable=true, onDelete="SET NULL")
     */
    protected $user;

    /**
     * @ORM\ManyToOne(targetEntity="UTT\ReservationBundle\Entity\DiscountCode")
     * @ORM\JoinColumn(name="discount_code_id", referencedColumnName="id", nullable=true, onDelete="CASCADE")
     */
    protected $discountCode;

    /**
     * @ORM\Column(name="paid", type="decimal", scale=2, nullable=true)
     */
    protected $paid;

    /**
     * @ORM\Column(name="total_price", type="decimal", scale=2, nullable=true)
     */
    protected $totalPrice;

    /**
     * @ORM\Column(name="price", type="text")
     */
    protected $price;

    /**
     * @ORM\Column(name="user_data", type="text")
     */
    protected $userData;

    /**
     * @ORM\Column(name="pin", type="string", length=255)
     */
    protected $pin;

    /**
     * @ORM\OneToMany(targetEntity="VoucherHistoryEntry", mappedBy="voucher", cascade={"persist"})
     */
    protected $voucherHistoryEntries;

    /**
     * @ORM\OneToMany(targetEntity="VoucherPaymentTransaction", mappedBy="voucher", cascade={"persist"})
     */
    protected $voucherPaymentTransactions;

    public function __toString(){
        return $this->getName().' '.$this->getValue();
    }

    public function getUploadDir()
    {
        return 'uploads/voucher';
    }

    /**
     * @param $id
     * @return $this
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set value
     *
     * @param integer $value
     * @return Voucher
     */
    public function setValue($value)
    {
        $this->value = $value;

        return $this;
    }

    /**
     * Get value
     *
     * @return integer 
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * Set expiresAt
     *
     * @param \DateTime $expiresAt
     * @return Voucher
     */
    public function setExpiresAt($expiresAt)
    {
        $this->expiresAt = $expiresAt;

        return $this;
    }

    /**
     * Get expiresAt
     *
     * @return \DateTime 
     */
    public function getExpiresAt()
    {
        return $this->expiresAt;
    }

    /**
     * Set filename
     *
     * @param string $filename
     * @return Voucher
     */
    public function setFilename($filename)
    {
        $this->filename = $filename;

        return $this;
    }

    /**
     * Get filename
     *
     * @return string 
     */
    public function getFilename()
    {
        return $this->filename;
    }

    /**
     * Set user
     *
     * @param \UTT\UserBundle\Entity\User $user
     * @return Voucher
     */
    public function setUser(\UTT\UserBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \UTT\UserBundle\Entity\User 
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set discountCode
     *
     * @param \UTT\ReservationBundle\Entity\DiscountCode $discountCode
     * @return Voucher
     */
    public function setDiscountCode(\UTT\ReservationBundle\Entity\DiscountCode $discountCode = null)
    {
        $this->discountCode = $discountCode;

        return $this;
    }

    /**
     * Get discountCode
     *
     * @return \UTT\ReservationBundle\Entity\DiscountCode 
     */
    public function getDiscountCode()
    {
        return $this->discountCode;
    }

    /**
     * @return bool
     */
    public function isDiscount()
    {
        return !is_null($this->getDiscountCode());
    }

    /**
     * Set status
     *
     * @param integer $status
     * @return Voucher
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return integer 
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return Voucher
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime 
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     * @return Voucher
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime 
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set paid
     *
     * @param string $paid
     * @return Voucher
     */
    public function setPaid($paid)
    {
        $this->paid = $paid;

        return $this;
    }

    /**
     * Get paid
     *
     * @return string 
     */
    public function getPaid()
    {
        return $this->paid;
    }

    /**
     * Set totalPrice
     *
     * @param string $totalPrice
     * @return Voucher
     */
    public function setTotalPrice($totalPrice)
    {
        $this->totalPrice = $totalPrice;

        return $this;
    }

    /**
     * Get totalPrice
     *
     * @return string 
     */
    public function getTotalPrice()
    {
        return $this->totalPrice;
    }

    /**
     * Set price
     *
     * @param string $price
     * @return Voucher
     */
    public function setPrice($price)
    {
        $this->price = $price;

        return $this;
    }

    /**
     * Get price
     *
     * @return string 
     */
    public function getPrice()
    {
        return $this->price;
    }
    /**
     * Get price
     *
     * @return VoucherDataPrice
     */
    public function getPriceDecoded()
    {
        return new VoucherDataPrice(json_decode($this->price));
    }

    /**
     * Set userData
     *
     * @param string $userData
     * @return Voucher
     */
    public function setUserData($userData)
    {
        $this->userData = $userData;

        return $this;
    }

    /**
     * Get userData
     *
     * @return string 
     */
    public function getUserData()
    {
        return $this->userData;
    }
    /**
     * Get userData
     *
     * @return VoucherDataUserData
     */
    public function getUserDataDecoded()
    {
        return new VoucherDataUserData(json_decode($this->userData));
    }

    /**
     * Set pin
     *
     * @param string $pin
     * @return Voucher
     */
    public function setPin($pin)
    {
        $this->pin = $pin;

        return $this;
    }

    /**
     * Get pin
     *
     * @return string 
     */
    public function getPin()
    {
        return $this->pin;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Voucher
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Add voucherHistoryEntries
     *
     * @param \UTT\ReservationBundle\Entity\VoucherHistoryEntry $voucherHistoryEntries
     * @return Voucher
     */
    public function addVoucherHistoryEntry(\UTT\ReservationBundle\Entity\VoucherHistoryEntry $voucherHistoryEntries)
    {
        $this->voucherHistoryEntries[] = $voucherHistoryEntries;

        return $this;
    }

    /**
     * Remove voucherHistoryEntries
     *
     * @param \UTT\ReservationBundle\Entity\VoucherHistoryEntry $voucherHistoryEntries
     */
    public function removeVoucherHistoryEntry(\UTT\ReservationBundle\Entity\VoucherHistoryEntry $voucherHistoryEntries)
    {
        $this->voucherHistoryEntries->removeElement($voucherHistoryEntries);
    }

    /**
     * Get voucherHistoryEntries
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getVoucherHistoryEntries()
    {
        return $this->voucherHistoryEntries;
    }

    /**
     * Add voucherPaymentTransactions
     *
     * @param \UTT\ReservationBundle\Entity\VoucherPaymentTransaction $voucherPaymentTransactions
     * @return Voucher
     */
    public function addVoucherPaymentTransaction(\UTT\ReservationBundle\Entity\VoucherPaymentTransaction $voucherPaymentTransactions)
    {
        $this->voucherPaymentTransactions[] = $voucherPaymentTransactions;

        return $this;
    }

    /**
     * Remove voucherPaymentTransactions
     *
     * @param \UTT\ReservationBundle\Entity\VoucherPaymentTransaction $voucherPaymentTransactions
     */
    public function removeVoucherPaymentTransaction(\UTT\ReservationBundle\Entity\VoucherPaymentTransaction $voucherPaymentTransactions)
    {
        $this->voucherPaymentTransactions->removeElement($voucherPaymentTransactions);
    }

    /**
     * Get voucherPaymentTransactions
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getVoucherPaymentTransactions()
    {
        return $this->voucherPaymentTransactions;
    }
}
