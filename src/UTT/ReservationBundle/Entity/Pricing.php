<?php

namespace UTT\ReservationBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Pricing
 *
 * @ORM\Table(name="pricing")
 * @ORM\Entity(repositoryClass="UTT\ReservationBundle\Entity\PricingRepository")
 */
class Pricing
{
    const TYPE_FLEXIBLE_BOOKING = 1;
    const TYPE_STANDARD_M_F = 2;

    public function getAllowedTypes(){
        $array = array(
            self::TYPE_FLEXIBLE_BOOKING => 'Flexible booking',
            self::TYPE_STANDARD_M_F => 'Standard (M-F)'
        );
        return $array;
    }

    public function getTypeName($typeId = null){
        $array = $this->getAllowedTypes();
        if(is_null($typeId)){
            if($this->getType()) {
                return array_key_exists($this->getType(), $array) ? $array[$this->getType()] : false;
            }else{
                return false;
            }
        }
        return array_key_exists($typeId, $array) ? $array[$typeId] : false;
    }

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(name="type", type="integer", nullable=true)
     */
    protected $type;

    /**
     * @ORM\ManyToOne(targetEntity="UTT\EstateBundle\Entity\Estate")
     * @ORM\JoinColumn(name="estate_id", referencedColumnName="id", nullable=true, onDelete="CASCADE")
     */
    protected $estate;

    /**
     * @ORM\Column(name="from_date", type="datetime", nullable=true)
     */
    protected $fromDate;

    /**
     * @ORM\Column(name="to_date", type="datetime", nullable=true)
     */
    protected $toDate;

    /**
     * @ORM\Column(name="price", type="decimal", scale=2, nullable=true)
     */
    protected $price;

    /**
     * @ORM\Column(name="initial_price", type="decimal", scale=2, nullable=true)
     */
    protected $initialPrice;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }


    /**
     * Set fromDate
     *
     * @param \DateTime $fromDate
     * @return Pricing
     */
    public function setFromDate($fromDate)
    {
        $this->fromDate = $fromDate;

        return $this;
    }

    /**
     * Get fromDate
     *
     * @return \DateTime 
     */
    public function getFromDate()
    {
        return $this->fromDate;
    }

    /**
     * Set toDate
     *
     * @param \DateTime $toDate
     * @return Pricing
     */
    public function setToDate($toDate)
    {
        $this->toDate = $toDate;

        return $this;
    }

    /**
     * Get toDate
     *
     * @return \DateTime 
     */
    public function getToDate()
    {
        return $this->toDate;
    }

    /**
     * Set price
     *
     * @param string $price
     * @return Pricing
     */
    public function setPrice($price)
    {
        $this->price = $price;

        return $this;
    }

    /**
     * Get price
     *
     * @return string 
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * Set estate
     *
     * @param \UTT\EstateBundle\Entity\Estate $estate
     * @return Pricing
     */
    public function setEstate(\UTT\EstateBundle\Entity\Estate $estate = null)
    {
        $this->estate = $estate;

        return $this;
    }

    /**
     * Get estate
     *
     * @return \UTT\EstateBundle\Entity\Estate 
     */
    public function getEstate()
    {
        return $this->estate;
    }

    /**
     * Set initialPrice
     *
     * @param string $initialPrice
     * @return Pricing
     */
    public function setInitialPrice($initialPrice)
    {
        $this->initialPrice = $initialPrice;

        return $this;
    }

    /**
     * Get initialPrice
     *
     * @return string 
     */
    public function getInitialPrice()
    {
        return $this->initialPrice;
    }

    /**
     * Set type
     *
     * @param integer $type
     * @return Pricing
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return integer 
     */
    public function getType()
    {
        return $this->type;
    }
}
