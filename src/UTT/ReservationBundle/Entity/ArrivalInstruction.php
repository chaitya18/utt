<?php

namespace UTT\ReservationBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ArrivalInstruction
 *
 * @ORM\Table(name="arrival_instruction")
 * @ORM\Entity(repositoryClass="UTT\ReservationBundle\Entity\ArrivalInstructionRepository")
 */
class ArrivalInstruction
{
    const TYPE_FOR_RESERVATION = 1;

    public function getAllowedTypes(){
        $array = array(
            self::TYPE_FOR_RESERVATION => 'Arrival instruction for reservation'
        );
        return $array;
    }

    public static function getAllowedTypesStatic(){
        $array = array(
            self::TYPE_FOR_RESERVATION => 'Arrival instruction for reservation'
        );
        return $array;
    }

    public function getTypeName($typeId = null){
        $array = $this->getAllowedTypes();
        if(is_null($typeId)){
            if($this->getType()) {
                return array_key_exists($this->getType(), $array) ? $array[$this->getType()] : false;
            }else{
                return false;
            }
        }
        return array_key_exists($typeId, $array) ? $array[$typeId] : false;
    }

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(name="type", type="integer")
     */
    protected $type;

    /**
     * @ORM\ManyToOne(targetEntity="UTT\ReservationBundle\Entity\Reservation")
     * @ORM\JoinColumn(name="reservation_id", referencedColumnName="id", nullable=true, onDelete="CASCADE")
     */
    protected $reservation;

    /**
     * @ORM\Column(name="filename", type="string", length=255)
     */
    protected $filename;

    /**
     * @ORM\Column(name="external_filename", type="string", length=255, nullable=true)
     */
    protected $externalFilename;

    public function getUploadDir()
    {
        return 'uploads/arrivalInstruction';
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set type
     *
     * @param integer $type
     * @return ArrivalInstruction
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return integer 
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set filename
     *
     * @param string $filename
     * @return ArrivalInstruction
     */
    public function setFilename($filename)
    {
        $this->filename = $filename;

        return $this;
    }

    /**
     * Get filename
     *
     * @return string 
     */
    public function getFilename()
    {
        return $this->filename;
    }

    /**
     * Set reservation
     *
     * @param \UTT\ReservationBundle\Entity\Reservation $reservation
     * @return ArrivalInstruction
     */
    public function setReservation(\UTT\ReservationBundle\Entity\Reservation $reservation = null)
    {
        $this->reservation = $reservation;

        return $this;
    }

    /**
     * Get reservation
     *
     * @return \UTT\ReservationBundle\Entity\Reservation 
     */
    public function getReservation()
    {
        return $this->reservation;
    }

    /**
     * Set externalFilename
     *
     * @param string $externalFilename
     * @return ArrivalInstruction
     */
    public function setExternalFilename($externalFilename)
    {
        $this->externalFilename = $externalFilename;

        return $this;
    }

    /**
     * Get externalFilename
     *
     * @return string 
     */
    public function getExternalFilename()
    {
        return $this->externalFilename;
    }
}
