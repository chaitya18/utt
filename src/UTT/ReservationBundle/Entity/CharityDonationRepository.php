<?php

namespace UTT\ReservationBundle\Entity;

use Doctrine\ORM\EntityRepository;

class CharityDonationRepository extends EntityRepository
{
    public function getDonationsSum(){
        $query = $this->_em->createQuery("SELECT sum(cd.paid) as donationSum FROM UTTReservationBundle:CharityDonation cd");

        $result = $query->getArrayResult();
        if(isset($result[0]) && isset($result[0]['donationSum'])){
            return $result[0]['donationSum'];
        }

        return 0;
    }
}
