<?php

namespace UTT\ReservationBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Offer
 *
 * @ORM\Table(name="offer")
 * @ORM\Entity(repositoryClass="UTT\ReservationBundle\Entity\OfferRepository")
 */
class Offer
{
    const TYPE_GENERAL = 1;
    const TYPE_SPECIFIC = 2;
    const TYPE_LATE_AVAILABILITY = 3;
    const TYPE_ADVERTISING = 4;
    const TYPE_AUTOMATIC_SPECIAL_OFFER = 5;

    public function getAllowedTypes(){
        $array = array(
            self::TYPE_GENERAL => 'General offer',
            self::TYPE_SPECIFIC => 'Specific offer',
            self::TYPE_LATE_AVAILABILITY => 'Late availability',
            self::TYPE_ADVERTISING => 'Advertising',
            self::TYPE_AUTOMATIC_SPECIAL_OFFER => 'Automatic special offer',
        );
        return $array;
    }

    public function getTypeName($typeId = null){
        $array = $this->getAllowedTypes();
        if(is_null($typeId)){
            if($this->getType()) {
                return array_key_exists($this->getType(), $array) ? $array[$this->getType()] : false;
            }else{
                return false;
            }
        }
        return array_key_exists($typeId, $array) ? $array[$typeId] : false;
    }

    const STATUS_ACTIVE = 1;
    const STATUS_NOT_ACTIVE = 2;

    public function getAllowedStatuses(){
        $array = array(
            self::STATUS_ACTIVE => 'Active',
            self::STATUS_NOT_ACTIVE => 'Not active',
        );
        return $array;
    }

    public function getStatusName($statusId = null){
        $array = $this->getAllowedStatuses();
        if(is_null($statusId)){
            if($this->getStatus()) {
                return array_key_exists($this->getStatus(), $array) ? $array[$this->getStatus()] : false;
            }else{
                return false;
            }
        }
        return array_key_exists($statusId, $array) ? $array[$statusId] : false;
    }

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(name="name", type="string", length=255, nullable=true)
     */
    protected $name;

    /**
     * @ORM\Column(name="description", type="text", nullable=true)
     */
    protected $description;

    /**
     * @ORM\Column(name="description_short", type="text", nullable=true)
     */
    protected $descriptionShort;

    /**
     * @ORM\Column(name="valid_from", type="datetime", nullable=true)
     */
    protected $validFrom;

    /**
     * @ORM\Column(name="valid_to", type="datetime", nullable=true)
     */
    protected $validTo;

    /**
     * @ORM\ManyToMany(targetEntity="UTT\EstateBundle\Entity\Estate")
     * @ORM\JoinTable(name="offer_estates",
     *      joinColumns={@ORM\JoinColumn(name="offer_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="estate_id", referencedColumnName="id")}
     *      )
     */
    protected $estates;

    /**
     * @ORM\Column(name="value", type="decimal", scale=2, nullable=true)
     */
    protected $value;

    /**
     * @ORM\Column(name="image", type="string", length=255, nullable=true)
     */
    protected $image;
    public $file;

    /**
     * @ORM\Column(name="type", type="integer", nullable=true)
     */
    protected $type;

    /**
     * @ORM\Column(name="status", type="integer", nullable=true)
     */
    protected $status;

#--------------------------------------------------- ENTITY METHODS ---------------------------------------------------#

    public function __toString(){
        if($this->getName())
            return $this->getName();
        return '';
    }

    public function getUploadDir(){
        return 'uploads/offerImages';
    }

    protected function getUploadRootDir($basepath = null){
        return $basepath.$this->getUploadDir();
    }

    public function uploadImage($basepath = null){
        if (null === $this->file) {
            return;
        }

        if (null === $basepath) {
            return;
        }
        $name =  uniqid().substr($this->file->getClientOriginalName(),-4);
        $this->file->move($this->getUploadRootDir($basepath), $name);
        $this->setImage($name);
        $this->file = null;
    }

#################################################### ENTITY METHODS ####################################################

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->estates = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Offer
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return Offer
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set descriptionShort
     *
     * @param string $descriptionShort
     * @return Offer
     */
    public function setDescriptionShort($descriptionShort)
    {
        $this->descriptionShort = $descriptionShort;

        return $this;
    }

    /**
     * Get descriptionShort
     *
     * @return string 
     */
    public function getDescriptionShort()
    {
        return $this->descriptionShort;
    }

    /**
     * Set validFrom
     *
     * @param \DateTime $validFrom
     * @return Offer
     */
    public function setValidFrom($validFrom)
    {
        $this->validFrom = $validFrom;

        return $this;
    }

    /**
     * Get validFrom
     *
     * @return \DateTime 
     */
    public function getValidFrom()
    {
        return $this->validFrom;
    }

    /**
     * Set validTo
     *
     * @param \DateTime $validTo
     * @return Offer
     */
    public function setValidTo($validTo)
    {
        $this->validTo = $validTo;

        return $this;
    }

    /**
     * Get validTo
     *
     * @return \DateTime 
     */
    public function getValidTo()
    {
        return $this->validTo;
    }

    /**
     * Set value
     *
     * @param string $value
     * @return Offer
     */
    public function setValue($value)
    {
        $this->value = $value;

        return $this;
    }

    /**
     * Get value
     *
     * @return string 
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * Set image
     *
     * @param string $image
     * @return Offer
     */
    public function setImage($image)
    {
        $this->image = $image;

        return $this;
    }

    /**
     * Get image
     *
     * @return string 
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * Set type
     *
     * @param integer $type
     * @return Offer
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return integer 
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Add estates
     *
     * @param \UTT\EstateBundle\Entity\Estate $estates
     * @return Offer
     */
    public function addEstate(\UTT\EstateBundle\Entity\Estate $estates)
    {
        $this->estates[] = $estates;

        return $this;
    }

    /**
     * Remove estates
     *
     * @param \UTT\EstateBundle\Entity\Estate $estates
     */
    public function removeEstate(\UTT\EstateBundle\Entity\Estate $estates)
    {
        $this->estates->removeElement($estates);
    }

    /**
     * Get estates
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getEstates()
    {
        return $this->estates;
    }

    /**
     * Set status
     *
     * @param integer $status
     * @return Offer
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return integer 
     */
    public function getStatus()
    {
        return $this->status;
    }
}
