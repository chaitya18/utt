<?php

namespace UTT\ReservationBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * PricingSeason
 *
 * @ORM\Table(name="pricing_season")
 * @ORM\Entity(repositoryClass="UTT\ReservationBundle\Entity\PricingSeasonRepository")
 */
class PricingSeason
{
    const TYPE_MAXIMUM = 1;
    const TYPE_PEAK = 2;
    const TYPE_HIGH = 3;
    const TYPE_MEDIUM = 4;
    const TYPE_LOW = 5;
    const TYPE_MINIMUM = 6;

    public function getAllowedTypes(){
        $array = array(
            self::TYPE_MAXIMUM => 'Maximum',
            self::TYPE_PEAK => 'Peak',
            self::TYPE_HIGH => 'High',
            self::TYPE_MEDIUM => 'Medium',
            self::TYPE_LOW => 'Low',
            self::TYPE_MINIMUM => 'Minimum',
        );
        return $array;
    }

    public function getTypeName($typeId = null){
        $array = $this->getAllowedTypes();
        if(is_null($typeId)){
            if($this->getType()) {
                return array_key_exists($this->getType(), $array) ? $array[$this->getType()] : false;
            }else{
                return false;
            }
        }
        return array_key_exists($typeId, $array) ? $array[$typeId] : false;
    }

    const NAME_LATE_WINTER = 1;
    const NAME_FEB_HALF_TERM = 2;
    const NAME_EARLY_SPRING = 3;
    const NAME_EASTER = 4;
    const NAME_LATE_SPRING = 5;
    const NAME_WHITSUN = 6;
    const NAME_EARLY_SUMMER = 7;
    const NAME_SUMMER = 8;
    const NAME_EARLY_AUTUMN = 9;
    const NAME_OCT_HALF_TERM = 10;
    const NAME_EARLY_WINTER = 11;
    const NAME_CHRISTMAS = 12;

    public function getAllowedNames(){
        $array = array(
            self::NAME_LATE_WINTER => 'Late winter',
            self::NAME_FEB_HALF_TERM => 'Feb half term',
            self::NAME_EARLY_SPRING => 'Early spring',
            self::NAME_EASTER => 'Easter',
            self::NAME_LATE_SPRING => 'Late spring',
            self::NAME_WHITSUN => 'Whitsun',
            self::NAME_EARLY_SUMMER => 'Early summer',
            self::NAME_SUMMER => 'Summer',
            self::NAME_EARLY_AUTUMN => 'Early autumn',
            self::NAME_OCT_HALF_TERM => 'Oct half term',
            self::NAME_EARLY_WINTER => 'Early winter',
            self::NAME_CHRISTMAS => 'Christmas',
        );
        return $array;
    }

    public function getNameName($nameId = null){
        $array = $this->getAllowedNames();
        if(is_null($nameId)){
            if($this->getName()) {
                return array_key_exists($this->getName(), $array) ? $array[$this->getName()] : false;
            }else{
                return false;
            }
        }
        return array_key_exists($nameId, $array) ? $array[$nameId] : false;
    }

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(name="name", type="integer")
     */
    protected $name;

    /**
     * @ORM\Column(name="type", type="integer")
     */
    protected $type;

    /**
     * @ORM\Column(name="from_date", type="date")
     */
    protected $fromDate;

    /**
     * @ORM\Column(name="to_date", type="date")
     */
    protected $toDate;

    public function __toString(){
        if($this->getName())
            return $this->getNameName().' ('.$this->getTypeName().' - '.$this->getFromDate()->format('Y').')';
        return '';
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param integer $name
     * @return PricingSeason
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return integer 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set type
     *
     * @param integer $type
     * @return PricingSeason
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return integer 
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set fromDate
     *
     * @param \DateTime $fromDate
     * @return PricingSeason
     */
    public function setFromDate($fromDate)
    {
        $this->fromDate = $fromDate;

        return $this;
    }

    /**
     * Get fromDate
     *
     * @return \DateTime 
     */
    public function getFromDate()
    {
        return $this->fromDate;
    }

    /**
     * Set toDate
     *
     * @param \DateTime $toDate
     * @return PricingSeason
     */
    public function setToDate($toDate)
    {
        $this->toDate = $toDate;

        return $this;
    }

    /**
     * Get toDate
     *
     * @return \DateTime 
     */
    public function getToDate()
    {
        return $this->toDate;
    }
}
