<?php

namespace UTT\ReservationBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * PaymentTransactionNotification
 *
 * @ORM\Table(name="payment_transaction_notification")
 * @ORM\Entity
 */
class PaymentTransactionNotification
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(name="request_post_data", type="text", nullable=true)
     */
    protected $requestPostData;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }


    /**
     * Set requestPostData
     *
     * @param string $requestPostData
     * @return PaymentTransactionNotification
     */
    public function setRequestPostData($requestPostData)
    {
        $this->requestPostData = $requestPostData;

        return $this;
    }

    /**
     * Get requestPostData
     *
     * @return string 
     */
    public function getRequestPostData()
    {
        return $this->requestPostData;
    }
}
