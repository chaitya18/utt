<?php

namespace UTT\ReservationBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * VoucherHistoryEntry
 *
 * @ORM\Table(name="voucher_history_entry")
 * @ORM\Entity(repositoryClass="UTT\ReservationBundle\Entity\VoucherHistoryEntryRepository")
 */
class VoucherHistoryEntry
{
    const TYPE_NEW_VOUCHER = 1000;
    const TYPE_VOUCHER_CONFIRMATION_RESENT = 1005;
    const TYPE_CARD_PAYMENT = 1010;
    const TYPE_CARD_FAILED = 2015;
    const TYPE_VOUCHER_DISCOUNT_CODE = 2020;

    public function isAllowedForUserManagePage(){
        if($this->getType()){
            $allowedTypes = array(
                self::TYPE_NEW_VOUCHER,
                self::TYPE_VOUCHER_CONFIRMATION_RESENT
            );

            if(in_array($this->getType(), $allowedTypes)){
                return true;
            }
        }

        return false;
    }

    public function getAllowedTypes(){
        $array = array(
            self::TYPE_NEW_VOUCHER => '1000 - New Voucher',
            self::TYPE_VOUCHER_CONFIRMATION_RESENT => '1005 - Voucher Confirmation sent',
            self::TYPE_CARD_PAYMENT => '1010 - Card Payment',
            self::TYPE_CARD_FAILED => '1015 - Card Payment Failed',
            self::TYPE_VOUCHER_DISCOUNT_CODE => '1020 - Voucher Discount Code',
        );
        return $array;
    }

    public function isValidType($typeId){
        $array = $this->getAllowedTypes();
        return array_key_exists($typeId, $array) ? true : false;
    }

    public function getTypeName($typeId = null){
        $array = $this->getAllowedTypes();
        if(is_null($typeId)){
            if($this->getType()) {
                return array_key_exists($this->getType(), $array) ? $array[$this->getType()] : false;
            }else{
                return false;
            }
        }
        return array_key_exists($typeId, $array) ? $array[$typeId] : false;
    }

    public function __construct() {
        $this->setCreatedAt(new \DateTime('now'));
    }

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(name="type", type="integer")
     */
    protected $type;

    /**
     * @ORM\ManyToOne(targetEntity="Voucher", inversedBy="voucherHistoryEntries")
     * @ORM\JoinColumn(name="voucher_id", referencedColumnName="id", nullable=true, onDelete="SET NULL")
     */
    protected $voucher;

    /**
     * @ORM\ManyToOne(targetEntity="UTT\UserBundle\Entity\User")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id", nullable=true, onDelete="SET NULL")
     */
    protected $user;

    /**
     * @ORM\Column(name="created_at", type="datetime")
     */
    protected $createdAt;

    /**
     * @ORM\Column(name="comment", type="text", nullable=true)
     */
    protected $comment;

    public function __toString(){
        return $this->getCreatedAt()->format('d-M-y H:i').', '.$this->getTypeName().': "'.$this->getComment().'" by '.$this->getUser()->getFirstName().' '.$this->getUser()->getLastName();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set type
     *
     * @param integer $type
     * @return VoucherHistoryEntry
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return integer 
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return VoucherHistoryEntry
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime 
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set comment
     *
     * @param string $comment
     * @return VoucherHistoryEntry
     */
    public function setComment($comment)
    {
        $this->comment = $comment;

        return $this;
    }

    /**
     * Get comment
     *
     * @return string 
     */
    public function getComment()
    {
        return $this->comment;
    }

    /**
     * Set voucher
     *
     * @param \UTT\ReservationBundle\Entity\Voucher $voucher
     * @return VoucherHistoryEntry
     */
    public function setVoucher(\UTT\ReservationBundle\Entity\Voucher $voucher = null)
    {
        $this->voucher = $voucher;

        return $this;
    }

    /**
     * Get voucher
     *
     * @return \UTT\ReservationBundle\Entity\Voucher 
     */
    public function getVoucher()
    {
        return $this->voucher;
    }

    /**
     * Set user
     *
     * @param \UTT\UserBundle\Entity\User $user
     * @return VoucherHistoryEntry
     */
    public function setUser(\UTT\UserBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \UTT\UserBundle\Entity\User 
     */
    public function getUser()
    {
        return $this->user;
    }
}
