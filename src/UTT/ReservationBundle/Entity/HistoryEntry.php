<?php

namespace UTT\ReservationBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * HistoryEntry
 *
 * @ORM\Table(name="history_entry")
 * @ORM\Entity(repositoryClass="UTT\ReservationBundle\Entity\HistoryEntryRepository")
 */
class HistoryEntry
{
    public function __construct() {
        $this->setCreatedAt(new \DateTime('now'));
    }

    //<option selected="selected" value="*"> ALL </option>

    const TYPE_NEW_BOOKING = 100;
    const TYPE_NEW_TELEPHONE_BOOKING = 101;
    const TYPE_NEW_BLOCK_BOOKING = 105;
    const TYPE_NEW_GROUPON_BOOKING = 106;
    const TYPE_NEW_AIRBNB_BOOKING = 107;
    const TYPE_AIRBNB_BOOKING_CHANGED = 108;
    const TYPE_CANCELLATION = 120;
    const TYPE_GUEST_REACTIVATION_NO_PAYMENT = 140;
    const TYPE_WE_HAVE_REINSTATED_YOUR_HOLIDAY = 150;
    const TYPE_NEW_QUOTE = 170;
    const TYPE_HOLIDAY_CHANGED_REQUOTE = 180;
    const TYPE_MANUAL_CREDIT = 200;
    const TYPE_VOUCHER_PAYMENT = 202;
    const TYPE_MANUAL_REFUND = 205;
    const TYPE_HOLD_FOR_PAYMENT = 207;
    const TYPE_CARD_PAYMENT = 210;
    const TYPE_CARD_SURCHARGE_ADDED = 212;
    const TYPE_CARD_FAILED = 215;
    const TYPE_DISCOUNT_VOUCHER_USED = 220;
    const TYPE_COMPLAINT_REFUND = 225;
    const TYPE_COMMENT_TO_GUEST_EMAILED = 300;
    const TYPE_SILENT_COMMENT_TO_HISTORY = 310;
    const TYPE_INTERNAL_COMMENT_HIDDEN_FROM_GUEST = 335;
    const TYPE_CROSS_LINK_CHANGE = 400;
    const TYPE_AUTOMATIC_CANCELLATION_DUE_TO_NON_PAYMENT = 500;
    const TYPE_AUTOMATIC_CANCELLATION_DUE_TO_OVERDUE_BALANCE_PAYMENT = 510;
    const TYPE_BALANCE_DUE_REMINDER_EMAIL = 520;
    const TYPE_ARRIVAL_INSTRUCTIONS = 530;
    const TYPE_AUTOMATIC_REINSTATEMENT_REMINDER = 540;
    const TYPE_THANK_YOU_EMAIL = 550;
    const TYPE_BOOKING_CONFIRMATION_RESENT = 605;
    const TYPE_LANDLORD_NOTIFICATION_SENT = 615;
    const TYPE_DISCOUNT_CODE_ISSUED = 707;
    const TYPE_DISCOUNT_CODE_IN_RETURN_OF_REVIEW = 710;
    const TYPE_CUSTOMER_CONFIRMED_PAYMENT_HAS_BEEN_SENT = 789;
    const TYPE_SELF_SERVICE_GUEST_NOTE = 800;
    const TYPE_SELF_SERVICE_LANDLORD_NOTE = 805;
    const TYPE_SELF_SERVICE_HOLIDAY_OPTIONS_CHANGED_BY_GUEST = 810;
    const TYPE_SELF_SERVICE_GUEST_CANCELLED = 820;
    const TYPE_SELF_SERVICE_ADDED_EXTRA = 830;
    const TYPE_LANDLORD_INTERNAL_COMMENT = 835;
    const TYPE_GUEST_HOUSEKEEPING_INSTRUCTION = 840;
    const TYPE_LANDLORT_HOUSEKEEPING_INSTRUCTION = 845;
    const TYPE_COMPLAINT = 900;
    const TYPE_BLACKLISTED = 910;
    const TYPE_REFERRED_FLAG_CHANGED = 920;
    const TYPE_NO_PAYMENT_CANCELLATION_REINSTATE_EMAIL_SENT = 950;

    public function isAllowedForUserManagePage(){
        if($this->getType()){
            $allowedTypes = array(
                self::TYPE_NEW_BOOKING,
                self::TYPE_SELF_SERVICE_GUEST_NOTE,
                self::TYPE_SELF_SERVICE_LANDLORD_NOTE,
                self::TYPE_SELF_SERVICE_HOLIDAY_OPTIONS_CHANGED_BY_GUEST,
                self::TYPE_SELF_SERVICE_GUEST_CANCELLED,
                self::TYPE_GUEST_HOUSEKEEPING_INSTRUCTION,
                self::TYPE_COMMENT_TO_GUEST_EMAILED,
                self::TYPE_ARRIVAL_INSTRUCTIONS,
                self::TYPE_MANUAL_REFUND,
                self::TYPE_BOOKING_CONFIRMATION_RESENT
            );

            if(in_array($this->getType(), $allowedTypes)){
                return true;
            }
        }

        return false;
    }

    public function isAllowedForCleaningRota(){
        if($this->getType()){
            $allowedTypes = array(
                self::TYPE_SELF_SERVICE_GUEST_NOTE,
                self::TYPE_GUEST_HOUSEKEEPING_INSTRUCTION
            );

            if(in_array($this->getType(), $allowedTypes)){
                return true;
            }
        }

        return false;
    }

    public function isNotAllowedForOwner(){
        if($this->getType()){
            $allowedTypes = array(
                self::TYPE_INTERNAL_COMMENT_HIDDEN_FROM_GUEST,
                self::TYPE_REFERRED_FLAG_CHANGED
            );

            if(in_array($this->getType(), $allowedTypes)){
                return true;
            }
        }

        return false;
    }

    public function getAllowedTypes(){
        $array = array(
            self::TYPE_NEW_BOOKING => '100 - New Booking',
            self::TYPE_NEW_TELEPHONE_BOOKING => '101 - New Telephone Booking',
            self::TYPE_NEW_BLOCK_BOOKING => '105 - New Block Booking',
            self::TYPE_NEW_GROUPON_BOOKING => '106 - New Groupon Booking',
            self::TYPE_NEW_AIRBNB_BOOKING => '107 - New Airbnb Booking',
            self::TYPE_AIRBNB_BOOKING_CHANGED => '108 - Airbnb Booking Changed',
            self::TYPE_CANCELLATION => '120 - Cancellation',
            self::TYPE_GUEST_REACTIVATION_NO_PAYMENT => '140 - Guest reactivation - no payment',
            self::TYPE_WE_HAVE_REINSTATED_YOUR_HOLIDAY => '150 - We have reinstated your holiday',
            self::TYPE_NEW_QUOTE => '170 - New Quote',
            self::TYPE_HOLIDAY_CHANGED_REQUOTE => '180 - Holiday Changed - requote',
            self::TYPE_MANUAL_CREDIT => '200 - Manual Credit',
            self::TYPE_VOUCHER_PAYMENT => '202 - Voucher payment',
            self::TYPE_MANUAL_REFUND => '205 - Refund Money',
            self::TYPE_HOLD_FOR_PAYMENT => '207 - Hold for payment',
            self::TYPE_CARD_PAYMENT => '210 - Card Payment',
            self::TYPE_CARD_SURCHARGE_ADDED => '212 - Card Surcharge Added',
            self::TYPE_CARD_FAILED => '215 - Card Failed',
            self::TYPE_DISCOUNT_VOUCHER_USED => '220 - Discount Voucher Used',
            self::TYPE_COMPLAINT_REFUND => '225 - Complaint refund',
            self::TYPE_COMMENT_TO_GUEST_EMAILED => '300 - Comment to Guest - emailed',
            self::TYPE_SILENT_COMMENT_TO_HISTORY => '310 - Silent Comment to History',
            self::TYPE_INTERNAL_COMMENT_HIDDEN_FROM_GUEST => '335 - Internal Comment - hidden from guest',
            self::TYPE_CROSS_LINK_CHANGE => '400 - Cross Link Change',
            self::TYPE_AUTOMATIC_CANCELLATION_DUE_TO_NON_PAYMENT => '500 - Automatic cancellation due to non payment',
            self::TYPE_AUTOMATIC_CANCELLATION_DUE_TO_OVERDUE_BALANCE_PAYMENT => '510 - Automatic cancellation due to overdue balance payment',
            self::TYPE_BALANCE_DUE_REMINDER_EMAIL => '520 - Balance Due Reminder Email',
            self::TYPE_ARRIVAL_INSTRUCTIONS => '530 - Arrival Instructions',
            self::TYPE_AUTOMATIC_REINSTATEMENT_REMINDER => '540 - Automatic reinstatement reminder',
            self::TYPE_THANK_YOU_EMAIL => '550 - Thank you email',
            self::TYPE_BOOKING_CONFIRMATION_RESENT => '605 - Booking Confirmation sent',
            self::TYPE_LANDLORD_NOTIFICATION_SENT => '615 - Landlord / Owner Notification sent',
            self::TYPE_DISCOUNT_CODE_ISSUED => '707 - Discount Code Issued',
            self::TYPE_DISCOUNT_CODE_IN_RETURN_OF_REVIEW => '710 - Discount Code in return of review',
            self::TYPE_CUSTOMER_CONFIRMED_PAYMENT_HAS_BEEN_SENT => '789 - Customer confirmed payment has been sent',
            self::TYPE_SELF_SERVICE_GUEST_NOTE => '800 - Self Service - Guest Note',
            self::TYPE_SELF_SERVICE_LANDLORD_NOTE => '805 - Self Service - Landlord Note',
            self::TYPE_SELF_SERVICE_HOLIDAY_OPTIONS_CHANGED_BY_GUEST => '810 - Self Service - Holiday options changed by guest',
            self::TYPE_SELF_SERVICE_GUEST_CANCELLED => '820 - Self Service - Guest Cancelled',
            self::TYPE_SELF_SERVICE_ADDED_EXTRA => '830 - Self Service - Added Extra',
            self::TYPE_LANDLORD_INTERNAL_COMMENT => '835 - Landlord Internal Comment',
            self::TYPE_GUEST_HOUSEKEEPING_INSTRUCTION => '840 - Guest Housekeeping Instruction',
            self::TYPE_LANDLORT_HOUSEKEEPING_INSTRUCTION => '845 - Landlord Housekeeping Instruction',
            self::TYPE_COMPLAINT => '900 - Complaint',
            self::TYPE_BLACKLISTED => '910 - Blacklisted',
            self::TYPE_REFERRED_FLAG_CHANGED => '920 - Referred flag changed',
            self::TYPE_NO_PAYMENT_CANCELLATION_REINSTATE_EMAIL_SENT => '950 - No payment cancellation reinstate email sent',
        );
        return $array;
    }

    public function isValidType($typeId){
        $array = $this->getAllowedTypes();
        return array_key_exists($typeId, $array) ? true : false;
    }

    public function getTypeName($typeId = null){
        $array = $this->getAllowedTypes();
        if(is_null($typeId)){
            if($this->getType()) {
                return array_key_exists($this->getType(), $array) ? $array[$this->getType()] : false;
            }else{
                return false;
            }
        }
        return array_key_exists($typeId, $array) ? $array[$typeId] : false;
    }

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(name="type", type="integer")
     */
    protected $type;

    /**
     * @ORM\ManyToOne(targetEntity="Reservation", inversedBy="historyEntries")
     * @ORM\JoinColumn(name="reservation_id", referencedColumnName="id", nullable=true, onDelete="SET NULL")
     */
    protected $reservation;

    /**
     * @ORM\ManyToOne(targetEntity="UTT\UserBundle\Entity\User")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id", nullable=true, onDelete="SET NULL")
     */
    protected $user;

    /**
     * @ORM\Column(name="created_at", type="datetime")
     */
    protected $createdAt;

    /**
     * @ORM\Column(name="comment", type="text", nullable=true)
     */
    protected $comment;

    /**
     * @ORM\Column(name="external_data", type="text", nullable=true)
     */
    protected $externalData;

    /**
     * @ORM\Column(name="ip_address", type="string", length=255, nullable=true)
     */
    protected $ipAddress;

    public function __toString(){
        return $this->getCreatedAt()->format('d-M-y H:i').', '.$this->getTypeName().': "'.$this->getComment().'" by '.$this->getUser()->getFirstName().' '.$this->getUser()->getLastName();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set type
     *
     * @param integer $type
     * @return HistoryEntry
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return integer 
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return HistoryEntry
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime 
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set reservation
     *
     * @param \UTT\ReservationBundle\Entity\Reservation $reservation
     * @return HistoryEntry
     */
    public function setReservation(\UTT\ReservationBundle\Entity\Reservation $reservation = null)
    {
        $this->reservation = $reservation;

        return $this;
    }

    /**
     * Get reservation
     *
     * @return \UTT\ReservationBundle\Entity\Reservation 
     */
    public function getReservation()
    {
        return $this->reservation;
    }

    /**
     * Set comment
     *
     * @param string $comment
     * @return HistoryEntry
     */
    public function setComment($comment)
    {
        $this->comment = $comment;

        return $this;
    }

    /**
     * Get comment
     *
     * @return string 
     */
    public function getComment()
    {
        return $this->comment;
    }

    /**
     * Set user
     *
     * @param \UTT\UserBundle\Entity\User $user
     * @return HistoryEntry
     */
    public function setUser(\UTT\UserBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \UTT\UserBundle\Entity\User 
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set externalData
     *
     * @param string $externalData
     * @return HistoryEntry
     */
    public function setExternalData($externalData)
    {
        $this->externalData = $externalData;

        return $this;
    }

    /**
     * Get externalData
     *
     * @return string 
     */
    public function getExternalData()
    {
        return $this->externalData;
    }

    /**
     * Set ipAddress
     *
     * @param string $ipAddress
     * @return HistoryEntry
     */
    public function setIpAddress($ipAddress)
    {
        $this->ipAddress = $ipAddress;

        return $this;
    }

    /**
     * Get ipAddress
     *
     * @return string 
     */
    public function getIpAddress()
    {
        return $this->ipAddress;
    }
}
