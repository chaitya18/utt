<?php

namespace UTT\ReservationBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

use UTT\ReservationBundle\Entity\Reservation;
use UTT\ReservationBundle\Entity\ReservationRepository;
use Doctrine\ORM\EntityManager;
use UTT\IndexBundle\Service\EmailService;

class ReservationBrokenStatusCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('reservation:reservationBrokenStatus')
            ->setDescription('')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        /** @var EntityManager $_em */
        $_em = $this->getContainer()->get('doctrine')->getManager();
        /** @var EmailService $emailService */
        $emailService = $this->getContainer()->get('utt.emailService');
        /** @var ReservationRepository $reservationRepository */
        $reservationRepository = $_em->getRepository('UTTReservationBundle:Reservation');

        $reservations = $reservationRepository->getReservationsWithBrokenStatus();
        if($reservations){
            /** @var Reservation $reservation */
            foreach($reservations as $reservation){
                $reservation->setStatus(Reservation::STATUS_FULLY_PAID);
                $emailService->sendReservationBrokenStatusChangedEmail($reservation);

                echo $reservation->getId().PHP_EOL;
            }

            $_em->flush();
        }else{
            echo 'No reservations found'.PHP_EOL;
        }
    }
}