<?php

namespace UTT\ReservationBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

use UTT\ReservationBundle\Entity\DiscountCode;
use UTT\ReservationBundle\Entity\DiscountCodeRepository;

use UTT\ReservationBundle\Service\DiscountCodeService;

use Doctrine\ORM\EntityManager;

class DiscountCodeClearExpiredCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('discountCode:clearExpired')
            ->setDescription('')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        /** @var EntityManager $_em */
        $_em = $this->getContainer()->get('doctrine')->getManager();
        /** @var DiscountCodeRepository $discountCodeRepository */
        $discountCodeRepository = $_em->getRepository('UTTReservationBundle:DiscountCode');
        /** @var DiscountCodeService $discountCodeService */
        $discountCodeService = $this->getContainer()->get('utt.discountcodeservice');

        $discountCodes = $discountCodeRepository->findExpired();
        if($discountCodes){
            /** @var DiscountCode $discountCode */
            foreach($discountCodes as $discountCode){
                $discountCodeService->remove($discountCode);
            }
        }
    }
}