<?php

namespace UTT\ReservationBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

use UTT\ReservationBundle\Entity\Reservation;
use UTT\ReservationBundle\Entity\ReservationRepository;
use UTT\ReservationBundle\Service\ReservationService;

class ReservationAutoCancelCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('reservation:autoCancel')
            ->setDescription('Auto cancel all not paid reservations')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        /** @var ReservationRepository $reservationRepository */
        $reservationRepository = $this->getContainer()->get('doctrine')->getManager()->getRepository('UTTReservationBundle:Reservation');
        $reservations = $reservationRepository->getNotPaidReservations();
        if($reservations){
            /** @var ReservationService $reservationService */
            $reservationService = $this->getContainer()->get('utt.reservationservice');

            /** @var Reservation $reservation */
            foreach($reservations as $reservation){
                $cancelled = $reservationService->autoCancel($reservation);
                if($cancelled){
                    echo 'Reservation '.$reservation->getId().' cancelled!'.PHP_EOL;
                }
            }
        }
    }
}