<?php

namespace UTT\ReservationBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

use UTT\ReservationBundle\Service\OfferService;

class ReservationUpdateLateAvailabilityOffersCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('reservation:updateLateAvailabilityOffers')
            ->setDescription('Update late availability offers for all estates')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $estates = $this->getContainer()->get('doctrine')->getManager()->getRepository('UTTEstateBundle:Estate')->getAllActive();
        if($estates){
            /** @var OfferService $offerService */
            $offerService = $this->getContainer()->get('utt.offerservice');

            foreach($estates as $estate){
                $offerService->updateLateAvailabilityOffers($estate);
            }

            echo 'Updated'.PHP_EOL;
        }else{
            echo 'No estates found'.PHP_EOL;
        }
    }
}