<?php

namespace UTT\ReservationBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

use UTT\QueueBundle\Entity\PropertyStatementQueue;
use UTT\QueueBundle\Service\PropertyStatementQueueService;
use UTT\ReservationBundle\Service\OwnerStatementService;
use UTT\ReservationBundle\Entity\OwnerStatement;

class ReservationCreatePropertyStatementsCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('reservation:createPropertyStatements')
            ->setDescription('Create property statements from queue')
            ->addArgument('limit', InputArgument::OPTIONAL, 'Number of documents which should be created', 2)
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $limit = (int) $input->getArgument('limit');
        if(!($limit > 0)){ $limit = 2; }

        /** @var PropertyStatementQueueService $propertyStatementQueueService */
        $propertyStatementQueueService = $this->getContainer()->get('utt.propertystatementqueueservice');

        /** @var OwnerStatementService $ownerStatementService */
        $ownerStatementService = $this->getContainer()->get('utt.ownerstatementservice');

        $propertyStatementQueues = $propertyStatementQueueService->getLatest($limit);
        if($propertyStatementQueues){
            /** @var PropertyStatementQueue $propertyStatementQueue */
            foreach($propertyStatementQueues as $propertyStatementQueue){
                $ownerStatement = $ownerStatementService->generatePropertyStatementPDF($propertyStatementQueue->getEstate(), $propertyStatementQueue->getMonthDate(), true);
                if($ownerStatement instanceof OwnerStatement){
                    echo 'SUCCESS: '.$ownerStatement->getFilename().' created'.PHP_EOL;
                }else{
                    echo 'FAIL: Estate: '.$propertyStatementQueue->getEstate()->getName().', date '.$propertyStatementQueue->getMonthDate()->format('d M Y').' not created'.PHP_EOL;
                }
            }
        }else{
            echo 'Queue is empty'.PHP_EOL;
        }
    }
}