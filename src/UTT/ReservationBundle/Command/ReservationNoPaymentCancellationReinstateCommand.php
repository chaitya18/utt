<?php

namespace UTT\ReservationBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

use UTT\ReservationBundle\Entity\Reservation;
use UTT\ReservationBundle\Entity\ReservationRepository;
use UTT\ReservationBundle\Service\ReservationService;
use UTT\IndexBundle\Service\EmailService;

use Doctrine\ORM\EntityManager;
use UTT\ReservationBundle\Service\HistoryEntryService;
use UTT\ReservationBundle\Entity\HistoryEntry;


class ReservationNoPaymentCancellationReinstateCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('reservation:noPaymentCancellationReinstate')
            ->setDescription('')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        // Greg asked to remove it
        return false;

        /** @var EntityManager $_em */
        $_em = $this->getContainer()->get('doctrine')->getManager();
        /** @var EmailService $emailService */
        $emailService = $this->getContainer()->get('utt.emailService');

        /** @var ReservationRepository $reservationRepository */
        $reservationRepository = $this->getContainer()->get('doctrine')->getManager()->getRepository('UTTReservationBundle:Reservation');
        $reservations = $reservationRepository->getFor1HourNoPaymentCancellationReinstate();
        if($reservations){
            /** @var Reservation $reservation */
            foreach($reservations as $reservation){
                $emailSent = $emailService->sendNoPaymentCancellationReinstate($reservation);
                if($emailSent){
                    $reservation->setIs1HourNoPaymentCancellationReinstateSent(true);
                    $_em->flush();

                    echo 'No payment cancellation email with reinstate for reservation '.$reservation->getId().' sent'.PHP_EOL;
                }
            }
        }
        $reservations = $reservationRepository->getForNoPaymentCancellationReinstate();
        if($reservations){
            /** @var Reservation $reservation */
            foreach($reservations as $reservation){
                $emailSent = $emailService->sendNoPaymentCancellationReinstate($reservation);
                if($emailSent){
                    $reservation->setIsNoPaymentCancellationReinstateSent(true);
                    $_em->flush();

                    echo 'No payment cancellation email with reinstate for reservation '.$reservation->getId().' sent'.PHP_EOL;
                }
            }
        }
    }
}