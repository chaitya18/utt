<?php

namespace UTT\ReservationBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

use UTT\ReservationBundle\Factory\ReservationFactory;
use UTT\ReservationBundle\Entity\Reservation;
use UTT\ReservationBundle\Entity\ReservationRepository;
use UTT\EstateBundle\Entity\Estate;
use UTT\EstateBundle\Entity\EstateRepository;
use UTT\UserBundle\Entity\UserRepository;
use UTT\UserBundle\Factory\UserFactory;

use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\Encoder\XmlEncoder;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\GetSetMethodNormalizer;

use UTT\UserBundle\Entity\User;
use UTT\ReservationBundle\Model\ReservationDataPrice;
use UTT\ReservationBundle\Model\ReservationDataUserData;
use UTT\ReservationBundle\Model\ReservationDataDiscountPrice;
use UTT\ReservationBundle\Model\ReservationDataEstateData;

use Symfony\Component\HttpFoundation\JsonResponse;
use UTT\ReservationBundle\Entity\HistoryEntry;

class ImportBookingHistoryCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('import:bookingHistory')
            ->setDescription('Import booking history')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $xmlReader = new \XMLReader;
        $xmlReader->open('web/import/lb_transaction_history.xml');
        $i = 0;
        $bookingHistoryEntries = array();

        while($xmlReader->read() && $xmlReader->name !== 'table');
        while($xmlReader->read()) {
            if($xmlReader->nodeType == \XMLReader::ELEMENT && $xmlReader->localName == 'column') {
                $nodeName = $xmlReader->getAttribute('name');

                $xmlReader->read();
                $bookingHistoryEntries[$i][$nodeName] = $xmlReader->value;

                if($nodeName == 'comment') {
                    $i++;
                }
            }
        }

        if(count($bookingHistoryEntries) > 0){
            $_em = $this->getContainer()->get('doctrine')->getManager();

            /** @var ReservationRepository $reservationRepository */
            $reservationRepository = $_em->getRepository('UTTReservationBundle:Reservation');
            /** @var ReservationFactory $reservationFactory */
            $reservationFactory = $this->getContainer()->get('utt.reservationfactory');
            /** @var EstateRepository $estateRepository */
            $estateRepository = $_em->getRepository('UTTEstateBundle:Estate');
            /** @var UserRepository $userRepository */
            $userRepository = $_em->getRepository('UTTUserBundle:User');
            /** @var UserFactory $userFactory */
            $userFactory = $this->getContainer()->get('utt.userfactory');

            $encoders = array(new XmlEncoder(), new JsonEncoder());
            $normalizers = array(new GetSetMethodNormalizer());
            $serializer = new Serializer($normalizers, $encoders);

            $i = 0;
            foreach($bookingHistoryEntries as $bookingHistoryEntry){
                $i = $i+1;
                if($i > 50){
                    $_em->clear();

                    $i = 0;
                }
                $reservation = $reservationRepository->findOneBy(array('externalId' => $bookingHistoryEntry['bookingid']));
                if(!($reservation instanceof Reservation)){
                    echo 'Reservation does not exists for booking '.$bookingHistoryEntry['bookingid'].PHP_EOL;
                    continue;
                }

                $user = $userRepository->findOneBy(array('externalId' => $bookingHistoryEntry['userid']));
                if(!($user instanceof User)){
                    $user = $reservation->getUser();
                }

                if(!($user instanceof User)){
                    echo 'User cannot be found for booking '.$bookingHistoryEntry['bookingid'].PHP_EOL;
                    continue;
                }

                $historyEntry = new HistoryEntry();
                $historyEntry->setReservation($reservation);
                $createdAt = new \Datetime($bookingHistoryEntry['actiondate']);
                $historyEntry->setCreatedAt($createdAt);
                $historyEntry->setType($bookingHistoryEntry['cmttype']);
                $historyEntry->setIpAddress($bookingHistoryEntry['ipaddress']);
                $historyEntry->setUser($user);
                $historyEntry->setComment(str_replace('&pound;', '£', $bookingHistoryEntry['comment']));
                $historyEntry->setExternalData($serializer->serialize($bookingHistoryEntry, 'json'));

                try{
                    $_em->persist($historyEntry);
                    $_em->flush();
                    $_em->getConnection()->getConfiguration()->setSQLLogger(null);

                }catch (\Exception $e){
                    throw new \Exception('ERROR! Booking history entry for booking '.$bookingHistoryEntry['bookingid'].' not created. '. $e);
                }
            }
        }
    }
}