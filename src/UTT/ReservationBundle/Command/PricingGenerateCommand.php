<?php

namespace UTT\ReservationBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

use UTT\ReservationBundle\Factory\ReservationFactory;
use UTT\ReservationBundle\Entity\Reservation;
use UTT\ReservationBundle\Entity\ReservationRepository;
use UTT\EstateBundle\Entity\Estate;
use UTT\EstateBundle\Entity\EstateRepository;
use UTT\UserBundle\Entity\UserRepository;
use UTT\UserBundle\Factory\UserFactory;

use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\Encoder\XmlEncoder;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\GetSetMethodNormalizer;

use UTT\UserBundle\Entity\User;
use UTT\ReservationBundle\Model\ReservationDataPrice;
use UTT\ReservationBundle\Model\ReservationDataUserData;
use UTT\ReservationBundle\Model\ReservationDataDiscountPrice;
use UTT\ReservationBundle\Model\ReservationDataEstateData;

use Symfony\Component\HttpFoundation\JsonResponse;
use UTT\ReservationBundle\Entity\HistoryEntry;
use UTT\ReservationBundle\Entity\PricingCategory;
use UTT\ReservationBundle\Entity\PricingCategoryRepository;
use UTT\ReservationBundle\Service\PricingService;


class PricingGenerateCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('pricing:generate')
            ->setDescription('Generate pricing for all properties')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $_em = $this->getContainer()->get('doctrine')->getManager();
        /** @var EstateRepository $estateRepository */
        $estateRepository = $_em->getRepository('UTTEstateBundle:Estate');
        /** @var PricingService $pricingService */
        $pricingService = $this->getContainer()->get('utt.pricingservice');

        $activeEstates = $estateRepository->getAllActive();
        if($activeEstates){
            /** @var Estate $estate */
            foreach($activeEstates as $estate){
                $pricingCategory = $estate->getPricingCategory();

                if($estate->getType() == Estate::TYPE_STANDARD_M_F && $pricingCategory->getType() == PricingCategory::TYPE_STANDARD_M_F){
                    $pricingService->generateStandardPricingForEstate($estate, $pricingCategory);
                }elseif($estate->getType() == Estate::TYPE_FLEXIBLE_BOOKING && $pricingCategory->getType() == PricingCategory::TYPE_FLEXIBLE_BOOKING){
                    $pricingService->generateFlexiblePricingForEstate($estate, $pricingCategory);
                }

                $lowestPrice = $pricingService->findLowestPriceByPricingCategory($estate);
                if($lowestPrice != -1){ $estate->setLowestPrice($lowestPrice); }

                $highestPrice = $pricingService->findHighestPrice($estate);
                if($highestPrice != -1){ $estate->setHighestPrice($highestPrice); }

                $_em->flush();

                echo 'Generated for '.$estate->getName().PHP_EOL;
            }
        }else{
            throw new \Exception('No estates found');
        }
    }
}
