<?php

namespace UTT\ReservationBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

use UTT\ReservationBundle\Factory\ReservationFactory;
use UTT\ReservationBundle\Entity\Reservation;
use UTT\ReservationBundle\Entity\ReservationRepository;
use UTT\EstateBundle\Entity\Estate;
use UTT\EstateBundle\Entity\EstateRepository;
use UTT\UserBundle\Entity\UserRepository;
use UTT\UserBundle\Factory\UserFactory;

use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\Encoder\XmlEncoder;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\GetSetMethodNormalizer;

use UTT\UserBundle\Entity\User;
use UTT\ReservationBundle\Model\ReservationDataPrice;
use UTT\ReservationBundle\Model\ReservationDataUserData;
use UTT\ReservationBundle\Model\ReservationDataDiscountPrice;
use UTT\ReservationBundle\Model\ReservationDataEstateData;

use Symfony\Component\HttpFoundation\JsonResponse;

class ImportBookingCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('import:bookings')
            ->setDescription('Import bookings')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $xmlReader = new \XMLReader;
        $xmlReader->open('web/import/lb_customer_bookings.xml');
        $i = 0;
        $bookings = array();

        while($xmlReader->read() && $xmlReader->name !== 'table');
        while($xmlReader->read()) {
            if($xmlReader->nodeType == \XMLReader::ELEMENT && $xmlReader->localName == 'column') {
                $nodeName = $xmlReader->getAttribute('name');

                $xmlReader->read();
                $bookings[$i][$nodeName] = $xmlReader->value;

                if($nodeName == 'commission') {
                    $i++;
                }
            }
        }

        if(count($bookings) > 0){
            $_em = $this->getContainer()->get('doctrine')->getManager();

            /** @var ReservationRepository $reservationRepository */
            $reservationRepository = $_em->getRepository('UTTReservationBundle:Reservation');
            /** @var ReservationFactory $reservationFactory */
            $reservationFactory = $this->getContainer()->get('utt.reservationfactory');
            /** @var EstateRepository $estateRepository */
            $estateRepository = $_em->getRepository('UTTEstateBundle:Estate');
            /** @var UserRepository $userRepository */
            $userRepository = $_em->getRepository('UTTUserBundle:User');
            /** @var UserFactory $userFactory */
            $userFactory = $this->getContainer()->get('utt.userfactory');

            $encoders = array(new XmlEncoder(), new JsonEncoder());
            $normalizers = array(new GetSetMethodNormalizer());
            $serializer = new Serializer($normalizers, $encoders);

            $i = 0;
            foreach($bookings as $booking){
                if(!($booking['configid'] == 0 || $booking['configid'] == 1)){
                    continue;
                }

                $i = $i+1;
                if($i > 50){
                    $_em->clear();

                    $i = 0;
                }
                $reservation = $reservationRepository->findOneBy(array('externalId' => $booking['bookingid']));
                if($reservation instanceof Reservation){
                    echo 'Reservation already exists for booking '.$booking['bookingid'].PHP_EOL;
                    continue;
                }

                /** @var Estate $estate */
                $estate = $estateRepository->findOneBy(array('externalId' => $booking['accomid']));
                if(!($estate instanceof Estate)){
                    echo 'Estate not found for booking '.$booking['bookingid'].'. Missing accomid '.$booking['accomid'].PHP_EOL;
                    continue;
                }

                $user = false;
                if($booking['email']){
                    $user = $userRepository->findOneBy(array('email' => $booking['email']));
                    if(!($user instanceof User)){
                        $user = $userFactory->createImported($booking['email'], $booking['name'], $booking['phone'], $booking['mobile'], $booking['address1'], $booking['address2'], $booking['city'], $booking['postcode'], $booking['country']);
                    }
                }else{
                    $owners = $estate->getOwnerUsers();
                    if(count($owners) > 0){
                        $user = $owners[0];
                    }else{
                        $user = $userRepository->find(1);
                    }
                }

                if(!($user instanceof User)){
                    echo 'User cannot be found for booking '.$booking['bookingid'].'. Missing user email '.$booking['email'].PHP_EOL;
                    continue;
                }


                $fromDate = new \Datetime($booking['startdate']);
                $toDate = new \Datetime($booking['enddate']);
                $toDate->modify('+1 day');
                $sleeps = (int) $booking['guests'] + (int) $booking['guests_children'];
                $sleepsChildren = (int) $booking['guests_children'];
                $pets = (int)$booking['dogs'];
                $infants = (int) $booking['guests_infants'];

                $reservationDataPrice = new ReservationDataPrice();
                $reservationDataPrice->setBasePrice($booking['baseprice']);
                $reservationDataPrice->setAdditionalSleepsPrice($booking['optionsprice']);
                $reservationDataPrice->setAdminCharge($booking['admincharge']);
                $reservationDataPrice->setPetsPrice($booking['extrasprice']);
                $reservationDataPrice->setTotal((float) $reservationDataPrice->getBasePrice() + (float) $reservationDataPrice->getAdditionalSleepsPrice() + (float) $reservationDataPrice->getAdminCharge() + (float) $reservationDataPrice->getPetsPrice());

                $reservationDataUserData = new ReservationDataUserData();
                $reservationDataUserData->setEmail($booking['email']);
                $reservationDataUserData->setFirstName($booking['name']);
                $reservationDataUserData->setLastName('');
                $reservationDataUserData->setPhone($booking['phone'].', '.$booking['mobile']);
                $reservationDataUserData->setAddress($booking['address1'].', '.$booking['address2']);
                $reservationDataUserData->setCity($booking['city']);
                $reservationDataUserData->setPostcode($booking['postcode']);
                # TODO ustawianie country
                # $reservationDataUserData->setCountry('');

                $reservationDataDiscountPrice = new ReservationDataDiscountPrice();
                $reservationDataDiscountPrice->setDiscountCodePrice((float) $booking['discountprice'] + (float) $booking['partusediscount']);
                $reservationDataDiscountPrice->setTotal($reservationDataDiscountPrice->getDiscountCodePrice());

                $reservation = $reservationFactory->create($estate, $user, $fromDate, $toDate, $sleeps, $sleepsChildren, $pets, $infants, $reservationDataPrice, $reservationDataUserData, $reservationDataDiscountPrice);
                $reservation->setId($booking['bookingid']);
                $reservation->setIpAddress($booking['ipaddress']);
                $reservation->setExternalPin($booking['pin']);
                try{
                    $reservation->setCreatedAt(new \Datetime($booking['bookingdate']));
                }catch (\Exception $e){
                    $createdAt = clone $fromDate;

                    $reservation->setCreatedAt($createdAt->modify('-2 days'));
                }

                try{
                    $reservation->setBalanceDueDate(new \Datetime($booking['balanceduedate']));
                }catch (\Exception $e){

                }

                $reservation->setPaid($booking['payments']);
                $reservation->setExternalId($booking['bookingid']);
                $reservation->setExternalData($serializer->serialize($booking, 'json'));
                $reservation->setCommission((int) $booking['commission']);
                if(!$booking['email']){
                    $reservation->setNote($booking['name']);
                }

                switch($booking['status']){
                    case 'X':
                        $status = Reservation::STATUS_CANCELLED;
                        break;
                    case 'R':
                        $status = Reservation::STATUS_RESERVED;
                        break;
                    case 'P':
                        $status = Reservation::STATUS_PART_PAID;
                        break;
                    case 'B':
                        $status = Reservation::STATUS_BLOCKED_OWNER;
                        break;
                    case 'F':
                        $status = Reservation::STATUS_FULLY_PAID;
                        break;
                    case 'H':
                        $status = '';
                        break;
                    default:
                        $status = Reservation::STATUS_RESERVED;
                }
                $reservation->setStatus($status);

                try{
                    $_em->persist($reservation);

                    $metadata = $_em->getClassMetaData(get_class($reservation));
                    $metadata->setIdGeneratorType(\Doctrine\ORM\Mapping\ClassMetadata::GENERATOR_TYPE_NONE);

                    $_em->flush();
                    $_em->getConnection()->getConfiguration()->setSQLLogger(null);

                    //echo 'Reservation created for booking '.$booking['bookingid'].PHP_EOL;
                }catch (\Exception $e){
                    throw new \Exception('ERROR! Reservation '.$booking['bookingid'].' not created. '. $e);
                }
            }
        }
    }
}