<?php

namespace UTT\ReservationBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

use UTT\QueueBundle\Entity\PropertyStatementEmailQueue;
use UTT\QueueBundle\Service\PropertyStatementEmailQueueService;
use UTT\ReservationBundle\Service\OwnerStatementService;
use UTT\ReservationBundle\Entity\OwnerStatement;
use UTT\IndexBundle\Service\EmailService;

class ReservationEmailPropertyStatementsCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('reservation:emailPropertyStatements')
            ->setDescription('Email property statements from queue')
            ->addArgument('limit', InputArgument::OPTIONAL, 'Number of documents which should be emailed', 2)
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $limit = (int) $input->getArgument('limit');
        if(!($limit > 0)){ $limit = 2; }

        /** @var PropertyStatementEmailQueueService $propertyStatementEmailQueueService */
        $propertyStatementEmailQueueService = $this->getContainer()->get('utt.propertystatementemailqueueservice');

        /** @var EmailService $emailService */
        $emailService = $this->getContainer()->get('utt.emailservice');

        $propertyStatementEmailQueues = $propertyStatementEmailQueueService->getLatest($limit);
        if($propertyStatementEmailQueues){
            /** @var PropertyStatementEmailQueue $propertyStatementEmailQueue */
            foreach($propertyStatementEmailQueues as $propertyStatementEmailQueue){
                /** @var OwnerStatement $ownerStatement */
                $ownerStatement = $propertyStatementEmailQueue->getOwnerStatement();
                if($ownerStatement instanceof OwnerStatement){
                    $emailService->sendOwnerStatementsEmail($ownerStatement);
                    $propertyStatementEmailQueueService->removeFromQueue($ownerStatement);

                    echo 'SUCCESS: '.$ownerStatement->getFilename().' emailed'.PHP_EOL;
                }
            }
        }else{
            echo 'Queue is empty'.PHP_EOL;
        }
    }
}