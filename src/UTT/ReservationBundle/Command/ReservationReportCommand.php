<?php

namespace UTT\ReservationBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

use UTT\ReservationBundle\Entity\Reservation;
use UTT\ReservationBundle\Entity\ReservationRepository;

use UTT\EstateBundle\Entity\Estate;
use UTT\EstateBundle\Entity\EstateRepository;

use Doctrine\ORM\EntityManager;

class ReservationReportCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('reservation:reportGenerate')
            ->setDescription('')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        /** @var EntityManager $_em */
        $_em = $this->getContainer()->get('doctrine')->getManager();
        /** @var EstateRepository $estateRepository */
        $estateRepository = $_em->getRepository('UTTEstateBundle:Estate');
        /** @var ReservationRepository $reservationRepository */
        $reservationRepository = $_em->getRepository('UTTReservationBundle:Reservation');

        $estates = $estateRepository->getAllActive();
        if($estates){
            /** @var Estate $estate */
            foreach($estates as $estate){
                echo $estate->getName().';';

                $normalBookings = 0;
                $ownerBookings = 0;
                $reservations = $reservationRepository->getReservationsByYearStringAndEstateId(2014, $estate->getId(), null, false);
                if($reservations){
                    /** @var Reservation $reservation */
                    foreach($reservations as $reservation){
                        if($reservation->isNormalBooking()) $normalBookings++;
                        if($reservation->isBlocked()) $ownerBookings++;
                    }
                }
                echo $normalBookings.';'.$ownerBookings.';';

                $normalBookings = 0;
                $ownerBookings = 0;
                $reservations = $reservationRepository->getReservationsByYearStringAndEstateId(2015, $estate->getId(), null, false);
                if($reservations){
                    /** @var Reservation $reservation */
                    foreach($reservations as $reservation){
                        if($reservation->isNormalBooking()) $normalBookings++;
                        if($reservation->isBlocked()) $ownerBookings++;
                    }
                }
                echo $normalBookings.';'.$ownerBookings.';';

                $normalBookings = 0;
                $ownerBookings = 0;
                $reservations = $reservationRepository->getReservationsByYearStringAndEstateId(2016, $estate->getId(), null, false);
                if($reservations){
                    /** @var Reservation $reservation */
                    foreach($reservations as $reservation){
                        if($reservation->isNormalBooking()) $normalBookings++;
                        if($reservation->isBlocked()) $ownerBookings++;
                    }
                }
                echo $normalBookings.';'.$ownerBookings.';';

                $normalBookings = 0;
                $ownerBookings = 0;
                $reservations = $reservationRepository->getReservationsByYearStringAndEstateId(2017, $estate->getId(), null, false);
                if($reservations){
                    /** @var Reservation $reservation */
                    foreach($reservations as $reservation){
                        if($reservation->isNormalBooking()) $normalBookings++;
                        if($reservation->isBlocked()) $ownerBookings++;
                    }
                }
                echo $normalBookings.';'.$ownerBookings.';';
                echo PHP_EOL;
            }
        }
    }
}