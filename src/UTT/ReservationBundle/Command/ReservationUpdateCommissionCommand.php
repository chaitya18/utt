<?php

namespace UTT\ReservationBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

use UTT\ReservationBundle\Entity\Reservation;
use UTT\ReservationBundle\Entity\ReservationRepository;
use Doctrine\ORM\EntityManager;

class ReservationUpdateCommissionCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('reservation:updateCommission')
            ->setDescription('')
            ->addArgument('id', InputArgument::REQUIRED, 'Estate ID')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $id = $input->getArgument('id');

        /** @var EntityManager $_em */
        $_em = $this->getContainer()->get('doctrine')->getManager();

        /** @var ReservationRepository $reservationRepository */
        $reservationRepository = $_em->getRepository('UTTReservationBundle:Reservation');
        $reservations = $reservationRepository->getReservationsMadeForEstateIn2018($id);
        if($reservations){
            /** @var Reservation $reservation */
            foreach($reservations as $reservation){
                echo $reservation->getId().' - '.$reservation->getCommission().PHP_EOL;
                $reservation->setCommission($reservation->getEstate()->getUttCommissionRate());
                echo $reservation->getId().' - '.$reservation->getCommission().PHP_EOL.PHP_EOL;
            }

            $_em->flush();
        }else{
            echo 'No reservations found'.PHP_EOL;
        }
    }
}