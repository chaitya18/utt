<?php

namespace UTT\ReservationBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

use UTT\ReservationBundle\Entity\Reservation;
use UTT\ReservationBundle\Entity\ReservationRepository;
use Doctrine\ORM\EntityManager;

class ReservationUpdateCommissionValueCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('reservation:updateCommissionValue')
            ->setDescription('')
//            ->addArgument('id', InputArgument::REQUIRED, 'Estate ID')
            ->addArgument('id', InputArgument::REQUIRED, 'Reservation ID')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $id = $input->getArgument('id');

        /** @var EntityManager $_em */
        $_em = $this->getContainer()->get('doctrine')->getManager();

        /** @var ReservationRepository $reservationRepository */
        $reservationRepository = $_em->getRepository('UTTReservationBundle:Reservation');
//        $reservations = $reservationRepository->getReservationsMadeForEstateIn2018($id);
//        $reservations = $reservationRepository->find($id);
        $reservation = $reservationRepository->find($id);
//        if($reservations){
            /** @var Reservation $reservation */
//            foreach($reservations as $reservation){
                echo $reservation->getId().' - '.$reservation->getCommissionValue().PHP_EOL;
                $reservation->setCommissionValue($reservation->calculateCommissionValue());
                echo $reservation->getId().' - '.$reservation->getCommissionValue().PHP_EOL.PHP_EOL;
//            }

            $_em->flush();
//        }else{
//            echo 'No reservations found'.PHP_EOL;
//        }
    }
}
