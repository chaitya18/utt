<?php

namespace UTT\ReservationBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

use UTT\ReservationBundle\Entity\Reservation;
use UTT\ReservationBundle\Entity\ReservationRepository;
use UTT\ReservationBundle\Service\ReservationService;

use UTT\EstateBundle\Entity\Estate;
use UTT\EstateBundle\Entity\EstateRepository;

use UTT\EstateBundle\Service\AirbnbService;
use UTT\EstateBundle\Library\ICal;

use UTT\UserBundle\Entity\User;

use Doctrine\ORM\EntityManager;

use PhpImap\Mailbox as ImapMailbox;
use PhpImap\IncomingMail as ImapIncomingMail;

class ReservationAirbnbGuestsImportCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('reservation:airbnbGuestsImport')
            ->setDescription('Airbnb guests Import')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        /** @var EntityManager $_em */
        $_em = $this->getContainer()->get('doctrine')->getManager();
        /** @var ReservationRepository $reservationRepository */
        $reservationRepository = $_em->getRepository('UTTReservationBundle:Reservation');
        /** @var ReservationService $reservationService */
        $reservationService = $this->getContainer()->get('utt.reservationservice');

        $mailbox = new ImapMailbox('{underthethatch.co.uk:143}INBOX', 'airbnb@underthethatch.co.uk', 'airbnb#123');

        $mailsIds = $mailbox->searchMailBox('ALL');
        if($mailsIds){
            foreach($mailsIds as $mailId){
                /** @var ImapIncomingMail $mail */
                $mail = $mailbox->getMail($mailId);
                if(isset($mail->textPlain) && $mail->textPlain){
                    preg_match('/Confirmation Code: (.*?)\n/', $mail->textPlain, $code);
                    if(isset($code[1]) && $code[1]){
                        $code = trim($code[1]);
                        preg_match('/\* (.*?) Guests/', $mail->textPlain, $guests);
                        if(isset($guests[1]) && $guests[1]){
                            $guests = trim($guests[1]);

                            /** @var Reservation $reservation */
                            $reservation = $reservationRepository->findAirbnbReservation($code);
                            if($reservation instanceof Reservation){
                                $reservation = $reservationService->airbnbBookingUpdate($reservation, $reservation->getFromDate(), $reservation->getToDate(), $guests);
                                if($reservation instanceof Reservation){
                                    echo 'updated '.$reservation->getId().' booking'.PHP_EOL;
                                }
                            }
                        }
                    }
                }

                $mailbox->deleteMail($mailId);
            }
        }

    }
}