<?php

namespace UTT\ReservationBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

use UTT\ReservationBundle\Entity\Reservation;
use UTT\ReservationBundle\Entity\ReservationRepository;
use UTT\ReservationBundle\Service\ReservationService;
use UTT\IndexBundle\Service\EmailService;

use Doctrine\ORM\EntityManager;
use UTT\ReservationBundle\Service\HistoryEntryService;
use UTT\ReservationBundle\Entity\HistoryEntry;


class ReservationThankYouCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('reservation:thankYou')
            ->setDescription('Thank You reminder')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        /** @var EntityManager $_em */
        $_em = $this->getContainer()->get('doctrine')->getManager();
        /** @var HistoryEntryService $historyEntryService */
        $historyEntryService = $this->getContainer()->get('utt.historyentryservice');
        /** @var EmailService $emailService */
        $emailService = $this->getContainer()->get('utt.emailService');
        /** @var ReservationRepository $reservationRepository */
        $reservationRepository = $_em->getRepository('UTTReservationBundle:Reservation');

        $reservations = $reservationRepository->getReservationForThankYouReminder();
        if($reservations){
            /** @var Reservation $reservation */
            foreach($reservations as $reservation){
                $emailSent = $emailService->sendReservationThankYouEmail($reservation);
                if($emailSent){
                    $historyEntryComment = $historyEntryService->newCommentThankYouEmailSent();
                    $historyEntryService->newEntry(HistoryEntry::TYPE_THANK_YOU_EMAIL, $reservation, $reservation->getUser(), $historyEntryComment);

                    $reservation->setIsThankYouSent(true);
                    $_em->flush();
                    
                    echo 'Thank you email for reservation '.$reservation->getId().' sent'.PHP_EOL;
                }else{
                    error_log(date('c')." Failed To Mail send to the ". $reservation->getId() ."\n", 3, realpath( __DIR__.'/../../../../ownerNotificationReview.log'));
                }
            }
        }
    }
}