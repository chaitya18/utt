<?php

namespace UTT\ReservationBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

use UTT\ReservationBundle\Entity\Reservation;
use UTT\ReservationBundle\Entity\ReservationRepository;
use UTT\EstateBundle\Entity\Estate;
use UTT\EstateBundle\Entity\EstateRepository;
use UTT\ReservationBundle\Service\ReservationService;
use UTT\IndexBundle\Service\EmailService;

use Doctrine\ORM\EntityManager;
use UTT\ReservationBundle\Service\HistoryEntryService;
use UTT\ReservationBundle\Entity\HistoryEntry;


class MultipropertySyncRunCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('multipropertySync:Run')
            ->setDescription('')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        /** @var EntityManager $_em */
        $_em = $this->getContainer()->get('doctrine')->getManager();
        /** @var ReservationRepository $reservationRepository */
        $reservationRepository = $_em->getRepository('UTTReservationBundle:Reservation');
        /** @var EstateRepository $estateRepository */
        $estateRepository = $_em->getRepository('UTTEstateBundle:Estate');
        /** @var ReservationService $reservationService */
        $reservationService = $this->getContainer()->get('utt.reservationservice');

        /** @var EmailService $emailService */
        $emailService = $this->getContainer()->get('utt.emailservice');

        $estates = $estateRepository->getMultipropertyEstates();
        if($estates){
            /** @var Estate $estate */
            foreach($estates as $estate){
                $subEstates = $estate->getSubEstates();

                if($subEstates){
                    $reservations = $reservationRepository->getReservationsForAirbnbExportSync($estate);
                    if($reservations){
                        /** @var Estate $subEstate */
                        foreach($subEstates as $subEstate){
                            /** @var Reservation $reservation */
                            foreach($reservations as $reservation){
                                //https://trello.com/c/VmgXpAuy/137-urgent-cancel-bookng :(
                                if($reservation->getId() == 79322){
                                    continue;
                                }
                                try {
                                    $blockedReservation = $reservationService->bookBlocked(
                                        $subEstate,
                                        $reservation->getUser(),
                                        $reservation->getFromDate(),
                                        $reservation->getToDate(),
                                        Reservation::STATUS_BLOCKED_MULTUPROPERTY,
                                        'Coppied from '.$estate->getName(),
                                        $reservation->getSleeps(),
                                        false
                                    );
                                } catch (\Exception $e) {
                                    $blockedReservation = false;
                                    echo 'Error copy: '.$e->getMessage();
                                }


                                if($blockedReservation instanceof Reservation){
                                    echo $blockedReservation->getId().PHP_EOL;
                                } else {
                                    $emailService->sendTechEmail('Multiproperty Error', 'Can\'t copy reservation id: '. $reservation->getId(). ' for estate: ' . $subEstate->getId());
                                }
                            }
                        }
                    }


                    /** @var Estate $subEstate */
                    foreach($subEstates as $subEstate){
                        $reservations = $reservationRepository->getReservationsForAirbnbExportSync($subEstate);
                        if($reservations){
                            /** @var Reservation $reservation */
                            foreach($reservations as $reservation){
                                try {
                                    $blockedReservation = $reservationService->bookBlocked(
                                        $estate,
                                        $reservation->getUser(),
                                        $reservation->getFromDate(),
                                        $reservation->getToDate(),
                                        Reservation::STATUS_BLOCKED_MULTUPROPERTY,
                                        'Coppied from '.$subEstate->getName(),
                                        $reservation->getSleeps(),
                                        false
                                    );
                                } catch (\Exception $e) {
                                    $blockedReservation = false;
                                    echo 'Error copy: '.$e->getMessage();
                                }
                                if($blockedReservation instanceof Reservation){
                                    echo $blockedReservation->getId().PHP_EOL;
                                } else {
                                    $emailService->sendTechEmail('Multiproperty Error '.$reservation->getId(), 'Can\'t copy reservation id: '. $reservation->getId() .' from: '.$reservation->getFromDate()->format('Y-m-d').' to: '.$reservation->getToDate()->format('Y-m-d'). ' for estate: '.$estate->getId());
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}