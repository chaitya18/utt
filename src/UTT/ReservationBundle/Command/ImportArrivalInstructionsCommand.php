<?php

namespace UTT\ReservationBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

use UTT\EstateBundle\Entity\EstateRepository;
use UTT\EstateBundle\Entity\Estate;

use UTT\ReservationBundle\Entity\Reservation;
use UTT\ReservationBundle\Entity\ReservationRepository;

use UTT\ReservationBundle\Entity\ArrivalInstruction;
use UTT\ReservationBundle\Service\ArrivalInstructionService;

class ImportArrivalInstructionsCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('import:arrivalInstructions')
            ->setDescription('Import arrival instructions')
            ->addArgument('type', InputArgument::REQUIRED, 'Type of arrival instruction')
            ->addArgument('page', InputArgument::REQUIRED, 'Page')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $arrivalInstructionType = $input->getArgument('type');
        $arrivalInstructionPage = $input->getArgument('page');

        if(!array_key_exists($arrivalInstructionType, ArrivalInstruction::getAllowedTypesStatic())){
            throw new \Exception('Type not allowed');
        }

        $_em = $this->getContainer()->get('doctrine')->getManager();

        /** @var ArrivalInstructionService $arrivalInstructionService */
        $arrivalInstructionService = $this->getContainer()->get('utt.arrivalinstructionservice');
        $arrivalInstructionDirectory = $arrivalInstructionService->getFileDirectory(true);
        $arrivalInstructionImportFromDirectory = $arrivalInstructionService->getImportFromFileDirectory(true);

        if($arrivalInstructionType == ArrivalInstruction::TYPE_FOR_RESERVATION){
            /** @var ReservationRepository $reservationRepository */
            $reservationRepository = $_em->getRepository('UTTReservationBundle:Reservation');
            $reservations = $reservationRepository->getLimitedByPage($arrivalInstructionPage);

            if($reservations){
                /** @var Reservation $reservation */
                foreach($reservations as $reservation){
                    $fileName = $arrivalInstructionService->generateExternalArrivalInstructionForReservationFilName($reservation);
                    $newFileName = $arrivalInstructionService->generateArrivalInstructionForReservationFilName($reservation);

                    try{
                        file_put_contents($arrivalInstructionDirectory.$newFileName, file_get_contents($arrivalInstructionImportFromDirectory.$fileName));
                        if(file_exists($arrivalInstructionDirectory.$newFileName)){
                            $arrivalInstructionService->createArrivalInstructionForReservation($reservation, $newFileName, $fileName);
                        }
                    }catch (\Exception $e){
                        echo $fileName.' not found'.PHP_EOL;
                    }
                }
            }else{
                throw new \Exception('No reservations found');
            }
        }else{
            throw new \Exception('Type not allowed');
        }
    }
}