<?php

namespace UTT\ReservationBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

use UTT\ReservationBundle\Entity\Reservation;
use UTT\ReservationBundle\Entity\ReservationRepository;
use UTT\ReservationBundle\Service\ReservationService;

use UTT\EstateBundle\Entity\Estate;
use UTT\EstateBundle\Entity\EstateRepository;

use UTT\EstateBundle\Service\AirbnbService;
use UTT\EstateBundle\Library\ICal;

use UTT\UserBundle\Entity\User;

use Doctrine\ORM\EntityManager;
use UTT\ReservationBundle\Entity\AirbnbIgnore;
use UTT\ReservationBundle\Entity\AirbnbIgnoreRepository;

class ReservationAirbnbImportCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('reservation:airbnbImport')
            ->setDescription('Airbnb Import')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        /** @var EntityManager $_em */
        $_em = $this->getContainer()->get('doctrine')->getManager();
        /** @var EstateRepository $estateRepository */
        $estateRepository = $_em->getRepository('UTTEstateBundle:Estate');
        /** @var ReservationRepository $reservationRepository */
        $reservationRepository = $_em->getRepository('UTTReservationBundle:Reservation');
        /** @var AirbnbService $airbnbService */
        $airbnbService = $this->getContainer()->get('utt.airbnbService');
        /** @var ReservationService $reservationService */
        $reservationService = $this->getContainer()->get('utt.reservationservice');

        $estates = $estateRepository->getActiveWithAirbnbCalendar();
        if($estates){
            /** @var Estate $estate */
            foreach($estates as $estate){
                /** @var AirbnbIgnoreRepository $airbnbIgnoreRepository */
                $airbnbIgnoreRepository = $_em->getRepository('UTTReservationBundle:AirbnbIgnore');
                $airbnbIgnoreDates = $airbnbIgnoreRepository->findBy(array(
                    'estate' => $estate->getId()
                ));

                echo 'Estate '.$estate->getName().PHP_EOL;
                /** @var User $ownerUser */
                $ownerUser = $estate->getManagingUser();
                if(!($ownerUser instanceof User)) { continue; }

                $listingDirectory = $airbnbService->getFileDirectory(true);
                $listingFileName = $airbnbService->generateListingFilName($estate);

                file_put_contents($listingDirectory.$listingFileName, $airbnbService->downloadListing($estate->getAirbnbCalendar()));
                if(file_exists($listingDirectory.$listingFileName)){
                    /** @var ICal $calendar */
                    $calendar = new ICal($listingDirectory.$listingFileName);
                    try{
                        $events = $calendar->events();
                    }catch (\Exception $e){
                       continue;
                    }
                    if(is_array($events) && count($events) > 0){
                        foreach($events as $event){
                            if(!(isset($event['DTSTART']) && isset($event['DTEND']))){ continue; }

                            $description = '';
                            $airbnbRef = '';
                            if(isset($event['DESCRIPTION']) && $event['DESCRIPTION']) {
                                $keys = array_keys($event);
                                foreach($keys as $key => $value){
                                    if($value == 'DTEND' || $value == 'DTSTART' || $value == 'UID' || $value == 'SUMMARY' || $value == 'LOCATION'){
                                        unset($keys[$key]);
                                    }
                                }

                                foreach($keys as $key){
                                    if($key == 'DESCRIPTION'){
                                        $description = $event['DESCRIPTION'];
                                    }else{
                                        $description .= $key.':' . $event[$key];
                                    }
                                }

                                if($description) {
                                    $description = explode('\n', $description);
                                    preg_match_all('#\bhttps?://[^,\s()<>]+(?:\([\w\d]+\)|([^,[:punct:]\s]|/))#', $description[0], $match);
                                    if(!empty($match[0][0])) {
                                        $parsedDesc = parse_url($match[0][0]);
                                        if(!empty($parsedDesc['query'])){
                                            parse_str($parsedDesc['query'], $parserdUrlParams);
                                            if(!empty($parserdUrlParams['code'])){
                                                $airbnbRef = $parserdUrlParams['code'];
                                            }
                                        }
                                    }

                                    $description = join(', ', $description).', ';


//                                    foreach($description as $key => $item){
//                                        if(substr($item, 0 , 5) == 'PHONE' || substr($item, 0, 5) == 'EMAIL'){
//
//                                        }else{
//                                            unset($description[$key]);
//                                        }
//                                    }
//
//                                    $description = join(', ', $description).', ';

                                }
                            }

                            $fromDate = new \DateTime($event['DTSTART']);
                            $toDate = new \DateTime($event['DTEND']);
                            $reason = $description;

                            if($airbnbIgnoreDates){
                                $skipReservation = false;

                                /** @var AirbnbIgnore $airbnbIgnore */
                                foreach($airbnbIgnoreDates as $airbnbIgnore){
                                    if($airbnbIgnore->areDatesEqual($fromDate, $toDate)){
                                        $skipReservation = true;
                                        break;
                                    }
                                }

                                if($skipReservation) continue;
                            }

                            $isAvailableForBooking = $reservationService->isAvailableForBooking($estate, $fromDate, $toDate);
                            if($isAvailableForBooking){
                                if(isset($event['SUMMARY'])){ $reason .= $event['SUMMARY']; }

                                $nowDate = new \DateTime('now');
                                $nowDate->modify('+1 year');
                                $nowDate->modify('-3 days');
                                if($fromDate > $nowDate) continue;

                                if(trim($reason) == 'Airbnb (Not available)') continue;

                                /** @var Reservation $reservation */
                                echo 'CREATE'.PHP_EOL;
                                $reservation = $reservationService->airbnbBooking($estate, $ownerUser, $fromDate, $toDate, $reason);
                                if($reservation instanceof Reservation){
                                    echo 'Created '.$reservation->getId().' booking'.PHP_EOL;
                                }
                            }else{

                                if(isset($event['SUMMARY']) && isset($event['DESCRIPTION']) && !empty($airbnbRef)){
                                    /** @var Reservation $reservation */
                                    $reservation = $reservationRepository->findAirbnbReservationForEstate($airbnbRef, $estate);
                                    if($reservation instanceof Reservation){
                                        if($reservation->getFromDate() == $fromDate && $reservation->getToDate() == $toDate){

                                        }else{
                                            $reservation = $reservationService->airbnbBookingUpdate($reservation, $fromDate, $toDate, $reservation->getSleeps());
                                            if($reservation instanceof Reservation){
                                                echo 'updated '.$reservation->getId().' booking'.PHP_EOL;
                                            }
                                        }
                                    }
                                }
                            }
                        }

                        $airbnbReservations = $reservationRepository->findAirbnbReservations($estate);
                        if($airbnbReservations && count($airbnbReservations) > 0){
                            /** @var Reservation $airbnbReservation */
                            foreach($airbnbReservations as $airbnbReservation){
                                $isValid = false;

                                foreach($events as $event){
                                    if(!(isset($event['DTSTART']) && isset($event['DTEND']))){ continue; }

                                    $fromDate = new \DateTime($event['DTSTART']);
                                    $toDate = new \DateTime($event['DTEND']);

                                    if($airbnbReservation->getFromDate() == $fromDate && $airbnbReservation->getToDate() == $toDate){
                                        $isValid = true;
                                        break;
                                    }
                                }

                                if(!$isValid){
                                    $cancelled = $reservationService->airbnbCancel($airbnbReservation);
                                    if($cancelled){
                                        echo 'cancelled '.$airbnbReservation->getId().PHP_EOL;
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}
