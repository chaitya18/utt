<?php

namespace UTT\ReservationBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

use UTT\EstateBundle\Entity\EstateRepository;
use UTT\EstateBundle\Entity\Estate;
use UTT\ReservationBundle\Entity\OwnerStatement;
use UTT\ReservationBundle\Service\OwnerStatementService;

class ImportOwnerStatementsCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('import:ownerStatements')
            ->setDescription('Import owner statements')
            ->addArgument('type', InputArgument::REQUIRED, 'Type of statement')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $statementType = $input->getArgument('type');
        if(!array_key_exists($statementType, OwnerStatement::getAllowedTypesStatic())){
            throw new \Exception('Type not allowed');
        }

        $agencyConfigId = $this->getContainer()->getParameter('agency_config_id');
        $_em = $this->getContainer()->get('doctrine')->getManager();

        /** @var OwnerStatementService $ownerStatementService */
        $ownerStatementService = $this->getContainer()->get('utt.ownerstatementservice');
        $ownerStatementDirectory = $ownerStatementService->getFileDirectory(true);

        $periodStart = new \DateTime( sprintf('%04d-01-01', '2012') );
        $periodEnd = new \DateTime('now');
        $periodInterval = new \DateInterval('P1M');
        $period = new \DatePeriod($periodStart, $periodInterval, $periodEnd);

        if($statementType == OwnerStatement::TYPE_PROPERTY_STATEMENT){
            /** @var EstateRepository $estateRepository */
            $estateRepository = $_em->getRepository('UTTEstateBundle:Estate');
            $estates = $estateRepository->findAll();
            if($estates){
                /** @var Estate $estate */
                foreach($estates as $estate){
                    if($estate->getExternalId()){
                        /** @var \Datetime $periodDate */
                        foreach ($period as $periodDate) {
                            $fileName = $ownerStatementService->generatePropertyStatementFilName($estate->getExternalId(), $periodDate);
                            $newFileName = $ownerStatementService->generatePropertyStatementFilName($estate->getId(), $periodDate);

                            try{
                                file_put_contents($ownerStatementDirectory.$newFileName, file_get_contents('http://admin.underthethatch.co.uk/admin/pdfcache/ownerinvoices/'.$fileName));
                                if(file_exists($ownerStatementDirectory.$newFileName)){
                                    $ownerStatementService->createPropertyStatement($estate, $periodDate, $newFileName);
                                }
                            }catch (\Exception $e){
                                echo $fileName.' not found'.PHP_EOL;
                            }
                        }
                    }
                }
            }
        }elseif($statementType == OwnerStatement::TYPE_FULL_STATEMENT){
            /** @var \Datetime $periodDate */
            foreach ($period as $periodDate) {
                $fileName = $ownerStatementService->generateFullStatementFileName($periodDate);

                try{
                    file_put_contents($ownerStatementDirectory.$fileName, file_get_contents('http://admin.underthethatch.co.uk/admin/pdfcache/ownerinvoices/'.$fileName));
                    if(file_exists($ownerStatementDirectory.$fileName)){
                        $ownerStatementService->createFullStatement($periodDate, $fileName);
                    }
                }catch (\Exception $e){
                    echo $fileName.' not found'.PHP_EOL;
                }
            }
        }elseif($statementType == OwnerStatement::TYPE_FULL_SUMMARY){
            /** @var \Datetime $periodDate */
            foreach ($period as $periodDate) {
                $fileName = $ownerStatementService->generateSummaryFileName($periodDate);

                try{
                    file_put_contents($ownerStatementDirectory.$fileName, file_get_contents('http://admin.underthethatch.co.uk/admin/pdfcache/ownerinvoices/'.$fileName));
                    if(file_exists($ownerStatementDirectory.$fileName)){
                        $ownerStatementService->createFullSummaryStatement($periodDate, $fileName);
                    }
                }catch (\Exception $e){
                    echo $fileName.' not found'.PHP_EOL;
                }
            }
        }else{
            throw new \Exception('Type not allowed');
        }
    }
}