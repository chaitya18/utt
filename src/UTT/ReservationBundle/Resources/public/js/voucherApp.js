var voucherApp = angular.module('voucherApp', []);

voucherApp.factory('priceFactory', function() {
    var basePrice = 0;
    var creditCardCharge = 0;
    var charityCharge = 0;

    var setBasePrice = function(price){ basePrice = price; };
    var getBasePrice = function(){ return basePrice; };
    var setCreditCardCharge = function(price) { creditCardCharge = price; };
    var getCreditCardCharge = function(){ return creditCardCharge; };
    var setCharityCharge = function(price) { charityCharge = price; };
    var getCharityCharge = function(){ return charityCharge; };

    return {
        setBasePrice: setBasePrice,
        getBasePrice: getBasePrice,
        setCreditCardCharge: setCreditCardCharge,
        getCreditCardCharge: getCreditCardCharge,
        setCharityCharge: setCharityCharge,
        getCharityCharge: getCharityCharge,

        getPrice: function(){
            return {
                basePrice: getBasePrice(),
                creditCardCharge: getCreditCardCharge(),
                charityCharge: getCharityCharge(),
                total: function(){
                    return parseFloat(getBasePrice()) + parseFloat(getCreditCardCharge()) + parseFloat(getCharityCharge());
                }
            }
        }
    }
});

voucherApp.factory('filterFactory', function() {
    var voucherValueChoices = [];
    var getVoucherValueChoices = function(){ return voucherValueChoices; };

    var voucherValue = false;
    var setVoucherValue = function(voucherValueItem){ voucherValue = voucherValueItem; };
    var getVoucherValue = function() { return voucherValue; };

    var voucherName = false;
    var setVoucherName = function(voucherNameItem){ voucherName = voucherNameItem; };
    var getVoucherName = function() { return voucherName; };

    var initVouchers = function(vouchersJson){
        voucherValueChoices = [];
        voucherValueChoices.push({ 'id': 50 });
        voucherValueChoices.push({ 'id': 100 });
        voucherValueChoices.push({ 'id': 150 });
        voucherValueChoices.push({ 'id': 200 });
        voucherValueChoices.push({ 'id': 229 });
        voucherValueChoices.push({ 'id': 250 });
        voucherValueChoices.push({ 'id': 300 });
        voucherValueChoices.push({ 'id': 500 });
        voucherValueChoices.push({ 'id': 1000 });
    };

    return {
        setVoucherValue: setVoucherValue,
        getVoucherValue: getVoucherValue,
        setVoucherName: setVoucherName,
        getVoucherName: getVoucherName,

        getVoucherValueTotal: function(){
            return getVoucherValue().id;
        },

        getVoucherValueChoices: getVoucherValueChoices,
        initVouchers: initVouchers
    }
});

voucherApp.factory('apiService', function($http) {
    $http.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';
    $http.defaults.headers.post["Content-Type"] = "application/x-www-form-urlencoded";

    return {
        getPrice: function(params, callback){
            $http({
                method: 'POST',
                url: Routing.generate('utt_voucher_price'),
                data: $.param(params),
                headers: {'Content-Type':'application/x-www-form-urlencoded; charset=UTF-8'}
            })
                .success(function(data, status, headers, config) {
                    var dataOb = angular.fromJson(data);
                    if(!(typeof dataOb.success === typeof undefined) && dataOb.success == true && !(typeof dataOb.newVoucherPrice === typeof undefined)){
                        callback(dataOb.newVoucherPrice);
                    }else{
                        callback(false);
                    }
                })
                .error(function(data, status, headers, config) {
                    callback(false);
                });
        },
        createVoucher: function(params, callback){
            $http({
                method: 'POST',
                url: Routing.generate('utt_voucher_create'),
                data: $.param(params),
                headers: {'Content-Type':'application/x-www-form-urlencoded; charset=UTF-8'}
            })
                .success(function(data, status, headers, config) {
                    var dataOb = angular.fromJson(data);
                    if(!(typeof dataOb.success === typeof undefined) && dataOb.success == true){
                        callback(true);
                    }else{
                        callback(false);
                    }
                })
                .error(function(data, status, headers, config) {
                    callback(false);
                });
        },
        getCountries: function(callback){
            $http({
                method: 'POST',
                url: Routing.generate('utt_api_all_countries'),
                headers: {'Content-Type':'application/x-www-form-urlencoded; charset=UTF-8'}
            })
                .success(function(data, status, headers, config) {
                    var dataOb = angular.fromJson(data);
                    if(!(typeof dataOb.success === typeof undefined) && dataOb.success == true && !(typeof dataOb.countries === typeof undefined)){
                        callback(dataOb.countries);
                    }else{
                        callback(false);
                    }
                })
                .error(function(data, status, headers, config) {
                    callback(false);
                });
        }
    }
});

voucherApp.factory('userDataFactory', function($http) {
    var countryChoices = [];
    var setCountryChoices = function(countries){ countryChoices = countries; };
    var getCountryChoices = function(){ return countryChoices; };

    var userData = {
        firstName: '',
        lastName: '',
        email: '',
        phone: '',
        address: '',
        city: '',
        postcode: '',
        country: ''
    };

    var setUserFirstName = function(data) { userData.firstName = data; };
    var getUserFirstName = function() { return userData.firstName; };

    var setUserLastName = function(data) { userData.lastName = data; };
    var getUserLastName = function() { return userData.lastName; };

    var setUserEmail = function(data) { userData.email = data; };
    var getUserEmail = function() { return userData.email; };

    var setUserPhone = function(data) { userData.phone = data; };
    var getUserPhone = function() { return userData.phone; };

    var setUserAddress = function(data) { userData.address = data; };
    var getUserAddress = function() { return userData.address; };

    var setUserCity = function(data) { userData.city = data; };
    var getUserCity = function() { return userData.city; };

    var setUserPostcode = function(data) { userData.postcode = data; };
    var getUserPostcode = function() { return userData.postcode; };

    var setUserCountry = function(data) { userData.country = data; };
    var getUserCountry = function() { return userData.country; };

    return {
        setCountryChoices: setCountryChoices,
        getCountryChoices: getCountryChoices,
        setUserFirstName: setUserFirstName,
        getUserFirstName: getUserFirstName,
        setUserLastName: setUserLastName,
        getUserLastName: getUserLastName,
        setUserEmail: setUserEmail,
        getUserEmail: getUserEmail,
        setUserPhone: setUserPhone,
        getUserPhone: getUserPhone,
        setUserAddress: setUserAddress,
        getUserAddress: getUserAddress,
        setUserCity: setUserCity,
        getUserCity: getUserCity,
        setUserPostcode: setUserPostcode,
        getUserPostcode: getUserPostcode,
        setUserCountry: setUserCountry,
        getUserCountry: getUserCountry,

        setData: function(data){
            if(typeof data.firstName === typeof undefined){}else{
                setUserFirstName(data.firstName);
            }
            if(typeof data.lastName === typeof undefined){}else{
                setUserLastName(data.lastName);
            }
            if(typeof data.email === typeof undefined){}else{
                setUserEmail(data.email);
            }
            if(typeof data.phone === typeof undefined){}else{
                setUserPhone(data.phone);
            }
            if(typeof data.address === typeof undefined){}else{
                setUserAddress(data.address);
            }
            if(typeof data.city === typeof undefined){}else{
                setUserCity(data.city);
            }
            if(typeof data.postcode === typeof undefined){}else{
                setUserPostcode(data.postcode);
            }
            if(typeof data.country === typeof undefined){}else{
                setUserCountry(data.country);
            }
        },
        getData: function(){
            return userData;
        }
    }
});

voucherApp.controller('voucherAppCtrl', ['$scope', '$window', 'filterFactory', 'priceFactory', 'userDataFactory', 'apiService', function($scope, $window, filterFactory, priceFactory, userDataFactory, apiService){
    $scope.initUserData = function(json){
        var userData = angular.fromJson(json);
        userDataFactory.setData(userData);

        userDataApply();
    };

    $scope.initCountryChoices = function(){
        apiService.getCountries(function(countries){
            var countryChoices = countries;
            userDataFactory.setCountryChoices(countryChoices);

            $scope.countryChoices = countryChoices;
            if(userDataFactory.getUserCountry()){
                angular.forEach(countryChoices, function(country){
                    if(country.code == userDataFactory.getUserCountry()){
                        var index = countryChoices.indexOf(country);
                        $scope.userDataCountry = countryChoices[index];
                    }
                });
            }else{
                $scope.userDataCountry = countryChoices[0];
            }
        });
    };

    $scope.emailRegexp = /^[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,4}$/;

    $scope.initNewVoucherPrice = function(json){
        var newVoucherPrice = angular.fromJson(json);
        if(typeof newVoucherPrice.basePrice === typeof undefined){}else{
            priceFactory.setBasePrice(newVoucherPrice.basePrice);
        }
        if(typeof newVoucherPrice.creditCardCharge === typeof undefined){}else{
            priceFactory.setCreditCardCharge(newVoucherPrice.creditCardCharge);
        }
        if(typeof newVoucherPrice.charityCharge === typeof undefined){}else{
            priceFactory.setCharityCharge(newVoucherPrice.charityCharge);
        }
    };
    $scope.totalPrice = function(){
        return parseFloat(priceFactory.getPrice().total());
    };
    $scope.basePrice = function(){
        return priceFactory.getPrice().basePrice;
    };
    $scope.creditCardCharge = function(){
        var price = priceFactory.getPrice().creditCardCharge;
        if(price){ return price; }

        return false;
    };
    $scope.charityCharge = function(){
        var price = priceFactory.getPrice().charityCharge;
        if(price){ return price; }

        return false;
    };
/*     ####     */

    $scope.initFilterVoucherValue = function(voucherValue){
        filterFactory.initVouchers();

        var voucherValueChoices = filterFactory.getVoucherValueChoices();
        $scope.voucherValueChoices = voucherValueChoices;
        angular.forEach(voucherValueChoices, function(choice){
            if(choice.id == voucherValue){
                filterFactory.setVoucherValue(choice);
            }
        });

        $scope.voucherValue = filterFactory.getVoucherValue();
    };

    $scope.voucherValueChoices = [];
    $scope.voucherValue = false;
    $scope.setVoucherValue = function(voucherValue){
        filterFactory.setVoucherValue(voucherValue);
        formApply();
    };

    $scope.voucherName = '';
    $scope.setVoucherName = function(voucherName){
        filterFactory.setVoucherName(voucherName);
        //formApply();
    };

    /*
     User change methods
     */
    $scope.changeUserFirstName = function(firstName){
        userDataFactory.setUserFirstName(firstName);
        userDataApply();
    };
    $scope.changeUserLastName = function(lastName){
        userDataFactory.setUserLastName(lastName);
        userDataApply();
    };
    $scope.changeUserEmail = function(email){
        userDataFactory.setUserEmail(email);
        userDataApply();
    };
    $scope.changeUserPhone = function(phone){
        userDataFactory.setUserPhone(phone);
        userDataApply();
    };
    $scope.changeUserAddress = function(address){
        userDataFactory.setUserAddress(address);
        userDataApply();
    };
    $scope.changeUserCity = function(city){
        userDataFactory.setUserCity(city);
        userDataApply();
    };
    $scope.changeUserPostcode = function(postcode){
        userDataFactory.setUserPostcode(postcode);
        userDataApply();
    };
    $scope.countryChoices = [];
    $scope.userDataCountry = false;
    $scope.changeUserCountry = function(country){
        $scope.userDataCountry = country;
        userDataFactory.setUserCountry(country.code);

        userDataApply();
    };

    var userDataApply = function(){
        $scope.userData = userDataFactory.getData();
    };

    var formApply = function(){
        $scope.voucherValue = filterFactory.getVoucherValue();

        var httpData = {
            voucherValue: filterFactory.getVoucherValueTotal(),
            isVouchersOffer: $isVouchersOffer
        };

        apiService.getPrice(httpData, function(result){
            if(typeof result.basePrice === typeof undefined){}else{
                priceFactory.setBasePrice(result.basePrice);
            }
            if(typeof result.creditCardCharge === typeof undefined){}else{
                priceFactory.setCreditCardCharge(result.creditCardCharge);
            }
            if(typeof result.charityCharge === typeof undefined){}else{
                priceFactory.setCharityCharge(result.charityCharge);
            }
        });
    };

    //$scope.formApply = formApply;

    $scope.bookVoucher = function(){
        var httpData = {
            voucherValue: filterFactory.getVoucherValueTotal(),
            voucherName: filterFactory.getVoucherName(),
            userData: userDataFactory.getData(),
            price: priceFactory.getPrice()
        };
        apiService.createVoucher(httpData, function(result){
            if(result){
                $window.location.href = Routing.generate('utt_voucher_confirm', {}, true);
            }
        });
    };


    $scope.isFormValid = function(){
        if($scope.voucherForm.$valid == true && $scope.termsAgreed){
            return true;
        }
        return false;
    };
}]);

angular.bootstrap(document.getElementById("voucherAppHandler"),["voucherApp"]);