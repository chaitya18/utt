var reservationApp = angular.module('reservationApp', []);

reservationApp.factory('discountPriceFactory', function() {
    var discountCodePrice = 0;
    var discountCode = '';

    var setDiscountCodePrice = function(price){ discountCodePrice = price; };
    var getDiscountCodePrice = function(){ return discountCodePrice; };

    var setDiscountCode = function(discountCodeItem){ discountCode = discountCodeItem; };
    var getDiscountCode = function(){ return discountCode; };

    return {
        setDiscountCodePrice: setDiscountCodePrice,
        getDiscountCodePrice: getDiscountCodePrice,
        setDiscountCode: setDiscountCode,
        getDiscountCode: getDiscountCode,

        getPrice: function(){
            return {
                discountCodePrice: getDiscountCodePrice(),
                discountCode: getDiscountCode(),
                total: function(){
                    return parseFloat(getDiscountCodePrice());
                }
            }
        }
    }
});

reservationApp.factory('priceFactory', function() {
    var basePrice = 0;
    var additionalSleepsPrice = 0;
    var adminCharge = 0;
    var creditCardCharge = 0;
    var bookingProtectCharge = 0;
    var charityCharge = 0;
    var petsPrice = 0;

    var setBasePrice = function(price){ basePrice = price; };
    var getBasePrice = function(){ return basePrice; };
    var setAdditionalSleepsPrice = function(price) { additionalSleepsPrice = price; };
    var getAdditionalSleepsPrice = function(){ return additionalSleepsPrice; };
    var setAdminCharge = function(price) { adminCharge = price; };
    var getAdminCharge = function(){ return adminCharge; };
    var setCreditCardCharge = function(price) { creditCardCharge = price; };
    var getCreditCardCharge = function(){ return creditCardCharge; };
    var setBookingProtectCharge = function(price) { bookingProtectCharge = price; };
    var getBookingProtectCharge = function(){ return bookingProtectCharge; };
    var setCharityCharge = function(price) { charityCharge = price; };
    var getCharityCharge = function(){ return charityCharge; };
    var setPetsPrice = function(price){ petsPrice = price; };
    var getPetsPrice = function(){ return petsPrice; };

    return {
        setBasePrice: setBasePrice,
        getBasePrice: getBasePrice,
        setAdditionalSleepsPrice: setAdditionalSleepsPrice,
        getAdditionalSleepsPrice: getAdditionalSleepsPrice,
        setAdminCharge: setAdminCharge,
        getAdminCharge: getAdminCharge,
        setCreditCardCharge: setCreditCardCharge,
        getCreditCardCharge: getCreditCardCharge,
        setBookingProtectCharge: setBookingProtectCharge,
        getBookingProtectCharge: getBookingProtectCharge,
        setCharityCharge: setCharityCharge,
        getCharityCharge: getCharityCharge,
        setPetsPrice: setPetsPrice,
        getPetsPrice: getPetsPrice,

        getPrice: function(){
            return {
                basePrice: getBasePrice(),
                additionalSleepsPrice: getAdditionalSleepsPrice(),
                adminCharge: getAdminCharge(),
                creditCardCharge: getCreditCardCharge(),
                bookingProtectCharge: getBookingProtectCharge(),
                charityCharge: getCharityCharge(),
                petsPrice: getPetsPrice(),
                total: function(){
                    return parseFloat(getBasePrice()) + parseFloat(getAdditionalSleepsPrice()) + parseFloat(getAdminCharge()) + parseFloat(getCreditCardCharge()) + parseFloat(getBookingProtectCharge()) + parseFloat(getCharityCharge()) + parseFloat(getPetsPrice());
                }
            }
        }
    }
});

reservationApp.factory('filterFactory', function() {
    var maxSleeps = false;
    var maxPets = false;
    var maxInfants = false;

    var sleepsAdultsChoices = [];
    var sleepsChildrenChoices = [];
    var petsChoices = [];
    var infantsChoices = [];
    var getSleepsAdultsChoices = function(){ return sleepsAdultsChoices; };
    var getSleepsChildrenChoices = function(){ return sleepsChildrenChoices; };
    var getPetsChoices = function(){ return petsChoices; };
    var getInfantsChoices = function(){ return infantsChoices; };

    var sleepsAdults = false;
    var sleepsChildren = false;
    var pets = false;
    var infants = false;
    var code = '';
    var setSleepsAdults = function(sleepsAdultsItem){ sleepsAdults = sleepsAdultsItem; };
    var getSleepsAdults = function(){ return sleepsAdults; };
    var setSleepsChildren = function(sleepsChildrenItem){ sleepsChildren = sleepsChildrenItem; };
    var getSleepsChildren = function(){ return sleepsChildren; };
    var setPets = function(petsItem){ pets = petsItem; };
    var getPets = function() { return pets; };
    var setInfants = function(infantsItem){ infants = infantsItem; };
    var getInfants = function() { return infants; };
    var setCode = function(codeItem){ code = codeItem; };
    var getCode = function() { return code; };
    var getMaxSleeps = function(){ return maxSleeps; };

    var initMaxSleeps = function(maxSleepsItem){
        maxSleeps = maxSleepsItem;
    };

    var initSleepsAdults = function(maxSleepsAdults){
        sleepsAdultsChoices = [];
        for(var i=0; i<=maxSleepsAdults; i++){
            sleepsAdultsChoices.push({ 'id': i });
        }
    };

    var initSleepsChildren = function(maxSleepsChildren){
        sleepsChildrenChoices = [];
        for(var i=0; i<=maxSleepsChildren; i++){
            sleepsChildrenChoices.push({ 'id': i });
        }
    };

    var initPets = function(pets){
        maxPets = pets;

        petsChoices = [];
        for(var i=0; i<=maxPets; i++){
            petsChoices.push({ 'id': i });
        }
    };

    var initInfants = function(infants){
        maxInfants = infants;

        infantsChoices = [];
        for(var i=0; i<=maxInfants; i++){
            infantsChoices.push({ 'id': i });
        }
    };
    return {
        setSleepsAdults: setSleepsAdults,
        getSleepsAdults: getSleepsAdults,
        setSleepsChildren: setSleepsChildren,
        getSleepsChildren: getSleepsChildren,
        getPets: getPets,
        setPets: setPets,
        getInfants: getInfants,
        setInfants: setInfants,
        getCode: getCode,
        setCode: setCode,
        getMaxSleeps: getMaxSleeps,

        getSleepsTotal: function(){
            return parseInt(getSleepsAdults().id) + parseInt(getSleepsChildren().id);
        },
        getSleepsAdultsTotal: function(){
            return getSleepsAdults().id;
        },
        getSleepsChildrenTotal: function(){
            return getSleepsChildren().id;
        },
        getPetsTotal: function(){
            return getPets().id;
        },
        getInfantsTotal: function(){
            return getInfants().id;
        },

        getSleepsAdultsChoices: getSleepsAdultsChoices,
        getSleepsChildrenChoices: getSleepsChildrenChoices,
        getPetsChoices: getPetsChoices,
        getInfantsChoices: getInfantsChoices,
        initMaxSleeps: initMaxSleeps,
        initSleepsAdults: initSleepsAdults,
        initSleepsChildren: initSleepsChildren,
        initPets: initPets,
        initInfants: initInfants
    }
});

reservationApp.factory('apiService', function($http) {
    $http.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';
    $http.defaults.headers.post["Content-Type"] = "application/x-www-form-urlencoded";

    return {
        getPrice: function(params, callback){
            $http({
                method: 'POST',
                url: Routing.generate('utt_reservation_price'),
                data: $.param(params),
                headers: {'Content-Type':'application/x-www-form-urlencoded; charset=UTF-8'}
            })
                .success(function(data, status, headers, config) {
                    var dataOb = angular.fromJson(data);
                    if(!(typeof dataOb.success === typeof undefined) && dataOb.success == true && !(typeof dataOb.newReservationPrice === typeof undefined)){
                        if(!(typeof dataOb.newReservationDiscountPrice === typeof undefined)){
                            callback(dataOb.newReservationPrice, dataOb.newReservationDiscountPrice);
                        }else{
                            callback(dataOb.newReservationPrice);
                        }
                    }else{
                        callback(false);
                    }
                })
                .error(function(data, status, headers, config) {
                    callback(false);
                });
        },
        createReservation: function(params, callback){
            var ts = new Date().getTime();
            $http({
                method: 'POST',
                url: Routing.generate('utt_reservation_create'),
                data: $.param(params),
                headers: {'Content-Type':'application/x-www-form-urlencoded; charset=UTF-8'}
            })
                .success(function(data, status, headers, config) {
                    var dataOb = angular.fromJson(data);
                    if(!(typeof dataOb.success === typeof undefined) && dataOb.success == true){
                        callback(true);
                    }else{
                        callback(false);
                    }
                })
                .error(function(data, status, headers, config) {
                    callback(false);
                });
        },
        getCountries: function(callback){
            $http({
                method: 'POST',
                url: Routing.generate('utt_api_all_countries'),
                headers: {'Content-Type':'application/x-www-form-urlencoded; charset=UTF-8'}
            })
                .success(function(data, status, headers, config) {
                    var dataOb = angular.fromJson(data);
                    if(!(typeof dataOb.success === typeof undefined) && dataOb.success == true && !(typeof dataOb.countries === typeof undefined)){
                        callback(dataOb.countries);
                    }else{
                        callback(false);
                    }
                })
                .error(function(data, status, headers, config) {
                    callback(false);
                });
        }
    }
});

reservationApp.factory('userDataFactory', function($http) {
    var countryChoices = [];
    var setCountryChoices = function(countries){ countryChoices = countries; };
    var getCountryChoices = function(){ return countryChoices; };

    var userData = {
        firstName: '',
        lastName: '',
        email: '',
        phone: '',
        address: '',
        city: '',
        postcode: '',
        country: ''
    };

    var setUserFirstName = function(data) { userData.firstName = data; };
    var getUserFirstName = function() { return userData.firstName; };

    var setUserLastName = function(data) { userData.lastName = data; };
    var getUserLastName = function() { return userData.lastName; };

    var setUserEmail = function(data) { userData.email = data; };
    var getUserEmail = function() { return userData.email; };

    var setUserPhone = function(data) { userData.phone = data; };
    var getUserPhone = function() { return userData.phone; };

    var setUserAddress = function(data) { userData.address = data; };
    var getUserAddress = function() { return userData.address; };

    var setUserCity = function(data) { userData.city = data; };
    var getUserCity = function() { return userData.city; };

    var setUserPostcode = function(data) { userData.postcode = data; };
    var getUserPostcode = function() { return userData.postcode; };

    var setUserCountry = function(data) { userData.country = data; };
    var getUserCountry = function() { return userData.country; };

    return {
        setCountryChoices: setCountryChoices,
        getCountryChoices: getCountryChoices,
        setUserFirstName: setUserFirstName,
        getUserFirstName: getUserFirstName,
        setUserLastName: setUserLastName,
        getUserLastName: getUserLastName,
        setUserEmail: setUserEmail,
        getUserEmail: getUserEmail,
        setUserPhone: setUserPhone,
        getUserPhone: getUserPhone,
        setUserAddress: setUserAddress,
        getUserAddress: getUserAddress,
        setUserCity: setUserCity,
        getUserCity: getUserCity,
        setUserPostcode: setUserPostcode,
        getUserPostcode: getUserPostcode,
        setUserCountry: setUserCountry,
        getUserCountry: getUserCountry,

        setData: function(data){
            if(typeof data.firstName === typeof undefined){}else{
                setUserFirstName(data.firstName);
            }
            if(typeof data.lastName === typeof undefined){}else{
                setUserLastName(data.lastName);
            }
            if(typeof data.email === typeof undefined){}else{
                setUserEmail(data.email);
            }
            if(typeof data.phone === typeof undefined){}else{
                setUserPhone(data.phone);
            }
            if(typeof data.address === typeof undefined){}else{
                setUserAddress(data.address);
            }
            if(typeof data.city === typeof undefined){}else{
                setUserCity(data.city);
            }
            if(typeof data.postcode === typeof undefined){}else{
                setUserPostcode(data.postcode);
            }
            if(typeof data.country === typeof undefined){}else{
                setUserCountry(data.country);
            }
        },
        getData: function(){
            return userData;
        }
    }
});

reservationApp.controller('reservationAppCtrl', ['$scope', '$window', 'filterFactory', 'priceFactory', 'userDataFactory', 'apiService', 'discountPriceFactory', function($scope, $window, filterFactory, priceFactory, userDataFactory, apiService, discountPriceFactory){
    $scope.initUserData = function(json){
        var userData = angular.fromJson(json);
        userDataFactory.setData(userData);

        userDataApply();
    };

    $scope.initCountryChoices = function(){
        apiService.getCountries(function(countries){
            var countryChoices = countries;
            userDataFactory.setCountryChoices(countryChoices);

            $scope.countryChoices = countryChoices;
            if(userDataFactory.getUserCountry()){
                angular.forEach(countryChoices, function(country){
                    if(country.code == userDataFactory.getUserCountry()){
                        var index = countryChoices.indexOf(country);
                        $scope.userDataCountry = countryChoices[index];
                    }
                });
            }else{
                $scope.userDataCountry = countryChoices[0];
            }
        });
    };

    $scope.emailRegexp = /^[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,4}$/;

    $scope.initNewReservationPrice = function(json){
        var newReservationPrice = angular.fromJson(json);
        if(typeof newReservationPrice.basePrice === typeof undefined){}else{
            priceFactory.setBasePrice(newReservationPrice.basePrice);
        }
        if(typeof newReservationPrice.additionalSleepsPrice === typeof undefined){}else{
            priceFactory.setAdditionalSleepsPrice(newReservationPrice.additionalSleepsPrice);
        }
        if(typeof newReservationPrice.adminCharge === typeof undefined){}else{
            priceFactory.setAdminCharge(newReservationPrice.adminCharge);
        }
        if(typeof newReservationPrice.creditCardCharge === typeof undefined){}else{
            priceFactory.setCreditCardCharge(newReservationPrice.creditCardCharge);
        }
        if(typeof newReservationPrice.bookingProtectCharge === typeof undefined){}else{
            priceFactory.setBookingProtectCharge(newReservationPrice.bookingProtectCharge);
        }
        if(typeof newReservationPrice.charityCharge === typeof undefined){}else{
            priceFactory.setCharityCharge(newReservationPrice.charityCharge);
        }
        if(typeof newReservationPrice.petsPrice === typeof undefined){}else{
            priceFactory.setPetsPrice(newReservationPrice.petsPrice);
        }
    };
    $scope.totalPrice = function(){
        return parseFloat(priceFactory.getPrice().total()) - parseFloat(discountPriceFactory.getPrice().total());
    };
    $scope.basePrice = function(){
        return priceFactory.getPrice().basePrice;
    };
    $scope.additionalSleepsPrice = function(){
        var price = priceFactory.getPrice().additionalSleepsPrice;
        if(price){ return price.toFixed(2); }

        return false;
    };
    $scope.adminCharge = function(){
        var price = priceFactory.getPrice().adminCharge;
        if(price){ return price; }

        return false;
    };
    $scope.creditCardCharge = function(){
        var price = priceFactory.getPrice().creditCardCharge;
        if(price){ return price; }

        return false;
    };
    $scope.bookingProtectCharge = function(){
        var price = priceFactory.getPrice().bookingProtectCharge;
        if(price){ return price; }

        return false;
    };
    $scope.charityCharge = function(){
        var price = priceFactory.getPrice().charityCharge;
        if(price){ return price; }

        return false;
    };
    $scope.petsPrice = function(){
        var price = priceFactory.getPrice().petsPrice;
        if(price){ return price; }

        return false;
    };
    $scope.discountTotal = function(){
        var price = discountPriceFactory.getPrice().total();
        if(price){ return price; }

        return false;
    };

/*     ####     */

    $scope.initFilterSleeps = function(maxSleeps){
        filterFactory.initMaxSleeps(maxSleeps);
        $scope.initFilterSleepsAdults(parseInt($sleeps), maxSleeps);

        if(parseInt($sleeps) == maxSleeps){
            $scope.initFilterSleepsChildren(0, 0);
        }else{
            $scope.initFilterSleepsChildren(0, maxSleeps);
        }
    };

    $scope.initFilterSleepsAdults = function(adults, maxAdults){
        if(adults > maxAdults){ adults = 0; }
        filterFactory.initSleepsAdults(maxAdults);

        var sleepsAdultsChoices = filterFactory.getSleepsAdultsChoices();
        $scope.sleepsAdultsChoices = sleepsAdultsChoices;
        angular.forEach(sleepsAdultsChoices, function(choice){
            if(choice.id == adults){
                filterFactory.setSleepsAdults(choice);
            }
        });

        $scope.sleepsAdults = filterFactory.getSleepsAdults();
    };

    $scope.initFilterSleepsChildren = function(children, maxChildren){
        if(children > maxChildren){ children = 0; }
        filterFactory.initSleepsChildren(maxChildren);

        var sleepsChildrenChoices = filterFactory.getSleepsChildrenChoices();
        $scope.sleepsChildrenChoices = sleepsChildrenChoices;
        angular.forEach(sleepsChildrenChoices, function(choice){
            if(choice.id == children){
                filterFactory.setSleepsChildren(choice);
            }
        });

        $scope.sleepsChildren = filterFactory.getSleepsChildren();
    };

    $scope.isFilterPetsVisible = true;
    $scope.initFilterPets = function(maxPets){
        if(maxPets > 0){
            $scope.isFilterPetsVisible = true;
        }else{
            $scope.isFilterPetsVisible = false;
        }
        filterFactory.initPets(maxPets);

        var petsChoices = filterFactory.getPetsChoices();
        $scope.petsChoices = petsChoices;
        filterFactory.setPets(petsChoices[0]);

        $scope.pets = filterFactory.getPets();
    };

    $scope.initFilterInfants = function(maxInfants){
        filterFactory.initInfants(maxInfants);

        var infantsChoices = filterFactory.getInfantsChoices();
        $scope.infantsChoices = infantsChoices;
        filterFactory.setInfants(infantsChoices[0]);

        $scope.infants = filterFactory.getInfants();
    };

    $scope.sleepsAdultsChoices = [];
    $scope.sleepsChildrenChoices = [];
    $scope.petsChoices = [];
    $scope.infantsChoices = [];

    $scope.sleepsAdults = false;
    $scope.sleepsChildren = false;
    $scope.pets = false;
    $scope.infants = false;
    $scope.code = '';
    $scope.isBookingProtect = true;

    $scope.isBookingProtectAllowed = function(){
        return priceFactory.getPrice().basePrice <= 1500;
    };

    var resolveBookingProtectValue = function(){
        if($scope.isBookingProtectAllowed() && $scope.isBookingProtect){
            return 1;
        }

        return 0;
    };

    $scope.setSleepsAdults = function(sleepsAdults){
        filterFactory.setSleepsAdults(sleepsAdults);

        var maxChildren = filterFactory.getMaxSleeps() - filterFactory.getSleepsAdultsTotal();
        var children = filterFactory.getSleepsChildrenTotal();

        $scope.initFilterSleepsChildren(children, maxChildren);
        formApply();
    };

    $scope.setSleepsChildren = function(sleepsChildren){
        filterFactory.setSleepsChildren(sleepsChildren);

        var maxAdults = filterFactory.getMaxSleeps() - filterFactory.getSleepsChildrenTotal();
        var adults = filterFactory.getSleepsAdultsTotal();

        $scope.initFilterSleepsAdults(adults, maxAdults);
        formApply();
    };

    $scope.setPets = function(pets){
        filterFactory.setPets(pets);
        formApply();
    };

    $scope.setInfants = function(infants){
        filterFactory.setInfants(infants);
        formApply();
    };

    $scope.setCode = function(code){
        filterFactory.setCode(code);
    };

    $scope.changeIsBookingProtect =  function(){
        formApply();
    };

    /*
     User change methods
     */
    $scope.changeUserFirstName = function(firstName){
        userDataFactory.setUserFirstName(firstName);
        userDataApply();
    };
    $scope.changeUserLastName = function(lastName){
        userDataFactory.setUserLastName(lastName);
        userDataApply();
    };
    $scope.changeUserEmail = function(email){
        userDataFactory.setUserEmail(email);
        userDataApply();
    };
    $scope.changeUserPhone = function(phone){
        userDataFactory.setUserPhone(phone);
        userDataApply();
    };
    $scope.changeUserAddress = function(address){
        userDataFactory.setUserAddress(address);
        userDataApply();
    };
    $scope.changeUserCity = function(city){
        userDataFactory.setUserCity(city);
        userDataApply();
    };
    $scope.changeUserPostcode = function(postcode){
        userDataFactory.setUserPostcode(postcode);
        userDataApply();
    };
    $scope.countryChoices = [];
    $scope.userDataCountry = false;
    $scope.changeUserCountry = function(country){
        $scope.userDataCountry = country;
        userDataFactory.setUserCountry(country.code);

        userDataApply();
    };

    var userDataApply = function(){
        $scope.userData = userDataFactory.getData();
    };

    var formApply = function(){
        $scope.termsAgreed = false;

        $scope.sleepsAdults = filterFactory.getSleepsAdults();
        $scope.sleepsChildren = filterFactory.getSleepsChildren();
        $scope.pets = filterFactory.getPets();
        $scope.infants = filterFactory.getInfants();
        $scope.code = filterFactory.getCode();

        var httpData = {
            estateShortName: $scope.estateShortName,
            fromDate: $scope.fromDate,
            toDate: $scope.toDate,
            sleeps: filterFactory.getSleepsTotal(),
            pets: filterFactory.getPetsTotal(),
            infants: filterFactory.getInfantsTotal(),
            code: filterFactory.getCode(),
            isBookingProtect: resolveBookingProtectValue()
        };

        apiService.getPrice(httpData, function(result, resultDiscount){
            if(typeof result.basePrice === typeof undefined){}else{
                priceFactory.setBasePrice(result.basePrice);
            }
            if(typeof result.additionalSleepsPrice === typeof undefined){}else{
                priceFactory.setAdditionalSleepsPrice(result.additionalSleepsPrice);
            }
            if(typeof result.adminCharge === typeof undefined){}else{
                priceFactory.setAdminCharge(result.adminCharge);
            }
            if(typeof result.creditCardCharge === typeof undefined){}else{
                priceFactory.setCreditCardCharge(result.creditCardCharge);
            }
            if(typeof result.bookingProtectCharge === typeof undefined){}else{
                priceFactory.setBookingProtectCharge(result.bookingProtectCharge);
            }
            if(typeof result.charityCharge === typeof undefined){}else{
                priceFactory.setCharityCharge(result.charityCharge);
            }
            if(typeof result.petsPrice === typeof undefined){}else{
                priceFactory.setPetsPrice(result.petsPrice);
            }

            if(typeof resultDiscount === typeof undefined){}else{
                if(typeof resultDiscount.discountCodePrice === typeof undefined){}else{
                    discountPriceFactory.setDiscountCodePrice(resultDiscount.discountCodePrice);
                }
                if(typeof resultDiscount.discountCode === typeof undefined){}else{
                    discountPriceFactory.setDiscountCode(resultDiscount.discountCode);
                }
            }
        });
    };

    $scope.formApply = formApply;

    $scope.bookHoliday = function(){
        var httpData = {
            estateShortName: $scope.estateShortName,
            fromDate: $scope.fromDate,
            toDate: $scope.toDate,
            sleeps: filterFactory.getSleepsTotal(),
            sleepsChildren: filterFactory.getSleepsChildrenTotal(),
            pets: filterFactory.getPetsTotal(),
            infants: filterFactory.getInfantsTotal(),
            code: discountPriceFactory.getDiscountCode(),//filterFactory.getCode(),
            userData: userDataFactory.getData(),
            price: priceFactory.getPrice(),
            discountPrice: discountPriceFactory.getPrice(),
            isBookingProtect: resolveBookingProtectValue(),
            timeStamp: new Date().getTime()
        };
        apiService.createReservation(httpData, function(result){
            if(result){
                $window.location.href = Routing.generate('utt_reservation_confirm', {}, true);
            }
        });
    };


    $scope.isFormValid = function(){
        if($scope.reservationForm.$valid == true && $scope.termsAgreed){
            return true;
        }
        return false;
    };

    $scope.isMobile = function(){
        if(window.innerWidth <= 480){
            return true;
        }else{
            return false;
        }
    };

    /*
     TODO

     Move it to filterService
      */

    $scope.estateShortName = '';
    $scope.setEstateShortName = function(estateShortName){
        $scope.estateShortName = estateShortName;
    };
    $scope.fromDate = false;
    $scope.setFromDate = function(fromDate){
        $scope.fromDate = fromDate;
    };
    $scope.toDate = false;
    $scope.setToDate = function(toDate){
        $scope.toDate = toDate;
    };
}]);

angular.bootstrap(document.getElementById("reservationAppHandler"),["reservationApp"]);
