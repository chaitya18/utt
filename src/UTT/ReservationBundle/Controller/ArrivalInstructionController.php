<?php

namespace UTT\ReservationBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Session\Session;
use UTT\ReservationBundle\Entity\Reservation;
use UTT\ReservationBundle\Service\ArrivalInstructionService;
use UTT\ReservationBundle\Entity\ArrivalInstruction;
use UTT\ReservationBundle\Entity\ArrivalInstructionRepository;
use Symfony\Component\HttpFoundation\Request;
use UTT\ReservationBundle\Entity\ReservationRepository;

class ArrivalInstructionController extends Controller
{
    public function renderArrivalInstructionForWebAction(Reservation $reservation){
        /** @var ArrivalInstructionService $arrivalInstructionService */
        $arrivalInstructionService = $this->get('utt.arrivalinstructionservice');
        $content = $arrivalInstructionService->getArrivalInstructionContentForWeb();

        return $this->render('UTTReservationBundle:ArrivalInstruction:renderArrivalInstructionForWeb.html.twig', array(
            'content' => $content,
            'reservation' => $reservation
        ));
    }

    public function displayByReservationAndPinAction(Request $request, $reservationId, $pin){
        /** @var ReservationRepository $reservationRepository */
        $reservationRepository = $this->getDoctrine()->getManager()->getRepository('UTTReservationBundle:Reservation');
        /** @var ArrivalInstructionService $arrivalInstructionService */
        $arrivalInstructionService = $this->get('utt.arrivalinstructionservice');
        /** @var Reservation $reservation */
        $reservation = $reservationRepository->findOneBy(array(
            'id' => $reservationId,
            'pin' => $pin
        ));

        if($reservation instanceof Reservation){
            $arrivalInstruction = $arrivalInstructionService->findOrCreate($reservation);
            if($arrivalInstruction instanceof ArrivalInstruction){
                $file = $arrivalInstruction->getUploadDir().'/'.$arrivalInstruction->getFilename();

                header('Content-type: application/pdf');
                header('Content-Disposition: inline; filename="' . $arrivalInstruction->getFilename() . '"');
                header('Content-Transfer-Encoding: binary');
                header('Accept-Ranges: bytes');
                @readfile($file);
                exit;
            }
        }

        return $this->redirect($this->generateUrl('utt_index_homepage'));
    }
}
