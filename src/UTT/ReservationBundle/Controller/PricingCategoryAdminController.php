<?php

namespace UTT\ReservationBundle\Controller;

use Sonata\AdminBundle\Controller\CRUDController as Controller;
use UTT\ReservationBundle\Entity\PricingCategory;
use UTT\ReservationBundle\Entity\PricingCategoryRepository;
use UTT\ReservationBundle\Service\PricingCategoryService;

class PricingCategoryAdminController extends Controller
{

    public function editAction($id = null){
        /** @var PricingCategory $object */
        $object = $this->admin->getSubject();
        if($object){
            $request = $this->getRequest();
            if($request->isMethod('POST') && $request->query->has('uniqid')){
                $uniqid = $request->query->get('uniqid');
                if($request->request->has($uniqid)){
                    $postData =  $request->request->get($uniqid);
                    if(isset($postData['generatePricesYear']) && isset($postData['generatePricesRun']) && $postData['generatePricesRun'] == true && $postData['generatePricesYear']){
                        $importFromPricingCategory = null;
                        $generatePricesOption = null;

                        /** @var PricingCategoryService $pricingCategoryService */
                        $pricingCategoryService = $this->get('utt.pricingcategoryservice');

                        /** @var PricingCategoryRepository $pricingCategoryRepository */
                        $pricingCategoryRepository = $this->getDoctrine()->getManager()->getRepository('UTTReservationBundle:PricingCategory');
                        if(isset($postData['generatePricesImportFrom']) && $postData['generatePricesImportFrom']){
                            /** @var PricingCategory $importFromPricingCategory */
                            $importFromPricingCategory = $pricingCategoryRepository->find($postData['generatePricesImportFrom']);
                        }
                        if(isset($postData['generatePricesOption']) && $postData['generatePricesOption']){
                            $generatePricesOption = $postData['generatePricesOption'];
                        }

                        if($object->getType() == PricingCategory::TYPE_FLEXIBLE_BOOKING){
                            if($importFromPricingCategory instanceOf PricingCategory){
                                $pricingCategoryService->importPricesForFlexible($object, $postData['generatePricesYear'], $importFromPricingCategory, $generatePricesOption);
                            }else{
                                $pricingCategoryService->generateNewPricesForFlexible($object, $postData['generatePricesYear'], $generatePricesOption);
                            }
                        }elseif($object->getType() == PricingCategory::TYPE_STANDARD_M_F){
                            if($importFromPricingCategory instanceOf PricingCategory){
                                $pricingCategoryService->importPricesForStandard($object, $postData['generatePricesYear'], $importFromPricingCategory, $generatePricesOption);
                            }else{
                                $pricingCategoryService->generateNewPricesForStandard($object, $postData['generatePricesYear'], $generatePricesOption);
                            }
                        }

                        return $this->redirect($this->admin->generateObjectUrl('edit', $object));
                    }
                }
            }
        }

        return parent::editAction($id);
    }

}