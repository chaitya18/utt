<?php

namespace UTT\ReservationBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use UTT\ReservationBundle\Entity\PaymentTransaction;
use UTT\ReservationBundle\Entity\Reservation;
use UTT\ReservationBundle\Entity\Voucher;
use UTT\ReservationBundle\Entity\VoucherPaymentTransaction;
use UTT\ReservationBundle\Payment\Price;
use UTT\ReservationBundle\Payment\PriceFactory;
use UTT\ReservationBundle\Payment\TransactionService;
use UTT\ReservationBundle\Payment\Worldpay\Order;
use UTT\ReservationBundle\Payment\Worldpay\OrderFactory;
use UTT\ReservationBundle\Payment\Worldpay\PaymentTransactionFactory;
use UTT\ReservationBundle\Payment\Worldpay\Result;
use UTT\ReservationBundle\Payment\Worldpay\TransactionPaymentFactory;
use UTT\ReservationBundle\Payment\Worldpay\WorldPayConfig;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;



class PaymentVoucherController extends Controller
{



    public function renderPayFormAction(Voucher $voucher, $charityCharge = 0){

        $twigArray = array();

        /** @var WorldPayConfig $worldPayConfig */
        $worldPayConfig = $this->get('utt.worldpay.config');
        /** @var OrderFactory $orderFactory */
        $orderFactory = $this->get('utt.worldpay.order.factory');
        /** @var PriceFactory $priceFactory */
        $priceFactory = $this->get('utt.payment.price.factory');



        if($voucher->getPaid() < $voucher->getTotalPrice()){

            $price = $priceFactory->createFromVoucher($voucher, $charityCharge);

            $order = $orderFactory->createFromVoucher($voucher, $price);

            $worldPayConfig->onTestMode();

            $twigArray['payUrl'] = $worldPayConfig->getUrl();
            $twigArray['returnUrl'] = $this->get('request')->getSchemeAndHttpHost().$this->generateUrl('utt_voucher_payment_status',
                    array('cartId' => $order->getCartId()));
            $twigArray['callbackUrl'] = $this->get('request')->getSchemeAndHttpHost().$this->generateUrl('utt_voucher_payment_wd_result');
            $twigArray['order'] = $order;

            $this->saveVoucherTransaction($voucher, $order);

        }

        return $this->render('UTTReservationBundle:Reservation:renderWorldPayForm.html.twig', $twigArray);

    }

    /**
     * @param Voucher $voucher
     * @param Order $order
     */
    protected function saveVoucherTransaction(Voucher $voucher, Order $order)
    {
        $price = $order->getPrice();
        // create transaction
        $voucherPaymentTransaction = new VoucherPaymentTransaction();
        $voucherPaymentTransaction->setStatus(VoucherPaymentTransaction::STATUS_PREPARED);
        $voucherPaymentTransaction->setVoucher($voucher);
        $voucherPaymentTransaction->setCode($order->getCartId());
        $voucherPaymentTransaction->setAmount($price->getTotal());
        $voucherPaymentTransaction->setAmountCreditCardCharge($price->getCreditCardCharge());
        $voucherPaymentTransaction->setAmountCharityCharge($price->getCharityCharge());
        $voucherPaymentTransaction->setType(VoucherPaymentTransaction::TYPE_DEBIT_CARD);

        $_em = $this->getDoctrine()->getManager();
        $_em->persist($voucherPaymentTransaction);
        $_em->flush();
    }


    public function paymentResultAction(Request $request){

        $logger = $this->get('logger.payment');
        $query = $request->query->all();

        try {
            $paymentResult = Result::createFromRequest($request->request->all());
            //$paymentResult = Result::createFromRequest($this->getTestInputData());
            /**
             * @todo remove
             */
            $logger->notice('POST: '.serialize($request->request->all()));
            $logger->notice('GET: '.serialize($query));
            /**
             * @var $paymentWorldPayTransactionService \UTT\ReservationBundle\Payment\Worldpay\Voucher\TransactionService
             */
            $paymentWorldPayTransactionService = $this->get('utt.payment.worldpay.voucher.transaction.service');
            $paymentWorldPayTransactionService->handle($paymentResult);
        } catch (\Exception $e) {
            return $this->paymentResultErrorResponse($e, $logger);
        }

        return new Response('OK');


    }

    private function getTestInputData()
    {
        $transId = rand(91472700, 914727999);
        $data = array(
            'authAmount' => '102',
            'authCurrency' => 'GBP',
            'transId' => $transId,
            'transStatus' => 'Y',
            'transTime' => time(),
            'cardType' => 'Visa',
            'testMode' => 100,
            'rawAuthMessage' => 'cardbe.msg.authorised',
            'M_userId' => '20343',
            'email' => 'test@intssseria.pl',
            'instId' => '1424214',
            'cartId' => '184_863bb2c05ff56f17',
            'callbackPW' => 't7ADqRxq9vaurZWbj3KQa#'

        );
        return $data;
    }

    /**
     * @param \Exception $e
     * @param $logger
     * @return Response
     */
    protected function paymentResultErrorResponse(\Exception $e, $logger) {
        $logger->error($e->getMessage().' code:'.$e->getCode().' File: '.$e->getFile().':'.$e->getLine());
        $code = (int) $e->getCode();
        if($code > 300 && $code < 500){
            return new Response('Error - '.$e->getMessage(), $code);
        }
        return new Response('Error - '.$e->getMessage(), 500);
    }

    /**
     * @param null $cartId
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|Response
     */
    public function paymentStatusAction($cartId = null)
    {
        $twigArray = array();
        try {
            /**
             * @var $paymentWorldPayTransactionService \UTT\ReservationBundle\Payment\Worldpay\Voucher\TransactionService
             */
            $paymentWorldPayTransactionService = $this->get('utt.payment.worldpay.voucher.transaction.service');
            $paymentTransaction = $paymentWorldPayTransactionService->getPaymentTransaction($cartId);
            $voucher = $paymentTransaction->getVoucher();
            if($paymentTransaction->isPaymentAuthSuccess()) {
                $this->get('session')->getFlashBag()->add('success', 'Payment successful!');
            } else {
                $this->get('session')->getFlashBag()->add('error', 'An unknown error has occured. Please wait a while and then try again to make your payment.');
            }

            return $this->redirect($this->generateUrl('utt_user_voucher_manage', array(
                'voucherId' => $voucher->getId()
            )));
        } catch (\Exception $e) {
            $twigArray['statusText'] = 'An error occurred during the payment process';
            return $this->render('UTTReservationBundle:Reservation:paymentFailure.html.twig', $twigArray);
        }

    }


}
