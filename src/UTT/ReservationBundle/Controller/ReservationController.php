<?php

namespace UTT\ReservationBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use UTT\EstateBundle\Entity\Estate;
use UTT\ReservationBundle\Model\ReservationDataUserData;
use UTT\ReservationBundle\Service\ReservationService;
use Symfony\Component\HttpFoundation\JsonResponse;
use UTT\ReservationBundle\Entity\Reservation;
use UTT\ReservationBundle\Entity\ReservationRepository;
use UTT\ReservationBundle\Entity\PaymentTransaction;
use UTT\ReservationBundle\Entity\PaymentTransactionRepository;
use Symfony\Component\HttpFoundation\Session\Session;
use UTT\ReservationBundle\Model\ReservationData;
use UTT\ReservationBundle\Model\ReservationChangeData;
use UTT\UserBundle\Entity\User;
use FOS\UserBundle\Model\UserInterface;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use UTT\ReservationBundle\Service\DiscountCodeService;
use UTT\ReservationBundle\Model\ReservationDataPrice;
use UTT\ReservationBundle\Model\ReservationDataDiscountPrice;
use UTT\ReservationBundle\Factory\ReservationFactory;
use UTT\ReservationBundle\Entity\HistoryEntry;
use UTT\ReservationBundle\Service\HistoryEntryService;
use UTT\IndexBundle\Service\EmailService;
use UTT\IndexBundle\Service\ConfigurationService;
use UTT\IndexBundle\Entity\Configuration;
use UTT\UserBundle\Factory\UserFactory;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Validator\Constraints\Email as EmailConstraint;
use UTT\ReservationBundle\Payment\SmartPayFormService;
use UTT\ReservationBundle\Service\ArrivalInstructionService;
use UTT\ReservationBundle\Entity\PaymentTransactionNotification;
use UTT\AdminBundle\Service\UTTAclService;
use Monolog\Logger;
use Monolog\Handler\StreamHandler;
use UTT\ReservationBundle\Service\PricingService;
use UTT\ReservationBundle\Entity\VoucherRepository;
use UTT\ReservationBundle\Entity\Voucher;

class ReservationController extends Controller
{
    private function userAuth($throwException = true){
        if(!$this->container->get('security.context')->isGranted('IS_AUTHENTICATED_FULLY') ){
            if($throwException){
                throw new AccessDeniedException('This user does not have access to this section.');
            }else{
                return false;
            }
        }

        $user = $this->getUserFromSession();
        if(!$user && $throwException) {
            throw new AccessDeniedException('This user does not have access to this section.');
        }

        return $user;
    }


    protected function getUserFromSession() {
        $user = $this->container->get('security.context')->getToken()->getUser();
        if (!is_object($user) || !$user instanceof UserInterface) {
            return false;
        }
        return $user;
    }

    public function resendArrivalInstructionMailAction(Request $request, $reservationId, $isForced = false){
        $referer = $request->headers->get('referer');
        if(!$referer) $referer = $this->generateUrl('utt_user_reservation_manage', array(
            'reservationId' => $reservationId
        ));

        /** @var User $user */
        $user = $this->userAuth();

        /** @var ReservationRepository $reservationRepository */
        $reservationRepository = $this->getDoctrine()->getManager()->getRepository('UTTReservationBundle:Reservation');

        /** @var Reservation $reservation */
        $reservation = $reservationRepository->find($reservationId);
        if($reservation instanceof Reservation && $user instanceof User && ($reservation->isAllowedResendArrivalInstructionMail() || $isForced == true)){
            /** @var ArrivalInstructionService $arrivalInstructionService */
            $arrivalInstructionService = $this->get('utt.arrivalinstructionservice');
            $arrivalInstructionService->findOrCreate($reservation);

            /** @var EmailService $emailService */
            $emailService = $this->get('utt.emailservice');
            $sent = $emailService->sendReservationArrivalInstructionEmail($reservation);
            if($sent){
                /** @var HistoryEntryService $historyEntryService */
                $historyEntryService = $this->get('utt.historyentryservice');
                $historyEntryComment = $historyEntryService->newCommentArrivalInstructionEmail();
                $historyEntryService->newEntry(HistoryEntry::TYPE_ARRIVAL_INSTRUCTIONS, $reservation, $user, $historyEntryComment);

                $this->get('session')->getFlashBag()->add('success', 'Reservation arrival instruction sent!');
            }else{
                $this->get('session')->getFlashBag()->add('error', 'Reservation arrival instruction NOT sent!');
            }
        }else{
            $this->get('session')->getFlashBag()->add('error', 'Reservation arrival instructions NOT sent! Reservation is NOT allowed to resend arrival instructions.');
        }

        return $this->redirect($referer);
    }

    public function resendConfirmationMailAction(Request $request, $reservationId){
        $referer = $request->headers->get('referer');
        if(!$referer) $referer = $this->generateUrl('utt_user_reservation_manage', array(
            'reservationId' => $reservationId
        ));

        /** @var User $user */
        $user = $this->userAuth();

        /** @var ReservationRepository $reservationRepository */
        $reservationRepository = $this->getDoctrine()->getManager()->getRepository('UTTReservationBundle:Reservation');

        /** @var Reservation $reservation */
        $reservation = $reservationRepository->find($reservationId);
        if($reservation instanceof Reservation && $user instanceof User){
            /** @var EmailService $emailService */
            $emailService = $this->get('utt.emailservice');
            $sent = $emailService->sendReservationConfirmation($reservation);
            if($sent){
                $this->get('session')->getFlashBag()->add('success', 'Reservation confirmation sent!');
            }else{
                $this->get('session')->getFlashBag()->add('error', 'Reservation confirmation NOT sent!');
            }
        }

        return $this->redirect($referer);
    }

    public function cancelAction(Request $request, $reservationId){
        $referer = $request->headers->get('referer');
        if(!$referer) $referer = $this->generateUrl('utt_user_reservation_manage', array(
            'reservationId' => $reservationId
        ));

        /** @var User $user */
        $user = $this->userAuth();

        /** @var ReservationRepository $reservationRepository */
        $reservationRepository = $this->getDoctrine()->getManager()->getRepository('UTTReservationBundle:Reservation');

        /** @var Reservation $reservation */
        $reservation = $reservationRepository->find($reservationId);
        if($reservation instanceof Reservation && $user instanceof User){
            /** @var ReservationService $reservationService */
            $reservationService = $this->get('utt.reservationservice');
            $cancelled = $reservationService->manualCancel($reservation, $user);
            if($cancelled){
                $this->get('session')->getFlashBag()->add('success', 'Reservation cancelled!');
            }else{
                $this->get('session')->getFlashBag()->add('error', 'Reservation NOT cancelled!');
            }
        }

        return $this->redirect($referer);
    }

    public function renderSmartPayFormForReservationAction(Reservation $reservation, $applyCreditCardCharge = false, $charityCharge = 0){
        $twigArray = array();

        /** @var SmartPayFormService $smartPayFormService */
        $smartPayFormService = $this->get('utt.smartpayformservice');

        /** @var ReservationFactory $reservationFactory */
        $reservationFactory = $this->get('utt.reservationfactory');

        /** @var PricingService $pricingService */
        $pricingService = $this->get('utt.pricingservice');

        if($reservation->getPaid() < $reservation->getTotalPrice()){
            if($reservationFactory->isFullPaymentRequired($reservation)){
                $toPay = $reservation->getTotalPrice() - $reservation->getPaid();
            }else{
                if($reservation->getPaid() == 0){
                    $toPay = $reservation->getDeposit();
                }else{
                    $toPay = $reservation->getTotalPrice() - $reservation->getPaid();
                }
            }

            $creditCardCharge = 0;
            if($applyCreditCardCharge){
                $creditCardCharge = $pricingService->calculateCreditCardCharge($toPay);
                $toPay = (float)$toPay + (float)$creditCardCharge;

                $twigArray['applyCreditCardCharge'] = $applyCreditCardCharge;
                $twigArray['creditCardCharge'] = $creditCardCharge;
            }

            if($charityCharge){
                $toPay = (float)$toPay + (float)$charityCharge;

                $twigArray['charityCharge'] = $charityCharge;
            }

            $transactionCode = $smartPayFormService->generateTransactionCodeReservation($reservation);

            $twigArray['smartPayUrl'] = $smartPayFormService->getUrl();
            $twigArray['merchantReference'] = $transactionCode;
            $twigArray['paymentAmount'] = $toPay * 100;
            $twigArray['currencyCode'] = $smartPayFormService->getCurrencyCode();
            $twigArray['shipBeforeDate'] = date("Ymd" , mktime(date("H"), date("i"), date("s"), date("m"), date("j")+5, date("Y")));
            $twigArray['skinCode'] = $smartPayFormService->getSkinCodeReservation();
            $twigArray['merchantAccount'] = $smartPayFormService->getMerchantAccount();
            $twigArray['shopperLocale'] = $smartPayFormService->getShopperLocale();
            $twigArray['orderData'] = $smartPayFormService->generateOrderDataReservation($reservation);
            $twigArray['sessionValidity'] = date(DATE_ATOM	, mktime(date("H")+1, date("i"), date("s"), date("m"), date("j"), date("Y")));
            $twigArray['shopperEmail'] = $reservation->getUserDataDecoded()->email;
            $twigArray['shopperReference'] = $reservation->getUser()->getId();

            $twigArray['merchantSig'] = $smartPayFormService->generateMerchantSigReservation(
                $twigArray['paymentAmount'],
                $twigArray['shipBeforeDate'],
                $twigArray['merchantReference'],
                $twigArray['sessionValidity'],
                $twigArray['shopperEmail'],
                $twigArray['shopperReference']
            );

            // create transaction
            $paymentTransaction = new PaymentTransaction();
            $paymentTransaction->setStatus(PaymentTransaction::STATUS_PREPARED);
            $paymentTransaction->setReservation($reservation);
            $paymentTransaction->setCode($transactionCode);
            $paymentTransaction->setAmount($toPay);
            $paymentTransaction->setAmountCreditCardCharge($creditCardCharge);
            $paymentTransaction->setAmountCharityCharge($charityCharge);
            if($applyCreditCardCharge){
                $paymentTransaction->setType(PaymentTransaction::TYPE_CREDIT_CARD);
            }else{
                $paymentTransaction->setType(PaymentTransaction::TYPE_DEBIT_CARD);
            }
            $_em = $this->getDoctrine()->getManager();
            $_em->persist($paymentTransaction);
            $_em->flush();
        }

        return $this->render('UTTReservationBundle:Reservation:renderSmartPayForm.html.twig', $twigArray);
    }

    public function paymentNotificationAction(Request $request){
        $requestPostData = $request->request->all();

        $_em = $this->getDoctrine()->getManager();
        $paymentTransactionNotification = new PaymentTransactionNotification();
        $paymentTransactionNotification->setRequestPostData(serialize($requestPostData));
        $_em->persist($paymentTransactionNotification);
        $_em->flush();

        print "[accepted]";
        exit;
    }

    public function paymentResultAction(Request $request){
        /** @var Logger $logger */
        $logger = $this->get('logger');

        $authResult = $request->query->get('authResult');
        $pspReference = $request->query->get('pspReference');
        $merchantReference = $request->query->get('merchantReference');
        $merchantSig = $request->query->get('merchantSig');
        $paymentMethod = $request->query->get('paymentMethod');
        $shopperLocale = $request->query->get('shopperLocale');
        $merchantReturnData = $request->query->get('merchantReturnData');

        //$logger->notice('1 | merchantReference: '.$merchantReference.' | PAYMENT STARTED');

        if($authResult){
            /** @var SmartPayFormService $smartPayFormService */
            $smartPayFormService = $this->get('utt.smartpayformservice');
            /** @var EmailService $emailService */
            $emailService = $this->get('utt.emailservice');
            /** @var HistoryEntryService $historyEntryService */
            $historyEntryService = $this->get('utt.historyentryservice');

            $_em = $this->getDoctrine()->getManager();
            /** @var PaymentTransaction $paymentTransaction */
            $paymentTransaction = $smartPayFormService->findOrCreatePaymentTransactionByCode($merchantReference);

            $isDuplicatedPayment = false;
            if($paymentTransaction->getSmartPayPspReference() == $pspReference && $paymentTransaction->getStatus() == PaymentTransaction::STATUS_SUCCESS){
                $isDuplicatedPayment = true;
            }

            //$logger->notice('2 | merchantReference: '.$merchantReference.' | PAYMENT TRANSACTION | findOrCreatePaymentTransactionByCode');
            $paymentTransaction = $smartPayFormService->assignResponseParamsToPaymentTransaction($paymentTransaction, $authResult, $pspReference, $merchantReference, $merchantSig, $paymentMethod, $shopperLocale, $merchantReturnData);
            //$logger->notice('3 | merchantReference: '.$merchantReference.' | PAYMENT TRANSACTION | assignResponseParamsToPaymentTransaction');
            if($authResult == 'AUTHORISED'){
                //$logger->notice('4 | merchantReference: '.$merchantReference.' | AUTHORISED');
                $paymentTransaction->setStatus(PaymentTransaction::STATUS_SUCCESS);
                $_em->persist($paymentTransaction);
                $_em->flush();
                $this->get('session')->getFlashBag()->add('success', 'Payment successful!');
                //$logger->notice('5 | merchantReference: '.$merchantReference.' | PAYMENT TRANSACTION | flushed | flash added');

                /** @var Reservation $reservation */
                $reservation = $paymentTransaction->getReservation();
                //$logger->notice('6 | merchantReference: '.$merchantReference.' | RESERVATION | getReservation');
                if($reservation instanceof Reservation){
                    if($isDuplicatedPayment){
                        $historyEntryComment = $historyEntryService->newCommentCardPaymentDuplicated($paymentTransaction->getTypeName(), $paymentTransaction->getAmount(), $paymentTransaction->getSmartPayAuthResult(), $paymentTransaction->getSmartPayPaymentMethod(), $paymentTransaction->getSmartPayPspReference());
                        $historyEntryService->newEntry(HistoryEntry::TYPE_CARD_PAYMENT, $reservation, $reservation->getUser(), $historyEntryComment);

                        return $this->redirect($this->generateUrl('utt_user_reservation_manage', array(
                            'reservationId' => $reservation->getId()
                        )));
                    }

                    //$logger->notice('7 | merchantReference: '.$merchantReference.' | RESERVATION: '.$reservation->getId().' | found');
                    /** @var ReservationFactory $reservationFactory */
                    $reservationFactory = $this->get('utt.reservationfactory');
                    $reservation = $reservationFactory->addCreditCardCharge($reservation, $paymentTransaction->getAmountCreditCardCharge());
                    $reservation = $reservationFactory->addCharityCharge($reservation, $paymentTransaction->getAmountCharityCharge());
                    $reservation = $reservationFactory->addPaymentToReservation($reservation, $paymentTransaction->getAmount());
                    //$logger->notice('8 | merchantReference: '.$merchantReference.' | RESERVATION: '.$reservation->getId().' | addPaymentToReservation | '.$paymentTransaction->getAmount());

                    $_em->persist($reservation);
                    $_em->flush();
                    $this->get('session')->getFlashBag()->add('success', 'Payment amount added to reservation!');

                    $emailService->sendThankYouForPaymentEmail($reservation, $paymentTransaction);
                    if($paymentTransaction->getAmountCharityCharge() > 0){
                        $emailService->sendThankYouForCharityPaymentEmail($reservation, $paymentTransaction);
                    }

                    //$logger->notice('9 | merchantReference: '.$merchantReference.' | RESERVATION: '.$reservation->getId().' | flushed');

                    $user = $this->userAuth(false);
                    //$logger->notice('10 | merchantReference: '.$merchantReference.' | RESERVATION: '.$reservation->getId().' | userAuth');
                    if(!($user instanceof User)){
                        $user = $reservation->getUser();
                        //$logger->notice('10a | merchantReference: '.$merchantReference.' | RESERVATION: '.$reservation->getId().' | get user from reservation');
                    }
                    if($user instanceof User){
                        //$logger->notice('11 | merchantReference: '.$merchantReference.' | RESERVATION: '.$reservation->getId().' | user instanceof User');
                        // add entry to history
                        $historyEntryComment = $historyEntryService->newCommentCardPayment($reservation->getTotalPrice(), $paymentTransaction->getTypeName(), $paymentTransaction->getAmount(), $paymentTransaction->getSmartPayAuthResult(), $paymentTransaction->getSmartPayPaymentMethod(), $paymentTransaction->getSmartPayPspReference());
                        $historyEntryService->newEntry(HistoryEntry::TYPE_CARD_PAYMENT, $reservation, $user, $historyEntryComment);
                        //$logger->notice('12 | merchantReference: '.$merchantReference.' | RESERVATION: '.$reservation->getId().' | added history entry | '.$historyEntryComment);
                    }

                    $emailService->sendForLandlord($reservation, 'Booking payment');
                    //$logger->notice('13 | merchantReference: '.$merchantReference.' | RESERVATION: '.$reservation->getId().' | sent email to landlord');

                    if($reservation->getPaid() == $reservation->getTotalPrice()){
                        //$logger->notice('14 | merchantReference: '.$merchantReference.' | RESERVATION: '.$reservation->getId().' | paid == totalprice');

                        /** @var ArrivalInstructionService $arrivalInstructionService */
                        $arrivalInstructionService = $this->get('utt.arrivalinstructionservice');
                        try{
                            $arrivalInstructionService->findOrCreate($reservation);
                            //$logger->notice('15 | merchantReference: '.$merchantReference.' | RESERVATION: '.$reservation->getId().' | ARRIVAL INSTRUCTIONS | generated');
                            $sent = $emailService->sendReservationArrivalInstructionEmail($reservation);
                            //$logger->notice('16 | merchantReference: '.$merchantReference.' | RESERVATION: '.$reservation->getId().' | ARRIVAL INSTRUCTIONS | send');
                            if($sent && $user instanceof User){
                                //$logger->notice('17 | merchantReference: '.$merchantReference.' | RESERVATION: '.$reservation->getId().' | ARRIVAL INSTRUCTIONS | sent and user instance of user');

                                /** @var HistoryEntryService $historyEntryService */
                                $historyEntryService = $this->get('utt.historyentryservice');
                                $historyEntryComment = $historyEntryService->newCommentArrivalInstructionEmail();
                                $historyEntryService->newEntry(HistoryEntry::TYPE_ARRIVAL_INSTRUCTIONS, $reservation, $user, $historyEntryComment);

                                $this->get('session')->getFlashBag()->add('success', 'Reservation arrival instruction sent!');
                                //$logger->notice('18 | merchantReference: '.$merchantReference.' | RESERVATION: '.$reservation->getId().' | ARRIVAL INSTRUCTIONS | flash added');
                            }
                        }catch(\Exception $e){
                            /** @var HistoryEntryService $historyEntryService */
                            $historyEntryService = $this->get('utt.historyentryservice');
                            $historyEntryComment = "ARRIVAL INSTRUCTIONS - ERROR ".$e->getMessage();
                            $historyEntryService->newEntry(HistoryEntry::TYPE_ARRIVAL_INSTRUCTIONS, $reservation, $user, $historyEntryComment);

                            //$logger->notice('ERROR | merchantReference: '.$merchantReference.' | RESERVATION: '.$reservation->getId().' | ARRIVAL INSTRUCTIONS | ERROR');
                        }
                    }

                    return $this->redirect($this->generateUrl('utt_user_reservation_manage', array(
                        'reservationId' => $reservation->getId()
                    )));
                }
            }else{
                $twigArray = array();
                $paymentTransaction->setStatus(PaymentTransaction::STATUS_NOT_SUCCESS);
                $_em->persist($paymentTransaction);
                $_em->flush();

                switch ($authResult){
                    case 'REFUSED':
                        $twigArray['statusText'] = 'Authorisation was unsuccessful, Declined.';
                        break;
                    case 'CANCELLED':
                        $twigArray['statusText'] = 'Payment cancelled.';
                        break;
                    case 'PENDING':
                        $twigArray['statusText'] = 'Pending.';
                        break;
                    case 'ERROR':
                        $twigArray['statusText'] = 'An error occurred during the payment process';
                        break;
                    default:
                        $twigArray['statusText'] = 'An unknown error has occured. Please wait a while and then try again to make your payment.';
                        break;
                }

                /** @var Reservation $reservation */
                $reservation = $paymentTransaction->getReservation();
                if($reservation instanceof Reservation){
                    $user = $this->userAuth(false);
                    if($user){
                        // add entry to history
                        /** @var HistoryEntryService $historyEntryService */
                        $historyEntryService = $this->get('utt.historyentryservice');
                        $historyEntryComment = $historyEntryService->newCommentCardFailed($authResult);
                        $historyEntryService->newEntry(HistoryEntry::TYPE_CARD_FAILED, $reservation, $user, $historyEntryComment);
                    }
                }

                return $this->render('UTTReservationBundle:Reservation:paymentFailure.html.twig', $twigArray);
            }
        }



        exit;
    }

    public function changeConfirmAction(Request $request){
        $this->get('utt.activityLogService')->createForCurrentRoute();

        /** @var User $user */
        $user = $this->userAuth();

        /** @var ReservationService $reservationService */
        $reservationService = $this->get('utt.reservationservice');
        /** @var ReservationChangeData $reservationChangeData */
        $reservationChangeData = $reservationService->getReservationChangeData();
        if($reservationChangeData){
            $reservationId = $reservationChangeData->getReservationId();
            $fromDate = $reservationChangeData->getFromDate();
            $toDate = $reservationChangeData->getToDate();
            $sleeps = (int) $reservationChangeData->getSleeps();
            $sleepsChildren = (int) $reservationChangeData->getSleepsChildren();
            $pets = (int) $reservationChangeData->getPets();
            $infants = (int) $reservationChangeData->getInfants();

            ######### Params preparing
            #
            /** @var Reservation $reservation */
            $reservation = $this->getDoctrine()->getManager()->getRepository('UTTReservationBundle:Reservation')->findOneBy(array('id' => $reservationId));
            if(!($reservation && $reservation instanceof Reservation)){
                return $this->redirect($this->generateUrl('utt_index_homepage'));
            }

            if(!$fromDate){
                $fromDate = $reservation->getFromDate();
            }else{
                try { $fromDate = new \Datetime($fromDate); }catch (\Exception $e){
                    return $this->redirect($this->generateUrl('utt_index_homepage'));
                }
            }
            if(!$toDate){
                $toDate = $reservation->getToDate();
            }else{
                try { $toDate = new \Datetime($toDate); }catch (\Exception $e){
                    return $this->redirect($this->generateUrl('utt_index_homepage'));
                }
            }
            if(!$sleeps){
                $sleeps = $reservation->getSleeps();
            }
            if(!$sleepsChildren){
                $sleepsChildren = $reservation->getSleepsChildren();
            }
            if(!$pets){
                $pets = $reservation->getPets();
            }
            if(!$infants){
                $infants = $reservation->getInfants();
            }

            /** @var ReservationService $reservationService */
            $reservationService = $this->get('utt.reservationservice');
            /** @var ReservationDataPrice $newReservationPrice */
            $newReservationPrice = $reservationService->manageReservationPrice($reservation, $reservation->getEstate(), $user, $fromDate, $toDate, $sleeps, $pets, $infants);
            if($newReservationPrice instanceof ReservationDataPrice){
                if($reservationChangeData->getPrice()->isEqual($newReservationPrice)){
                        if($request->isMethod('POST')){
                            $changed = $reservationService->change($reservation, $user, $fromDate, $toDate, $sleeps, $sleepsChildren, $pets, $infants, $reservationChangeData->getPrice(), $reservationChangeData->getUserData());

                            if($changed){
                                $this->get('session')->getFlashBag()->add('success', 'Reservation changed!');
                            }else{
                                $this->get('session')->getFlashBag()->add('error', 'Reservation NOT changed!');
                            }

                            return $this->redirect($this->generateUrl('utt_user_reservation_manage', array(
                                'reservationId' => $reservation->getId()
                            )));
                        }else{
                            $twigArray = array();
                            $twigArray['reservation'] = $reservation;
                            $twigArray['estate'] = $reservation->getEstate();
                            $twigArray['fromDate'] = $fromDate;
                            $twigArray['toDate'] = $toDate;
                            $twigArray['sleeps'] = $sleeps;
                            $twigArray['sleepsChildren'] = $sleepsChildren;
                            $twigArray['pets'] = $pets;
                            $twigArray['infants'] = $infants;
                            $twigArray['price'] = $reservationChangeData->getPrice();
                            $twigArray['userData'] = $reservationChangeData->getUserData();
                            $twigArray['totalPrice'] = (float) $reservationChangeData->getPrice()->getTotal();

                            return $this->render('UTTReservationBundle:Reservation:changeConfirm.html.twig', $twigArray);
                        }

                }
            }
        }

        return $this->redirect($this->generateUrl('utt_index_homepage'));
    }

    /**
     * Authenticate a user with Symfony Security
     *
     * @param \FOS\UserBundle\Model\UserInterface        $user
     * @param \Symfony\Component\HttpFoundation\Response $response
     */
    protected function authenticateUser(UserInterface $user, Response $response)
    {
        try {
            $this->container->get('fos_user.security.login_manager')->loginUser(
                $this->container->getParameter('fos_user.firewall_name'),
                $user,
                $response);
        } catch (AccountStatusException $ex) {
            // We simply do not authenticate users which do not pass the user
            // checker (not enabled, expired, etc.).
        }
    }


    public function guestConfirmAction(Request $request){
        $this->get('utt.activityLogService')->createForCurrentRoute();

        $response = new RedirectResponse($this->generateUrl('utt_reservation_confirm'));

        /** @var ReservationService $reservationService */
        $reservationService = $this->get('utt.reservationservice');
        /** @var ReservationData $reservationData */
        $reservationData = $reservationService->getReservationData();
        if($reservationData){
            $emailErrors = $this->get('validator')->validateValue($reservationData->getUserData()->getEmail(), new EmailConstraint());
            if (count($emailErrors) > 0) {
                $this->get('session')->getFlashBag()->add('error', 'E-mail is not valid. Please type correct e-mail address.');

                return $this->redirect($this->generateUrl('utt_estate_make_reservation', array(
                    'estateShortName' => $reservationData->getEstateShortName(),
                    'fromDate' => $reservationData->getFromDate(),
                    'toDate' => $reservationData->getToDate()
                )));
            }

            $user = $this->getDoctrine()->getManager()->getRepository('UTTUserBundle:User')->findOneBy(array(
                'email' => $reservationData->getUserData()->getEmail()
            ));
            if(!($user instanceof User)){
                /** @var UserFactory $userFactory */
                $userFactory = $this->get('utt.userfactory');
                $user = $userFactory->create(
                    $reservationData->getUserData()->getEmail(),
                    $reservationData->getUserData()->getFirstName(),
                    $reservationData->getUserData()->getLastName(),
                    $userFactory->generateTemporaryPassword($reservationData->getUserData()->getEmail())
                );
                if($user instanceof User){
                    $this->authenticateUser($user, $response);
                }
            }else{
                $this->get('session')->getFlashBag()->add('error', 'There is already an account connected to your e-mail. Please log in below to continue. If you have forgotten your password you can reset it using resetting button below.');
            }
        }

        return $response;
    }

    public function confirmAction(Request $request){
	$logger = $this->get('logger');
        $this->get('utt.activityLogService')->createForCurrentRoute();

        /** @var User $user */
        $user = $this->userAuth();

        /** @var ReservationService $reservationService */
        $reservationService = $this->get('utt.reservationservice');
        /** @var ReservationData $reservationData */
        $reservationData = $reservationService->getReservationData();
        if($reservationData){
            $emailErrors = $this->get('validator')->validateValue($reservationData->getUserData()->getEmail(), new EmailConstraint());
            if (count($emailErrors) > 0) {
                $this->get('session')->getFlashBag()->add('error', 'E-mail is not valid. Please type correct e-mail address.');

                return $this->redirect($this->generateUrl('utt_estate_make_reservation', array(
                    'estateShortName' => $reservationData->getEstateShortName(),
                    'fromDate' => $reservationData->getFromDate(),
                    'toDate' => $reservationData->getToDate()
                )));
            }

            $estateShortName = $reservationData->getEstateShortName();
            $fromDate = $reservationData->getFromDate();
            $toDate = $reservationData->getToDate();
            $sleeps = (int) $reservationData->getSleeps();
            $sleepsChildren = (int) $reservationData->getSleepsChildren();
            $pets = (int) $reservationData->getPets();
            $infants = (int) $reservationData->getInfants();
            $code = $reservationData->getCode();
            $isBookingProtect = $reservationData->isIsBookingProtect();

            ######### Params preparing
            /** @var Estate $estate */
            $estate = $this->getDoctrine()->getManager()->getRepository('UTTEstateBundle:Estate')->findOneBy(array('shortName' => $estateShortName));
            if(!($estate && $estate instanceof Estate)){
                $logger->error('$reservationData $estate is not set, '.$user->getId());
                return $this->redirect($this->generateUrl('utt_index_homepage'));
            }
            try { $fromDate = new \Datetime($fromDate); }catch (\Exception $e){
                $logger->error('$reservationData $fromDate is not set, '.$user->getId());
                return $this->redirect($this->generateUrl('utt_index_homepage'));
            }
            try { $toDate = new \Datetime($toDate); }catch (\Exception $e){
                $logger->error('$reservationData $toDate is not set, '.$user->getId());
                return $this->redirect($this->generateUrl('utt_index_homepage'));
            }
            if(is_null($sleeps)){ $sleeps = $estate->getMinimumGuests(); }
            if(is_null($sleepsChildren)){ $sleepsChildren = 0; }
            if(is_null($pets)){ $pets = 0; }
            if(is_null($infants)){ $infants = 0; }

            /** @var ReservationDataPrice $newReservationPrice */
            $newReservationPrice = $reservationService->newReservationPrice($estate, $fromDate, $toDate, $sleeps, $pets, $infants, $isBookingProtect);
            if($newReservationPrice instanceof ReservationDataPrice){
                //if($reservationData->getPrice() == $newReservationPrice){
                if($reservationData->getPrice()->isEqual($newReservationPrice)){
                    /** @var DiscountCodeService $discountCodeService */
                    $discountCodeService = $this->get('utt.discountcodeservice');
                    /** @var ReservationDataDiscountPrice $newReservationDiscountPrice */
                    $newReservationDiscountPrice = $discountCodeService->getDiscountPriceForParams($newReservationPrice, $estate, $fromDate, $toDate, $code);
                    if(!($newReservationDiscountPrice instanceof ReservationDataDiscountPrice)) $newReservationDiscountPrice = new ReservationDataDiscountPrice;

                    if($reservationData->getDiscountPrice() == $newReservationDiscountPrice){
                        if($request->isMethod('POST')){
                            /** @var Reservation $bookedReservation */
                            $bookedReservation = $reservationService->book($estate, $user, $fromDate, $toDate, $sleeps, $sleepsChildren, $pets, $infants, $reservationData->getPrice(), $reservationData->getUserData(), $reservationData->getDiscountPrice(), $code, $isBookingProtect);

                            if($bookedReservation instanceOf Reservation){
                                /** @var ConfigurationService $configurationService */
                                $configurationService = $this->get('utt.configurationservice');
                                /** @var Configuration $configuration */
                                $configuration = $configurationService->get();

                                $paymentDeadline = 0;
                                if($configuration instanceOf Configuration){ $paymentDeadline = $configuration->getPaymentDeadline(); }

                                $this->get('session')->getFlashBag()->add('success', 'Congratulations. Your holiday has been reserved. You have '.$paymentDeadline.' hour/s to make payment. See below...');
                                $this->get('session')->getFlashBag()->add('success', 'We have just emailed you your confirmation. If you do not receive your confirmation in the next hour then please check your SPAM or JUNK folders, and if you experience any problem please email enquiries@underthethatch.co.uk or call 01239 727 029 with your booking reference.');
                                return $this->redirect($this->generateUrl('utt_user_reservation_manage', array(
                                    'reservationId' => $bookedReservation->getId()
                                )));
                                //return $this->render('UTTReservationBundle:Reservation:confirmSuccess.html.twig');
                            }
                        }else{
                            $twigArray = array();
                            $twigArray['isBookingProtect'] = $isBookingProtect;
                            $twigArray['estate'] = $estate;
                            $twigArray['fromDate'] = $fromDate;
                            $twigArray['toDate'] = $toDate;
                            $twigArray['sleeps'] = $sleeps;
                            $twigArray['sleepsChildren'] = $sleepsChildren;
                            $twigArray['pets'] = $pets;
                            $twigArray['infants'] = $infants;
                            $twigArray['price'] = $reservationData->getPrice();
                            $twigArray['discountPrice'] = $reservationData->getDiscountPrice();
                            $twigArray['userData'] = $reservationData->getUserData();
                            $twigArray['totalPrice'] = (float) $reservationData->getPrice()->getTotal() - (float) $reservationData->getDiscountPrice()->getTotal();

                            return $this->render('UTTReservationBundle:Reservation:confirm.html.twig', $twigArray);
                        }
                    }
                }
            }
        } else {
            $logger->error('$reservationData is not set, '.$user->getId());
        }

        return $this->redirect($this->generateUrl('utt_index_homepage'));
    }

    public function changeCreateAction(Request $request){
        if($request->isXmlHttpRequest()) $requestQuery = $request->request; else $requestQuery = $request->query;
        if($requestQuery->has('reservationId')) $reservationId = $requestQuery->get('reservationId'); else $reservationId = null;
        if($requestQuery->has('fromDate')) $fromDate = $requestQuery->get('fromDate'); else $fromDate = null;
        if($requestQuery->has('toDate')) $toDate = $requestQuery->get('toDate'); else $toDate = null;
        if($requestQuery->has('sleeps')) $sleeps = (int) $requestQuery->get('sleeps'); else $sleeps = null;
        if($requestQuery->has('sleepsChildren')) $sleepsChildren = (int) $requestQuery->get('sleepsChildren'); else $sleepsChildren = null;
        if($requestQuery->has('pets')) $pets = (int) $requestQuery->get('pets'); else $pets = null;
        if($requestQuery->has('infants')) $infants = (int) $requestQuery->get('infants'); else $infants = null;
        if($requestQuery->has('userData')) $userData = $requestQuery->get('userData'); else $userData = null;
        if($requestQuery->has('price')) $price = $requestQuery->get('price'); else $price = null;

        if(!is_array($userData)) $userData = array();
        if(!is_array($price)) $price = array();

        /** @var ReservationService $reservationService */
        $reservationService = $this->get('utt.reservationservice');
        $reservationService->saveReservationChangeData($reservationId, $fromDate, $toDate, $sleeps, $sleepsChildren, $pets, $infants, $userData, $price);

        return new JsonResponse(array('success' => true));
    }

    public function telephoneBookingAction(Request $request){
        if(!$this->userAuth(false)) return new JsonResponse(array('success' => false));

        /** @var UTTAclService $uttAclService */
        $uttAclService = $this->container->get('utt.aclService');
        if($uttAclService->isGrantedCleaner()) return new JsonResponse(array('success' => false));

        if($request->isXmlHttpRequest()) $requestQuery = $request->request; else $requestQuery = $request->query;
        if($requestQuery->has('estateShortName')) $estateShortName = $requestQuery->get('estateShortName'); else $estateShortName = null;
        if($requestQuery->has('fromDate')) $fromDate = $requestQuery->get('fromDate'); else $fromDate = null;
        if($requestQuery->has('toDate')) $toDate = $requestQuery->get('toDate'); else $toDate = null;
        if($requestQuery->has('price')) $price = $requestQuery->get('price'); else $price = null;
        if($requestQuery->has('guestEmail')) $guestEmail = $requestQuery->get('guestEmail'); else $guestEmail = null;
        if($requestQuery->has('guestFirstName')) $guestFirstName = $requestQuery->get('guestFirstName'); else $guestFirstName = null;
        if($requestQuery->has('guestLastName')) $guestLastName = $requestQuery->get('guestLastName'); else $guestLastName = null;
        if($requestQuery->has('isFullyPaid')) $isFullyPaid = (bool) $requestQuery->get('isFullyPaid'); else $isFullyPaid = false;

        /** @var Estate $estate */
        $estate = $this->getDoctrine()->getManager()->getRepository('UTTEstateBundle:Estate')->findOneBy(array('shortName' => $estateShortName));
        if(!($estate && $estate instanceof Estate)){
            return new JsonResponse(array('success' => false));
        }
        try { $fromDate = new \Datetime($fromDate); }catch (\Exception $e){
            return new JsonResponse(array('success' => false));
        }
        try { $toDate = new \Datetime($toDate); }catch (\Exception $e){
            return new JsonResponse(array('success' => false));
        }

        $user = $this->getDoctrine()->getManager()->getRepository('UTTUserBundle:User')->findOneBy(array(
            'email' => $guestEmail
        ));
        if(!($user instanceof User)) {
            /** @var UserFactory $userFactory */
            $userFactory = $this->get('utt.userfactory');
            $user = $userFactory->create($guestEmail, $guestFirstName, $guestLastName,
                $userFactory->generateTemporaryPassword($guestEmail));
        }

        $manager = $this->getUserFromSession();
        $reservationDataUserData = new ReservationDataUserData(json_decode(json_encode(array(
            'firstName' => $guestFirstName,
            'lastName' => $guestLastName,
            'email' => $guestEmail
        ))));

        $reservationDataPrice = new ReservationDataPrice(json_decode(json_encode(array(
            'basePrice' => $price,
            'total' => $price
        ))));

        /** @var ReservationService $reservationService */
        $reservationService = $this->get('utt.reservationservice');
        /** @var Reservation $bookedReservation */
        $bookedReservation = $reservationService->telephoneBooking($estate, $user, $fromDate, $toDate, $reservationDataPrice, $reservationDataUserData, $isFullyPaid, $manager);
        if($bookedReservation){
            return new JsonResponse(array('success' => true, 'reservation' => $reservationService->reservationResponseArray($bookedReservation)));
        }

        return new JsonResponse(array('success' => false));
    }

    public function grouponBookingAction(Request $request){
        if(!$this->userAuth(false)) return new JsonResponse(array('success' => false));

        /** @var UTTAclService $uttAclService */
        $uttAclService = $this->container->get('utt.aclService');
        if($uttAclService->isGrantedCleaner()) return new JsonResponse(array('success' => false));

        if($request->isXmlHttpRequest()) $requestQuery = $request->request; else $requestQuery = $request->query;
        if($requestQuery->has('estateShortName')) $estateShortName = $requestQuery->get('estateShortName'); else $estateShortName = null;
        if($requestQuery->has('fromDate')) $fromDate = $requestQuery->get('fromDate'); else $fromDate = null;
        if($requestQuery->has('toDate')) $toDate = $requestQuery->get('toDate'); else $toDate = null;
        if($requestQuery->has('guestEmail')) $guestEmail = $requestQuery->get('guestEmail'); else $guestEmail = null;
        if($requestQuery->has('guestFirstName')) $guestFirstName = $requestQuery->get('guestFirstName'); else $guestFirstName = null;
        if($requestQuery->has('guestLastName')) $guestLastName = $requestQuery->get('guestLastName'); else $guestLastName = null;

        /** @var Estate $estate */
        $estate = $this->getDoctrine()->getManager()->getRepository('UTTEstateBundle:Estate')->findOneBy(array('shortName' => $estateShortName));
        if(!($estate && $estate instanceof Estate)){
            return new JsonResponse(array('success' => false));
        }
        try { $fromDate = new \Datetime($fromDate); }catch (\Exception $e){
            return new JsonResponse(array('success' => false));
        }
        try { $toDate = new \Datetime($toDate); }catch (\Exception $e){
            return new JsonResponse(array('success' => false));
        }

        $user = $this->getDoctrine()->getManager()->getRepository('UTTUserBundle:User')->findOneBy(array(
            'email' => $guestEmail
        ));
        if(!($user instanceof User)){
            /** @var UserFactory $userFactory */
            $userFactory = $this->get('utt.userfactory');
            $user = $userFactory->create($guestEmail, $guestFirstName, $guestLastName, $userFactory->generateTemporaryPassword($guestEmail));
        }

        $manager = $this->getUserFromSession();
        $reservationDataUserData = new ReservationDataUserData(json_decode(json_encode(array(
            'firstName' => $guestFirstName,
            'lastName' => $guestLastName,
            'email' => $guestEmail
        ))));

        /** @var ReservationService $reservationService */
        $reservationService = $this->get('utt.reservationservice');
        /** @var Reservation $bookedReservation */
        $bookedReservation = $reservationService->grouponBooking($estate, $user, $fromDate, $toDate, $reservationDataUserData, $manager);
        if($bookedReservation){
            return new JsonResponse(array('success' => true, 'reservation' => $reservationService->reservationResponseArray($bookedReservation)));
        }

        return new JsonResponse(array('success' => false));
    }

    public function voucherPaymentAction(Request $request, $reservationId){
        $voucherCode = $request->get('voucherCode');

        $referer = $request->headers->get('referer');
        if(!$referer) $referer = $this->generateUrl('utt_user_reservation_manage', array(
            'reservationId' => $reservationId
        ));

        if($voucherCode){
            /** @var ReservationRepository $reservationRepository */
            $reservationRepository = $this->getDoctrine()->getManager()->getRepository('UTTReservationBundle:Reservation');
            /** @var VoucherRepository $voucherRepository */
            $voucherRepository = $this->getDoctrine()->getManager()->getRepository('UTTReservationBundle:Voucher');

            /** @var Reservation $reservation */
            $reservation = $reservationRepository->find($reservationId);
            $voucher = $voucherRepository->findVoucherByCode($voucherCode);

            if($reservation instanceof Reservation && $voucher instanceof Voucher){
                /** @var ReservationService $reservationService */
                $reservationService = $this->get('utt.reservationservice');

                $reservation = $reservationService->payWithVoucher($reservation, $voucher);
                if($reservation instanceof Reservation){
                    $this->get('session')->getFlashBag()->add('success', 'Thank you! Voucher payment added to the reservation.');
                }else{
                    $this->get('session')->getFlashBag()->add('error', 'Voucher payment not added to the reservation.');
                }
            }else{
                $this->get('session')->getFlashBag()->add('error', 'Voucher payment not added to the reservation.');
            }
        }else{
            $this->get('session')->getFlashBag()->add('error', 'Voucher code is empty. Voucher payment not added to the reservation.');
        }

        return $this->redirect($referer);
    }

    public function bookBlockedAction(Request $request){
        /** @var User $user */
        $user = $this->userAuth(false);
        if(!$user) return new JsonResponse(array('success' => false));

        /** @var UTTAclService $uttAclService */
        $uttAclService = $this->container->get('utt.aclService');
        if($uttAclService->isGrantedCleaner()) return new JsonResponse(array('success' => false));

        if($request->isXmlHttpRequest()) $requestQuery = $request->request; else $requestQuery = $request->query;
        if($requestQuery->has('estateShortName')) $estateShortName = $requestQuery->get('estateShortName'); else $estateShortName = null;
        if($requestQuery->has('blockedStatus')) $blockedStatus = $requestQuery->get('blockedStatus'); else $blockedStatus = null;
        if($requestQuery->has('fromDate')) $fromDate = $requestQuery->get('fromDate'); else $fromDate = null;
        if($requestQuery->has('toDate')) $toDate = $requestQuery->get('toDate'); else $toDate = null;
        if($requestQuery->has('reason')) $reason = $requestQuery->get('reason'); else $reason = null;
        if($requestQuery->has('sleeps')) $sleeps = $requestQuery->get('sleeps'); else $sleeps = null;

        /** @var Estate $estate */
        $estate = $this->getDoctrine()->getManager()->getRepository('UTTEstateBundle:Estate')->findOneBy(array('shortName' => $estateShortName));
        if(!($estate && $estate instanceof Estate)){
            return new JsonResponse(array('success' => false));
        }
        try { $fromDate = new \Datetime($fromDate); }catch (\Exception $e){
            return new JsonResponse(array('success' => false));
        }
        try { $toDate = new \Datetime($toDate); }catch (\Exception $e){
            return new JsonResponse(array('success' => false));
        }

        if($blockedStatus == 'owner_booking'){ $blockedStatus = Reservation::STATUS_BLOCKED_OWNER; }
        elseif($blockedStatus == 'dates_not_for_sale'){ $blockedStatus = Reservation::STATUS_BLOCKED_NOT_FOR_SALE; }
        elseif($blockedStatus == 'other_agency'){ $blockedStatus = Reservation::STATUS_BLOCKED_OTHER_AGENCY; }
        else{
            return new JsonResponse(array('success' => false));
        }

        if(is_null($sleeps)){ $sleeps = $estate->getMinimumGuests(); }

        /** @var ReservationService $reservationService */
        $reservationService = $this->get('utt.reservationservice');
        /** @var Reservation $bookedReservation */
        $bookedReservation = $reservationService->bookBlocked($estate, $user, $fromDate, $toDate, $blockedStatus, $reason, $sleeps);
        if($bookedReservation){
            return new JsonResponse(array('success' => true, 'reservation' => $reservationService->reservationResponseArray($bookedReservation)));
        }

        return new JsonResponse(array('success' => false));
    }

    public function createAction(Request $request){
        if($request->isXmlHttpRequest()) $requestQuery = $request->request; else $requestQuery = $request->query;
        if($requestQuery->has('estateShortName')) $estateShortName = $requestQuery->get('estateShortName'); else $estateShortName = null;
        if($requestQuery->has('fromDate')) $fromDate = $requestQuery->get('fromDate'); else $fromDate = null;
        if($requestQuery->has('toDate')) $toDate = $requestQuery->get('toDate'); else $toDate = null;
        if($requestQuery->has('sleeps')) $sleeps = (int) $requestQuery->get('sleeps'); else $sleeps = null;
        if($requestQuery->has('sleepsChildren')) $sleepsChildren = (int) $requestQuery->get('sleepsChildren'); else $sleepsChildren = null;
        if($requestQuery->has('pets')) $pets = (int) $requestQuery->get('pets'); else $pets = null;
        if($requestQuery->has('infants')) $infants = (int) $requestQuery->get('infants'); else $infants = null;
        if($requestQuery->has('userData')) $userData = $requestQuery->get('userData'); else $userData = null;
        if($requestQuery->has('price')) $price = $requestQuery->get('price'); else $price = null;
        if($requestQuery->has('discountPrice')) $discountPrice = $requestQuery->get('discountPrice'); else $discountPrice = null;
        if($requestQuery->has('code')) $code = $requestQuery->get('code'); else $code = null;
        if($requestQuery->has('isBookingProtect')) $isBookingProtect = (bool) $requestQuery->get('isBookingProtect'); else $isBookingProtect = false;

        if(!is_array($userData)) $userData = array();
        if(!is_array($price)) $price = array();
        if(!is_array($discountPrice)) $discountPrice = array();

        /** @var ReservationService $reservationService */
        $reservationService = $this->get('utt.reservationservice');
        $reservationService->saveReservationData($estateShortName, $fromDate, $toDate, $sleeps, $sleepsChildren, $pets, $infants, $userData, $price, $discountPrice, $code, $isBookingProtect);

        return new JsonResponse(array('success' => true));
    }

    public function managePriceAction(Request $request){
        $result = array('success' => false);

        if($request->isXmlHttpRequest()) $requestQuery = $request->request; else $requestQuery = $request->query;
        if($requestQuery->has('reservationId')) $reservationId = $requestQuery->get('reservationId'); else $reservationId = null;
        if($requestQuery->has('estateShortName')) $estateShortName = $requestQuery->get('estateShortName'); else $estateShortName = null;
        if($requestQuery->has('fromDate')) $fromDate = $requestQuery->get('fromDate'); else $fromDate = null;
        if($requestQuery->has('toDate')) $toDate = $requestQuery->get('toDate'); else $toDate = null;
        if($requestQuery->has('sleeps')) $sleeps = (int) $requestQuery->get('sleeps'); else $sleeps = null;
        if($requestQuery->has('pets')) $pets = (int) $requestQuery->get('pets'); else $pets = null;
        if($requestQuery->has('infants')) $infants = (int) $requestQuery->get('infants'); else $infants = null;

        /** @var Reservation $reservation */
        $reservation = $this->getDoctrine()->getManager()->getRepository('UTTReservationBundle:Reservation')->findOneBy(array('id' => $reservationId));
        if(!($reservation && $reservation instanceof Reservation)){
            return new JsonResponse($result);
        }
        /** @var Estate $estate */
        $estate = $reservation->getEstate();
        if(!is_null($estateShortName)){
            $estate = $this->getDoctrine()->getManager()->getRepository('UTTEstateBundle:Estate')->findOneBy(array('shortName' => $estateShortName));
            if(!($estate && $estate instanceof Estate)){
                return new JsonResponse($result);
            }
        }

        /** @var User $user */
        $user = $this->userAuth(false);
        if(!$user){
            return new JsonResponse($result);
        }

        /** @var UTTAclService $uttAclService */
        $uttAclService = $this->container->get('utt.aclService');
        if($uttAclService->isGrantedSuperAdmin()){
            $user = $reservation->getUser();
        }elseif($uttAclService->isGrantedOwner()){
            try{
                $uttAclService->authEstateOwnerUser($estate);
                $user = $reservation->getUser();
            }catch(AccessDeniedException $e){}
        }

        if(is_null($fromDate)){
            $fromDate = $reservation->getFromDate();
        }else{
            try { $fromDate = new \Datetime($fromDate); }catch (\Exception $e){
                return new JsonResponse($result);
            }
        }
        if(is_null($toDate)){
            $toDate = $reservation->getToDate();
        }else{
            try { $toDate = new \Datetime($toDate); }catch (\Exception $e){
                return new JsonResponse($result);
            }
        }
        if(is_null($sleeps)){
            $sleeps = $reservation->getSleeps();
        }
        if(is_null($pets)){
            $pets = $reservation->getPets();
        }
        if(is_null($infants)){
            $infants = $reservation->getInfants();
        }


        /** @var ReservationService $reservationService */
        $reservationService = $this->get('utt.reservationservice');

        if(!$reservationService->validateReservationDate($estate, $fromDate, $toDate)) {
            return new JsonResponse($result);
        }
        /** @var ReservationDataPrice $newReservationPrice */
        $newReservationPrice = $reservationService->manageReservationPrice($reservation, $estate, $user, $fromDate, $toDate, $sleeps, $pets, $infants);
        if($newReservationPrice instanceof ReservationDataPrice){
            $result = array('success' => true, 'newReservationPrice' => $newReservationPrice);
        }

        return new JsonResponse($result);
    }

    public function priceAction(Request $request){
        $result = array('success' => false);

        if($request->isXmlHttpRequest()) $requestQuery = $request->request; else $requestQuery = $request->query;
        if($requestQuery->has('estateShortName')) $estateShortName = $requestQuery->get('estateShortName'); else $estateShortName = null;
        if($requestQuery->has('fromDate')) $fromDate = $requestQuery->get('fromDate'); else $fromDate = null;
        if($requestQuery->has('toDate')) $toDate = $requestQuery->get('toDate'); else $toDate = null;
        if($requestQuery->has('sleeps')) $sleeps = (int) $requestQuery->get('sleeps'); else $sleeps = null;
        if($requestQuery->has('pets')) $pets = (int) $requestQuery->get('pets'); else $pets = null;
        if($requestQuery->has('infants')) $infants = (int) $requestQuery->get('infants'); else $infants = null;
        if($requestQuery->has('code')) $code = $requestQuery->get('code'); else $code = null;
        if($requestQuery->has('isBookingProtect')) $isBookingProtect = (bool) $requestQuery->get('isBookingProtect'); else $isBookingProtect = false;

        ######### Params preparing
        #
        /** @var Estate $estate */
        $estate = $this->getDoctrine()->getManager()->getRepository('UTTEstateBundle:Estate')->findOneBy(array('shortName' => $estateShortName));
        if(!($estate && $estate instanceof Estate)){
            return new JsonResponse($result);
        }
        try { $fromDate = new \Datetime($fromDate); }catch (\Exception $e){
            return new JsonResponse($result);
        }
        try { $toDate = new \Datetime($toDate); }catch (\Exception $e){
            return new JsonResponse($result);
        }
        if(is_null($sleeps)){ $sleeps = $estate->getMinimumGuests(); }
        if(is_null($pets)){ $pets = 0; }
        if(is_null($infants)){ $infants = 0; }

        /** @var ReservationService $reservationService */
        $reservationService = $this->get('utt.reservationservice');
        /** @var ReservationDataPrice $newReservationPrice */
        $newReservationPrice = $reservationService->newReservationPrice($estate, $fromDate, $toDate, $sleeps, $pets, $infants, $isBookingProtect);
        if($newReservationPrice instanceof ReservationDataPrice){
            $result = array('success' => true, 'newReservationPrice' => $newReservationPrice);

            /** @var DiscountCodeService $discountCodeService */
            $discountCodeService = $this->get('utt.discountcodeservice');
            /** @var ReservationDataDiscountPrice $newReservationDiscountPrice */
            $newReservationDiscountPrice = $discountCodeService->getDiscountPriceForParams($newReservationPrice, $estate, $fromDate, $toDate, $code);
            if($newReservationDiscountPrice instanceof ReservationDataDiscountPrice){
                $result['newReservationDiscountPrice'] = $newReservationDiscountPrice;
            }
        }

        return new JsonResponse($result);
    }

    public function indexAction(){
        return $this->render('UTTReservationBundle:Reservation:index.html.twig');
    }
}
