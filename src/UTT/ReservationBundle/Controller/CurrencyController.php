<?php

namespace UTT\ReservationBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Validator\Constraints\Currency;
use UTT\EstateBundle\Entity\Estate;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Session\Session;
use FOS\UserBundle\Model\UserInterface;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use UTT\ReservationBundle\Entity\CurrencyRate;
use UTT\ReservationBundle\Entity\PricingRepository;
use UTT\ReservationBundle\Entity\Pricing;
use UTT\ReservationBundle\Service\PricingService;
use UTT\ReservationBundle\Service\CurrencyService;

class CurrencyController extends Controller
{
    private function userAuth($throwException = true){
        if(!$this->container->get('security.context')->isGranted('IS_AUTHENTICATED_FULLY') ){
            if($throwException){
                throw new AccessDeniedException('This user does not have access to this section.');
            }else{
                return false;
            }
        }

        $user = $this->container->get('security.context')->getToken()->getUser();
        if (!is_object($user) || !$user instanceof UserInterface) {
            if($throwException){
                throw new AccessDeniedException('This user does not have access to this section.');
            }else{
                return false;
            }
        }

        return $user;
    }

    public function currencyRateRefreshAction(){
        $xmlCurrencies = array('EUR' => 1);

        # load from XML
        try{
            $xml = file_get_contents("http://www.ecb.europa.eu/stats/eurofxref/eurofxref-daily.xml");
            $data = new \SimpleXMLElement($xml);
            if(isset($data->Cube) && isset($data->Cube->Cube) && isset($data->Cube->Cube->Cube)){
                foreach($data->Cube->Cube->Cube as $cube){
                    if(isset($cube['currency']) && isset($cube['rate'])){
                        $xmlCurrency = (string)$cube['currency'];
                        $xmlRate = (float)$cube['rate'];

                        if(array_key_exists($xmlCurrency, CurrencyRate::getAllowedCurrencyCodesStatic())){
                            $xmlCurrencies[$xmlCurrency] = $xmlRate;
                        }
                    }
                }
            }else{
                return new JsonResponse(array('success' => false));
            }
        }catch(\Exception $e){
            return new JsonResponse(array('success' => false));
        }

        if(isset($xmlCurrencies['GBP'])){
            $baseRate = $xmlCurrencies['GBP'];

            /** @var CurrencyService $currencyService */
            $currencyService = $this->get('utt.currencyservice');

            $isAdded = false;
            foreach($xmlCurrencies as $currencyKey => $currencyValue){
                if(array_key_exists($currencyKey, CurrencyRate::getAllowedCurrencyCodesStatic())){
                    $currentCurrencyRate = $currencyService->getCurrentCurrencyRate($currencyKey);

                    if(!($currentCurrencyRate instanceof CurrencyRate)){
                        try{
                            $nowDate = new \Datetime('now');
                            $value = $currencyValue/$baseRate;

                            $currencyRate = $currencyService->createCurrencyRate($currencyKey, $nowDate, $value);
                            if($currencyRate instanceof CurrencyRate){
                                $isAdded = true;
                            }
                        }catch(\Exception $e){
                            return new JsonResponse(array('success' => false));
                        }

                    }
                }
            }

            if($isAdded) return new JsonResponse(array('success' => true));
        }
        return new JsonResponse(array('success' => false));
    }

}
