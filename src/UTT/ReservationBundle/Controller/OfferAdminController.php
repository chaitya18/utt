<?php

namespace UTT\ReservationBundle\Controller;

use Sonata\AdminBundle\Controller\CRUDController as Controller;
use UTT\ReservationBundle\Entity\Offer;
use UTT\EstateBundle\Entity\Estate;


class OfferAdminController extends Controller
{
    public function editAction($id = null){
        /** @var Offer $offer */
        $offer = $this->admin->getSubject();
        if($offer){
            /** @var Estate $estate */
            foreach($offer->getEstates() as $estate){
                $this->container->get('utt.aclService')->authEstateOwnerUser($estate);
            }
        }

        return parent::editAction($id);
    }

    public function deleteAction($id){
        /** @var Offer $offer */
        $offer = $this->admin->getSubject();
        if($offer){
            /** @var Estate $estate */
            foreach($offer->getEstates() as $estate){
                $this->container->get('utt.aclService')->authEstateOwnerUser($estate);
            }
        }

        return parent::deleteAction($id);
    }

}