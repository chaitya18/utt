<?php

namespace UTT\ReservationBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use UTT\ReservationBundle\Entity\PaymentTransaction;
use UTT\ReservationBundle\Entity\Reservation;
use UTT\ReservationBundle\Payment\PriceFactory;
use UTT\ReservationBundle\Payment\TransactionService;
use UTT\ReservationBundle\Payment\Worldpay\Order;
use UTT\ReservationBundle\Payment\Worldpay\OrderFactory;
use UTT\ReservationBundle\Payment\Worldpay\PaymentTransactionFactory;
use UTT\ReservationBundle\Payment\Worldpay\Result;
use UTT\ReservationBundle\Payment\Worldpay\TransactionPaymentFactory;
use UTT\ReservationBundle\Payment\Worldpay\WorldPayConfig;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;



class PaymentController extends Controller
{


    /**
     * @param Reservation $reservation
     * @param false $applyCreditCardCharge
     * @param int $charityCharge
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function renderPayFormReservationAction(Reservation $reservation, $applyCreditCardCharge = false, $charityCharge = 0){

        $twigArray = array();

        /** @var WorldPayConfig $worldPayConfig */
        $worldPayConfig = $this->get('utt.worldpay.config');
        /** @var OrderFactory $orderFactory */
        $orderFactory = $this->get('utt.worldpay.order.factory');
        /** @var PriceFactory $priceFactory */
        $priceFactory = $this->get('utt.payment.price.factory');


        if($reservation->isAllowedToPay()) {
            $price = $priceFactory->createFromReservation($reservation, $applyCreditCardCharge, $charityCharge);
            $order = $orderFactory->createFromReservation($reservation, $price);
            $twigArray['payUrl'] = $worldPayConfig->getUrl();
            $twigArray['returnUrl'] = $this->get('request')->getSchemeAndHttpHost().$this->generateUrl('utt_reservation_payment_status',
                    array('cartId' => $order->getCartId()));
            $twigArray['callbackUrl'] = $this->get('request')->getSchemeAndHttpHost().$this->generateUrl('utt_reservation_payment_wd_result');
            $twigArray['order'] = $order;

            $twigArray['applyCreditCardCharge'] = $applyCreditCardCharge;
            $this->savePaymentTransaction($reservation, $order, $applyCreditCardCharge);

        }

        return $this->render('UTTReservationBundle:Reservation:renderWorldPayForm.html.twig', $twigArray);

    }

    /**
     * @todo refactor - is not good place
     * @param Reservation $reservation
     * @param Order $order
     * @param $applyCreditCardCharge
     */
    protected function savePaymentTransaction(Reservation $reservation, Order $order, $applyCreditCardCharge)
    {
        $price = $order->getPrice();
        $paymentTransaction = new PaymentTransaction();
        $paymentTransaction->setStatus(PaymentTransaction::STATUS_PREPARED);
        $paymentTransaction->setReservation($reservation);
        $paymentTransaction->setCode($order->getCartId());
        $paymentTransaction->setAmount($price->getTotal());
        $paymentTransaction->setAmountCreditCardCharge($price->getCreditCardCharge());
        $paymentTransaction->setAmountCharityCharge($price->getCharityCharge());
        $paymentTransaction->setSmartPayMerchantSig($order->getSignature());
        if($applyCreditCardCharge){
            $paymentTransaction->setType(PaymentTransaction::TYPE_CREDIT_CARD);
        }else{
            $paymentTransaction->setType(PaymentTransaction::TYPE_DEBIT_CARD);
        }
        $_em = $this->getDoctrine()->getManager();
        $_em->persist($paymentTransaction);
        $_em->flush();
    }


    public function paymentResultAction(Request $request){

        $logger = $this->get('logger.payment');
        $query = $request->query->all();

        try {
            $paymentResult = Result::createFromRequest($request->request->all());
            //$paymentResult = Result::createFromRequest($this->getTestInputData());
            /**
             * @todo remove
             */
            $logger->notice('POST: '.serialize($request->request->all()));
            $logger->notice('GET: '.serialize($query));
            /**
             * @var $paymentWorldPayTransactionService \UTT\ReservationBundle\Payment\Worldpay\TransactionService
             */
            $paymentWorldPayTransactionService = $this->get('utt.payment.worldpay.transaction.service');
            $paymentWorldPayTransactionService->handle($paymentResult);
        } catch (\Exception $e) {
            return $this->paymentResultErrorResponse($e, $logger);
        }

        return new Response('OK');


    }

    private function getTestInputData()
    {
        $data = array(
            'authAmount' => '45.60',
            'authCurrency' => 'GBP',
            'transId' => 914727643,
            'transStatus' => 'Y',
            'transTime' => time(),
            'cardType' => 'Visa',
            'testMode' => 100,
            'rawAuthMessage' => 'cardbe.msg.authorised',
            'M_userId' => '20343',
            'email' => 'test@intssseria.pl',
            'instId' => '1424214',
            'cartId' => '71620_36028781c4b2f550',
            'callbackPW' => 't7ADqRxq9vaurZWbj3KQa#'

        );
        return $data;
    }

    /**
     * @param \Exception $e
     * @param $logger
     * @return Response
     */
    protected function paymentResultErrorResponse(\Exception $e, $logger) {
        $logger->error($e->getMessage().' code:'.$e->getCode().' File: '.$e->getFile().':'.$e->getLine());
        $code = (int) $e->getCode();
        if($code > 300 && $code < 500){
            return new Response('Error - '.$e->getMessage(), $code);
        }
        return new Response('Error - '.$e->getMessage(), 500);
    }

    /**
     * @param null $cartId
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|Response
     */
    public function paymentStatusAction($cartId = null)
    {
        $twigArray = array();
        try {
            /**
             * @var $paymentWorldPayTransactionService \UTT\ReservationBundle\Payment\Worldpay\TransactionService
             */
            $paymentWorldPayTransactionService = $this->get('utt.payment.worldpay.transaction.service');
            $paymentTransaction = $paymentWorldPayTransactionService->getPaymentTransaction($cartId);
            $reservation = $paymentTransaction->getReservation();
            if($paymentTransaction->isPaymentAuthSuccess()) {
                $this->get('session')->getFlashBag()->add('success', 'Payment successful!');
            } else {
                $this->get('session')->getFlashBag()->add('error', 'An unknown error has occured. Please wait a while and then try again to make your payment.');
            }

            return $this->redirect($this->generateUrl('utt_user_reservation_manage', array(
                'reservationId' => $reservation->getId()
            )));
        } catch (\Exception $e) {
            $twigArray['statusText'] = 'An error occurred during the payment process';
            return $this->render('UTTReservationBundle:Reservation:paymentFailure.html.twig', $twigArray);
        }

    }

}
