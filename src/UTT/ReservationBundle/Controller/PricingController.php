<?php

namespace UTT\ReservationBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use UTT\EstateBundle\Entity\Estate;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Session\Session;
use FOS\UserBundle\Model\UserInterface;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use UTT\ReservationBundle\Entity\PricingRepository;
use UTT\ReservationBundle\Entity\Pricing;
use UTT\ReservationBundle\Entity\PricingSeason;
use UTT\ReservationBundle\Service\PricingService;
use Symfony\Component\HttpFoundation\Request;
use UTT\ReservationBundle\Entity\PricingCategory;
use UTT\ReservationBundle\Service\PricingCategoryService;

class PricingController extends Controller
{
    private function userAuth($throwException = true){
        if(!$this->container->get('security.context')->isGranted('IS_AUTHENTICATED_FULLY') ){
            if($throwException){
                throw new AccessDeniedException('This user does not have access to this section.');
            }else{
                return false;
            }
        }

        $user = $this->container->get('security.context')->getToken()->getUser();
        if (!is_object($user) || !$user instanceof UserInterface) {
            if($throwException){
                throw new AccessDeniedException('This user does not have access to this section.');
            }else{
                return false;
            }
        }

        return $user;
    }

    public function getPricingManageCalendarAction($year){
        return $this->render('UTTReservationBundle:Reservation:getPricingManageCalendar.html.twig', array(
            'year' => $year
        ));
    }

    public function getPricingSeasonsAction(){
        $result = array('success' => false);

        $pricingSeasons = $this->getDoctrine()->getManager()->getRepository('UTTReservationBundle:PricingSeason')->findAll();
        if($pricingSeasons){
            $pricingSeasonsResult = array();

            /** @var PricingSeason $pricingSeason */
            foreach($pricingSeasons as $pricingSeason){
                $pricingResultItem = array(
                    'id' => $pricingSeason->getId(),
                    'type' => $pricingSeason->getType(),
                    'typeName' => $pricingSeason->getTypeName(),
                    'name' => $pricingSeason->getNameName(),
                    'fromDate' => $pricingSeason->getFromDate()->format('Y-m-d'),
                    'toDate' => $pricingSeason->getToDate()->format('Y-m-d')
                );

                $pricingSeasonsResult[] = $pricingResultItem;
            }
            $result = array('success' => true, 'pricingSeasons' => $pricingSeasonsResult);
        }

        return new JsonResponse($result);
    }

    public function getPricingAction($estateShortName){
        $result = array('success' => false);

        $_em = $this->getDoctrine()->getManager();

        /** @var Estate $estate */
        $estate = $_em->getRepository('UTTEstateBundle:Estate')->findOneBy(array('shortName' => $estateShortName));
        if(!($estate && $estate instanceof Estate)){
            return new JsonResponse($result);
        }

        /** @var PricingService $pricingService */
        $pricingService = $this->get('utt.pricingservice');
        $pricingList = $pricingService->getPricingListForEstateForYear($estate, null, 2);
        if($pricingList){
            $pricingResult = array();
            /** @var Pricing $pricing */
            foreach($pricingList as $pricing){
                $pricingResultItem = array(
                    'fromDate' => $pricing->getFromDate()->format('Y-m-d'),
                    'toDate' => $pricing->getToDate()->format('Y-m-d'),
                    'price' => $pricing->getPrice(),
                );
                if($pricing->getType() == Pricing::TYPE_FLEXIBLE_BOOKING){
                    $pricingResultItem['initialPrice'] = $pricing->getInitialPrice();
                }
                $pricingResult[] = $pricingResultItem;
            }

            $result = array('success' => true, 'pricingList' => $pricingResult);
        }


        return new JsonResponse($result);
    }

    public function pricingUpdateStandardAction($estateShortName, $fromDate, $toDate, $price){
        $result = array('success' => false);

        $_em = $this->getDoctrine()->getManager();

        /** @var Estate $estate */
        $estate = $_em->getRepository('UTTEstateBundle:Estate')->findOneBy(array('shortName' => $estateShortName));
        if(!($estate && $estate instanceof Estate)){
            return new JsonResponse($result);
        }

        $this->get('utt.aclService')->authEstateOwnerUser($estate);

        try { $fromDate = new \Datetime($fromDate); }catch (\Exception $e){
            return new JsonResponse($result);
        }
        try { $toDate = new \Datetime($toDate); }catch (\Exception $e){
            return new JsonResponse($result);
        }

        if($estate->getType(Estate::TYPE_STANDARD_M_F)){
            /** @var PricingRepository $pricingRepository */
            $pricingRepository = $_em->getRepository('UTTReservationBundle:Pricing');
            $pricing = $pricingRepository->findOneBy(array(
                'estate' => $estate->getId(),
                'type' => Pricing::TYPE_STANDARD_M_F,
                'fromDate' => $fromDate,
                'toDate' => $toDate
            ));
            if($pricing instanceof Pricing){
                $pricing->setPrice((float)$price);

                $_em->flush();
                $result = array('success' => true);

                /** @var PricingService $pricingService */
                $pricingService = $this->get('utt.pricingservice');

                $lowestPrice = $pricingService->findLowestPriceByPricingCategory($estate);
                if($lowestPrice != -1){ $estate->setLowestPrice($lowestPrice); }

                $highestPrice = $pricingService->findHighestPrice($estate);
                if($highestPrice != -1){ $estate->setHighestPrice($highestPrice); }

                $_em->flush();
            }
        }

        return new JsonResponse($result);
    }

    public function pricingUpdateFlexibleAction($estateShortName, $fromDate, $toDate, $price, $initialPrice){
        $result = array('success' => false);

        $_em = $this->getDoctrine()->getManager();

        /** @var Estate $estate */
        $estate = $_em->getRepository('UTTEstateBundle:Estate')->findOneBy(array('shortName' => $estateShortName));
        if(!($estate && $estate instanceof Estate)){
            return new JsonResponse($result);
        }

        $this->get('utt.aclService')->authEstateOwnerUser($estate);

        try { $fromDate = new \Datetime($fromDate); }catch (\Exception $e){
            return new JsonResponse($result);
        }
        try { $toDate = new \Datetime($toDate); }catch (\Exception $e){
            return new JsonResponse($result);
        }

        if($estate->getType(Estate::TYPE_FLEXIBLE_BOOKING)){
            /** @var PricingRepository $pricingRepository */
            $pricingRepository = $_em->getRepository('UTTReservationBundle:Pricing');
            $pricing = $pricingRepository->findOneBy(array(
                'estate' => $estate->getId(),
                'type' => Pricing::TYPE_FLEXIBLE_BOOKING,
                'fromDate' => $fromDate,
                'toDate' => $toDate
            ));
            if($pricing instanceof Pricing){
                $pricing->setPrice((float)$price);
                $pricing->setInitialPrice((float)$initialPrice);

                $_em->flush();
                $result = array('success' => true);

                /** @var PricingService $pricingService */
                $pricingService = $this->get('utt.pricingservice');

                $lowestPrice = $pricingService->findLowestPriceByPricingCategory($estate);
                if($lowestPrice != -1){ $estate->setLowestPrice($lowestPrice); }

                $highestPrice = $pricingService->findHighestPrice($estate);
                if($highestPrice != -1){ $estate->setHighestPrice($highestPrice); }

                $_em->flush();
            }
        }

        return new JsonResponse($result);
    }

    public function renderPricingEstateUpdateYearSelectAction(PricingCategory $pricingCategory){
        $twigArray = array();

        /** @var PricingCategoryService $pricingCategoryService */
        $pricingCategoryService = $this->get('utt.pricingcategoryservice');
        $yearArray = $pricingCategoryService->getYearsFromPricingCategory($pricingCategory);

        $twigArray['yearArray'] = $yearArray;
        return $this->render('UTTReservationBundle:Pricing:renderPricingEstateUpdateYearSelect.html.twig', $twigArray);
    }

    public function pricingEstateUpdateAction(Request $request, $estateShortName){
        $referer = $request->headers->get('referer');
        if(!$referer) $referer = $this->generateUrl('utt_index_homepage');

        $year = $request->query->get('year', null);
        $updateAction = $request->query->get('updateAction', null);

        if($year && $updateAction){
            $_em = $this->getDoctrine()->getManager();
            /** @var Estate $estate */
            $estate = $_em->getRepository('UTTEstateBundle:Estate')->findOneBy(array('shortName' => $estateShortName));
            if(!($estate && $estate instanceof Estate)){
                return $this->redirect($referer);
            }

            $this->get('utt.aclService')->authEstateOwnerUser($estate);

            /** @var PricingService $pricingService */
            $pricingService = $this->get('utt.pricingservice');

            $fromDate = new \DateTime();
            $fromDate->setDate($year, 1, 1);
            $fromDate->setTime(0,0,0);

            $updated = false;
            if($updateAction == 'round'){
                $updated = $pricingService->roundPricingForEstateForYearFromDate($estate, $fromDate);
                if($updated){
                    $this->get('session')->getFlashBag()->add('sonata_flash_success', 'Prices for current year rounded.');
                }else{
                    $this->get('session')->getFlashBag()->add('sonata_flash_error', 'Prices for current year NOT rounded.');
                }
            }elseif($updateAction == 'increasePercent5'){
                $updated = $pricingService->increasePricingForEstateForYearFromDate($estate, 5, $fromDate);
                if($updated){
                    $this->get('session')->getFlashBag()->add('sonata_flash_success', 'Prices for current year increased by 5%');
                }else{
                    $this->get('session')->getFlashBag()->add('sonata_flash_error', 'Prices for current year NOT increased.');
                }
            }elseif($updateAction == 'increasePercent10'){
                $updated = $pricingService->increasePricingForEstateForYearFromDate($estate, 10, $fromDate);
                if($updated){
                    $this->get('session')->getFlashBag()->add('sonata_flash_success', 'Prices for current year increased by 10%');
                }else{
                    $this->get('session')->getFlashBag()->add('sonata_flash_error', 'Prices for current year NOT increased.');
                }
            }elseif($updateAction == 'decreasePercent5'){
                $updated = $pricingService->decreasePricingForEstateForYearFromDate($estate, 5, $fromDate);
                if($updated){
                    $this->get('session')->getFlashBag()->add('sonata_flash_success', 'Prices for current year decreased by 5%');
                }else{
                    $this->get('session')->getFlashBag()->add('sonata_flash_error', 'Prices for current year NOT decreased.');
                }
            }elseif($updateAction == 'decreasePercent10'){
                $updated = $pricingService->decreasePricingForEstateForYearFromDate($estate, 10, $fromDate);
                if($updated){
                    $this->get('session')->getFlashBag()->add('sonata_flash_success', 'Prices for current year decreased by 10%');
                }else{
                    $this->get('session')->getFlashBag()->add('sonata_flash_error', 'Prices for current year NOT decreased.');
                }
            }

            if($updated){
                $lowestPrice = $pricingService->findLowestPriceByPricingCategory($estate);
                if($lowestPrice != -1){ $estate->setLowestPrice($lowestPrice); }

                $highestPrice = $pricingService->findHighestPrice($estate);
                if($highestPrice != -1){ $estate->setHighestPrice($highestPrice); }

                $_em->flush();
            }
        }else{
            $this->get('session')->getFlashBag()->add('sonata_flash_error', 'Prices NOT changed.');
        }

        return $this->redirect($referer);
    }
}
