<?php

namespace UTT\ReservationBundle\Controller;

use Sonata\AdminBundle\Controller\CRUDController as Controller;
use UTT\ReservationBundle\Entity\Reservation;
use UTT\ReservationBundle\Entity\ReservationRepository;
use UTT\EstateBundle\Entity\Estate;
use UTT\EstateBundle\Entity\EstateRepository;
use UTT\ReservationBundle\Model\ReservationDataDiscountPrice;
use UTT\ReservationBundle\Model\ReservationDataPrice;
use UTT\ReservationBundle\Model\ReservationDataUserData;
use UTT\ReservationBundle\Service\ReservationService;
use Symfony\Component\HttpFoundation\JsonResponse;
use UTT\UserBundle\Entity\User;
use FOS\UserBundle\Model\UserInterface;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;

class ReservationAdminController extends Controller
{
    private function userAuth($throwException = true){
        if(!$this->container->get('security.context')->isGranted('IS_AUTHENTICATED_FULLY') ){
            if($throwException){
                throw new AccessDeniedException('This user does not have access to this section.');
            }else{
                return false;
            }
        }

        $user = $this->container->get('security.context')->getToken()->getUser();
        if (!is_object($user) || !$user instanceof UserInterface) {
            if($throwException){
                throw new AccessDeniedException('This user does not have access to this section.');
            }else{
                return false;
            }
        }

        return $user;
    }

    public function showAction($id = null){
        /** @var Reservation $reservation */
        $reservation = $this->admin->getSubject();
        if($reservation){
            $this->container->get('utt.aclService')->authEstateOwnerUser($reservation->getEstate());

            $request = $this->getRequest();
            if($request->isMethod('POST')){
                $postData = $request->request->all();

                /** @var User $user */
                $user = $this->userAuth();

                /** @var ReservationService $reservationService */
                $reservationService = $this->get('utt.reservationservice');

                if(isset($postData['formName']) && $postData['formName'] == 'userDataForm'){
                    if(isset($postData['firstName']) && isset($postData['lastName']) && isset($postData['email']) && isset($postData['phone']) && isset($postData['address']) && isset($postData['city']) && isset($postData['postcode']) && isset($postData['country'])){
                        $userData = new ReservationDataUserData(json_decode(json_encode(array(
                            'firstName' => $postData['firstName'],
                            'lastName' => $postData['lastName'],
                            'email' => $postData['email'],
                            'phone' => $postData['phone'],
                            'address' => $postData['address'],
                            'city' => $postData['city'],
                            'postcode' => $postData['postcode'],
                            'country' => $postData['country']
                        ))));

                        $reservationService->assignNewUserData($reservation, $userData);
                        $this->get('session')->getFlashBag()->add('success', 'User data saved!');
                    }
                }elseif(isset($postData['formName']) && $postData['formName'] == 'updatePaymentForm'){
                    if(isset($postData['value']) && isset($postData['addPayment']) && $postData['value'] && $postData['addPayment'] == true){
                        $added = $reservationService->manualAddPayment($reservation, $postData['value']);
                        if($added){
                            $this->get('session')->getFlashBag()->add('success', 'Payment amount added to reservation!');
                        }else{
                            $this->get('session')->getFlashBag()->add('error', 'Payment NOT added. You cannot credit the account with more than is due.');
                        }
                    }elseif(isset($postData['value']) && isset($postData['refundPayment']) && $postData['value'] && $postData['refundPayment'] == true){
                        $refunded = $reservationService->manualRefundPayment($reservation, $postData['value']);
                        if($refunded){
                            $this->get('session')->getFlashBag()->add('success', 'Payment amount refunded to reservation!');
                        }else{
                            $this->get('session')->getFlashBag()->add('error', 'Payment NOT refunded. You cannot refund more than has been taken.');
                        }
                    }
                }elseif(isset($postData['formName']) && $postData['formName'] == 'referredFlagChange'){

                    $newHistoryCommentAdded = false;
                    $historyCommentText = false;
                    if($postData['historyCommentText']){
                        $historyCommentText = $postData['historyCommentText'];

                        if(isset($postData['submitEmail']) && $postData['submitEmail'] == true){
                            $newHistoryCommentAdded = $reservationService->newHistoryCommentFromAdminEmail($reservation, $user, $historyCommentText);
                        }elseif(isset($postData['submitAdd']) && $postData['submitAdd'] == true){
                            $newHistoryCommentAdded = $reservationService->newHistoryCommentFromAdminAdd($reservation, $user, $historyCommentText);
                        }elseif(isset($postData['submitHousekeeping']) && $postData['submitHousekeeping'] == true){
                            $newHistoryCommentAdded = $reservationService->newHistoryCommentFromAdminHousekeeping($reservation, $user, $historyCommentText);
                        }elseif(isset($postData['submitInternal']) && $postData['submitInternal'] == true){
                            $newHistoryCommentAdded = $reservationService->newHistoryCommentFromAdminInternal($reservation, $user, $historyCommentText);
                        }
                    }
                    if($newHistoryCommentAdded){ $this->get('session')->getFlashBag()->add('success', 'History comment added!'); }

                    $referredFlagChanged = false;
                    if(isset($postData['referredFlag'])){
                        if($postData['referredFlag'] == 'submitAdmin'){
                            $referredFlagChanged = $reservationService->referredFlagChange($reservation, $user, Reservation::REFERRED_FLAG_ADMIN, $historyCommentText);
                        }elseif($postData['referredFlag'] == 'submitLandlord'){
                            $referredFlagChanged = $reservationService->referredFlagChange($reservation, $user, Reservation::REFERRED_FLAG_LANDLORD, $historyCommentText);
                        }elseif($postData['referredFlag'] == 'submitLukaszRak'){
                            $referredFlagChanged = $reservationService->referredFlagChange($reservation, $user, Reservation::REFERRED_FLAG_LUKASZ_RAK, $historyCommentText);
                        }elseif($postData['referredFlag'] == 'submitSpot'){
                            $referredFlagChanged = $reservationService->referredFlagChange($reservation, $user, Reservation::REFERRED_FLAG_SPOT, $historyCommentText);
                        }elseif($postData['referredFlag'] == 'submitGreg'){
                            $referredFlagChanged = $reservationService->referredFlagChange($reservation, $user, Reservation::REFERRED_FLAG_GREG, $historyCommentText);
                        }elseif($postData['referredFlag'] == 'submitEleri'){
                            $referredFlagChanged = $reservationService->referredFlagChange($reservation, $user, Reservation::REFERRED_FLAG_ELERI, $historyCommentText);
                        }elseif($postData['referredFlag'] == 'submitYvonne'){
                            $referredFlagChanged = $reservationService->referredFlagChange($reservation, $user, Reservation::REFERRED_FLAG_YVONNE, $historyCommentText);
                        }elseif($postData['referredFlag'] == 'submitBethan'){
                            $referredFlagChanged = $reservationService->referredFlagChange($reservation, $user, Reservation::REFERRED_FLAG_BETHAN, $historyCommentText);
                        }elseif($postData['referredFlag'] == 'submitKatie'){
                            $referredFlagChanged = $reservationService->referredFlagChange($reservation, $user, Reservation::REFERRED_FLAG_KATIE, $historyCommentText);
                        }elseif($postData['referredFlag'] == 'submitClear'){
                            $referredFlagChanged = $reservationService->referredFlagChange($reservation, $user, null);
                        }
                    }
                    if($referredFlagChanged){ $this->get('session')->getFlashBag()->add('success', 'Referred flag changed!'); }

                    return $this->redirect($this->generateUrl('admin_utt_reservation_reservation_show', array(
                        'id' => $reservation->getId()
                    )));
                }elseif(isset($postData['formName']) && $postData['formName'] == 'addBookingProtectForm'){
                    /** @var ReservationService $reservationService */
                    $reservationService = $this->get('utt.reservationservice');

                    $assigned = $reservationService->assignBookingProtectCharge($reservation);

                    if($assigned){
                        $this->get('session')->getFlashBag()->add('success', 'Protection applied!');
                    }else{
                        $this->get('session')->getFlashBag()->add('error', 'Protection NOT applied!');
                    }

                    return $this->redirect($this->generateUrl('admin_utt_reservation_reservation_show', array(
                        'id' => $reservation->getId()
                    )));
                }elseif(isset($postData['formName']) && $postData['formName'] == 'holdForPaymentForm'){
                    /** @var ReservationService $reservationService */
                    $reservationService = $this->get('utt.reservationservice');

                    if(isset($postData['reason']) && isset($postData['hold24']) && $postData['reason'] && $postData['hold24'] == true){
                        $success = $reservationService->holdForPayment($reservation, $user, 24, $postData['reason']);
                        if($success) $this->get('session')->getFlashBag()->add('success', 'Hold for 24hrs.. Successfully applied!');
                    }elseif(isset($postData['reason']) && isset($postData['hold48']) && $postData['reason'] && $postData['hold48'] == true){
                        $success = $reservationService->holdForPayment($reservation, $user, 24, $postData['reason']);
                        if($success) $this->get('session')->getFlashBag()->add('success', 'Hold for 48hrs.. Successfully applied!');
                    }

                    return $this->redirect($this->generateUrl('admin_utt_reservation_reservation_show', array(
                        'id' => $reservation->getId()
                    )));
                }
            }
        }

        return parent::showAction($id);
    }

    public function reinstateAction(){
        /** @var Reservation $reservation */
        $reservation = $this->admin->getSubject();
        /** @var User $user */
        $user = $this->userAuth();
        if($reservation && $user){
            $this->container->get('utt.aclService')->authForReservationReinstate($reservation);

            /** @var ReservationService $reservationService */
            $reservationService = $this->get('utt.reservationservice');
            $reinstated = $reservationService->reinstate($reservation, $user);
            $reservationService->holdForPayment($reservation, $user, 24, 'Reservation reinstated');
            if($reinstated){
                $this->get('session')->getFlashBag()->add('success', 'Reservation reinstated!');
            }else{
                $this->get('session')->getFlashBag()->add('error', 'Reservation NOT reinstated.');
            }

            return $this->redirect($this->generateUrl('admin_utt_reservation_reservation_show', array(
                'id' => $reservation->getId()
            )));
        }
        return $this->redirect($this->generateUrl('sonata_admin_dashboard'));
    }

    public function cancelAction(){
        /** @var Reservation $reservation */
        $reservation = $this->admin->getSubject();
        if($reservation){
            $this->container->get('utt.aclService')->authForReservationCancel($reservation);

            if((float)$reservation->getPaid() > (float)$reservation->getDeposit()){
                $customerRefund = number_format((float)$reservation->getPaid() - (float)$reservation->getDeposit(), 2);
            }else{
                $customerRefund = 0;
            }

            $request = $this->getRequest();
            if($request->isMethod('POST')){
                /** @var User $user */
                $user = $this->userAuth();

                $postData = $request->request->all();

                /** @var ReservationService $reservationService */
                $reservationService = $this->get('utt.reservationservice');
                if(isset($postData['formName']) && $postData['formName'] == 'cancelForm'){
                    $cancelled = false;

                    if(isset($postData['notPaid']) && $postData['notPaid'] == true){
                        $cancelled = $reservationService->adminCancelNotPaid($reservation, $user);
                    }elseif(isset($postData['refundOther']) && $postData['refundOther'] == true){
                        $cancelled = $reservationService->adminCancelRefundOther($reservation, $user);
                    }elseif(isset($postData['fullRefund']) && $postData['fullRefund'] == true){
                        $cancelled = $reservationService->adminCancelFullRefund($reservation, $user);
                    }elseif(isset($postData['byUs']) && $postData['byUs'] == true){
                        $cancelled = $reservationService->adminCancelByUs($reservation, $user);
                    }elseif(isset($postData['customer']) && $postData['customer'] == true){
                        $cancelled = $reservationService->adminCancelCustomer($reservation, $user, $customerRefund);
                    }elseif(isset($postData['noRefund']) && $postData['noRefund'] == true){
                        $cancelled = $reservationService->adminCancelNoRefund($reservation, $user);
                    }

                    if($cancelled){
                        $this->get('session')->getFlashBag()->add('success', 'Reservation cancelled!');

                        return $this->redirect($this->generateUrl('admin_utt_reservation_reservation_show', array(
                            'id' => $reservation->getId()
                        )));
                    }else{
                        $this->get('session')->getFlashBag()->add('error', 'Reservation NOT cancelled.');
                    }
                }
            }

            return $this->render('UTTReservationBundle:ReservationAdmin:cancel.html.twig', array(
                'action'   => 'show',
                'object'   => $reservation,
                'elements' => $this->admin->getShow(),

                // custom params
                'customerRefund' => $customerRefund
            ));
        }
        return $this->redirect($this->generateUrl('sonata_admin_dashboard'));
    }

    public function requoteAction(){
        /** @var Reservation $reservation */
        $reservation = $this->admin->getSubject();
        /** @var User $user */
        $user = $this->userAuth();
        if($reservation && $user){
            $this->container->get('utt.aclService')->authForReservationRequote($reservation);

            $request = $this->getRequest();
            if($request->isMethod('POST') && $request->isXmlHttpRequest()){
                $result = array('success' => false);

                $requestQuery = $request->request;
                if($requestQuery->has('reservationId')) $reservationId = $requestQuery->get('reservationId'); else return new JsonResponse($result);
                if($requestQuery->has('estateShortName')) $estateShortName = $requestQuery->get('estateShortName'); else return new JsonResponse($result);
                if($requestQuery->has('fromDate')) $fromDate = $requestQuery->get('fromDate'); else return new JsonResponse($result);
                if($requestQuery->has('toDate')) $toDate = $requestQuery->get('toDate'); else return new JsonResponse($result);
                if($requestQuery->has('sleeps')) $sleeps = (int) $requestQuery->get('sleeps'); else return new JsonResponse($result);
                if($requestQuery->has('sleepsChildren')) $sleepsChildren = (int) $requestQuery->get('sleepsChildren'); else return new JsonResponse($result);
                if($requestQuery->has('pets')) $pets = (int) $requestQuery->get('pets'); else return new JsonResponse($result);
                if($requestQuery->has('infants')) $infants = (int) $requestQuery->get('infants'); else return new JsonResponse($result);
                if($requestQuery->has('price')) $price = (array) $requestQuery->get('price'); else return new JsonResponse($result);
                if($requestQuery->has('discountPrice')) $discountPrice = (array) $requestQuery->get('discountPrice'); else return new JsonResponse($result);
                if($requestQuery->has('sendHistoryEmail')) $sendHistoryEmail = (boolean) $requestQuery->get('sendHistoryEmail'); else return new JsonResponse($result);

                /** @var Estate $estate */
                $estate = $this->getDoctrine()->getManager()->getRepository('UTTEstateBundle:Estate')->findOneBy(array('shortName' => $estateShortName));
                if(!($estate && $estate instanceof Estate)){
                    return new JsonResponse($result);
                }
                try { $fromDate = new \Datetime($fromDate); }catch (\Exception $e){
                    return new JsonResponse($result);
                }
                try { $toDate = new \Datetime($toDate); }catch (\Exception $e){
                    return new JsonResponse($result);
                }
                try {
                    /** @var ReservationDataPrice $price */
                    $price = new ReservationDataPrice(json_decode(json_encode($price)));
                }catch (\Exception $e){
                    return new JsonResponse($result);
                }
                try{
                    /** @var ReservationDataDiscountPrice $discountPrice */
                    $discountPrice = new ReservationDataDiscountPrice(json_decode(json_encode($discountPrice)));
                }catch (\Exception $e){
                    return new JsonResponse($result);
                }

                if($reservation->getId() == $reservationId){
                    /** @var ReservationService $reservationService */
                    $reservationService = $this->get('utt.reservationservice');
                    $requoted = $reservationService->requote($reservation, $estate, $fromDate, $toDate, $sleeps, $sleepsChildren, $pets, $infants, $price, $discountPrice, $sendHistoryEmail, $user);
                    if($requoted){
                        return new JsonResponse(array('success' => true));
                    }
                }

                return new JsonResponse($result);
            }

            return $this->render('UTTReservationBundle:ReservationAdmin:requote.html.twig', array(
                'action'   => 'show',
                'object'   => $reservation,
                'elements' => $this->admin->getShow()
            ));
        }
        return $this->redirect($this->generateUrl('sonata_admin_dashboard'));
    }

}