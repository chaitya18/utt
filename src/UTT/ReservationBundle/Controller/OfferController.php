<?php

namespace UTT\ReservationBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use UTT\EstateBundle\Entity\Estate;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Session\Session;
use FOS\UserBundle\Model\UserInterface;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use UTT\ReservationBundle\Entity\Offer;
use UTT\ReservationBundle\Entity\OfferRepository;
use Symfony\Component\HttpFoundation\Request;
use UTT\ReservationBundle\Service\OfferService;
use UTT\AdminBundle\Service\UTTAclService;
use UTT\UserBundle\Entity\User;

class OfferController extends Controller
{
    private function userAuth($throwException = true){
        if(!$this->container->get('security.context')->isGranted('IS_AUTHENTICATED_FULLY') ){
            if($throwException){
                throw new AccessDeniedException('This user does not have access to this section.');
            }else{
                return false;
            }
        }

        $user = $this->container->get('security.context')->getToken()->getUser();
        if (!is_object($user) || !$user instanceof UserInterface) {
            if($throwException){
                throw new AccessDeniedException('This user does not have access to this section.');
            }else{
                return false;
            }
        }

        return $user;
    }

    public function createOfferForEstateAction(Request $request){
        /** @var User $user */
        $user = $this->userAuth(false);
        if(!$user) return new JsonResponse(array('success' => false));

        /** @var UTTAclService $uttAclService */
        $uttAclService = $this->container->get('utt.aclService');
        if($uttAclService->isGrantedCleaner()) return new JsonResponse(array('success' => false));

        if($request->isXmlHttpRequest()) $requestQuery = $request->request; else $requestQuery = $request->query;
        if($requestQuery->has('estateShortName')) $estateShortName = $requestQuery->get('estateShortName'); else $estateShortName = null;
        if($requestQuery->has('fromDate')) $fromDate = $requestQuery->get('fromDate'); else $fromDate = null;
        if($requestQuery->has('toDate')) $toDate = $requestQuery->get('toDate'); else $toDate = null;
        if($requestQuery->has('price')) $price = $requestQuery->get('price'); else $price = null;
        if($requestQuery->has('offerName')) $offerName = $requestQuery->get('offerName'); else $offerName = null;
        if($requestQuery->has('offerDescription')) $offerDescription = $requestQuery->get('offerDescription'); else $offerDescription = null;

        /** @var Estate $estate */
        $estate = $this->getDoctrine()->getManager()->getRepository('UTTEstateBundle:Estate')->findOneBy(array('shortName' => $estateShortName));
        if(!($estate && $estate instanceof Estate)){
            return new JsonResponse(array('success' => false));
        }
        try { $fromDate = new \Datetime($fromDate); }catch (\Exception $e){
            return new JsonResponse(array('success' => false));
        }
        try { $toDate = new \Datetime($toDate); }catch (\Exception $e){
            return new JsonResponse(array('success' => false));
        }


        /** @var OfferService $offerService */
        $offerService = $this->get('utt.offerservice');
        $createdOffer = $offerService->create($estate, Offer::TYPE_SPECIFIC, $fromDate, $toDate, $price, $offerName, $offerDescription);
        if($createdOffer instanceof Offer){
            $this->get('utt.aclService')->updateObjectAcl($createdOffer);

            return new JsonResponse(array('success' => true, 'offer' => $offerService->offerResponseArray($createdOffer)));
        }

        return new JsonResponse(array('success' => false));
    }

}
