<?php

namespace UTT\ReservationBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use UTT\EstateBundle\Entity\Estate;
use UTT\ReservationBundle\Entity\VoucherPaymentTransaction;
use UTT\ReservationBundle\Model\ReservationDataUserData;
use UTT\ReservationBundle\Service\ReservationService;
use Symfony\Component\HttpFoundation\JsonResponse;
use UTT\ReservationBundle\Entity\Reservation;
use UTT\ReservationBundle\Entity\ReservationRepository;
use Symfony\Component\HttpFoundation\Session\Session;
use UTT\ReservationBundle\Model\ReservationData;
use UTT\ReservationBundle\Model\ReservationChangeData;
use UTT\UserBundle\Entity\User;
use FOS\UserBundle\Model\UserInterface;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use UTT\ReservationBundle\Service\DiscountCodeService;
use UTT\ReservationBundle\Model\ReservationDataPrice;
use UTT\ReservationBundle\Model\ReservationDataDiscountPrice;
use UTT\ReservationBundle\Factory\VoucherFactory;
use UTT\ReservationBundle\Entity\VoucherHistoryEntry;
use UTT\ReservationBundle\Service\VoucherHistoryEntryService;
use UTT\IndexBundle\Service\EmailService;
use UTT\IndexBundle\Service\ConfigurationService;
use UTT\IndexBundle\Entity\Configuration;
use UTT\UserBundle\Factory\UserFactory;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Validator\Constraints\Email as EmailConstraint;
use UTT\ReservationBundle\Payment\SmartPayFormService;
use UTT\ReservationBundle\Service\ArrivalInstructionService;
use UTT\AdminBundle\Service\UTTAclService;
use Monolog\Logger;
use Monolog\Handler\StreamHandler;
use UTT\ReservationBundle\Service\PricingService;
use UTT\ReservationBundle\Service\VoucherService;
use UTT\ReservationBundle\Model\VoucherDataPrice;
use UTT\ReservationBundle\Model\VoucherDataUserData;
use UTT\ReservationBundle\Model\VoucherData;
use UTT\ReservationBundle\Entity\Voucher;
use UTT\ReservationBundle\Entity\VoucherRepository;
use UTT\ReservationBundle\Entity\DiscountCode;

class VoucherController extends Controller
{
    private function userAuth($throwException = true){
        if(!$this->container->get('security.context')->isGranted('IS_AUTHENTICATED_FULLY') ){
            if($throwException){
                throw new AccessDeniedException('This user does not have access to this section.');
            }else{
                return false;
            }
        }

        $user = $this->container->get('security.context')->getToken()->getUser();
        if (!is_object($user) || !$user instanceof UserInterface) {
            if($throwException){
                throw new AccessDeniedException('This user does not have access to this section.');
            }else{
                return false;
            }
        }

        return $user;
    }

    public function voucherPdfAction(Request $request, $voucherId, $pin){
        /** @var VoucherRepository $voucherRepository */
        $voucherRepository = $this->getDoctrine()->getManager()->getRepository('UTTReservationBundle:Voucher');

        /** @var Voucher $voucher */
        $voucher = $voucherRepository->findOneBy(array('id' => $voucherId, 'pin' => $pin));
        if(!($voucher instanceof Voucher)) return $this->redirect($this->generateUrl('utt_user_voucher_manage', array('voucherId' => $voucherId)));
        if(!$voucher->isAllowedToPdf()) return $this->redirect($this->generateUrl('utt_user_voucher_manage', array('voucherId' => $voucherId)));

        /** @var VoucherService $voucherService */
        $voucherService = $this->get('utt.voucherservice');
        $voucherService->generateVoucherPDF($voucher);
        exit;
    }

    public function paymentResultAction(Request $request){
        /** @var Logger $logger */
        $logger = $this->get('logger');

        $authResult = $request->query->get('authResult');
        $pspReference = $request->query->get('pspReference');
        $merchantReference = $request->query->get('merchantReference');
        $merchantSig = $request->query->get('merchantSig');
        $paymentMethod = $request->query->get('paymentMethod');
        $shopperLocale = $request->query->get('shopperLocale');
        $merchantReturnData = $request->query->get('merchantReturnData');

        $logger->notice('1 | merchantReference: '.$merchantReference.' | PAYMENT STARTED');

        if($authResult){
            /** @var SmartPayFormService $smartPayFormService */
            $smartPayFormService = $this->get('utt.smartpayformservice');
            /** @var EmailService $emailService */
            $emailService = $this->get('utt.emailservice');

            $_em = $this->getDoctrine()->getManager();
            /** @var VoucherPaymentTransaction $voucherPaymentTransaction */
            $voucherPaymentTransaction = $smartPayFormService->findOrCreateVoucherPaymentTransactionByCode($merchantReference);
            $logger->notice('2 | merchantReference: '.$merchantReference.' | PAYMENT TRANSACTION | findOrCreatePaymentTransactionByCode');
            $voucherPaymentTransaction = $smartPayFormService->assignResponseParamsToVoucherPaymentTransaction($voucherPaymentTransaction, $authResult, $pspReference, $merchantReference, $merchantSig, $paymentMethod, $shopperLocale, $merchantReturnData);
            $logger->notice('3 | merchantReference: '.$merchantReference.' | PAYMENT TRANSACTION | assignResponseParamsToPaymentTransaction');
            if($authResult == 'AUTHORISED'){
                $logger->notice('4 | merchantReference: '.$merchantReference.' | AUTHORISED');
                $voucherPaymentTransaction->setStatus(VoucherPaymentTransaction::STATUS_SUCCESS);
                $_em->persist($voucherPaymentTransaction);
                $_em->flush();
                $this->get('session')->getFlashBag()->add('success', 'Payment successful!');
                $logger->notice('5 | merchantReference: '.$merchantReference.' | PAYMENT TRANSACTION | flushed | flash added');

                /** @var Voucher $voucher */
                $voucher = $voucherPaymentTransaction->getVoucher();
                $logger->notice('6 | merchantReference: '.$merchantReference.' | VOUCHER | getVoucher');
                if($voucher instanceof Voucher){
                    $logger->notice('7 | merchantReference: '.$merchantReference.' | VOUCHER: '.$voucher->getId().' | found');
                    /** @var VoucherFactory $voucherFactory */
                    $voucherFactory = $this->get('utt.voucherfactory');
                    $voucher = $voucherFactory->addCreditCardCharge($voucher, $voucherPaymentTransaction->getAmountCreditCardCharge());
                    $voucher = $voucherFactory->addCharityCharge($voucher, $voucherPaymentTransaction->getAmountCharityCharge());
                    $voucher = $voucherFactory->addPaymentToVoucher($voucher, $voucherPaymentTransaction->getAmount());
                    $logger->notice('8 | merchantReference: '.$merchantReference.' | VOUCHER: '.$voucher->getId().' | addPaymentToVoucher | '.$voucherPaymentTransaction->getAmount());

                    $_em->persist($voucher);
                    $_em->flush();
                    $this->get('session')->getFlashBag()->add('success', 'Payment amount added to voucher!');

                    //$emailService->sendThankYouForPaymentEmail($reservation, $paymentTransaction);

                    $logger->notice('9 | merchantReference: '.$merchantReference.' | VOUCHER: '.$voucher->getId().' | flushed');

                    $user = $this->userAuth(false);
                    $logger->notice('10 | merchantReference: '.$merchantReference.' | VOUCHER: '.$voucher->getId().' | userAuth');
                    if(!($user instanceof User)){
                        $user = $voucher->getUser();
                        $logger->notice('10a | merchantReference: '.$merchantReference.' | VOUCHER: '.$voucher->getId().' | get user from voucher');
                    }
                    if($user instanceof User){
                        $logger->notice('11 | merchantReference: '.$merchantReference.' | VOUCHER: '.$voucher->getId().' | user instanceof User');
                        // add entry to history
                        /** @var VoucherHistoryEntryService $voucherHistoryEntryService */
                        $voucherHistoryEntryService = $this->get('utt.voucherhistoryentryservice');
                        $voucherHistoryEntryComment = $voucherHistoryEntryService->newCommentCardPayment($voucher->getTotalPrice(), $voucherPaymentTransaction->getTypeName(), $voucherPaymentTransaction->getAmount(), $voucherPaymentTransaction->getSmartPayAuthResult(), $voucherPaymentTransaction->getSmartPayPaymentMethod(), $voucherPaymentTransaction->getSmartPayPspReference());
                        $voucherHistoryEntryService->newEntry(VoucherHistoryEntry::TYPE_CARD_PAYMENT, $voucher, $user, $voucherHistoryEntryComment);
                        $logger->notice('12 | merchantReference: '.$merchantReference.' | VOUCHER: '.$voucher->getId().' | added history entry | '.$voucherHistoryEntryComment);
                    }


                    if($voucher->getPaid() == $voucher->getTotalPrice()){
                        $logger->notice('14 | merchantReference: '.$merchantReference.' | VOUCHER: '.$voucher->getId().' | paid == totalprice');
                        /** @var DiscountCodeService $discountCodeService */
                        $discountCodeService = $this->get('utt.discountcodeservice');
                        /** @var DiscountCode $discountCode */
                        $discountCode = $discountCodeService->createVoucherDiscount($voucher);
                        if($discountCode instanceof DiscountCode){
                            $voucher->setDiscountCode($discountCode);
                            $_em->flush();

                            $emailSent = $emailService->sendDiscountCodeEmail($user->getEmail(), $discountCode);
                            if($emailSent){
                                /** @var VoucherHistoryEntryService $voucherHistoryEntryService */
                                $voucherHistoryEntryService = $this->get('utt.voucherhistoryentryservice');
                                $voucherHistoryEntryComment = $voucherHistoryEntryService->newCommentVoucherDiscount($discountCode);
                                $voucherHistoryEntryService->newEntry(VoucherHistoryEntry::TYPE_VOUCHER_DISCOUNT_CODE, $voucher, $user, $voucherHistoryEntryComment);
                            }
                        }
                    }

                    return $this->redirect($this->generateUrl('utt_user_voucher_manage', array(
                        'voucherId' => $voucher->getId()
                    )));
                }
            }else{
                $twigArray = array();
                $voucherPaymentTransaction->setStatus(VoucherPaymentTransaction::STATUS_NOT_SUCCESS);
                $_em->persist($voucherPaymentTransaction);
                $_em->flush();

                switch ($authResult){
                    case 'REFUSED':
                        $twigArray['statusText'] = 'Authorisation was unsuccessful, Declined.';
                        break;
                    case 'CANCELLED':
                        $twigArray['statusText'] = 'Payment cancelled.';
                        break;
                    case 'PENDING':
                        $twigArray['statusText'] = 'Pending.';
                        break;
                    case 'ERROR':
                        $twigArray['statusText'] = 'An error occurred during the payment process';
                        break;
                    default:
                        $twigArray['statusText'] = 'An unknown error has occured. Please wait a while and then try again to make your payment.';
                        break;
                }

                /** @var Voucher $voucher */
                $voucher = $voucherPaymentTransaction->getVoucher();
                if($voucher instanceof Voucher){
                    $user = $this->userAuth(false);
                    if($user instanceof User){
                        // add entry to history
                        /** @var VoucherHistoryEntryService $voucherHistoryEntryService */
                        $voucherHistoryEntryService = $this->get('utt.voucherhistoryentryservice');
                        $voucherHistoryEntryComment = $voucherHistoryEntryService->newCommentCardFailed($authResult);
                        $voucherHistoryEntryService->newEntry(VoucherHistoryEntry::TYPE_CARD_FAILED, $voucher, $user, $voucherHistoryEntryComment);
                    }
                }

                return $this->render('UTTReservationBundle:Reservation:paymentFailure.html.twig', $twigArray);
            }
        }



        exit;
    }

    public function renderSmartPayFormForVoucherAction(Voucher $voucher, $applyCreditCardCharge = false, $charityCharge = 0){
        $twigArray = array();

        /** @var SmartPayFormService $smartPayFormService */
        $smartPayFormService = $this->get('utt.smartpayformservice');

        /** @var PricingService $pricingService */
        $pricingService = $this->get('utt.pricingservice');

        if($voucher->getPaid() < $voucher->getTotalPrice()){
            $toPay = $voucher->getTotalPrice() - $voucher->getPaid();

            $creditCardCharge = 0;
            if($applyCreditCardCharge){
                $creditCardCharge = $pricingService->calculateCreditCardCharge($toPay);
                $toPay = (float)$toPay + (float)$creditCardCharge;

                $twigArray['applyCreditCardCharge'] = $applyCreditCardCharge;
                $twigArray['creditCardCharge'] = $creditCardCharge;
            }
            if($charityCharge){
                $toPay = (float)$toPay + (float)$charityCharge;

                $twigArray['charityCharge'] = $charityCharge;
            }

            $transactionCode = $smartPayFormService->generateTransactionCodeVoucher($voucher);

            $twigArray['smartPayUrl'] = $smartPayFormService->getUrl();
            $twigArray['merchantReference'] = $transactionCode;
            $twigArray['paymentAmount'] = $toPay * 100;
            $twigArray['currencyCode'] = $smartPayFormService->getCurrencyCode();
            $twigArray['shipBeforeDate'] = date("Ymd" , mktime(date("H"), date("i"), date("s"), date("m"), date("j")+5, date("Y")));
            $twigArray['skinCode'] = $smartPayFormService->getSkinCodeVoucher();
            $twigArray['merchantAccount'] = $smartPayFormService->getMerchantAccount();
            $twigArray['shopperLocale'] = $smartPayFormService->getShopperLocale();
            $twigArray['orderData'] = $smartPayFormService->generateOrderDataVoucher($voucher);
            $twigArray['sessionValidity'] = date(DATE_ATOM	, mktime(date("H")+1, date("i"), date("s"), date("m"), date("j"), date("Y")));
            $twigArray['shopperEmail'] = $voucher->getUserDataDecoded()->email;
            $twigArray['shopperReference'] = $voucher->getUser()->getId();

            $twigArray['merchantSig'] = $smartPayFormService->generateMerchantSigVoucher(
                $twigArray['paymentAmount'],
                $twigArray['shipBeforeDate'],
                $twigArray['merchantReference'],
                $twigArray['sessionValidity'],
                $twigArray['shopperEmail'],
                $twigArray['shopperReference']
            );

            // create transaction
            $voucherPaymentTransaction = new VoucherPaymentTransaction();
            $voucherPaymentTransaction->setStatus(VoucherPaymentTransaction::STATUS_PREPARED);
            $voucherPaymentTransaction->setVoucher($voucher);
            $voucherPaymentTransaction->setCode($transactionCode);
            $voucherPaymentTransaction->setAmount($toPay);
            $voucherPaymentTransaction->setAmountCreditCardCharge($creditCardCharge);
            $voucherPaymentTransaction->setAmountCharityCharge($charityCharge);
            if($applyCreditCardCharge){
                $voucherPaymentTransaction->setType(VoucherPaymentTransaction::TYPE_CREDIT_CARD);
            }else{
                $voucherPaymentTransaction->setType(VoucherPaymentTransaction::TYPE_DEBIT_CARD);
            }
            $_em = $this->getDoctrine()->getManager();
            $_em->persist($voucherPaymentTransaction);
            $_em->flush();
        }

        return $this->render('UTTReservationBundle:Reservation:renderSmartPayForm.html.twig', $twigArray);
    }

    public function renderVoucherAction($voucherValue = null, $voucherName = null, \DateTime $expiresAt = null){
        return $this->render('UTTReservationBundle:Voucher:renderVoucher.html.twig', array(
            'voucherValue' => $voucherValue,
            'voucherName' => $voucherName,
            'expiresAt' => $expiresAt
        ));
    }

    public function confirmAction(Request $request){
        //$this->get('utt.activityLogService')->createForCurrentRoute();

        /** @var User $user */
        $user = $this->userAuth();

        /** @var VoucherService $voucherService */
        $voucherService = $this->get('utt.voucherservice');
        /** @var ConfigurationService $configurationService */
        $configurationService = $this->get('utt.configurationservice');

        /** @var Configuration $configuration */
        $configuration = $configurationService->get();
        /** @var VoucherData $voucherData */
        $voucherData = $voucherService->getVoucherData();
        if($voucherData && $configuration instanceof Configuration){
            $emailErrors = $this->get('validator')->validateValue($voucherData->getUserData()->getEmail(), new EmailConstraint());
            if (count($emailErrors) > 0) {
                $this->get('session')->getFlashBag()->add('error', 'E-mail is not valid. Please type correct e-mail address.');

                return $this->redirect($this->generateUrl('utt_voucher_new', array(
                    'voucherValue' => $voucherData->getValue()
                )));
            }

            $voucherValue = (int) $voucherData->getValue();
            $voucherName = $voucherData->getName();

            /** @var VoucherDataPrice $newVoucherPrice */
            $newVoucherPrice = $voucherService->newVoucherPrice($voucherValue);
            /** @var VoucherDataPrice $newVoucherPrice */
            $newVoucherPriceConfiguration = $voucherService->newVoucherPrice($voucherValue, $configuration);
            if($newVoucherPrice instanceof VoucherDataPrice && $newVoucherPriceConfiguration instanceof VoucherDataPrice){
                if($voucherData->getPrice()->isEqual($newVoucherPrice) || $voucherData->getPrice()->isEqual($newVoucherPriceConfiguration)){
                    if($request->isMethod('POST')){
                        /** @var Voucher $bookedVoucher */
                        $bookedVoucher = $voucherService->book($voucherValue, $voucherName, $user, $voucherData->getPrice(), $voucherData->getUserData());

                        if($bookedVoucher instanceOf Voucher){
                            $this->get('session')->getFlashBag()->add('success', 'Congratulations. Your voucher has been reserved. You have to make payment to receive your discount code. See below...');
                            $this->get('session')->getFlashBag()->add('success', 'We have just emailed you your confirmation. If you do not receive your confirmation in the next hour then please check your SPAM or JUNK folders, and if you experience any problem please email enquiries@underthethatch.co.uk or call 01239 727 029 with your booking reference.');
                            return $this->redirect($this->generateUrl('utt_user_voucher_manage', array(
                                'voucherId' => $bookedVoucher->getId()
                            )));
                        }
                    }else{
                        $twigArray = array();
                        $twigArray['voucherValue'] = $voucherValue;
                        $twigArray['voucherName'] = $voucherName;
                        $twigArray['price'] = $voucherData->getPrice();
                        $twigArray['userData'] = $voucherData->getUserData();
                        $twigArray['totalPrice'] = (float) $voucherData->getPrice()->getTotal();

                        return $this->render('UTTReservationBundle:Voucher:confirm.html.twig', $twigArray);
                    }
                }
            }
        }

        return $this->redirect($this->generateUrl('utt_index_homepage'));
    }

    public function guestConfirmAction(Request $request){
        $this->get('utt.activityLogService')->createForCurrentRoute();

        $response = new RedirectResponse($this->generateUrl('utt_voucher_confirm'));

        /** @var VoucherService $voucherService */
        $voucherService = $this->get('utt.voucherservice');
        /** @var VoucherData $voucherData */
        $voucherData = $voucherService->getVoucherData();
        if($voucherData){
            $emailErrors = $this->get('validator')->validateValue($voucherData->getUserData()->getEmail(), new EmailConstraint());
            if (count($emailErrors) > 0) {
                $this->get('session')->getFlashBag()->add('error', 'E-mail is not valid. Please type correct e-mail address.');

                return $this->redirect($this->generateUrl('utt_voucher_new', array(
                    'voucherValue' => $voucherData->getValue()
                )));
            }

            $user = $this->getDoctrine()->getManager()->getRepository('UTTUserBundle:User')->findOneBy(array(
                'email' => $voucherData->getUserData()->getEmail()
            ));
            if(!($user instanceof User)){
                /** @var UserFactory $userFactory */
                $userFactory = $this->get('utt.userfactory');
                $user = $userFactory->create(
                    $voucherData->getUserData()->getEmail(),
                    $voucherData->getUserData()->getFirstName(),
                    $voucherData->getUserData()->getLastName(),
                    $userFactory->generateTemporaryPassword($voucherData->getUserData()->getEmail())
                );
                if($user instanceof User){
                    $this->authenticateUser($user, $response);
                }
            }else{
                $this->get('session')->getFlashBag()->add('error', 'There is already an account connected to your e-mail. <br/> Please log in below to continue. <br/> Reset your password using button below if you have forgotten.');
            }
        }

        return $response;
    }

    /**
     * Authenticate a user with Symfony Security
     *
     * @param \FOS\UserBundle\Model\UserInterface        $user
     * @param \Symfony\Component\HttpFoundation\Response $response
     */
    protected function authenticateUser(UserInterface $user, Response $response)
    {
        try {
            $this->container->get('fos_user.security.login_manager')->loginUser(
                $this->container->getParameter('fos_user.firewall_name'),
                $user,
                $response);
        } catch (AccountStatusException $ex) {
            // We simply do not authenticate users which do not pass the user
            // checker (not enabled, expired, etc.).
        }
    }

    public function createAction(Request $request){
        if($request->isXmlHttpRequest()) $requestQuery = $request->request; else $requestQuery = $request->query;
        if($requestQuery->has('userData')) $userData = $requestQuery->get('userData'); else $userData = null;
        if($requestQuery->has('price')) $price = $requestQuery->get('price'); else $price = null;
        if($requestQuery->has('voucherValue')) $voucherValue = $requestQuery->get('voucherValue'); else $voucherValue = null;
        if($requestQuery->has('voucherName')) $voucherName = $requestQuery->get('voucherName'); else $voucherName = null;

        if(!is_array($userData)) $userData = array();
        if(!is_array($price)) $price = array();

        /** @var VoucherService $voucherService */
        $voucherService = $this->get('utt.voucherservice');
        $voucherService->saveVoucherData($voucherValue, $voucherName, $userData, $price);

        return new JsonResponse(array('success' => true));
    }

    public function priceAction(Request $request){
        $result = array('success' => false);

        if($request->isXmlHttpRequest()) $requestQuery = $request->request; else $requestQuery = $request->query;
        if($requestQuery->has('voucherValue')) $voucherValue = (int) $requestQuery->get('voucherValue'); else $voucherValue = null;
        if($requestQuery->has('isVouchersOffer')) $isVouchersOffer = (boolean) $requestQuery->get('isVouchersOffer'); else $isVouchersOffer = false;

        if(is_null($voucherValue)){ $voucherValue = 0; }

        /** @var VoucherService $voucherService */
        $voucherService = $this->get('utt.voucherservice');
        /** @var VoucherDataPrice $newVoucherPrice */
        $newVoucherPrice = $voucherService->newVoucherPrice($voucherValue);
        if($isVouchersOffer){
            /** @var ConfigurationService $configurationService */
            $configurationService = $this->get('utt.configurationservice');
            /** @var Configuration $configuration */
            $configuration = $configurationService->get();

            if($configuration instanceOf Configuration){
                /** @var VoucherDataPrice $newVoucherPrice */
                $newVoucherPrice = $voucherService->newVoucherPrice($voucherValue, $configuration);
            }
        }

        if($newVoucherPrice instanceof VoucherDataPrice){
            $result = array('success' => true, 'newVoucherPrice' => $newVoucherPrice);
        }

        return new JsonResponse($result);
    }

    public function voucherNewAction($voucherValue, $isVouchersOffer = false){
        $twigArray = array('voucherValue' => $voucherValue);

        /** @var User $user */
        $user = $this->userAuth(false);

        /** @var VoucherService $voucherService */
        $voucherService = $this->get('utt.voucherservice');

        /** @var VoucherDataPrice $newVoucherPrice */
        $newVoucherPrice = $voucherService->newVoucherPrice($voucherValue);
        if($isVouchersOffer){
            /** @var ConfigurationService $configurationService */
            $configurationService = $this->get('utt.configurationservice');
            /** @var Configuration $configuration */
            $configuration = $configurationService->get();

            if($configuration instanceOf Configuration){
                /** @var VoucherDataPrice $newVoucherPrice */
                $newVoucherPrice = $voucherService->newVoucherPrice($voucherValue, $configuration);
            }
        }

        if(!($newVoucherPrice instanceof VoucherDataPrice)) return $this->redirect($this->generateUrl('utt_vouchers'));
        $twigArray['newVoucherPrice'] = json_encode($newVoucherPrice);

        $userData = new VoucherDataUserData();

        /** @var VoucherData $voucherData */
        $voucherData = $voucherService->getVoucherData();
        if($voucherData){
            $userData = $voucherData->getUserData();
        }elseif($user){
            $userData->setFirstName($user->getFirstName());
            $userData->setLastName($user->getLastName());
            $userData->setEmail($user->getEmail());
            $userData->setPhone($user->getPhone());
            $userData->setAddress($user->getAddress());
            $userData->setCity($user->getCity());
            $userData->setPostcode($user->getPostcode());
            if($user->getCountry()){
                $userData->setCountry($user->getCountry()->getCode());
            }
        }

        $twigArray['userData'] = json_encode($userData);
        $twigArray['isVouchersOffer'] = $isVouchersOffer;

        return $this->render('UTTReservationBundle:Voucher:voucherNew.html.twig', $twigArray);
    }

    public function vouchersAction(){
        // $voucherValues = Voucher::getAllowedValuesStatic();

        // return $this->render('UTTReservationBundle:Voucher:vouchers.html.twig', array(
        //     'voucherValues' => $voucherValues
        // ));
    }

    public function vouchersOffersAction(){
        $voucherValues = Voucher::getAllowedValuesStatic();

        return $this->render('UTTReservationBundle:Voucher:vouchers.html.twig', array(
            'voucherValues' => $voucherValues,
            'isVouchersOffer' => true
        ));
    }
}
