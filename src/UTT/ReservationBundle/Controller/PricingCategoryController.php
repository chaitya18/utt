<?php

namespace UTT\ReservationBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use UTT\EstateBundle\Entity\Estate;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Session\Session;
use FOS\UserBundle\Model\UserInterface;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use UTT\ReservationBundle\Entity\PricingRepository;
use UTT\ReservationBundle\Entity\Pricing;
use UTT\ReservationBundle\Entity\PricingCategory;
use UTT\ReservationBundle\Entity\PricingCategoryRepository;
use UTT\ReservationBundle\Service\PricingService;

class PricingCategoryController extends Controller
{
    public function adminEditAction($id){
        return $this->redirect($this->generateUrl('admin_utt_reservation_pricingcategory_edit', array(
            'id' => $id
        )));
    }

    public function renderPricingCategoriesSelectForEstateTypeAction($estateType){
        $twigArray = array();

        /** @var PricingCategoryRepository $pricingCategoryRepository */
        $pricingCategoryRepository = $this->getDoctrine()->getManager()->getRepository('UTTReservationBundle:PricingCategory');

        if($estateType == Estate::TYPE_FLEXIBLE_BOOKING){
            $pricingCategories = $pricingCategoryRepository->findBy(array(
                'type' => PricingCategory::TYPE_FLEXIBLE_BOOKING
            ));
            if($pricingCategories) $twigArray['pricingCategories'] = $pricingCategories;
        }elseif($estateType == Estate::TYPE_STANDARD_M_F){
            $pricingCategories = $pricingCategoryRepository->findBy(array(
                'type' => PricingCategory::TYPE_STANDARD_M_F
            ));
            if($pricingCategories) $twigArray['pricingCategories'] = $pricingCategories;
        }

        return $this->render('UTTReservationBundle:PricingCategory:renderPricingCategoriesSelectForEstateType.html.twig', $twigArray);
    }
}
