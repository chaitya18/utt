<?php

namespace UTT\ReservationBundle\Controller;

use Sonata\AdminBundle\Controller\CRUDController as Controller;
use UTT\ReservationBundle\Entity\DiscountCode;
use UTT\EstateBundle\Entity\Estate;

class DiscountCodeAdminController extends Controller
{
    public function editAction($id = null){
        /** @var DiscountCode $discountCode */
        $discountCode = $this->admin->getSubject();
        if($discountCode){
            /** @var Estate $estate */
            foreach($discountCode->getEstates() as $estate){
                $this->container->get('utt.aclService')->authEstateOwnerUser($estate);
            }
        }

        return parent::editAction($id);
    }

    public function deleteAction($id){
        /** @var DiscountCode $discountCode */
        $discountCode = $this->admin->getSubject();
        if($discountCode){
            /** @var Estate $estate */
            foreach($discountCode->getEstates() as $estate){
                $this->container->get('utt.aclService')->authEstateOwnerUser($estate);
            }
        }

        return parent::deleteAction($id);
    }

}