<?php

namespace UTT\ReservationBundle\Model;

use UTT\ReservationBundle\Model\VoucherDataUserData;
use UTT\ReservationBundle\Model\VoucherDataPrice;

class VoucherData
{
    /** @var integer $value */
    public $value;

    /** @var string $name */
    public $name;

    /** @var VoucherDataUserData $userData */
    public $userData;

    /** @var VoucherDataPrice $price */
    public $price;

    /**
     * @param \UTT\ReservationBundle\Model\VoucherDataPrice $price
     */
    public function setPrice($price)
    {
        $this->price = $price;
    }

    /**
     * @return \UTT\ReservationBundle\Model\VoucherDataPrice
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * @return int
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * @param int $value
     */
    public function setValue($value)
    {
        $this->value = $value;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @param \UTT\ReservationBundle\Model\VoucherDataUserData $userData
     */
    public function setUserData($userData)
    {
        $this->userData = $userData;
    }

    /**
     * @return \UTT\ReservationBundle\Model\VoucherDataUserData
     */
    public function getUserData()
    {
        return $this->userData;
    }

}
