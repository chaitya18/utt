<?php

namespace UTT\ReservationBundle\Model;

class ReservationDataEstateData {
    /** @var string $name */
    public $name;

    public function __construct(\StdClass $estateData = null){
        if(isset($estateData->name)) $this->setName($estateData->name);
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }
}