<?php

namespace UTT\ReservationBundle\Model;

use UTT\ReservationBundle\Model\ReservationDataDiscountPrice;
use UTT\ReservationBundle\Model\ReservationDataBase as BaseModel;

class ReservationData extends BaseModel
{
    /** @var string $estateShortName */
    public $estateShortName;

    /** @var ReservationDataDiscountPrice $discountPrice */
    public $discountPrice;

    /** @var string $code */
    public $code;

    /** @var boolean $isBookingProtect */
    public $isBookingProtect;

    /**
     * @param string $estateShortName
     */
    public function setEstateShortName($estateShortName)
    {
        $this->estateShortName = $estateShortName;
    }

    /**
     * @return string
     */
    public function getEstateShortName()
    {
        return $this->estateShortName;
    }

    /**
     * @param string $code
     */
    public function setCode($code)
    {
        $this->code = $code;
    }

    /**
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * @param \UTT\ReservationBundle\Model\ReservationDataDiscountPrice $discountPrice
     */
    public function setDiscountPrice($discountPrice)
    {
        $this->discountPrice = $discountPrice;
    }

    /**
     * @return \UTT\ReservationBundle\Model\ReservationDataDiscountPrice
     */
    public function getDiscountPrice()
    {
        return $this->discountPrice;
    }

    /**
     * @return boolean
     */
    public function isIsBookingProtect()
    {
        return $this->isBookingProtect;
    }

    /**
     * @param boolean $isBookingProtect
     */
    public function setIsBookingProtect($isBookingProtect)
    {
        $this->isBookingProtect = $isBookingProtect;
    }
}
