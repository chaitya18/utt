<?php

namespace UTT\ReservationBundle\Model;

class ReservationDataUserData {
    /** @var string $firstName */
    public $firstName;

    /** @var string $lastName */
    public $lastName;

    /** @var string $email */
    public $email;

    /** @var string $phone */
    public $phone;

    /** @var string $address */
    public $address;

    /** @var string $city */
    public $city;

    /** @var string $postcode */
    public $postcode;

    /** @var string $country */
    public $country;

    public function __construct(\StdClass $userData = null){
        if(isset($userData->firstName)) $this->setFirstName($userData->firstName);
        if(isset($userData->lastName)) $this->setLastName($userData->lastName);
        if(isset($userData->email)) $this->setEmail($userData->email);
        if(isset($userData->phone)) $this->setPhone($userData->phone);
        if(isset($userData->address)) $this->setAddress($userData->address);
        if(isset($userData->city)) $this->setCity($userData->city);
        if(isset($userData->postcode)) $this->setPostcode($userData->postcode);
        if(isset($userData->country)) $this->setCountry($userData->country);
    }

    /**
     * @param string $address
     */
    public function setAddress($address)
    {
        $this->address = $address;
    }

    /**
     * @return string
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * @param string $city
     */
    public function setCity($city)
    {
        $this->city = $city;
    }

    /**
     * @return string
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * @param string $country
     */
    public function setCountry($country)
    {
        $this->country = $country;
    }

    /**
     * @return string
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * @param string $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param string $phone
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;
    }

    /**
     * @return string
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * @param string $postcode
     */
    public function setPostcode($postcode)
    {
        $this->postcode = $postcode;
    }

    /**
     * @return string
     */
    public function getPostcode()
    {
        return $this->postcode;
    }

    /**
     * @param string $firstName
     */
    public function setFirstName($firstName)
    {
        $this->firstName = $firstName;
    }

    /**
     * @return string
     */
    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     * @param string $lastName
     */
    public function setLastName($lastName)
    {
        $this->lastName = $lastName;
    }

    /**
     * @return string
     */
    public function getLastName()
    {
        return $this->lastName;
    }

    /**
     * @return array
     */
    public function toArrayEscaped()
    {
        $result = array();
        $result['firstName'] = $this->escape($this->getFirstName());
        $result['lastName'] = $this->escape($this->getLastName());
        $result['email'] = $this->escape($this->getEmail());
        $result['phone'] = $this->escape($this->getPhone());
        $result['address'] = $this->escape($this->getAddress());
        $result['city'] = $this->escape($this->getCity());
        $result['postcode'] = $this->escape($this->getPostcode());
        $result['country'] = $this->escape($this->getCountry());
        return $result;
    }

    protected function escape($data)
    {
        return htmlspecialchars($data, ENT_QUOTES | ENT_HTML401);
    }


}