<?php

namespace UTT\ReservationBundle\Model;

class VoucherDataPrice {
    /** @var float $basePrice */
    public $basePrice;

    /** @var float $creditCardCharge */
    public $creditCardCharge;

    /** @var float $charityCharge */
    public $charityCharge;

    /** @var float $total */
    public $total;

    public function __construct(\StdClass $priceData = null){
        $this->basePrice = 0;
        $this->creditCardCharge = 0;
        $this->charityCharge = 0;
        $this->total = 0;

        if(isset($priceData->basePrice)) $this->setBasePrice($priceData->basePrice);
        if(isset($priceData->creditCardCharge)) $this->setCreditCardCharge($priceData->creditCardCharge);
        if(isset($priceData->charityCharge)) $this->setCharityCharge($priceData->charityCharge);
        if(isset($priceData->total)) $this->setTotal($priceData->total);
    }

    public function isEqual(VoucherDataPrice $object){
        if($this->getCreditCardCharge() != $object->getCreditCardCharge()) return false;
        if($this->getCharityCharge() != $object->getCharityCharge()) return false;
        if($this->getBasePrice() != $object->getBasePrice()) return false;
        if($this->getTotal() != $object->getTotal()) return false;

        return true;
    }

    /**
     * @return float
     */
    public function getCreditCardCharge()
    {
        return $this->creditCardCharge;
    }

    /**
     * @param float $creditCardCharge
     */
    public function setCreditCardCharge($creditCardCharge)
    {
        $this->creditCardCharge = $creditCardCharge;
    }

    /**
     * @return float
     */
    public function getCharityCharge()
    {
        return $this->charityCharge;
    }

    /**
     * @param float $charityCharge
     */
    public function setCharityCharge($charityCharge)
    {
        $this->charityCharge = $charityCharge;
    }

    /**
     * @param float $basePrice
     */
    public function setBasePrice($basePrice)
    {
        $this->basePrice = $basePrice;
    }

    /**
     * @return float
     */
    public function getBasePrice()
    {
        return $this->basePrice;
    }

    /**
     * @param float $total
     */
    public function setTotal($total)
    {
        $this->total = $total;
    }

    /**
     * @return float
     */
    public function getTotal()
    {
        return $this->total;
    }
}