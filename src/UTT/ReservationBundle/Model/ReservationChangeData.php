<?php

namespace UTT\ReservationBundle\Model;

use UTT\ReservationBundle\Model\ReservationDataBase as BaseModel;

class ReservationChangeData extends BaseModel
{
    /** @var string $reservationId */
    public $reservationId;

    /**
     * @param string $reservationId
     */
    public function setReservationId($reservationId)
    {
        $this->reservationId = $reservationId;
    }

    /**
     * @return string
     */
    public function getReservationId()
    {
        return $this->reservationId;
    }
}
