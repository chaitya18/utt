<?php

namespace UTT\ReservationBundle\Model;

use UTT\ReservationBundle\Model\ReservationDataUserData;
use UTT\ReservationBundle\Model\ReservationDataPrice;

class ReservationDataBase
{
    /** @var string $fromDate */
    public $fromDate;

    /** @var string $toDate */
    public $toDate;

    /** @var integer $sleeps */
    public $sleeps;

    /** @var integer $sleepsChildren */
    public $sleepsChildren;

    /** @var integer $pets */
    public $pets;

    /** @var integer $infants */
    public $infants;

    /** @var ReservationDataUserData $userData */
    public $userData;

    /** @var ReservationDataPrice $price */
    public $price;

    /**
     * @param string $fromDate
     */
    public function setFromDate($fromDate)
    {
        $this->fromDate = $fromDate;
    }

    /**
     * @return string
     */
    public function getFromDate()
    {
        return $this->fromDate;
    }

    /**
     * @param int $infants
     */
    public function setInfants($infants)
    {
        $this->infants = $infants;
    }

    /**
     * @return int
     */
    public function getInfants()
    {
        return $this->infants;
    }

    /**
     * @param int $pets
     */
    public function setPets($pets)
    {
        $this->pets = $pets;
    }

    /**
     * @return int
     */
    public function getPets()
    {
        return $this->pets;
    }

    /**
     * @param \UTT\ReservationBundle\Model\ReservationDataPrice $price
     */
    public function setPrice($price)
    {
        $this->price = $price;
    }

    /**
     * @return \UTT\ReservationBundle\Model\ReservationDataPrice
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * @param int $sleeps
     */
    public function setSleeps($sleeps)
    {
        $this->sleeps = $sleeps;
    }

    /**
     * @return int
     */
    public function getSleeps()
    {
        return $this->sleeps;
    }

    /**
     * @return int
     */
    public function getSleepsChildren()
    {
        return $this->sleepsChildren;
    }

    /**
     * @param int $sleepsChildren
     */
    public function setSleepsChildren($sleepsChildren)
    {
        $this->sleepsChildren = $sleepsChildren;
    }

    /**
     * @param string $toDate
     */
    public function setToDate($toDate)
    {
        $this->toDate = $toDate;
    }

    /**
     * @return string
     */
    public function getToDate()
    {
        return $this->toDate;
    }

    /**
     * @param \UTT\ReservationBundle\Model\ReservationDataUserData $userData
     */
    public function setUserData($userData)
    {
        $this->userData = $userData;
    }

    /**
     * @return \UTT\ReservationBundle\Model\ReservationDataUserData
     */
    public function getUserData()
    {
        return $this->userData;
    }

}
