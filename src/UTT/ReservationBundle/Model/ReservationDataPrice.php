<?php

namespace UTT\ReservationBundle\Model;

class ReservationDataPrice {
    /** @var float $basePrice */
    public $basePrice;

    /** @var float $additionalSleepsPrice */
    public $additionalSleepsPrice;

    /** @var float $adminCharge */
    public $adminCharge;

    /** @var float $creditCardCharge */
    public $creditCardCharge;

    /** @var float $bookingProtectCharge */
    public $bookingProtectCharge;

    /** @var float $charityCharge */
    public $charityCharge;

    /** @var float $petsPrice */
    public $petsPrice;

    /** @var float $total */
    public $total;

    /** @var boolean $isSpecialOffer */
    public $isSpecialOffer;

    public function __construct(\StdClass $priceData = null){
        $this->basePrice = 0;
        $this->additionalSleepsPrice = 0;
        $this->adminCharge = 0;
        $this->creditCardCharge = 0;
        $this->bookingProtectCharge = 0;
        $this->charityCharge = 0;
        $this->petsPrice = 0;
        $this->total = 0;
        $this->isSpecialOffer = false;

        if(isset($priceData->basePrice)) $this->setBasePrice($priceData->basePrice);
        if(isset($priceData->additionalSleepsPrice)) $this->setAdditionalSleepsPrice($priceData->additionalSleepsPrice);
        if(isset($priceData->adminCharge)) $this->setAdminCharge($priceData->adminCharge);
        if(isset($priceData->creditCardCharge)) $this->setCreditCardCharge($priceData->creditCardCharge);
        if(isset($priceData->bookingProtectCharge)) $this->setBookingProtectCharge($priceData->bookingProtectCharge);
        if(isset($priceData->charityCharge)) $this->setCharityCharge($priceData->charityCharge);
        if(isset($priceData->petsPrice)) $this->setPetsPrice($priceData->petsPrice);
        if(isset($priceData->total)) $this->setTotal($priceData->total);
    }

    public function isEqual(ReservationDataPrice $object){
        if($this->getAdditionalSleepsPrice() != $object->getAdditionalSleepsPrice()) return false;
        if(round($this->getAdminCharge(),2) != round($object->getAdminCharge(),2)) return false;
        if($this->getCreditCardCharge() != $object->getCreditCardCharge()) return false;
        if($this->getBookingProtectCharge() != $object->getBookingProtectCharge()) return false;
        if($this->getCharityCharge() != $object->getCharityCharge()) return false;
        if($this->getBasePrice() != $object->getBasePrice()) return false;
        if($this->getPetsPrice() != $object->getPetsPrice()) return false;
        if($this->getTotal() != $object->getTotal()) return false;

        return true;
    }

    /**
     * @param float $additionalSleepsPrice
     */
    public function setAdditionalSleepsPrice($additionalSleepsPrice)
    {
        $this->additionalSleepsPrice = $additionalSleepsPrice;
    }

    /**
     * @return float
     */
    public function getAdditionalSleepsPrice()
    {
        return $this->additionalSleepsPrice;
    }

    /**
     * @param float $adminCharge
     */
    public function setAdminCharge($adminCharge)
    {
        $this->adminCharge = $adminCharge;
    }

    /**
     * @return float
     */
    public function getAdminCharge()
    {
        return $this->adminCharge;
    }

    /**
     * @return float
     */
    public function getCreditCardCharge()
    {
        return $this->creditCardCharge;
    }

    /**
     * @param float $creditCardCharge
     */
    public function setCreditCardCharge($creditCardCharge)
    {
        $this->creditCardCharge = $creditCardCharge;
    }

    /**
     * @return float
     */
    public function getBookingProtectCharge()
    {
        return $this->bookingProtectCharge;
    }

    /**
     * @param float $bookingProtectCharge
     */
    public function setBookingProtectCharge($bookingProtectCharge)
    {
        $this->bookingProtectCharge = $bookingProtectCharge;
    }

    /**
     * @return float
     */
    public function getCharityCharge()
    {
        return $this->charityCharge;
    }

    /**
     * @param float $charityCharge
     */
    public function setCharityCharge($charityCharge)
    {
        $this->charityCharge = $charityCharge;
    }

    /**
     * @param float $basePrice
     */
    public function setBasePrice($basePrice)
    {
        $this->basePrice = $basePrice;
    }

    /**
     * @return float
     */
    public function getBasePrice()
    {
        return $this->basePrice;
    }

    /**
     * @param float $petsPrice
     */
    public function setPetsPrice($petsPrice)
    {
        $this->petsPrice = $petsPrice;
    }

    /**
     * @return float
     */
    public function getPetsPrice()
    {
        return $this->petsPrice;
    }

    /**
     * @param float $total
     */
    public function setTotal($total)
    {
        $this->total = $total;
    }

    /**
     * @return float
     */
    public function getTotal()
    {
        return $this->total;
    }

    /**
     * @return boolean
     */
    public function isIsSpecialOffer()
    {
        return $this->isSpecialOffer;
    }

    /**
     * @param boolean $isSpecialOffer
     */
    public function setIsSpecialOffer($isSpecialOffer)
    {
        $this->isSpecialOffer = $isSpecialOffer;
    }
}
