<?php

namespace UTT\ReservationBundle\Model;

class ReservationDataDiscountPrice {
    /** @var float $discountCodePrice */
    public $discountCodePrice;

    /** @var string $discountCode */
    public $discountCode;

    /** @var float $total */
    public $total;

    public function __construct(\StdClass $discountPriceData = null){
        $this->discountCodePrice = 0;
        $this->discountCode = '';
        $this->total = 0;

        if(isset($discountPriceData->discountCodePrice)) $this->setDiscountCodePrice($discountPriceData->discountCodePrice);
        if(isset($discountPriceData->discountCode)) $this->setDiscountCode($discountPriceData->discountCode);
        if(isset($discountPriceData->total)) $this->setTotal($discountPriceData->total);
    }

    /**
     * @param float $discountCodePrice
     */
    public function setDiscountCodePrice($discountCodePrice)
    {
        $this->discountCodePrice = $discountCodePrice;
    }

    /**
     * @return float
     */
    public function getDiscountCodePrice()
    {
        return $this->discountCodePrice;
    }

    /**
     * @return string
     */
    public function getDiscountCode()
    {
        return $this->discountCode;
    }

    /**
     * @param string $discountCode
     */
    public function setDiscountCode($discountCode)
    {
        $this->discountCode = $discountCode;
    }

    /**
     * @param float $total
     */
    public function setTotal($total)
    {
        $this->total = $total;
    }

    /**
     * @return float
     */
    public function getTotal()
    {
        return $this->total;
    }

}