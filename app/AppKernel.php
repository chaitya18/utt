<?php

use Symfony\Component\HttpKernel\Kernel;
use Symfony\Component\Config\Loader\LoaderInterface;

class AppKernel extends Kernel
{
    public function registerBundles()
    {
        $bundles = array(
            new Symfony\Bundle\FrameworkBundle\FrameworkBundle(),
            new Symfony\Bundle\SecurityBundle\SecurityBundle(),
            new Symfony\Bundle\TwigBundle\TwigBundle(),
            new Symfony\Bundle\MonologBundle\MonologBundle(),
            new Symfony\Bundle\SwiftmailerBundle\SwiftmailerBundle(),
            new Symfony\Bundle\AsseticBundle\AsseticBundle(),
            new Doctrine\Bundle\DoctrineBundle\DoctrineBundle(),
            new Sensio\Bundle\FrameworkExtraBundle\SensioFrameworkExtraBundle(),
            new UTT\IndexBundle\UTTIndexBundle(),

            new Sonata\CoreBundle\SonataCoreBundle(),
            new FOS\UserBundle\FOSUserBundle(),
            new Sonata\AdminBundle\SonataAdminBundle(),
            new Sonata\BlockBundle\SonataBlockBundle(),
            new Sonata\DoctrineORMAdminBundle\SonataDoctrineORMAdminBundle(),
            new Knp\Bundle\MenuBundle\KnpMenuBundle(),
            new Sonata\UserBundle\SonataUserBundle(),
            //new Application\Sonata\UserBundle\ApplicationSonataUserBundle(),
            new Sonata\EasyExtendsBundle\SonataEasyExtendsBundle(),

            new UTT\AdminBundle\UTTAdminBundle(),
            new UTT\EstateBundle\UTTEstateBundle(),

            new FOS\JsRoutingBundle\FOSJsRoutingBundle(),
            new Liip\ImagineBundle\LiipImagineBundle(),
            new FOS\FacebookBundle\FOSFacebookBundle(),
            new UTT\ReservationBundle\UTTReservationBundle(),
            new UTT\UserBundle\UTTUserBundle(),

            new SimpleThings\EntityAudit\SimpleThingsEntityAuditBundle(),

            new Ivory\CKEditorBundle\IvoryCKEditorBundle(),
            new WhiteOctober\TCPDFBundle\WhiteOctoberTCPDFBundle(),
            new UTT\ApiBundle\UTTApiBundle(),
            new UTT\QueueBundle\UTTQueueBundle(),
            new UTT\AnalyticsBundle\UTTAnalyticsBundle(),
            new BOMO\IcalBundle\BOMOIcalBundle(),
        );

        if (in_array($this->getEnvironment(), array('dev', 'test'))) {
            $bundles[] = new Symfony\Bundle\WebProfilerBundle\WebProfilerBundle();
            $bundles[] = new Sensio\Bundle\DistributionBundle\SensioDistributionBundle();
            $bundles[] = new Sensio\Bundle\GeneratorBundle\SensioGeneratorBundle();
        }

        return $bundles;
    }

    public function registerContainerConfiguration(LoaderInterface $loader)
    {
        $loader->load(__DIR__.'/config/config_'.$this->getEnvironment().'.yml');
    }
}